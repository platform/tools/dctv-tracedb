# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for installing system dependencies
include sysdeps-base.mk

.ONESHELL:

# Keep sorted within each block
debian_pkgs:=

# Core
debian_pkgs+=python3-pip
debian_pkgs+=python$(CONFIG_PYVER)
debian_pkgs+=python$(CONFIG_PYVER)-venv
debian_pkgs+=python$(CONFIG_PYVER)-dev
debian_pkgs+=ragel
debian_pkgs+=libpcre2-dev
debian_pkgs+=libblosc-dev

# Compilers
debian_pkgs+=clang-9

# GUI
debian-pkgs+=libcairo2-dev
debian_pkgs+=gobject-introspection
debian_pkgs+=libgirepository1.0-dev
debian_pkgs+=libgtk-3-dev
debian_pkgs+=gir1.2-gtk-3.0

# For documentation
debian_pkgs+=ghostscript
debian_pkgs+=poppler-utils
debian_pkgs+=rsync
debian_pkgs+=tcl
debian_pkgs+=tk

# For abseil
debian_pkgs+=cmake

define check_debian_pkg
printf 'Checking for package %s... ' "$(1)"
if dpkg -s $(1) 2>/dev/null | grep -q 'Status.*installed'; then
	echo found
else
	echo MISSING
	missing+="$(1) "
fi
$(newline)
endef

uname_a:=$(shell uname -a)
is_debian:=$(filter Debian,$(uname_a))
ifeq ($(is_debian),)
$(error "Only Debian-based systems supported at the moment")
endif

check_deps=$(foreach pkg,$(debian_pkgs),$(call check_debian_pkg,$(pkg)))

sysdeps.stamp: sysdeps.mk
	@set -e
	missing=
	$(check_deps)
	if [ -n "$$missing" ]; then
		echo >&2 "missing packages: $$missing"
		echo >&2 "try running this command, then running make again:"
		echo >&2 "  sudo apt install $$missing"
		exit 1
	fi
	touch $@

.PHONY: all
all: sysdeps.stamp dummy

.DEFAULT_GOAL:=all
