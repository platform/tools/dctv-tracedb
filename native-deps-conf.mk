# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile containing some native package dependencies
abseil_upstream:=https://github.com/abseil/abseil-cpp.git
abseil_rev:=c964fcffac27bd4a9ff67fe393410dd1146ef8b8
# Checked into third_party under the filename
# third_party/boost_$(boost_version).tar.xz extracting to a directory
# called boost_$(boost_version).
boost_version:=1_72_0
