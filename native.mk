# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for the native code in DCTV
#
# This file is invoked recursively by the top-level Makefile after it
# verifies that native dependencies are installed.  We split this file
# into a separate Makefile so that we can use top-level shell-based
# variable assignments and fail immediately (in a user-unfriendly
# manner) if something goes wrong.  It's the top-level Makefile's job
# to guide the user in a more friendly fashion through
# dependency installation.
#
# No, we can't use delayed-expansion variables instead, since these
# will call shell evaluation on _each_ reference!

include	native-base.mk
include native-conf.mk

# TODO(dancol):	support out-of-tree builds

CMAKE=cmake

.SECONDARY:

CXX_MODULES:= \
	agg \
	autoevent \
	awaitable_qe_call \
	backfill \
	block \
	block_builder \
	command_stream \
	dctv \
	dump_events_to_fd \
	event_parser_gen \
	fsutil \
	hunk \
	input_channel \
	io_spec \
	merge_spans_into_events \
	mmap \
	native_aggregation \
	npy \
	npyiter \
	operator_context \
	output_channel \
	pcreutil \
	perf \
	pybuffer \
	pyerr \
	pylog \
	pyobj \
	pyseq \
	pythread \
	pyutil \
	qe_call_io \
	qe_call_resize_buffers \
	qe_call_setup \
	qe_call_terminated \
	qe_call_yield_to_query_runner \
	query \
	query_cache \
	query_execution \
	query_key \
	result_buffer \
	simple_variant \
	sort \
	span \
	span_fixup \
	span_group \
	span_group_column \
	span_merge_join \
	span_pivot \
	span_pivot_column \
	stackify_start_stop \
	string_table \
	sync_coro \
	take \
	test_dummy \
	thread_analysis \
	time_series_to_span \
	unique_fd \
	$(EMPTY)

CXX_MODULES_SRC:=$(addsuffix .cpp,$(CXX_MODULES))
CXX_SOURCES:=$(addprefix $(DCTV_SRC)/,$(CXX_MODULES_SRC))
CXX_OBJ:=$(CXX_SOURCES:.cpp=.o)

$(DCTV_SRC)/include_all_modules.h: native-conf.mk
	: > $@
	for module in $(CXX_MODULES); do \
		echo "#include \"$$module.h\"" >> $@; \
	done

$(DCTV_SRC)/do_all_modules.h: native-conf.mk
	: > $@
	for module in $(CXX_MODULES); do \
		echo "DOIT($$module)" >> $@; \
	done

$(DCTV_SRC)/dctv.o: $(DCTV_SRC)/include_all_modules.h
$(DCTV_SRC)/dctv.o: $(DCTV_SRC)/do_all_modules.h

ABSEIL_CMAKE_OPTIONS:=
ABSEIL_CMAKE_OPTIONS+=-DBUILD_TESTING=OFF
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_POSITION_INDEPENDENT_CODE=YES
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_CXX_STANDARD=17

# Ugh.  CMake barfs if CXX is a multi-word sequence.  We need to give
# it a separate "launcher" if we're using ccache.
CMAKE_CXX:=$(CXX)
ifeq ($(firstword $(CMAKE_CXX)),ccache)
CMAKE_CXX:=$(wordlist 2,$(words $(CXX)),$(CXX))
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_CXX_COMPILER_LAUNCHER=ccache
endif
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_CXX_COMPILER="$(CMAKE_CXX)"
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_CXX_FLAGS="$(CFLAGS) -DDCTV_DETERMINISTIC_ABSEIL=1"
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_CXX_VISIBILITY_PRESET=hidden

# We need the `which` invocation below because if cmake doesn't have
# an absolute(!) path to the ar or ranlib executable, it tries to run
# the program relative to the build directory instead of
# searching PATH.

ifdef LTO_RANLIB
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_RANLIB=$(shell which $(RANLIB))
endif

ifdef LTO_AR
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_AR=$(shell which $(AR))
endif

# Let's be boring.
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_VERBOSE_MAKEFILE=YES
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_COLOR_MAKEFILE=NO
ABSEIL_CMAKE_OPTIONS+=-DCMAKE_RULE_MESSAGES=NO

$(ABSEIL_INSTALL_DIR)/mkdir-stamp:
	mkdir -p $(ABSEIL_INSTALL_DIR)
	touch $@

$(ABSEIL_BUILD_DIR)/stamp: $(ABSEIL_INSTALL_DIR)/mkdir-stamp
	rm -rf $(ABSEIL_BUILD_DIR)
	mkdir -p $(ABSEIL_BUILD_DIR)
	cd $(ABSEIL_BUILD_DIR) && $(CMAKE) $(ABSEIL_CMAKE_OPTIONS) -DCMAKE_INSTALL_PREFIX=$(realpath $(ABSEIL_INSTALL_DIR)) $(realpath $(ABSEIL_SRC_DIR))
	$(MAKE) -C $(ABSEIL_BUILD_DIR)
	touch $@

$(ABSEIL_INSTALL_DIR)/stamp: $(ABSEIL_INSTALL_DIR)/mkdir-stamp $(ABSEIL_BUILD_DIR)/stamp
	$(MAKE) -C $(ABSEIL_BUILD_DIR) install
	touch $@

boost_dir:=$(VENV)/boost
dctv_boost_jam:=using gcc : : $(CXX) : <ranlib>"$(RANLIB)" <archiver>"$(AR)" ;

$(BOOST_INSTALL_DIR)/stamp: native-conf.mk
	rm -rf $(BOOST_INSTALL_DIR) $(BOOST_BUILD_DIR)
	mkdir -p $(BOOST_BUILD_DIR)
	mkdir -p $(BOOST_INSTALL_DIR)
	$(file >dctv.jam,$(dctv_boost_jam))
	env -C $(boost_dir)/tools/build ./bootstrap.sh
	env -C $(boost_dir)/tools/build ./b2 install --prefix=$(PWD)/$(BOOST_INSTALL_DIR)
	env -C $(boost_dir) $(PWD)/$(BOOST_INSTALL_DIR)/bin/b2 --config=$(PWD)/dctv.jam --no-cmake-config --build-dir=$(PWD)/$(BOOST_BUILD_DIR) --stagedir=$(PWD)/$(BOOST_BUILD_DIR) --prefix=$(PWD)/$(BOOST_INSTALL_DIR) --reconfigure -a --layout=system link=static threading=multi cflags="$(CFLAGS)" cxxflags="$(CFLAGS)" linkflags="$(LDFLAGS)" toolset=gcc -d2 $(boost_libs:%=--with-%) -j`nproc` install
	touch $@

%.cpp: %.cpp.in $(DCTV_SRC)/parsers.rl native-conf.mk
	rm -f $@
	ragel -G2 -o $@ $<
	chmod -w $@

native_deps:=$(ABSEIL_INSTALL_DIR)/stamp $(BOOST_INSTALL_DIR)/stamp

%.o: %.cpp native-conf.mk $(native_deps)
	$(CXX) -MMD -MP -c -o $@ -I$(NUMPY_INCDIR) -I$(ABSEIL_INSTALL_DIR)/include -I$(BOOST_INSTALL_DIR)/include $(CPPFLAGS) $(PCRE2_CFLAGS) $(PYTHON_CFLAGS) $(WARNINGS) $(CFLAGS) $<

# Abseil has a ton of tiny internal libraries for random things.
# We could just manually find the correct toposort or parse the
# generated CMake data, but for now, let's just brute force it with
# the linker.
ABSEIL_LIBS=-Wl,--start-group $(wildcard $(ABSEIL_INSTALL_DIR)/lib/*.a) -Wl,--end-group

$(DCTV_SRC)/_native.so: $(CXX_OBJ) $(native_deps) native-conf.mk
	$(CXX) -shared -Wl,--gc-sections -o $@ $(PCRE2_CFLAGS) $(WARNINGS) $(CFLAGS) $(PYTHON_CFLAGS) $(CXX_OBJ) -L$(NUMPY_LIBDIR) -L$(ABSEIL_INSTALL_DIR)/lib -L $(LDFLAGS) $(PYTHON_LIBS) $(boost_libs:%=$(BOOST_INSTALL_DIR)/lib/libboost_%.a) -lnpymath $(PCRE2_LIBS) $(ABSEIL_LIBS) -lblosc

$(DCTV_SRC)/DctvGtkHack-1.0.typelib:
	g-ir-compiler --shared-library=$(GTK_SHARED_LIBRARY) $(DCTV_SRC)/DctvGtkHack-1.0.gir > $@

$(DCTV_SRC)/DctvGObjectHack-1.0.typelib:
	g-ir-compiler --shared-library=$(GOBJECT_SHARED_LIBRARY) $(DCTV_SRC)/DctvGObjectHack-1.0.gir > $@

.PHONY: all
all: dummy
all: $(DCTV_SRC)/DctvGtkHack-1.0.typelib
all: $(DCTV_SRC)/DctvGObjectHack-1.0.typelib
all: $(DCTV_SRC)/_native.so

.DEFAULT_GOAL:=all

-include $(DCTV_SRC)/*.d
