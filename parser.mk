# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for DCTV's SQL paraer
include parser-base.mk

$(PARSER_FILES): $(DCTV_SRC)/sql_parser.py
	./dctv internal-regenerate-sql-parser

parsers.dot: $(DCTV_SRC)/parsers.rl
	ragel -Vp -o $@ $<

parsers.pdf: parsers.dot
	dot -Tpdf -Gdpi=200 $< > $@

dctv.el: dctv.el.in $(DCTV_SRC)/sql_parser.py
	rm -f $@
	sed <dctv.el.in -e "s/^ *;;; *@DCTV_KEYWORDS@$$/$$(./dctv internal-dump-keywords | sed -e 's/.*/\"\0\"/' | tr -s '\n' ' ')/" > $@
	chmod -w $@

.PHONY: all
all: $(PARSER_FILES) dctv.el dummy

.DEFAULT_GOAL=all

# Make sure we run regenerate-sql-parser only once
.NOTPARALLEL:
