# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for probably-bitrotten standalone distribution machinery.

include dist-base.mk

ALL_PY:=$(shell find src/ -name '*.py')

PYVER:=$(shell $(PYBIN) -c 'import sys; print("{}.{}".format(*sys.version_info[:2]))')
ifeq ($(PYVER),)
$(error "No python3 binary found")
endif

DIST_PACKAGES:=/usr/lib/python3/dist-packages
GI_DIR:=$(DIST_PACKAGES)/gi
CAIRO_DIR:=$(DIST_PACKAGES)/cairo
VENV_SITE_PACKAGES:=$(VENV_NAME)/lib/python$(PYVER)/site-packages

.PHONY: dctv.dist.opt
dctv.dist.opt: dctv.dist.opt/stamp
dctv.dist.opt/stamp: $(VENV_NAME)/stamp $(ALL_PY) dev
	rm -rf dist/ build/ $(dir $@)
	VIRTUAL_ENV=$(PWD)/$(VENV_NAME) \
	DCTV_OPT=1 \
	$(PWD)/venv-cmd.sh python3 -O setup.py install -O1 --prefix=$(PWD)/dist
	rm -rf build/
	find dist/ -name '*.so' -exec strip {} +
	touch dist/stamp
	mv dist/ $(dir $@)

.PHONY: dctv.dist.dbg
dctv.dist.dbg: dctv.dist.dbg/stamp
dctv.dist.dbg/stamp: $(VENV_NAME)/stamp $(ALL_PY) dev
	rm -rf dist/ build/ $(dir $@)
	VIRTUAL_ENV=$(PWD)/$(VENV_NAME) \
	$(PWD)/venv-cmd.sh python3 setup.py install --prefix=$(PWD)/dist
	rm -rf build/
	touch dist/stamp
	mv dist/ $(dir $@)

out/dctv.%: dctv.dist.%/stamp dctv.script.in
	mkdir -p $(dir $@)
	rm -f $@
	cat \
		<(sed -e \
			"s/@version@/$(notdir $@)-$$(./getver.sh)/" \
			dctv.script.in) \
		<(cd $(dir $<) && tar -cf- . \
		| uuencode -) > $@
	chmod +x $@

venv_cmd:=\
	CC=$(PWD)/ccache-gcc.sh \
	CXX=$(PWD)/ccache-g++.sh \
	VIRTUAL_ENV=$(PWD)/$(VENV_NAME) \
	$(PWD)/venv-cmd.sh

# Use a stamp file so that we consider the venv complete only after we
# finish building it.
.PHONY: $(VENV_NAME)
$(VENV_NAME): $(VENV_NAME)/stamp
$(VENV_NAME)/stamp: requirements.txt
	./check-package.sh ccache
	./check-package.sh python3-gi
	./check-package.sh python3-cairo
	./check-package.sh libblas-dev
	./check-package.sh liblapack-dev
	./check-package.sh python$(PYVER)-venv
	./check-package.sh python$(PYVER)-dev
	./check-package.sh gfortran

	gfortran --version >/dev/null

	rm -rf "$(VENV_NAME)"
	python3 -m venv $(VENV_NAME)

	$(venv_cmd) \
		python3 -O -m pip install \
			--install-option="--optimize=1" \
			-r requirements.txt

	[[ -d $(GI_DIR) ]] && ln -Tvsf \
		$(GI_DIR) \
		$(VENV_SITE_PACKAGES)/gi

	[[ -d $(CAIRO_DIR) ]] && ln -Tvsf \
		$(CAIRO_DIR) \
		$(VENV_SITE_PACKAGES)/cairo

	cd third_party/cx_Freeze && \
		$(venv_cmd) python3 -O setup.py install --optimize=1

	cd third_party/Banyan && \
		$(venv_cmd) python3 -O setup.py install --optimize=1

	touch $(VENV_NAME)/stamp

all: out/dctv.opt out/dctv.dbg dev
	@echo -n "Build complete. Standalone release "
	@echo "binaries are in the \`out' directory."

# Unfortunately, we need to disable parallelism because setuptools
# hardcodes output paths, access to which we need to serialize.  (We
# can't just use separate subdirectories: cx_Freeze depends on cwd
# being at setup.py.)
.NOTPARALLEL:
