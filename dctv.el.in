;; Copyright (C) 2020 The Android Open Source Project
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;      http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
;; Emacs utilities for working with DCTV code.

(require 'align)
(require 'sql-indent)
(require 'rx)


;; DCTV source code editing

(defconst dctv-alignment-rules
  `((dctv-event-table
     (regexp    .  ,(rx bol
                        (* (syntax whitespace))
                        ?\"
                        (* (not (any "\"")))
                        ?\"
                        (* (syntax whitespace))
                        ":"
                        (group
                         (+ (syntax whitespace))
                         (not (syntax whitespace))
                         (* nonl)
                         ",")
                        eol))
     (modes     .  '(python-mode))
     (group     .  1)
     (tab-stop  .  nil)
     (justify   .  t)
     (column    .  50)
     (separate  .  ,(rx bol
                        (| (: (* (syntax whitespace))
                              symbol-start
                              (| "def" "class" "assert")
                              symbol-end)
                           (: (not (syntax whitespace)))
                           (: (* (syntax whitespace))
                              eol))))
     )))

(defconst dctv-alignment-exclude-rules
  (list
   (assq 'exc-open-comment
         align-exclude-rules-list)))

(defun dctv-align (beg end)
  "Apply DCTV-specific alignment rules"
  (interactive "r")
  (align beg end nil
         dctv-alignment-rules
         dctv-alignment-exclude-rules))

(defun dctv-align-current ()
  "Apply DCTV-specific alignment rules to current section"
  (interactive)
  (align-current dctv-alignment-rules
                 dctv-alignment-exclude-rules))


;; DCTV SQL mode

(defconst dctv-sql-font-lock-keywords
  (eval-when-compile
    (list
     (sql-font-lock-keywords-builder
      'font-lock-keyword-face nil
      ;;; @DCTV_KEYWORDS@
      )

     (sql-font-lock-keywords-builder
      'font-lock-type-face nil
      "bool"
      "float32"
      "float64"
      "int16"
      "int32"
      "int64"
      "int8"
      "uint16"
      "uint32"
      "uint64"
      "uint8"
      )))

  "DCTV SQL keywords used by sql-mode.

This variable is used by `sql-mode' and `sql-interactive-mode'.  The
regular expressions are created during compilation by calling the
function `regexp-opt'.  Therefore, take a look at the source before
you define your own `dctv-font-lock-keywords'.")

(defconst dctv-sql-product
  '(:free-software t
    :font-lock dctv-sql-font-lock-keywords
    :prompt-regexp "^DCTV> "
    :prompt-length 5
    :syntax-alist ((?# . "< b"))
    :input-filter sql-remove-tabs-filter))

(ignore-errors
  (sql-del-product 'dctv))
(sql-add-product 'dctv "DCTV" dctv-sql-product)

(defun dctv-sql-p ()
  "Whether the current mode is for editing DCTV-SQL"
  (eq sql-product 'dctv))

(defun dctv-sqlind-maybe-defun-hook (fn &rest args)
  (if (and (dctv-sql-p)
           (looking-back
            (rx "view" (+ (syntax whitespace)) point)
            (point-at-bol)))
      nil
    (apply fn args)))

(advice-add 'sqlind-maybe-defun-statement
            :around
            'dctv-sqlind-maybe-defun-hook)

(defun dctv-sqlind-if-candidate-hook (fn &rest args)
  (and (apply fn args)
       (if (dctv-sql-p)
           (and
            (not (save-excursion
                   (forward-sexp)
                   (skip-syntax-forward "-")
                   (eq (char-after) ?\()
                   ))
            (not (looking-back
                  (rx "create" (+ (syntax whitespace)) point)
                  (point-at-bol))))
         t)))

(advice-add 'sqlind-good-if-candidate
            :around
            'dctv-sqlind-if-candidate-hook)

(defun dctv-sqlind-maybe-skip-create-options-hook (fn &rest args)
  (if (and (dctv-sql-p)
           (looking-at
            (rx symbol-start "if" symbol-end)))
      (progn
        (ignore-errors (forward-sexp 2))
        (sqlind-forward-syntactic-ws))
    (apply fn args)))

(advice-add 'sqlind-maybe-skip-create-options
            :around
            'dctv-sqlind-maybe-skip-create-options-hook)

(defun dctv-sqlind-current-line-with-documentation-p ()
  (save-excursion
    (back-to-indentation)
    (let ((case-fold-search t))
      (looking-at
       (rx
        ;;(* ")" (* (syntax whitespace)))
        symbol-start "with" symbol-end
        (+ (syntax whitespace))
        symbol-start "documentation" symbol-end)))))

(defun dctv-sqlind-syntax-of-line (fn)
  (let ((syntax (funcall fn)))
    (when (dctv-sql-p)
      (when (and (or (eq (caar syntax) 'select-table-continuation)
                     (eq (car-safe (caar syntax)) 'in-select-clause))
                 (cdr syntax)
                 (dctv-sqlind-current-line-with-documentation-p))
        ;; We're not really looking at a continuation of the
        ;; select-table so much as a continuation of the
        ;; larger statement.
        (pop syntax))
      (when (and (eq (caar syntax) 'string-continuation)
                 (save-excursion
                   (goto-char (cdar syntax))
                   (dctv-sqlind-current-line-with-documentation-p)))
        (pop syntax)))
    syntax))

(advice-add 'sqlind-syntax-of-line
            :around
            'dctv-sqlind-syntax-of-line)

(provide 'dctv)
