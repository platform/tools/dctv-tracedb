# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Top-level Makefile
.PHONY: help
help:
	@echo Use '"make dev" to set up local development'
	@echo Use '"make check" to run the test suite'
	@echo Use '"make doc" to build documentation'
	@echo Use '"make dist" to build standalone binaries'
	@echo Use '"make clean" to remove build products'
.DEFAULT_GOAL:=help

all_sub_makefiles:=$(wildcard *.mk)
sub_makefiles:=$(all_sub_makefiles)
sub_makefiles:=$(filter-out %-base.mk,$(sub_makefiles))
sub_makefiles:=$(filter-out %-conf.mk,$(sub_makefiles))
sub_makefiles:=$(filter-out base.mk,$(sub_makefiles))
modules:=$(sub_makefiles:.mk=)

define per-module-define
clean-$1:
	@+$(MAKE) --no-print-directory -f $1-base.mk clean
all-$1:
	@+$(MAKE) --no-print-directory -f $1.mk
endef

$(foreach module,$(modules),$(eval $(call per-module-define,$(module))))

all-venv: all-sysdeps

all-native: all-venv

# all-parser depends on all-native because the SQL parser library
# imports from _native.  Maybe it shouldn't?
all-parser: all-venv all-native

.PHONY: clean
clean: $(modules:%=clean-%)
	find . -type d -name '__pycache__' -exec rm -rvf {} +
	rm -rf .pytest_cache
	rm -rf src/dctv.egg-info
	rm -f pingpong

.PHONY: dev
dev: all-native all-parser

DEV_COMPILER=clang++-9

dev-debug: CXX=$(DEV_COMPILER)
dev-debug:
	$(MAKE) CFLAGS_OPT=-O1 NDEBUG= CXX="$(CXX)" dev

dev-debug-opt: CXX=$(DEV_COMPILER)
dev-debug-opt:
	$(MAKE) CFLAGS_OPT=-O3 NDEBUG= CXX="$(CXX)" dev

dev-debug-noopt: CXX=$(DEV_COMPILER)
dev-debug-noopt:
	$(MAKE) CFLAGS_OPT=-O0 NDEBUG= CXX="$(CXX)" dev

check: dev
	./pytest src/

.PHONY: doc
doc: all-doc

.PHONY: push-doc-x20
push-doc-x20:
	@+$(MAKE) --no-print-directory -f doc.mk $@

# Simple IPC benchmark
pingpong: pingpong.cpp
	g++ -Wall -O2 -std=gnu++17 $< -o $@

# Ugh. Hack.
always:
compile_commands.json: always
	$(MAKE) clean-native
	rm -f compile_commands.json
	bear $(MAKE) dev-debug-noopt

.PHONY: clang-tidy-lint
clang-tidy-lint: compile_commands.json
	clang-tidy-7 src/dctv/*.cpp
