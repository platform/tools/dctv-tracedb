// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// Simple program for establishing a best-case, native-code IPC
// latency bound so we can see how much overhead the modernmp stuff
// adds on top.
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/wait.h>

#include <chrono>
#include <iostream>
#include <string>

void
xclose(int fd)
{
  if (close(fd) && errno == EBADF)
    abort();
}

__attribute__((noreturn))
void
child(int sock)
{
  char buf;
  for (;;) {
    ssize_t ret = TEMP_FAILURE_RETRY(read(sock, &buf, 1));
    if (ret < 0)
      abort();
    if (ret == 0)
      break;
    ret = TEMP_FAILURE_RETRY(write(sock, &buf, 1));
    if (ret < 1)
      abort();
  }
  exit(0);
}

int
main(int argc, char** argv)
{
  if (argc < 2) {
    fprintf(stderr, "error: no iteration count supplied\n");
    return 1;
  }
  int iterations = std::stoi(argv[1]);

  int sockets[2];
  if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, sockets))
    abort();
  pid_t child_pid = fork();
  if (child_pid < 0)
    abort();
  if (!child_pid) {
    xclose(sockets[1]);
    child(sockets[0]);
  }
  xclose(sockets[0]);
  int sock = sockets[1];
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < iterations; ++i) {
    char buf = '.';
    char reply_buf;
    ssize_t ret = TEMP_FAILURE_RETRY(write(sock, &buf, 1));
    if (ret < 1)
      abort();
    ret = TEMP_FAILURE_RETRY(read(sock, &reply_buf, 1));
    if (ret < 1)
      abort();
    assert(buf == reply_buf);
  }
  xclose(sock);
  if (TEMP_FAILURE_RETRY(wait(nullptr)) != child_pid)
    abort();

  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  std::chrono::duration<double, std::milli> elapsed_ms = elapsed;
  printf("elapsed: %gms\n", elapsed_ms.count());
  printf("%gus per iteration\n", (1000*elapsed_ms.count()) / iterations);
  return 0;
}
