# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Base makefile
.DELETE_ON_ERROR:
DCTV_SRC:=src/dctv
VENV:=venv
EMPTY:=
CONFIG_PYVER:=3.8
PYBIN:=python$(CONFIG_PYVER)
ABSEIL_SRC_DIR_VENV_RELATIVE=abseil
ABSEIL_SRC_DIR:=$(VENV)/$(ABSEIL_SRC_DIR_VENV_RELATIVE)

ifneq ($(filter 3.%,$(MAKE_VERSION)),)
$(error You are running GNU Make $(MAKE_VERSION), which is too old.  We need at least 4.1)
endif

define newline

endef

# For suppressing "nothing to be done for all"
.PHONY: dummy
dummy:
	@true
