DCTV's C++ coding style is basically the Google C++ one for
*formatting* with good judgement substituted for the fuzzy bits.

* We love C++ exceptions.  We also love noexcept.
  Annotate functions that don't throw and that logically as part of
  their contract don't throw as "noexcept".  Don't slap "noexcept"
  on something that could conceptually throw and just happens not to
  throw today.

* Mark classes final when possible.

* Separate functionality into small chunks, split across foo.h,
  foo-inl.h, and foo.cpp, to keep incremental build times low.

* The list of C++ files to build is in native.mk.  Each one defined an
  void init_FILENAME(pyref m) function that we call automatically from
  the Python initialization function.

* Use Python exceptions for all program errors, even ones caused by
  C++, unless there's a good reason for doing otherwise.

* Brief usage comments go in headers.

* All member functions are defined in out-of-line style and in
  foo-inl.h, except when practical considerations or weird enable_if
  or other template nonsense dictate that we define them inline.
  This way, we keep the headers generally clean.


