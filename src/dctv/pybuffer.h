// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <functional>

#include "pyutil.h"

namespace dctv {

struct PyBuffer final {
  inline explicit PyBuffer(pyref obj);
  inline ~PyBuffer();
  inline const Py_buffer* operator->() const noexcept;
 private:
  Py_buffer py_buffer;
};

using GetBufferFunction =
    std::function<int(PyObject* self,
                      Py_buffer* view,
                      int flags)>;

unique_pyref make_buffer_holder(
    GetBufferFunction&& get_buffer,
    char dtype);

void init_pybuffer(pyref m);

}  // namespace dctv

#include "pybuffer-inl.h"
