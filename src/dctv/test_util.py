# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for utility code"""
# pylint: disable=missing-docstring

import logging
import collections.abc

import pytest
import pypict.tools
from cytoolz import get

from .util import (
  AutoNumber,
  AutoNumberFast,
  AutoNumberSafe,
  EqImmutable,
  ExplicitInheritance,
  FrozenDict,
  FrozenIntRangeDict,
  IdentityHashedImmutable,
  Immutable,
  ImmutableError,
  InheritanceConstraintViolationError,
  Interned,
  SortedDict,
  abstract,
  argmax,
  argmin,
  cached_property,
  common_prefix,
  enumattr,
  final,
  iattr,
  load_pandas,
  override,
  passivize_re,
  sattr,
  tattr,
  timeline_to_eta,
)

log = logging.getLogger(__name__)

def test_immutable():
  class _TestImmutable(Immutable):
    foo = iattr(int, default=5)
  assert _TestImmutable().foo == 5
  assert _TestImmutable(7).foo == 7
  assert _TestImmutable(foo=9).foo == 9
  with pytest.raises(AssertionError):
    _TestImmutable("5")

def test_immutable_post_init_check_elision():
  class _TestImmutable(Immutable):
    foo = iattr(int, default=5, kwonly=True)
  class _TestImmutable2(_TestImmutable):
    @override
    def _post_init_check(self):
      super()._post_init_check()
  pic_called = False
  def _pic_hack(_cls):
    nonlocal pic_called
    pic_called = True
  # pylint: disable=protected-access
  _TestImmutable._post_init_check = _pic_hack
  _TestImmutable2()
  assert pic_called
  pic_called = False
  class _TestImmutable3(_TestImmutable):
    pass
  _TestImmutable3()
  assert not pic_called

def test_immutable_post_init_assert_elision():
  class _TestImmutable(Immutable):
    foo = iattr(int, default=5, kwonly=True)
  class _TestImmutable2(_TestImmutable):
    @override
    def _post_init_assert(self):
      super()._post_init_assert()
  pic_called = False
  def _pic_hack(_cls):
    nonlocal pic_called
    pic_called = True
  # pylint: disable=protected-access
  _TestImmutable._post_init_assert = _pic_hack
  _TestImmutable2()
  assert pic_called
  pic_called = False
  class _TestImmutable3(_TestImmutable):
    pass
  _TestImmutable3()
  assert not pic_called

def test_immutable_evolve():
  class _TestImmutable(Immutable):
    foo = iattr(int, converter=int)
    bar = iattr(str, nullable=True, converter=str)
    qux = iattr(float)
  ti1 = _TestImmutable(5, "val1", 4.5)
  ti2 = ti1.evolve(bar="val2")
  assert ti2.foo == ti1.foo
  assert ti2.bar == "val2"
  ti3 = ti1.evolve(foo="4")
  assert ti3.foo == 4
  assert ti3.bar is ti1.bar

def test_immutable_evolve_optimized():
  class _TestImmutable(Immutable, emit_checks=False):
    foo = iattr(int, converter=int)
    bar = iattr(str, nullable=True, converter=str)
    qux = iattr(float)
    @override
    def _post_init_check(self):
      super()._post_init_check()
      assert self.foo < 10
  ti1 = _TestImmutable(5, "val1", 4.5)
  ti2 = ti1.evolve(bar="val2")
  assert ti2.foo == ti1.foo
  assert ti2.bar == "val2"
  ti3 = ti1.evolve(foo="4")
  assert ti3.foo == 4
  assert ti3.bar is ti1.bar
  with pytest.raises(AssertionError):
    ti1.evolve(foo=10)

def test_immutable_evolve_check():
  class _TestImmutable(Immutable):
    foo = iattr(int)
    bar = iattr(int)
    @override
    def _post_init_check(self):
      super()._post_init_check()
      assert self.foo >= 1
    @override
    def _post_init_assert(self):
      super()._post_init_assert()
      assert self.bar < 1
  ti = _TestImmutable(1, -1)
  assert (ti.foo, ti.bar) == (1, -1)
  with pytest.raises(AssertionError):
    ti.evolve(-1, -1)
  with pytest.raises(AssertionError):
    ti.evolve(1, 5)

def test_immutable_evolve_intern():
  class _TestImmutable(Interned):
    foo = iattr(int)
  ti1 = _TestImmutable(10)
  ti2 = _TestImmutable(10)
  assert ti1 is ti2
  ti3 = ti1.evolve(foo=20)
  assert ti3 is not ti1
  ti4 = ti1.evolve(foo=20)
  assert ti3 is ti4

def test_immutable_kw_only():
  class _TestImmutable(Immutable):
    foo = iattr(int, default=5, kwonly=True)
  assert _TestImmutable().foo == 5
  with pytest.raises(TypeError):
    _TestImmutable(7)
  assert _TestImmutable(foo=9).foo == 9
  class _TestImmutable2(Immutable):
    foo = iattr(kwonly=True)
  with pytest.raises(TypeError):
    _TestImmutable2()
  with pytest.raises(TypeError):
    _TestImmutable2(7)
  assert _TestImmutable2(foo=9).foo == 9

def test_immutable_assert_checker():
  class _TestImmutable(Immutable):
    foo = iattr(int, assert_checker=lambda x: x < 10)
  assert _TestImmutable(9).foo == 9
  with pytest.raises(AssertionError):
    _TestImmutable(10)

def test_immutable_converter():
  class _TestImmutable(Immutable):
    foo = iattr(int, converter=int)
  assert _TestImmutable("10").foo == 10

def test_immutable_converter_nullable():
  class _TestImmutable(Immutable):
    foo = iattr(int, converter=int, nullable=True)
  assert _TestImmutable("10").foo == 10
  assert _TestImmutable(None).foo is None

def test_immutable_converter_nullable_optimized():
  class _TestImmutable(Immutable, emit_checks=False):
    foo = iattr(int, converter=int, nullable=True)
  assert _TestImmutable("10").foo == 10
  assert _TestImmutable(None).foo is None

def test_immutable_converter_optimized():
  class _TestImmutable(Immutable, emit_checks=False):
    foo = iattr(int, converter=int)
  assert _TestImmutable("10").foo == 10

def test_immutable_divergent_name():
  class _TestImmutable(Immutable):
    foo = iattr(int, name="bar")
  assert _TestImmutable(bar=7).foo == 7

def test_immutable_post_init_check():
  class _TestImmutable(Immutable):
    foo = iattr(int)
    @override
    def _post_init_check(self):
      super()._post_init_check()
      assert self.foo < 7
  assert _TestImmutable(5).foo == 5
  with pytest.raises(AssertionError):
    _TestImmutable(7)

def test_immutable_post_init_assert():
  class _TestImmutable(Immutable):
    foo = iattr(int)
    @override
    def _post_init_assert(self):
      super()._post_init_assert()
      assert self.foo < 7
  assert _TestImmutable(5).foo == 5
  with pytest.raises(AssertionError):
    _TestImmutable(7)

class _TestImmutableOverrideNew(Immutable):
  foo = iattr(int)
  def __new__(cls, bar):
    return cls._do_new(cls, bar + 1)
def test_override_new():
  assert _TestImmutableOverrideNew(5).foo == 6

class _TestImmutablePickle(Immutable):
  foo = iattr(int)
def test_immutable_pickle_round_trip():
  im1 = _TestImmutablePickle(5)
  from pickle import dumps, loads
  im2 = loads(dumps(im1))
  assert im1 is not im2
  assert im1.foo == im2.foo

class _TestImmutableWithNewPickle(Immutable):
  bar = iattr(str)
  def __new__(cls, foo):
    return cls._do_new(cls, bar=repr(foo))

def test_immutable_with_new_pickle_round_trip():
  im1 = _TestImmutableWithNewPickle([1, 2, 3])
  assert im1.bar == repr([1, 2, 3])
  from pickle import dumps, loads
  im2 = loads(dumps(im1))
  assert im1 is not im2
  assert im1.bar == im2.bar

def test_interned_interned():
  class _TestInterned(Interned):
    value = iattr(converter=int)
  foo1 = _TestInterned(5)
  foo2 = _TestInterned(5)
  assert foo1 is foo2

def test_interned_interned_derived():
  class _TestInterned(Interned):
    pass

  class _TestInterned2(_TestInterned):
    value = iattr(converter=int)
  foo1 = _TestInterned2(5)
  foo2 = _TestInterned2(5)
  assert foo1 is foo2

def test_interned_interned_optimized():
  class _TestInterned(Interned, emit_checks=False):
    value = iattr(converter=int)
  foo1 = _TestInterned(5)
  foo2 = _TestInterned(5)
  assert foo1 is foo2

def test_immutable_key():
  class _TestImmutable1(Immutable):
    pass
  assert _TestImmutable1().__immutable_key__ == ()
  class _TestImmutable2(Immutable):
    value = iattr(int)
  assert _TestImmutable2(5).__immutable_key__ == 5
  class _TestImmutable3(Immutable):
    foo = iattr(int)
    bar = iattr(int)
  foo = _TestImmutable3(1, 2)
  key = foo.__immutable_key__
  assert key == (1, 2)
  assert foo.__immutable_key__ is key

def test_interned_str():
  class _TestInterned2(Interned):
    value = iattr((str, float))
  foo1 = _TestInterned2("asdfa")
  foo2 = _TestInterned2(4.3)
  assert foo1 is not foo2

class _TestPickleIntern(Interned):
  value = iattr(str)

def test_interned_pickle_round_trip():
  ia1 = _TestPickleIntern("foo")
  from pickle import dumps, loads
  assert loads(dumps(ia1)) is ia1

class _TestPickleInternWithNew(Interned):
  bar = iattr(int)
  @override
  def __new__(cls, foo):
    return cls._do_new(cls, bar=int(foo))

def test_interned_pickle_round_trip_explicit_new():
  ia1 = _TestPickleInternWithNew(foo="5")
  assert ia1.bar == 5
  from pickle import dumps, loads
  assert loads(dumps(ia1)) is ia1

def test_eq_immutable():
  class _Test(EqImmutable):
    foo = iattr(int)
    bar = iattr(str)
  foo = _Test(3, "hi")
  bar = _Test(3, "hi")
  qux = _Test(4, "hi")
  assert foo == bar
  assert hash(foo) == hash(bar)
  assert hash(foo) == hash(bar)  # Again, to test cache
  assert foo != qux
  assert hash(foo) != hash(qux)

def test_immutable_incomparable_by_default():
  class _Test(Immutable):
    foo = iattr(int)
    bar = iattr(str)
  foo = _Test(3, "hi")
  bar = _Test(3, "hi")
  with pytest.raises(TypeError):
    assert foo != bar
  with pytest.raises(TypeError):
    assert hash(foo) != hash(bar)

def test_immutable_identity_hash():
  class _Test(IdentityHashedImmutable):
    foo = iattr(str)
  foo = _Test("foo")
  bar = _Test("foo")
  assert foo != bar
  assert foo == foo  # pylint: disable=comparison-with-itself
  assert hash(foo) != hash(bar)

def test_immutable_field_inheritance():
  # pylint: disable=function-redefined
  class _Base(Immutable):
    blarg = iattr(int)
  with pytest.raises(ImmutableError) as ex:
    class _Derived(_Base):
      foo = iattr(str)
  assert "cannot inherit non-kw-only" in str(ex)
  class _Base(Immutable):
    blarg = iattr(int, kwonly=True)
  class _Derived(_Base):
    foo = iattr(str)
  inst = _Derived(foo="test", blarg=7)
  assert inst.foo == "test"
  assert inst.blarg == 7

def test_immutable_field_inheritance_constraint():
  # pylint: disable=function-redefined,no-self-use
  class _Base(Immutable):
    def blarg(self):
      return 5
  with pytest.raises(InheritanceConstraintViolationError):
    class _Derived(_Base):
      blarg = iattr(int)
  class _Base(Immutable):
    blarg = iattr(int, kwonly=True)
  with pytest.raises(InheritanceConstraintViolationError):
    class _Derived(_Base):
      def blarg(self):
        return 5

def test_immutable_field_inheritance_constraint_2():
  class _Base(Immutable):
    blarg = iattr(int, kwonly=True)
  with pytest.raises(ImmutableError):
    class _Derived(_Base):
      blarg = iattr(int, kwonly=True)

def test_immutable_tattr_notype():
  class _TestImmutable(Immutable):
    foo = tattr()
  assert _TestImmutable([1, 2, 3]).foo == (1, 2, 3)

def test_immutable_sattr_notype():
  class _TestImmutable(Immutable):
    foo = sattr()
  assert _TestImmutable([1, 2, 3]).foo == {3, 2, 1}

def test_immutable_sattr():
  class _TestImmutable(Immutable):
    foo = sattr(int)
  assert _TestImmutable([1, 2, 3]).foo == {3, 2, 1}
  with pytest.raises(AssertionError):
    _TestImmutable(["foo"])
  with pytest.raises(Exception):
    _TestImmutable(7)

def test_immutable_enumattr():
  class _Enum(AutoNumber):
    __start__ = 5
    FOO = ()
    BAR = ()

  class _TestImmutable(Immutable):
    foo = enumattr(_Enum)

  assert _TestImmutable(_Enum.FOO).foo == _Enum.FOO
  assert _TestImmutable(_Enum.BAR).foo == _Enum.BAR

  assert repr(_TestImmutable(_Enum.FOO)) == \
    "<_TestImmutable foo=test_immutable_enumattr.<locals>._Enum.FOO>"

  class _Enum2(AutoNumber):
    __start__ = 10
    QUX = ()

  class _TestImmutableMultiEnum(Immutable):
    foo = enumattr((_Enum, _Enum2))

  assert repr(_TestImmutableMultiEnum(_Enum2.QUX)) == \
    ("<_TestImmutableMultiEnum "
     "foo=test_immutable_enumattr.<locals>._Enum2.QUX>")

  with pytest.raises(Exception):
    _TestImmutable(99)

def test_frozendict():
  """Test FrozenDict"""
  rdict = dict(foo=5, bar=7, qux=9)
  fdict = FrozenDict(rdict)
  assert sorted(rdict.keys()) == sorted(fdict.keys())
  assert sorted(rdict.values()) == sorted(fdict.values())
  assert sorted(rdict.items()) == sorted(fdict.items())
  fdict2 = FrozenDict(rdict)
  assert fdict == fdict2
  assert hash(fdict) == hash(fdict2)
  fdict3 = FrozenDict(qux=11)
  assert fdict3 != fdict2

def test_frozendict_serialization():
  """Test that frozendict survives pickling"""
  rdict = dict(foo=5, bar=7, qux=9)
  fdict = FrozenDict(rdict)
  from pickle import dumps, loads
  xdict = loads(dumps(fdict))
  assert xdict == fdict
  assert hash(xdict) == hash(fdict)


def test_frozen_int_range_dict():
  """Test that the range dict works"""
  ranges = [
    (4, "foo"),
    (5, "qux"),
    (7, "bar"),
  ]
  fird = FrozenIntRangeDict(ranges, 10)
  assert list(fird.items()) == [
    (4, "foo"),
    (5, "qux"),
    (6, "qux"),
    (7, "bar"),
    (8, "bar"),
    (9, "bar"),
  ]
  assert len(fird) == 6
  assert fird[5] == "qux"
  assert fird[6] == "qux"
  assert fird[7] == "bar"
  with pytest.raises(KeyError):
    fird[11]  # pylint: disable=pointless-statement
  with pytest.raises(KeyError):
    fird[0]  # pylint: disable=pointless-statement

def test_frozen_int_range_end_sentinel():
  """Test that we can build a range dict with end sentinel"""
  ranges = [
    (0, "foo"),
    (5, "qux"),
    (7, "bar"),
    10
  ]
  fird = FrozenIntRangeDict(ranges)
  assert len(fird) == 10

def test_inheritance_basic():
  """Test that basic ExplicitInheritance works"""
  # pylint: disable=no-self-use,unused-variable
  class Base(ExplicitInheritance):
    """Doc string"""
    def method(self):
      return None

def test_inheritance_abstract():
  """Test that @abstract works"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    """Doc string"""
    @abstract
    def method(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      def method(self):
        pass

  class DerivedAbstract(Base):
    """Doc string"""
    __abstract__ = True

  class Derived(Base):
    """Doc string"""
    @override
    def method(self):
      return None

def test_inheritance_abstract_explicit():
  """Test that @abstract works via __inherit__"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    """Doc string"""
    __inherit__ = dict(method=abstract)
    def method(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      def method(self):
        pass

  class DerivedAbstract(Base):
    """Doc string"""
    __abstract__ = True

  class Derived(Base):
    """Doc string"""
    __inherit__ = dict(method=override)
    def method(self):
      return None

def test_inheritance_abstract_2():
  """Test that we can chain explicit abstracts"""
  class _Base(ExplicitInheritance):
    @abstract
    def method(self):
      pass

  class _Derived1(_Base):
    __abstract__ = True

  with pytest.raises(InheritanceConstraintViolationError):
    class _Foo1(_Derived1):
      pass

  class _Derived2(_Base):
    __abstract__ = True

  with pytest.raises(InheritanceConstraintViolationError):
    class _Foo2(_Derived2):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class _Derived4(_Derived2):
      pass

  class _Derived4(_Derived2):
    @override
    def method(self):
      pass

def test_inheritance_explicit_conflict():
  """Test that we complain when __inherit__ conflicts with decorator"""
  # pylint: disable=function-redefined,unused-variable
  with pytest.raises(InheritanceConstraintViolationError):
    class Foo(ExplicitInheritance):
      __inherit__ = dict(method=abstract)
      @abstract
      def method(self):  # pylint: disable=no-self-use
        return 5

def test_brain_suck_abc():
  """Test that the special logic for sequence ABCs works"""
  class _TestSequence(ExplicitInheritance,
                      brain_suck_abc=collections.abc.Sequence):
    @override
    def __len__(self):
      return 3
    @override
    def __getitem__(self, key):
      return (4, 5, 6)[key]

  x = _TestSequence()
  assert list(x) == [4, 5, 6]
  assert x.count(5) == 1
  assert isinstance(x, collections.abc.Sequence)

  with pytest.raises(InheritanceConstraintViolationError):
    class _Foo(ExplicitInheritance, brain_suck_abc=collections.abc.Sequence):
      def __len__(self):
        return 3
      def __getitem__(self, key):
        return (4, 5, 6)[key]

  with pytest.raises(InheritanceConstraintViolationError):
    class _Foo(ExplicitInheritance, brain_suck_abc=collections.abc.Sequence):
      pass

  class _Foo(ExplicitInheritance, brain_suck_abc=collections.abc.Sequence):
    __abstract__ = True

  class _Bar(_Foo):
    @override
    def __len__(self):
      return 3
    @override
    def __getitem__(self, key):
      return (4, 5, 6)[key]
  assert _Bar().count(4) == 1

def test_inheritance_explicit_excess():
  """Test that we complain when __inherit__ conflicts with decorator"""
  # pylint: disable=unused-variable
  with pytest.raises(InheritanceConstraintViolationError):
    class Foo(ExplicitInheritance):
      __inherit__ = dict(method=abstract)

def test_inheritance_override_explicit():
  """Test that @override works via __inherit__"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    """Doc string"""
    def foo(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      def foo(self):
        pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      @override
      def bar(self):
        pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      foo = 5

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      __inherit__ = dict(bar=override)
      bar = 7

  class Derived(Base):
    __inherit__ = dict(foo=override)
    def foo(self):
      pass

def test_inheritance_override_property_explicit():
  """Test that @override works on property via __inherit__"""
  class Base(ExplicitInheritance):
    """Doc string"""
    foo = 5

  class Derived(Base):
    __inherit__ = dict(foo=override)
    foo = 6

  assert Derived.foo == 6

def test_inheritance_override():
  """Test that @override works"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    """Doc string"""
    def foo(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      """Doc string"""
      def foo(self):
        pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      @override
      def bar(self):
        pass

  class Derived(Base):
    @override
    def foo(self):
      pass

def test_inheritance_final():
  """Test that @final works"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    @final
    def foo(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      def foo(self):
        pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      @override
      def foo(self):
        pass

def test_inheritance_override_final():
  """Test that @override and @final work together"""
  # pylint: disable=function-redefined,unused-variable
  class Base(ExplicitInheritance):
    def foo(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      @final
      def foo(self):
        pass

  class Derived(Base):
    @override
    @final
    def foo(self):
      pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived2(Derived):
      @override
      def foo(self):
        pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived2(Derived):
      @final
      def foo(self):
        pass

def test_inheritance_class_final():
  """Test @final works when applied to class"""
  # pylint: disable=unused-variable

  @final
  class Base(ExplicitInheritance):
    pass

  with pytest.raises(InheritanceConstraintViolationError):
    class Derived(Base):
      pass

def test_argmin():
  seq = [3, 4, 2, 0, 8, 2]
  assert argmin(seq) == seq.index(min(seq))

def test_argmax():
  seq = [3, 4, 2, 0, 8, 2]
  assert argmax(seq) == seq.index(max(seq))

def test_common_prefix():
  from operator import eq
  seq1 = [1, 2, 3, 4]
  seq2 = [1, 2, 3]
  assert list(common_prefix(eq, seq1, seq2)) == seq2

def test_load_pandas():
  """Test that loading pandas with caching mechanism works"""
  m1 = load_pandas()
  m2 = load_pandas()
  assert m1 is m2

def test_timeline_to_eta():
  timeline = [
    {"a"},
    {"b"},
    {"c"},
  ]
  eta = timeline_to_eta(timeline)
  assert len(eta) == len(timeline)
  assert eta == [
    {"a": 0, "b": 1, "c": 2},
    {"b": 0, "c": 1},
    {"c": 0},
  ]

def test_timeline_to_eta_dups():
  timeline = [
    {"a"},
    {"b"},
    {"c"},
    {"b", "a"},
    {"c"},
  ]
  eta = timeline_to_eta(timeline)
  assert len(eta) == len(timeline)
  assert eta == [
    {"a": 0, "b": 1, "c": 2},
    {"a": 2, "b": 0, "c": 1},
    {"a": 1, "b": 1, "c": 0},
    {"a": 0, "b": 0, "c": 1},
    {"c": 0},
  ]

def test_fast_auto_number():
  class Blah(AutoNumberFast):
    FOO = ()
    BAR = ()

  assert Blah.FOO == 1
  assert Blah.BAR == 2
  assert not Blah.owns("qqqq")
  assert not Blah.owns(0)
  assert Blah.owns(1)
  assert not Blah.owns(10)

  # TODO(dancol): do we want to pervert isinstance this way?
  assert isinstance(1, Blah)
  assert not isinstance(10, Blah)

  # TODO(dancol): teach pylint about enum members

  assert Blah.__members__ == dict(FOO=1, BAR=2)  # pylint: disable=no-member
  assert list(Blah) == [1, 2]

  class Blah2(AutoNumberFast):
    __start__ = 5
    FOO = ()
    BAR = ()

  assert Blah2.FOO == 5
  assert Blah2.BAR == 6

def test_safe_auto_number():
  # pylint: disable=no-member
  class Blah(AutoNumberSafe):
    FOO = ()
    BAR = ()

  assert Blah.FOO.value == 1
  assert Blah.BAR.value == 2
  assert Blah.FOO is not 0  # pylint: disable=compare-to-zero,literal-comparison
  assert Blah.owns(Blah.FOO)

def test_passive_re():
  re_str = r"x1(?P<foo>foo)(?P<bar>bar)\(P<foo>dummy)"
  def _predicate(re_group_name):
    return re_group_name == "bar"
  assert passivize_re(re_str, _predicate) \
    == r"x1(?:foo)(?P<bar>bar)\(P<foo>dummy)"

def test_cached_property():
  bar = 0
  class _TestObject(object):
    @cached_property
    def foo(self):  # pylint: disable=no-self-use
      nonlocal bar
      bar += 1
      return 5
  foo = _TestObject()
  assert bar == 0  # pylint: disable=compare-to-zero
  assert foo.foo == 5
  assert bar == 1
  assert foo.foo == 5
  assert bar == 1
  del foo.foo
  assert foo.foo == 5
  assert bar == 2

def test_cached_property_immutable():
  class _TestImmutable(Immutable):
    foo = iattr(int)
    @cached_property
    def bar(self):
      return self.foo + 1
  assert _TestImmutable(1).bar == 2

def test_sorted_dict():
  sd = SortedDict()
  assert sd.keys() == ()
  sd[5] = -5
  assert sd.keys() == (5,)
  sd[1] = -1
  assert sd.keys() == (1, 5)
  sd[9] = -9
  assert list(sd.items()) == [(1, -1), (5, -5), (9, -9)]
  assert list(sd.items()) == [(1, -1), (5, -5), (9, -9)]
  assert sd.keys() is sd.keys()
  del sd[5]
  assert list(sd.values()) == [-1, -9]

def pict_parameterize(data):
  keys = list(data)
  out_data = [
    tuple(get(list(keys), case_dict))
    for case_dict in pypict.tools.from_dict(data)
  ]
  return pytest.mark.parametrize(",".join(keys), out_data)

def exhaustive_parameterize(data):
  def _mark(fn):
    for key, values in data.items():
      fn = pytest.mark.parametrize(key, values)(fn)
    return fn
  return _mark

def parameterize_dwim(**params):
  """Conditionally parametrize using PICT or Cartesian product"""
  import os
  if os.environ.get("DCTV_TEST_EXHAUSTIVE"):
    return exhaustive_parameterize(params)
  return pict_parameterize(params)

assertrepr_compare_hooks = []
