// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "thread_analysis.h"

#include <stdint.h>
#include <string.h>

#include <array>
#include <exception>
#include <istream>
#include <ostream>
#include <tuple>

#include <boost/spirit/home/x3.hpp>

#include "autoevent.h"
#include "automethod.h"
#include "container_util.h"
#include "fmt.h"
#include "hash_table.h"
#include "linux.h"
#include "map.h"
#include "meta_util.h"
#include "npyiter.h"
#include "optional.h"
#include "pyerr.h"
#include "pyerrfmt.h"
#include "pylog.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "query.h"
#include "result_buffer.h"
#include "set.h"
#include "simple_variant.h"
#include "span.h"
#include "string_table.h"
#include "stringstream.h"

namespace dctv {

// Code for analyzing threads in a process.  We use dedicated
// domain-specific C++ code, contrary to general DCTV philosophy,
// because process assignment is hairy and ambiguous due to process
// snapshots not being atomic.  It's also easier to do some kinds of
// sanity checking in iterative code.
//
// Interpreting snapshots is tricky: they're not atomic.
// Snapshots take a meaningful amount of time to produce, can
// interleave with ongoing process creation and destruction, and
// because we generate snapshots by walking /proc, internal
// inconsistencies can arise, e.g., a thread can be reported as
// belonging to multiple processes.
//
// We do our best to clean up this mess when we can do so without
// ambiguity.  We reject the trace in cases where we can't possibly
// interpret a snapshot correctly.  We'll see how often we reject
// traces due to snapshot ambiguity: if we're doing it often, that's
// an argument for adding atomic snapshot support to the kernel.
//
// Ambiguity comes from tid recycling.  If a thread with a
// particular tid dies while inside the snapshot and a new thread
// with this tid starts while we're still collecting the snapshot,
// we must abort the trace: we don't know whether any fact that the
// snapshot asserts about that tid refers to the old thread or the
// new thread by that name.  Likewise, if a snapshot asserts
// contradictory things about a thread, e.g., that it has two
// different tgids, we abort the trace, since we can't determine
// which assertion is true as of the end of the snapshot.
// These situations are rare, but we must account for them and test
// them if we're to have confidence in the correctness of the
// analysis system across long traces.
//
// If we see a subsequent snapshot, we use it to verify that we've
// correctly inferred the system process state, but we don't update
// our process table according to the information in the snapshot.
// We can't currently cope with event loss.
//
// N.B. the term "pid" is strongly overloaded.  Sometimes it means a
// thread ID; other times, it means a thread group (i.e. process) ID.
// Here, we try to use TID and TGID only.
//
// We use negative numbers to avoid confusing our synthetic IDs with
// raw IDs from the trace, which are always positive.  We start at
// -100 and decrement from there to avoid small negative numbers
// with special meaning for waitpid and friends, again, to
// avoid confusion.
//
// TODO(dancol): recover from buffer loss?
//
// TODO(dancol): get a timestamped process table snapshot operation
// built into the kernel

namespace {

using std::string_view;
using std::is_same_v;
using std::decay_t;
namespace x3 = boost::spirit::x3;
namespace h = hana;

#define S BOOST_HANA_STRING

static const IdValue system_utid_base = -100000;

enum class IdState {
  UNMAPPED,
  ALIVE,
  DEAD,
};

const char* id_state_name(IdState state);

enum class IdKind {
  RAW_TID,
  RAW_TGID,
  UNIQUE_TID,
  UNIQUE_TGID,
};

template<IdKind Kind>
constexpr const char* id_kind_name() noexcept;

template<IdKind Kind>
struct Id final : boost::totally_ordered<Id<Kind>> {
  Id(const Id& other) noexcept = default;
  Id(Id&& other) noexcept = default;
  Id& operator=(const Id& other) noexcept = default;
  Id& operator=(Id&& other) noexcept = default;
  bool operator<(const Id& other) const noexcept {
    return this->value < other.value;
  }
  bool operator==(const Id& other) const noexcept {
    return this->value == other.value;
  }
  IdValue get_value() const noexcept { return this->value; }
  static constexpr bool is_valid(IdValue value) noexcept {
    if constexpr (Kind == IdKind::RAW_TID ||
                  Kind == IdKind::RAW_TGID) {
      // We use negative "raw" values here for synthetic threads
      return value != 0;
    } else if constexpr (Kind == IdKind::UNIQUE_TID ||  // NOLINT
                         Kind == IdKind::UNIQUE_TGID) {
      return value < 0;
    } else {
      errhack<decltype(h::int_c<Kind>)>::bad();
    }
  }

  static Id from_trace_field(const char* source, IdValue value) {
    if (!Id::is_valid(value))
      throw_invalid_query_fmt("invalid %s in %s: %s",
                              id_kind_name<Kind>(), source, value);
    return Id(value);
  }

  static constexpr Id from_raw_value(IdValue value) noexcept {
    return Id(value);
  }

  template<typename P, typename D>
  friend auto handle_pyarg(Id*, P&& parse_arg, D&& default_) {
    constexpr bool has_default = CONSTEXPR_VALUE(default_ != h::false_c);
    IdValue value;
    if constexpr (has_default) {
      Id default_id = default_();
      value = default_id.value;
    }
    return parse_arg(
        h::make_string(pyarg_basic_conversions[h::type_c<IdValue>]),
        h::make_tuple(&value),
        [&] {
          if (!Id::is_valid(value))
            throw_invalid_query_fmt("invalid value for %s: %s",
                                    id_kind_name<Kind>(), value);
          return Id::from_raw_value(value);
        });
  }

 private:
  explicit constexpr Id(IdValue value) noexcept : value(value) {}
  IdValue value;
};

template<IdKind Kind>
std::ostream& operator<<(std::ostream& out, const Id<Kind>& id);
template<IdKind Kind>
std::ostream& operator<<(std::ostream& out, const optional<Id<Kind>>& id);

using RawTid = Id<IdKind::RAW_TID>;
using RawTgid = Id<IdKind::RAW_TGID>;
using UniqueTid = Id<IdKind::UNIQUE_TID>;
using UniqueTgid = Id<IdKind::UNIQUE_TGID>;

constexpr RawTid raw_init_tid = RawTid::from_raw_value(1);
constexpr RawTgid raw_init_tgid = RawTgid::from_raw_value(1);
constexpr RawTid raw_kthreadd_tid = RawTid::from_raw_value(2);

RawTid leader_id(RawTgid tgid);
RawTgid tg_from_leader(RawTid raw_tid);
bool rootp(RawTid raw_tid);

enum class ConsistentUpdateMode {
  WARN_ON_MISMATCH,
  FAIL_ON_MISMATCH,
};

struct Snapshot;

template<typename TheId, typename T, typename Value>
bool consistent_update(
    TheId the_id,
    const char* name,
    ConsistentUpdateMode mode,
    Snapshot* snapshot,
    optional<T>* field,
    const Value& proposed_value);

// Thrown when we see a thread exit while not known to be alive.
// We use a custom exception type so we can hackily catch it, scan the
// event queue, and potentially retry if the thread error is due to
// timestamp-quantization-driven event inversion.  This is kind of
// a hack.
struct DyingThreadNotAliveError : virtual PyException {

  void set_pyexception() const noexcept override;
  RawTid raw_tid;
  IdState found_state;

  DyingThreadNotAliveError(RawTid raw_tid, IdState found_state)
      : raw_tid(raw_tid),
        found_state(found_state)
  {}
};

void
DyingThreadNotAliveError::set_pyexception() const noexcept
{
  try {
    throw_invalid_query_fmt(
        "%s is dying: expected thread state is %s, "
        "but observed state is %s",
        this->raw_tid,
        id_state_name(IdState::ALIVE),
        id_state_name(this->found_state));
  } catch (...) {
    _set_pending_cxx_exception_as_pyexception();
  }
}

struct Snapshot final : BasePyObject,
                        SupportsWeakRefs,
                        HasPyCtor
{
  Snapshot();
  Snapshot(pyref args, pyref kwargs);

  // TODO(dancol): using optional for each of these fields is
  // inefficient.  All we need is one bit per field, and optional gives
  // us a whole word!

  DCTV_DEFINE_HANA_STRUCT(
      ThreadData,
      // Raw TID of creator: from creation event
      (optional<RawTid>, raw_creator_tid)
      // Scanned parent tgid: from /proc
      (optional<RawTgid>, raw_parent_tgid)
      // OOM score adjustment: from /proc?
      (optional<OomScore>, oom_score_adj)
      // Thread group identity
      (optional<RawTgid>, raw_tgid)
      // Thread name
      (optional<String>, comm)
      // Start time *OBSERVED FROM EVENT*
      (optional<TimeStamp>, start_time)
      // Death time *OBSERVED FROM EVENT*
      (optional<TimeStamp>, death_time)
      // Status from /proc sample.
      (optional<char>, state)
      // Memory samples.
      // TODO(dancol): make generic at runtime?!!?!?
      (optional<MemorySize>, rss_anon)
      (optional<MemorySize>, rss_file)
  );

  // The thread structure is a dumb flat record of everything that
  // might happen to a thread during a snapshot.  When we process a
  // snapshot-end event, we mine these records for information that we
  // import into the main thread state table.
  struct Thread final : ThreadData {
    struct ImportStatus {
      struct NotImported {
        bool operator==(const NotImported& /*other*/) const {
          return true;
        }
      };
      struct ImportInProgress {
        bool operator==(const ImportInProgress& /*other*/) const {
          return true;
        }
      };
      struct ElideBecauseProcRace {
        bool operator==(const ElideBecauseProcRace& /*other*/) const {
          return true;
        }
      };
      struct Imported {
        RawTgid tgid;
        bool operator==(const Imported& other) const {
          return this->tgid == other.tgid;
        }
      };
    };

    SimpleVariant<
      ImportStatus::NotImported,
      ImportStatus::ImportInProgress,
      ImportStatus::ElideBecauseProcRace,
      ImportStatus::Imported
      > import_status;

    void check_is_tg_leader(RawTid raw_tid);
    bool operator==(const Thread& other) const noexcept;
    bool operator!=(const Thread& other) const noexcept;

    void merge_from(RawTid raw_tid,
                    Snapshot* merge_into,
                    const Thread& other);
  };

  void note_thread_start(TimeStamp ts,
                         RawTid raw_tid,
                         optional<RawTid> creator_tid,
                         bool is_thread_group_leader);
  void note_thread_exit(TimeStamp ts, RawTid raw_tid);
  void note_scanned_oom_score_adj(
      RawTgid raw_tgid, OomScore oom_score_adj);
  void note_scanned_parent(RawTid raw_tid, RawTgid parent);
  void note_scanned_tgid(RawTid raw_tid, RawTgid tgid);
  void note_scanned_state(RawTid raw_tid, char state);
  void note_scanned_comm(RawTid raw_tid, string_view comm);
  void note_scanned_rss_anon(RawTid raw_tid, MemorySize bytes);
  void note_scanned_rss_file(RawTid raw_tid, MemorySize bytes);
  void parse_text_dctv_snapshot(string_view text_snapshot);
  void parse_tid_status(RawTid raw_tid, string_view status);
  void parse_tid_stat(RawTid raw_tid, string_view stat);
  void update_from_py(pyref py_data);

  unique_obj_pyref<Snapshot> copy() const;
  void merge_from(const Snapshot& other);
  String format() const;

  unique_pyref py_richcompare(pyref other, int op) const;

  using ThreadInfo = Map<RawTid, Thread>;
  optional<TimeStamp> start_time;
  ThreadInfo threads;
  Vector<String> warnings;

  static PyTypeObject pytype;
  static PyMethodDef pymethods[];

  void queue_warning(String warning) {
    this->warnings.push_back(std::move(warning));
  }

  inline bool operator==(const Snapshot& other) const noexcept;
  inline bool operator!=(const Snapshot& other) const noexcept;
};

// Lifecycle of a thread or process: finalized -> alive -> dead ->
// finalized A finalized object is one with no actual thread attached;
// an alive one is one that's running.  A dead thread has exited from
// a runtime perspective, but since its ID hasn't been reused, we
// still remember what it was for later remapping purposes.

struct OutThreadSpan {
  TimeStamp ts;
  TimeStamp duration;
  TimeStamp duration_live;
  RawTid raw_tid;
  UniqueTid unique_tid;
  UniqueTgid unique_tgid;
};

struct OutProcessSpan {
  TimeStamp ts;
  TimeStamp duration;
  TimeStamp duration_live;
  RawTgid raw_tgid;
  UniqueTgid unique_tgid;
  optional<UniqueTgid> unique_parent_tgid;
};

struct OutEmitSamples {
  Snapshot::Thread* s;
  TimeStamp sample_time;
  UniqueTid utid;
  UniqueTgid utgid;
};

template<typename Out>
struct TaskDb final {
  TaskDb(Out out,
         bool kernel_threads_as_threads,
         CpuNumber nr_cpus);

  void note_thread_start(TimeStamp ts,
                         RawTid raw_tid,
                         optional<RawTid> creator_tid,
                         bool is_thread_group_leader);
  void note_thread_exit(TimeStamp ts, RawTid raw_tid);
  void note_snapshot_start(TimeStamp ts,
                           unique_obj_pyref<Snapshot> snapshot);
  void note_snapshot_end(TimeStamp ts);
  void finalize(TimeStamp ts);

  template<typename... Args>
  void warning(Args&&... args);

  // PID_MAX_LIMIT =                                4194304;
  static constexpr IdValue generation_increment = -10000000;

 private:
  struct Thread final {
    // raw_tid is the key
    IdState get_state() const noexcept;
    UniqueTid get_unique_tid() const noexcept;
    RawTgid get_raw_tgid() const noexcept;
    TimeStamp get_start_time() const noexcept;
    TimeStamp get_death_time() const noexcept;
    void make_alive(TimeStamp ts,
                    UniqueTid unique_tid,
                    RawTgid raw_tgid);
    void make_dead(TimeStamp ts);
    void reset();
   private:
    struct Info final {
      TimeStamp start_time;
      UniqueTid unique_tid;
      RawTgid raw_tgid;
      optional<TimeStamp> death_time;
    };
    optional<Info> info;
  };

  struct Process final {
    // raw_tgid is the key
    IdState get_state() const noexcept;
    TimeStamp get_start_time() const noexcept;
    TimeStamp get_death_time() const noexcept;
    UniqueTgid get_unique_tgid() const noexcept;
    optional<UniqueTgid> get_unique_parent_tgid() const noexcept;
    IdValue get_live_thread_count() const noexcept;
    void increment_thread_count();
    void decrement_thread_count(TimeStamp ts);
    void make_alive(TimeStamp ts,
                    UniqueTgid unique_tgid,
                    optional<UniqueTgid> parent_unique_tgid);
    void reset();

   private:
    struct Info final {
      TimeStamp start_time;
      UniqueTgid unique_tgid;
      optional<UniqueTgid> parent;
      IdValue number_live_threads;
      optional<TimeStamp> death_time;
    };
    void check_consistency() const;
    optional<Info> info;
  };

  Thread* note_thread_start_1(TimeStamp ts,
                              RawTid raw_tid,
                              optional<RawTid> creator_tid,
                              bool is_thread_group_leader);
  UniqueTid allocate_unique_tid(RawTid wanted);
  void finalize_thread(TimeStamp ts,
                       RawTid raw_tid,
                       Thread* thread,
                       bool at_end_of_trace);
  void import_snapshot_thread_1(
      Snapshot* snapshot,
      RawTid raw_tid,
      Snapshot::Thread* snapshot_thread);
  void import_snapshot_thread(
      Snapshot* snapshot,
      RawTid raw_tid,
      Snapshot::Thread* snapshot_thread);
  void import_snapshot_thread_death(RawTid raw_tid,
                                    Snapshot::Thread* snapshot_thread);

  UniqueTid system_tid_for_core(CpuNumber cpu_number);

  Map<RawTid, Thread> threads;
  Map<RawTgid, Process> processes;
  unique_obj_pyref<Snapshot> pending_snapshot;
  bool first_snapshot = true;
  Set<UniqueTid> allocated_unique_tids;
  IdValue next_unique_tid_value = -100;
  Out out;

  // If true, represent kernel threads as threads of kthreadd instead
  // of independent "processes" even though they all appear to be
  // thread group leaders in /proc.
  bool kernel_threads_as_threads;

  // We model tasks done outside process context as synthetic "system"
  // threads, one per core, all belonging to a synthetic system
  // "process".  This hack requires that we know the number of cores
  // on the system.
  CpuNumber nr_cpus;
};

template<typename IS>
const IS*
sst_is(const Snapshot::Thread* snapshot_thread)
{
  return snapshot_thread->import_status.template get_if<IS>();
}

template<IdKind Kind>
struct errhack_id_kind;

template<IdKind Kind>
constexpr
const char*
id_kind_name() noexcept
{
  if constexpr (Kind == IdKind::RAW_TID) {
    return "RawTid";
  } else if constexpr(Kind == IdKind::RAW_TGID) {  // NOLINT
    return "RawTgid";
  } else if constexpr(Kind == IdKind::UNIQUE_TID) {  // NOLINT
    return "UniqueTid";
  } else if constexpr(Kind == IdKind::UNIQUE_TGID) {  // NOLINT
    return "UniqueTigd";
  } else {  // NOLINT
    typename errhack_id_kind<Kind>::bad();
  }
}

template<IdKind Kind>
std::ostream&
operator<<(std::ostream& out, const Id<Kind>& id)
{
  out << optional<Id<Kind>>(id);
  return out;
}

template<IdKind Kind>
std::ostream&
operator<<(std::ostream& out, const optional<Id<Kind>>& id)
{
  out << id_kind_name<Kind>() << "(";
  if (id)
    out << id->get_value();
  else
    out << "None";
  out << ")";
  return out;
}

template<typename TheId, typename T, typename Value>
bool
consistent_update(TheId the_id,
                  const char* name,
                  ConsistentUpdateMode mode,
                  Snapshot* snapshot,
                  optional<T>* field,
                  const Value& proposed_value)
{
  if (field->has_value()) {
    if (**field != proposed_value) {
      bool is_warn = mode == ConsistentUpdateMode::WARN_ON_MISMATCH;
      String msg = fmt(
          (is_warn
           ? "mismatch on field %1% of %2%: %3% vs %4%: choosing %3%"
           : "mismatch on field %1% of %2%: %3% vs %4%"),
          name, the_id,
          repr(fmt("%s", **field)),
          repr(fmt("%s", proposed_value)));
      if (is_warn) {
        if (snapshot)
          snapshot->queue_warning(std::move(msg));
        else
          throw_invalid_query(std::move(msg));
      } else if (mode == ConsistentUpdateMode::FAIL_ON_MISMATCH) {
        throw_invalid_query(std::move(msg));
      } else {
        assume(false);
        abort();
      }
    }
    return false;
  }
  *field = proposed_value;
  return true;
}

const char*
id_state_name(IdState state)
{
  switch (state) {
    case IdState::ALIVE:
      return "alive";
    case IdState::DEAD:
      return "dead";
    case IdState::UNMAPPED:
      return "unmapped";
    default:
      assert(false);
      abort();
  }
}

template<typename Out>
IdState
TaskDb<Out>::Thread::get_state() const noexcept
{
  if (!this->info)
    return IdState::UNMAPPED;
  return this->info->death_time
      ? IdState::DEAD
      : IdState::ALIVE;
}

template<typename Out>
UniqueTid
TaskDb<Out>::Thread::get_unique_tid() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->unique_tid;
}

template<typename Out>
RawTgid
TaskDb<Out>::Thread::get_raw_tgid() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->raw_tgid;
}

template<typename Out>
TimeStamp
TaskDb<Out>::Thread::get_start_time() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->start_time;
}

template<typename Out>
TimeStamp
TaskDb<Out>::Thread::get_death_time() const noexcept
{
  assume(this->get_state() == IdState::DEAD);
  assume(this->info->death_time);
  return *this->info->death_time;
}

template<typename Out>
void
TaskDb<Out>::Thread::make_alive(
    TimeStamp ts,
    UniqueTid unique_tid,
    RawTgid raw_tgid)
{
  assume(this->get_state() == IdState::UNMAPPED);
  assume(!this->info);
  this->info = Info { ts, unique_tid, raw_tgid };
}

template<typename Out>
void
TaskDb<Out>::Thread::make_dead(TimeStamp ts)
{
  assume(this->get_state() == IdState::ALIVE);
  assume(!this->info->death_time);
  if (ts < this->get_start_time())
    throw_invalid_query_fmt(
        "thread death time %s before start time %s",
        ts, this->get_start_time());
  this->info->death_time = ts;
}

template<typename Out>
void
TaskDb<Out>::Thread::reset()
{
  assume(this->get_state() != IdState::ALIVE);
  this->info.reset();
}

template<typename Out>
void
TaskDb<Out>::Process::check_consistency() const
{
  IdState state = this->get_state();
  if (state == IdState::ALIVE) {
    assume(this->info->number_live_threads > 0);
    assume(!this->info->death_time);
  } else if (state == IdState::DEAD) {
    assume(this->info->number_live_threads == 0);
    assume(this->info->death_time);
    assume(this->info->start_time <= this->info->death_time);
  } else {
    assume(state == IdState::UNMAPPED);
    assume(!this->info);
  }
}

template<typename Out>
TimeStamp
TaskDb<Out>::Process::get_start_time() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->start_time;
}

template<typename Out>
TimeStamp
TaskDb<Out>::Process::get_death_time() const noexcept
{
  assume(this->get_state() == IdState::DEAD);
  assume(this->info->death_time);
  return *this->info->death_time;
}

template<typename Out>
UniqueTgid
TaskDb<Out>::Process::get_unique_tgid() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->unique_tgid;
}

template<typename Out>
optional<UniqueTgid>
TaskDb<Out>::Process::get_unique_parent_tgid() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->parent;
}

template<typename Out>
IdValue
TaskDb<Out>::Process::get_live_thread_count() const noexcept
{
  assume(this->get_state() != IdState::UNMAPPED);
  return this->info->number_live_threads;
}

template<typename Out>
void
TaskDb<Out>::Process::increment_thread_count()
{
  assume(this->get_state() == IdState::ALIVE);
  assume(this->info->number_live_threads > 0);
  if (this->info->number_live_threads ==
      std::numeric_limits<IdValue>::max())
    throw_invalid_query_fmt("too many live threads in process");
  this->info->number_live_threads += 1;
  this->check_consistency();
}

template<typename Out>
void
TaskDb<Out>::Process::decrement_thread_count(TimeStamp ts)
{
  assume(this->get_state() == IdState::ALIVE);
  assume(this->info->number_live_threads > 0);
  assume(!this->info->death_time);
  this->info->number_live_threads -= 1;
  if (!this->info->number_live_threads)
    this->info->death_time = ts;
  this->check_consistency();
}

template<typename Out>
void
TaskDb<Out>::Process::make_alive(TimeStamp ts,
                                 UniqueTgid unique_tgid,
                                 optional<UniqueTgid> parent_unique_tgid)
{
  assume(this->get_state() == IdState::UNMAPPED);
  this->info = Info {
    ts,
    unique_tgid,
    parent_unique_tgid,
    /*number_live_threads=*/1,
  };
  assume(this->get_state() == IdState::ALIVE);
  this->check_consistency();
}

template<typename Out>
void
TaskDb<Out>::Process::reset()
{
  assume(this->get_state() == IdState::DEAD);
  this->info.reset();
}

template<typename Out>
IdState
TaskDb<Out>::Process::get_state() const noexcept
{
  if (!this->info)
    return IdState::UNMAPPED;
  return this->info->death_time
      ? IdState::DEAD
      : IdState::ALIVE;
}

template<typename Out>
TaskDb<Out>::TaskDb(Out out,
                    bool kernel_threads_as_threads,
                    CpuNumber nr_cpus)
    : out(std::move(out)),
      kernel_threads_as_threads(kernel_threads_as_threads),
      nr_cpus(nr_cpus)
{
  assume(this->nr_cpus >= 0);
  for (CpuNumber cpu_number = 0;
       cpu_number < this->nr_cpus;
       ++cpu_number) {
    auto [it, inserted] = this->allocated_unique_tids.insert(
        this->system_tid_for_core(cpu_number));
    assume(inserted);
  }
}

template<typename Out>
UniqueTid
TaskDb<Out>::system_tid_for_core(CpuNumber cpu_number)
{
  assume(0 <= cpu_number && cpu_number < this->nr_cpus);
  return UniqueTid::from_raw_value(
      system_utid_base - cpu_number);
}

template<typename Out>
UniqueTid
TaskDb<Out>::allocate_unique_tid(RawTid wanted)
{
  if (wanted.get_value() < 0) {
    // This is a preallocated synthetic TID.
    UniqueTid out = UniqueTid::from_raw_value(wanted.get_value());
    assert(map_contains(this->allocated_unique_tids, out));
    return out;
  }

  // The general allocation strategy is that the utid for a given tid
  // X is -X if -X hasn't been used already.  If -X has been used
  // already, we try -10000000 - X, -20000000 - X, and so on until we
  // either find an used utid or exhaust the space of utid numbers.
  // If _that_ doesn't work, we just try small negative numbers until
  // we find an unused slot, and failing that, we just die.

  assume(wanted.get_value() > 0);
  if (wanted.get_value() > PID_MAX_LIMIT)
    throw_invalid_query_fmt(
        "unexpectedly huge TID found: PID_MAX_LIMIT is %s, but we "
        "saw a TID with value %s", PID_MAX_LIMIT, wanted.get_value());
  static_assert(sizeof (IdValue) >= sizeof (IdValue));
  static_assert(-generation_increment > PID_MAX_LIMIT,
                "generation increment should generate values "
                "outside the range of valid TID/TGIDs");
  UniqueTid proposed = UniqueTid::from_raw_value(-wanted.get_value());
  const IdValue floor = std::numeric_limits<IdValue>::min() + 1;
  static_assert(-floor >= PID_MAX_LIMIT);
  assume(this->generation_increment < 0);
  for (;;) {
    auto [it, inserted] = this->allocated_unique_tids.insert(proposed);
    if (inserted)
      return proposed;
    IdValue gap = -floor - -proposed.get_value();
    assume(gap >= 0);
    if (gap < -this->generation_increment)
      break;
    proposed = UniqueTid::from_raw_value(
        proposed.get_value() + this->generation_increment);
  }

  for (;;) {
    if (this->next_unique_tid_value <= floor)
      throw_invalid_query_fmt("out of unique tid space");
    proposed = UniqueTid::from_raw_value(this->next_unique_tid_value--);
    auto [it, inserted] = this->allocated_unique_tids.insert(proposed);
    if (inserted)
      return proposed;
  }
  assume(false /*not reached */);
  abort();
}

Snapshot::Snapshot()  // NOLINT
{}

Snapshot::Snapshot(pyref args, pyref kwargs)
    : Snapshot()
{
  PARSEPYARGS()(args, kwargs);
}

void
Snapshot::Thread::check_is_tg_leader(RawTid raw_tid)
{
  if (this->raw_tgid && leader_id(*this->raw_tgid) != raw_tid)
    throw_invalid_query_fmt("%s is supposed to be a tg leader but is "
                            "known not to be", raw_tid);
}

bool
Snapshot::Thread::operator==(const Thread& other) const noexcept
{
  return h::equal(static_cast<const ThreadData&>(*this),
                     static_cast<const ThreadData&>(other));
}

bool
Snapshot::Thread::operator!=(const Thread& other) const noexcept
{
  return !(*this == other);
}

void
Snapshot::note_thread_start(
    TimeStamp ts,
    RawTid raw_tid,
    optional<RawTid> raw_creator_tid,
    bool is_thread_group_leader)
{
  // The snapshot may or may not already contain an entry for this
  // tid depending on /proc enumeration order.  Just make sure that
  // if we already have an entry, the data match.
  Thread* thread = &this->threads[raw_tid];
  if (thread->start_time)
    throw_invalid_query_fmt("%s started twice?!", raw_tid);
  if (thread->raw_creator_tid)
    throw_invalid_query_fmt("%s has creator already?!?!", raw_tid);
  if (thread->raw_tgid) {
    bool already_tg_leader =
        thread->raw_tgid->get_value() == raw_tid.get_value();
    if (is_thread_group_leader != already_tg_leader)
      throw_invalid_query_fmt(
          "disagreement about thread group leadership status of tid %s "
          "in snapshot", raw_tid);
  }
  thread->start_time = ts;
  thread->raw_creator_tid = raw_creator_tid;
  if (is_thread_group_leader)
    thread->raw_tgid = tg_from_leader(raw_tid);
}

void
Snapshot::note_thread_exit(TimeStamp ts, RawTid raw_tid)
{
  Thread* thread = &this->threads[raw_tid];
  if (thread->death_time.has_value())
    throw_invalid_query_fmt("%s already dead", raw_tid);
  thread->death_time = ts;
}

void
Snapshot::note_scanned_oom_score_adj(RawTgid raw_tgid,
                                          OomScore oom_score_adj)
{
  Thread* thread = &this->threads[leader_id(raw_tgid)];
  if (consistent_update(raw_tgid, "oom_score_adj",
                        ConsistentUpdateMode::WARN_ON_MISMATCH,
                        this, &thread->oom_score_adj, oom_score_adj))
    thread->check_is_tg_leader(leader_id(raw_tgid));
}

void
Snapshot::note_scanned_comm(RawTid raw_tid, string_view comm)
{
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "comm",
                    ConsistentUpdateMode::WARN_ON_MISMATCH,
                    this, &thread->comm, comm);
}

void
Snapshot::note_scanned_rss_anon(RawTid raw_tid, MemorySize bytes)
{
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "rss_anon",
                    ConsistentUpdateMode::WARN_ON_MISMATCH,
                    this, &thread->rss_anon, bytes);
}

void
Snapshot::note_scanned_rss_file(RawTid raw_tid, MemorySize bytes)
{
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "rss_file",
                    ConsistentUpdateMode::WARN_ON_MISMATCH,
                    this, &thread->rss_file, bytes);
}

void
Snapshot::note_scanned_parent(RawTid raw_tid, RawTgid parent)
{
  // A thread's parent can change over the course of a snapshot if its
  // parent dies, forcing the kernel to reparent the child of the
  // now-deceased parent to init.
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "parent",
                    ConsistentUpdateMode::WARN_ON_MISMATCH,
                    this, &thread->raw_parent_tgid, parent);
}

void
Snapshot::note_scanned_tgid(RawTid raw_tid, RawTgid raw_tgid)
{
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "tgid",
                    ConsistentUpdateMode::FAIL_ON_MISMATCH,
                    this, &thread->raw_tgid, raw_tgid);
}

void
Snapshot::note_scanned_state(RawTid raw_tid, char state)
{
  // The "idle" state is used by certain kernel threads to wait for
  // things without being counted toward the system load average.
  // For our purposes, just consider these threads as being asleep.
  // See
  // https://github.com/torvalds/linux/commit/80ed87c8a9ca0cad7ca66cf3bbdfb17559a66dcf
  // Likewise, consider a parked thread ot be sleeping.
  if (state == 'I' || state == 'P')
    state = 'S';
  if (!seq_contains(procfs_thread_states, state))
    throw_invalid_query_fmt("unknown thread state %s", repr(
        string_view(&state, 1)));
  Thread* thread = &this->threads[raw_tid];
  consistent_update(raw_tid, "state",
                    ConsistentUpdateMode::WARN_ON_MISMATCH,
                    this, &thread->state, state);
}

void
Snapshot::update_from_py(pyref py_data)
{
  if (!isinstance(py_data, &PyUnicode_Type))
    throw_pyerr_fmt(PyExc_TypeError,
                    "only string snapshots supported currently: found %s",
                    repr(dctv::pytype(py_data)));
  this->parse_text_dctv_snapshot(pystr_to_string_view(py_data));
}

// Comm and previous fields are hand-parsed and so aren't included in
// the struct.
BOOST_FUSION_DEFINE_STRUCT_INLINE(  // NOLINT
    ProcStatTail,
    (char,      state)
    (int32_t,   ppid)
    (int32_t,   pgrp)
    (int32_t,   session)
    (int32_t,   tty_nr)
    (int32_t,   tpgid)
    (uint32_t,  flags)
    (uint64_t,  minflt)
    (uint64_t,  cminflt)
    (uint64_t,  majflt)
    (uint64_t,  cmajflt)
    (uint64_t,  utime)
    (uint64_t,  stime)
    (uint64_t,  cutime)
    (uint64_t,  cstime)
    (int64_t,   priority)
    (int64_t,   nice)
    (int64_t,   num_threads)
    (int64_t,   itrealvalue)
    (uint64_t,  starttime)
    (uint64_t,  vsize)
    (int64_t,   rss)
    (uint64_t,  rsslim)
    (uint64_t,  startcode)
    (uint64_t,  endcode)
    (uint64_t,  startstack)
    (uint64_t,  kstkesp)
    (uint64_t,  kstkeip)
    (uint64_t,  signal)
    (uint64_t,  blocked)
    (uint64_t,  sigignore)
    (uint64_t,  sigcatch)
    (uint64_t,  wchan)
    (uint64_t,  nswap)
    (uint64_t,  cnswap)
    (int32_t,   exit_signal)
    (int32_t,   processor)
    (uint32_t,  rt_priority)
    (uint32_t,  policy)
    (uint64_t,  delayacct_blkio_ticks)
    (uint64_t,  guest_time)
    (int64_t,   cguest_time)
    (uint64_t,  start_data)
    (uint64_t,  end_data)
    (uint64_t,  start_brk)
    (uint64_t,  arg_start)
    (uint64_t,  arg_end)
    (uint64_t,  env_start)
    (uint64_t,  env_end)
    (int32_t,   exit_code)
);

auto primitive_parser_map = h::make_map(
    h::make_pair(h::type_c<char>, x3::char_),
    h::make_pair(h::type_c<int8_t>, x3::int8),
    h::make_pair(h::type_c<uint8_t>, x3::uint8),
    h::make_pair(h::type_c<int16_t>, x3::int16),
    h::make_pair(h::type_c<uint16_t>, x3::uint16),
    h::make_pair(h::type_c<int32_t>, x3::int32),
    h::make_pair(h::type_c<uint32_t>, x3::uint32),
    h::make_pair(h::type_c<int64_t>, x3::int64),
    h::make_pair(h::type_c<uint64_t>, x3::uint64)
);

namespace x3_stuff {

using x3::_attr;
using x3::ascii::blank;
using x3::attr;
using x3::char_;
using x3::eoi;
using x3::lexeme;
using x3::lit;
using x3::lit;
using x3::rule;
using x3::space;

// Escaped string under snapshot.h rules
x3::rule<class escaped_string, String> const escaped_string;
auto const escaped_string_part =
    (char_ - char_("\"\\\n")) |
    ("\\n" > attr('\n')) |
    ("\\\"" > attr('"')) |
    ("\\\\" > attr('\\'));
auto const escaped_string_def =
    lexeme['"' > *(escaped_string_part) > '"'];

// Escaped string under /proc/pid/status rules
x3::rule<class status_escaped_string, String> const status_escaped_string;
auto const status_escaped_string_def = *(
      (char_ - char_("\\\n")) |
      ("\\n" > attr('\n')) |
      ("\\\\" > attr('\\')));

x3::rule<class proc_stat_tail, ProcStatTail> const proc_stat_tail;

auto eow = &(space|eoi);

auto kw = [](auto token) {
  return lexeme[token >> eow];
};

template<typename T>
auto
make_struct_parser(T&& types)
{
  return h::fold(
      h::transform(types,
                   h::partial(h::at_key,
                              primitive_parser_map)),
      [](auto&& left, auto&& right) {
        return left > right;
      });
}

auto const proc_stat_tail_def =
    make_struct_parser(
        struct_field_types_from_fusion<ProcStatTail>) > eoi;

x3::rule<class unknown_status_line_field_name, String>
const unknown_status_line_field_name;

auto const unknown_status_line_field_name_def =
    +(char_ - char_(':'));

BOOST_SPIRIT_DEFINE(escaped_string ,
                    status_escaped_string,
                    proc_stat_tail);

}  // namespace x3_stuff

// Utility function that parses, throws on returned error, and
// rethrows exception failure as a query failure.
template<typename Iterator, typename... Args>
void
x3_phrase_parse(const char* name,
                const Iterator& begin,
                const Iterator& end,
                Args&&... args)
{
  bool result;
  Iterator pos = begin;
  const char* what = "parse";
  try {
    result = x3::phrase_parse(pos, end, std::forward<Args>(args)...);
  } catch (const x3::expectation_failure<Iterator>& ex) {
    result = false;
    pos = ex.where();
    what = "expectation";
  }
  if (!result)
    throw_invalid_query_fmt(
        "invalid %s: %s failed at %s",
        name, what,
        repr(string_view(&*pos, end - pos).substr(0, 64)));
}

DCTV_NO_INLINE_FOR_PROFILER
void
Snapshot::parse_tid_stat(RawTid raw_tid, string_view stat)
{
  using namespace x3_stuff;

  // The comm of a thread in Linux is a string of up to 16 arbitrary
  // non-NUL bytes, even ')'.  In procfs stat, we emit the comm
  // verbatim, with no escaping, surrounded by '(' and ')'.  We need
  // to find the *last* ')' in the stat buffer and regard that as the
  // end of the comm field.  For simplicity, we do that by hand and
  // just use the x3 parser for the past after comm.

  string_view::size_type comm_open_pos = stat.find('(');
  string_view::size_type comm_close_pos = stat.rfind(')');
  if (comm_open_pos == string_view::npos ||
      comm_close_pos == string_view::npos ||
      comm_open_pos >= comm_close_pos)
    throw_invalid_query_fmt("invalid task stat: %s", repr(stat));

  auto comm = stat.substr(comm_open_pos + 1,
                          comm_close_pos - comm_open_pos - 1);
  note_scanned_comm(raw_tid, comm);
  auto stat_tail = stat.substr(comm_close_pos + 1);
  ProcStatTail parsed_stat_tail;
  x3_phrase_parse("stat",
                  stat_tail.begin(),
                  stat_tail.end(),
                  proc_stat_tail,
                  blank, parsed_stat_tail);

  this->note_scanned_state(
      raw_tid,
      parsed_stat_tail.state);

  if (parsed_stat_tail.ppid)
    this->note_scanned_parent(
        raw_tid,
        RawTgid::from_trace_field("proc stat",
                                  parsed_stat_tail.ppid));
}

DCTV_NO_INLINE_FOR_PROFILER
void
Snapshot::parse_tid_status(RawTid raw_tid, string_view status)
{
  using namespace x3_stuff;

  auto sl = [](const char* name) {
    return lit(name) > ':';
  };

  auto handle_name = [&](auto&& context) {
    this->note_scanned_comm(raw_tid, _attr(context));
  };

  auto handle_state = [&](auto&& context) {
    this->note_scanned_state(raw_tid, _attr(context));
  };

  auto handle_tgid = [&](auto&& context) {
    IdValue value = _attr(context);
    this->note_scanned_tgid(
        raw_tid,
        RawTgid::from_trace_field("proc status", value));
  };

  auto handle_ppid = [&](auto&& context) {
    if (_attr(context))
      this->note_scanned_parent(
          raw_tid,
          RawTgid::from_trace_field("proc status",
                                    _attr(context)));
  };

  auto handle_rss_anon = [&](auto&& context) {
    this->note_scanned_rss_anon(
        raw_tid, _attr(context) * 1024);
  };

  auto handle_rss_file = [&](auto&& context) {
    this->note_scanned_rss_file(
        raw_tid, _attr(context) * 1024);
  };

  auto handle_unknown_field = [&](auto&& context) {};

  // Case-sensitive.  Note the capitalization.
  auto status_line =
      ((sl("Name")
        // Note the careful space management here to avoid eating
        // leading whitespace, which is actually part of comm!
        > x3::no_skip[blank]
        > x3::no_skip[status_escaped_string[handle_name]]) |
       (sl("State")
        > char_[handle_state]
        > lexeme[lit('(') > *(~lit('\n'))]) |
       (sl("Tgid")
        > x3::int32[handle_tgid]) |
       (sl("PPid")
        > x3::int32[handle_ppid]) |
       (sl("RssAnon")
        > x3::int64[handle_rss_anon]
        > kw("kB")) |
       (sl("RssFile")
        > x3::int64[handle_rss_file]
        > kw("kB")) |
       ((unknown_status_line_field_name_def[handle_unknown_field])
        > ':' > *(~lit('\n')))
      ) > ('\n' | eoi);
  auto grammar = *status_line > eoi;
  x3_phrase_parse("status", status.begin(), status.end(),
                  grammar, blank);
}

DCTV_NO_INLINE_FOR_PROFILER
void
Snapshot::parse_text_dctv_snapshot(string_view text_snapshot)
{
  using namespace x3_stuff;

  using boost::fusion::get;

  auto do_tgid_oom_score_adj = [&](auto&& ctx) {
    this->note_scanned_oom_score_adj(
        RawTgid::from_trace_field(
            "tgid_oom_score_adj.tgid",
            get<0>(_attr(ctx))),
        OomScore(get<1>(_attr(ctx))));
  };

  auto do_tid_stat = [&](auto& ctx) {
    RawTid tid = RawTid::from_trace_field(
        "tid_stat",
        get<0>(_attr(ctx)));
    string_view stat_text = get<1>(_attr(ctx));
    this->parse_tid_stat(tid, stat_text);
  };

  auto do_tid_status = [&](auto&& ctx) {
    RawTid tid = RawTid::from_trace_field(
        "tid_status",
        get<0>(_attr(ctx)));
    string_view status_text = get<1>(_attr(ctx));
    this->parse_tid_status(tid, status_text);
  };

  auto do_tid_tgid = [&](auto&& ctx) {
    this->note_scanned_tgid(
        RawTid::from_trace_field(
            "tid_tgid.tid",
            get<0>(_attr(ctx))),
        RawTgid::from_trace_field(
            "tid_tgid.tgid",
            get<1>(_attr(ctx))));
  };

  auto snapshot_directive = (
      ((kw("tgid_oom_score_adj") > x3::int32 > x3::int32)[
          do_tgid_oom_score_adj]) |
      ((kw("tid_status") > x3::int32 > escaped_string)[do_tid_status]) |
      ((kw("tid_stat") > x3::int32 > escaped_string)[do_tid_stat]) |
      ((kw("tid_tgid") > (x3::int32 > x3::int32))[do_tid_tgid])  // NOLINT
  ) > (eoi | '\n');
  auto blank_line = (*blank >> '\n');
  auto grammar = *(snapshot_directive | blank_line) > eoi;
  x3_phrase_parse("dctv snapshot",
                  text_snapshot.begin(),
                  text_snapshot.end(),
                  grammar, blank);
}

template<typename Out>
typename TaskDb<Out>::Thread*
TaskDb<Out>::note_thread_start_1(
    TimeStamp ts,
    RawTid raw_tid,
    optional<RawTid> raw_creator_tid,
    bool is_thread_group_leader)
{
  assume(!this->pending_snapshot);
  bool is_root = rootp(raw_tid);
  Thread* thread = &this->threads[raw_tid];
  Thread* creator_thread = nullptr;
  if (is_root) {
    if (raw_creator_tid)
      throw_invalid_query_fmt(
          "%s must have no creator: we found %s",
          raw_tid, *raw_creator_tid);
  } else {
    if (!raw_creator_tid)
      throw_invalid_query_fmt(
          "tid %s has no creator, but is not a root thread", raw_tid);
    // Check only for unmapped, not dead, because a thread group
    // leader may die and remain a zombie until the whole group exits,
    // and in some cases, we regard the tgid as the "creator" of a
    // thread despite being a zombie.
    creator_thread = map_try_get(this->threads, *raw_creator_tid);
    if (!creator_thread ||
        creator_thread->get_state() == IdState::UNMAPPED) {
      throw_invalid_query_fmt(
          "%s is being created by %s but creator is unmapped",
          raw_tid, *raw_creator_tid);
    } else {
      if (creator_thread->get_raw_tgid() ==
          tg_from_leader(raw_kthreadd_tid) &&
          this->kernel_threads_as_threads)
        is_thread_group_leader = false;  // Fake.
    }
  }
  assume((creator_thread == nullptr) == is_root);
  this->finalize_thread(ts, raw_tid, thread,
                        /*at_end_of_trace=*/false);
  assume(thread->get_state() == IdState::UNMAPPED);
  UniqueTid unique_tid = this->allocate_unique_tid(raw_tid);
  thread->make_alive(ts,
                     unique_tid,
                     (is_thread_group_leader
                      ? tg_from_leader(raw_tid)
                      : creator_thread->get_raw_tgid()));
  assume(thread->get_state() == IdState::ALIVE);
  RawTgid raw_tgid = thread->get_raw_tgid();
  Process* process = &this->processes[raw_tgid];
  if (is_thread_group_leader) {
    // The process object should have been unmapped as a side effect
    // of unmapping the tid.
    if (process->get_state() != IdState::UNMAPPED)
      throw_invalid_query_fmt(
          "tg-leader %s is supposed to be forming a new "
          "thread group %s: expected tgid state is %s "
          "but actual state is %s",
          raw_tid, raw_tgid,
          id_state_name(IdState::UNMAPPED),
          id_state_name(process->get_state()));
    // Init and kthreadd have no parents.
    optional<UniqueTgid> parent_unique_tgid;
    if (!is_root) {
      RawTgid creator_raw_tgid = creator_thread->get_raw_tgid();
      // Use map_try_get to avoid iterator invalidation.
      Process* creator_process = map_try_get(this->processes,
                                             creator_raw_tgid);
      if (!creator_process ||
          creator_process->get_state() != IdState::ALIVE)
        throw_invalid_query_fmt(
            "tg-leader %s is being created by %s of %s "
            "but the creating thread group isn't running!",
            raw_tid, raw_creator_tid, creator_raw_tgid);
      parent_unique_tgid = creator_process->get_unique_tgid();
    }
    process->make_alive(
        ts,
        UniqueTgid::from_raw_value(unique_tid.get_value()),
        parent_unique_tgid);
    assume(process->get_state() == IdState::ALIVE);
    assume(process->get_live_thread_count() == 1);
  } else {
    if (is_root)
      throw_invalid_query_fmt(
          "root thread must always be a thread group leader");
    if (process->get_state() != IdState::ALIVE)
      throw_invalid_query_fmt(
          "non-tg-leader %s is supposed to belong to %s "
          "but the thread group is not alive",
          raw_tid, raw_tgid);
    process->increment_thread_count();
  }
  return thread;
}

template<typename Out>
void
TaskDb<Out>::note_thread_start(
    TimeStamp ts,
    RawTid raw_tid,
    optional<RawTid> raw_creator_tid,
    bool is_thread_group_leader)
{
  if (this->pending_snapshot)
    this->pending_snapshot->note_thread_start(
        ts,
        raw_tid,
        raw_creator_tid,
        is_thread_group_leader);
  else
    this->note_thread_start_1(ts,
                              raw_tid,
                              raw_creator_tid,
                              is_thread_group_leader);
}

template<typename Out>
void
TaskDb<Out>::note_thread_exit(TimeStamp ts, RawTid raw_tid)
{
  if (this->pending_snapshot) {
    this->pending_snapshot->note_thread_exit(ts, raw_tid);
    return;
  }
  Thread* thread = &this->threads[raw_tid];
  if (thread->get_state() != IdState::ALIVE)
    throw DyingThreadNotAliveError(raw_tid, thread->get_state());
  thread->make_dead(ts);
  assume(thread->get_state() == IdState::DEAD);
  RawTgid raw_tgid = thread->get_raw_tgid();
  Process* process = &this->processes[raw_tgid];
  if (process->get_state() != IdState::ALIVE)
    throw_invalid_query_fmt(
        "%s in %s is dying: expect state is %s, but "
        "observed state is %s",
        raw_tid, raw_tgid,
        id_state_name(IdState::ALIVE),
        id_state_name(process->get_state()));
  process->decrement_thread_count(ts);
}

template<typename Out>
void
TaskDb<Out>::finalize_thread(
    TimeStamp ts,
    RawTid raw_tid,
    Thread* thread,
    bool at_end_of_trace)
{
  // Emit thread (and maybe process) information because we're either
  // about to either reuse an ID or finish thread analysis.
  // Finalization happens as long as possible after death so that we
  // can map tids and tgids logged after process death but
  // before reuse.
  IdState thread_state = thread->get_state();
  if (thread_state != IdState::UNMAPPED) {
    if (thread_state == IdState::ALIVE)
      throw_invalid_query_fmt(
          "%s being finalized while still alive?!",
          raw_tid);
    assume(thread_state == IdState::DEAD);
    Process* process = &this->processes[thread->get_raw_tgid()];
    if (process->get_state() == IdState::UNMAPPED)
      throw_invalid_query_fmt(
          "finalizing %s in %s but the tgid is unmapped!",
          raw_tid, thread->get_raw_tgid());
    TimeStamp start_time = thread->get_start_time();
    assume(ts >= start_time);
    if (ts == start_time)
      if (rootp(raw_tid) || !at_end_of_trace)
        throw_invalid_query_fmt(
            "refusing to emit zero-duration lifetime for %s",
            raw_tid);
    this->out(OutThreadSpan {
        start_time,
        /*duration=*/ts - start_time,
        /*live_duration=*/thread->get_death_time() - start_time,
        raw_tid,
        thread->get_unique_tid(),
        process->get_unique_tgid(),
      });
    const bool is_thread_group_leader =
        tg_from_leader(raw_tid) == thread->get_raw_tgid();
    if (is_thread_group_leader) {
      if (process->get_state() != IdState::DEAD)
        throw_invalid_query_fmt(
            "finalizing tg leader %s belongs to %s; "
            "tgid state should be %s but is %s",
            raw_tid,
            thread->get_raw_tgid(),
            id_state_name(IdState::DEAD),
            id_state_name(process->get_state()));
      assume(process->get_start_time() == start_time);
      assume(ts >= start_time);
      if (!at_end_of_trace && ts == start_time)
        throw_invalid_query_fmt(
            "refusing to emit zero-duration lifetime for %s",
            thread->get_raw_tgid());
      this->out(OutProcessSpan {
          start_time,
          /*duration=*/ts - start_time,
          /*live_duration=*/process->get_death_time() - start_time,
          thread->get_raw_tgid(),
          process->get_unique_tgid(),
          process->get_unique_parent_tgid(),
        });
      process->reset();
    }
    thread->reset();
  }
  assume(thread->get_state() == IdState::UNMAPPED);
}

template<typename Out>
void
TaskDb<Out>::finalize(TimeStamp ts)
{
  if (this->pending_snapshot) {
    pylog.warning("Ending trace with active snapshot?!");
    this->note_snapshot_end(ts);
  }
  assume(!this->pending_snapshot);

  // Okay to iterate over the map because note_thread_exit never
  // changes the content of the map, instead only manipulating
  // existing entries.
  for (auto& [raw_tid, thread] : this->threads) {
    if (thread.get_state() == IdState::ALIVE) {
      this->note_thread_exit(ts, raw_tid);
      assume(thread.get_state() == IdState::DEAD);
    }
  }

  // Finalize the threads in two passes: do the
  // non-thread-group-leader processes, then do the thread group
  // leaders.  This way, we preserve the invariant that all
  // non-tg-leader threads are finalized before the tg-leader.

  for (auto& [raw_tid, thread] : this->threads) {
    assume(thread.get_state() == IdState::DEAD ||
           thread.get_state() == IdState::UNMAPPED);
    if (thread.get_state() == IdState::DEAD &&
        leader_id(thread.get_raw_tgid()) != raw_tid)
      this->finalize_thread(ts, raw_tid, &thread,
                            /*at_end_of_trace=*/true);
  }

  for (auto& [raw_tid, thread] : this->threads) {
    assume(thread.get_state() == IdState::DEAD ||
           thread.get_state() == IdState::UNMAPPED);
    this->finalize_thread(ts, raw_tid, &thread,
                          /*at_end_of_trace=*/true);
  }

  for (auto& [raw_tid, thread] : this->threads)
    assume(thread.get_state() == IdState::UNMAPPED);
  for (auto& [raw_tgid, process] : this->processes)
    assume(process.get_state() == IdState::UNMAPPED);
}

template<typename Out>
template<typename... Args>
void
TaskDb<Out>::warning(Args&&... args)
{
  pywarn(
      find_pyclass("dctv.query", "QueryWarning"),
      fmt(AUTOFWD(args)...).c_str(),
      /*stack_level=*/1);
}

template<typename Out>
void
TaskDb<Out>::note_snapshot_start(
    TimeStamp ts,
    unique_obj_pyref<Snapshot> snapshot)
{
  if (this->pending_snapshot)
    throw_invalid_query("snapshot already started");
  assume(!snapshot->start_time.has_value());
  snapshot->start_time = ts;
  this->pending_snapshot = std::move(snapshot);
}

template<typename Out>
void
TaskDb<Out>::import_snapshot_thread_1(
    Snapshot* snapshot,
    RawTid raw_tid,
    Snapshot::Thread* snapshot_thread)
{
  using IS = Snapshot::Thread::ImportStatus;
  assume(sst_is<IS::ImportInProgress>(snapshot_thread));

  const bool is_thread_group_leader = snapshot_thread->raw_tgid &&
      snapshot_thread->raw_tgid->get_value() == raw_tid.get_value();
  const bool is_root = rootp(raw_tid);
  if (is_root && !is_thread_group_leader)
    throw_invalid_query_fmt(
        "root thread %s must always be a thread group leader", raw_tid);
  if (is_root && snapshot_thread->raw_parent_tgid)
    throw_invalid_query_fmt(
        "root thread must not have a parent: saw %s",
        raw_tid, *snapshot_thread->raw_parent_tgid);

  // Make sure we import any ancestors or thread group leaders of this
  // thread first so we maintain invariants for the addition
  // functions.  These functions may invalidate Thread iterators
  // and pointers!

  struct ReferencedThread final {
    RawTid raw_tid;
    Snapshot::Thread* thread;
    IS::Imported imported;
  };

  struct ReferencedProcess final {
    RawTgid raw_tgid;
    RawTid raw_leader_tid;
    Snapshot::Thread* leader_thread;
    IS::Imported imported;
  };

  optional<RawTid> bad_reference_tid;

  auto dereference_snapshot_tid = [&](optional<RawTid> raw_tid)
      -> optional<ReferencedThread> {
    if (!raw_tid)
      return {};
    Snapshot::Thread* t = map_try_get(snapshot->threads, *raw_tid);
    if (t) {
      this->import_snapshot_thread(snapshot, *raw_tid, t);
      if (sst_is<IS::ElideBecauseProcRace>(t))
        t = nullptr;
    }
    if (!t) {
      bad_reference_tid = raw_tid;
      snapshot_thread->import_status = IS::ElideBecauseProcRace{};
      return {};
    }
    assume(sst_is<IS::Imported>(t));
    return ReferencedThread {
      *raw_tid,
      t,
      *sst_is<IS::Imported>(t)
    };
  };

  auto dereference_snapshot_tgid = [&](optional<RawTgid> raw_tgid)
      -> optional<ReferencedProcess> {
    if (!raw_tgid)
      return {};
    RawTid raw_leader_tid = leader_id(*raw_tgid);
    optional<ReferencedThread> rt =
      dereference_snapshot_tid(raw_leader_tid);
    if (!rt)
      return {};
    return ReferencedProcess {
      *raw_tgid,
      raw_leader_tid,
      rt->thread,
      rt->imported,
    };
  };

  const char raw_thread_state = snapshot_thread->state.value_or(' ');
  const bool in_dead_state =
      raw_thread_state == 'X' ||
      raw_thread_state == 'Z';

  optional<ReferencedThread> creator =
      dereference_snapshot_tid(snapshot_thread->raw_creator_tid);
  if (!creator &&
      snapshot_thread->raw_creator_tid &&
      sst_is<IS::ElideBecauseProcRace>(snapshot_thread) &&
      !snapshot_thread->death_time &&
      is_thread_group_leader &&
      !in_dead_state)
  {
    // We observed this thread being created by a thread that exited
    // while we were creating the snapshot.  Log a warning, then
    // pretend that we were created by init.
    warning(
        "%1% was created by %2%, but %2% has incomplete snapshot "
        "information (perhaps because it exited during the snapshot) "
        "and %1% outlives the snapshot: "
        "pretending that %1% was created by init",
        raw_tid, *snapshot_thread->raw_creator_tid);
    snapshot_thread->import_status = IS::ImportInProgress{};
    creator = dereference_snapshot_tid(raw_init_tid);
  }

  optional<ReferencedProcess> thread_group;
  if (!is_thread_group_leader) {
    // In the is_thread_group_leader case, the thread we're importing
    // is in its own thread group, so trying to import
    // snapshot_thread->raw_tgid here would trip the circular
    // reference detector.
    thread_group = dereference_snapshot_tgid(snapshot_thread->raw_tgid);

    // We don't need special reparenting logic for the thread group
    // reference because we can't suffer from the proc race here: a tg
    // leader lives as a zombie as long as any of its threads
    // is alive.
  }

  // Don't dereference the parent if we have a creator, since the
  // parent may differ from the creator if the creator dies after the
  // creation but before we scan the child, and when that happens, the
  // scan observes the child having been reparented to init,
  // creating inconsistency.

  optional<ReferencedProcess> parent;
  if (is_thread_group_leader && !creator) {
    parent = dereference_snapshot_tgid(snapshot_thread->raw_parent_tgid);
    if (!parent &&
        sst_is<IS::ElideBecauseProcRace>(snapshot_thread) &&
        snapshot_thread->raw_parent_tgid &&
        !snapshot_thread->death_time &&
        !in_dead_state)
    {
      // Similar to the creator case above, we internally reparent to
      // init when the parent suffers from the proc race.
      warning(
          "%1% was scanned as having parent %2%, but %2% has "
          "incomplete snapshot information (perhaps because it "
          "exited during the snapshot) and %1% outlives the "
          "snapshot.  Pretending that the parent of %1% is "
          "init.", raw_tid, snapshot_thread->raw_parent_tgid);
      snapshot_thread->import_status = IS::ImportInProgress{};
      parent = dereference_snapshot_tgid(raw_init_tgid);
    }
  }

  Thread* const thread = &this->threads[raw_tid];

  if (in_dead_state) {
    // A /proc scan reported this thread as being dead but we didn't
    // see an exit event for it.  Either we missed an exit event or
    // the thread was dead when we started the snapshot.
    if (!snapshot_thread->death_time && snapshot_thread->start_time)
      throw_invalid_query_fmt(
          "%s was scanned as being in dead state %s "
          "but we observed it being born during the trace "
          "and didn't see an exit event: we should have "
          "seen the thread die, not just scanned it as "
          "dead afterwards.  Did we lose an event?",
          raw_tid,
          *snapshot_thread->state);
    // Similarly, if the thread was marked as alive in our
    // pre-snapshot process table, we shouldn't have scanned it as
    // dead without also seeing an exit event.
    if (!snapshot_thread->death_time &&
        thread->get_state() == IdState::ALIVE)
      throw_invalid_query_fmt(
          "%s was scanned as being in dead state %s "
          "but we regarded it was alive pre-snapshot and "
          "didn't see an exit event for the thread.  Did we "
          "lose an event?",
          raw_tid,
          *snapshot_thread->state);
  }

  if (!snapshot_thread->start_time &&
      thread->get_state() != IdState::ALIVE) {
    // Ordinarily, we don't allow for discovering new processes via
    // proc walks that we didn't also observe through events.
    if (!this->first_snapshot)
      throw_invalid_query_fmt(
          "%s was listed in the snapshot, did not start"
          "in the snapshot, and wasn't present in the pre-snapshot "
          "process table either.  Why didn't we see this process "
          "earlier?  Did we lose an event?",
          raw_tid);
    // Before we process the first snapshot, we don't have a
    // pre-existing process table, so we have to just consider
    // discovered processes whose creation we don't observe to have
    // started at the beginning of the trace.  (Although... we *could*
    // look /proc/pid/stat's starttime field, I guess?  The clock
    // wouldn't be synced to the trace clock though.)
    assume(snapshot->start_time);
    snapshot_thread->start_time = *snapshot->start_time;
    if (in_dead_state && !snapshot_thread->death_time) {
      // Pretend the thread died as soon as it was born.
      snapshot_thread->death_time = snapshot_thread->start_time;
    }
  }

  if (snapshot_thread->start_time &&
      thread->get_state() == IdState::ALIVE)
    throw_invalid_query_fmt(
        "%s started in snapshot but was alive pre-snapshot: "
        "lost an event?", raw_tid);

  if (in_dead_state && !snapshot_thread->death_time)
    throw_invalid_query_fmt(
        "%s scanned as dead but we don't have a death time!",
        raw_tid);

  if (sst_is<IS::ElideBecauseProcRace>(snapshot_thread)) {
    if (!snapshot_thread->death_time)
      throw_invalid_query_fmt(
          "%1% depends on a thread with incomplete snapshot "
          "information %2%, but %1% itself outlives the snapshot: "
          "aborting snapshot import", raw_tid, bad_reference_tid);
    warning("%1% depends on a thread with incomplete "
            "snapshot information %2%, but because %1% "
            "is known die inside the snapshot "
            "pretending that %1% never happened",
            raw_tid, bad_reference_tid);
    return;
  }

  if (!snapshot_thread->start_time) {
    // If this were the first snapshot, we'd have faked a start time
    // by now, so if we get here, we know we're looking at a
    // subsequent snapshot.  Validate basic facts about the process
    // and continue.
    if (snapshot_thread->raw_tgid &&
        snapshot_thread->raw_tgid != thread->get_raw_tgid())
      throw_invalid_query_fmt(
          "%s in incremental snapshot has tgid %s "
          "but in taskdb it has tgid %s",
          raw_tid,
          *snapshot_thread->raw_tgid,
          thread->get_raw_tgid());
    snapshot_thread->import_status =
        IS::Imported{thread->get_raw_tgid()};
    return;
  }

  if (snapshot_thread->start_time &&
      snapshot_thread->death_time &&
      *snapshot_thread->start_time > *snapshot_thread->death_time)
    throw_invalid_query_fmt(
        "%1% died before it was created: tid reuse in snapshot?",
        raw_tid);

  // Process started in the snapshot (or we're pretending it did), so
  // re-play the start here.
  optional<RawTgid> expected_tgid;
  optional<RawTgid> expected_parent_tgid;

  auto set_expected_tgid = [&](RawTgid raw_tgid) {
    consistent_update(raw_tid, "expected_tgid",
                      ConsistentUpdateMode::FAIL_ON_MISMATCH,
                      snapshot, &expected_tgid, raw_tgid);
  };

  auto set_expected_parent_tgid = [&](RawTgid raw_tgid) {
    if (!is_thread_group_leader)
      throw_invalid_query_fmt("expecting parent of non-tg-leader");
    if (is_root)
      throw_invalid_query_fmt("root %s must not have a parent", raw_tid);
    consistent_update(raw_tid, "expected_parent_tgid",
                      ConsistentUpdateMode::WARN_ON_MISMATCH,
                      snapshot, &expected_parent_tgid, raw_tgid);
  };

  // Fold information from other referenced threads into our
  // expectation of our new thread's identity.

  if (is_thread_group_leader)
    set_expected_tgid(tg_from_leader(raw_tid));

  if (creator) {
    // If we happen to have an explicit creator (because we saw a
    // process creation during the snapshot) we can still collect the
    // thread properly even if the /proc walker loses the race with
    // process exit.
    if (!is_thread_group_leader)
      set_expected_tgid(creator->imported.tgid);
    if (is_thread_group_leader)
      set_expected_parent_tgid(creator->imported.tgid);
  }

  if (thread_group)
    set_expected_tgid(thread_group->raw_tgid);

  if (parent)
    set_expected_parent_tgid(parent->raw_tgid);

  // Now figure out the creator TID for the purpose of simulating
  // process creation.  If we have a creator TID, we can just use it
  // directly.  Otherwise, we use a thread group as the creator ---
  // which might be a lie, but that shouldn't produce any observable
  // incorrect output, since we don't record parentage of
  // non-thread-group-leader threads.

  optional<RawTid> replay_creator;
  if (creator)
    replay_creator = creator->raw_tid;
  else if (thread_group)
    replay_creator = thread_group->raw_leader_tid;
  else if (parent)
    replay_creator = parent->raw_leader_tid;

  if (!replay_creator && !is_root) {
    // We're missing essential structural information about this
    // process.  If we're processing our first snapshot and the process
    // died while we're collecting this snapshot, the /proc walker may
    // have failed to describe it because it ran after the process
    // exited.  In this case, we can just pretend the process
    // doesn't exist.
    const bool may_elide_because_proc_race =
        this->first_snapshot && snapshot_thread->death_time;
    if (!may_elide_because_proc_race)
      throw_invalid_query_fmt(
          "no creator tid information available for tid %s: "
          "missing event?",
          raw_tid);
    warning("%1% has incomplete snapshot information "
            "but dies inside the snapshot, so pretending "
            "that %1% never happened", raw_tid);
    snapshot_thread->import_status =
        IS::ElideBecauseProcRace {};
    return;
  }

  if (is_root && replay_creator)
    throw_invalid_query_fmt(
        "root %s must not have a creator", raw_tid);

  assume(snapshot_thread->start_time);

  Thread* new_thread =
      this->note_thread_start_1(*snapshot_thread->start_time,
                                raw_tid,
                                replay_creator,
                                is_thread_group_leader);
  assume(new_thread);

  if (safe_mode) {
    assume(new_thread->get_state() == IdState::ALIVE);
    bool munged_kthread = this->kernel_threads_as_threads &&
        new_thread->get_raw_tgid() == tg_from_leader(raw_kthreadd_tid);
    if (expected_tgid)
      assume(*expected_tgid == new_thread->get_raw_tgid() ||
             munged_kthread);
    Process* new_process =
        map_try_get(this->processes, new_thread->get_raw_tgid());
    assume(new_process);
    assume(new_process->get_state() == IdState::ALIVE);
    if (expected_parent_tgid && !munged_kthread) {
      assume(!is_root);
      Process* parent_process =
          map_try_get(this->processes, *expected_parent_tgid);
      assume(parent_process);
      assume(parent_process->get_unique_tgid() ==
             new_process->get_unique_parent_tgid());
    }
  }

  snapshot_thread->import_status =
      IS::Imported{new_thread->get_raw_tgid()};
}

template<typename Out>
void
TaskDb<Out>::import_snapshot_thread(
    Snapshot* snapshot,
    RawTid raw_tid,
    Snapshot::Thread* snapshot_thread)
{
  using IS = Snapshot::Thread::ImportStatus;

  // Catch circular references from bogus traces.
  if (sst_is<IS::ImportInProgress>(snapshot_thread))
    throw_invalid_query_fmt("circular snapshot reference involving %s",
                            raw_tid);

  // Make import idempotent.
  if (sst_is<IS::NotImported>(snapshot_thread)) {
    snapshot_thread->import_status = IS::ImportInProgress{};
    this->import_snapshot_thread_1(snapshot, raw_tid, snapshot_thread);
    assume (!sst_is<IS::NotImported>(snapshot_thread));
    assume (!sst_is<IS::ImportInProgress>(snapshot_thread));
  }
}

template<typename Out>
void
TaskDb<Out>::import_snapshot_thread_death(
    RawTid raw_tid,
    Snapshot::Thread* snapshot_thread)
{
  using IS = Snapshot::Thread::ImportStatus;
  assume (!sst_is<IS::NotImported>(snapshot_thread));
  assume (!sst_is<IS::ImportInProgress>(snapshot_thread));
  if (sst_is<IS::ElideBecauseProcRace>(snapshot_thread))
    return;
  assume (sst_is<IS::Imported>(snapshot_thread));

  if (snapshot_thread->death_time) {
    // We reject traces with TID reuse during a snapshot window, so we
    // should never see a death followed by a birth of the same TID.
    if (snapshot_thread->start_time)
      assume(*snapshot_thread->start_time <= *snapshot_thread->death_time);
    this->note_thread_exit(*snapshot_thread->death_time,
                           raw_tid);
  }
}

template<typename Out>
void
TaskDb<Out>::note_snapshot_end(TimeStamp ts)
{
  if (!this->pending_snapshot)
    throw_invalid_query("snapshot end without snapshot start");
  unique_obj_pyref<Snapshot> snapshot = std::move(this->pending_snapshot);
  this->pending_snapshot.reset();
  if (!map_contains(snapshot->threads, raw_init_tid))
    throw_invalid_query_fmt(
        "incomplete snapshot: missing %s, i.e., init", raw_init_tid);

  // Add synthetic idle threads
  if (this->nr_cpus && this->first_snapshot) {
    auto raw_tid_for_core = [&](CpuNumber cpu_number) -> RawTid {
      assume(0 <= cpu_number && cpu_number < this->nr_cpus);
      return RawTid::from_raw_value(
          system_utid_base - cpu_number);
    };
    for (CpuNumber cpu_number = 0;
         cpu_number < this->nr_cpus;
         ++cpu_number) {
      RawTid raw_core_tid = raw_tid_for_core(cpu_number);
      assert(!map_contains(snapshot->threads, raw_core_tid));
      Snapshot::Thread* core_thread =
          &snapshot->threads[raw_core_tid];
      core_thread->comm = fmt("CPU#%s", cpu_number);
      core_thread->state = 'S';
      core_thread->raw_tgid =
          tg_from_leader(raw_tid_for_core(0));
    }
  }

  for (auto& [raw_tid, snapshot_thread] : snapshot->threads)
    this->import_snapshot_thread(snapshot.get(),
                                 raw_tid,
                                 &snapshot_thread);
  for (auto& [raw_tid, snapshot_thread] : snapshot->threads)
    this->import_snapshot_thread_death(raw_tid, &snapshot_thread);

  for (auto& [raw_tid, snapshot_thread] : snapshot->threads) {
    Thread* t = map_try_get(this->threads, raw_tid);
    if (!t || t->get_state() == IdState::UNMAPPED)
      return;
    Process* p = map_try_get(this->processes, t->get_raw_tgid());
    assume(p);
    TimeStamp sample_time = ts;
    if (snapshot_thread.death_time) {
      sample_time = *snapshot_thread.death_time;
      assume(sample_time <= ts);
    }
    this->out(OutEmitSamples{
        &snapshot_thread,
        sample_time,
        t->get_unique_tid(),
        p->get_unique_tgid()});
  }

  for (const String& warning : snapshot->warnings)
    this->warning("%s", warning);

  this->first_snapshot = false;
}

RawTid
leader_id(RawTgid tgid)
{
  return RawTid::from_raw_value(tgid.get_value());
}

RawTgid
tg_from_leader(RawTid raw_tid)
{
  return RawTgid::from_raw_value(raw_tid.get_value());
}

bool
rootp(RawTid raw_tid)
{
  return raw_tid == raw_init_tid ||
      raw_tid == raw_kthreadd_tid ||
      raw_tid.get_value() == system_utid_base;
}

template<typename T>
T
read_scalar(pyref py_iter, const char* name)
{
  PythonChunkIterator<T> iter(py_iter.addref());
  if (iter.is_at_eof())
    throw_invalid_query_fmt("%s produced no rows", name);
  auto [result] = iter.get();
  if (iter.advance())
    throw_invalid_query_fmt("%s must provide exactly one row", name);
  return result;
}

template<typename... Types>
constexpr auto make_sampler = []
    (auto&& fn, unique_pyref cb, QueryExecution* qe)
{
  using Fn = decay_t<decltype(fn)>;
  using Rb = ResultBuffer<Types...>;
  struct Sampler {
    Fn fn;
    Rb out;
  };
  return Sampler { AUTOFWD(fn), Rb(std::move(cb), qe) };
};

constexpr auto comm_sampler = h::partial(
    make_sampler<
    /*ts*/   TimeStamp,
    /*utid*/ IdValue,
    /*comm*/ StringTable::id_type>,
    [](auto&& out, const OutEmitSamples& oes, StringTable* st) {
      if (oes.s->comm)
        out.add(oes.sample_time,
                oes.utid.get_value(),
                st->intern(*oes.s->comm));
    });

constexpr auto oom_score_adj_sampler = h::partial(
    make_sampler<
    /*ts*/            TimeStamp,
    /*utgid*/         IdValue,
    /*oom_score_adj*/ OomScore>,
    [](auto&& out, const OutEmitSamples& oes, StringTable*) {
      if (oes.s->oom_score_adj)
        out.add(oes.sample_time,
                oes.utgid.get_value(),
                *oes.s->oom_score_adj);
    });

constexpr auto rss_anon_sampler = h::partial(
    make_sampler<
    /*ts*/       TimeStamp,
    /*utgid*/    IdValue,
    /*rss_anon*/ MemorySize>,
    [](auto&& out, const OutEmitSamples& oes, StringTable*) {
      if (oes.s->rss_anon)
        out.add(oes.sample_time,
                oes.utgid.get_value(),
                *oes.s->rss_anon);
    });

constexpr auto rss_file_sampler = h::partial(
    make_sampler<
    /*ts*/       TimeStamp,
    /*utgid*/    IdValue,
    /*rss_file*/ MemorySize>,
    [](auto&& out, const OutEmitSamples& oes, StringTable*) {
      if (oes.s->rss_file)
        out.add(oes.sample_time,
                oes.utgid.get_value(),
                *oes.s->rss_file);
    });

static
RawThreadStateFlags
make_fake_thread_state(char c)
{
  if (c == 'R')
    return LINUX_TASK_RUNNING /* == 0 */;
  if (c == 'S')
    return LINUX_TASK_INTERRUPTIBLE;
  if (c == 'D')
    return LINUX_TASK_UNINTERRUPTIBLE;
  if (c == 'T')
    return LINUX_TASK_STOPPED;
  if (c == 't')
    return LINUX_TASK_TRACED;
  if (c == 'X')
    return LINUX_EXIT_DEAD;
  if (c == 'Z')
    return LINUX_EXIT_ZOMBIE;
  if (c == 'I')
    return LINUX_TASK_INTERRUPTIBLE;  // Pretend it's a normal sleep
  // Unlike in parsing ftrace sched_switch, the allowed letter options
  // are limited to the ones above.
  throw_invalid_query_fmt("unrecognized process state '%c'", c);
}

constexpr auto tid_state_sampler = h::partial(
    make_sampler<
    /*ts*/    TimeStamp,
    /*utid*/  IdValue,
    /*state*/ RawThreadStateFlags>,
    [](auto&& out, const OutEmitSamples& oes, StringTable*) {
      if (oes.s->state)
        out.add(oes.sample_time,
                oes.utid.get_value(),
                make_fake_thread_state(*oes.s->state));
    });

// Keep list consistent with the one in thread_analysis.py.
constexpr auto sampler_info = h::make_tuple(
    comm_sampler,
    oom_score_adj_sampler,
    rss_anon_sampler,
    rss_file_sampler,
    tid_state_sampler
);

unique_pyref
analyze_threads(PyObject*, pyref py_args, pyref py_kwargs)
{
  namespace ae = auto_event;
  auto args = PARSEPYARGS_V(
      (pyref, sched_process_exit)
      (pyref, task_newtask)
      (pyref, snapshot_start)
      (pyref, snapshot_end)
      (pyref, first_ts)
      (pyref, last_ts)
      (pyref, get_snapshot_data)
      (pyref, tid_mapping_out)
      (pyref, tgid_mapping_out)
      (pyref, sampler_outs)
      (QueryExecution*, qe)
      (bool, kernel_threads_as_threads)
      (pyref, nr_cpus)
  )(py_args, py_kwargs);

  // TODO(dancol): coalesce multiple read_scalar calls.

  TimeStamp first_ts = read_scalar<TimeStamp>(
      args.first_ts, "first_ts");

  CpuNumber nr_cpus;
  if (args.nr_cpus == Py_None)
    nr_cpus = 0;
  else
    nr_cpus = read_scalar<CpuNumber>(args.nr_cpus, "nr_cpus");
  if (nr_cpus < 0)
    throw_invalid_query_fmt(
        "cannot have negative cores", nr_cpus);

  ResultBuffer<
    TimeStamp /* ts */,
    TimeStamp /* duration */,
    TimeStamp /* live_duration */,
    IdValue /* raw_tid=partition */,
    IdValue /* utid */,
    IdValue /* utgid */
    > out_tid_mapping(args.tid_mapping_out.addref(), args.qe);

  ResultBuffer<
    TimeStamp /* ts */,
    TimeStamp /* duration */,
    TimeStamp /* live_duration */,
    IdValue /* raw_tgid=partition */,
    IdValue /* utgid */,
    IdValue /* parent_utgid */
    > out_tgid_mapping(args.tgid_mapping_out.addref(), args.qe);

  size_t nsamplers =
      static_cast<size_t>(pyseq_size(args.sampler_outs));
  if (nsamplers != h::size(sampler_info))
    throw_invalid_query_fmt(
        "sampler list size mismatch: wanted %s, but got %s",
        static_cast<size_t>(h::size(sampler_info)),
        nsamplers);
  auto samplers = h::fold_left(
      sampler_info,
      h::make_tuple(),
      [&](auto&& state, auto&& info) {
        size_t idx = h::size(state);
        unique_pyref cb = get_item(args.sampler_outs, idx);
        return h::append(AUTOFWD(state), info(std::move(cb), args.qe));
      });

  auto emit = [&](const auto& row) {
    using Row = decay_t<decltype(row)>;
    if constexpr (is_same_v<Row, OutThreadSpan>) {
      assume(row.duration_live >= 0);
      assume(row.duration_live <= row.duration);
      if (!row.duration)
        return;
      out_tid_mapping.add(
          row.ts,
          row.duration,
          row.duration_live,
          row.raw_tid.get_value(),
          row.unique_tid.get_value(),
          row.unique_tgid.get_value());
    } else if constexpr (is_same_v<Row, OutProcessSpan>) {
      assume(row.duration_live >= 0);
      assume(row.duration_live <= row.duration);
      if (!row.duration)
        return;
      out_tgid_mapping.add(
          row.ts,
          row.duration,
          row.duration_live,
          row.raw_tgid.get_value(),
          row.unique_tgid.get_value(),
          (row.unique_parent_tgid.has_value()
           ? row.unique_parent_tgid->get_value()
           : 0 /* TODO(dancol): mask */));
    } else if constexpr (is_same_v<Row, OutEmitSamples>) {
      h::for_each(samplers, [&](auto&& sampler) {
        sampler.fn(sampler.out, row, st_from_qe(args.qe));
      });
    } else {
      errhack<Row>::unknown_row_type();
    }
  };

  auto emit_process_span = [&](
      TimeStamp ts,
      TimeStamp duration,
      TimeStamp duration_live,
      RawTgid raw_tgid,
      UniqueTgid unique_tgid,
      optional<UniqueTgid> unique_parent_tgid) {
  };

  TaskDb task_db(emit,
                 args.kernel_threads_as_threads,
                 nr_cpus);

  auto get_py_snapshot = [&](SnapshotNumber id) {
    return call(args.get_snapshot_data, make_pylong(id))
        .addref_as<Snapshot>();
  };

  AUTOEVENT_DECLARE_EVENT(
      SchedProcessExit,
      (IdValue, exiting_tid)
  );

  AUTOEVENT_DECLARE_EVENT(
      TaskNewTask,
      (IdValue, creating_tid)
      (IdValue, new_tid)
      (CloneFlags, clone_flags)
  );

  auto handle_sched_process_exit = [&](
      auto&& ae,
      TimeStamp ts,
      const SchedProcessExit& event) {
    auto do_it = [&] {
      task_db.note_thread_exit(
          ts,
          RawTid::from_trace_field(
              "sched_process_exit.exiting_tid",
              event.exiting_tid));
    };

    try {
      do_it();
    } catch (const DyingThreadNotAliveError& ex) {
      // Scan the pending create events for the failed tid at the
      // current timestamp --- we've already eagerly slurped all of
      // them.  If we can find one, process it early, then retry the
      // thread exit processing.  This situation happens rarely, so
      // doing it by exception instead of plumbing it through the
      // normal control flow path should be fine.

      // TODO(dancol): maybe a custom stateful sort function would be
      // less hacky and more general.
      if (ex.raw_tid.get_value() != event.exiting_tid)
        throw;
      bool dispatched = ae::dispatch_from_pending_events(
          ae,
          [&](TimeStamp ev_ts, const auto& raw_payload) {
            using RawPayload = decay_t<decltype(raw_payload)>;
            // The actual payload can be an internal derived class!
            if constexpr (std::is_base_of_v<TaskNewTask, RawPayload>) {
              if (ev_ts == ts &&
                  raw_payload.new_tid == event.exiting_tid) {
                return true;
              }
            }
            return false;
          });
      if (!dispatched)
        throw;
      do_it();
    }
  };

  auto handle_task_newtask = [&](
      auto&& ae,
      TimeStamp ts,
      const TaskNewTask& event) {
    task_db.note_thread_start(
        ts,
        RawTid::from_trace_field("task_newtask.new_tid",
                                 event.new_tid),
        RawTid::from_trace_field("task_newtask.creating_tid",
                                 event.creating_tid),
        !(event.clone_flags & CLONE_THREAD));
  };

  AUTOEVENT_DECLARE_EVENT(
      SnapshotStart,
      (SnapshotNumber, snapshot_number)
  );

  // Create an implicit "floating" snapshot at the start of trace
  // processing so we can absorb random events before we get the
  // message about the actual first snapshot start.

  unique_obj_pyref<Snapshot> floating_snapshot =
      make_pyobj<Snapshot>();
  task_db.note_snapshot_start(first_ts,
                              floating_snapshot.addref());

  auto handle_snapshot_start = [&](
      auto&& ae,
      TimeStamp ts,
      const SnapshotStart& event) {
    auto snapshot = get_py_snapshot(event.snapshot_number);
    if (floating_snapshot) {
      floating_snapshot->merge_from(*snapshot);
      floating_snapshot.reset();
    } else {
      task_db.note_snapshot_start(ts, std::move(snapshot));
    }
  };

  AUTOEVENT_DECLARE_EVENT(
      SnapshotEnd,
  );

  auto handle_snapshot_end = [&](
      auto&& ae,
      TimeStamp ts,
      const SnapshotEnd&) {
    if (floating_snapshot)
      throw_invalid_query_fmt(
          "saw snapshot end while initial snapshot still floating");
    task_db.note_snapshot_end(ts);
  };

  optional<TimeStamp> last_event_ts;

  ae::process(
      // If multiple things happen at a timestamp, get into
      // snapshot-processing mode as quickly as possible.
      ae::input<SnapshotStart>(
          args.snapshot_start,
          handle_snapshot_start),
      // If a process dies and is immediately reborn, make sure to
      // process the death first.
      ae::input<SchedProcessExit>(
          args.sched_process_exit,
          handle_sched_process_exit),
      ae::input<TaskNewTask>(
          args.task_newtask,
          handle_task_newtask,
          ae::eager_slurp),
      // Leave snapshot mode after processing every else that happens
      // on a timestamp.
      ae::input<SnapshotEnd>(
          args.snapshot_end,
          handle_snapshot_end),
      ae::before_dispatch_hook(
          [&](auto&& ae, TimeStamp ts, const auto&) {
            if (last_event_ts) {
              assume(ts >= last_event_ts);
            } else if (ts < first_ts) {
              throw_invalid_query_fmt(
                  "saw event at %s before first_ts %s",
                  ts, first_ts);
            }
            last_event_ts = ts;
          }
      )
  );

  // Create the last_ts iterator after we're done with main loop
  // processing so we don't force query execution to stall on finding
  // the last ts.

  if (args.last_ts != Py_None) {
    TimeStamp last_ts = read_scalar<TimeStamp>(
        args.last_ts, "last_ts");
    if (*last_event_ts > last_ts)
      task_db.warning("explicit last timestamp in thread analysis %s "
                      "before end of trace %s!",
                      last_ts, *last_event_ts);
    if (!last_event_ts || *last_event_ts < last_ts)
      last_event_ts = last_ts;
  }

  if (last_event_ts)
    task_db.finalize(*last_event_ts);

  out_tid_mapping.flush();
  out_tgid_mapping.flush();
  h::for_each(samplers, [&](auto&& sampler) {
    sampler.out.flush();
  });
  return addref(Py_None);
}

unique_obj_pyref<Snapshot>
Snapshot::copy() const
{
  if (this->start_time)
    throw_pyerr_fmt(PyExc_ValueError, "too late to copy snapshot");
  auto new_snapshot = make_pyobj<Snapshot>();
  new_snapshot->threads = ThreadInfo(this->threads);
  new_snapshot->warnings = this->warnings;
  return new_snapshot;
}

void
Snapshot::merge_from(const Snapshot& other)
{
  this->warnings.insert(this->warnings.end(),
                        other.warnings.begin(),
                        other.warnings.end());
  if (other.start_time.has_value())
    throw_pyerr_fmt(PyExc_ValueError, "cannot merge used snapshot");
  for (const auto& [raw_tid, ot] : other.threads)
    this->threads[raw_tid].merge_from(raw_tid, this, ot);
}

String
Snapshot::format() const
{
  StringStream ss;

  auto format_field = [&](auto&& field_name, auto&& opt) {
    ss << field_name.c_str() << " ";
    if (!opt.has_value()) {
      ss << "[unset]";
    } else {
      if constexpr (std::is_scalar_v<decay_t<decltype(*opt)>>) {
        ss << *opt;
      } else {
        ss << repr(fmt("%s", *opt));
      }
    }
    ss << "\n";
  };

  for (const auto& [raw_tid, thread] : this->threads) {
    ss << raw_tid << "\n";
    h::for_each(static_cast<const ThreadData&>(thread),
                h::fuse(format_field));
    ss << "\n";
  }
  return ss.str();
}

void
Snapshot::Thread::merge_from(RawTid raw_tid,
                             Snapshot* merge_into,
                             const Snapshot::Thread& ot)
{
  assume(sst_is<ImportStatus::NotImported>(&ot));
  ThreadData& to = static_cast<ThreadData&>(*this);
  const ThreadData& from = static_cast<const ThreadData&>(ot);

  auto merge_field = [&](auto&& field_name, auto&& from_field) {
    auto& to_field = h::at_key(to, field_name);
    auto mode = ConsistentUpdateMode::WARN_ON_MISMATCH;
    if constexpr (is_same_v<
                  decay_t<decltype(to_field)>,
                  decay_t<decltype(this->raw_tgid)>>) {
      if (&to_field == &this->raw_tgid)
        mode = ConsistentUpdateMode::FAIL_ON_MISMATCH;
    }
    if (from_field.has_value())
      consistent_update(
          raw_tid,
          field_name.c_str(),
          mode,
          merge_into,
          &to_field,
          *from_field);
  };

  h::for_each(from, h::fuse(merge_field));
}

bool
Snapshot::operator==(const Snapshot& other) const noexcept
{
  return this->threads == other.threads;
}

bool
Snapshot::operator!=(const Snapshot& other) const noexcept
{
  return !(*this == other);
}

unique_pyref
Snapshot::py_richcompare(pyref other, int op) const
{
  if (isinstance_exact(other, &Snapshot::pytype)) {
    auto sother = other.as_unsafe<Snapshot>();
    if (op == Py_EQ)
      return make_pybool(*this == *sother);
    if (op == Py_NE)
      return make_pybool(*this != *sother);
  }
  return addref(Py_NotImplemented);
}



PyMethodDef Snapshot::pymethods[] = {
  AUTOMETHOD(&Snapshot::note_scanned_oom_score_adj,
             "Add OOM score adjust for a process",
             (RawTgid, tgid)
             (OomScore, oom_score_adj)
  ),
  AUTOMETHOD(&Snapshot::note_scanned_parent,
             "Add parent for a thread",
             (RawTid, tid)
             (RawTgid, parent)
  ),
  AUTOMETHOD(&Snapshot::note_scanned_tgid,
             "Add a thread's thread-group membership",
             (RawTid, tid)
             (RawTgid, tgid)
  ),
  AUTOMETHOD(&Snapshot::note_scanned_state,
             "Add a thread's running state",
             (RawTid, tid)
             (py_char_code, state)
  ),
  AUTOMETHOD(&Snapshot::note_scanned_comm,
             "Add a thread's comm",
             (RawTid, tid)
             (string_view, comm)
  ),
  AUTOMETHOD(&Snapshot::note_scanned_rss_anon,
             "Note a thread's anonymous RSS",
             (RawTid, tid)
             (MemorySize, bytes)),
  AUTOMETHOD(&Snapshot::note_scanned_rss_file,
             "Note a thread's file-backed RSS",
             (RawTid, tid)
             (MemorySize, bytes)),
  AUTOMETHOD(&Snapshot::parse_tid_stat,
             "Parse a /proc/tid/stat file into the snapshot",
             (RawTid, tid)
             (string_view, stat)
  ),
  AUTOMETHOD(&Snapshot::parse_tid_status,
             "Parse a /proc/tid/status file into the snapshot",
             (RawTid, tid)
             (string_view, status)
  ),
  AUTOMETHOD(&Snapshot::parse_text_dctv_snapshot,
             "Parse a whole DCTV text snapshot",
             (string_view, snapshot)
  ),
  AUTOMETHOD(&Snapshot::copy,
             "Make an independent copy of a snapshot",
  ),
  AUTOMETHOD(&Snapshot::format,
             "Format snapshot to a string",),
  { 0 },
};

PyTypeObject Snapshot::pytype = make_py_type<Snapshot>(
    "dctv._native.Snapshot",
    "Thread analysis snapshot",
    [](PyTypeObject* t) {
      t->tp_methods = Snapshot::pymethods;
      t->tp_richcompare = wraperr<&Snapshot::py_richcompare>();
    }
);

PyMethodDef functions[] = {
  make_methoddef("analyze_threads",
                 wraperr<analyze_threads>(),
                 METH_VARARGS | METH_KEYWORDS,
                 "Perform lifetime analysis of threads"),
  { 0 }
};

}  // anonymous namespace



void
init_thread_analysis(pyref m)
{
  register_type(m, &Snapshot::pytype);
  register_functions(m, functions);
  register_constant(m, "PID_MAX_LIMIT", PID_MAX_LIMIT);
  register_constant(m, "SYSTEM_UTID_BASE", system_utid_base);
}

}  // namespace dctv
