// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "pyerr.h"

#include <assert.h>
#include <functional>

#include "fmt.h"

namespace dctv {

struct StoredPyException final : virtual PyException {
  explicit StoredPyException(PyExceptionInfo info) noexcept;
  void set_pyexception() const noexcept override;
 private:
  PyExceptionInfo info;
};

const char*
PyException::what() const noexcept
{
  try {
    if (this->msg.empty()) {
      PyExceptionSaver saved;  // Make transparent
      this->set_pyexception();
      PyExceptionInfo info = PyExceptionInfo::fetch();
      assert(info.type);
      info.normalize();
      assert(info.value);
      this->msg = fmt("%s: %s", str(info.type), str(info.value));
    }
    return this->msg.c_str();
  } catch (...) {
    return "[exception making PyException::what()]";
  }
}

PyExceptionInfo
PyExceptionInfo::fetch() noexcept
{
  PyObject* py_type;
  PyObject* py_value;
  PyObject* py_traceback;
  PyErr_Fetch(&py_type, &py_value, &py_traceback);
  return PyExceptionInfo{adopt(py_type),
                         adopt(py_value),
                         adopt(py_traceback)};
}

void
PyExceptionInfo::normalize() noexcept
{
  PyObject* py_type = this->type.release();
  PyObject* py_value = this->value.release();
  PyObject* py_traceback = this->traceback.release();
  PyErr_NormalizeException(&py_type, &py_value, &py_traceback);
  this->type = adopt(py_type);
  this->value = adopt(py_value);
  this->traceback = adopt(py_traceback);
  // TODO(dancol): figure out whether we want traceback
  if (false && this->traceback)  // NOLINT
    (void) PyException_SetTraceback(this->value.get(),
                                    this->traceback.get());
}

void
PyExceptionInfo::restore() && noexcept
{
  PyErr_Restore(this->type.release(),
                this->value.release(),
                this->traceback.release());
}

PyExceptionInfo
PyExceptionInfo::dup() const noexcept
{
  return { this->type.addref(),
           this->value.addref(),
           this->traceback.addref() };
}

StoredPyException::StoredPyException(PyExceptionInfo info) noexcept
    : info(std::move(info))
{
  assume(this->info.type);
}

void
StoredPyException::set_pyexception() const noexcept
{
  this->info.dup().restore();
}

// The compiler (well, Clang 5 at least) is too stupid to understand
// that even though _set_pending_cxx_exception_as_pyexception contains
// a "throw", its noexcept clause is valid because we always catch the
// exception we throw.

#ifdef __clang__
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wexceptions"
#endif  // __clang__

void
_set_pending_cxx_exception_as_pyexception() noexcept
{
  // Using ExFun here erases the functor type in the catch clauses
  // below and short-circuits infinite recursion in template
  // expansion.  In the error case, we don't really care about the
  // overhead of calling through std::function, especially since the
  // functor should fit in the std::function's internal buffer.
  using ExFun = std::function<void()>;
  try {
    throw;
  } catch (const PyException& ex) {
    ex.set_pyexception();
  } catch (const std::bad_alloc& ex) {
    PyErr_NoMemory();
  } catch (const std::exception& ex) {
    _exceptions_to_pyerr<ExFun>([&](){
      _throw_pyerr_fmt(PyExc_SystemError,
                       "unknown C++ exception: %s",
                       ex.what());
    });
  } catch (...) {
    _exceptions_to_pyerr<ExFun>([&](){
      throw_pyerr_msg(PyExc_SystemError,
                      "unknown C++ exception");
    });
  }
  assume(pyerr_occurred());
}

#ifdef __clang__
# pragma GCC diagnostic pop
#endif // __clang__

void
throw_current_pyerr()
{
  PyExceptionInfo info = PyExceptionInfo::fetch();
  if (!info.type) {
    PyErr_SetString(PyExc_AssertionError,
                    "got_py_error found no exception set");
    info = PyExceptionInfo::fetch();
    assume(!pyerr_occurred());
  }
  throw StoredPyException(std::move(info));
}

void
throw_from_errno()
{
  PyErr_SetFromErrno(PyExc_OSError);
  throw_current_pyerr();
}

void
throw_from_errno_with_filename(const char* filename)
{
  int saved_errno = errno;
  unique_pyref fn = make_pystr(filename);
  errno = saved_errno;
  PyErr_SetFromErrnoWithFilenameObject(PyExc_OSError, fn.get());
  throw_current_pyerr();
}

void
throw_pyerr_msg(pyref cls, const char* msg)
{
  PyErr_SetString(cls.get(), msg);
  throw_current_pyerr();
}

void
_throw_pyerr_fmt(pyref cls, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  throw_pyerr_fmtv(cls.get(), fmt, args);
}

void
throw_pyerr_fmtv(pyref cls, const char* fmt, va_list args)
{
  PyErr_FormatV(cls.get(), fmt, args);
  throw_current_pyerr();
}

void
throw_wrong_type_error(pyref obj, obj_pyref<PyTypeObject> expected_type)
{
  _throw_pyerr_fmt(PyExc_TypeError,
                   "wrong argument type: want a %R: obj=%R",
                   expected_type.get(), obj.get());
}

}  // namespace dctv
