// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

QueryKey::QueryKey(pyref query_node)
    : query_node(query_node.notnull().addref())
{
  check_pytype(QueryClasses::get().cls_query_node,
               query_node.get());
}

QueryKey::QueryKey(const QueryKey& other) noexcept
    : query_node(addref(other.as_pyref()))
{}

QueryKey&
QueryKey::operator=(const QueryKey& other) noexcept
{
  if (this != &other)
    this->query_node = addref(other.query_node);
  return *this;
}

QueryKey
QueryKey::from_py(pyref query_node)
{
  return QueryKey(query_node);
}

const unique_pyref&
QueryKey::as_pyref() const noexcept
{
  return this->query_node;
}

uintptr_t
QueryKey::get_sort_key() const noexcept
{
  return reinterpret_cast<uintptr_t>(this->query_node.get());
}

bool
QueryKey::operator==(const QueryKey& other) const noexcept
{
  return this->get_sort_key() == other.get_sort_key();
}

bool
QueryKey::operator<(const QueryKey& other) const noexcept
{
  return this->get_sort_key() < other.get_sort_key();
}

constexpr
size_t
QueryKey::get_query_node_offset()
{
  return offsetof(QueryKey, query_node) + unique_pyref::get_pyval_offset();
}

}  // namespace dctv
