// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename From, typename To>
void
typecheck_cast(obj_pyref<From> obj)
    noexcept(is_safe_pycast<From, To>())
{
  if constexpr (!is_safe_pycast<From, To>()) {  // NOLINT
    if (obj) {
      PyTypeObject* wanted_type = pytype_of<To>();
      pyref obj_as_base = obj.template as_unsafe<PyObject>();
      if constexpr (std::is_final_v<To>) {
        check_pytype_exact(wanted_type, obj_as_base);
      } else {  // NOLINT
        check_pytype(wanted_type, obj_as_base);
      }
    }
  }
}

template<typename T>
constexpr
unique_obj_pyref<T>::unique_obj_pyref() noexcept
    : ref(nullptr)
{}

template<typename T>
unique_obj_pyref<T>::~unique_obj_pyref() noexcept
{
  Py_XDECREF(this->ref);
}

template<typename T>
unique_obj_pyref<T>::unique_obj_pyref(unique_obj_pyref&& other) noexcept
    : ref(other.ref)  // NOLINT
{
  other.ref = nullptr;
}

template<typename T>
unique_obj_pyref<T>&
unique_obj_pyref<T>::operator=(unique_obj_pyref&& other) noexcept
{
  // Be careful to not release our currently-held object before
  // assigning the new once, since dropping the reference can run
  // arbitrary code.
  swap(*this, other);
  other.reset();
  return *this;
}

template<typename T>
T*
unique_obj_pyref<T>::get() const noexcept
{
  return this->ref;
}

template<typename T>
T&
unique_obj_pyref<T>::operator*() const noexcept
{
  return *this->get();
}

template<typename T>
T*
unique_obj_pyref<T>::operator->() const noexcept
{
  return this->get();
}

template<typename T>
T*
unique_obj_pyref<T>::release() noexcept
{
  T* ret = this->ref;
  this->ref = nullptr;
  return ret;
}

template<typename T>
void
unique_obj_pyref<T>::reset() noexcept
{
  // N.B. dropping the reference might run arbitrary code, so clear
  // the field before the refcount drop.
  if (this->ref) {
    T* tmp_ref = this->ref;
    this->ref = nullptr;
    Py_DECREF(tmp_ref);
  }
}

template<typename T>
unique_obj_pyref<T>
unique_obj_pyref<T>::addref() const & noexcept
{
  return this->addref_as<T>();
}

template<typename T>
unique_obj_pyref<T>
unique_obj_pyref<T>::addref() && noexcept
{
  return this->addref_as<T>();
}

template<typename T>
unique_obj_pyref<T>::operator bool() const noexcept
{
  return this->ref != nullptr;
}

template<typename T>
unique_obj_pyref<T>
unique_obj_pyref<T>::adopt(T* obj) noexcept
{
  return unique_obj_pyref<T>(obj);
}

template<typename T>
template<typename R>
obj_pyref<R>
unique_obj_pyref<T>::as_unsafe() const noexcept
{
  return obj_pyref<T>(*this).template as_unsafe<R>();
}

template<typename T>
template<typename R>
obj_pyref<R>
unique_obj_pyref<T>::as() const
    noexcept(is_safe_pycast<T, R>())
{
  typecheck_cast<T, R>(*this);
  return this->as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::addref_as_unsafe() const & noexcept
{
  return this->template as_unsafe<R>().addref();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::addref_as_unsafe() && noexcept
{
  return this->template move_as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::addref_as() const &
    noexcept(is_safe_pycast<T, R>())
{
  typecheck_cast<T, R>(*this);
  return this->template addref_as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::addref_as() &&
    noexcept(is_safe_pycast<T, R>())
{
  typecheck_cast<T, R>(*this);
  return this->template addref_as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::move_as_unsafe() noexcept
{
  return unique_obj_pyref<R>::adopt(
      pyref(this->release())
      .template as_unsafe<R>()
      .get());
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
unique_obj_pyref<T>::move_as()
    noexcept(is_safe_pycast<T, R>())
{
  typecheck_cast<T, R>(*this);
  return this->template move_as_unsafe<R>();
}

template<typename T>
template<typename... Args>
unique_obj_pyref<T>
unique_obj_pyref<T>::make(obj_pyref<PyTypeObject> type, Args&&... args)
{
  static_assert(std::is_base_of_v<BasePyObject, T>);
  assume(type->tp_basicsize >= static_cast<ssize_t>(sizeof (T)));
  const bool supports_gc = std::is_base_of_v<SupportsGc, T>;
  const bool has_gc_flag = PyType_IS_GC(type);
  if (supports_gc)
    assume(has_gc_flag);
  PyObject* py_obj = type->tp_alloc(type.get(), 0);
  if (!py_obj)
    throw_current_pyerr();
  assume(Py_TYPE(py_obj));
  bool success = false;
  FINALLY([&]{
    if (!success) {
      if(Py_REFCNT(py_obj) != 1)
        abort();  // See the comment above make_py_type in pyobj.h.
      type->tp_free(py_obj);
    }});
  if constexpr (sizeof...(Args) == 0) {
    // Avoid unwanted zero initialization via value initialization by
    // not using () or {}, both of which trigger bonus zero-fill
    // (clobbering the Python object header) before
    // invoking constructors.
    new (py_obj) T;
  } else {  // NOLINT
    new (py_obj) T{std::forward<Args>(args)...};
  }
  assume(Py_TYPE(py_obj));
  success = true;
  if (has_gc_flag)
    PyObject_GC_Track(py_obj);
  return unique_pyref::adopt(py_obj).template addref_as_unsafe<T>();
}

template<typename T>
constexpr
size_t
unique_obj_pyref<T>::get_pyval_offset()
{
  static_assert(sizeof (unique_obj_pyref::ref) == sizeof (PyObject*));
  return offsetof(unique_obj_pyref, ref);
}

template<typename T>
unique_obj_pyref<T>&
unique_obj_pyref<T>::notnull() & noexcept
{
  assume(*this);
  return *this;
}

template<typename T>
unique_obj_pyref<T>&&
unique_obj_pyref<T>::notnull() && noexcept
{
  assume(*this);
  return std::move(*this);
}

template<typename T>
unique_obj_pyref<T> const&
unique_obj_pyref<T>::notnull() const& noexcept
{
  assume(*this);
  return *this;
}

template<typename T>
unique_obj_pyref<T>::unique_obj_pyref(T* obj) noexcept
    : ref(obj)
{}

template<typename T>
constexpr
obj_pyref<T>::obj_pyref() noexcept
    : ref(nullptr)
{}

template<typename T>
obj_pyref<T>::operator bool() const noexcept
{
  return this->ref != nullptr;
}

template<typename T>
T*
obj_pyref<T>::get() const noexcept
{
  return this->ref;
}

template<typename T>
T&
obj_pyref<T>::operator*() const noexcept
{
  return *this->get();
}

template<typename T>
T*
obj_pyref<T>::operator->() const noexcept
{
  return this->get();
}

template<typename T>
unique_obj_pyref<T>
obj_pyref<T>::addref() const noexcept
{
  Py_XINCREF(this->get());
  return unique_obj_pyref<T>::adopt(this->get());
}

template<typename T>
template<typename R>
obj_pyref<R>
obj_pyref<T>::as_unsafe() const noexcept
{
  if (safe_mode)
    typecheck_cast<T, R>(*this);
  return reinterpret_cast<R*>(this->get());
}

template<typename T>
template<typename R>
obj_pyref<R>
obj_pyref<T>::as() const noexcept(is_safe_pycast<T, R>())
{
  typecheck_cast<T, R>(*this);
  return this->template as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
obj_pyref<T>::addref_as_unsafe() const noexcept
{
  return this->addref().template addref_as_unsafe<R>();
}

template<typename T>
template<typename R>
unique_obj_pyref<R>
obj_pyref<T>::addref_as() const noexcept(is_safe_pycast<T, R>())
{
  return this->addref().template addref_as<R>();
}

template<typename T>
obj_pyref<T>&
obj_pyref<T>::notnull() & noexcept
{
  assume(*this);
  return *this;
}

template<typename T>
obj_pyref<T>&&
obj_pyref<T>::notnull() && noexcept
{
  assume(*this);
  return std::move(*this);
}

template<typename T>
obj_pyref<T> const&
obj_pyref<T>::notnull() const& noexcept
{
  assume(*this);
  return *this;
}

bool
operator==(pyref left, pyref right) noexcept
{
  return left.get() == right.get();
}

bool
operator!=(pyref left, pyref right) noexcept
{
  return left.get() != right.get();
}

bool
isinstance_exact(pyref obj, obj_pyref<PyTypeObject> type) noexcept
{
  return pytype(obj) == type.get();
}

template<typename T>
constexpr
PyTypeObject*
pytype_of()
{
  return pytype_traits<T>::get_pytype();
}

void
check_pytype_exact(obj_pyref<PyTypeObject> type, pyref obj)
{
  if (!isinstance_exact(obj, type))
    throw_wrong_type_error(obj, type);
}

obj_pyref<PyTypeObject>
pytype(pyref obj) noexcept
{
  return obj_pyref<PyTypeObject>(Py_TYPE(obj.get())).notnull();
}

Py_ssize_t
refcount(pyref obj) noexcept
{
  return Py_REFCNT(obj.get());
}

template<typename... Args>
unique_pyref
call(pyref callable, Args&&... args)
{
  // N.B. the fastcall API was blessed in PEP 580 (for Python 3.8) but
  // exists as far back at 3.6, so let's just use it even though we
  // target 3.6.
  PyObject* argbuf[sizeof...(Args)] = {
    pyref(std::forward<Args>(args)).get()...
  };
  return adopt_check(_PyObject_FastCall(
      callable.get(), argbuf, sizeof...(Args)));
}

template<typename... Args>
std::pair<GenRet, unique_pyref>
call_gen(pyref callable, Args&&... args)
{
  // N.B. the fastcall API was blessed in PEP 580 (for Python 3.8) but
  // exists as far back at 3.6, so let's just use it even though we
  // target 3.6.
  PyObject* argbuf[sizeof...(Args)] = {
    pyref(std::forward<Args>(args)).get()...
  };
  unique_pyref ret = adopt(_PyObject_FastCall(callable.get(),
                                              argbuf,
                                              sizeof...(Args)));
  if (ret)
    return { GenRet::YIELDED, std::move(ret) };
  return { GenRet::RETURNED, fetch_stop_iteration_value().notnull() };
}

unique_pystr
make_pystr(char c, const char* errors)
{
  return make_pystr(std::string_view(&c, 1), errors);
}

unique_pyref
make_pylong(int value)
{
  return make_pylong(static_cast<long>(value));
}

unique_pyref
make_pylong(unsigned int value)
{
  return make_pylong(static_cast<unsigned long>(value));
}

unique_pyref
adopt(PyObject* obj) noexcept
{
  return unique_pyref::adopt(obj);
}

unique_pyref
adopt_check(PyObject* obj)
{
  if (!obj)
    throw_current_pyerr();
  return adopt(obj);
}

template<typename T>
unique_obj_pyref<T>
adopt_as(PyObject* obj) noexcept
{
  return adopt(obj).addref_as<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_check_as(PyObject* obj)
{
  return adopt_check(obj).addref_as<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_as_unsafe(PyObject* obj) noexcept
{
  return adopt(obj).addref_as_unsafe<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_check_as_unsafe(PyObject* obj)
{
  return adopt_check(obj).addref_as_unsafe<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_check_as(T* obj)
{
  return adopt_check(reinterpret_cast<PyObject*>(obj)).addref_as<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_as_unsafe(T* obj) noexcept
{
  return adopt(reinterpret_cast<PyObject*>(obj))
      .addref_as_unsafe<T>();
}

template<typename T>
unique_obj_pyref<T>
adopt_check_as_unsafe(T* obj)
{
  return adopt_check(reinterpret_cast<PyObject*>(obj))
      .addref_as_unsafe<T>();
}

unique_pyref
addref(pyref obj) noexcept
{
  return obj.addref();
}

template<typename T>
unique_pyref
addref(unique_obj_pyref<T>&& obj) noexcept
{
  static_assert(noexcept(obj.template addref_as<PyObject>()));
  return AUTOFWD(obj).template addref_as<PyObject>();
}

template<typename T>
unique_obj_pyref<T>
xaddref(const T* obj,
        typename std::enable_if_t<pytype_traits<T>::exists>*)
    noexcept
{
  return obj_pyref<T>(obj).addref();
}

template<typename T>
unique_obj_pyref<T>
xaddref_unsafe(T* obj) noexcept
{
  return unique_obj_pyref<T>::adopt(
      reinterpret_cast<T*>(
          addref(reinterpret_cast<PyObject*>(obj)).release()));
}

template<typename T, typename... Args>
unique_obj_pyref<T>
make_pyobj(Args&&... args)
{
  // Catch accidentally using a non-registered type.
  assert(pytype(pytype_of<T>()));
  return unique_obj_pyref<T>::make(
      pytype_of<T>(),
      std::forward<Args>(args)...);
}

template<typename T>
T
from_py(pyref value)
{
  T result;
  if constexpr(std::is_same_v<T, long>) {
    result = PyLong_AsLong(value.get());
  } else if constexpr(std::is_same_v<T, unsigned long>) {
    result = PyLong_AsUnsignedLong(value.get());
  } else if constexpr(std::is_same_v<T, long long>) {
    result = PyLong_AsLongLong(value.get());
  } else if constexpr(std::is_same_v<T, unsigned long long>) {
    result = PyLong_AsUnsignedLongLong(value.get());
  } else {
    result = errhack<T>::err();
  }
  if (result == static_cast<T>(-1) && pyerr_occurred())
    throw_current_pyerr();
  return result;
}

unique_pyref
make_pybool(bool value) noexcept
{
  return addref(value ? Py_True : Py_False);
}

template<typename T>
T
mul_raise_on_overflow(T a, T b)
{
  T ret;
  if (mul_overflow(a, b, &ret))
    _throw_pyerr_fmt(PyExc_OverflowError,
                     "multiplication overflow: %R x %R",
                     make_pylong(a).get(),
                     make_pylong(b).get());
  return ret;
}

template<typename T>
T
mul_raise_on_overflow_safe_only(T a, T b) noexcept(!safe_mode)
{
  return safe_mode
      ? mul_raise_on_overflow(a, b)
      : a * b;
}

template<typename T>
T
add_raise_on_overflow(T a, T b)
{
  T ret;
  if (add_overflow(a, b, &ret))
    _throw_pyerr_fmt(PyExc_OverflowError,
                     "addition overflow: %R + %R",
                     make_pylong(a).get(),
                     make_pylong(b).get());
  return ret;
}

bool
is_py_debug() noexcept
{
  return static_cast<bool>(Py_DebugFlag);
}

}  // namespace dctv
