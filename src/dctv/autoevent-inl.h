// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

namespace auto_event {

constexpr auto make_python_chunk_iterator_type =
    hana::template_<PythonChunkIterator>;

constexpr auto make_variant_type =
    hana::template_<SimpleVariant>;

// Implemented by inputs
DCTV_DEFINE_TAG(op_prime);
DCTV_DEFINE_TAG(op_refill);
DCTV_DEFINE_TAG(op_get_payload_type);
DCTV_DEFINE_TAG(op_setup);
DCTV_DEFINE_TAG(op_dispatch);
DCTV_DEFINE_TAG(op_tiebreak_order);

// Implemented by the central loop
DCTV_DEFINE_TAG(op_enqueue_event);
DCTV_DEFINE_TAG(op_dispatch_from_pending_events);
DCTV_DEFINE_TAG(op_is_partition_major_mode);

DCTV_DEFINE_TAG(config_inputs);
DCTV_DEFINE_TAG(config_before_dispatch_hooks);
DCTV_DEFINE_TAG(config_ts_batch_end_hooks);
DCTV_DEFINE_TAG(config_partition_end_hooks);
DCTV_DEFINE_TAG(config_partition_major_mode);

DCTV_DEFINE_TAG(iconfig_add_index_argument);
DCTV_DEFINE_TAG(iconfig_eager_slurp);
DCTV_DEFINE_TAG(iconfig_enable_when);
DCTV_DEFINE_TAG(iconfig_ordering_tiebreaker);

constexpr auto config_add = [](auto&& config, auto&& key, auto&& value) {
  namespace h = hana;
  auto old_value = std::move(config[key]);
  auto new_value = h::append(std::move(old_value), AUTOFWD(value));
  return h::union_(AUTOFWD(config),
                   h::make_map(
                       h::make_pair(key, std::move(new_value))));
};

constexpr auto config_set = [](auto&& config, auto&& key, auto&& value) {
  namespace h = hana;
  static_assert(CONSTEXPR_VALUE(h::contains(config, key)));
  return h::union_(AUTOFWD(config),
                   h::make_map(h::make_pair(key, AUTOFWD(value))));
};

constexpr auto payload_tag_map = [](auto&& payload_w) {
  namespace h = hana;
  using PayloadW = std::decay_t<decltype(payload_w)>;
  using Payload = typename PayloadW::payload_type;
  return h::unpack(
      h::filter(
          h::transform(
              h::zip(Payload::_make_autoevent_meta(),
                     struct_field_names<Payload>),
              h::fuse([](auto&& tag, auto&& field_name) {
                return h::make_pair(tag, field_name);
              })),
          h::fuse([](auto&& tag, auto&& field_name) {
            return tag != no_annotation;
          })),
      h::make_map);
};

constexpr auto payload_get_field_by_tag = [](auto&& payload_w, auto&& tag) {
  namespace h = hana;
  using PayloadW = std::decay_t<decltype(payload_w)>;
  using Payload = typename PayloadW::payload_type;
  const auto& payload = static_cast<const Payload&>(payload_w);
  auto payload_map = payload_tag_map(payload);
  auto payload_field_name = h::at_key(payload_map, tag);
  return h::at_key(payload, payload_field_name);
};

constexpr auto payload_has_tag = [](auto&& payload_w, auto&& tag) {
  using PayloadW = std::decay_t<decltype(payload_w)>;
  return hana::contains(PayloadW::_make_autoevent_meta(), tag);
};

template<typename SN>
struct OneSource {
  using SourceNumber = SN;
  OneSource(SourceNumber source_number) {  // NOLINT
    assume(source_number == 0);
  }
  SourceNumber get_source_number() const noexcept {
    return 0;
  }
};

template<typename SN>
struct MultiSource {
  using SourceNumber = SN;
  SourceNumber get_source_number() const noexcept {
    return this->source_number;
  }
  SourceNumber source_number;
};

struct WithIndexBase {
  Index index;

  template<typename Args>
  auto maybe_add_index(Args&& args) const {
    return hana::append(AUTOFWD(args), this->index);
  }
};

struct NoIndexBase {
  NoIndexBase(Index /*ignored*/) {}  // NOLINT
  template<typename Args>
  auto maybe_add_index(Args&& args) const {
    return args;
  }
};

struct EventCheckMeta final {
  TimeStamp ts;
  Partition partition;
};

template<typename Ae, typename Payload, typename SourceInfo>
void
check_source_consistency(Ae&& ae,
                         TimeStamp ts,
                         const Payload& payload,
                         SourceInfo& source_info)
{
  if (ts < 0)
    throw_invalid_query_fmt("saw invalid timestamp %s", ts);
  constexpr bool pmm =
      CONSTEXPR_VALUE(is_partition_major_mode(ae));
  constexpr bool has_partition =
      CONSTEXPR_VALUE(payload_has_tag(payload, partition));
  constexpr bool is_span =
      CONSTEXPR_VALUE(payload_has_tag(payload, span_duration));
  static_assert(!pmm || has_partition);
  if constexpr (is_span) {
    if (!source_info.end_ts_by_partition)
      source_info.end_ts_by_partition.emplace();
  }

  EventCheckMeta ecm = { ts, 0 };
  if constexpr (has_partition) {
    ecm.partition = payload_get_field_by_tag(payload, partition);
  }
  if (source_info.last_ecm) {
    const EventCheckMeta& last_ecm = *source_info.last_ecm;
    if (pmm) {
      if (ecm.partition < last_ecm.partition)
        throw_invalid_query_fmt("partition inversion: %s < %s",
                                ecm.partition, last_ecm.partition);
      if (ecm.partition == last_ecm.partition && ecm.ts < last_ecm.ts)
        throw_invalid_query_fmt("timestamp inversion: %s < %s",
                                ecm.ts, last_ecm.ts);
      if constexpr (is_span) {
        TimeStamp duration =
            payload_get_field_by_tag(payload, span_duration);
        if (duration <= 0)
          throw_invalid_query_fmt("invalid span duration %s", duration);
        if (last_ecm.partition == ecm.partition) {
          if (last_ecm.ts == ecm.ts)
            throw_invalid_query_fmt(
                "timestamp %s failed to advance within partition %s",
                ecm.partition);
          TimeStamp* last_end =
              map_try_get(*source_info.end_ts_by_partition, 0);
          if (last_end && *last_end > ts)
            throw_invalid_query_fmt(
                "span start %s before span end %s in partition %s",
                ts, *last_end, ecm.partition);
        }
        (*source_info.end_ts_by_partition)[0] = ts + duration;
      }
    } else {
      if constexpr (!is_span) {
        if (ecm.ts < last_ecm.ts)
          throw_invalid_query_fmt("time inversion in event: %s < %s",
                                  ecm.ts, last_ecm.ts);
        // Partition is unordered for events
      } else {  // NOLINT
        TimeStamp duration =
            payload_get_field_by_tag(payload, span_duration);
        if (duration <= 0)
          throw_invalid_query_fmt("invalid span duration %s", duration);
        if (ecm.ts < last_ecm.ts)
          throw_invalid_query_fmt("time inversion in span: %s < %s",
                                  ecm.ts, last_ecm.ts);
        if (ecm.ts == last_ecm.ts && ecm.partition <= last_ecm.partition)
          throw_invalid_query_fmt(
              "partition %s (last %s) failed "
              "to increase monotonically at ts %s",
              ecm.partition,
              last_ecm.partition,
              ecm.ts);
        TimeStamp* last_end =
            map_try_get(*source_info.end_ts_by_partition, ecm.partition);
        if (last_end && *last_end > ts)
          throw_invalid_query_fmt(
              "span start %s before span end %s in partition %s",
              ts, *last_end, ecm.partition);
        (*source_info.end_ts_by_partition)[ecm.partition] = ts + duration;
      }
    }
  }
  source_info.last_ecm = ecm;
}

template<typename Payload,
         typename SourceNumber,
         typename Container,
         typename Callback,
         typename... Options>
auto
multi_input(const Container& py_iterators,
            Callback callback,
            Options&&... options)
{
  namespace h = hana;

  auto initial_iconfig = h::make_map(
      h::make_pair(iconfig_add_index_argument, h::false_c),
      h::make_pair(iconfig_eager_slurp, h::false_c),
      h::make_pair(iconfig_enable_when, h::true_c),
      h::make_pair(iconfig_ordering_tiebreaker,
                   [](auto&&...) { return false; })
  );

  auto iconfig = h::fold_left(
      h::make_tuple(AUTOFWD(options)...),
      initial_iconfig,
      [&](auto&& iconfig, auto&& option) {
        if constexpr (CONSTEXPR_VALUE(
            h::typeid_(option) ==
            h::typeid_(add_index_argument))) {
          return config_set(AUTOFWD(iconfig),
                            iconfig_add_index_argument,
                            h::true_c);
        } else if constexpr(CONSTEXPR_VALUE(  // NOLINT
            h::typeid_(option) ==
            h::typeid_(eager_slurp))) {
          return config_set(AUTOFWD(iconfig),
                            iconfig_eager_slurp,
                            h::true_c);
        } else {
          return option(AUTOFWD(iconfig));
        }
      });

  auto payload_field_types = struct_field_types<Payload>;
  auto decorated_payload_field_types =
      h::prepend(payload_field_types, h::type_c<TimeStamp>);
  using ChunkIterator = typename decltype(
      h::unpack(
          decorated_payload_field_types,
          make_python_chunk_iterator_type)
  )::type;
  struct SourceInfo final {
    explicit SourceInfo(unique_pyref py_iter)
        : iterator(std::move(py_iter))
    {}
    ChunkIterator iterator;
    // For automated constraint checking.
    optional<EventCheckMeta> last_ecm;
    optional<HashTable<Partition, TimeStamp>> end_ts_by_partition;
  };

  auto opt_nsources = maybe_compile_time_container_size(py_iterators);
  constexpr bool nsources_is_known = h::is_just(opt_nsources);
  constexpr bool nsources_is_one =
      opt_nsources.value_or(h::size_c<0>) == h::size_c<1>;

  // Use the empty base optimization in case the source number is a
  // constant zero.

  constexpr bool add_index_argument = CONSTEXPR_VALUE(
      iconfig[iconfig_add_index_argument]);

  struct ExtendedPayload :
      std::conditional_t<nsources_is_one,
                         OneSource<SourceNumber>,
                         MultiSource<SourceNumber>>,
      std::conditional_t<add_index_argument,
                         WithIndexBase,
                         NoIndexBase>,
      Payload
  {
    const Payload& as_base_payload() const {
      return static_cast<const Payload&>(*this);
    }
  };

  auto make_source_info_bundle_1 = [&] {
    if constexpr (nsources_is_known) {
      using SourceInfoArray =
          std::array<SourceInfo, CONSTEXPR_VALUE(*opt_nsources)>;
      return h::unpack(
          py_iterators,
          [](auto&&... py_iters) {
            return SourceInfoArray{SourceInfo(py_iters.addref())...};
          });
    } else {  // NOLINT
      using Sn = typename ExtendedPayload::SourceNumber;
      static_assert(std::numeric_limits<Sn>::max() <=
                    std::numeric_limits<size_t>::max());
      constexpr auto max_sources = std::numeric_limits<Sn>::max();
      if (py_iterators.size() > static_cast<size_t>(max_sources))
        throw_pyerr_msg(PyExc_OverflowError, "too many sources");
      Vector<SourceInfo> source_info_bundle;
      source_info_bundle.reserve(py_iterators.size());
      for (pyref py_it : py_iterators)
        source_info_bundle.emplace_back(py_it.addref());
      return source_info_bundle;
    }
  };

  auto make_source_info_bundle = [&] {
    using SourceInfoBundle = decltype(make_source_info_bundle_1());
    using OptionalSourceInfoBundle = optional<SourceInfoBundle>;
    if (iconfig[iconfig_enable_when]) {
      return OptionalSourceInfoBundle(make_source_info_bundle_1());
    } else {  //  NOLINT
      return OptionalSourceInfoBundle();
    }
  };

  return [source_info_bundle{make_source_info_bundle()},
          iconfig{std::move(iconfig)},
          cb(Callback(AUTOFWD(callback)))]
      (auto op, auto&&... args) mutable
  {
    auto maybe_slurp = [&](auto&& ae, SourceNumber source_number) {
      auto& source_info = (*source_info_bundle)[source_number];
      auto& chunk_iterator = source_info.iterator;

      if (chunk_iterator.is_at_eof())
        return;
      bool keep_looping;
      do {
        auto row = chunk_iterator.get();
        TimeStamp ts = h::at_c<0>(row);
        auto payload = h::unpack(h::drop_front_exactly(row),
                                 make_aggregate<Payload>);
        if (check_source_invariants)
          check_source_consistency(ae, ts, payload, source_info);
        ae(op_enqueue_event,
           ts,
           make_aggregate<ExtendedPayload>(
               source_number,
               chunk_iterator.get_index(),
               std::move(payload)));
        // Keep looping while we're at the same ts
        keep_looping = (chunk_iterator.advance() &&
                        iconfig[iconfig_eager_slurp] &&
                        h::at_c<0>(chunk_iterator.get()) == ts);
      } while (keep_looping);
    };

    auto do_dispatch = [&](auto&& ae,
                           TimeStamp ts,
                           const ExtendedPayload& ex_payload) {
      // GCC 8 (8.0.1 anyway) has a bug that makes it evaluate *both*
      // branches of an "if constexpr (add_index_argument)" here,
      // which causes the compile to fail because NoIndexBase doesn't
      // have an index field.  Instead, indirect through a member
      // function of the index base to force the compiler to get it
      // right because it has to do type lookup correctly to find the
      // member function.
      return h::unpack(
          ex_payload.maybe_add_index(
              h::make_tuple(
                  ex_payload.get_source_number(),
                  AUTOFWD(ae),
                  ts,
                  std::cref(ex_payload.as_base_payload()))),
          cb);
    };

    auto do_prime = [&](auto&& ae) {
      if (source_info_bundle) {
        const SourceNumber nsources =
            static_cast<SourceNumber>(source_info_bundle->size());
        for (SourceNumber source_number = 0;
             source_number < nsources;
             ++source_number)
          maybe_slurp(AUTOFWD(ae), source_number);
      }
    };

    auto do_refill = [&](auto&& ae, const ExtendedPayload& ex_payload) {
      if (source_info_bundle) {
        SourceNumber nsources =
            static_cast<SourceNumber>(source_info_bundle->size());
        SourceNumber source_number = ex_payload.get_source_number();
        assume(source_number < nsources);
        maybe_slurp(AUTOFWD(ae), source_number);
      }
    };

    auto do_get_payload_type = [&]() {
      return h::type_c<ExtendedPayload>;
    };

    auto do_setup = [&](auto&& config, auto&& self) {
      return config_add(AUTOFWD(config),
                        config_inputs,
                        AUTOFWD(self));
    };

    auto do_tiebreak_order = [&](TimeStamp left_ts,
                                 const ExtendedPayload& left,
                                 TimeStamp right_ts,
                                 const ExtendedPayload& right)
    {
      // See the comment about GCC bugs in do_dispatch for why we use
      // the horribly ugly maybe_add_index thing.
      return h::unpack(
          right.maybe_add_index(
              left.maybe_add_index(
                  h::make_tuple(left.get_source_number(),
                                right.get_source_number(),
                                std::cref(left.as_base_payload()),
                                std::cref(right.as_base_payload())))),
          iconfig[iconfig_ordering_tiebreaker]);
    };

    return h::make_map(
        h::make_pair(op_tiebreak_order, do_tiebreak_order),
        h::make_pair(op_dispatch, do_dispatch),
        h::make_pair(op_prime, do_prime),
        h::make_pair(op_refill, do_refill),
        h::make_pair(op_get_payload_type, do_get_payload_type),
        h::make_pair(op_setup, do_setup)
    )[op](AUTOFWD(args)...);
  };
}

template<typename Payload,
         typename Callback,
         typename... Options>
auto
input(pyref iterator,
      Callback callback,
      Options&&... options)
{
  return multi_input<Payload>(
      std::array{iterator},
      [callback{std::move(callback)}]
      (auto&& source_number, auto&&... args) {
        assume(source_number == 0);
        return callback(AUTOFWD(args)...);
      },
      std::forward<Options>(options)...);
}

template<typename Payload, typename Callback>
auto
synthetic_event(Callback callback)
{
  namespace h = hana;

  return [cb(Callback(AUTOFWD(callback)))]
      (auto op, auto&&... args)
  {
    auto do_dispatch = [&](auto&& ae,
                           TimeStamp ts,
                           const Payload& payload) {
      return cb(AUTOFWD(ae), ts, payload);
    };

    auto do_prime = [&](auto&& ae) {
      // Nothing to do.
    };

    auto do_refill = [&](auto&& ae, const Payload&) {
      // Nothing to do.
    };

    auto do_get_payload_type = [&]() {
      return h::type_c<Payload>;
    };

    auto do_setup = [&](auto&& config, auto&& self) {
      return config_add(AUTOFWD(config),
                        config_inputs,
                        AUTOFWD(self));
    };

    auto do_tiebreak_order = [&](TimeStamp left_ts,
                                 const Payload& left,
                                 TimeStamp right_ts,
                                 const Payload& right)
    {
      return false;
    };

    return h::make_map(
        h::make_pair(op_tiebreak_order, do_tiebreak_order),
        h::make_pair(op_dispatch, do_dispatch),
        h::make_pair(op_prime, do_prime),
        h::make_pair(op_refill, do_refill),
        h::make_pair(op_get_payload_type, do_get_payload_type),
        h::make_pair(op_setup, do_setup)
    )[op](AUTOFWD(args)...);
  };
}

template<typename SetupFunction>
auto
_make_ae_setup(SetupFunction fn)
{
  namespace h = hana;
  return [fn{std::move(fn)}](auto op, auto&&... args) {
    auto do_setup = [&](auto&& config, auto&& self) {
      return std::move(fn)(AUTOFWD(config));
    };
    static_assert(CONSTEXPR_VALUE(op == h::typeid_(op_setup)));
    return do_setup(AUTOFWD(args)...);
  };
}

template<typename ConfigField, typename Callback>
auto
_ae_add_hook(ConfigField cf, Callback callback)
{
  return _make_ae_setup([cf, cb{std::move(callback)}](auto&& config) {
    return config_add(AUTOFWD(config), cf, std::move(cb));
  });
}

template<typename Callback>
auto
before_dispatch_hook(Callback callback)
{
  return _ae_add_hook(config_before_dispatch_hooks, std::move(callback));
}

template<typename Callback>
auto
ts_batch_end_hook(Callback callback)
{
  return _ae_add_hook(config_ts_batch_end_hooks, std::move(callback));
}

template<typename Callback>
auto
partition_end_hook(Callback callback)
{
  return _ae_add_hook(config_partition_end_hooks, std::move(callback));
}

template<typename Condition>
auto
enable_when(Condition condition)
{
  return [condition{std::move(condition)}] (auto&& iconfig) {
    return config_set(AUTOFWD(iconfig),
                      iconfig_enable_when,
                      std::move(condition));
  };
}

template<typename Tiebreaker>
auto
ordering_tiebreaker(Tiebreaker tiebreaker)
{
  return [tiebreaker{std::move(tiebreaker)}] (auto&& iconfig) {
    return config_set(AUTOFWD(iconfig),
                      iconfig_ordering_tiebreaker,
                      std::move(tiebreaker));
  };
}

template<typename T,
         typename Container = std::vector<T>,
         typename Compare = std::less<typename Container::value_type>>
struct PriorityQueueEx final : std::priority_queue<T, Container, Compare>
{
  using Base = std::priority_queue<T, Container, Compare>;
  using Base::Base;
  T pop_destructive() {
    T front = std::move(this->c.front());
    this->pop();
    return front;
  };

  template<typename Predicate>
  optional<T> remove_one_if(Predicate&& predicate);
};

template<typename T, typename Container, typename Compare>
template<typename Predicate>
optional<T>
PriorityQueueEx<T, Container, Compare>::remove_one_if(
    Predicate&& predicate)
{
  std::sort(this->c.begin(), this->c.end(), this->comp);
  FINALLY(std::make_heap(this->c.begin(),
                         this->c.end(),
                         this->comp));
  auto it = std::find_if(this->c.rbegin(), this->c.rend(), predicate);
  optional<T> ret;
  if (it != this->c.rend()) {
    ret = std::move(*it);
    this->c.erase(it.base());
  }
  return ret;
}

template<typename... Args>
void
process(Args&&... args)
{
  namespace h = hana;

  auto initial_config = h::make_map(
      h::make_pair(config_inputs, h::make_tuple()),
      h::make_pair(config_before_dispatch_hooks, h::make_tuple()),
      h::make_pair(config_ts_batch_end_hooks, h::make_tuple()),
      h::make_pair(config_partition_end_hooks, h::make_tuple()),
      h::make_pair(config_partition_major_mode, h::false_c)
  );

  auto config = h::fold_left(
      h::make_tuple(AUTOFWD(args)...),
      initial_config,
      [&](auto&& config, auto&& arg) {
        if constexpr (
            CONSTEXPR_VALUE(h::typeid_(arg) ==
                            h::typeid_(partition_major_mode))) {
          return config_set(AUTOFWD(config),
                            config_partition_major_mode,
                            h::true_c);
        } else {  // NOLINT
          return arg(op_setup, AUTOFWD(config), AUTOFWD(arg));
        }
      });

  constexpr bool pmm = CONSTEXPR_VALUE(config[config_partition_major_mode]);

  constexpr bool have_ts_batch_end_hooks = CONSTEXPR_VALUE(
      !h::is_empty(config[config_ts_batch_end_hooks]));

  constexpr bool have_partition_end_hooks = CONSTEXPR_VALUE(
      !h::is_empty(config[config_partition_end_hooks]));

  static_assert(!have_partition_end_hooks || pmm,
                "using partition-end hooks requires partition major mode");

  constexpr bool track_last_ts = have_ts_batch_end_hooks;
  constexpr bool track_last_partition =
      pmm && (have_ts_batch_end_hooks || have_partition_end_hooks);

  static_assert(!have_partition_end_hooks || track_last_partition);
  static_assert(!have_ts_batch_end_hooks || track_last_ts);

  auto& handlers = config[config_inputs];

  auto payload_variant_type = h::unpack(
      h::transform(handlers,
                   [](auto&& handler) {
                     return handler(op_get_payload_type);
                   }),
      make_variant_type);

  auto handler_by_payload_type = h::unpack(
      h::transform(handlers,
                   [](auto&& handler) {
                     return h::make_pair(
                         handler(op_get_payload_type),
                         std::ref(handler));
                   }),
      h::make_map);

  using PayloadVariantType =
      typename decltype(payload_variant_type)::type;

  struct Event final {
    TimeStamp ts;
    PayloadVariantType payload;
  };

  auto get_event_partition = [&](const Event& event) -> Partition {
    if constexpr (pmm) {
      return event.payload.visit([&](const auto& payload) {
        return payload_get_field_by_tag(payload, partition);
      });
    } else {  // NOLINT
      return 0;
    }
  };

  auto event_lessp = [&](const Event& left, const Event& right) {
    if constexpr (pmm) {
      if (get_event_partition(left) < get_event_partition(right))
        return true;
      if (get_event_partition(left) > get_event_partition(right))
        return false;
    }
    if (left.ts < right.ts)
      return true;
    if (left.ts > right.ts)
      return false;
    if (left.payload.get_index() < right.payload.get_index())
      return true;
    if (left.payload.get_index() > right.payload.get_index())
      return false;
    assume(left.payload.get_index() == right.payload.get_index());
    return left.payload.visit([&](const auto& left_payload) {
      using Payload = std::decay_t<decltype(left_payload)>;
      const Payload* right_payload =
          right.payload.template get_if<Payload>();
      assume(right_payload);
      return static_cast<bool>(
          handler_by_payload_type[h::typeid_(left_payload)](
              op_tiebreak_order,
              left.ts,
              left_payload,
              right.ts,
              *right_payload));
    });
  };

  PriorityQueueEx<
    Event,
    Vector<Event>,
    InvertingComparator<decltype(event_lessp)>> events(event_lessp);

  auto enqueue_event = [&](TimeStamp ts, auto&& payload) {
    events.push(Event{ts, AUTOFWD(payload)});
  };

  auto dispatch_event = [&](auto&& ae, const Event& event) {
    event.payload.visit([&] (auto&& payload) {
      h::for_each(config[config_before_dispatch_hooks],
                  [&](auto&& hook) {
                    hook(ae, event.ts, payload);
                  });
      handler_by_payload_type[h::typeid_(payload)](
          op_dispatch, ae, event.ts, payload);
    });
  };

  auto dispatch_from_pending_events = [&](auto&& ae, auto&& predicate) {
    optional<Event> found_event = events.remove_one_if(
        [&](const Event& event) {
          return event.payload.visit([&](const auto& payload) {
            return predicate(event.ts, payload);
          });
        });
    if (found_event) {
      found_event->payload.visit(
          [&] (auto&& payload) {
            handler_by_payload_type[h::typeid_(payload)]
                (op_refill, ae, payload);
          });
      dispatch_event(AUTOFWD(ae), *found_event);
      return true;
    }
    return false;
  };

  auto is_partition_major_mode = [&]() {
    return h::bool_c<pmm>;
  };

  auto ae_ops = h::make_map(
      h::make_pair(op_enqueue_event, enqueue_event),
      h::make_pair(op_dispatch_from_pending_events,
                   dispatch_from_pending_events),
      h::make_pair(op_is_partition_major_mode,
                   is_partition_major_mode)
  );

  auto ae = [&](auto op, auto&&... args) {
    return ae_ops[op](AUTOFWD(args)...);
  };

  auto get_next_event = [&]() -> optional<Event> {
    if (events.empty())
      return {};
    Event event = events.pop_destructive();
    event.payload.visit([&] (auto&& payload) {
      handler_by_payload_type[h::typeid_(payload)](op_refill, ae, payload);
    });
    return event;
  };

  auto run_hooks = [&](auto&& config_field, auto&&... args) {
    h::for_each(config[AUTOFWD(config_field)],
                [&](auto&& hook) {hook(ae, args...);});
  };

  h::for_each(handlers, [&](auto&& handler) {
    handler(op_prime, ae);
  });

  optional<Event> event;
  optional<Partition> last_partition;
  optional<TimeStamp> last_ts;

  while ((event = get_next_event())) {
    if constexpr (track_last_partition) {
      static_assert(pmm);
      Partition p = get_event_partition(*event);
      if (last_partition && *last_partition != p) {
        // In partition-major mode, we may get timestamps out-of-order
        // (but always in-order within a single partition) so run the
        // end-ts-batch hooks ourselves here and pretend we're
        // starting a new run of events.
        assume(*last_partition <= p);
        if constexpr (track_last_ts) {
          assume(last_ts);
          run_hooks(config_ts_batch_end_hooks, *last_ts);
          last_ts = {};
        }
        run_hooks(config_partition_end_hooks);
      }
      last_partition = p;
    }
    if constexpr (track_last_ts) {
      if (last_ts && *last_ts != event->ts) {
        assume(*last_ts < event->ts);
        run_hooks(config_ts_batch_end_hooks, *last_ts);
      }
      last_ts = event->ts;
    }
    dispatch_event(ae, *event);
  }

  if constexpr (have_ts_batch_end_hooks) {
    static_assert(track_last_ts);
    if (last_ts)
      run_hooks(config_ts_batch_end_hooks, *last_ts);
  }

  if constexpr (have_partition_end_hooks) {
    static_assert(track_last_partition);
    if (last_partition)
      run_hooks(config_partition_end_hooks);
  }
}

template<typename Ae, typename Payload>
void
enqueue_event(Ae&& ae, TimeStamp ts, Payload payload)
{
  AUTOFWD(ae)(op_enqueue_event, ts, std::move(payload));
}

template<typename Ae, typename Predicate>
bool
dispatch_from_pending_events(Ae&& ae, Predicate&& predicate)
{
  return ae(op_dispatch_from_pending_events,
            ae, AUTOFWD(predicate));
}

template<typename Ae>
auto
is_partition_major_mode(Ae&& ae)
{
  return ae(op_is_partition_major_mode);
}

#define AUTOEVENT_FIELD_SPEC_TO_META(d, _data, field_spec)              \
  BOOST_PP_IIF(                                                         \
      BOOST_PP_GREATER_EQUAL(BOOST_PP_LIST_SIZE_D(d, field_spec), 3),   \
      BOOST_PP_LIST_AT_D(d, field_spec, 2),                             \
      auto_event::no_annotation)

#define AUTOEVENT_MAKE_METADATA(field_specs)                            \
  ::boost::hana::make_tuple(                                            \
       BOOST_PP_LIST_ENUM(                                              \
           BOOST_PP_LIST_TRANSFORM(                                     \
               AUTOEVENT_FIELD_SPEC_TO_META,                            \
               _,                                                       \
               field_specs)))

#define AUTOEVENT_FIELD_SPEC_TO_HANA_FIELD_SPEC(d, _data, field_spec)   \
  (BOOST_PP_LIST_AT_D(d, field_spec, 0),                                \
   BOOST_PP_LIST_AT_D(d, field_spec, 1))

// Don't mark the structure final: we inherit from it to
// make ExtendedPayload.  (It's a long story.)

#define AUTOEVENT_DECLARE_EVENT_1(struct_name, field_specs)             \
  struct struct_name {                                                  \
    static constexpr auto _make_autoevent_meta() {                      \
      return AUTOEVENT_MAKE_METADATA(field_specs);                      \
    }                                                                   \
    using payload_type = struct_name;                                   \
    BOOST_PP_EXPAND(                                                    \
        BOOST_HANA_DEFINE_STRUCT                                        \
        BOOST_PP_LIST_TO_TUPLE(                                         \
            BOOST_PP_LIST_CONS(                                         \
                struct_name,                                            \
                BOOST_PP_LIST_TRANSFORM(                                \
                    AUTOEVENT_FIELD_SPEC_TO_HANA_FIELD_SPEC,            \
                    _,                                                  \
                    field_specs))));                                    \
                                                                        \
  };                                                                    \
  static_assert(true, "force trailing semicolon")

#define AUTOEVENT_MUNGE_EVENT_FIELD_SPEC(_d, _data, field_spec)         \
  BOOST_PP_TUPLE_TO_LIST(field_spec)

#define AUTOEVENT_MUNGE_EVENT_FIELD_SPECS(field_specs)                  \
  BOOST_PP_LIST_TRANSFORM(                                              \
      AUTOEVENT_MUNGE_EVENT_FIELD_SPEC,                                 \
      _,                                                                \
      BOOST_PP_SEQ_TO_LIST(DCTV_WRAP_SEQ(field_specs)))

}  // namespace auto_event
}  // namespace dctv
