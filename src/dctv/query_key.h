// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <boost/operators.hpp>
#include <stddef.h>

#include "npy.h"
#include "pyutil.h"
#include "query.h"

namespace dctv {

struct QueryKey final : boost::totally_ordered<QueryKey> {
  // Build a reference to a QueryNode from Python
  inline static QueryKey from_py(pyref query_node);

  // Retrieve a reference to the raw Python object
  inline const unique_pyref& as_pyref() const noexcept;

  // Return a unique numeric key identifying this query.  N.B. since
  // over in Pythonland we intern QueryNode instances, this key is
  // just a memory address.
  inline uintptr_t get_sort_key() const noexcept;

  // Unlike Python references in general, we give this one a copy
  // constructor so that some algorithms are less painful to write.
  inline QueryKey(QueryKey&& other) noexcept = default;
  inline QueryKey& operator=(QueryKey&& other) noexcept = default;
  inline QueryKey(const QueryKey& other) noexcept;
  inline QueryKey& operator=(const QueryKey& other) noexcept;

  // Total ordering.
  inline bool operator==(const QueryKey& other) const noexcept;
  inline bool operator<(const QueryKey& other) const noexcept;

  // Read the numpy dtype from Python.
  unique_dtype get_dtype() const;

  // Return the offset of the Python field for the query node, useful
  // for member setup.
  static constexpr size_t get_query_node_offset();

  // GC support
  int py_traverse(visitproc visit, void* arg) const noexcept;

 private:
  inline explicit QueryKey(pyref query_node);
  unique_pyref query_node;
};

inline void init_query_key(pyref m) {}

}  // namespace dctv

#include "query_key-inl.h"
