// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"  // NOLINT

#include "hunk.h"
#include "npy.h"
#include "pyutil.h"

namespace dctv {

// Immutable unit of data that flows through the DCTV query system.
// Note that due to broadcasting, the logical size of a DCTV block may
// differ from the physical size of its constituent arrays.

struct Hunk;
struct QueryCache;

struct Block final : BasePyObject, SupportsGc, SupportsWeakRefs, HasRepr
{
  static unique_obj_pyref<Block> make_empty(
      QueryCache* qc,
      unique_dtype dtype);

  inline Block(unique_obj_pyref<Hunk> data,
               unique_obj_pyref<Hunk> mask /* nullable */);

  inline dtype_ref get_dtype() const noexcept;
  inline npy_intp get_size() const noexcept;
  inline bool has_mask() const noexcept;
  inline bool is_broadcasted() const noexcept;

  // Inflate on demand!
  inline unique_pyarray get_data() const;
  inline unique_pyarray get_mask() const;

  int py_traverse(visitproc visit, void* arg) const noexcept;

  unique_pyarray as_array(unique_dtype dtype = {}) const;

  static unique_obj_pyref<Block> from_arrays(
      QueryCache* qc,
      unique_pyarray data,
      unique_pyarray mask /* nullable */);

  inline Hunk* get_data_hunk();
  inline Hunk* /* nullable */ get_mask_hunk();

  // Description
  explicit operator String() const;

  static PyTypeObject pytype;
 private:
  unique_obj_pyref<Hunk> data;  // non-null
  unique_obj_pyref<Hunk> mask;  // nullptr means no mask!

  static PyMethodDef pymethods[];
  static PyGetSetDef pygetset[];
  static PySequenceMethods pyseq;
};

void init_block(pyref m);

}  // namespace dctv

#include "block-inl.h"
