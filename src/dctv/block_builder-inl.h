// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

bool
BlockBuilder::is_eof() const noexcept
{
  return this->block_size == 0;
}

constexpr
size_t
BlockBuilder::get_dtype_offset()
{
  return offsetof(BlockBuilder, dtype) + unique_pyref::get_pyval_offset();
}

const unique_dtype&
BlockBuilder::get_dtype() const noexcept
{
  return this->dtype;
}

npy_intp
BlockBuilder::get_block_size() const noexcept
{
  return this->block_size;
}

npy_intp
BlockBuilder::get_partial_size() const noexcept
{
  return this->partial_size;
}

}  // namespace dctv
