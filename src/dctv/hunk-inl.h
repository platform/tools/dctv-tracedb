// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

bool
Hunk::CoreBase::is_broadcasted(const Hunk*) const noexcept
{
  return false;
}

pyarray_ref
Hunk::CoreBase::get_broadcast_base(const Hunk*) const noexcept
{
  return {};
}

void
Hunk::CoreBase::on_unpin(const Hunk*) const noexcept
{}

void
Hunk::CoreBase::on_core_detached(QueryCache* /*qc*/) noexcept
{}

bool
Hunk::CoreBase::is_composite(const Hunk*) const noexcept
{
  return false;
}

npy_intp
Hunk::MmapCoreBase::get_size(const Hunk*) const noexcept
{
  return this->nelem;
}

dtype_ref
Hunk::MmapCoreBase::get_dtype(const Hunk*) const noexcept
{
  return this->dtype;
}

const char*
Hunk::MmapCore::get_core_name(const Hunk*) const noexcept
{
  return "mmap";
}

const char*
Hunk::MmapCoreWithSpill::get_core_name(const Hunk*) const noexcept
{
  return "mmap_spilled";
}

bool
Hunk::MegaCore::is_composite(const Hunk*) const noexcept
{
  return true;
}

const char*
Hunk::MegaCore::get_core_name(const Hunk*) const noexcept
{
  return "mega";
}

pyarray_ref
Hunk::NumpyCoreBase::get_broadcast_base(const Hunk*) const noexcept
{
  if (npy_size1d(this->backing_store) == 1 &&
      (npy_flags(this->backing_store) & NPY_ARRAY_OWNDATA))
    return this->backing_store.addref();
  return {};
}

npy_intp
Hunk::NumpyCoreBase::get_size(const Hunk*) const noexcept
{
  return npy_size1d(this->backing_store);
}

dtype_ref
Hunk::NumpyCoreBase::get_dtype(const Hunk*) const noexcept
{
  return npy_dtype(this->backing_store);
}

const char*
Hunk::NumpyCore::get_core_name(const Hunk*) const noexcept
{
  return "numpy";
}

const char*
Hunk::NumpyCoreWithSpill::get_core_name(const Hunk*) const noexcept
{
  return "numpy_spilled";
}

pyarray_ref
Hunk::BroadcastCore::get_broadcast_base(const Hunk*) const noexcept
{
  return this->broadcaster.addref();
}

npy_intp
Hunk::BroadcastCore::get_size(const Hunk*) const noexcept
{
  return this->nelem;
}

dtype_ref
Hunk::BroadcastCore::get_dtype(const Hunk*) const noexcept
{
  return npy_dtype(this->broadcaster);
}

const char*
Hunk::BroadcastCore::get_core_name(const Hunk*) const noexcept
{
  return "broadcast";
}

npy_intp
Hunk::SubCore::get_size(const Hunk*) const noexcept
{
  return this->nelem;
}

dtype_ref
Hunk::SubCore::get_dtype(const Hunk*) const noexcept
{
  return this->base->get_dtype();
}

const char*
Hunk::SubCore::get_core_name(const Hunk*) const noexcept
{
  return "sub";
}

npy_intp
Hunk::SpilledCore::get_size(const Hunk*) const noexcept
{
  return this->nelem;
}

dtype_ref
Hunk::SpilledCore::get_dtype(const Hunk*) const noexcept
{
  return this->dtype;
}

const char*
Hunk::SpilledCore::get_core_name(const Hunk*) const noexcept
{
  return "spilled";
}

dtype_ref
Hunk::get_dtype() const noexcept
{
  return this->core.get_dtype(this).notnull();
}

npy_intp
Hunk::get_size() const noexcept
{
  return this->core.get_size(this);
}

bool
Hunk::is_broadcasted() const noexcept
{
  return this->core.is_broadcasted(this);
}

void
Hunk::copy_or_pin(CopyOrPin* cop, npy_intp offset, npy_intp nelem)
{
  this->core.copy_or_pin(this, cop, offset, nelem);
}

bool
Hunk::is_composite() const noexcept
{
  return this->core.is_composite(this);
}

void
Hunk::enumerate_children(const WalkCb& cb) const
{
  this->core.enumerate_children(this, cb);
}

void
Hunk::freeze()
{
  if (!this->frozen)
    this->frozen = true;
}

bool
Hunk::is_frozen() const noexcept
{
  return this->frozen;
}

bool
Hunk::is_pinned() const noexcept
{
  return this->pin;
}

unique_hunk
Hunk::view(npy_intp low, npy_intp high) const
{
  assume(0 <= low && low <= high);
  assume(high <= this->get_size());
  if (low == high)
    return Hunk::make_empty(this->qc.get(),
                            this->get_dtype().addref());
  if (low == 0 && high == this->get_size())
    return xaddref(this);
  unique_hunk ret = this->core.view(this, low, high);
  assert(ret->get_size() == high - low);
  return ret;
}

Hunk::ResourceUse
Hunk::get_resource_use() const noexcept
{
  ResourceUse mu;
  this->core.add_resource_use(this, &mu);
  return mu;
}

unique_hunk
Hunk::from_core(QueryCache* qc, Core core)
{
  assume(qc);
  bool success = false;
  FINALLY(if (!success) core.on_core_detached(qc));
  unique_hunk hunk = make_pyobj<Hunk>(qc, std::move(core));
  success = true;
  return hunk;
}

HunkReblocker::HunkReblocker(npy_intp low, npy_intp high)
{
  assume(0 <= low);
  assume(low <= high);
  this->skip = low;
  this->nelem = high - low;
}

npy_intp
HunkReblocker::nelem_remaining() const noexcept
{
  return this->nelem;
}

bool
HunkReblocker::done() const noexcept
{
  return this->nelem_remaining() == 0;
}



HunkExtraDebug::HunkExtraDebug(double new_hunk_score_override) noexcept
    : score_override(new_hunk_score_override)
{}

double
HunkExtraDebug::get_score_override() const noexcept
{
  return this->score_override;
}

HunkExtraOpt::HunkExtraOpt(double new_hunk_score_override) noexcept
{
  assert(isnan(new_hunk_score_override));
}

double
HunkExtraOpt::get_score_override() const noexcept
{
  return NAN;
}

}  // namespace dctv
