# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""ipython integration for dctv"""
import importlib
import os
import sys
from types import ModuleType, FunctionType

from IPython.core.interactiveshell import DummyMod
from IPython.core.magic import Magics, magics_class, line_magic

from .iddict import (WeakKeyIdentityDictionary,
                     IdentitySet,
                     IdentityDictionary)

_NO_RELOAD = True
magics = None

weak_obj_to_path = WeakKeyIdentityDictionary()  # pylint: disable=invalid-name

def dctv_p(name):
  """Return whether NAME refers to a dctv module"""
  assert isinstance(name, str), "not a string?! {!r}".format(name)
  return name == "dctv" or name.startswith("dctv.")

def dctv_module_p(module):
  """Return whether MODULE refers to a dctv module"""
  return hasattr(module, "__name__") and \
    dctv_p(module.__name__) and \
    not getattr(module, "_NO_RELOAD", False)

def gen_objects(obj, examined=None):
  """Generate all objects reachable from OBJ"""
  if examined is None:
    examined = IdentitySet()
  if obj not in examined:
    examined.add(obj)
    yield obj
    if (isinstance(obj, type) or
        (isinstance(obj, ModuleType) and dctv_module_p(obj))):
      for value in obj.__dict__.values():
        yield from gen_objects(value, examined)

def resolve_path(path):
  """Find object by path"""
  try:
    obj = importlib.import_module(path[0])
  except ImportError:
    return None, False
  for attr in path[1]:
    try:
      obj = getattr(obj, attr)
    except AttributeError:
      return None, False
  return obj, True

def transmute_type(old_cls, new_cls):
  """Attempt to change object type in-place"""
  old_keys = set(old_cls.__dict__.keys()) - {"__dict__"}
  new_keys = set(new_cls.__dict__.keys()) - {"__dict__"}
  for key in new_keys:
    has_attr = hasattr(old_cls, key)
    old_value = has_attr and getattr(old_cls, key)
    new_value = getattr(new_cls, key)
    if (not has_attr) or (old_value is not new_value):
      type.__setattr__(old_cls, key, new_value)
  for key in old_keys - new_keys:
    type.__delattr__(old_cls, key)

def transmute_function(old_fn, new_fn):
  """Attempt to change function in-place"""
  for attr in ("__code__", "__defaults__", "__doc__",
               "__closure__", "__globals__", "__dict__"):
    try:
      setattr(old_fn, attr, getattr(new_fn, attr))
    except (AttributeError, TypeError):
      pass

def maybe_transmute(obj):
  """Transmute object if possible"""
  path = (isinstance(obj, (type, FunctionType)) and
          weak_obj_to_path.get(obj))
  if path:
    new_obj, new_obj_found = resolve_path(path)
    if not new_obj_found:
      pass
    if isinstance(obj, type) and isinstance(new_obj, type):
      transmute_type(obj, new_obj)
    elif (isinstance(obj, FunctionType) and
          isinstance(new_obj, FunctionType)):
      transmute_function(obj, new_obj)
  else:
    type_path = weak_obj_to_path.get(type(obj))
    if type_path is not None:
      new_type, new_type_found = resolve_path(type_path)
      if new_type_found:
        object.__setattr__(obj, "__class__", new_type)
        clear_cache_method = getattr(obj, "__reload_clear_cache__", None)
        if clear_cache_method is not None:
          clear_cache_method()

def fix_ns(ns):
  """Fix up stale references in NS and transmute objects inside"""
  # pylint: disable=unnecessary-lambda
  if isinstance(ns, dict):
    items = list(ns.items())
    replace_fn = lambda k, v: ns.__setitem__(k, v)
    remove_fn = lambda k: ns.__delitem__(k)
  elif isinstance(ns, (type, ModuleType, DummyMod)):
    items = list(ns.__dict__.items())
    replace_fn = lambda k, v: setattr(ns, k, v)
    remove_fn = lambda k: delattr(ns, k)
  else:
    assert False, "unrecognized ns type"
  for name, old_obj in items:
    path = weak_obj_to_path.get(old_obj)
    if path:
      new_obj, new_obj_found = resolve_path(path)
      if new_obj_found:
        replace_fn(name, new_obj)
      else:
        remove_fn(name)
    else:
      maybe_transmute(old_obj)

def scan_modules():
  """Learn dctv modules"""
  dctv_modules = {}
  other_modules = {}
  for n, m in tuple(sys.modules.items()):  # pylint: disable=invalid-name
    if dctv_module_p(m):
      dctv_modules[n] = m
    else:
      other_modules[n] = m
  return dctv_modules, other_modules

def make_replace(roots):
  """Make the replacement set for a reload"""
  replace = IdentityDictionary()
  for root in roots:
    for obj in gen_objects(root):
      if obj not in replace:
        path = None
        if isinstance(obj, ModuleType):
          path = (obj.__name__, [])
        elif isinstance(obj, (type, FunctionType)):
          path = (obj.__module__, obj.__qualname__.split("."))
        if path and path[0] and dctv_p(path[0]):
          replace[obj] = path
  return replace

def _reload_dctv(addl_fixups=()):
  """Like autoreload's superreload, but reloads whole package"""

  old_dctv_modules, new_modules = scan_modules()
  replace = make_replace(old_dctv_modules.values())
  for obj, path in replace.items():
    weak_obj_to_path[obj] = path

  importlib.invalidate_caches()
  sys.modules.clear()
  sys.modules.update(new_modules)

  # Replace references to old objects with references to new ones in
  # scopes that we know about.
  for old_dctv_module in old_dctv_modules.values():
    fix_ns(old_dctv_module)
  for thing in addl_fixups:
    fix_ns(thing)

  # Transmute old object instances (may not work, but YOLO).
  for _, old_obj in replace.values():
    maybe_transmute(old_obj)
  import gc
  for obj in gc.get_objects():
    maybe_transmute(obj)

@magics_class
class DctvMagics(Magics):
  """dctv magics registration"""
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__known_files = {}
    self.__auto_reload = True

  @line_magic
  def dctv_autoreload(self, arg=""):
    """Configure dctv autoreload"""
    if arg == "":  # pylint: disable=compare-to-empty-string
      pass
    elif arg == "0":
      self.__auto_reload = False
      self.__known_files.clear()
      print("Auto reload:", self.__auto_reload)
    elif arg == "1":
      self.__auto_reload = True
      print("Auto reload:", self.__auto_reload)
    elif arg == "dump":
      print(self.__known_files)
    elif arg == "now":
      self.__do_reload()
    else:
      raise ValueError("unknown command " + arg)

  @staticmethod
  def __get_modtime(file_name):
    try:
      return os.stat(file_name).st_mtime
    except OSError:
      return None

  def scan_for_new_modules(self):
    """Scan for new modules"""
    for _name, module in tuple(sys.modules.items()):
      if (dctv_module_p(module) and
          module.__file__ not in self.__known_files):
        self.refresh_module_file(module)

  def refresh_module_file(self, module):
    """Re-learn the modtime of a module's backing file"""
    file_name = module.__file__
    modtime = self.__get_modtime(file_name)
    self.__known_files[file_name] = modtime

  def refresh_module_files(self):
    """Reset timestamps on known files"""
    for file_name in list(self.__known_files):
      modtime = self.__get_modtime(file_name)
      if modtime is None:
        del self.__known_files[file_name]
      else:
        self.__known_files[file_name] = modtime

  def __find_changed_files(self):
    changed_files = []
    for file_name, last_modtime in self.__known_files.items():
      current_modtime = self.__get_modtime(file_name)
      if last_modtime != current_modtime:
        changed_files.append(file_name)
    return changed_files

  def __do_reload(self):
    print("Reloading DCTV")
    self.refresh_module_files()
    _reload_dctv([self.shell.user_module])
    self.scan_for_new_modules()

  def on_pre_command(self):
    """Run before a user command"""
    if self.__auto_reload:
      self.scan_for_new_modules()
      changed_files = self.__find_changed_files()
      if changed_files:
        print("DCTV files changed: {!r}".format(sorted(changed_files)))
        self.__do_reload()

  def on_post_command(self):
    """Run after a user command"""
    if self.__auto_reload:
      self.scan_for_new_modules()

def load_ipython_extension(ip):
  """ipython entry point"""
  global magics
  magics = DctvMagics(ip)
  magics.scan_for_new_modules()
  ip.register_magics(magics)
  ip.events.register("pre_run_cell", magics.on_pre_command)
  ip.events.register("post_run_cell", magics.on_post_command)
