// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "qe_call_setup.h"

#include <algorithm>
#include <iterator>

#include "input_channel.h"
#include "operator_context.h"
#include "output_channel.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyparsetuple.h"
#include "pyseq.h"
#include "query_execution.h"
#include "query_key.h"
#include "vector.h"

namespace dctv {

struct QeCallSetup final : QeCall {

  using InputSpecList = Vector<unique_obj_pyref<InputChannelSpec>>;
  using OutputSpecList = Vector<unique_obj_pyref<OutputChannelSpec>>;

  QeCallSetup(InputSpecList input_specs, OutputSpecList output_specs);
  unique_pyref do_it(OperatorContext* oc) override;
  Score compute_score(const OperatorContext* oc) const override;
  void do_operator_setup(QueryExecution* qe,
                         OperatorContext* oc,
                         QueryDb* query_db) override;
  int py_traverse(visitproc visit, void* arg) const noexcept override;
 private:
  InputSpecList input_specs;
  OutputSpecList output_specs;
};

struct QueryUsageValidator {
  QueryUsageValidator(const char* name, Vector<QueryKey> declared);
  void note_used(const QueryKey& query);
  void validate() const;
 private:
  const char* name;
  Vector<QueryKey> available;
  mutable Vector<QueryKey> seen;
};

QeCallSetup::QeCallSetup(
    InputSpecList input_specs,
    OutputSpecList output_specs)
    : input_specs(std::move(input_specs)),
      output_specs(std::move(output_specs))
{}

unique_pyref
QeCallSetup::do_it(OperatorContext* /*oc*/)
{
  throw_pyerr_msg(PyExc_RuntimeError, "cannot run setup normally");
}

Score
QeCallSetup::compute_score(const OperatorContext* oc) const
{
  Score score = oc->compute_basic_score();
  score.state = OperatorState::YIELDING;
  return score;
}

void
QeCallSetup::do_operator_setup(QueryExecution* qe,
                               OperatorContext* oc,
                               QueryDb* query_db)
{
  // N.B. we create the channels here, but defer wiring them up until
  // we're examined the whole query set.

  assume(qe);
  assume(oc->link_qe == nullptr);
  assume(oc->link_state == OperatorContext::LinkState::ORPHANED);

  QueryCache* qc = qe->get_qc();
  assume(qc);

  auto make_input_channels = [&] {
    QueryUsageValidator validator("input", oc->get_declared_inputs());
    assert(oc->input_channels.empty());
    const size_t nr_input_channels = this->input_specs.size();
    Vector<unique_pyref> ret_input_channels;
    oc->input_channels.reserve(nr_input_channels);
    ret_input_channels.reserve(nr_input_channels);
    for (size_t i = 0; i < nr_input_channels; ++i) {
      const InputChannelSpec& spec = *this->input_specs[i];
      const QueryKey& query = spec.query;
      auto source_it = query_db->producers.find(query);
      if (source_it == query_db->producers.end())
        throw_pyerr_fmt(PyExc_AssertionError,
                        "no query produces %s needed by %s",
                        repr(query.as_pyref()),
                        repr(oc->get_query_action()));
      auto input_channel = make_pyobj<InputChannel>(
          oc, spec, source_it->second);
      validator.note_used(input_channel->query);
      ret_input_channels.push_back(addref(input_channel));
      oc->input_channels.push_back(std::move(input_channel));
    }
    validator.validate();
    return ret_input_channels;
  };

  auto make_output_channels = [&] {
    QueryUsageValidator validator("output", oc->get_declared_outputs());
    assert(oc->output_channels.empty());
    const size_t nr_output_channels = this->output_specs.size();
    Vector<unique_pyref> ret_output_channels;
    oc->output_channels.reserve(nr_output_channels);
    ret_output_channels.reserve(nr_output_channels);
    for (size_t i = 0; i < nr_output_channels; ++i) {
      auto output_channel = make_pyobj<OutputChannel>(
          oc, qc, *this->output_specs[i]);
      auto [it, inserted] = query_db->producers.insert(
          {output_channel->query, output_channel.get()});
      if (!inserted) {
        // TODO(dancol): teach python query planning not to tolerate
        // redundant computation.
        throw_pyerr_msg(PyExc_AssertionError, "redundant query production");
      }
      validator.note_used(output_channel->query);
      ret_output_channels.push_back(addref(output_channel));
      oc->output_channels.push_back(std::move(output_channel));
    }
    validator.validate();
    return ret_output_channels;
  };

  // "Return" a list instead of a tuple so we don't fall down the slow
  // path of Python iterator send.
  oc->communicate(pylist::of(pytuple::from(make_input_channels()),
                             pytuple::from(make_output_channels())));
}

int
QeCallSetup::py_traverse(visitproc visit, void* arg) const noexcept
{
  for (auto& item : this->input_specs)
    Py_VISIT(item.get());  // NOLINT
  for (auto& item : this->output_specs)
    Py_VISIT(item.get());  // NOLINT
  return 0;
}

static
String
fmt_queries(const Vector<QueryKey>& queries)
{
  return repr(pylist::from(queries,
                           [](const QueryKey& query) {
                             return addref(query.as_pyref());
                           }));
}

QueryUsageValidator::QueryUsageValidator(const char* name,
                                         Vector<QueryKey> declared)
    : name(name), available(std::move(declared))
{
  std::sort(this->available.begin(), this->available.end());
  auto last_it = std::unique(this->available.begin(), this->available.end());
  if (last_it != this->available.end()) {
    Vector<QueryKey> dups(last_it, this->available.end());
    throw_pyerr_fmt(PyExc_AssertionError,
                    "duplicate declared %s queries: %s",
                    this->name, fmt_queries(dups));
  }
}

void
QueryUsageValidator::note_used(const QueryKey& query)
{
  this->seen.push_back(query);
}

void
QueryUsageValidator::validate() const
{
  std::sort(this->seen.begin(), this->seen.end());
  auto last_it = std::unique(this->seen.begin(), this->seen.end());
  this->seen.erase(last_it, this->seen.end());

  Vector<QueryKey> not_used;
  std::set_difference(this->available.begin(), this->available.end(),
                      this->seen.begin(), this->seen.end(),
                      std::back_inserter(not_used));
  if (!not_used.empty())
    throw_pyerr_fmt(PyExc_AssertionError,
                    "unused %s queries: %s",
                    this->name, fmt_queries(not_used));

  Vector<QueryKey> not_supplied;
  std::set_difference(this->seen.begin(), this->seen.end(),
                      this->available.begin(), this->available.end(),
                      std::back_inserter(not_supplied));
  if (!not_supplied.empty())
    throw_pyerr_fmt(PyExc_AssertionError,
                    "undeclared %s queries: %s",
                    this->name,
                    fmt_queries(not_supplied));
}

unique_pyref
make_qe_call_setup(pytuple::ref args)
{
  PARSEPYARGS(
      (pyref, inputs)
      (pyref, outputs)
  )(args);
  return make_qe_call(
      std::make_unique<QeCallSetup>(
          py2vec_fast(inputs, InputChannelSpec::from_py),
          py2vec_fast(outputs, OutputChannelSpec::from_py)));
}

}  // namespace dctv
