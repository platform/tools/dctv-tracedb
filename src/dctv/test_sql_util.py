# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for SQL utilities"""

# pylint: disable=missing-docstring

import logging
import numpy as np
from .sql_util import SqlBundle

log = logging.getLogger(__name__)

def test_sql_bundle_lex():
  assert SqlBundle.lex("hello world") == ["hello world"]
  assert SqlBundle.lex("hello :foo bar") == ["hello ", ":foo", " bar"]
  assert SqlBundle.lex("hello :foo") == ["hello ", ":foo"]
  assert SqlBundle.lex(":foo bar") == [":foo", " bar"]
  assert SqlBundle.lex(":foo") == [":foo"]
  assert SqlBundle.lex("") == []

def test_sql_bundle_format():
  assert SqlBundle.format("hello").sql == "hello"
  foo = SqlBundle.format("foo :bar", bar=5)
  assert foo.sql == "foo :bar"
  assert dict(foo.args) == {"bar": 5}
  bar = SqlBundle.format(":foo :qux", foo=foo, qux=7)
  assert bar.sql == "foo :bar :qux"
  assert dict(bar.args) == {"bar": 5, "qux": 7}

def test_sql_bundle_collision():
  foo = SqlBundle.format("foo :bar", bar=5)
  spam = SqlBundle.format(":foo :bar", foo=foo, bar=7)
  assert spam.sql == "foo :bar :bar_"
  assert dict(spam.args) == {"bar": 5, "bar_": 7}

def test_sql_bundle_canonicalize():
  foo = SqlBundle.format("word1 :arg", arg=SqlBundle.of("word2"))
  bar = SqlBundle.format("word1 word2")
  assert foo.sql == bar.sql
  assert foo is bar

def test_sql_bundle_numpy_literals():
  assert SqlBundle.format(":foo", foo=np.int32(5)) is \
    SqlBundle.format(":foo", foo=5)
  assert SqlBundle.format(":foo", foo=np.float(7.1)) is \
    SqlBundle.format(":foo", foo=7.1)
