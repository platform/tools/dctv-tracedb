// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "pyseq.h"

namespace dctv {

template<typename Sequence>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::make_uninit(size_t sz)
{
  if (sz > std::numeric_limits<Py_ssize_t>::max())
    throw_pyerr_msg(PyExc_OverflowError, "too long");
  using Constructor = PyObject*(*)(Py_ssize_t);
  Constructor constructor;
  if constexpr(std::is_same_v<Sequence, PyTupleObject>) {
    constructor = PyTuple_New;
  } else if constexpr(std::is_same_v<Sequence, PyListObject>) {  // NOLINT
    constructor = PyList_New;
  } else {  // NOLINT
    constructor = typename errhack<Sequence>::bad();
  }
  return adopt_check_as_unsafe<Sequence>(
      constructor(static_cast<Py_ssize_t>(sz)));
}

template pyseq<PyTupleObject>::unique_ref
pyseq<PyTupleObject>::make_uninit(size_t);
template pyseq<PyListObject>::unique_ref
pyseq<PyListObject>::make_uninit(size_t);

}  // namespace dctv
