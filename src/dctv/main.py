# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Entry point for application"""
import argparse
import logging
import sys
import os
from modernmp.util import ignore
from .util import partition

# pylint gets confused by the command argument stuff and thinks it's
# looking at docstrings.

# pylint: disable=bad-docstring-quotes

log = logging.getLogger(__name__)

p = argparse.ArgumentParser(description="dancol's trace viewer")
p.add_argument("--debug",
               action="store_true",
               help="Enable debug logging")
p.add_argument("--profile",
               metavar="OUTPUT",
               help="Use cProfile to time program; write to OUTPUT")
_subp = p.add_subparsers(help="sub-command help")  # pylint:disable=invalid-name

class add_argument(tuple):  # pylint: disable=invalid-name
  """Argument specification for subcommand decorator"""
  def __new__(cls, *args, **kwargs):
    return tuple.__new__(cls, (args, kwargs))
  __slots__ = ()

def subcommand(*args, default=False, **kwargs):
  """Decorator for a subcommand main function"""
  sa_arglist, args = partition(lambda a: isinstance(a, add_argument), args)
  def _decorator(fn):
    subcommand_name = fn.__name__.replace("_", "-")
    fn_p = _subp.add_parser(subcommand_name, *args, **kwargs)
    for sa_args, sa_kwargs in sa_arglist:
      fn_p.add_argument(*sa_args, **sa_kwargs)
    fn_p.set_defaults(func=fn)
    if default:
      p.default_subcommand = subcommand_name
    return fn
  return _decorator

def _threads_argument():
  return add_argument(
    "--threads",
    type=lambda arg: {"no": False, "yes": True}[arg],
    default=True,
    help="Use threads for certain query operations (default=yes)",
  )

@subcommand(
  add_argument(
    "trace_file_names",
    default=[],
    metavar="TRACE",
    nargs="*",
    help="Trace file to view",
  ),
  default=True,
)
def gui(args):
  """Run the GUI"""
  from .gui import run_gui
  run_gui(initial_trace_file_names=args.trace_file_names)

@subcommand(
  _threads_argument(),
  add_argument(
    "trace_mounts",
    metavar="MOUNT=TRACE_FILE",
    nargs="*",
    help="Trace file mount points",
  ),
  add_argument(
    "--drawing",
    choices=["auto", "fancy", "ascii"],
    default="auto",
    help="Line-drawing style: ascii or fancy: default is 'auto', "
    "which means fancy if TERM isn't 'dumb' and "
    "standard output is a TTY.",
  ),
)
def repl(args):
  """Run a SQL repl"""
  from .repl import run_repl
  return run_repl(args)

@subcommand(
  add_argument(
    "--absolute-time",
    dest="absolute_time",
    action="store_true",
    help="Timestamps in queries match trace exactly"),
  add_argument(
    "--relative-time",
    dest="absolute_time",
    action="store_false",
    help="Timestamps in queries are relative from the trace start"),
  add_argument(
    "trace_file_name",
    metavar="TRACE",
    help="Trace file to open"),
  add_argument(
    "filter",
    metavar="FILTER",
    help="SQL expression to match against events"),
)
def subset(args):
  """Extract part of a trace matching FILTER

FILTER is a SQL expression matched against each trace schema.
  """
  from .subset import run_subset
  return run_subset(
    trace_file_name=args.trace_file_name,
    filter_expression=args.filter,
    absolute_time=args.absolute_time,
  )

@subcommand(
  _threads_argument(),
  add_argument(
    "--format",
    metavar="FORMAT",
    default="json_records",
    help="Output format: one of json_records, json_columns, "
    "csv_rows,none [default json_records]",
  ),
  add_argument(
    "query",
    metavar="QUERY",
    help="DCTV-SQL query to execute",
  ),
  add_argument(
    "trace_mounts",
    metavar="MOUNT=TRACE_FILE",
    nargs="*",
    help="Trace file mount points",
  ),
)
def query(args):
  """Run a DCTV query and report results in machine-readable format"""
  from .cmd_query import run_query
  trace_mounts = []
  for mount_directive in args.trace_mounts:
    if "=" not in mount_directive:
      raise ValueError("invald mount directive: {!r}"
                       .format(mount_directive))
    path_str, trace_file_name = mount_directive.split("=", 1)
    path = path_str.split(".")
    trace_mounts.append((path, trace_file_name))
  return run_query(
    query=args.query,
    trace_mounts=trace_mounts,
    output_format=args.format,
    threads=args.threads,
  )

@subcommand()
def profile_qp(_args):
  """Internal gadget for profiling the query planner"""
  from dctv.test_queryplan import test_query_planner_greater_torture
  test_query_planner_greater_torture()

@subcommand()
def internal_regenerate_sql_parser(_args):
  """Internal command to regenerate SQL parser code"""
  from .sql_parser import regenerate_sql_parser as rsp
  rsp()

@subcommand()
def internal_dump_keywords(_args):
  """Internal command to dump all DCTV keywords"""
  from .sql_parser import dump_keywords
  dump_keywords()

@subcommand()
def generate_ebnf(_args):
  """Generate EBNF for SQL parser"""
  from .sql_parser import generate_ebnf as _generate_ebnf
  _generate_ebnf()

@subcommand(
  add_argument("-f", "--force",
               action="store_true",
               help="Overwrite destination file if it exists"),
  add_argument("-o", "--output",
               default="dctv.trace",
               help="Name of file to write: default is dctv.trace"),
  add_argument("-a", "--apps",
               help="Comma-separated list of applications in which we "
               "enable tracing; '*' means all applications",
               default=","),
  add_argument("--list-categories",
               action="store_true",
               help="List available categories and exit"),
  add_argument("categories",
               nargs="*",
               help="List of atrace categories to enable"),
)
def collect(args):
  """Collect a DCTV trace via ADB"""
  from .collect import run_collect
  return run_collect(args)

@subcommand(
  add_argument("--debug-angr",
               action="store_true",
               help="Enable additional debugging for angr"),
  add_argument("--load-dependencies",
               action="store_true",
               help="Load additional libraries"),
  add_argument("--veritesting",
               action="store_true",
               help="Use an optimization techqniue"),
  add_argument("--demangle",
               help="[yes, no] whether to demangle symbols (default=yes)",
               default="yes"),
  add_argument("dso",
               metavar="DSO",
               help="Shared library to analyze"),
  )
def dso_scan(args):
  """Analyze a shared library (a DSO) and report about its init costs"""
  from .dso_scan import analyze_dso
  return analyze_dso(args)

def _global_except_hook(*args):
  sys.__excepthook__(*args)
  os._exit(1)  # pylint: disable=protected-access

def main(argv):
  """Entry point"""
  # Make background threads kill the process if they fail
  sys.excepthook = _global_except_hook

  args = p.parse_args(argv)
  if not hasattr(args, "func"):
    args = p.parse_args(argv + [p.default_subcommand])

  logging.basicConfig(
    level=logging.DEBUG if args.debug else logging.INFO,
    format="%(process)d/%(threadName)s %(levelname)s:%(name)s:%(message)s")

  if args.profile:
    from .util import collect_profile
    collect_profile(lambda: args.func(args), args.profile)
    return 0
  try:
    return args.func(args)
  except BrokenPipeError:
    try:
      sys.stdout.flush()
    except BrokenPipeError:
      log.debug("suppressing BrokenPipeError")
    else:
      raise
    sys.stdout.flush = ignore
    return 1
