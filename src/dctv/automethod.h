// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <functional>
#include <tuple>

#include "meta_util.h"
#include "pyerr.h"
#include "pyobj.h"
#include "pyparsetuple.h"

namespace dctv {

// Make a PyMethodDef that automatically parses an argument list and
// calls the given function.  This facility is very useful when we
// want to expose a C++ member function to Python without creating an
// explicit Python-specific wrapper.  You can always use
// make_methoddef and PARSEPYARGS directly in cases where AUTOMETHOD
// doesn't do the right thing.

// AUTOMETHOD is in its own header so that PARSEPYARGS doesn't have to
// depend on pyobj.h.

// TODO(dancol): provide way to opt out of keyword argument support?

enum class AutoMethodKind {
  METHOD,
  FUNCTION,
};

template<typename ArgSpec, auto Fn, AutoMethodKind Kind>
constexpr PyMethodDef automethod(const char* name, const char* doc);

template<typename T>
inline PyObject* retval_to_py(unique_obj_pyref<T> ref) noexcept;

inline PyObject* retval_to_py(bool value) noexcept;

inline PyObject* retval_to_py(int value);
inline PyObject* retval_to_py(unsigned int value);
inline PyObject* retval_to_py(long value);
inline PyObject* retval_to_py(unsigned long value);
inline PyObject* retval_to_py(long long value);
inline PyObject* retval_to_py(unsigned long long value);

inline PyObject* retval_to_py(const String& s);
inline PyObject* retval_to_py(float value);
inline PyObject* retval_to_py(double value);

#define AUTOMETHOD(...)                                                 \
  BOOST_PP_OVERLOAD(AUTOMETHOD_,__VA_ARGS__)(                           \
      ::dctv::AutoMethodKind::METHOD, __VA_ARGS__)

#define AUTOFUNCTION(...)                                               \
  BOOST_PP_OVERLOAD(AUTOMETHOD_,__VA_ARGS__)(                           \
      ::dctv::AutoMethodKind::FUNCTION, __VA_ARGS__)

#define AUTOMETHOD_3(kind, fn, doc, arg_specs)                          \
  []() constexpr {                                                      \
    PARSEPYARGS_DEFINE_ARGS_STRUCT(                                     \
        am_args,                                                        \
        PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs));                        \
    AUTOMETHOD_DEFINE_META_MAKER(                                       \
        MetaMaker,                                                      \
        PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs));                        \
    return ::dctv::automethod                                           \
        <am_args, fn, kind, MetaMaker>(                                 \
            BOOST_HANA_STRING(#fn),                                     \
            BOOST_HANA_STRING(doc));                                    \
  }()

#define AUTOMETHOD_4(kind, fn, name, doc, arg_specs)                    \
  []() constexpr {                                                      \
    PARSEPYARGS_DEFINE_ARGS_STRUCT(                                     \
        am_args,                                                        \
        PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs));                        \
    AUTOMETHOD_DEFINE_META_MAKER(                                       \
        MetaMaker,                                                      \
        PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs));                        \
    return ::dctv::automethod                                           \
        <am_args, fn, kind, MetaMaker>(                                 \
        BOOST_HANA_STRING(name),                                        \
        BOOST_HANA_STRING(doc));                                        \
  }()

}  // namespace dctv

#include "automethod-inl.h"
