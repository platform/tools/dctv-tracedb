// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<std::size_t N>
struct ArmNumberBase<N, typename std::enable_if_t<(N == 1)>> {
  // Empty base optimization: avoid storing a type field when we can
  // store only one type.
  constexpr std::size_t get_index() const noexcept { return 0; }
 protected:
  explicit constexpr ArmNumberBase(std::size_t) noexcept {}
  void set_index(std::size_t index) noexcept {
    assume(index == 0);
  }
};

template<std::size_t N>
struct ArmNumberBase<N, typename std::enable_if_t<(N > 1)>> {
  constexpr std::size_t get_index() const noexcept {
    return static_cast<std::size_t>(this->index);
  }
 protected:
  explicit constexpr ArmNumberBase(std::size_t index) noexcept
      : index(index)
  {}
  void set_index(std::size_t index) noexcept {
    assume(index < N);
    this->index = static_cast<IndexType>(index);
  }
 private:
  using SmallIndexType = int;
  using IndexType = std::conditional_t<
    (N <= std::numeric_limits<SmallIndexType>::max()),
    SmallIndexType, std::size_t>;
  IndexType index;
};

template<std::size_t Index,
         typename Needle,
         typename HaystackHead,
         typename... HaystackTail>
struct index_of_v_impl<Index, Needle, HaystackHead, HaystackTail...>
    : index_of_v_impl<Index + 1, Needle, HaystackTail...>
{};

template<std::size_t Index, typename Needle, typename... Haystack>
struct index_of_v_impl<Index, Needle, Needle, Haystack...> {
  static constexpr std::size_t value = Index;
};

template<typename... T>
template<typename R>
SimpleVariant<T...>::SimpleVariant(R r) noexcept
    : Base(index_of_v<R, T...>)
{
  static_assert(sizeof (r) <= sizeof (this->storage));
  new (&this->storage) R(std::move(r));
}

template<typename... T>
template<typename Functor>
auto
SimpleVariant<T...>::visit(Functor&& functor) &
{
  assume(this->get_index() < sizeof...(T));
  using R = std::common_type_t<std::result_of_t<Functor&&(T&)>...>;
  return SimpleVariant::template visit_impl<0, R>(
      *this, std::forward<Functor>(functor));
}

template<typename... T>
template<typename Functor>
auto
SimpleVariant<T...>::visit(Functor&& functor) &&
{
  assume(this->get_index() < sizeof...(T));
  using R = std::common_type_t<std::result_of_t<Functor&&(T&&)>...>;
  return SimpleVariant::template visit_impl<0, R>(
      std::move(*this), std::forward<Functor>(functor));
}

template<typename... T>
template<typename Functor>
auto
SimpleVariant<T...>::visit(Functor&& functor) const &
{
  assume(this->get_index() < sizeof...(T));
  using R = std::common_type_t<std::result_of_t<Functor&&(const T&)>...>;
  return SimpleVariant::template visit_impl<0, R>(
      *this, std::forward<Functor>(functor));
}

template<typename... T>
template<typename Functor>
auto
SimpleVariant<T...>::visit(Functor&& functor) const &&
{
  assume(this->get_index() < sizeof...(T));
  using R = std::common_type_t<std::result_of_t<Functor&&(const T&&)>...>;
  return SimpleVariant::template visit_impl<0, R>(
      std::move(*this), std::forward<Functor>(functor));
}

template<typename... T>
template<std::size_t Index,
         typename R,
         typename This,
         typename Functor>
R
SimpleVariant<T...>::visit_impl(This&& this_, Functor&& functor)
{
  if constexpr (Index < sizeof...(T)) {
    using TypeAtIndex = ArmAtIndex<Index>;
    if (this_.get_index() == Index) {
      using CvType = std::conditional_t<
        std::is_const_v<std::remove_reference_t<This>>,
        const TypeAtIndex, TypeAtIndex>;
      using RefType = std::conditional_t<
        std::is_rvalue_reference_v<decltype(this_)>, CvType&&, CvType&>;
      return std::forward<Functor>(functor)(
          static_cast<RefType>(
              *reinterpret_cast<CvType*>(&this_.storage)));
    }
    return visit_impl<Index + 1, R>(std::forward<This>(this_),
                                    std::forward<Functor>(functor));
  } else {  // NOLINT
    __builtin_unreachable();
  }
}

template<typename... T>
void
SimpleVariant<T...>::destroy() noexcept
{
  this->visit([](auto& value) {
    using ValueType = typename std::decay_t<decltype(value)>;
    value.ValueType::~ValueType();
  });
}

template<typename... T>
SimpleVariant<T...>::~SimpleVariant() noexcept
{
  this->destroy();
}

template<typename... T>
SimpleVariant<T...>::SimpleVariant(SimpleVariant&& other) noexcept
    : Base(other)
{
  other.visit([&](auto& value) {
    using ValueType = typename std::decay_t<decltype(value)>;
    new (&this->storage) ValueType(std::move(value));
  });
}

template<typename... T>
SimpleVariant<T...>&
SimpleVariant<T...>::operator=(SimpleVariant&& other) noexcept
{
  if (this != &other) {
    this->destroy();
    this->set_index(other.get_index());
    other.visit([&](auto& value) {
      using ValueType = typename std::decay_t<decltype(value)>;
      new (&this->storage) ValueType(std::move(value));
    });
  };
  return *this;
}

template<typename... T>
SimpleVariant<T...>::SimpleVariant(const SimpleVariant& other)
    noexcept(is_nothrow_copy_constructible)
    : Base(other)
{
  other.visit([&](const auto& value) {
    using ValueType = typename std::decay_t<decltype(value)>;
    new (&this->storage) ValueType(value);
  });
}

template<typename... T>
SimpleVariant<T...>&
SimpleVariant<T...>::operator=(const SimpleVariant& other)
    noexcept(is_nothrow_copy_assignable)
{
  if (this != &other)
    *this = SimpleVariant(other);
  return *this;
}

template<typename... T>
typename SimpleVariant<T...>::FirstType
SimpleVariant<T...>::make_default_type()
{
  FirstType value;  // Avoid value initialization!
  return value;
}

template<typename... T>
template<typename R>
R*
SimpleVariant<T...>::get_if() noexcept
{
  constexpr std::size_t index = index_of_v<R, T...>;
  if (this->get_index() != index)
    return nullptr;
  return reinterpret_cast<R*>(&this->storage);
}

template<typename... T>
template<typename R>
const R*
SimpleVariant<T...>::get_if() const noexcept
{
  constexpr std::size_t index = index_of_v<R, T...>;
  if (this->get_index() != index)
    return nullptr;
  return reinterpret_cast<const R*>(&this->storage);
}

template<typename... T>
bool
SimpleVariant<T...>::operator==(const SimpleVariant& other) const
{
  if (this->get_index() != other.get_index())
    return false;
  return this->visit([&](const auto& payload) {
    using Payload = std::decay_t<decltype(payload)>;
    return payload == *other.template get_if<Payload>();
  });
}

template<typename... T>
bool
SimpleVariant<T...>::operator!=(const SimpleVariant& other) const
{
  return !(*this == other);
}

}  // namespace dctv
