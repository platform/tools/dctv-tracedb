// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

#include <assert.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

// N.B. assume(EXPR) evaluates EXPR for side effect on GCC!  Use plain
// old assert when you don't want to generate code for EXPR in
// optimized builds!

#ifdef NDEBUG
constexpr bool debug_mode = false;
# define assume(expr) ({                          \
      if (!( expr)) {                             \
        __builtin_unreachable();                  \
      }                                           \
    })
#else
constexpr bool debug_mode = true;
# define assume(expr) assert(expr)
#endif

#ifdef __clang__
# define restrict __restrict
#else
# define restrict __restrict__
#endif

#define AUTOFWD(value) ::std::forward<decltype(value)>(value)
#define VALTYPE(value) ::std::decay_t<decltype(value)>

#define likely(condition)                               \
  __builtin_expect(static_cast<bool>(condition), 1)
#define unlikely(condition)                             \
  __builtin_expect(static_cast<bool>(condition), 0)

namespace dctv {

constexpr bool safe_mode =
#ifdef NDEBUG
    false
#else
    true
#endif
    ;

constexpr bool little_endian =
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    true
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    false
#else
# error "Cannot detect endian?!"
#endif
    ;

constexpr bool use_absl_hash_tables = true;

#define UTX_CAT2(a,b) a ## b
#define UTX_CAT(a,b) UTX_CAT2 (a, b)
#define UTX_GENSYM(ar) UTX_CAT (ar, __LINE__)

template<typename T, auto V=0>
struct errhack final {};

struct NoCopy {
  inline NoCopy() noexcept = default;
  NoCopy(const NoCopy&) = delete;
  NoCopy& operator=(const NoCopy&) = delete;
  NoCopy(NoCopy&&) = default;
  NoCopy& operator=(NoCopy&&) = default;
};

struct NoMove {
  inline NoMove() noexcept = default;
  NoMove(const NoMove&) = default;
  NoMove& operator=(const NoMove&) = default;
  NoMove(NoMove&&) = delete;
  NoMove& operator=(NoMove&&) = delete;
};

template<typename Functor>
struct Finally final : NoCopy, NoMove
{
  inline explicit Finally(Functor functor) noexcept;
  inline ~Finally();
 private:
  const Functor functor;
};

// Run the given code on scope exit unconditionally.
#define FINALLY(...)                                    \
  auto UTX_GENSYM (utx_finally) =                       \
      Finally ([&] () { __VA_ARGS__ ; })                \

__attribute__((malloc)) void* _do_py_malloc(size_t nbytes);
void _do_py_free(void* mem) noexcept;

template<typename T>
struct PyAllocator {
  constexpr PyAllocator() noexcept = default;
  template<typename U>
  PyAllocator(const PyAllocator<U>&) noexcept {}  // NOLINT
  using value_type = T;
  inline value_type* allocate(std::size_t nr);
  inline void deallocate(T* p, std::size_t) noexcept;
  inline size_t max_size() const noexcept;

  // We need this legacy crud for the sake of Boost
  template<typename U>
  struct rebind final {
    using other = PyAllocator<U>;
  };
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using is_always_equal = std::true_type;
  using pointer = T*;
  using const_pointer = const T*;
  using reference = T&;
  using const_reference = const T&;

  template<typename U, typename... Args>
  void construct(U* p, Args&&... args)
      noexcept(noexcept(new(static_cast<void*>(p))
                        U(std::forward<Args>(args)...)))
  {
    ::new(static_cast<void*>(p)) U(std::forward<Args>(args)...);
  }

  template<typename U>
  void destroy(U* p) noexcept(noexcept(p->~U())) {
      p->~U();
  }
  pointer address(reference x) const noexcept;
  const_pointer address(const_reference x) const noexcept;
};

template<typename T, typename U>
inline bool operator==(const PyAllocator<T>&, const PyAllocator<U>&) noexcept;

template<typename T, typename U>
inline bool operator!=(const PyAllocator<T>&, const PyAllocator<U>&) noexcept;

using String = std::basic_string<char,
                                 std::char_traits<char>,
                                 PyAllocator<char>>;

void in_place_fmt_v(String* buffer, const char* fmt, va_list args);
__attribute__((format(printf, 2, 3)))
void in_place_fmt(String* buffer, const char* fmt, ...);

template<typename Comparator>
struct InvertingComparator final {
  InvertingComparator() = default;
  // NOLINTNEXTLINE
  InvertingComparator(Comparator functor) : functor(std::move(functor)) {}
  template<typename Left, typename Right>
  inline bool operator()(Left&& left, Right&& right) const;
 private:
  Comparator functor;
};

#define DCTV_NO_INLINE_FOR_PROFILER __attribute__((noinline))

#define DCTV_NORETURN_ERROR __attribute__((noreturn, cold))
#define DCTV_UNREACHABLE ({__builtin_unreachable(); abort();})

// https://quuxplusone.github.io/blog/2018/05/02/trivial-abi-101/
#ifdef __clang__
# define DCTV_TRIVIAL_ABI [[clang::trivial_abi]]
#else
# define DCTV_TRIVIAL_ABI
#endif

template<typename ExplicitType, typename... T>
struct make_array_type {
  using element_type = std::conditional_t<
    std::is_void_v<ExplicitType>,
    std::decay_t<typename std::common_type_t<T...>>,
    ExplicitType>;
  using type = std::array<element_type, sizeof...(T)>;
};

template<typename ExplicitType=void, typename... T>
constexpr auto make_array(T&&... values) ->
    typename make_array_type<ExplicitType, T...>::type
{
  using array_type = typename make_array_type<ExplicitType, T...>::type;
  return array_type{std::forward<T>(values)...};
}

template<typename T>
inline bool mul_overflow(T a, T b, T* out) noexcept;

template<typename T>
inline bool add_overflow(T a, T b, T* out) noexcept;

}  // namespace dctv

#include "dctv-inl.h"
