// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include "optional.h"
#include "pyutil.h"
#include "result_buffer.h"
#include "span.h"

namespace dctv {

struct SpanTableResultBuffer {
  SpanTableResultBuffer(unique_pyref output_cb,
                        QueryExecution* qe,
                        bool using_partition)
      : out_with_partition(
            using_partition
            ? PartitionedResultBuffer{addref(output_cb), qe}
            : optional<PartitionedResultBuffer>{}),
        out_without_partition(
            using_partition
            ? optional<UnpartitionedResultBuffer>{}
            : UnpartitionedResultBuffer{addref(output_cb), qe})
  {}

  void add(TimeStamp ts, TimeStamp duration, Partition partition) {
    if (this->out_with_partition) {
      this->out_with_partition->add(ts, duration, partition);
    } else {
      assume(partition == 0);
      this->out_without_partition->add(ts, duration);
    }
  }

  void flush() {
    if (this->out_with_partition)
      this->out_with_partition->flush();
    else
      this->out_without_partition->flush();
  }

 private:
  using PartitionedResultBuffer =
      ResultBuffer<TimeStamp /* _ts */,
                   TimeStamp /* _duration */,
                   Partition /* partition */>;
  using UnpartitionedResultBuffer =
      ResultBuffer<TimeStamp /* _ts */,
                   TimeStamp /* _duration */>;
  optional<PartitionedResultBuffer> out_with_partition;
  optional<UnpartitionedResultBuffer> out_without_partition;
};

struct EventTableResultBuffer {
  EventTableResultBuffer(unique_pyref output_cb,
                         QueryExecution* qe,
                         bool using_partition)
      : out_with_partition(
            using_partition
            ? PartitionedResultBuffer{addref(output_cb), qe}
            : optional<PartitionedResultBuffer>{}),
        out_without_partition(
            using_partition
            ? optional<UnpartitionedResultBuffer>{}
            : UnpartitionedResultBuffer{addref(output_cb), qe})
  {}

  void add(TimeStamp ts, Partition partition) {
    if (this->out_with_partition) {
      this->out_with_partition->add(ts, partition);
    } else {
      assume(partition == 0);
      this->out_without_partition->add(ts);
    }
  }

  void flush() {
    if (this->out_with_partition)
      this->out_with_partition->flush();
    else
      this->out_without_partition->flush();
  }

 private:
  using PartitionedResultBuffer =
      ResultBuffer<TimeStamp /* _ts */,
                   Partition /* partition */>;
  using UnpartitionedResultBuffer =
      ResultBuffer<TimeStamp /* _ts */>;
  optional<PartitionedResultBuffer> out_with_partition;
  optional<UnpartitionedResultBuffer> out_without_partition;
};

}  // namespace dctv
