// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include "meta_util.h"
#include "npy.h"
#include "pyparsetuple.h"

namespace dctv {

constexpr auto convert_dtype = make_pyobject_converter([](pyref value) {
  return as_dtype(value);
});

constexpr auto convert_any_array = make_pyobject_converter([](pyref value) {
  return as_any_pyarray(value);
});

constexpr auto convert_base_1d_array =
    make_pyobject_converter([](pyref value) {
      return as_base_1d_pyarray(value);
    });

struct QueryExecution;
struct QueryCache;

QueryExecution* query_execution_check(PyObject* ref);
QueryCache* query_cache_check(PyObject* ref);

template<auto Checker, typename P, typename D>
auto do_query_indir_hack(P&& parse_arg, D&& default_);

// Special hook for pyparseargs that lets functions accept a
// QueryExecution* in their argument lists without having to include
// query_execution.h.
template<typename P, typename D>
auto
handle_pyarg(QueryExecution**, P&& parse_arg, D&& default_)
{
  return do_query_indir_hack<query_execution_check>(
      AUTOFWD(parse_arg), AUTOFWD(default_));
}

// Special hook for pyparseargs that lets functions accept a
// QueryCache* in their argument lists without having to include
// query_cache.h.
template<typename P, typename D>
auto
handle_pyarg(QueryCache**, P&& parse_arg, D&& default_)
{
  return do_query_indir_hack<query_cache_check>(
      AUTOFWD(parse_arg), AUTOFWD(default_));
}

}  // namespace dctv

#include "pyparsetuplenpy-inl.h"
