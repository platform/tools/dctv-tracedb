# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for query operators"""
import logging
from collections import OrderedDict, defaultdict
from cytoolz import count, first, unique
import numpy as np
import pytest

from ._native import (
  QueryCache,
)

from .query import (
  DURATION_SCHEMA,
  EVENT_UNPARTITIONED_TIME_MAJOR,
  QueryNode,
  QuerySchema,
  QueryTable,
  SPAN_UNPARTITIONED_TIME_MAJOR,
  STRING_SCHEMA,
  TS_SCHEMA,
  TableKind,
  TableSchema,
  TableSorting,
)

from .queryop import (
  GroupCountQuery,
  GroupLabelsQuery,
  GroupSizesQuery,
  NativeGroupedAggregationQuery,
  NativeUngroupedAggregationQuery,
  Stackify,
  TimeSeriesEventEdge,
  TimeSeriesQueryTable,
  TimeSeriesSourceRole,
  WELL_KNOWN_AGGREGATIONS,
)

from .test_query import (
  TestQuery,
  TestQueryTable,
  _execute_q,
  _execute_qt,
  _test_execute,
)

from .util import (
  BOOL,
  INT64,
  final,
  iattr,
  lmap,
  load_pandas,
  override,
  tattr,
)

log = logging.getLogger(__name__)

# pylint: disable=redefined-variable-type,missing-docstring

def _manual_group_by(group_by, grouped, agg):
  """Pure-python equivalent of group-by; for testing"""
  if not isinstance(group_by, tuple):
    group_by = (group_by,)
  assert all(len(group_by_column) == len(grouped)
             for group_by_column in group_by)
  if isinstance(agg, str):
    agg = PY_AGG[agg]
  groups = OrderedDict()
  for group, grouped_value in zip(map(tuple, zip(*group_by)), grouped):
    group = _replace_nan(group)
    groups.setdefault(group, []).append(grouped_value)
  return OrderedDict(
    (group, agg(values))
    for group, values in groups.items())

N = None

# TODO(dancol): modernize these ancient tests

GROUP_TEST_REGULAR = (
  [3, 2, 1, 3],
  [8, 4, 3, 9],
)

GROUP_TEST_EMPTY = (
  [],
  [],
)

GROUP_TEST_NAN = (
  [3, 2, np.nan, 3],
  [8, 4, 3, 9],
)

GROUP_MULTI_TEST_REGULAR = (
  ([3, 3, 1, 3],
   [3, 1, 1, 3]),
  [8, 4, 3, 9],
)

GROUP_MULTI_TEST_NAN = (
  ([3, 2, np.nan, 3],
   [3, np.nan, 3, 5]),
  [8, 4, 3, 9],
)

def _replace_nan(sequence):
  # pylint: disable=comparison-with-itself
  if any(value != value for value in sequence):
    sequence = type(sequence)(None if v != v else v for v in sequence)
  return sequence

def _do_test_group_by(aggfunc, group_by, grouped, distinct=False):
  if not isinstance(group_by, tuple):
    group_by = (group_by,)
  q_grouped = TestQuery(grouped)
  q_group_by = tuple(TestQuery(group_by_column)
                     for group_by_column in group_by)
  q_group = NativeGroupedAggregationQuery(
    group_by=q_group_by,
    data=q_grouped,
    aggregation=aggfunc,
    distinct=distinct)
  result_v = _execute_q(q_group)
  golden = list(_manual_group_by(group_by, grouped, aggfunc).values())
  assert _replace_nan(list(result_v)) == _replace_nan(list(golden))

@pytest.mark.parametrize("distinct", [True, False])
@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
def test_query_group_by(aggfunc, distinct):
  """Test that group-by functionality works"""
  _do_test_group_by(aggfunc, *GROUP_TEST_REGULAR, distinct=distinct)

@pytest.mark.parametrize("distinct", [True, False])
@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
def test_query_group_by_empty(aggfunc, distinct):
  """Test that group-by functionality works"""
  _do_test_group_by(aggfunc, *GROUP_TEST_EMPTY, distinct=distinct)

@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
def test_query_group_by_nan(aggfunc):
  """Test that group-by functionality works with NaN"""
  _do_test_group_by(aggfunc, *GROUP_TEST_NAN)

@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
def test_query_multi_group_by(aggfunc):
  """Test that multi-column group-by functionality works"""
  _do_test_group_by(aggfunc, *GROUP_MULTI_TEST_REGULAR)

@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
def test_query_multi_group_by_nan(aggfunc):
  """Test that multi-column group-by functionality works with NaN"""
  _do_test_group_by(aggfunc, *GROUP_MULTI_TEST_NAN)

def _do_test_query_group_sizes(group_by, grouped):
  if not isinstance(group_by, tuple):
    group_by = (group_by,)
  q_group_by = tuple(TestQuery(group_by_column)
                     for group_by_column in group_by)
  q_sizes = GroupSizesQuery(group_by=q_group_by)
  result_v = _execute_q(q_sizes)
  golden = list(_manual_group_by(group_by, grouped, count).values())
  assert list(result_v) == golden

def test_query_group_sizes():
  """Test that we can retrieve group-by group sizes"""
  _do_test_query_group_sizes(*GROUP_TEST_REGULAR)

def test_query_group_sizes_nan():
  """Test that we can retrieve group-by group sizes with NaN"""
  _do_test_query_group_sizes(*GROUP_TEST_NAN)

def _do_test_query_group_count(group_by, grouped):
  if not isinstance(group_by, tuple):
    group_by = (group_by,)
  q_group_by = tuple(TestQuery(group_by_column)
                     for group_by_column in group_by)
  q_count = GroupCountQuery(group_by=q_group_by)
  result_v = _execute_q(q_count)
  golden = list(_manual_group_by(group_by, grouped, count).values())
  assert list(result_v) == [len(golden)]

def test_query_group_count():
  """Test that we can retrieve the total group counts"""
  group_by = [3, 2, 1, 3]
  grouped = [8, 4, 3, 9]
  _do_test_query_group_count(group_by, grouped)

def test_query_group_count_nan():
  """Test that we can retrieve the total group counts with NaN"""
  group_by = [3, 2, np.nan, 3]
  grouped = [8, 4, 3, 9]
  _do_test_query_group_count(group_by, grouped)

def _do_test_query_group_list(group_by, grouped):
  if not isinstance(group_by, tuple):
    group_by = (group_by,)
  q_group_by = [TestQuery(group_by_column)
                for group_by_column in group_by]
  label_queries = [GroupLabelsQuery(q_group_by, group_by_column_q)
                   for group_by_column_q in q_group_by]
  result = {
    label_query: _execute_q(label_query)
    for label_query in label_queries
  }
  golden_keys = list(_manual_group_by(group_by, grouped, count).keys())
  for i, label_query in enumerate(label_queries):
    assert list(result[label_query]) == [key[i] for key in golden_keys]

def test_query_group_labels():
  """Test that we can retrieve a list of all groups"""
  _do_test_query_group_list(*GROUP_TEST_REGULAR)

def test_query_group_labels_nan():
  """Test that we can retrieve a list of all groups with NaN"""
  _do_test_query_group_list(*GROUP_TEST_NAN)

def test_query_group_labels_schema():
  """Test that the grouped column schema is propagated in a labels query"""
  q_group_by = TestQuery(["abc", "def"], schema=STRING_SCHEMA)
  q_labels = GroupLabelsQuery((q_group_by,), q_group_by)
  assert q_labels.schema is q_group_by.schema

def _gqt_columns_converter(aggregations):
  columns = OrderedDict()
  def _agg(column, aggregation, input_column=None):
    input_column = input_column or column
    columns[column] = (input_column, aggregation)
  for aggregation in aggregations:
    _agg(*aggregation)
  return columns

@final
class GroupingQueryTable(QueryTable):  # TODO(dancol): remove me!
  """QueryTable that groups by a column or a function of a column"""

  _base_table = iattr(QueryTable, kwonly=True, name="base_table")
  _group_by = tattr(str, kwonly=True, name="group_by")
  _columns = iattr(converter=_gqt_columns_converter, kwonly=True,
                   name="aggregations")

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    base_table = self._base_table
    for input_column, _ in self._columns.values():
      assert input_column in base_table
      group_by = self._group_by
      assert all(col in base_table for col in self._group_by), \
      "group_by values not in base table: {!r} BT provides {!r}".format(
        sorted(set(group_by) - base_table.columns),
        sorted(base_table.columns))

  @property
  def __group_by_queries(self):
    return tuple(self._base_table.column(group_by_column)
                 for group_by_column in self._group_by)

  @override
  def countq(self):
    if not self._group_by:
      return self._base_table.countq()
    return GroupCountQuery(self.__group_by_queries)

  def group_sizes(self):
    """Generate a query that yields the size of each group"""
    if not self._group_by:
      return QueryNode.filled(1, self._base_table.countq())
    return GroupSizesQuery(self.__group_by_queries)

  @override
  def _make_column_tuple(self):
    return tuple(self._columns)

  @override
  def _make_column_query(self, column):
    input_column, aggregation = self._columns[column]
    if input_column in self._group_by:
      return GroupLabelsQuery(
        self.__group_by_queries,
        self._base_table.column(input_column))
    input_column_query = self._base_table.column(input_column)
    assert self._group_by
    return NativeGroupedAggregationQuery(
      group_by=self.__group_by_queries,
      data=input_column_query,
      aggregation=aggregation,
    )

def test_query_table_group():
  """Test that a grouping query table works"""
  group_by, grouped = GROUP_TEST_REGULAR
  qt = TestQueryTable(columns=[group_by, grouped], names=["foo", "bar"])
  qg = GroupingQueryTable(base_table=qt,
                          group_by=["foo"],
                          aggregations=[
                            ("foo/min", "min", "foo"),
                            ("bar/min", "min", "bar"),
                            ("bar/max", "max", "bar")])
  manual_group_bar_min = _manual_group_by(group_by, grouped, min)
  manual_group_bar_max = _manual_group_by(group_by, grouped, max)
  manual_group_sizes = _manual_group_by(group_by, grouped, count)

  assert len({len(manual_group_bar_min),
              len(manual_group_bar_max),
              len(manual_group_sizes)}) == 1

  queries = [
    qg["foo/min"],
    qg["bar/min"],
    qg["bar/max"],
    qg.countq(),
    qg.group_sizes(),
  ]
  results = dict(_test_execute(queries))
  assert list(results[qg["foo/min"]]) == \
    lmap(first, manual_group_bar_min.keys())
  assert list(results[qg["bar/min"]]) == \
    list(manual_group_bar_min.values())
  assert list(results[qg["bar/max"]]) == \
    list(manual_group_bar_max.values())
  assert list(results[qg.countq()]) == \
    [len(manual_group_sizes)]
  assert list(results[qg.group_sizes()]) == \
    list(manual_group_sizes.values())

def test_filter():
  """Test that we can filter on a condition"""
  qk = TestQuery([1, 2, 2, 3])
  qv = TestQuery([4, 5, 6, 8])
  mq = qk.handle_binop("=", QueryNode.scalar(2))
  fq = qv[mq]
  result = _execute_q(fq)
  assert list(result) == [5, 6]

def _format_plan_step(action, _qdb):
  type_name = type(action).__name__
  return "{}".format(type_name)

def _format_query_plan(plan):
  """Build a human-readable description of a query plan"""
  from io import StringIO
  from itertools import count as icount
  label_count = icount()
  qdb = defaultdict(lambda: "#{:02}".format(next(label_count)))
  out = StringIO()
  described = set()
  provided = set()
  for i, action in enumerate(plan):
    outputs = list(action.outputs)
    provided.update(action.outputs)
    action_desc = type(action).__name__
    out.write("{:3}: ".format(i))
    if len(outputs) > 1 or not outputs:
      out.write("{} <- {} -> {}\n".format(
        action_desc,
        ("{}" if not action.inputs
         else ", ".join(sorted(qdb[q] for q in action.inputs))),
        ("{}" if not outputs
         else ", ".join(sorted(qdb[q] for q in outputs)))))
      new_outputs = []
      for q in outputs:
        if q not in described and q not in new_outputs:
          new_outputs.append(q)
          described.add(q)
      new_outputs.sort(key=lambda q: qdb[q])
      # TODO(dancol): fix query descriptions!
      for q in new_outputs:
        out.write("\t{} := {}\n".format(
          qdb[q], str(q)))
    else:
      assert len(outputs) == 1
      out.write("{} <- {}\n".
                format(qdb[outputs[0]],
                       str(outputs[0])))
    needed = set()
    for qaction in plan[i + 1:]:
      needed.update(qaction.inputs)
    dead = provided - needed
    if dead:
      out.write("\tdead: {}\n".format(
        ", ".join(sorted(qdb[q] for q in dead))))
      provided -= dead

  return out.getvalue()


def test_query_interning():
  """Test that identical query objects are identical"""
  assert QueryNode.scalar(5) is QueryNode.scalar(5)

def _py_biggest_key(v):
  if isinstance(v, str):
    return len(v)
  k = int(np.log10(abs(v)))
  if v < 0:
    k += 1
  return k

def _wrap_null_empty(func):
  def _wrapper(seq):
    if not len(seq):  # pylint: disable=len-as-condition
      return None
    return func(seq)
  return _wrapper

def _fake_biggest(seq):
  groups = defaultdict(list)
  for item in seq:
    groups[_py_biggest_key(item)].append(item)
  return groups[max(groups)][0]

PY_AGG = {
  "first": _wrap_null_empty(first),
  "max": _wrap_null_empty(max),
  "min": _wrap_null_empty(min),
  "sum": _wrap_null_empty(sum),
  "avg": _wrap_null_empty(np.mean),
  "prod": _wrap_null_empty(np.prod),
  "count": len,
  "stddev": _wrap_null_empty(lambda s: load_pandas().Series(s).std()),
  "variance": _wrap_null_empty(lambda s: load_pandas().Series(s).var()),
  "nunique": _wrap_null_empty(lambda s: count(unique(s))),
  "unique_mask": _wrap_null_empty(lambda s: all(v == s[0] for v in s)),
  "biggest": _wrap_null_empty(_fake_biggest),
}

DATA_TABLE = {
  "simple": [1, 2, 341, 3],
  "dups": [1, 2, 1, 44, 1, 4],
  "allsame": [1, 1, 1, 1],
  "empty": []
}

@pytest.mark.parametrize("aggfunc", WELL_KNOWN_AGGREGATIONS)
@pytest.mark.parametrize("data", sorted(DATA_TABLE.keys()))
def test_aggregation_query(aggfunc, data):
  """Test that simple aggregation works"""
  numbers = DATA_TABLE[data]
  q = TestQuery(numbers)
  q = NativeUngroupedAggregationQuery(aggregation=aggfunc, data=q)
  result = _execute_q(q, block_size=2)
  pd = load_pandas()
  assert list(result) == [PY_AGG[aggfunc](pd.Series(numbers))]

def test_aggregation_biggest():
  """Test that biggest aggregation works"""
  numbers = [1, 3241, -6, 14]
  q = TestQuery(numbers)
  q = NativeUngroupedAggregationQuery(aggregation="biggest", data=q)
  assert list(_execute_q(q)) == [max(numbers)]

def test_aggregation_biggest_zero_nowarn(recwarn):
  """Test that zero doesn't barf"""
  numbers = [0, 1, 2]
  q = TestQuery(numbers)
  q = NativeUngroupedAggregationQuery(aggregation="biggest", data=q)
  assert list(_execute_q(q)) == [1]
  warnings = list(filter(lambda w: "size changed" not in str(w), recwarn))
  assert not warnings, "warnings triggered: {!r}".format(lmap(str, warnings))

def test_aggregation_biggest_string():
  """Test that biggest works on strings"""
  strings = ["foo", "bar", "longest", "x"]
  q = TestQuery(strings)
  qa = NativeUngroupedAggregationQuery(aggregation="biggest", data=q)
  assert list(_execute_q(qa)) == ["longest"]

def _do_test_stack_computation(rows):
  task_id = []
  ts = []
  token = []
  end_flag = []
  next_ts = 1
  for row_task_id, row_token in rows:
    task_id.append(row_task_id)
    ts.append(next_ts)
    token.append(row_token)
    end_flag.append(row_token < 0)
    next_ts += 10
  qt = TestQueryTable(
    names=("task_id", "_ts", "start_token", "end_flag"),
    columns=[task_id, ts, token, end_flag],
    schemas=[
      QuerySchema(INT64),
      TS_SCHEMA,
      QuerySchema(INT64),
      QuerySchema(BOOL),
    ])

  stackify = Stackify(
    ts=qt["_ts"],
    partition=qt["task_id"],
    frame_id=qt["start_token"],
    end_flag=qt["end_flag"],
  )

  Meta = Stackify.Meta

  wanted = OrderedDict((
    (stackify.metaq(Meta.SH_PARTITION), "history_task_id"),
    (stackify.metaq(Meta.SH_TS), "history_ts"),
    (stackify.metaq(Meta.SH_STACK_ID), "history_stack_id"),
    (stackify.metaq(Meta.SD_STACK_ID), "stacks_stack_id"),
    (stackify.metaq(Meta.SD_DEPTH), "stacks_depth"),
    (stackify.metaq(Meta.SD_FRAME_ID), "stacks_token"),
  ))

  raw_result = dict(_test_execute(wanted))
  result = OrderedDict()
  for wanted_query, key in wanted.items():
    result[key] = list(raw_result[wanted_query])
    log.debug("result %20s = %s", key,
              ", ".join(
                ("%5s" % (item,)) for item in result[key]))
  return result


def test_query_stack_computation():
  """Test that the stack computation stuff works"""
  # pylint: disable=bad-whitespace
  result = _do_test_stack_computation([
    [0,   100],
    [0,   200],
    [0,  -1],
    [0,  -1],
    [0,   100],
  ])
  assert ([result[k] for k in ("history_task_id",
                               "history_ts",
                               "history_stack_id")]
          ==
          [
            [ 0,  0,  0,  0,  0],
            [ 1, 11, 21, 31, 41],
            [ 1,  2,  1,  0,  1],
          ])

  assert ([result[k] for k in ("stacks_stack_id",
                               "stacks_depth",
                               "stacks_token")]
          ==
          [
            [   1,   2,   2],
            [   0,   0,   1],
            [ 100, 100, 200],
          ])

@final
def test_dummy_native():
  """Test native array iteration code"""
  from ._native import _test_dummy, QueryExecution
  def _supply_data():
    yield np.array([1, 3], "i"), [7, 4]
    yield np.array([], "i"), np.array([], "l")
    yield np.array([], "i"), np.array([], "l")
    yield np.array([5, -3], "i"), [7, 0]
  output = []
  def _receive_output(*args):
    output.append(lmap(lambda item: np.asarray(item).tolist(), args))
  _test_dummy(_supply_data(), _receive_output,
              QueryExecution.make([], qc=QueryCache(block_size=3)))
  assert output == [
    [
      [8, 7, 12],
      [-6, -1, -2],
      [0, 1, 2],
    ],
    [
      [-3],
      [-3],
      [3],
    ],
  ]

def test_dummy_native_empty():
  """Test native array iteration code for the empty input case"""
  from ._native import _test_dummy, QueryExecution
  output = []
  def _receive_output(*args):
    output.append(lmap(lambda item: np.asarray(item).tolist(), args))
  _test_dummy(iter([]), _receive_output, QueryExecution.make([]))
  assert output == [[[], [], []]]

def test_time_series_conversion():
  """Converting time series to spans"""
  qt_start_events = TestQueryTable(
    names=("_ts", "coffee"),
    rows=[
      [10, 1],
      [20, 2],
      [40, 3],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR,
  )
  qt_ts = TimeSeriesQueryTable(
    sources=[
      dict(source=qt_start_events)
    ],
    columns=[
      dict(column="coffee_start", source=0, source_column="coffee"),
      dict(column="coffee_start2", source=0,
           source_column="coffee",
           edge=TimeSeriesEventEdge.RISING),
      dict(column="coffee_end", source=0,
           source_column="coffee",
           edge=TimeSeriesEventEdge.FALLING),
    ]
  ).to_schema(sorting=TableSorting.TIME_MAJOR)

  assert _execute_qt(qt_ts) == TestQueryTable(
    names=["_ts", "_duration", "coffee_start", "coffee_start2", "coffee_end"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT64, INT64],
    rows=[
      [10, 10, 1, 1, 2],
      [20, 20, 2, 2, 3],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)


# Time series conversion

def test_time_series_conversion_no_sources():
  """Test that time series conversion works with no event sources"""
  qt_ts = TimeSeriesQueryTable(
    sources=[],
    columns=[]
  ).to_schema(sorting=TableSorting.TIME_MAJOR)
  assert _execute_qt(qt_ts) == TestQueryTable(
    names=["_ts", "_duration"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA],
    rows=[
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_start_stop():
  """Test time series conversion with separate start and stop events"""
  qt_start_events = TestQueryTable(
    names=("_ts", "coffee"),
    rows=[
      [10, 1],
      [20, 2],
      [22, 2],  # Self-end
      [40, 3],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR,
  )

  qt_stop_events = TestQueryTable(
    names=("_ts", "coffee"),
    rows=[
      [15, -1],
      [24, -2],  # Spurious end
      [40, -3],  # Zero duration
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR,
  )

  qt_ts = TimeSeriesQueryTable(
    sources=[
      dict(source=qt_start_events,
           nickname="cstart"),
      dict(source=qt_stop_events,
           nickname="cstop",
           role=TimeSeriesSourceRole.STOP),
    ],
    columns=[
      dict(column="coffee_start",
           source="cstart",
           source_column="coffee"),
      dict(column="coffee_stop_invalid",
           source="cstop",
           source_column="coffee"),
      dict(column="coffee_end",
           source="cstop",
           source_column="coffee",
           edge=TimeSeriesEventEdge.FALLING),
    ]).to_schema(sorting=TableSorting.TIME_MAJOR)

  # pylint: disable=bad-whitespace
  assert _execute_qt(qt_ts) == TestQueryTable(
    names=["_ts", "_duration", "coffee_start", "coffee_stop_invalid", "coffee_end"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT64, INT64],
    rows=[
      [10, 5, 1, N, -1],
      [20, 2, 2, N,  N],
      [22, 2, 2, N, -2],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def _tsts_prio(*, high_prio_starts, low_prio_starts, stops, reverse_sources=False):
  qt_start_events_highprio = TestQueryTable(
    names=["_ts", "coffee"],
    rows=[
      [start, start * 10]
      for start in high_prio_starts
    ], table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  qt_start_events_lowprio = TestQueryTable(
    names=["_ts", "coffee"],
    rows=[
      [start, start * 10 + 1]
      for start in low_prio_starts
    ], table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  qt_stop_events = TestQueryTable(
    names=["_ts"],
    rows=[[stop] for stop in stops],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  sources = [dict(source=qt_start_events_highprio,
                  nickname="cstart_highprio"),
             dict(source=qt_start_events_lowprio,
                  nickname="cstart_lowprio",
                  priority=1),
             dict(source=qt_stop_events,
                  nickname="cstop",
                  role=TimeSeriesSourceRole.STOP)]
  if reverse_sources:
    sources.reverse()
  qt_ts = TimeSeriesQueryTable(
    sources=sources,
    columns=[
      dict(source="cstart_highprio",
           column="coffee_highprio",
           source_column="coffee"),
      dict(source="cstart_lowprio",
           column="coffee_lowprio",
           source_column="coffee"),
    ]).to_schema(sorting=TableSorting.TIME_MAJOR)
  return _execute_qt(qt_ts)

def test_time_series_conversion_prio_simple():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[30],
    stops=[40]) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [20, 40, 200, N],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_prio_before_highprio():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[10],
    stops=[40]) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [10, 20,   N, 101],
        [20, 40, 200,   N],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_prio_after_close():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[30],
    stops=[25, 40]) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [20, 25, 200,   N],
        [30, 40,   N, 301],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_prio_at_close():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[25],
    stops=[25, 40]) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [20, 25, 200,   N],
        [25, 40,   N, 251],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_prio_race():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[20],
    stops=[40]) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [20, 40, 200,   N],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_prio_race_backwards():
  # pylint: disable=bad-whitespace
  assert _tsts_prio(
    high_prio_starts=[20],
    low_prio_starts=[20],
    stops=[40],
    reverse_sources=True) == TestQueryTable(
      names=["_ts", "_end", "coffee_highprio", "coffee_lowprio"],
      rows=[
        [20, 40, 200,   N],
      ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_time_series_conversion_partitions():
  """Converting time series to spans with partitions"""
  qt_start_events = TestQueryTable(
    names=("_ts", "coffee", "partno"),
    rows=[
      [10, 1, 1],
      [20, 2, 2],
      [40, 3, 2],
      [50, 4, 1],
    ],
    table_schema=TableSchema(TableKind.EVENT,
                             "partno",
                             TableSorting.TIME_MAJOR),
  )
  qt_ts = TimeSeriesQueryTable(
    sources=[dict(source=qt_start_events)],
    columns=[dict(column="coffee", source=0)],
    partition="partno",
  ).to_schema(sorting=TableSorting.TIME_MAJOR)
  result = _execute_qt(qt_ts)
  assert result == dict(
    _ts=[10, 20],
    _duration=[40, 20],
    coffee=[1, 2],
    partno=[1, 2],
  )

@pytest.mark.parametrize("part_dtype_code", ["i", "L", "b", "l", "g"])
def test_time_series_conversion_partitions_casting(part_dtype_code):
  """Partition downcast correctness"""
  part_dtype = np.dtype(part_dtype_code)
  qt_start_events = TestQueryTable(
    names=("_ts", "coffee", "partno"),
    schemas=(TS_SCHEMA, INT64, part_dtype),
    rows=[
      [10, 1, 1],
      [20, 2, 2],
      [40, 3, 2],
      [50, 4, 1],
    ],
    table_schema=TableSchema(TableKind.EVENT,
                             "partno",
                             TableSorting.TIME_MAJOR))
  qt_ts = TimeSeriesQueryTable(
    sources=[dict(source=qt_start_events)],
    columns=[dict(column="coffee", source=0)],
    partition="partno",
  ).to_schema(sorting=TableSorting.TIME_MAJOR)
  if part_dtype_code == "g":
    with pytest.raises(TypeError):
      _execute_qt(qt_ts)
    return

  result = _execute_qt(qt_ts)
  assert result == TestQueryTable(
    names=("_ts", "_duration", "partno", "coffee"),
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype, INT64),
    rows=[
      [10, 40, 1, 1],
      [20, 20, 2, 2],
    ],
    table_schema=TableSchema(TableKind.SPAN,
                             "partno",
                             TableSorting.TIME_MAJOR))

def test_time_series_conversion_terminate_opened():
  """Converting time series to spans with partitions"""
  qt_start_events = TestQueryTable(
    names=("_ts", "coffee", "partno"),
    rows=[
      [10, 1, 1],
      [20, 2, 2],
      [40, 3, 2],
      [50, 4, 1],
    ],
    table_schema=TableSchema(TableKind.EVENT,
                             "partno",
                             TableSorting.TIME_MAJOR),
  )
  qt_end_broadcast = TestQueryTable(
    names=["_ts"],
    rows=[
      [70],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)

  qt_ts = TimeSeriesQueryTable(
    sources=[
      dict(source=qt_start_events),
      dict(source=qt_end_broadcast,
           role=TimeSeriesSourceRole.STOP_BROADCAST),
    ],
    columns=[
      dict(column="coffee", source=0),
      dict(column="fcoffee",
           source_column="coffee",
           source=0,
           edge=TimeSeriesEventEdge.FALLING),
    ],
    partition="partno",
  ).to_schema(sorting=TableSorting.TIME_MAJOR)
  result = _execute_qt(qt_ts)
  assert result == dict(
    _ts=[10, 20, 40, 50],
    _duration=[40, 20, 30, 20],
    coffee=[1, 2, 3, 4],
    fcoffee=[4, 3, None, None],
    partno=[1, 2, 2, 1],
  )
