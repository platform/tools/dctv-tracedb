// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "sort.h"

#include "automethod.h"
#include "npyiter.h"
#include "pyiter.h"
#include "pyparsetuplenpy.h"
#include "query.h"
#include "vector.h"

// If code in this file seems a bit C-ish, it's because some C++
// constructs (e.g. heavy use of lambdas) appear to make 2019
// compilers lose track of restrict aliasing information.

namespace dctv {
namespace {

using std::array;
using std::swap;

npy_intp min_radix_sort = 8192;

void
set_min_radix_sort(npy_intp new_min_radix_sort)
{
  min_radix_sort = new_min_radix_sort;
}

unique_pyref
standard_numpy_lexsort(pyref array_list)
{
  return adopt_check(PyArray_LexSort(array_list.get(), /*axis=*/0));
}

// Number of bits in each digit.
constexpr int digit_bits = 8;
constexpr int nr_digit_values = 1<<digit_bits;

static_assert(digit_bits <= CHAR_BIT * sizeof (unsigned));
static_assert(sizeof (float) == sizeof (int32_t));
static_assert(sizeof (double) == sizeof (int64_t));

template<typename T>
inline
unsigned
get_digit_value(T value, int digitno)
{
  static_assert(std::is_unsigned_v<T>);
  return (value >> (digitno * digit_bits)) &
      ((1U << digit_bits) - 1);
}

template<typename F, typename U>
U
sort_munge_float(F value)
{
  // IEEE 754 values of the same sign order themselves correctly as
  // unsigned integers.
  static_assert(sizeof (F) == sizeof (U));
  static_assert(std::is_unsigned_v<U>);
  U result;
  memcpy(&result, &value, sizeof (result));
  constexpr int nbits = CHAR_BIT * sizeof (U);
  const U sign_bit = static_cast<U>(1) << (nbits - 1);
  const bool is_positive = !(result & sign_bit);
  if (is_positive)
    result ^= sign_bit;
  else
    result = ~result;
  return result;
}

template<typename T>
auto
make_compare_value(T value)
{
  if constexpr (std::is_unsigned_v<T> || std::is_same_v<T, bool>) {
    return value;
  } else if constexpr (std::is_integral_v<T> && std::is_signed_v<T>) {
    // Flipping the sign bit shifts the integer into the
    // positive range.
    using UnsignedT = std::make_unsigned_t<T>;
    auto munged = static_cast<UnsignedT>(value);
    constexpr int nbits = CHAR_BIT * sizeof (UnsignedT);
    munged ^= static_cast<UnsignedT>(1) << (nbits - 1);
    return munged;
  } else if constexpr (std::is_same_v<T, float>) {
    return sort_munge_float<float, uint32_t>(value);
  } else if constexpr (std::is_same_v<T, double>) {
    return sort_munge_float<double, uint64_t>(value);
  } else {
    errhack<T>::unknown_compare_conversion();
  }
}

template<typename T>
constexpr
int
nr_digits_of()
{
  using CompareT = std::decay_t<decltype(make_compare_value(T()))>;
  constexpr int nr_bits = sizeof (CompareT) * CHAR_BIT;
  static_assert(nr_bits % digit_bits == 0);
  constexpr int nr_digits = nr_bits / digit_bits;
  return nr_digits;
}

int
nr_digits_of(char code)
{
  return npy_type_dispatch_code(code, [&](auto dummy) {
    return nr_digits_of<VALTYPE(dummy)>();
  });
}

void
prefix_sum_digits(npy_intp* restrict where,
                  npy_intp* restrict counts)
{
  // WHERE above doesn't include the bucket-used prefix!
  where[0] = 0;
  for (npy_intp digit_value = 1;
       digit_value < nr_digit_values;
       ++digit_value)
    where[digit_value] =
        where[digit_value - 1] + counts[digit_value - 1];
}

void
clear_counts(npy_intp* const restrict counts,
             const npy_intp nr_digits)
{
  std::fill(&counts[0], &counts[(1 + nr_digit_values) * nr_digits], 0);
}

__attribute__((always_inline))
npy_intp*
counts_for_digitno(npy_intp* const counts, const npy_intp digitno)
{
  // -1 is the used-bucket count for this digit number
  return &counts[(1 + nr_digit_values) * digitno + 1];
}

template<typename T>
void
count_1(const T row_value, npy_intp* restrict const counts)
{
  // Compute a count for all the digits at once.  The zero slot in
  // each digit-number bucket is a count of used buckets.
  const auto compare = make_compare_value(row_value);
  constexpr int nr_digits = nr_digits_of<T>();
  for (int digitno = 0; digitno < nr_digits; ++digitno) {
    npy_intp* dn_counts = counts_for_digitno(counts, digitno);
    npy_intp& dv_count = dn_counts[get_digit_value(compare, digitno)];
    dn_counts[-1] += (dv_count == 0);
    dv_count++;
  }
}

template<typename T>
void
count_column_via_scan(
    const npy_intp nrows,
    const T* restrict const column,
    npy_intp* restrict const counts)
{
  assume(nrows >= 0);
  clear_counts(counts, nr_digits_of<T>());
  for (npy_intp rowno = 0; rowno < nrows; ++rowno)
    count_1<T>(column[rowno], counts);
}

template<typename T>
void
do_initial_digit(
    const npy_intp nrows,
    const npy_intp digitno,
    const T* restrict const column,
    npy_intp* restrict const counts,
    npy_intp* restrict const out_index,
    T* restrict const out_value /* nullable */)
{
  assume(nrows >= 0);
  npy_intp* const dn_counts = counts_for_digitno(counts, digitno);
  assume(dn_counts[-1] > 1);
  npy_intp where[nr_digit_values];
  prefix_sum_digits(where, dn_counts);
  for (npy_intp rowno = 0; rowno < nrows; ++rowno) {
    const T row_value = column[rowno];
    const auto compare = make_compare_value(row_value);
    const auto digit_value = get_digit_value(compare, digitno);
    out_index[where[digit_value]] = rowno;
    if (out_value)
      out_value[where[digit_value]] = row_value;
    where[digit_value] += 1;
  }
}

template<typename T>
void
do_middle_digit(
    const npy_intp nrows,
    const npy_intp digitno,
    const T* restrict const column,
    npy_intp* restrict const counts,
    const npy_intp* restrict const in_index,
    const T* restrict const in_value,
    npy_intp* restrict const out_index,
    T* restrict const out_value)
{
  assume(nrows >= 0);
  npy_intp* const dn_counts = counts_for_digitno(counts, digitno);
  assert (dn_counts[-1] > 1);
  npy_intp where[nr_digit_values];
  prefix_sum_digits(where, dn_counts);
  for (npy_intp rowno = 0; rowno < nrows; ++rowno) {
    const auto compare = make_compare_value(in_value[rowno]);
    assert(column[in_index[rowno]] == in_value[rowno]);
    const auto digit_value = get_digit_value(compare, digitno);
    out_index[where[digit_value]] = in_index[rowno];
    if (out_value)
      out_value[where[digit_value]] = in_value[rowno];
    where[digit_value] += 1;
  }
}

template<typename FT, typename NT>
void
do_transitional_digit(
    const npy_intp nrows,
    const npy_intp digitno,
    const FT* restrict const column,
    const NT* restrict const next_column,
    npy_intp* restrict const counts,
    const npy_intp* restrict const in_index /* nullable */,
    const FT* restrict const in_value /* nullable */,
    npy_intp* restrict const out_index,
    NT* restrict const out_value)
{
  assume(nrows >= 0);
  npy_intp* const dn_counts = counts_for_digitno(counts, digitno);
  assume(dn_counts[-1] > 1);
  assume(!in_index == !in_value);
  npy_intp where[nr_digit_values];
  prefix_sum_digits(where, dn_counts);
  clear_counts(counts, nr_digits_of<NT>());
  for (npy_intp rowno = 0; rowno < nrows; ++rowno) {
    npy_intp in_index_v;
    FT row_value;
    if (in_index) {
      in_index_v = in_index[rowno];
      row_value = in_value[rowno];
    } else {
      assume(!in_value);
      in_index_v = rowno;
      row_value = column[rowno];
    }
    assert(column[in_index_v] == row_value);
    const auto compare = make_compare_value(row_value);
    const auto digit_value = get_digit_value(compare, digitno);
    out_index[where[digit_value]] = in_index_v;
    NT next_value = next_column[in_index_v];
    if (rowno + 1 < nrows && in_index)
      __builtin_prefetch(&next_column[in_index[rowno + 1]]);
    out_value[where[digit_value]] = next_value;
    where[digit_value] += 1;

    // Since we've already paid for the next_column value, we might as
    // well accumulate counts.  The logic here is similar to the logic
    // in do_first_column_first_digit, except there we do all the
    // counts in one scan, and here we do counts opportunistically as
    // we take values.
    count_1<NT>(next_value, counts);
  }
}

template<typename T>
void
fallback_take(
    const npy_intp nrows,
    const T* restrict const column,
    const npy_intp* restrict const index,
    T* restrict const out_value /* nullable */)
{
  assume(nrows >= 0);
  for (npy_intp rowno = 0; rowno < nrows; ++rowno)
    out_value[rowno] = column[index[rowno]];
}

unique_pyref
fast_argsort(QueryCache* qc, pyref array_list)
{
  // Fast-ish LSD radix sort.  Optimized for memory bandwidth.
  // About 4x faster than numpy's lexsort for large sorts.

  // TODO(dancol): we still need an exterior sort for truly
  // immense sorts.

  Vector<unique_pyarray> columns =
      py2vec_fast(array_list,
                  [&](pyref a) {return as_base_1d_pyarray(a);});
  assume(columns.size() <=
         static_cast<size_t>(std::numeric_limits<npy_intp>::max()));

  if (columns.empty())
    throw_invalid_query("cannot sort zero columns");

  const npy_intp nrows = npy_size1d(columns[0]);
  assume(nrows >= 0);

  if (nrows < min_radix_sort)
    return standard_numpy_lexsort(pytuple::from(std::move(columns)));

  npy_intp max_nr_digits = 0;
  npy_intp max_elsize = 0;

  for (auto& array : columns) {
    if (npy_size1d(array) != nrows)
      throw_invalid_query_fmt("mismatch array sizes in sort: %s vs %s",
                              npy_size1d(array), nrows);
    char kind = npy_dtype(array)->kind;
    npy_intp elsize = npy_dtype(array)->elsize;
    max_elsize = std::max(elsize, max_elsize);
    // Defer to numpy sort if we're looking at anything weird.
    if ((kind != 'u' && kind != 'i' && kind != 'b' && kind != 'f') ||
        npy_strides(array)[0] != elsize) {
      return standard_numpy_lexsort(pytuple::from(std::move(columns)));
    }
    npy_intp nr_digits = nr_digits_of(npy_dtype(array)->type);
    max_nr_digits = std::max(max_nr_digits, nr_digits);
  }

  assume(max_nr_digits > 0);
  assume(max_elsize > 0);

  // If we're sorting more than a single bool, we need to allocate
  // intermediate storage for the column values --- otherwise, we'd
  // need a random-access take per byte instead of
  // per-non-first-column.
  void* buffer_in = nullptr;
  FINALLY(_do_py_free(buffer_in));
  void* buffer_out = nullptr;
  FINALLY(_do_py_free(buffer_out));

  // We need memory to store the digit counts.  We count all the
  // digits in a column at once, during the first pass, so we reserve
  // space for all counts here.  The 1+ below is to hold the
  // bucked-used count for each group of digit counts.
  const npy_intp count_nentries = max_nr_digits * (1 + nr_digit_values);
  const npy_intp count_nbytes = count_nentries * sizeof (npy_intp);
  npy_intp* const counts = reinterpret_cast<npy_intp*>(
      _do_py_malloc(count_nbytes));
  FINALLY(_do_py_free(counts));

  // For checking whether incremental counting produced the
  // correct results.
  npy_intp* const counts_aux = safe_mode
      ? reinterpret_cast<npy_intp*>(_do_py_malloc(count_nbytes))
      : nullptr;
  FINALLY(if (counts_aux) _do_py_free(counts_aux));

  unique_pyarray indexer_in;
  unique_pyarray indexer_out;

  // Columns are oriented least-significant first, so flip the
  // array and take them from the end so way can pop in constant time.
  std::reverse(columns.begin(), columns.end());
  bool have_column_counts = false;
  do {
    unique_pyarray column = std::move(columns.back());
    columns.pop_back();
    void* column_data = npy_data_raw(column);
    char type = npy_dtype(column)->type;
    bool need_column_values;
    int nr_digits = nr_digits_of(type);

    if (have_column_counts) {
      assume(indexer_in);
      assume(buffer_in);
      have_column_counts = false;
      need_column_values = false;
      if (safe_mode) {
        npy_type_dispatch_code(type, [&](auto dummy) {
          using T = VALTYPE(dummy);
          count_column_via_scan<T>(
              nrows, reinterpret_cast<T*>(column_data), counts_aux);
        });
        // Use an explicit loop instead of memcmp so we can break more
        // precisely in the debugger.  This is !NDEBUG-only code.
        for (npy_intp digitno = 0; digitno < nr_digits; ++digitno) {
          npy_intp* const dn_counts =
              counts_for_digitno(counts, digitno);
          npy_intp* const dn_counts_aux =
              counts_for_digitno(counts_aux, digitno);
          for (npy_intp i = -1; i < nr_digit_values; ++i)
            assert(dn_counts[i] == dn_counts_aux[i]);
        }
      }
    } else {
      npy_type_dispatch_code(type, [&](auto dummy) {
        using T = VALTYPE(dummy);
        count_column_via_scan<T>(
            nrows, reinterpret_cast<T*>(column_data), counts);
      });
      if (!indexer_in) {
        assume(!indexer_out);
        assume(!buffer_in);
        assume(!buffer_out);
        need_column_values = false;
      } else {
        assume(buffer_in);
        need_column_values = true;
      }
    };

    // At this point, we have all the digit counts for this column.
    // Reduce nr_digits by the number of trailing useless digits so
    // that we're more likely to use the transitional-digit path
    // below and fill the next column while we sort the last digit of
    // this one.
    assume(nr_digits > 0);
    do {
      npy_intp* const last_digit_counts =
          counts_for_digitno(counts, nr_digits - 1);
      if (last_digit_counts[-1] > 1)
        break;
    } while (--nr_digits);
    assume(nr_digits >= 0);
    if (nr_digits && need_column_values) {
      // Last column didn't fill our buffer with column values
      // during its last-digit scan, so do that ourselves now.
      npy_type_dispatch_code(type, [&](auto dummy) {
        using T = VALTYPE(dummy);
        fallback_take<T>(
            nrows,
            reinterpret_cast<T*>(column_data),
              npy_data<npy_intp>(indexer_in),
            reinterpret_cast<T*>(buffer_in));
      });
    }

    for (int digitno = 0; digitno < nr_digits; ++digitno) {
      npy_intp* const dn_counts = counts_for_digitno(counts, digitno);
      const npy_intp bucket_count = dn_counts[-1];
      const bool useful_to_sort = bucket_count > 1;
      if (!useful_to_sort)
        continue;
      const bool is_last_digit_this_column = digitno + 1 == nr_digits;
      const bool is_last_digit_all_columns =
          is_last_digit_this_column && columns.empty();
      if (!indexer_in) {
        assume(!buffer_in);
        assume(!indexer_out);
        assume(!buffer_out);
        indexer_out =
            make_uninit_hunk_array(qc, type_descr<npy_intp>(), nrows);
        if (!is_last_digit_all_columns) {
          indexer_in =
              make_uninit_hunk_array(qc, type_descr<npy_intp>(), nrows);
          npy_intp buffer_nbytes = nrows * max_elsize;  // won't overflow
          buffer_in = _do_py_malloc(buffer_nbytes);
          buffer_out = _do_py_malloc(buffer_nbytes);
        }
        if (is_last_digit_this_column && !is_last_digit_all_columns) {
          npy_type_dispatch_code(type, [&](auto dummy) {
            using FT = VALTYPE(dummy);
            pyarray_ref next_column = columns.back();
            char next_type = npy_dtype(next_column)->type;
            npy_type_dispatch_code(next_type, [&](auto dummy2) {
              using NT = VALTYPE(dummy2);
              do_transitional_digit<FT, NT>(
                  nrows,
                  digitno,
                  reinterpret_cast<FT*>(column_data),
                  npy_data<NT>(next_column),
                  counts,
                  /*index_in=*/nullptr,
                  /*in_value=*/nullptr,
                  npy_data<npy_intp>(indexer_out),
                  reinterpret_cast<NT*>(buffer_out));
            });
            have_column_counts = true;
          });
        } else {
          npy_type_dispatch_code(type, [&](auto dummy) {
            using T = VALTYPE(dummy);
            do_initial_digit<T>(
                nrows,
                digitno,
                reinterpret_cast<T*>(column_data),
                counts,
                npy_data<npy_intp>(indexer_out),
                reinterpret_cast<T*>(buffer_out));
          });
        }
        swap(indexer_in, indexer_out);
        swap(buffer_in, buffer_out);
      } else if (!is_last_digit_this_column) {
        assume(indexer_in);
        assume(buffer_in);
        assume(indexer_out);
        assume(buffer_out);
        npy_type_dispatch_code(type, [&](auto dummy) {
          using T = VALTYPE(dummy);
          do_middle_digit<T>(
              nrows,
              digitno,
              reinterpret_cast<T*>(column_data),
              counts,
              npy_data<npy_intp>(indexer_in),
              reinterpret_cast<T*>(buffer_in),
              npy_data<npy_intp>(indexer_out),
              reinterpret_cast<T*>(buffer_out));
        });
        swap(indexer_in, indexer_out);
        swap(buffer_in, buffer_out);
      } else if (!is_last_digit_all_columns) {
        assume(is_last_digit_this_column);
        assume(indexer_in);
        assume(buffer_in);
        assume(indexer_out);
        assume(buffer_out);
        npy_type_dispatch_code(type, [&](auto dummy) {
          using FT = VALTYPE(dummy);
          pyarray_ref next_column = columns.back();
          char next_type = npy_dtype(next_column)->type;
          npy_type_dispatch_code(next_type, [&](auto dummy2) {
            using NT = VALTYPE(dummy2);
            do_transitional_digit<FT, NT>(
                nrows,
                digitno,
                reinterpret_cast<FT*>(column_data),
                npy_data<NT>(next_column),
                counts,
                npy_data<npy_intp>(indexer_in),
                reinterpret_cast<FT*>(buffer_in),
                npy_data<npy_intp>(indexer_out),
                reinterpret_cast<NT*>(buffer_out));
          });
        });
        have_column_counts = true;
        swap(indexer_in, indexer_out);
        swap(buffer_in, buffer_out);
      } else {
        assume(is_last_digit_all_columns);
        assume(indexer_in);
        assume(buffer_in);
        assume(indexer_out);
        npy_type_dispatch_code(type, [&](auto dummy) {
          using T = VALTYPE(dummy);
          do_middle_digit<T>(
              nrows,
              digitno,
              reinterpret_cast<T*>(column_data),
              counts,
              npy_data<npy_intp>(indexer_in),
              reinterpret_cast<T*>(buffer_in),
              npy_data<npy_intp>(indexer_out),
              /*buffer_out=*/nullptr);
        });
        swap(indexer_in, indexer_out);
        swap(buffer_in, buffer_out);
      }
    }
  } while (!columns.empty());

  if (!indexer_in) {
    // Congratulations!  No column was useful for sorting.  Make a
    // sequential index instead.
    indexer_in =
        make_uninit_hunk_array(qc, type_descr<npy_intp>(), nrows);
    npy_intp* indexer_data = npy_data<npy_intp>(indexer_in);
    for (npy_intp rowno = 0; rowno < nrows; ++rowno)
      indexer_data[rowno] = rowno;
  }

  return indexer_in;
}

static PyMethodDef functions[] = {
  AUTOFUNCTION(fast_argsort,
               "Fast radix-based argsort",
               (QueryCache*, qc)
               (pyref, columns)
  ),
  AUTOFUNCTION(set_min_radix_sort,
               "Set row count threshold for radix sort",
               (npy_intp, min_radix_sort)
  ),
  { 0 }
};

}  // anonymous namespace

void
init_sort(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
