// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// Aggregation kernels for grouping by partition.
#include "span_pivot_column.h"

#include "command_stream.h"
#include "hash_table.h"
#include "native_aggregation.h"
#include "npyiter.h"
#include "optional.h"
#include "pyerr.h"
#include "pyobj.h"
#include "result_buffer.h"
#include "span.h"
#include "span_pivot.h"
#include "string_table.h"
#include "vector.h"

namespace dctv {
namespace {

template<typename Kernel>
struct HashOps final {
  using Out = typename Kernel::Out;
  inline void set(Partition input_partition, Out value);
  inline void clear(Partition input_partition);
  inline optional<Out> sample(NaContext* nac);
 private:
  HashTable<Partition, Out> values;
};

template<typename Kernel>
void
HashOps<Kernel>::set(Partition input_partition, Out value)
{
  this->values[input_partition] = value;
}

template<typename Kernel>
void
HashOps<Kernel>::clear(Partition input_partition)
{
  auto it = this->values.find(input_partition);
  if (it != this->values.end())
    this->values.erase(it);
}

template<typename Kernel>
optional<typename HashOps<Kernel>::Out>
HashOps<Kernel>::sample(NaContext* nac)
{
  if (this->values.empty())
    return Kernel::empty_value;
  auto it = this->values.begin();
  Kernel kernel((it++)->second);
  while (it != this->values.end())
    kernel.accumulate((it++)->second, nac);
  return kernel.get();
}

// Like HashOps, but remember value insertion order so that we get
// consistent results from order-dependent aggregation kernels.

template<typename Out>
struct LinkRecord : NoCopy {
  explicit LinkRecord(Out value) noexcept
      : prev(this), next(this), value(value)
  {}

  ~LinkRecord() noexcept {
    this->unlink();
  }

  LinkRecord(LinkRecord&& other) noexcept
      : value(other.value)
  {
    if (other.prev == &other) {
      assert(other.next == &other);
      this->prev = this->next = this;
    } else {
      this->prev = other.next;
      assert(this->prev->next == &other);
      this->prev->next = this;
      this->next = other.next;
      assert(this->next->prev == &other);
      this->next->prev = this;
      other.prev = other.next = &other;
    }
  }

  LinkRecord& operator=(LinkRecord&& other) noexcept {
    if (other != &this) {
      this->~LinkRecord();
      new (this) LinkRecord(std::move(other));
    }
    return *this;
  }

  void insert_after(LinkRecord* other) noexcept {
    assert(other->prev == other);
    assert(other->next == other);
    other->next = this->next;
    this->next = other;
    other->prev = this;
    other->next->prev = other;
  }

  void insert_before(LinkRecord* other) noexcept {
    this->prev->insert_after(other);
  }

  LinkRecord* get_next() const noexcept {
    return this->next;
  }

  void unlink() noexcept {
    this->prev->next = this->next;
    this->next->prev = this->prev;
    this->prev = this->next = this;
  }

  bool is_linked() const noexcept {
    bool linked = this->prev != this;
    if (!linked)
      assert(this->next == this);
    return linked;
  }

  Out get_value() const noexcept {
    return this->value;
  }

  void set_value(Out value) noexcept {
    this->value = value;
  }

 private:
  LinkRecord* prev;
  LinkRecord* next;
  Out value;
};

template<typename Kernel>
struct OrderedOps final {
  using Out = typename Kernel::Out;
  inline void set(Partition input_partition, Out value);
  inline void clear(Partition input_partition);
  inline optional<Out> sample(NaContext* nac);
 private:
  using Record = LinkRecord<Out>;
  HashTable<Partition, Record> values;
  Record list_head = Record(0);
};

template<typename Kernel>
void
OrderedOps<Kernel>::set(Partition input_partition, Out value)
{
  auto [it, inserted] = this->values.try_emplace(input_partition, value);
  if (!inserted) {
    assert(it->second.is_linked());
    it->second.unlink();
    it->second.set_value(value);
  }
  assert(!it->second.is_linked());
  this->list_head.insert_before(&it->second);
}

template<typename Kernel>
void
OrderedOps<Kernel>::clear(Partition input_partition)
{
  auto it = this->values.find(input_partition);
  if (it != this->values.end())
    this->values.erase(it);
}

template<typename Kernel>
optional<typename OrderedOps<Kernel>::Out>
OrderedOps<Kernel>::sample(NaContext* nac)
{
  if (!this->list_head.is_linked()) {
    assert(this->values.empty());
    return Kernel::empty_value;
  }
  Record* r = this->list_head.get_next();
  assert(r != &this->list_head);
  size_t n = 1;
  Kernel kernel(r->get_value());
  for (r = r->get_next(); r != &this->list_head; r = r->get_next(), ++n)
    kernel.accumulate(r->get_value(), nac);
  assert(n == this->values.size());
  return kernel.get();
}

struct PivotNa {
  template<typename Value, typename Aggregation>
  static void agg_column(CommandIn ci,
                         pyref py_data,
                         pyref py_out,
                         NaContext nac,
                         QueryExecution* qe);
};

template<typename Value, typename Aggregation>
void
PivotNa::agg_column(CommandIn ci,
                    pyref py_data,
                    pyref py_out,
                    NaContext nac,
                    QueryExecution* qe)
{
  using Kernel = typename Aggregation::template Kernel<Value>;
  // TODO(dancol): add Ops implementation specialized for first and
  // last, which don't need per-input-partition slots at all..
  using Ops = std::conditional_t<
    std::is_base_of_v<NaOrderDependent, Kernel>,
    OrderedOps<Kernel>,
    HashOps<Kernel>>;
  using Out = typename Ops::Out;

  HashTable<Partition, Ops> output_partitions;
  npy_uint32 extra_op_flags[2] = { NPY_ITER_NO_BROADCAST, 0 };
  NpyIterConfig config;
  config.extra_op_flags = &extra_op_flags[0];
  PythonChunkIterator<Value, bool> data(py_data.notnull().addref(), config);
  ResultBuffer<Out, bool> out(py_out.notnull().addref(), qe);

  while (!ci.is_at_eof()) {
    switch (ci.next<SpCommand>()) {
      case SpCommand::SLURP_INPUT_PARTITION: {
        Partition ip = ci.next<Partition>();
        Partition op = ci.next<Partition>();
        Ops* ops = &output_partitions[op];
        if (data.is_at_eof())
          throw_pyerr_msg(PyExc_AssertionError, "data underflow");
        auto [value, mask] = data.get();
        if (mask)
          ops->clear(ip);
        else
          ops->set(ip, value);
        data.advance();
        break;
      }
      case SpCommand::CLEAR_INPUT_PARTITION: {
        Partition ip = ci.next<Partition>();
        Partition op = ci.next<Partition>();
        output_partitions[op].clear(ip);
        break;
      }
      case SpCommand::EMIT_OUTPUT_PARTITION: {
        Partition op = ci.next<Partition>();
        optional<Out> sample = output_partitions[op].sample(&nac);
        if (sample.has_value())
          out.add(*sample, /*masked=*/false);
        else
          out.add(Out(), /*masked=*/true);
        break;
      }
      case SpCommand::FORGET_OUTPUT_PARTITION: {
        Partition op = ci.next<Partition>();
        auto it = output_partitions.find(op);
        if (it != output_partitions.end())
          output_partitions.erase(it);
        break;
      }
    }
  }

  out.flush();
}

PyMethodDef functions[] = {
  make_methoddef("span_pivot_column",
                 wraperr<na_agg_impl<PivotNa>>(),
                 METH_VARARGS,
                 "Compute a column of a partition aggregation"),
  { 0 }
};

}  // anonymous namespace

void
init_span_pivot_column(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
