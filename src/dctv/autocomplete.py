# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Clowny attempt at syntactic completion for DCTV-SQL"""

import logging
import re
from functools import partial
from itertools import chain
from cytoolz import identity

from ply.lex import LexToken

from modernmp.util import (
  ReferenceCountTracker,
  assert_seq_type,
  the,
)

from .util import (
  ExplicitInheritance,
  cached_property,
  final,
  override,
)

from .query import (
  InvalidQueryException,
)

from .sql import (
  AstNode,
  ColumnReference,
  DmlAction,
  DmlContext,
  Namespace,
  TableReference,
  TableValuedFunction,
  get_sql_documentation,
)

from .sql_parser import (
  SqlLexer,
  SqlParsingException,
  allow_error_recovery,
  make_dml_parser,
  make_lexer,
)

log = logging.getLogger(__name__)

def _end(token):
  return token.lexpos + len(token.value)

def _fake(tokens, type, value, lexpos):  # pylint: disable=redefined-builtin
  fake_token = LexToken()
  fake_token.type = the(str, type)
  fake_token.value = the(str, value)
  fake_token.lineno = tokens[-1].lineno if tokens else 1
  if tokens:
    assert lexpos >= _end(tokens[-1])
  fake_token.lexpos = the(int, lexpos)
  return fake_token

def _fake_tokenfunc(tokens):
  def _tokenfunc(tokens):
    yield from tokens
    while True:
      yield None
  return partial(next, _tokenfunc(tokens))

# TODO(dancol): this string shouldn't appear in user queries, but we
# should try harder to make sure.
_BAREWORD_MARKER = "__BAREWORD__"

def _is_path_prefix_of_path(prefix, path):
  if len(path) < len(prefix):
    return False
  for l, r in zip(prefix, path):
    if l.lower() != r.lower():
      return False
  return True

def _find_bareword_lexical_context(node, path=None):
  """Find a path from the root of NODE to the special bareword marker"""
  assert isinstance(node, AstNode)
  if path is None:
    path = []
  path.append(node)
  # TODO(dancol): put some logic in AstNode instead of hacking
  # together a typecase.
  if (isinstance(node, ColumnReference) and
      (node.column == _BAREWORD_MARKER or
       _BAREWORD_MARKER in node.path)):
    return path
  if (isinstance(node, TableReference) and
      _BAREWORD_MARKER in node.path):
    return path
  for child in node.get_children():
    child_path = _find_bareword_lexical_context(child, path)
    if child_path:
      return child_path
  path.pop()
  return None

# Same-kind brackets must have same position
_OPEN_PAREN =  "({["  # pylint: disable=bad-whitespace
_CLOSE_PAREN = ")}]"

def _try_ast_1(head, bw, tail):
  # Try to make parentheses balance.  We don't have to worry about
  # brackets in strings and comments: the lexer skipped those.
  # First, figure out what's still open in the head.
  lparen_stack = []
  for token in head:
    if token.type in _OPEN_PAREN:
      lparen_stack.append(token.type)
    if token.type in _CLOSE_PAREN:
      if lparen_stack:
        lparen_stack.pop()
  rdepth = len(lparen_stack)
  for token in tail:
    if token.type in _OPEN_PAREN:
      rdepth += 1
    if token.type in _CLOSE_PAREN:
      rdepth -= 1
  # If rdepth is zero, head and tail are balanced.  If rdepth is
  # positive, head opens more than tail closes so we should insert
  # synthetic closers.  We could insert synthetic opens for imbalance
  # the other way, but that should be rare.
  if rdepth > 0:
    tail = list(tail)
    for opener in reversed(lparen_stack[-rdepth:]):
      closer = _CLOSE_PAREN[_OPEN_PAREN.index(opener)]
      tail.insert(0, _fake(head, closer, closer, bw.lexpos))
  try:
    return make_dml_parser().parse(
      tokenfunc=_fake_tokenfunc(chain(head, [bw], tail)))
  except (SqlParsingException, InvalidQueryException):  # pylint: disable=overlapping-except
    return None

class DisgustingCompletionHack(BaseException):
  """You don't want to know"""

class InvalidAutoCompleteContextError(ValueError):
  """Exception raised for attempting autocompletion in string"""

@final
class DctvSqlAutoCompleter(ExplicitInheritance):
  """Autocompletion machinery for DCTV SQL

  These objects are intended to be ephemeral, used for one occasion of
  auto-completion.
  """

  @override
  def __init__(self, sql, point, namespace, *,
               bare_table_reference=False,
               decorate=True,
               ):
    """Create a new autocompleter.

    SQL is the string to complete.  POINT is the position of point (in
    range [0, len(SQL)]) in SQL.  NAMESPACE is the SQL namespace in
    effect for evaluation of SQL.

    If BARE_TABLE_REFERENCE is true, parse the input text as an
    explicit table-namespace reference instead of as a full SQL
    expression.

    If DECORATE is true, annotate completions with brackets and other
    syntactic addons.
    """
    self.__sql = the(str, sql)
    self.__point = the(int, point)
    self.__namespace = the(Namespace, namespace)
    self.__bare_table_reference = bare_table_reference
    self.__decorate = decorate

  @cached_property
  def __token_info(self):
    lexer = make_lexer()
    lexer.input(self.__sql)
    tokens = []
    tail_token = None
    point = self.__point
    with allow_error_recovery():
      while True:
        token = lexer.token()
        if not token:
          break
        if token.lexpos > point:
          tail_token = token
          break
        tokens.append(token)
    last_token_end = -1 if not tokens else _end(tokens[-1])

    # If point is exactly at the end of a token, consider that token
    # to be the completion token if that token is variable-length.
    # If point is immediately after a fixed-width token like ".", we
    # want to start a new token for completion purposes instead.
    # How do we tell whether a token is variable-length?  We can't
    # without complex regex analysis.  Instead, just see whether the
    # token matches the regex regex and whether it wouldn't match if
    # we glommed an "a" on the end.
    if (last_token_end == point and
        len(tokens[-1].type) == 1 and
        tokens[-1].type in SqlLexer.literals):
      last_token_end = -1
    if last_token_end == point:
      token_re = getattr(SqlLexer, "t_" + tokens[-1].type, None)
      if isinstance(token_re, str):
        token_re = "^(?:" + token_re + ")$"
        if (re.match(token_re, tokens[-1].value) and
            not re.match(token_re, tokens[-1].value + "a")):
          last_token_end = -1

    if last_token_end < point:
      # If we're not completing any specific token, add synthetic
      # empty token to represent the completion.
      tokens.append(_fake(tokens, "fake", "", point))
    else:
      # Turn the last token into a fake token
      last_token = tokens[-1]
      if ((last_token.type == "BAREWORD" and
           last_token.value[0] in "\"'`")
          or last_token.type in ("SINGLE_QUOTED_STRING",
                                 "DOUBLE_QUOTED_STRING",
                                 "BACK_QUOTED_STRING")):
        raise InvalidAutoCompleteContextError
      last_token.type = "fake"
      last_token.value = last_token.value[:point - last_token.lexpos]

    assert tokens
    assert _end(tokens[-1]) >= point

    # If we have more tokens to parse, let callers do it, but only on
    # demand.  (This way, we don't bother lexing the rest of the input
    # when we don't need to do identifier completion.)
    if tail_token:
      def _gen_tail():
        yield tail_token
        while True:
          token = lexer.token()
          if not token:
            break
          yield token
    else:
      _gen_tail = lambda: ()
    return tuple(tokens), _gen_tail

  @property
  def __head(self):
    return self.__token_info[0]

  @cached_property
  def __tail(self):
    return tuple(self.__token_info[1]())

  @cached_property
  def completion_token(self):
    """String value of the token we're completing.

    Return the empty string if we're completing a new token.  If we're
    completing the middle of a token, just return the part of the
    token up to point.
    """
    token = self.__head[-1]
    assert token.type == "fake"
    assert _end(token) == self.__point
    return token.value

  @cached_property
  def __fake_tokens(self):
    return list(self.__head)

  def __try_parse(self, type):  # pylint: disable=redefined-builtin
    tokens = self.__fake_tokens
    del tokens[len(self.__head) - 1:]
    fake_token = _fake(tokens, type, type, self.__point)
    tokens.append(fake_token)
    try:
      make_dml_parser().parse(tokenfunc=_fake_tokenfunc(tokens))
    except SqlParsingException as ex:
      if ex.lexpos is None:  # EOF
        return True
      return False
    else:
      return True

  def complete_keywords(self):
    """Generate keyword completions

    Each match is a bare string (in all UPPERCASE) naming the keyword
    matched.  Context-sensitive keywords that could match as
    identifiers are not returned at all.  Check only keywords matching
    the completion prefix.  Case insensitive.  Yield keywords in
    sorted order.
    """
    ct_uc = self.completion_token.upper()
    exclude = SqlLexer.non_reserved_keywords \
      if self.__try_parse("BAREWORD") else frozenset()
    for keyword in sorted(SqlLexer.keywords):
      # Don't bother trying keywords that won't match the token we're
      # completing.  Completing non-reserved keywords as keywords is
      # confusing if we can also match an identifier in the current
      # position, so skip non-reserved keywords in this situation.
      assert keyword.upper() == keyword, "keywords should be UPPERCASE"
      if (keyword.startswith(ct_uc)
          and keyword not in exclude
          and self.__try_parse(keyword)):
        yield keyword, None

  def __guess_ast(self):
    assert self.__head[-1].type == "fake"
    head = self.__head[:-1]
    bw = _fake(head, "BAREWORD", _BAREWORD_MARKER, self.__point)
    tail = self.__tail
    ast = _try_ast_1(head, bw, tail)
    if ast is not None:
      return ast
    return _try_ast_1(head, bw, ())

  def __get_lexenv_from_table_reference(self, lexical_context):
    if self.__bare_table_reference:
      assert len(lexical_context) == 1
      assert isinstance(lexical_context[0], TableReference)
      return DmlContext(self.__namespace).make_tctx().lexenv
    dml_action = lexical_context[0]
    assert isinstance(dml_action, DmlAction)
    tr = lexical_context[-1]
    assert isinstance(tr, TableReference)
    def _hook_self_resolve_te(ctx):
      raise DisgustingCompletionHack(ctx.tctx.lexenv)
    tr.__dict__["self_resolve_te"] = _hook_self_resolve_te
    lexenv = None
    try:
      dml_action.eval_for_autocomplete(DmlContext(self.__namespace))
    except DisgustingCompletionHack as ex:
      lexenv = ex.args[0]
    except:
      pass
    return lexenv

  def __complete_table_reference(self, lexical_context):
    lexenv = self.__get_lexenv_from_table_reference(lexical_context)
    if not lexenv:
      return
    tr = lexical_context[-1]
    assert isinstance(tr, TableReference)
    path = list(tr.path)
    del path[path.index(_BAREWORD_MARKER):]
    obj = lexenv
    for node in path:
      try:
        obj = obj.lookup_sql_attribute(node)  # pylint: disable=no-member
      except KeyError:
        return
    ct_lc = self.completion_token.lower()
    for name in sorted(obj.enumerate_sql_attributes()):
      if name.startswith("__colfunc_"):
        continue  # Ignore gross hack
      if not name.lower().startswith(ct_lc):
        continue  # Limit to matches
      value = obj.lookup_sql_attribute(name)
      if isinstance(value, TableValuedFunction):
        if self.__decorate:
          name += "("
      elif isinstance(value, Namespace):
        if value.disable_autocomplete:
          continue
        name += "."
      yield name, get_sql_documentation(value)

  def __get_resolver_from_column_reference(self, lexical_context):
    dml_action = lexical_context[0]
    assert isinstance(dml_action, DmlAction)
    cr = lexical_context[-1]
    assert isinstance(cr, ColumnReference)
    def _hook_self_resolve(ctx, resolver):
      raise DisgustingCompletionHack((ctx, resolver))
    cr.__dict__["self_resolve"] = _hook_self_resolve
    info = None
    try:
      dml_action.eval_for_autocomplete(DmlContext(self.__namespace))
    except DisgustingCompletionHack as ex:
      info = ex.args[0]
    except BaseException as ex:
      pass
    return info

  def __get_all_legal_column_refs(self, ctx, resolver):  # pylint: disable=no-self-use
    paths = frozenset(resolver.gen_table_paths(ctx))
    if __debug__:
      for path in paths:
        assert assert_seq_type(tuple, str, path)
    matches = ReferenceCountTracker()
    for path in paths:
      for column, _te in resolver.gen_wildcard_matches(ctx, path):
        matches.addref((path, column))
    # Skip refs with more than one match because those would be
    # illegal due to ambiguity
    return [match for match, count in matches.items() if count == 1]

  def __complete_column_reference(self, lexical_context):
    info = self.__get_resolver_from_column_reference(lexical_context)
    if not info:
      return
    ctx, resolver = info
    cr = lexical_context[-1]
    assert isinstance(cr, ColumnReference)
    prefix_path = list(cr.path)
    if _BAREWORD_MARKER in prefix_path:
      del prefix_path[prefix_path.index(_BAREWORD_MARKER):]
    else:
      assert cr.column == _BAREWORD_MARKER
    # Each completion is either a table or a column reference.
    # Each table reference ends with a ".".
    ct_lc = self.completion_token.lower()

    # Track yielded items: we won't yield duplicate column references
    # (those would be illegal) but we will yield duplicate
    # table names.
    seen = set()

    for path, column in self.__get_all_legal_column_refs(ctx, resolver):
      if not _is_path_prefix_of_path(prefix_path, path):
        continue
      if len(path) == len(prefix_path):
        ent = column
      elif len(path) > len(prefix_path):
        ent = path[len(prefix_path)] + "."
      else:
        continue
      # TODO(dancol): get quoting right
      ent = ent.lower()
      if not ent.startswith(ct_lc):
        continue
      # TODO(dancol): look up table documentation
      if ent not in seen:
        seen.add(ent)
        yield ent, None

  def __make_bare_table_reference_ast(self):
    assert self.__bare_table_reference
    path = []
    for i, token in enumerate(self.__head):
      if not i % 2:
        if token.type not in ("BAREWORD", "fake"):
          return None
        path.append(
          token.value if token.type == "BAREWORD" else _BAREWORD_MARKER)
      elif token.type != ".":
        return None
    return [TableReference(path)]

  def complete_identifiers(self):
    """Complete legal identifiers.  Mostly guesswork.

    Try to guess what identifiers might be legal at the completion
    position.  We can't complete identifiers as reliably as we can
    complete keywords because determining the set of legal identifiers
    requires a getting a valid AST and potentially calling into TVFs,
    which might be impossible under various editing scenarios.
    """
    # Complete identifiers only if we can put an identifier in this
    # syntactic position.
    if self.__bare_table_reference:
      dml_asts = self.__make_bare_table_reference_ast()
    else:
      if not self.__try_parse("BAREWORD"):
        return  # No identifiers legal here
      # Try to find a parse tree that includes the token
      # we're completing.
      dml_asts = self.__guess_ast()

    if not dml_asts:
      return  # Can't parse
    lexical_context = next(
      filter(identity, map(_find_bareword_lexical_context, dml_asts)),
      None)
    if not lexical_context:
      return  # Weird tree
    leaf = lexical_context[-1]
    # Now complete the identifier based on the kind of hole in which
    # we've placed the completion-shaped peg.
    if isinstance(leaf, TableReference):
      yield from self.__complete_table_reference(lexical_context)
    elif isinstance(leaf, ColumnReference):
      yield from self.__complete_column_reference(lexical_context)

  def complete(self):
    """Generate completions

    Yield a list of suggestions, each a string.  An empty list is a
    legal return value.  This function tries not to raise; instead, it
    returns an empty list if it gets confused.  But it may
    raise InvalidAutoCompleteContextError.
    """
    # Ugh. Figure out the legal keywords at point by trying all of
    # them and seeing which ones don't cause the parser to barf.
    assert self.__head[-1].type == "fake"
    # TODO(dancol): apply style
    for completion, doc in self.complete_identifiers():
      yield "I", completion, doc
    if not self.__bare_table_reference:
      for completion, doc in self.complete_keywords():
        yield "K", completion, doc
