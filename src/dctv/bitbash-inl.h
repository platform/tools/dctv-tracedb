// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename T>
int
fls(T value) noexcept
{
  static_assert(std::is_integral_v<T>);
  if (!value)
    return 0;
  using UT = std::make_unsigned_t<T>;
  UT xval = static_cast<UT>(value);
  static_assert(sizeof (T) <= sizeof (unsigned long long));
  int sz;
  int leading_zeroes;
  if (sizeof (UT) <= sizeof (unsigned int)) {
    sz = sizeof (unsigned int);
    leading_zeroes = __builtin_clz(static_cast<unsigned int>(xval));
  } else if (sizeof (UT) <= sizeof(unsigned long)) {
    sz = sizeof (unsigned long);
    leading_zeroes = __builtin_clzl(static_cast<unsigned long>(xval));
  } else {
    sz = sizeof (unsigned long long);
    leading_zeroes =
        __builtin_clzll(static_cast<unsigned long long>(xval));
  }
  return CHAR_BIT * sz - leading_zeroes;
}

template<typename T>
int
ffs(T value) noexcept
{
  static_assert(std::is_integral_v<T>);
  if (!value)
    return 0;
  using ST = std::make_signed_t<T>;
  ST xval = static_cast<ST>(value);
  static_assert(sizeof (ST) <= sizeof (long long));
  if (sizeof (ST) <= sizeof (int))
    return __builtin_ffs(xval);
  if (sizeof (ST) <= sizeof (long))
    return __builtin_ffsl(xval);
  return __builtin_ffsll(xval);
}

// Round NR down to the previous multiple of THE_POW2.  THE_POW2 must
// be a power of two.  Zero is not a power of two.
template<typename T>
T
pow2_round_down(T nr, T the_pow2) noexcept
{
  assume(the_pow2 != 0 && is_pow2(the_pow2));
  assume(is_pow2(the_pow2));
  return nr & ~(the_pow2 - 1);
}

// Round NR up to the next multiple of THE_POW2.  THE_POW2 must be a
// power of two.  Zero is not a power of two.
template<typename T>
T
pow2_round_up(T nr, T the_pow2) noexcept
{
  static_assert(std::is_unsigned_v<T>);
  assume(is_pow2(the_pow2));
  return pow2_round_down(nr + the_pow2 - 1, the_pow2);
}

template<typename T>
T
pow2_divide(T dividend, T divisor_pow2) noexcept
{
  static_assert(std::is_unsigned_v<T>);
  assume(is_pow2(divisor_pow2));
  return dividend >> (ffs(divisor_pow2) - 1);
}

template<typename T>
bool
is_pow2(T nr) noexcept
{
  static_assert(std::is_unsigned_v<T>);
  return nr != 0 && !(nr & (nr - 1));
}

}  // namespace dctv
