// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <memory>

#include "io_spec.h"
#include "pyutil.h"
#include "query.h"
#include "vector.h"

namespace dctv {

struct QeCallResizeBuffers final : QeCall {
  QeCallResizeBuffers(const OperatorContext* oc,
                      std::unique_ptr<QeCall>* self_ptr,
                      std::unique_ptr<QeCall> prev_qe_call);
  void refresh_resize_buffers(
      const OperatorContext* oc,
      std::unique_ptr<QeCall>* self_ptr) override;
  Score compute_score(const OperatorContext* oc) const override;
  unique_pyref do_it(OperatorContext* oc) override;
  int py_traverse(visitproc visit, void* arg) const noexcept override;

  // No need to override QeCall::setup() --- prev_qe_call is already
  // set up.

 private:
  QeCallResizeBuffers() = default;
  bool do_try_writes();
  std::unique_ptr<QeCall>* self_ptr;
  std::unique_ptr<QeCall> prev_qe_call;
  Vector<IoSpec> io_specs;
};

inline void init_qe_call_resize_buffers(pyref m) {}

}  // namespace dctv
