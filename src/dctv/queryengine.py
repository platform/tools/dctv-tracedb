# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""The world's shittiest query language

The classes in this file define a combinator system for building
queries against trace records. The query system described here is
basically purpose-built for trace analysis and the GUI's PivotTable.

We need something like this because we want to be able to efficiently
optimize query trees, cache them based on immutable descriptions of
the results to be produced, and automatically populate a UI based on
sane default views of the queries produced.

Queries themselves generate one-dimensional arrays of data; we specify
them using QueryNode objects, which can take a variety of
inputs. Grouped of labeled queries come from QueryTable objects, which
you can basically think of as QueryNode factories.

Query descriptions are all immutable in order to facilitate their use
as cache keys.
"""

import logging
from collections import defaultdict

import numpy as np

from modernmp.util import (
  assert_seq_type,
  ignore,
  the,
)

from .query import (
  QueryAction,
  QueryNode,
  _RestartQueryException,
)

from .queryplan import (
  make_action_graph,
  plan_query_minstar,
)

from .util import (
  AutoNumber,
  ExplicitInheritance,
  enumattr,
  final,
  iattr,
  override,
  sattr,
)

from ._native import (
  QueryCache,
  QueryExecution,
  StringTable,
  npy_has_mask,
)

log = logging.getLogger(__name__)

class Delivery(AutoNumber):
  """Delivery hint for query execution"""

  ARBITRARY = ()
  """Deliver result blocks in arbitrary order"""
  ROWWISE = ()
  """Deliver result blocks row-by-row."""
  COLUMNWISE = ()
  """Deliver results column-by-column."""

@final
class DeliveryAction(QueryAction):
  """Deliver wanted data to its eventual consumer.

  While QueryExecution will faithfully execute the QueryAction plan we
  give it, it doesn't *do* anything with what it computes.  In order
  to the query calculation to be useful, it needs to be shipped
  outside the QueryExecution system, and that's what this class does:
  it slurps up query results and yields them to the caller, making
  these results useful and visible outside the island universe of
  QueryExecution.
  """
  wanted = iattr(QueryNode)
  delivery_hint = enumattr(Delivery)
  precious = True

  __inherit__ = dict(precious=override)

  @override
  def _compute_inputs(self):
    return (self.wanted,)

  @override
  def _compute_outputs(self):
    return ()

  @override
  async def run_async(self, qe):
    query = self.wanted
    (ic,), _ = await qe.async_setup((query,), ())
    if self.delivery_hint == Delivery.COLUMNWISE:
      block = await ic.read_all()
      req = qe.async_yield_to_query_runner((query, block, True))
      del block
      await req
    else:
      is_eof = False
      while not is_eof:
        block = await ic.read(qe.block_size)
        is_eof = len(block) < qe.block_size
        req = qe.async_yield_to_query_runner((query, block, is_eof))
        del block
        await req

@final
class RowwiseDeliveryAction(QueryAction):
  """Delivery action for delivering queries row-by-row"""
  wanted = sattr(QueryNode)

  @override
  def _compute_inputs(self):
    return self.wanted

  @override
  def _compute_outputs(self):
    return ()

  @override
  async def run_async(self, qe):
    ics, [] = await qe.async_setup(self.wanted, ())
    while ics:
      blocks = await qe.async_io(*[ic.read(qe.block_size) for ic in ics])
      new_ics = []
      for ic, block in zip(ics, blocks):
        is_eof = len(block) < qe.block_size
        await qe.async_yield_to_query_runner((ic.query, block, is_eof))
        if not is_eof:
          new_ics.append(ic)
      ics = new_ics

@final
class QueryEngine(ExplicitInheritance):
  """Environment for executing queries.

  Users should subclass and provide any functionality used by
  special-purpose QueryNode objects, which get a reference to their
  QueryEngine during execution.
  """

  @override
  def __init__(self, cache, *, env=None):
    """Make a new query engine.

    CACHE is a QueryCache instance.

    ENV is a dict that provides the query environment.
    """
    self.qc = the(QueryCache, cache)
    self.st = the(StringTable, self.qc.st)
    self.__env = env or {}

  def __execute_1(self,
                  wanted_queries,
                  progress_callback,
                  delivery_hint,
                  yielded_blocks,
                  perf_callback):
    block_numbers_by_query = {query: 0 for query in wanted_queries}
    if delivery_hint == Delivery.ROWWISE:
      seed_actions = [RowwiseDeliveryAction(wanted_queries)]
    else:
      seed_actions = [DeliveryAction(q, delivery_hint)
                      for q in wanted_queries]
    dag = make_action_graph(seed_actions, cached={})
    for query, block, is_eof in QueryExecution.make(
        plan=plan_query_minstar(dag),
        env=self.__env,
        qc=self.qc,
        progress_callback=progress_callback,
        perf_callback=perf_callback):
      block_number = block_numbers_by_query[query]
      block_numbers_by_query[query] += 1
      if is_eof:
        wanted_queries.remove(query)
      key = (query, block_number)
      if key not in yielded_blocks:
        yielded_blocks.add(key)
        yield query, block.as_array(), is_eof

  def execute(self,
              wanted_queries,
              *,
              progress_callback=ignore,
              delivery_hint=Delivery.ARBITRARY,
              perf_callback=None):
    """Query the trace database.

    WANTED_QUERIES is a sequence of QueryNode objects.

    PROGRESS_CALLBACK is an optional progress callback that receives
    information about query execution.  See the documentation for
    progress callbacks generally.

    Yield (QUERY, ARRAY, IS_EOF) tuples, where QUERY is one of the
    entries in WANTED_QUERIES, ARRAY is a chunk of the result vector
    for QUERY (either an ndarray or a masked_array), and IS_EOF is a
    boolean indicating whether ARRAY is the last ARRAY for QUERY.
    Every query in WANTED_QUERIES yields at least one output ARRAY
    even if, internally, it generates no blocks.  (We make up an array
    out of thin air in this case.)

    With respect to a given QUERY, result arrays arrive in order.
    However, we may interleave delivery of different blocks of
    different QUERY objects.  For example, we might deliver (Q1, B1),
    (Q1, B2), then (Q2, B1), then (Q1, B3), and finally (Q2, B2).

    The previous paragraph applies in the general case, but the value
    of the DELIVERY_HINT parameter may constrain query result
    delivery.  If DELIVERY_HINT is Delivery.ROWWISE, then we deliver
    blocks from all wanted queries in lockstep.  If DELIVERY_HINT is
    Delivery.COLUMNWISE, then we deliver the result of each wanted
    query as a single big block.  If DELIVERY_HINT is
    Delivery.ARBITRARY (the default) then we deliver result blocks in
    arbitrary order, subject to the above general constraint.

    In any case, DELIVERY_HINT is just a hint.  Callers must expect
    the query engine to produce results according to the above
    contract.

    If PERF_CALLBACK is non-None, it's called with PERF_RECORD object,
    once per completed question action, where each PERF_RECORD is a
    tuple (QUERY_ACTION, PERF_INFO), QUERY_ACTION is a QueryAction we
    used to generate the query; PERF_INFO a tuple of key-value tuple
    pairs containing various performance-related fields.
    """

    # N.B. for various internal reasons, query execution may fail at
    # any time with a "please restart me" exception.  We insulate
    # callers from query restarts by tracking which blocks we've
    # already delivered and skipping any such blocks on query
    # re-execution.

    wanted_queries = set(wanted_queries)
    assert assert_seq_type(set, QueryNode, wanted_queries)
    yielded_blocks = set()  # Persist across __execute_1 calls!
    while True:
      try:
        yield from self.__execute_1(wanted_queries,
                                    progress_callback,
                                    delivery_hint,
                                    yielded_blocks,
                                    perf_callback)
      except _RestartQueryException as ex:
        log.debug("restarting query: %r", ex)
      else:
        break
    assert not wanted_queries, \
      "should have yielded all results: missing {!r}".format(
        sorted(wanted_queries))

  def execute_for_columns(self, wanted_queries,
                          *,
                          delivery_hint=Delivery.COLUMNWISE,
                          **kwargs):
    """Execute query column-by-column yielding arrays

    Yield, in arbitrary order, (QUERY, ARRAY) pairs, where ARRAY is
    the entire result vector for QUERY.
    """
    results = defaultdict(list)
    def _deliver(query):
      arrays = results.pop(query)
      assert len(arrays) >= 1
      if len(arrays) == 1:
        return arrays[0]
      if any(npy_has_mask(array) for array in arrays):
        return np.ma.concatenate(arrays)
      return np.concatenate(arrays)
    for query, array, is_eof in self.execute(
        wanted_queries,
        delivery_hint=delivery_hint,
        **kwargs):
      # pylint: disable=len-as-condition
      if len(array) or query not in results:
        results[query].append(array)
      del array
      if is_eof:
        yield query, _deliver(query)
