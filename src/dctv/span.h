// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include "pyutil.h"

namespace dctv {

// We used to use templates for these types, but there's really no
// point: we're going to be using int64 for these until the end
// of time.

using TimeStamp = int64_t;
using Duration = int64_t;
using Partition = int64_t;
using CloneFlags = int64_t;
using IdValue = int32_t;
using OomScore = int32_t;
using SnapshotNumber = int32_t;
using MemorySize = int64_t;
using RawThreadStateFlags = uint16_t;

// TODO(dancol): probably reduce to uint8_t
using CpuNumber = int32_t;

String ts2tracets(TimeStamp ts);
void init_span(pyref m);

}  // namespace dctv
