-- This file initializes a newly-mounted trace namespace.

-- TODO(dancol): pre-parse this file during DCTV build so that we
-- don't have to parse it on the first snapshot mount.

-- The DCTV core pre-populates some especially magical entries in this
-- namespace:
--
--   first_ts
--   last_ts
--   raw_events.*
--   cooked_events.*
--   metadata.*

-- TODO(dancol): assert that these magical tables exist.

-- Low-level internal primitives.

-- TODO(dancol): do we want some kind of "enter namespace" SQL
-- primitive to cut down on syntactic noise below?

-- Event table of all snapshot ends
CREATE VIEW internal.snapshot_ends AS
  SELECT EVENT snapshot_id AS number
    FROM raw_events.dctv_snapshot_end;

-- Singleton event table of the first snapshot end.  We left-truncate
-- "cooked" traces to this timestamp so further analysis can look at a
-- coherent baseline.
CREATE VIEW internal.first_snapshot_end AS
  SELECT _ts FROM internal.snapshot_ends ORDER BY _ts LIMIT 1;

-- Event table of thread deaths according to sched_switch switch-away
-- from dead threads.  We prefer using this event source to
-- sched_process_exit because a dying thread runs a little while after
-- sched_process_exit doing final cleanup before switching to the next
-- process, and we want to avoid creating an artifact whereby a thread
-- that's supposed to be dead is running on some core.
CREATE VIEW internal.raw_thread_death_ss AS
  SELECT EVENT prev_tid AS tid
    FROM raw_events.sched_switch
   WHERE prev_state&0x7f == 64 /* 64==TASK_DEAD */;

-- Event table of thread deaths according to sched_process_exit.
-- Used for experimentation right now.
CREATE VIEW internal.raw_thread_death_spe AS
  SELECT EVENT exiting_tid AS tid
    FROM raw_events.sched_process_exit;

-- Thread analysis object that examines the shape and history of the
-- process tree and helps us prepare the "cooked" event table for the
-- rest of the analysis.
CREATE VIEW internal.ta AS dctv.thread_analysis(
  sched_process_exit=>internal.raw_thread_death_ss,
  task_newtask=>(
    SELECT EVENT tid AS tid, new_tid, clone_flags
      FROM raw_events.task_newtask
  ),
  snapshot_start=>(
    SELECT EVENT snapshot_id AS number
      FROM raw_events.dctv_snapshot_start
  ),
  snapshot_end=>internal.snapshot_ends,
  first_ts=>first_ts,
  last_ts=>last_ts,
  get_snapshot_data=>internal.get_snapshot_data,
  nr_cpus=>metadata.nr_cpus,
  -- TODO(dancol): snap samples to snapshot end
  -- TODO(dancol): synthesize state samples
);

-- Mapping from MSB bit index (as given by the FLS function) to raw
-- Linux thread state.  We have an eight-bit value.

CREATE VIEW internal.raw_linux_thread_states AS
  SELECT assert_unique(col0, verify=>FALSE) AS task_state_index,
         assert_unique(col1, verify=>FALSE) AS task_state_name,
    FROM (VALUES
          (0, '+'), -- A runnable but yet running thread
          (1, 'S'), -- Sleeping
          (2, 'D'), -- Uninterruptible sleep (usually disk)
          (3, 'T'), -- Stopped (SIGSTOP?)
          (4, 't'), -- ptraced
          -- The Linux mappings follow
          -- (5, 'R'), -- EXIT_DEAD
          -- (6, 'X'), -- EXIT_ZOMBIE
          -- (7, 'X'), -- TASK_DEAD
          -- We use the bits for our own analysis however
          (5, 'R'), -- An actually-running thread as inferred from SS
    );

-- Translate raw TIDs and TGIDs to the unique values that the
-- thread_analysis module spits out.  A TID of numeric zero gets
-- translated to the synthetic idler thread for the core logging
-- the event, which seems reasonable.  Likewise, a TGID of numeric
-- zero gets translated to the system-wide idler process.

-- N.B. The DCTV core automagically invokes remap_raw_tid and
-- remap_raw_tgid on raw event columns marked with the raw_tid and
-- raw_tgid domains, respectively.

CREATE VIEW FUNCTION internal.remap_raw_tid AS
  SELECT IF(t.raw_tid==0,
            utid_of_cpu_idler(t.cpu),
            utid) AS utid
    FROM ? AS t EVENT LEFT JOIN SPANS internal.ta.threads;

CREATE VIEW FUNCTION internal.remap_raw_tgid AS
  SELECT IF(t.raw_tgid==0,
            utgid_of_idler(),
            utgid)
    FROM ? AS t EVENT LEFT JOIN SPANS internal.ta.processes;


-- Thread lifetime analysis.

-- TODO(dancol): do we want to represent dead threads as being in the
-- 'x' state or do we want to just omit their spans entirely?  One can
-- convert from one representation to the other pretty easily.

-- TODO(dancol): if sched_switch isn't available, rely exclusively on
-- sched_process_exit.

-- Event table giving thread deaths by tid (translated).

CREATE VIEW internal.thread_deaths_p_tid AS
  SELECT EVENT * FROM dctv.internal.cast_as_event_table(
    (SELECT _ts + live_duration AS _ts, utid AS tid
       FROM internal.ta.threads),
    `partition`=>'tid',
    verify=>FALSE,
    sort=>FALSE,
    sorting=>'none',
  );

-- Thread states observed from snapshots
CREATE VIEW internal.sampled_thread_states_p_tid AS
  SELECT EVENT PARTITIONED BY utid state FROM internal.ta.state;

-- Observed thread state switch events
CREATE VIEW internal.switched_from_states_p_tid AS
  SELECT EVENT PARTITIONED BY utid, prev_state AS state
    FROM cooked_events.sched_switch AS t(prev_tid=>utid)
   WHERE utid IS NOT NULL AND (state & 64 == 0);

-- Event table giving times at which threads began running
CREATE VIEW internal.switched_to_p_tid AS
  SELECT EVENT PARTITIONED BY tid
    FROM cooked_events.sched_switch AS t(next_tid=>tid)
   WHERE tid IS NOT NULL;

-- Recast the thread analysis view as a view of events
CREATE VIEW internal.cooked_thread_creations AS
  SELECT EVENT * FROM dctv.internal.cast_as_event_table(
    (SELECT _ts, utid AS tid FROM internal.ta.threads),
    `partition`=>'tid',
    verify=>FALSE,
    sort=>FALSE,
    sorting=>'none',
  );

-- First stage of thread event inference: decode the raw sequence of
-- events into event-source-specific columns.  We're not trying to
-- decode numeric state names here.
CREATE VIEW internal.state_p_tid_1 AS
  SELECT SPAN * FROM dctv.time_series_to_spans(
    `partition`=>'tid',
    sources=>[
      {
        -- Observed thread creations.  We consider a thread runnable
        -- as soon as it's created.
        nickname=>'new_tasks',
        source=>internal.cooked_thread_creations,
        -- The priority here says that other sources of thread state
        -- listed below should override the sampled state, which is
        -- important because sampled_states is emitted only at the end
        -- of a snapshot, by which time it can be out-of-date.
        priority=>1,
      },
      {
        -- Explicit state samples from snapshots.
        nickname=>'sampled_states',
        source=>internal.sampled_thread_states_p_tid,
        -- new_tasks and sampled_states have the same lower priority,
        -- but because sampled_states comes later in the sources list,
        -- it'll override new_tasks so that snapshot samples update
        -- the thread state even if the sample appears to occur at
        -- precisely the same instant as thread creation at the end of
        -- a snapshot.
        priority=>1,
      },
      {
        -- Explicit thread wakeups.  We consider a woken
        -- thread runnable.
        nickname=>'wakeups',
        source=>(
          SELECT EVENT PARTITIONED BY tid
            FROM cooked_events.sched_waking AS t(woken_tid=>tid)
           WHERE tid is not NULL
        ),
      },
      {
        -- A stream of thread state updates from sched_switch: we get
        -- one of these every time the kernel switches *away* from a
        -- thread, leaving the previous thread in some state that
        -- isn't running.
        nickname=>'switched_from_states',
        source=>internal.switched_from_states_p_tid,
      },
      {
        -- An observed thread death terminates the spans for a thread.
        -- If we have sched_switch available, the thread death is the
        -- x-state switch-away upon thread termination and not
        -- sched_process_exit.
        -- TODO(dancol): test what happens in the
        -- execve-in-non-tg-leader case!
        nickname=>'deaths',
        source=>internal.thread_deaths_p_tid,
        role=>'stop',
      },
      {
        -- Close all live thread spans at the end of the trace.
        source=>last_ts,
        role=>'stop_broadcast',
      },
    ],
    columns=>[
      {
        column=>'new_task_flag',
        source=>'new_tasks',
        source_column=>'tid',
      },
      {
        column=>'wakeup_flag',
        source=>'wakeups',
        source_column=>'tid',
      },
      {
        column=>'sampled_state',
        source=>'sampled_states',
        source_column=>'state',
      },
      {
        column=>'switched_from_state',
        source=>'switched_from_states',
        source_column=>'state',
      },
    ],
  );

-- Combine the source-specific columns from the raw
-- time_series_to_spans into a single column that represents our best
-- guess about the state of each thread.
CREATE VIEW internal.state_p_tid_2 AS
  -- TODO(dancol): teach TSTS to do this coalesce internally
  SELECT SPAN COALESCE(
    IF(new_task_flag IS NULL, NULL, 0),
    IF(wakeup_flag IS NULL, NULL, 0),
    fls(sampled_state & 0x3f),
    fls(switched_from_state & 0x3f),
  ) AS state FROM internal.state_p_tid_1;

-- Make an auxiliary table describing when each thread is
-- actually running.
CREATE VIEW internal.running_p_tid_1 AS
  SELECT SPAN * FROM dctv.time_series_to_spans(
    `partition`=>'tid',
    sources=>[
      {
        source=>internal.switched_from_states_p_tid,
        role=>'stop',
      },
      {
        source=>internal.switched_to_p_tid,
      },
      {
        source=>last_ts,
        role=>'stop_broadcast',
      },
    ],
    columns=>[],
  );

-- Join the thread state table with running-thread information to mark
-- actually-running intervals.
CREATE VIEW internal.state_p_tid_3 AS
  SELECT SPAN
           IF(rt._ts IS NULL, spt.state, CAST(5 AS int8)) AS state
  FROM internal.state_p_tid_2 AS spt
  SPAN LEFT JOIN (internal.running_p_tid_1 AS rt);

-- Convert numeric states to canonical strings.
CREATE VIEW internal.state_p_tid_4 AS
  SELECT SPAN task_state_name AS state
  FROM internal.state_p_tid_3 AS spt
  SPAN LEFT JOIN internal.raw_linux_thread_states AS rlts
           ON spt.state == rlts.task_state_index;


-- Public API


CREATE VIEW FUNCTION quantize AS
  SELECT SPAN FROM dctv.generate_sequential_spans(
    `start`=>first_ts,
    `end`=>last_ts,
    span_duration=>?,
  )
  WITH DOCUMENTATION '
  Break the timeline up into same-sized chunks

  Span function to break the trace timeline up into chunks of the
  given duration (a timestamp value), e.g., mytrace.quantize(5ms).
  Best used with SPAN GROUP BY SPANS to aggregate activity in
  each quantum.
  ';

CREATE VIEW found_event_types AS
  SELECT DISTINCT event_type AS `type`
    FROM raw_event_index
  WITH DOCUMENTATION '
  All events actually found in trace

  event_type: name of the event
  ';

CREATE VIEW good AS
  WITH _good_begin AS (SELECT _ts FROM internal.first_snapshot_end),
  WITH _good_end AS (SELECT _ts FROM last_ts),
  WITH _good_duration AS (_good_end - _good_begin),
  WITH _good_raw AS (
    SELECT
      (SELECT * FROM _good_begin) AS _ts,
      (SELECT * FROM _good_duration) AS _duration),
  SELECT SPAN FROM dctv.internal.cast_as_span_table(
    _good_raw,
    sort=>FALSE,
    verify=>FALSE,
  )
  WITH DOCUMENTATION '
  Span of confident analysis in the trace

  Singleton span table that covers the part of the trace we can
  confidently analyze.
  ';

-- Span table of thread state partitioned by unique thread ID
CREATE VIEW states_p_tid AS
  SELECT SPAN state,
    FROM good SPAN BROADCAST
    INTO SPAN PARTITIONS dctv.backfill(internal.state_p_tid_4)
  WITH DOCUMENTATION '
  Thread state partitioned by unique thread ID

  state: string describing the state of each process in each span

    "+": Thread is runnable, but not running
    "R": Thread is running
    "S": Thread is sleeping
    "D": Thread is in uninterruptible (usually disk) sleep

  This table is pre-intersected with the "good" table.
  ';

CREATE VIEW ion_buffers_p_addr AS
  SELECT SPAN * FROM dctv.time_series_to_spans(
    `partition`=>'addr',
    sources=>[
      {
        source=>(SELECT EVENT PARTITIONED BY addr *
                   FROM cooked_events.ion_buffer_create),
      },
      {
        source=>(SELECT EVENT PARTITIONED BY addr *
                   FROM cooked_events.ion_buffer_destroy),
        role=>'stop',
      },
      {
        source=>last_ts,
        role=>'stop_broadcast',
      },
    ],
    columns=>[
      {
        column=>'len',
        source=>0,
      },
    ],
  )
  WITH DOCUMENTATION '
  ION buffer liveness counts over time

  addr: the kernel address of the ION buffer
  len: the size of the ION buffer
  ';
