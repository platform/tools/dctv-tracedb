// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "simple_variant.h"
#include <assert.h>
#include <stdlib.h>
#include <type_traits>

namespace dctv {

using TestVariant = SimpleVariant<float, int>;

void
test_simple_variant()
{
  TestVariant foo = 0.1f;
  assert(*foo.get_if<float>() == 0.1f);
  assert(!foo.get_if<int>());
  bool got_int = false;
  bool got_float = false;
  foo.visit([&](auto& value) {
    using ValueType = std::decay_t<decltype(*&value)>;
    if (std::is_same_v<ValueType, float>)
      got_float = true;
    else if (std::is_same_v<ValueType, int>)
      got_int = true;
    else
      abort();
  });
  assert(got_float);
  assert(!got_int);
}

void
test_simple_variant_const()
{
  TestVariant foo = 0.1f;
  static_cast<TestVariant&>(foo).visit([&](auto&& value) {
    static_assert(std::is_lvalue_reference_v<decltype(value)>);
    static_assert(!std::is_const_v<
                  std::remove_reference_t<decltype(value)>>);
  });
  static_cast<const TestVariant&>(foo).visit([&](auto&& value) {
    static_assert(std::is_lvalue_reference_v<decltype(value)>);
    static_assert(std::is_const_v<
                  std::remove_reference_t<decltype(value)>>);
  });
  static_cast<TestVariant&&>(foo).visit([&](auto&& value) {
    static_assert(std::is_rvalue_reference_v<decltype(value)>);
    static_assert(!std::is_const_v<
                  std::remove_reference_t<decltype(value)>>);
  });
  static_cast<const TestVariant&&>(foo).visit([&](auto&& value) {
    static_assert(std::is_rvalue_reference_v<decltype(value)>);
    static_assert(std::is_const_v<
                  std::remove_reference_t<decltype(value)>>);
  });
}

}  // namespace dctv
