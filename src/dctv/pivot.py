# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# pylint: disable=anomalous-backslash-in-string
"""Fancy pivot table used for displaying query results.

A pivot table lets users interactively explore a data set using ad-hoc
grouping and filtering. Users control grouping and filtering by
dragging columns into one of two regions in the pivot table.

USER INTERACTION
----------------

An example will clarify how this widget works. Imagine that we're
examining timeslice data for an Android system.  A user might want to
group timeslices by CPU and by process name, displaying the average
timeslice length for each (CPU, COMM) pair by configuring an
"avg(duration)" column to appear on the right of the thick line. The
user can sort the table, search for strings in cells, apply filters,
and so on. These features are common to any table viewing tool.

The real power of the pivot table system comes from tree expansion
though. An example will help explain:

             GROUPING COLUMNS       AGGREGATION COLUMNS
             /          \                  |
            /            \                 V

  #   |   COMM       |   CPU  |=| AVG(duration_ms)
  ----+--------------+-------+|=|------------------
  1   | [+] kworker  |        |=| 16
  2   |     system_s | [+] 1  |=| 10
  3   |     snapchat | [+] 2  |=| 5
  4   | [+] lyft     |        |=| 15

All the columns to the left of the thick vertical line are "group by"
or "where" constraints, depending on tree expansion state (see
below). The values in the columns to the right of the line come from
the result of running a user-configurable aggregation function (e.g.,
max, average, count) over all the rows grouped by the values in the
columns to the left of the thick line.

A blank value in a grouping column indicates that more than one value
would fit there. As we'll see below, kworker here has timeslices for
both CPU #1 and CPU #2, so the "CPU" cell in row #1 in the table above
is empty. system_server only ran on CPU #1, so we can display a value
in the corresponding grouping column.

Suppose a user wants to explore the individual timeslices for kworker
--- he could drag the thick line to the left of the CPU and COMM
columns and display each timeslice as a single row in the table. Doing
so would produce a _lot_ of rows, however, and having to sort through
that many records would take mental energy best expended elsewhere.

A better option is to click the little "[+]" button next to the
grouping column cell value. This button, in effect, "ungroups" the
query, but only for rows matching the particular cell clicked. This
operation might produce the following new table:

  #   |   COMM       |   CPU  |=| AVG(duration_ms)
  ----+--------------+-------+|=|------------------
  1   | [-] kworker  |        |=| 16
  2   |              | [+] 1  |=| 14
  3   |              | [+] 2  |=| 14
  4   |     system_s | [+] 1  |=| 10
  5   |     snapchat | [+] 2  |=| 5
  6   | [+] lyft     |        |=| 15

Clicking the next [+], the one on row #2 of the new table, might
produce this view:

  #   |   COMM       |   CPU  |=| AVG(duration_ms)
  ----+--------------+-------+|=|------------------
  1   | [-] kworker  |        |=| 16
  2   |              | [-] 1  |=| 14
  3   |              |        |=| 16
  4   |              |        |=| 14
  5   |              |        |=| 12
  6   |              |        |=| 5
  7   |              | [+] 2  |=| 14
  8   |     system_s | [+] 1  |=| 10
  9   |     snapchat | [+] 2  |=| 5
 10   | [+] lyft     |        |=| 15

See how, with the last group-by column expanded, we get the raw
timeslice rows, but only for the specific values to the left of the
thick line? Here, we see three timeslices, of 14ms, 16ms, and 12ms,
which average to the 14ms displayed on row #2.

Because users can drag arbitrary sets of columns into the aggregate
region, the pivot table lets users construct a huge variety of useful
queries without having to write any code.

IMPLEMENTATION
--------------

Group expansions are implemented as independent queries filtered with
where-clauses. For example, in the second example table, we've
expanded the kworker line. Rows #2 and #3 are the result of running
the original query without the "group by COMM" bit and with an
additional "where COMM=kworker" constraint.

The actual display of the table is a FhView, which (unlike GTK
TreeView) can accommodate millions of rows of virtual table. FhView
displays only a flat row list, though, so we maintain a tree structure
for mapping "display rows" (as given by the "#" column above) to
"query rows". For example, in the last table above, display row #10
would map to query row #4 in the root query. (In the first table in
the example section, we hadn't expanded any groupings, so the display
and query numbers were identical).

To keep memory requirements reasonable, we keep only a subset of the
query results in memory at any one time and asynchronously ask the
backing query engine for more results as the user scrolls around the
FhView and expands grouping rows. Each subquery gets its own cache.

Actual querying is triggered by display: whenever we're asked to
display a cell value, if we don't have that cell's value cached, we
display a placeholder. After a short delay (to allow for batching
nearby cell-data requests), we issue a query for the missing data;
when the query completes, we redisplay the table with the missing data
displayed.
"""
import logging
from operator import is_
from enum import Enum
from functools import partial
from itertools import chain
from collections import OrderedDict

from cytoolz import count, first

from modernmp.util import (
  the,
  the_seq,
)

from .query import QuerySchema
from .sql_util import (
  SqlBundle,
)

from .gtk_util import (
  Gtk,
  GObject,
  Gdk,
  safe_destroy,
  window_ancestor_p,
  make_menu_item,
)

from .fhview import FhView, FhColumn

from .util import (
  ChainableFuture,
  EqImmutable,
  ExplicitInheritance,
  Immutable,
  abstract,
  all_unique,
  cached_property,
  common_prefix,
  final,
  future_exception,
  iattr,
  override,
  tattr,
)

from .pivot_engine import (
  AMBIGUOUS,
  ErrorResult,
  LOADING,
  PivotQueryEngine,
  Placeholder,
  PlaceholderPivotNode,
  QueryPivotNode,
  RESTRICTED,
  SqlFromPart,
  StaleNodeError,
)

log = logging.getLogger(__name__)

class PivotTable(Gtk.Bin):
  """Visualizes a query"""
  __gtype_name = "PivotTable"

  def __init__(self,
               *,
               query_engine,
               base_query,
               known_column_info,
               **kwargs):
    super().__init__(**kwargs)
    self.connect("destroy", self.__on_destroy)
    self.__known_columns = OrderedDict(
      (the(str, column_name), the(QuerySchema, schema))
      for column_name, schema in known_column_info)
    self.__base_query = the(SqlBundle, base_query)
    self.__localcc = PivotQueryEngine(
      query_engine=query_engine,
    )
    self.__localcc.connect("lookup-complete", self.__on_lookup_complete)
    self.__localcc.connect("row-map-changed", self.__on_row_map_changed)

    self.__presentations = presentations = \
      list(self._make_default_presentations())
    self.__aggregations = aggregations = \
      list(self._make_default_aggregations())

    # TODO: be more intelligent about column generation

    self.__qcc = None
    self.__table = FhView()
    self.__table.connect("columns-changed", self.__on_columns_changed)
    self.__table.connect("row-clicked", self.__on_row_clicked)
    self.__barrier_column = GroupingBarrierColumn()

    # TODO(dancol): restore row number column width precalculation
    columns = [RowNumberColumn()]
    columns.append(self.__barrier_column)
    for column_name, query_schema in self.__known_columns.items():
      aggregation = aggregations[0]
      presentation = None
      for candidate_presentation in presentations:
        if candidate_presentation.compatible_p(query_schema):
          presentation = candidate_presentation
          break
      assert presentation
      columns.append(
        PivotFhColumn(
          label=column_name,
          query_column=column_name,
          query_schema=query_schema,
          aggregation=aggregation,
          presentation=presentation,
        ))
    self.__sw = sw = Gtk.ScrolledWindow()
    sw.props.vscrollbar_policy = Gtk.PolicyType.ALWAYS
    sw.add(self.__table)
    self.add(sw)
    self.__on_row_map_changed(None)
    self.show_all()
    self.__table.columns = columns
    self.__popup_menu = None

  @staticmethod
  def _make_default_presentations():
    """Make the default list of presentations"""
    return (
      #StringPresentation(),
      #NumericPresentation(),
      Presentation(name="foo"),
    )

  @staticmethod
  def _make_default_aggregations():
    """Make the default list of aggregations"""
    return (
      UniqueColumnAggregation(name="Unique"),
      SqlFunctionColumnAggregation(name="Max", function="max"),
      SqlFunctionColumnAggregation(name="Min", function="min"),
      SqlFunctionColumnAggregation(name="Any", function="first"),
      SqlFunctionColumnAggregation(name="Sum", function="sum"),
    )

  def __on_lookup_complete(self, _data):
    self.__table.force_full_redraw()

  def __on_row_map_changed(self, _data):
    self.__table.total_rows = self.__localcc.total_display_rows
    self.__table.force_full_redraw()

  def __on_destroy(self, _data):
    self.__localcc.close()
    self.__sw.remove(self.__sw.get_child())
    safe_destroy(self.__sw)
    self.__sw = None
    safe_destroy(self.__table)
    self.__table = None

  def __set_qcc(self, qcc, *, force_refresh=False):
    localcc = self.__localcc
    old_qcc = self.__qcc
    self.__qcc = qcc

    qcc.apply_to_columns(localcc=localcc)

    model = localcc.model
    root = model.root

    old_sorts = old_qcc and old_qcc.sorts
    if old_sorts != qcc.sorts or force_refresh:
      # TODO(dancol): try to add some of these back based by
      # asynchronously querying the new query locations of the old
      # PivotNodes in their re-sorted parents.
      model.remove_nodes_if(lambda _n: True)
      # pylint: disable=len-as-condition
      assert not len(model.get_children(root))

    old_group_by = old_qcc.group_by if old_qcc else []
    if old_group_by != qcc.group_by:
      first_change = count(common_prefix(is_, old_group_by, qcc.group_by))
      model.remove_nodes_if(
        lambda n: n.number_restrict >= first_change)

    number_root_children = len(model.get_children(root))

    # The root of the tree is a dummy node; the actual top-level query
    # node is its single child.  If we haven't set up the tree yet, or
    # if we've changed the base query, the root actually has zero
    # children --- not even the dummy is present in this case.
    assert number_root_children in (0, 1)

    if not number_root_children:
      group_by = None
      if qcc.group_by:
        # Yes, even if we've conceptually grouped multiple columns,
        # from a query POV, only the first one matters for the
        # top-level PivotNode.  (Exercise: with no nodes expanded,
        # does adding another grouped column immediately to the left
        # of the grouping bar change the length of the table?)
        group_by = SqlBundle.of(qcc.group_by[0].query_column)
      self.__start_async_insert(parent_node=root,
                                index_in_parent=0,
                                number_restrict=0,
                                qm=qcc.make_pivot_node_qm(group_by, None))

  def __start_async_insert(self,
                           *,
                           parent_node,
                           index_in_parent,
                           number_restrict,
                           qm):
    model = self.__localcc.model
    assert model.has_node_p(parent_node)
    assert 0 <= the(int, index_in_parent) <= parent_node.number_rows
    assert 0 <= the(int, number_restrict)
    assert isinstance(qm, SqlFromPart)
    model.insert_node(
      PlaceholderPivotNode(value=LOADING,
                           number_restrict=number_restrict),
      parent_node, index_in_parent)
    error_future = ChainableFuture()
    # Insert an error node with a deferred insert instead of just
    # doing it immediately in the completion handler so that the error
    # insert (thanks to the tree's sequence number stuff) doesn't
    # clobber any subsequent tree modifications.
    error_node_future = model.defer_insert(
      parent_node=ChainableFuture.of(parent_node),
      index=ChainableFuture.of(index_in_parent),
      pivot_node_type=PlaceholderPivotNode,
      number_restrict=ChainableFuture.of(number_restrict),
      value=error_future)
    def _on_error_node_complete(future):
      assert future is error_node_future
      exception = future.done() and future_exception(future)
      if exception and not isinstance(exception, StaleNodeError):
        log.warning("adding error placeholder failed", exc_info=exception)
    error_node_future.add_done_callback(_on_error_node_complete)

    row_count_future = self.__localcc.query_int_async(qm.count_query)
    real_node_future = model.defer_insert(
      parent_node=ChainableFuture.of(parent_node),
      index=ChainableFuture.of(index_in_parent),
      pivot_node_type=QueryPivotNode,
      number_restrict=ChainableFuture.of(number_restrict),
      number_rows=row_count_future,
      qm=ChainableFuture.of(qm))
    def _on_real_node_future_done(future):
      assert future is real_node_future
      assert future.done()
      if not future.cancelled() and future_exception(future):
        error_future.set_result(ErrorResult(future_exception(future)))
    real_node_future.add_done_callback(_on_real_node_future_done)
    return real_node_future

  def __on_columns_changed(self, _table):
    if self.__table.in_destruction():
      return  # TODO(dancol): disconnect early so we can't get here
    columns = self.__table.columns
    new_sorts = [sort for sort in (self.__qcc.sorts if self.__qcc else ())
                 if sort.column in columns]
    self.__set_qcc(
      QueryColumnConfig(columns=columns,
                        base_query=self.__base_query,
                        sorts=new_sorts))

  def __on_row_clicked(self, _table, column, display_row_number, event):
    # TODO(dancol): use a future to get the cell value
    if event.button != Gdk.BUTTON_PRIMARY:
      return
    if column not in self.__qcc.group_by:
      return
    localcc = self.__localcc
    pivot_node, query_row_number = localcc.map_display_row(
      display_row_number)
    value, child_state = column.core.get_group_cell_info(
      pivot_node,
      display_row_number,
      query_row_number)
    if isinstance(value, Placeholder):
      return
    if child_state is None:
      return  # No child possible here
    if child_state:
      localcc.model.remove_node(child_state)
    else:
      assert child_state is False  # pylint: disable=compare-to-zero
      qcc = self.__qcc
      column_index = qcc.group_by.index(column)

      # The grouping of the new node is the grouping of the _next_
      # column over, or no grouping at all otherwise.
      group_by = (SqlBundle.of(qcc.group_by[column_index + 1].query_column)
                  if column_index < len(qcc.group_by) - 1 else None)

      # The expansion of this level is completely determined by the
      # value of the first non-restricted column in the row, _not_ the
      # value of this column, which we ignore. If it weren't, the
      # nodes to the left of us would be ambiguous --- but since we
      # got here, we know they're not.
      first_column = qcc.group_by[pivot_node.number_restrict]
      ungroup_value = first_column.core.get_group_value(
        pivot_node,
        display_row_number,
        query_row_number)
      if isinstance(ungroup_value, Placeholder):
        log.warning("placeholder in ungroup slot")
        return
      where = SqlBundle.format(
        "{} <=> :pivot_ungroup".format(
          first_column.query_column),
        pivot_ungroup=ungroup_value)
      self.__start_async_insert(
        parent_node=pivot_node,
        index_in_parent=query_row_number + 1,
        number_restrict=column_index + 1,
        qm=qcc.make_pivot_node_qm(group_by, where))


  def __find_column_by_widget(self, widget):
    for column in self.__table.columns:
      if window_ancestor_p(column.header_content_widget, widget):
        return column
    return None

  def on_header_clicked(self, widget):
    """Called when a header column widget is clicked"""
    column = self.__find_column_by_widget(widget)
    if not isinstance(column, PivotFhColumn):
      log.warning("clicked column was not PivotFhColumn: was %r",
                  type(column))
      return
    header = column.header_content_widget
    ascending = not header.sort_indicator is SortIndicator.ASCENDING
    self.prepend_sort(column, ascending=ascending)

  def on_header_agg_button_clicked(self, widget, agg_button):
    """Called when a header aggregation button is clicked"""
    # pylint: disable=no-member
    column = self.__find_column_by_widget(widget)
    if not isinstance(column, PivotFhColumn):
      log.warning("clicked column was not PivotFhColumn: was %r",
                  type(column))
      return
    aggregation_menu = Gtk.Menu()
    schema = self.__known_columns[column.query_column]
    for aggregation in self.__aggregations:
      aggregation_menu.append(
        make_menu_item(
          cls=Gtk.RadioMenuItem,
          label=aggregation.name,
          draw_as_radio=True,
          active=(aggregation is column.aggregation),
          sensitive=(aggregation.compatible_p(schema)),
          callback=partial(self.__on_menu_set_column_aggregation,
                           column,
                           aggregation)))
    aggregation_menu.show_all()
    aggregation_menu.popup_at_widget(
      agg_button,
      Gdk.Gravity.SOUTH_WEST,
      Gdk.Gravity.NORTH_WEST,
      Gtk.get_current_event())
    self.__popup_menu = aggregation_menu

  def __on_menu_set_column_aggregation(self, column, aggregation):  # pylint: disable=no-self-use
    column.aggregation = aggregation

  def on_set_column_aggregation(self, column):
    """Called when a column aggregation has been configured"""
    force_refresh = column in map(first, self.__qcc.sorts)
    # TODO(dancol): yuck. qcc should have column descriptors
    self.__set_qcc(self.__qcc, force_refresh=force_refresh)
    # TODO(dancol): redraw just this column
    self.__table.force_full_redraw()

  def __on_clone_column(self, column):
    columns = list(self.__table.columns)
    new_column = column.clone_column()
    columns.insert(columns.index(column) + 1, new_column)
    self.__table.columns = columns
    assert new_column.core  # Should have been set in qcc refresh (ugh)

  def __on_remove_column(self, column):
    self.__table.columns = filter(
      lambda c: c is not column,
      self.__table.columns)

  def __on_set_column_type(self, column, new_ctype_code):
    raise NotImplementedError("XXX")
    # column.ctype = ColumnType.lookup(new_ctype_code)
    # if column.aggregation not in column.ctype.agg_allowed:
    #   column.aggregation = column.ctype.agg_allowed[0]
    # self.__set_qcc(self.__qcc)

  def __make_column_ctype_menu(self, column):
    raise NotImplementedError("XXX")
    # ctype_menu = Gtk.Menu()
    # for column_type_code, column_type in sorted(ColumnType.all().items()):
    #   ctype_menu.append(
    #     make_menu_item(
    #       cls=Gtk.CheckMenuItem,
    #       label=column_type.label,
    #       draw_as_radio=True,
    #       active=(column_type_code == column.ctype.code),
    #       callback=partial(self.__on_set_column_type,
    #                        column,
    #                        column_type_code)))
    # return ctype_menu

  def __make_column_aggregation_menu(self, column):
    raise NotImplementedError("XXX")
    # aggregation_menu = Gtk.Menu()
    # ctype = column.ctype
    # for aggregation in sorted(ALL_AGGREGATIONS):
    #   aggregation_label = ("[None]" if aggregation == GRP
    #                      else aggregation).title()
    #   aggregation_menu.append(
    #     make_menu_item(
    #       cls=Gtk.RadioMenuItem,
    #       label=aggregation_label,
    #       draw_as_radio=True,
    #       active=(aggregation == column.aggregation),
    #       sensitive=(aggregation in ctype.agg_allowed),
    #       callback=partial(self.__on_set_column_aggregation,
    #                        column,
    #                        aggregation)))
    # return aggregation_menu

  def __make_header_popup_menu(self, column):
    raise NotImplementedError("XXX")
    # popup_menu = Gtk.Menu()
    # popup_menu.append(
    #   make_menu_item(
    #     label="Aggregation",
    #     sensitive=isinstance(column.core, AggregatingPivotColumnCore),
    #     submenu=self.__make_column_aggregation_menu(column)))
    # popup_menu.append(
    #   make_menu_item(
    #     label="Column type",
    #     submenu=self.__make_column_ctype_menu(column)))

    # popup_menu.append(Gtk.SeparatorMenuItem())

    # popup_menu.append(
    #   make_menu_item(
    #     label="Clone column",
    #     callback=partial(self.__on_clone_column, column)))
    # popup_menu.append(
    #   make_menu_item(
    #     label="Remove column",
    #     callback=partial(self.__on_remove_column, column)))

    # popup_menu.show_all()
    # self.__popup_menu = popup_menu  # XXX
    # return popup_menu

  def prepend_sort(self, sorting_column, *, ascending):
    """Sort by the given column.

    SORTING_COLUMN is the column by which to sort. ASCENDING indicates
    whether we should be sorting in the ascending direction. If
    ASCENDING is false, we sort descending.

    We maintain a history of sorted columns so that users can sort by
    multiple columns by clicking column headers in the desired
    sorting sequence.
    """
    if not self.__qcc:
      return
    new_sort = QueryColumnConfig.Sort(sorting_column, ascending)
    self.__set_qcc(self.__qcc.prepend_sort(new_sort))

@final
class QueryColumnConfig(Immutable):
  """Column configuring driving query creation"""

  @final
  class Sort(EqImmutable):
    """Sorting information for a QCC"""
    column = iattr(FhColumn)
    ascending = iattr(bool)

  base_query = iattr(SqlBundle, kwonly=True)
  group_by = tattr(FhColumn, kwonly=True)
  aggregations = tattr(FhColumn, kwonly=True)
  sorts = tattr(Sort, kwonly=True)

  def __new__(cls, *, base_query, columns, sorts):
    group_by, aggregations = cls.__split_columns(columns)
    return cls._do_new(cls,
                       base_query=base_query,
                       group_by=group_by,
                       aggregations=aggregations,
                       sorts=sorts)

  @cached_property
  def columns(self):
    """All columns described by this QCC"""
    return self.group_by + self.aggregations

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert all_unique(self.group_by)
    assert all_unique(self.aggregations)
    assert len(self.columns) == len(self.group_by) + len(self.aggregations)
    for sort in self.sorts:
      assert sort.column in self.columns

  def prepend_sort(self, sort):
    """Return a new QCC with a sort prepended"""
    return self.evolve(
      sorts=(sort,) + tuple(old_sort for old_sort in self.sorts
                            if old_sort.column is not sort.column))

  @staticmethod
  def __split_columns(columns):
    """Split a column list into the grouping and aggregating groups

    Remove all non-data columns (e..g, line number).
    """
    saw_split = False
    group_by = []
    aggregations = []
    for column in columns:
      if isinstance(column, RowNumberColumn):
        continue
      if isinstance(column, GroupingBarrierColumn):
        assert not saw_split
        saw_split = True
        continue
      assert isinstance(column, PivotFhColumn)
      (aggregations if saw_split else group_by).append(column)
    assert saw_split
    return tuple(group_by), tuple(aggregations)

  def apply_to_columns(self, *, localcc):
    """Update columns to match configuration"""
    # pylint: disable=redefined-variable-type,no-member
    for column in self.group_by:
      column.core = GroupingPivotColumnCore(
        query_column=column.query_column,
        localcc=localcc,
        presentation=column.presentation,
        exemplar=column.exemplar,
      )
      column.header_content_widget.agg_label = None
    grouping_cores = tuple(c.core for c in self.group_by)
    for column in self.group_by:
      column.core.set_grouping_cores(grouping_cores)

    for column in self.aggregations:
      aggregation = column.aggregation
      column.core = AggregatingPivotColumnCore(
        query_column=column.query_column,
        localcc=localcc,
        presentation=column.presentation,
        aggregation=aggregation,
        exemplar=column.exemplar,
      )
      column.header_content_widget.agg_label = aggregation.name

    first_sort = self.sorts and self.sorts[0]
    first_sort_column = first_sort and first_sort.column
    first_sort_ascending = first_sort and first_sort.ascending
    for column in chain(self.group_by, self.aggregations):
      assert isinstance(column, PivotFhColumn)
      header = column.header_content_widget
      if self.sorts and column is first_sort_column:
        header.sort_indicator = (SortIndicator.ASCENDING
                                 if first_sort_ascending
                                 else SortIndicator.DESCENDING)
      else:
        header.sort_indicator = SortIndicator.BLANK

  def make_pivot_node_qm(self, group_by, where):
    """Make a SqlFromPart suitable for a QueryPivotNode"""
    return SqlFromPart(self.base_query,
                       group_by=group_by,
                       where=where,
                       order_by=self.make_order_by_expr())

  def make_order_by_expr(self):
    """Make a SqlBundle describing our desired order"""
    sorts = self.sorts
    if not sorts:
      return None
    def _make_one_expr(sort):
      expr = sort.column.core.get_sort_ca()
      if not sort.ascending:
        expr = SqlBundle.format(":expr DESC", expr=expr)
      return expr
    # TODO(dancol): give SqlBundle some kind of join operator
    # so we don't need exponential time to do the join.
    sort_expr = _make_one_expr(sorts[0])
    for additional_sort in sorts[1:]:
      sort_expr = SqlBundle.format(":left, :right",
                                   left=sort_expr,
                                   right=_make_one_expr(additional_sort))
    return sort_expr

class RowNumberColumn(FhColumn):
  """Column that displays the current row number"""
  def __init__(self, exemplar=None, **kwargs):
    super().__init__(label="#", **kwargs)
    self.__renderer = Gtk.CellRendererText()
    self.__renderer.props.background = "gray"
    self.__renderer.props.foreground = "white"
    self.can_move = False
    self.add_renderer(self.__renderer)
    self.__exemplar = int(exemplar or 1000)

  def prepare(self, display_row_number):
    if display_row_number < 0:
      display_row_number = self.__exemplar
    self.__renderer.props.text = str(display_row_number)

  @override
  def _create_header_widget(self):
    super()._create_header_widget()
    self.header_widget.get_style_context().add_class("rownumber")

class GroupingBarrierColumn(FhColumn):
  """Column that separates grouping columns from aggregation columns"""
  def __init__(self, **kwargs):
    super().__init__(label=" ", **kwargs)
    self.__renderer = Gtk.CellRendererText()
    self.__renderer.props.background = "#ffd700"
    self.add_renderer(self.__renderer)
    self.explicit_width = 10
    self.can_resize = False

class PivotColumnCore(ExplicitInheritance):
  """Generates values for PivotFhColumn instances.

  Unlike PivotFhColumn instances, which are long-lived and mutable,
  PivotColumnCore objects are notionally immutable (all state coming
  from the environment) and live only as long as the column
  configuration itself lives.
  """

  @override
  def __init__(self, *, query_column, localcc):
    self._query_column = the(str, query_column)
    self._localcc = the(PivotQueryEngine, localcc)

  def setup_renderers(self, column):
    """Set up a column. Called on core set"""

  def prepare_dummy_row(self):
    """Prepare a dummy row"""
    self.prepare(-1)

  @abstract
  def prepare(self, display_row_number):
    """Prepare to render the column"""
    raise NotImplementedError("abstract")

  @abstract
  def get_sort_ca(self):
    """Retrieve the SqlBundle for this column's sorting expression"""
    raise NotImplementedError("abstract")

class AggregatingPivotColumnCore(PivotColumnCore):
  """Core for columns that aggregation some function over a table's groups"""

  @override
  def __init__(self, aggregation, presentation, exemplar, **kwargs):
    assert isinstance(aggregation, ColumnAggregation)
    super().__init__(**kwargs)
    query_column = self._query_column
    self.__renderer = Gtk.CellRendererText()
    self.__format_value = presentation.make_format_function()
    self.__sort_ca, self.__get_value = \
      aggregation.cook(SqlBundle.of(query_column), presentation)
    self.__exemplar = exemplar

  @override
  def setup_renderers(self, column):
    column.add_renderer(self.__renderer)

  @override
  def prepare_dummy_row(self):
    self.__renderer.props.text = self.__format_value(self.__exemplar or "X")

  @override
  def prepare(self, display_row_number):
    self.__renderer.props.text = \
      str(self.__get_value(self._localcc, display_row_number))

  @override
  def get_sort_ca(self):
    return self.__sort_ca

class GroupingPivotColumnCore(PivotColumnCore):
  """Column core for columns that are grouped"""

  @override
  def __init__(self, presentation, exemplar, **kwargs):
    super().__init__(**kwargs)
    raw_column_key = presentation.transform_sql_expression(
      SqlBundle.of(self._query_column))
    self.__value_key = SqlBundle.format(
      "dctv.first(:raw_ca)",
      raw_ca=raw_column_key)
    self.__unique_mask_key = SqlBundle.format(
      "dctv.unique_mask(:raw_ca)",
      raw_ca=raw_column_key)
    self.__grouping_cores = None
    self.__expander_renderer = Gtk.CellRendererText(
      xalign=0.0,
      font="Courier New Bold",
    )
    self.__format_value = presentation.make_format_function()
    self.__value_renderer = Gtk.CellRendererText(xalign=0.0)
    self.__exemplar = exemplar

  @override
  def get_sort_ca(self):
    return self.__value_key

  def set_grouping_cores(self, grouping_cores):
    """Inform core of all members of the current group set"""
    assert self.__grouping_cores is None
    self.__grouping_cores = the_seq(tuple,
                                    GroupingPivotColumnCore,
                                    grouping_cores)

  @override
  def setup_renderers(self, column):
    column.add_renderer(self.__value_renderer,
                        expand=True,
                        align=True,
                        fixed=False)
    column.add_renderer(self.__expander_renderer,
                        expand=False,
                        align=True,
                        fixed=True)

  # TODO(dancol): get_group_value should return a future!

  def get_group_value(self, pivot_node, display_row_number, query_row_number):
    """Retrieve the grouping value for the given cell

    The "grouping value" is the value of a cell in a group-by column
    if that value is the value all rows in that group share. If
    a group has different values for this column, we instead
    return the sentinel value AMBIGUOUS.

    We also have to deal with the possibility of not having data for
    the necessary columns. In this case, we return NO_DATA.
    """
    localcc = self._localcc
    is_unique = pivot_node.lookup_column(
      localcc,
      display_row_number,
      query_row_number,
      self.__unique_mask_key)
    if isinstance(is_unique, Placeholder):
      return is_unique
    if not is_unique:
      return AMBIGUOUS
    return pivot_node.lookup_column(
      localcc,
      display_row_number,
      query_row_number,
      self.__value_key)

  # TODO(dancol): get_group_cell_info should return a future!

  def get_group_cell_info(self,
                          pivot_node,
                          display_row_number,
                          query_row_number):
    """Compute grouping information for a cell.

    PIVOT_NODE is the pivot node that owns the row of interest;
    QUERY_ROW_NUMBER is the row number within the query node. (The
    caller is responsible for doing any display-row to
    query-row mapping.)

    The column must be in grouping mode.

    Return (CELL_VALUE, CHILD_STATE).

      CELL_VALUE may be a placeholder, in which case CHILD_STATE
      is unspecified. Interesting placeholders:

        NO_DATA: we didn't have a piece of information we needed to
          compute the grouping information for the given cell;
          request enqueued.

        AMBIGUOUS: this cell resolves to more than one value.

      If CELL_VALUE is not a placeholder, this cell has a single
      unambiguous value, CELL_VALUE. CHILD_STATE indicates whether we
      have a child. CHILD_STATE is None if no child is possible at
      this location. Otherwise, if a child is possible for this
      location, CHILD_STATE is a PivotNode if we actually do have a
      child and False is not.
    """
    # If we're blanked because we're the child of some other node,
    # figure that out now.
    grouping_cores = self.__grouping_cores
    number_restrict = pivot_node.number_restrict
    my_idx = grouping_cores.index(self)
    if my_idx < number_restrict:
      return RESTRICTED, None

    # In order to figure out whether we need to display an expansion
    # indicator, we need to determine whether we're the last
    # unambiguous cell in the row. Query all the grouping columns up
    # to and including the column after the current one; we query all
    # the columns here before checking whether we got NO_DATA for any
    # of them so that we kick off all the data fetches for these
    # columns in parallel.
    group_column_values = [None] * number_restrict  # Make the indexing work
    group_column_values.extend(
      c.get_group_value(pivot_node,
                        display_row_number,
                        query_row_number)
      for c in grouping_cores[number_restrict:my_idx+2])
    localcc = self._localcc
    group_size = pivot_node.lookup_group_size(localcc,
                                              display_row_number,
                                              query_row_number)
    if isinstance(group_size, Placeholder):
      return group_size, None
    for value in group_column_values:
      if isinstance(value, Placeholder) and value is not AMBIGUOUS:
        return value, None
    assert group_size >= 0

    # At this point, we've completely resolved the data we need. If we
    # don't have a singular value, we have neither a display value nor
    # the possibility of having a child, so we're done.
    my_value = group_column_values[my_idx]
    if my_value is AMBIGUOUS:
      return AMBIGUOUS, None

    # Now we're committed to actually producing a value.
    is_left_unambiguous = AMBIGUOUS not in group_column_values[:my_idx+1]
    is_right_ambiguous = (my_idx == len(group_column_values) - 1 or
                          group_column_values[my_idx + 1] is AMBIGUOUS)
    if is_left_unambiguous and is_right_ambiguous and group_size > 1:
      child_state = localcc.model.maybe_get_child_at(
        pivot_node, query_row_number + 1) or False
    else:
      child_state = None
    return my_value, child_state

  def __prepare_1(self, display_row_number):
    localcc = self._localcc
    pivot_node, query_row_number = localcc.map_display_row(
      display_row_number)
    value, child_state = self.get_group_cell_info(
      pivot_node,
      display_row_number,
      query_row_number)
    text = self.__format_value(value)
    return text, child_state

  @override
  def prepare(self, display_row_number):
    if display_row_number < 0:
      if self.__exemplar is None:
        text = "X"
      else:
        text = self.__format_value(self.__exemplar)
      child_state = True  # For proper size calculation
    else:
      text, child_state = self.__prepare_1(display_row_number)
    self.__value_renderer.props.text = text
    if child_state is None:
      self.__expander_renderer.props.text = " "
    elif child_state:
      self.__expander_renderer.props.text = "▼"
    else:
      self.__expander_renderer.props.text = "▶"

class SortIndicator(Enum):
  """Possible sorting states for a PivotFhColumn"""
  BLANK = 0
  ASCENDING = 1
  DESCENDING = 2

class PivotFhColumnHeader(Gtk.Button):
  """Header widget for a PivotFhColumn"""
  __gtype_name__ = "PivotFhColumnHeader"

  __gsignals__ = {
    "agg-button-clicked": (GObject.SIGNAL_RUN_FIRST, None, (
      Gtk.Widget,
    )),
  }

  def __init__(self, label, **kwargs):
    super().__init__(**kwargs)
    # Use class, not widget name, to distinguish us from other
    # buttons. This way, we retain button styling. You'd think
    # applying the "button" class would be sufficient, but it isn't.
    self.get_style_context().add_class("pivot_column_header")

    main_vbox = Gtk.VBox()
    self.add(main_vbox)

    label_sort_hbox = Gtk.HBox()
    label = Gtk.Label(label=label, xalign=0)
    label_sort_hbox.add(label)
    sort_indicator_widget = Gtk.Image()
    label_sort_hbox.add(sort_indicator_widget)
    label_sort_hbox.child_set(sort_indicator_widget, expand=False)
    label_sort_hbox.show_all()
    main_vbox.add(label_sort_hbox)

    agg_button = Gtk.Button.new_with_label("")
    agg_button.get_child().props.xalign = 0
    agg_button.get_style_context().add_class("pivot_agg_button")
    agg_button.show()
    main_vbox.add(agg_button)

    agg_button.connect("clicked", self.__on_agg_button_clicked)

    self.__sort_indicator_widget = sort_indicator_widget
    self.__agg_button = agg_button
    self.__sort_indicator = False  # Shut up pylint
    self.sort_indicator = SortIndicator.BLANK  # For side effect
    self.__agg_label = False  # Shut up pylint
    self.agg_label = None  # For side effect

  @property
  def sort_indicator(self):
    """Sorting indicator mode"""
    return self.__sort_indicator

  @sort_indicator.setter
  def sort_indicator(self, mode):
    assert isinstance(mode, SortIndicator)
    if self.__sort_indicator is not mode:
      if mode is SortIndicator.ASCENDING:
        icon_name = "pan-up-symbolic"
      else:
        icon_name = "pan-down-symbolic"
      sort_indicator_widget = self.__sort_indicator_widget
      sort_indicator_widget.props.icon_name = icon_name
      opacity = 0.0 if mode is SortIndicator.BLANK else 1.0
      sort_indicator_widget.set_opacity(opacity)
      self.__sort_indicator = mode

  @property
  def agg_label(self):
    """Aggregation label or None if hidden"""
    return self.__agg_label

  @agg_label.setter
  def agg_label(self, label):
    if self.__agg_label != label:
      assert isinstance(label, (str, type(None)))
      agg_button = self.__agg_button
      if label is None:
        agg_button.set_opacity(0.0)
      else:
        agg_button.get_child().props.label = label + "…"
        agg_button.set_opacity(1.0)
      self.__agg_label = label

  def do_map(self):
    """Gtk.Widget protocol"""
    # Reparent the child event window to our own event window so that
    # users can click the embedded button without the event going to
    # the outer button first.
    Gtk.Button.do_map(self)
    assert self.__agg_button.get_realized()
    agg_button = self.__agg_button
    agg_wevent = agg_button.get_event_window()
    agg_wevent.raise_()

  def __on_agg_button_clicked(self, button):
    self.emit("agg-button-clicked", button)

class PivotFhColumn(FhColumn):
  """Column that pulls its contents from the row cache"""
  def __init__(self,
               *,
               label,
               query_column,
               query_schema,
               presentation,
               aggregation,
               exemplar=None,
               **kwargs):
    """Create a new PivotFhColumn

    LABEL is the human-readable name for this column.

    QUERY_COLUMN is a string giving the name of the column in the
    query; it doesn't necessarily have to have anything to do with the
    display name as given for FhColumn.

    QUERY_SCHEMA is the schema to which we expect data filling this
    column to adhere.

    If CTYPE is None, guess it from QUERY_SCHEMA; otherwise, use it.
    If AGGREGATION is None, guess it from CTYPE; otherwise, use it.

    EXEMPLAR is a sample value used to auto-compute column width or
    None if the value isn't available.

    Remaining values are as for FhColumn.
    """
    super().__init__(label=label, **kwargs)
    header = PivotFhColumnHeader(label=label)
    header.show_all()
    self.header_content_widget = header

    # Externally inspectable and modifiable properties; persist
    # between core swaps.
    self.__query_column = the(str, query_column)
    self.__query_schema = the(QuerySchema, query_schema)
    self.__presentation = the(Presentation, presentation)
    self.__aggregation = the(ColumnAggregation, aggregation)
    self.__exemplar = exemplar

    # Shut up pylint
    self.__core = True
    self.prepare = None
    self.prepare_dummy_row = None

    # For side effect
    self.core = None

    self.__click_signal_connection = None
    self.__agg_button_signal_connection = None

  @property
  def query_column(self):
    """The name of the column that we display here

    (We may end up querying other columns as well for
    miscellaneous reasons.)
    """
    return self.__query_column

  @property
  def query_schema(self):
    """The QuerySchema describing the raw data for this column"""
    return self.__query_schema

  @property
  def presentation(self):
    """The current presentation of this column"""
    return self.__presentation

  @property
  def exemplar(self):
    """The 'dummy' exemplar value for this column or None if unavailable"""
    return self.__exemplar

  @property
  def aggregation(self):
    """The current aggregation function for this column"""
    return self.__aggregation

  @aggregation.setter
  def aggregation(self, new_aggregation):
    if self.__aggregation is new_aggregation:
      return
    self.__aggregation = the(ColumnAggregation, new_aggregation)
    if self.owner:
      self.__pivot_table.on_set_column_aggregation(self)

  def clone_column(self):
    """Return a copy of this column with most of the same attributes"""
    return PivotFhColumn(
      label=self.__label,
      query_column=self.query_column,
      query_schema=self.query_schema,
      presentation=self.presentation,
      aggregation=self.aggregation,
      exemplar=self.exemplar,
    )

  @staticmethod
  def __on_header_button_press(self, event):
    if (event.button == Gdk.BUTTON_SECONDARY and
        0 <= event.x < event.window.get_width() and
        0 <= event.y < event.window.get_height() and
        self.core):
      coord = event.get_root_coords()
      self.core.make_header_popup_menu(self).popup_for_device(
        event.device,
        None,  # parent_menu_shell
        None,  # parent_menu_item
        lambda menu, data: (coord[0], coord[1], True),
        None,  # data
        event.button,
        event.time)
      return True
    return False

  def __repr__(self):
    return "<PivotFhColumn query_column={!r}>".format(self.query_column)

  @property
  def core(self):
    """Return the current core for this column"""
    return self.__core

  @core.setter
  def core(self, core):
    """Set the core for this column, invalidating as needed"""
    if core == self.__core:
      return
    self.clear_renderers()
    self.__core = core
    if core:
      self.prepare = core.prepare
      self.prepare_dummy_row = core.prepare_dummy_row
      core.setup_renderers(self)
    else:
      self.prepare = None
      self.prepare_dummy_row = None

  @property
  def __pivot_table(self):
    """Owning pivot table or None"""
    widget = self.owner
    if widget:
      widget = widget.get_parent()
      if widget:
        widget = widget.get_parent()
        if widget:
          assert isinstance(widget, PivotTable)
          return widget
    raise ValueError("pivot table not connected")

  def __disconnect_signals(self):
    if self.__click_signal_connection is not None:
      self.header_content_widget.disconnect(self.__click_signal_connection)
      self.__click_signal_connection = None
    if self.__agg_button_signal_connection is not None:
      self.header_content_widget.disconnect(
        self.__agg_button_signal_connection)
      self.__agg_button_signal_connection = None

  def __connect_signals(self):
    self.__disconnect_signals()
    assert self.__click_signal_connection is None
    self.__click_signal_connection = self.header_content_widget.connect(
      "clicked",
      self.__pivot_table.on_header_clicked)
    self.__agg_button_signal_connection = self.header_content_widget.connect(
      "agg-button-clicked",
      self.__pivot_table.on_header_agg_button_clicked)

  @override
  def _on_own(self):
    super()._on_own()
    self.__connect_signals()

  @override
  def _on_disown(self):
    self.__disconnect_signals()
    self.core = None
    super()._on_disown()

class Presentation(Immutable):
  """A presentation describes how we format values for display.

  Presentation objects are orthogonal to aggregations and column
  cores. Different types of column core (e.g., grouping column,
  aggregating column) all use presentation objects, since presentation
  objects care only about formatting valued already determined.
  """

  name = iattr(str, kwonly=True)

  def compatible_p(self, _query_schema):  # pylint: disable=no-self-use
    """Return whether this presentation can work for QUERY_SCHEMA"""
    return True

  def make_format_function(self):  # pylint: disable=no-self-use
    """Make a function that turns the given value into a string

    The returned function takes as input one value from the DB result
    vector from the output of transform_sql_expression and yields
    a string.
    """
    return str # TODO(dancol): fancy formatting

  def transform_sql_expression(self, sql_bundle):  # pylint: disable=no-self-use
    """Transform a SQL expression before DB retrieval.

    This function allows a Presentation to tweak the value returned
    from the DB, e.g., to perform unit conversion.
    """
    assert isinstance(sql_bundle, SqlBundle)
    return sql_bundle

class ColumnAggregation(Immutable):
  """Aggregation function 'plugin' for AggregatingPivotColumnCore.

  The ColumnAggregation object tells AggregatingPivotColumnCore how to
  request aggregated values from the DB.
  """

  name = iattr(str, kwonly=True)

  @abstract
  def cook(self, query_bundle, presentation):
    """Make column cell display function.

    QUERY_BUNDLE is the SqlBundle providing the thing to query from
    the underlying data source.  PRESENTATION is a Presentation object
    that can contribute to the SQL expression we eventually form.

    Return a tuple SORT_CA, GET_VALUE. SORT_CA is a SqlBundle by which
    we sort this column in case sorting is necessary.

    GET_VALUE is a function that retrieves the value of a cell.
    It takes as arguments LOCALCC and DISPLAY_ROW_NUMBER.  GET_VALUE
    is responsible for doing its own mapping from DISPLAY_ROW_NUMBER
    to query row.

    GET_VALUE should hold a reference to PRESENTATION and use it for
    output formatting.
    """
    raise NotImplementedError("abstract")

  def compatible_p(self, _query):  # pylint: disable=no-self-use
    """Return whether this aggregation can be applied to QUERY"""
    return True

@final
class UniqueColumnAggregation(ColumnAggregation):
  """ColumnAggregation that specifies a unique value or nothing"""

  @override
  def cook(self, query_bundle, presentation):
    raw_column_key = presentation.transform_sql_expression(query_bundle)
    first_key = SqlBundle.format(
      "dctv.first(:raw_column_key)",
      raw_column_key=raw_column_key)
    unique_mask_key = SqlBundle.format(
      "dctv.unique_mask(:raw_column_key)",
      raw_column_key=raw_column_key)
    def _get_value(localcc, display_row_number):
      pivot_node, query_row_number = localcc.map_display_row(
        display_row_number)
      is_unique = pivot_node.lookup_column(
        localcc,
        display_row_number,
        query_row_number,
        unique_mask_key)
      value = pivot_node.lookup_column(
        localcc,
        display_row_number,
        query_row_number,
        first_key)
      if isinstance(is_unique, Placeholder):
        return is_unique
      if not is_unique:
        # Return AMBIGUOUS here so that any visual affordance we
        # supply for ambiguous grouping columns gets used here too.
        return AMBIGUOUS
      return value
    return first_key, _get_value

@final
class SqlFunctionColumnAggregation(ColumnAggregation):
  """ColumnAggregation that uses a query-level reduction function"""

  function = iattr(str, kwonly=True)

  @override
  def cook(self, query_bundle, presentation):
    column_key = SqlBundle.format(
      "dctv.{}(:raw_column_key)".format(self.function),
      raw_column_key=presentation.transform_sql_expression(
        query_bundle))
    def _get_value(localcc, display_row_number):
      pivot_node, query_row_number = localcc.map_display_row(
        display_row_number)
      return pivot_node.lookup_column(localcc,
                                      display_row_number,
                                      query_row_number,
                                      column_key)
    return column_key, _get_value

  @override
  def compatible_p(self, _query_schema):
    # XXX
    return True
