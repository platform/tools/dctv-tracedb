# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test synchronous coroutine support"""

# pylint: disable=missing-docstring,redefined-outer-name,compare-to-zero
import sys
import logging
from functools import partial
import pytest
from ._native import SynchronousCoroutine

log = logging.getLogger(__name__)

@pytest.fixture(scope="module", params=["thread", "stack"])
def coro_factory(request):
  on_stack = (request.param == "stack")
  return partial(SynchronousCoroutine, on_stack=on_stack)

def test_basic(coro_factory):
  values = [1, 2, 3]
  def _syncgen(sync_yield):
    for value in values:
      sync_yield(value)
  assert list(coro_factory(_syncgen)) == [1, 2, 3]

def test_null_initial(coro_factory):
  def _syncgen(_sync_yield):
    return
  x = coro_factory(_syncgen)
  with pytest.raises(TypeError):
    x.send(5)

def test_initial_exception(coro_factory):
  def _syncgen(sync_yield):
    raise RuntimeError("initial exception")
  x = coro_factory(_syncgen)
  with pytest.raises(RuntimeError) as ex:
    x.send(None)
  assert "initial exception" in str(ex)

def test_subsequent_exception(coro_factory):
  def _syncgen(sync_yield):
    sync_yield(1)
    raise ValueError("subsequent exception")
  x = coro_factory(_syncgen)
  assert x.send(None) == 1
  with pytest.raises(ValueError) as ex:
    x.send(1)
  assert "subsequent exception" in str(ex)

def test_send_value(coro_factory):
  def _syncgen(sync_yield):
    v = 0
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
  x = coro_factory(_syncgen)
  vals = [None]
  vals.append(x.send(vals[-1]))
  vals.append(x.send(vals[-1]))
  vals.append(x.send(vals[-1]))
  with pytest.raises(StopIteration):
    x.send(vals[-1])
  assert vals == [None, 0, 1, 2]

class TestException(Exception):
  pass

def test_send_exception_type(coro_factory):
  def _syncgen(sync_yield):
    v = 0
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
  x = coro_factory(_syncgen)
  assert x.send(None) == 0
  assert x.send(4) == 5
  with pytest.raises(TestException):
    x.throw(TestException)

def test_send_exception_by_value(coro_factory):
  def _syncgen(sync_yield):
    v = 0
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
  x = coro_factory(_syncgen)
  assert x.send(None) == 0
  assert x.send(4) == 5
  with pytest.raises(TestException) as ex:
    x.throw(TestException("asdf"))
  assert "asdf" in str(ex)

def test_send_exception_by_type_and_value(coro_factory):
  def _syncgen(sync_yield):
    v = 0
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
    v = sync_yield(v) + 1
  x = coro_factory(_syncgen)
  assert x.send(None) == 0
  assert x.send(4) == 5
  with pytest.raises(TestException) as ex:
    x.throw(TestException, TestException("asdf"))
  assert "asdf" in str(ex)

def test_send_exception_catch_and_continue(coro_factory):
  def _syncgen(sync_yield):
    v = 0
    v = sync_yield(v) + 1
    with pytest.raises(TestException) as ex:
      sync_yield(v)
    sync_yield(5)
    sync_yield(ex.value)
  x = coro_factory(_syncgen)
  assert x.send(None) == 0
  assert x.send(4) == 5
  ex = TestException("blarg")
  assert x.throw(ex) == 5
  assert x.send(-1) is ex

def test_explicit_close(coro_factory):
  raised = [False]
  def _syncgen(sync_yield):
    try:
      sync_yield(10)
    finally:
      assert sys.exc_info()[0] is GeneratorExit
      raised[0] = True
    assert False, "not reached"
  x = coro_factory(_syncgen)
  assert next(x) == 10
  x.close()
  assert raised[0] is True

def test_implicit_close(coro_factory):
  raised = [False]
  def _syncgen(sync_yield):
    try:
      sync_yield(10)
    finally:
      assert sys.exc_info()[0] is GeneratorExit
      raised[0] = True
    assert False, "not reached"
  x = coro_factory(_syncgen)
  assert next(x) == 10
  assert raised[0] is False
  del x
  assert raised[0] is True

def test_yield_during_shutdown(coro_factory):
  def _syncgen(sync_yield):
    try:
      sync_yield(10)
    except GeneratorExit:
      sync_yield(12345)
  x = coro_factory(_syncgen)
  assert next(x) == 10
  with pytest.warns(RuntimeWarning, match=r'12345'):
    x.close()

def test_return_value(coro_factory):
  def _syncgen(_sync_yield):
    return 5
  x = coro_factory(_syncgen)
  with pytest.raises(StopIteration) as ex:
    x.send(None)
  assert ex.value.args == (5,)

def test_obsolete_yield_handle(coro_factory):
  handle = [None]
  def _syncgen(sync_yield):
    handle[0] = sync_yield
    pass
  list(coro_factory(_syncgen))
  with pytest.raises(RuntimeError):
    handle[0](5)  # pylint: disable=not-callable

def test_yield_wrong_thread(coro_factory):
  handle = [None]
  def _syncgen(sync_yield):
    sync_yield(sync_yield)
    sync_yield(10)
  x = coro_factory(_syncgen)
  handle = x.send(None)
  with pytest.raises(RuntimeError):
    handle(20)
