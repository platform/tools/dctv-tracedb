# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Parsing code for SQL"""

# Allow us to use syntax that's convenient for spelling the
# SQL grammar.

# pylint: disable=bad-docstring-quotes,line-too-long
# pylint: disable=no-self-argument,unsupported-assignment-operation
# pylint: disable=unsubscriptable-object,useless-import-alias
# pylint: disable=docstring-first-line-empty

import sys
import os
from os.path import dirname, join as pjoin

import threading
from contextlib import contextmanager

import logging
import re

import ply.lex as lex
import ply.yacc as yacc

import attr
from cytoolz import merge

from modernmp.util import once, the
from .sql import (
  BetweenAndOperation,
  BinaryFunExprOperation,
  BinaryOperation,
  BindParameter,
  Cast,
  CollationTag,
  ColumnNameList,
  ColumnReference,
  Compound,
  CreateNamespace,
  CreateView,
  CteBinding,
  CteBindingName,
  Drop,
  ExpressionColumn,
  FalseLiteral,
  FloatLiteral,
  FunExprDict,
  FunExprDictItem,
  FunExprKeywordArgument,
  FunExprList,
  FunctionCall,
  GroupByExpressions,
  GroupBySpanGrouper,
  GroupBySpanPartition,
  IntegerLiteral,
  InvalidQueryException,
  Join,
  KeywordArgument,
  MountTrace,
  NullLiteral,
  OrderingTerm,
  RegularSelect,
  RowValue,
  SelectDirect,
  SelectWithCte,
  StringLiteral,
  SubqueryExpression,
  TableFunctionCall,
  TableReference,
  TableSubquery,
  TableValues,
  TrueLiteral,
  TvfDot,
  TvfFunctionCall,
  UnaryFunExprOperation,
  UnaryOperation,
  UnitConversion,
  WildcardColumn,
)

from .util import (
  NoneType,
  once,
  unlink_if_exists,
)

log = logging.getLogger(__name__)

def _my_modtime():
  return os.stat(__file__).st_mtime

def _make_string_re(quote_chars, *, allow_empty=True):
  rx = r"[X]([^X]|[X][X])*[X]".replace("X", quote_chars)
  if not allow_empty:
    rx = rx.replace("*", "+")
  return rx

@once()
def _get_parsing_tls():
  tls = threading.local()
  tls.error_recovery = False
  return tls

@contextmanager
def allow_error_recovery():
  """Context manager allowing SQL parsing error recovery"""
  tls = _get_parsing_tls()
  old_error_recovery = tls.error_recovery
  tls.error_recovery = True
  try:
    yield
  finally:
    tls.error_recovery = old_error_recovery

class SqlParsingException(InvalidQueryException):
  """Exception indicating parse error"""
  def __init__(self, lexpos):
    super().__init__()
    self.__lexpos = the((int, NoneType), lexpos)

  def __str__(self):
    return "Parse error at {}".format(self.lexpos)

  @property
  def lexpos(self):
    """The lexical position at which the error occurred

    None if EOF.
    """
    return self.__lexpos

class SqlLexer(object):
  """PLY lexer for DCTV SQL code"""

  @staticmethod
  def t_error(t):  # pylint: disable=missing-docstring
    if _get_parsing_tls().error_recovery:  # pylint: disable=no-else-return,inconsistent-return-statements
      nextc = t.lexer.lexdata[t.lexpos:t.lexpos+1]
      if nextc == "'":
        t.type = "SINGLE_QUOTED_STRING"
      elif nextc == "\"":
        t.type = "DOUBLE_QUOTED_STRING"
      elif nextc == "`":
        t.type = "BACK_QUOTED_STRING"
      else:
        t.lexer.skip(1)
        return None  # Try another token
      t.lexer.skip(len(t.value))
      return t
    else:
      raise SqlParsingException(t.lexpos)

  # Keep sorted
  reserved_keywords = {
    "ALL",
    "AND",
    "ARRAY",
    "AS",
    "ASC",
    "ASSERT",
    "ASSERTING",
    "BETWEEN",
    "BROADCAST",
    "BY",
    "CASE",
    "CAST",
    "COLLATE",
    "CONVERT",
    "CREATE",
    "CROSS",
    "CURRENT_DATE",
    "CURRENT_TIME",
    "CURRENT_TIMESTAMP",
    "DESC",
    "DISTINCT",
    "DOCUMENTATION",
    "DROP",
    "ELSE",
    "END",
    "ESCAPE",
    "EVENT",
    "EVENTS",
    "EXCEPT",
    "EXISTS",
    "EXPLAIN",
    "FALSE",
    "FROM",
    "FUNCTION",
    "GLOB",
    "GROUP",
    "HAVING",
    "IF",
    "ILIKE",
    "IN",
    "INDEXED",
    "INNER",
    "INTERSECT",
    "INTO",
    "IS",
    "ISNOT",
    "ISNULL",
    "JOIN",
    "LEFT",
    "LIKE",
    "LIMIT",
    "MATCH",
    "NAMESPACE",
    "NATURAL",
    "NOT",
    "NOTNULL",
    "NULL",
    "OFFSET",
    "ON",
    "OR",
    "ORDER",
    "OUTER",
    "OVER",
    "PARTITION",
    "PARTITIONED",
    "PARTITIONS",
    "RECURSIVE",
    "REGEXP",
    "RIGHT",
    "SELECT",
    "SEMI",
    "SPAN",
    "SPANS",
    "THE",
    "THEN",
    "TO",
    "TRUE",
    "UNION",
    "UNIT",
    "UNSAFE",
    "USING",
    "VALUES",
    "VIEW",
    "WHEN",
    "WHERE",
    "WITH",
  }

  non_reserved_keywords = {
    "MOUNT",
    "REPLACE",
    "TRACE",
  }

  assert not reserved_keywords & non_reserved_keywords
  keywords = reserved_keywords | non_reserved_keywords
  all_keyword_re = re.compile("|".join(map(re.escape, keywords)), re.I)

  t_ignore = " \t\r\n\v\f"
  comment_re = r"(?:/\*(.|\n)*?\*/)|(?:(?:--[ \t\r\n\v\f]|[#]).*)"
  ws_or_comment = "(?:(?:[{w}])|(?:{c}))+".format(w=t_ignore, c=comment_re)

  @staticmethod
  def t_comment(t):  # pylint: disable=missing-docstring
    pass

  # Make the two-word sequence IS NOT a token in its own right to
  # avoid grammar ambiguity in "left IS NOT right" --> does that mean
  # "(left) IS NOT (right)" or "(left) IS (NOT RIGHT)"?  We want to
  # force the former interpretation.
  @staticmethod
  def t_IS_NOT(t):  # pylint: disable=missing-docstring
    t.type = "IS_NOT"
    return t

  t_INTEGER = r"\d+"
  t_HEX_INTEGER = r"0x([0-9a-fA-F]+)"
  t_FLOAT = r"((\d*\.\d+)(E[\+-]?\d+)?|([1-9]\d*E[\+-]?\d+))"
  t_DOUBLE_SLASH = r"//"
  t_LSHIFT = r"<<"
  t_RSHIFT = r">>"
  t_SPACESHIP = r"<=>"
  t_SPACESHIP_NOT = r"<!=>"
  t_GE = r">="
  t_GT = r">"
  t_LE = r"<="
  t_LT = r"<"
  t_NE = r"<>|!="
  t_DOUBLE_EQ = r"=="

  t_FAT_ARROW = r"=>"

  t_DOUBLE_PIPE = r"\|\|"

  t_DOTSTAR = r"\.\*"

  literals = list("(),.*{}[]:;-+~!/%&|=?")

  t_SINGLE_QUOTED_STRING = _make_string_re("'")
  t_DOUBLE_QUOTED_STRING = _make_string_re("\"")
  t_BACK_QUOTED_STRING = _make_string_re("`", allow_empty=False)

  @staticmethod
  def t_BAREWORD(t):
    r"[a-zA-Z_][a-zA-Z_0-9]*"
    m = SqlLexer.all_keyword_re.fullmatch(t.value)
    if m:
      t.type = t.value.upper()
      assert t.type in SqlLexer.keywords
    return t

  @staticmethod
  def t_newline(t):
    r"\n"
    t.lexer.lineno += 1

  @classmethod
  def load(cls):
    """Load the lexer from compiled file"""
    try:
      from . import sql_parser_lexer
    except ImportError:
      raise RuntimeError(
        "lexer unbuilt: run dctv regenerate-sql-parser")
    lexer = cls()
    return lex.lex(object=lexer, lextab=sql_parser_lexer, optimize=1)

def _extract_tokens(scope):
  tokens = scope.keywords.copy()
  token_re = re.compile(r"^t_([A-Z_]+)$")
  for name in dir(scope):
    m = token_re.match(name)
    if not m:
      continue
    assert m.group(1) not in tokens
    tokens.add(m.group(1))
  return list(sorted(tokens))

SqlLexer.tokens = _extract_tokens(SqlLexer)
SqlLexer.t_comment.__doc__ = SqlLexer.comment_re
SqlLexer.t_IS_NOT.__doc__ = "{is_}{ws}{not_}".format(
  is_=r"[iI][sS]",
  ws=SqlLexer.ws_or_comment,
  not_=r"[nN][oO][tT]")

@attr.s
class DelimitedList(object):
  """Placeholder for autogenerated delimited list parsing rules"""
  item_name = attr.ib(type=str)
  delimiter = attr.ib(type=str, default="','")
  allow_trailing_delimiter = attr.ib(type=bool, default=True)
  allow_empty = attr.ib(type=bool, default=True)

  def generate(self, list_name):
    """Generate the PLY rules for a delimited list of a thing"""
    fns = []

    def _p_list_name_base(p):
      "{list_name} : {item_name}"
      p[0] = [p[1]]
    fns.append(_p_list_name_base)

    def _p_list_name_append(p):
      "{list_name} : {list_name} {delimiter} {item_name}"
      p[1].append(p[3])
      p[0] = p[1]
    fns.append(_p_list_name_append)

    if self.allow_trailing_delimiter:
      def _p_list_name_trailing(p):
        "{list_name} : {list_name} {delimiter}"
        p[0] = p[1]
      fns.append(_p_list_name_trailing)

    if self.allow_empty:
      def _p_list_name_empty(p):
        "{list_name} : "
        p[0] = []
      fns.append(_p_list_name_empty)

    prefix = "p_" + list_name
    env = dict(list_name=list_name,
               item_name=self.item_name,
               delimiter=self.delimiter)

    for fn in fns:
      fn.__name__ = fn.__name__.replace("_p_list_name", prefix)
      if fn.__doc__:
        fn.__doc__ = fn.__doc__.format(**env)
      yield fn.__name__, staticmethod(fn)

class SqlParserMeta(type):
  """Metaclass for automatic staticmethod-ing"""
  def __new__(mcs, name, bases, dict_):
    for dname in tuple(dict_):
      dvalue = dict_[dname]
      if isinstance(dvalue, DelimitedList):
        del dict_[dname]
        dict_.update(dvalue.generate(dname))
        continue
      if not dname.startswith("p_"):
        continue
      func = dict_[dname]
      dict_[dname] = staticmethod(func)
    cls = super().__new__(mcs, name, bases, dict_)
    return cls

class SqlParser(object, metaclass=SqlParserMeta):
  """LALR(1) SQL parser"""

  tokens = SqlLexer.tokens

  def p_identifier_from_BAREWORD(p):
    "identifier : BAREWORD"
    p[0] = p[1]

  def p_identifier_from_non_reserved_kw(p):  # pylint: disable=missing-docstring
    # Docstring dynamically generated immediately below
    p[0] = p[1]

  def _make_non_reserved_kw_grammar():  # pylint: disable=no-method-argument
    lines = []
    for kw in sorted(SqlLexer.non_reserved_keywords):
      if not lines:
        lines.append("identifier : {}".format(kw))
      else:
        lines.append("           | {}".format(kw))
    return "\n".join(lines)

  p_identifier_from_non_reserved_kw.__doc__ = \
    _make_non_reserved_kw_grammar()
  del _make_non_reserved_kw_grammar

  def p_identifier_from_BACK_QUOTED_STRING(p):
    "identifier : BACK_QUOTED_STRING"
    p[0] = StringLiteral.decode(p[1])
    assert p[0], "We don't allow empty identifiers"

  dotted_identifier = DelimitedList(
    "identifier",
    delimiter="'.'",
    allow_trailing_delimiter=False,
    allow_empty=False)

  def p_literal_from_INTEGER(p):
    "literal : INTEGER"
    p[0] = IntegerLiteral(p[1])

  def p_literal_from_HEX_INTEGER(p):
    "literal : HEX_INTEGER"
    p[0] = IntegerLiteral(int(p[1], 16))

  def p_unit_literal_from_INTEGER(p):
    "literal : INTEGER identifier"
    p[0] = IntegerLiteral(p[1], p[2])

  def p_literal_from_FLOAT(p):
    "literal : FLOAT"
    p[0] = FloatLiteral(p[1])

  def p_unit_literal_from_FLOAT(p):
    "literal : FLOAT identifier"
    p[0] = FloatLiteral(p[1], p[2])

  def p_literal_from_NULL(p):
    "literal : NULL"
    p[0] = NullLiteral()

  def p_literal_from_TRUE(p):
    "literal : TRUE"
    p[0] = TrueLiteral()

  def p_literal_from_FALSE(p):
    "literal : FALSE"
    p[0] = FalseLiteral()

  def p_quoted_string(p):
    """quoted_string : SINGLE_QUOTED_STRING
                     | DOUBLE_QUOTED_STRING
    """
    p[0] = StringLiteral.decode(p[1])

  def p_literal_from_QUOTED_STRING(p):
    "literal : quoted_string"
    p[0] = StringLiteral(p[1])

  def p_bind_parameter_implicit(p):
    "bind_parameter : '?'"
    lexer = p.lexer  # pylint: disable=no-member
    bind_id = getattr(lexer, "_bind_arg", 0)
    setattr(lexer, "_bind_arg", bind_id + 1)
    p[0] = BindParameter(bind_id)

  def p_bind_parameter_explicit(p):
    "bind_parameter : '?' INTEGER"
    p[0] = BindParameter(int(p[2]))

  def p_bind_parameter_named(p):
    "bind_parameter : ':' identifier"
    p[0] = BindParameter(p[2])

  def p_literal_from_bind_parameter(p):
    "literal : bind_parameter"
    p[0] = p[1]

  def p_funarg_from_atom(p):
    "funarg : expression"
    p[0] = p[1]

  def p_funarg_keyword(p):
    "funarg : identifier FAT_ARROW expression"
    p[0] = KeywordArgument(p[1], p[3])

  funarg_list = DelimitedList("funarg", allow_empty=True)

  def p_atom_from_literal(p):
    "atom : literal"
    p[0] = p[1]

  def p_atom_from_cast(p):
    "atom : CAST '(' expression AS identifier ')'"
    p[0] = Cast(p[3], p[5].lower(), True)

  def p_atom_from_cast_unsafe(p):
    "atom : CAST '(' expression AS identifier UNSAFE ')'"
    p[0] = Cast(p[3], p[5].lower(), False)

  # TODO(dancol): this isn't really what CAST is for.  Can we just use
  # an out-of-the-way function tucked away somewhere not in
  # the grammar?
  def p_atom_unit_cast(p):
    "atom : CAST '(' expression AS UNIT identifier  ')'"
    p[0] = UnitConversion(p[3], p[6], allow_unitless=True)

  def p_atom_from_function_call(p):
    """atom : table_reference '(' funarg_list ')'
            | table_reference '(' '*' ')'
            | table_reference '(' DISTINCT funarg_list ')'
            | table_reference '(' ALL funarg_list ')'
    """
    # TODO(dancol): this logic is gross; split the rule.
    path = p[1].path
    distinct = p[3]
    if isinstance(distinct, str):
      distinct = distinct.lower()
    if distinct in ("all", "distinct"):
      arguments = p[4]
    elif distinct == "*":  # Hack
      if len(path) != 1 or path[0].lower() != "count":
        raise InvalidQueryException("only COUNT can be used with star syntax")
      distinct = None
      arguments = ()
    else:
      assert not isinstance(distinct, str)
      arguments = distinct
      distinct = None
    is_distinct = distinct == "distinct"
    if is_distinct and not arguments:
      raise InvalidQueryException("illegal use of DISTINCT without arglist")
    p[0] = FunctionCall(path, arguments, is_distinct)

  def p_atom_from_function_call_if(p):
    "atom : IF '(' funarg_list ')'"
    p[0] = FunctionCall(["if"], p[3])

  def p_atom_from_dotted_identifier(p):
    "atom : dotted_identifier"
    path = p[1]
    p[0] = ColumnReference(path[-1], path[:-1])

  # TODO(dancol): IN and NOT IN queries on subquery expressions
  # TODO(dancol): CASE

  precedence = (
    ("left", "."),
    ("right", "UNOP"),
    ("nonassoc", "COLLATE"),
    ("left", "*", "/", "%", "DOUBLE_SLASH"),
    ("left", "+", "-"),
    ("left", "LSHIFT", "RSHIFT", "DOUBLE_PIPE"),
    ("left", "&"),
    ("left", "|"),
    ("left", "BETWEEN"),
    ("nonassoc", "LIKE", "REGEXP", "ILIKE"),
    ("left", "=", "SPACESHIP", "SPACESHIP_NOT", "GE",
     "GT", "LE", "LT", "NE", "DOUBLE_EQ", "IS"),
    ("right", "NOT"),
    ("left", "AND"),
    ("left", "OR"),
    ("left", "UNION", "EXCEPT"),
    # This dummy precedence token hack eliminates a shift/reduce
    # conflict between reducing an empty s_order_by and glomming a
    # parenthesis onto a select_item.
    ("nonassoc", "_SELECT_ITEM_HIGH"),
  )[::-1]  # In source, we want strongest first, not last

  def p_unop(p):
    """expr_core : '+' expr_core  %prec UNOP
                 | '-' expr_core  %prec UNOP
                 | '!' expr_core  %prec UNOP
                 | '~' expr_core  %prec UNOP
    """
    p[0] = UnaryOperation(p[1], p[2])

  def p_unop_not(p):
    "expr_core : NOT expr_core  %prec UNOP"
    p[0] = UnaryOperation("not", p[2])

  def p_unit_conversion(p):
    "expr_core : expr_core IN identifier"
    p[0] = UnitConversion(p[1], p[3])

  def p_unit_conversion_to_dimensionless(p):
    "expr_core : expr_core IN NULL"
    p[0] = UnitConversion(p[1], None)

  def p_expr_core_binop(p):
    """expr_core : expr_core '+' expr_core
                 | expr_core '-' expr_core
                 | expr_core '/' expr_core
                 | expr_core DOUBLE_SLASH expr_core
                 | expr_core '*' expr_core
                 | expr_core '%' expr_core
                 | expr_core LSHIFT expr_core
                 | expr_core RSHIFT expr_core
                 | expr_core '&' expr_core
                 | expr_core '|' expr_core
                 | expr_core '=' expr_core
                 | expr_core SPACESHIP expr_core
                 | expr_core SPACESHIP_NOT expr_core
                 | expr_core GE expr_core
                 | expr_core GT expr_core
                 | expr_core LE expr_core
                 | expr_core LT expr_core
                 | expr_core NE expr_core
                 | expr_core DOUBLE_EQ expr_core
                 | expr_core DOUBLE_PIPE expr_core
    """
    p[0] = BinaryOperation(p[1], p[2], p[3])

  # TODO(dancol): figure out how to move the NOT variants of the
  # pattern operations into expr_core without confusing the LALR
  # parser.  As it stands, when using the NOT LIKE, NOT REGEXP, etc.
  # operators users may need a pair of extra parenthesis in
  # some situations.

  def p_expr_core_binop_is(p):
    "expr_core : expr_core IS expr_core"
    p[0] = BinaryOperation(p[1], "is", p[3])

  def p_expr_core_binop_regexp(p):
    "expr_core : expr_core REGEXP expr_core"
    p[0] = BinaryOperation(p[1], "regexp", p[3])

  def p_expr1_binop_not_regexp(p):
    "expr1 : expr1 NOT REGEXP expr1"
    p[0] = UnaryOperation("not", BinaryOperation(p[1], "regexp", p[4]))

  def p_expr_core_binop_like(p):
    "expr_core : expr_core LIKE expr_core"
    p[0] = BinaryOperation(p[1], "like", p[3])

  def p_expr1_binop_not_like(p):
    "expr1 : expr1 NOT LIKE expr1"
    p[0] = UnaryOperation("not", BinaryOperation(p[1], "like", p[4]))

  def p_expr_core_binop_ilike(p):
    "expr_core : expr_core ILIKE expr_core"
    p[0] = BinaryOperation(p[1], "ilike", p[3])

  def p_expr1_binop_not_ilike(p):
    "expr1 : expr1 NOT ILIKE expr1"
    p[0] = UnaryOperation("not", BinaryOperation(p[1], "ilike", p[4]))

  def p_expr_core_collate(p):
    "expr_core : expr_core COLLATE identifier"
    collation = p[3]
    if collation not in ("binary", "nocase", "length"):
      raise InvalidQueryException(
        "bad requested collation {!r}".format(collation))
    p[0] = CollationTag(p[1], collation)

  def p_expr_core_binop_complex(p):
    """expr_core : expr_core IS_NOT expr_core  %prec IS
                 | expr_core IS_NOT DISTINCT FROM expr_core  %prec IS
                 | expr_core IS DISTINCT FROM expr_core  %prec IS
    """
    left = p[1]
    right = p[len(p) - 1]
    op = " ".join(p[2 : len(p) - 1]).lower()
    p[0] = BinaryOperation(left, op, right)

  def p_expr_core_parenthesis(p):
    "expr_core : '(' expression ')'"
    p[0] = p[2]

  def p_expr_core_subquery(p):
    "expr_core : '(' select ')'"
    p[0] = SubqueryExpression(p[2])

  def p_expr_core_from_atom(p):
    "expr_core : atom"
    p[0] = p[1]

  def p_expr1_between_and(p):
    "expr1 : expr1 BETWEEN expr1 AND expr1  %prec BETWEEN"
    p[0] = BetweenAndOperation(p[1], p[3], p[5])

  def p_expr1_not_between_and(p):
    "expr1 : expr1 NOT BETWEEN expr1 AND expr1  %prec BETWEEN"
    p[0] = UnaryOperation("not",
                          BetweenAndOperation(p[1], p[4], p[6]))

  def p_expr1_from_expr_core(p):
    "expr1 : expr_core"
    p[0] = p[1]

  def p_expression_binop(p):
    """expression : expression AND expression
                  | expression OR expression
    """
    p[0] = BinaryOperation(p[1], p[2].lower(), p[3])

  def p_expression_from_expr1(p):
    "expression : expr1"
    p[0] = p[1]

  def p_s_distinct_or_all_empty(p):
    "s_distinct_or_all : "

  def p_s_distinct_or_all(p):
    """s_distinct_or_all : DISTINCT
                         | ALL
    """
    p[0] = {"distinct": p[1].lower()}

  def p_s_kind_empty(p):
    "s_kind : "

  def p_s_kind_span(p):
    "s_kind : SPAN"
    p[0] = {"kind": "span"}

  def p_s_kind_event(p):
    "s_kind : EVENT"
    p[0] = {"kind": "event"}

  def p_s_kind_repartition_by(p):
    "s_kind : EVENT PARTITIONED BY identifier"
    p[0] = {"kind": "event",
            "repartition_by": p[4]}

  def p_s_kind_repartition_by_nothing(p):
    "s_kind : EVENT PARTITIONED BY NULL"
    p[0] = {"kind": "event",
            "repartition_by": ''}

  def p_result_column_from_wildcard_qualified(p):
    "result_column : dotted_identifier DOTSTAR"
    p[0] = WildcardColumn(p[1])

  def p_result_column_from_wildcard(p):
    "result_column : '*'"
    p[0] = WildcardColumn()

  def p_result_column_from_expression_alias(p):
    "result_column : expression AS identifier"
    p[0] = ExpressionColumn(p[1], p[3])

  def p_result_column_from_expression(p):
    "result_column : expression"
    p[0] = ExpressionColumn(p[1])

  result_column_list = DelimitedList("result_column", allow_empty=True)

  def p_s_result_column_list(p):
    "s_result_column_list : result_column_list"
    p[0] = {"columns": p[1]}

  def p_s_from_empty(p):
    "s_from : "

  def p_s_from_from_table_expr(p):
    "s_from : FROM table_expr"
    p[0] = {"from_": p[2]}

  def p_named_join_op(p):
    """named_join_op : INNER
                     | LEFT
                     | LEFT OUTER
                     | OUTER
                     | RIGHT
                     | RIGHT OUTER
    """
    p[0] = p[1].lower()

  def p_join_op_comma(p):
    "join_op : ','"
    p[0] = "inner"

  def p_join_op_explicit(p):
    "join_op : named_join_op JOIN"
    p[0] = p[1]

  def p_broadcast_op_implicit(p):
    "broadcast_op : "
    p[0] = "inner"

  def p_broadcast_op_explicit(p):
    "broadcast_op : named_join_op"
    p[0] = p[1]

  def p_table_expr_span_broadcast(p):
    """
    table_expr : table_expr SPAN  broadcast_op BROADCAST INTO SPAN PARTITIONS table_term
               | table_expr EVENT broadcast_op BROADCAST INTO SPAN PARTITIONS table_term
    """
    #0           1          2     3            4         5    6    7          8
    p[0] = Join(left=p[1],
                op=p[3],
                right=p[8],
                kind=p[2].lower() + " broadcast")

  def p_table_expr_span_join(p):
    "table_expr : table_expr SPAN join_op table_term join_condition"
    #0            1          2    3       4          5
    p[0] = Join(left=p[1], op=p[3], right=p[4], kind="span join", on=p[5])

  # We require an explicit "SPANS" here to make it clear that even
  # though the word EVENT appears on the left, the RHS of the join has
  # to be a span table.
  def p_table_expr_event_join(p):
    "table_expr : table_expr EVENT named_join_op JOIN SPANS table_term"
    #0            1          2     3             4    5     6
    p[0] = Join(left=p[1], op=p[3], right=p[6], kind="event join")

  def p_table_expr_join(p):
    "table_expr : table_expr join_op table_term join_condition"
    p[0] = Join(left=p[1], op=p[2], right=p[3], on=p[4])

  def p_join_condition_empty(p):
    "join_condition : "

  def p_join_condition_on(p):
    "join_condition : ON expression"
    p[0] = p[2]

  column_name_list = DelimitedList("identifier", allow_empty=False)

  def p_join_condition_using(p):
    "join_condition : USING '(' column_name_list ')'"
    p[0] = ColumnNameList(p[3])

  def p_table_expr_from_table_term(p):
    "table_expr : table_term"
    p[0] = p[1]

  def p_table_term_as_alias(p):
    "table_term : table_core AS identifier"
    p[0] = p[1].evolve(name=p[3])

  def p_table_term_as_alias_with_renaming(p):
    "table_term : table_core AS identifier '(' column_name_list ')'"
    p[0] = p[1].rename_columns(p[3], tuple(p[5]))

  def p_partial_rename(p):
    "partial_rename : identifier FAT_ARROW identifier"
    p[0] = (p[1], p[3])

  def p_partial_rename_null(p):
    "partial_rename : identifier FAT_ARROW NULL"
    p[0] = (p[1], "")

  partial_rename_list = DelimitedList("partial_rename")

  def p_table_term_as_alias_with_renaming_partial(p):
    "table_term : table_core AS identifier '(' partial_rename_list ')'"
    # Treat the empty case as the empty tuple case so that the empty
    # column list does something useful in the span case.
    renamings = dict(p[5])
    if not renamings:
      renamings = ()  # pylint: disable=redefined-variable-type
    p[0] = p[1].rename_columns(p[3], renamings)

  def p_table_term_from_table_core(p):
    "table_term : table_core"
    p[0] = p[1]

  def p_table_core_from_table_reference(p):
    "table_core : table_reference"
    p[0] = p[1]

  def p_table_core_from_table_subquery(p):
    "table_core : '(' select ')'"
    p[0] = TableSubquery(p[2])

  row_value_item_list = DelimitedList("expression", allow_empty=False)

  def p_row_value(p):
    "row_value : '(' row_value_item_list ')'"
    p[0] = RowValue(p[2])

  row_value_list = DelimitedList("row_value", allow_empty=False)

  def p_values_core_anonymous(p):
    "values_core : VALUES row_value_list"
    p[0] = TableValues(p[2])

  def p_table_core_bind(p):
    "table_core : bind_parameter"
    p[0] = p[1]

  def p_tvf_expression_literal(p):
    "tvf_expression : literal"
    p[0] = p[1]

  tvf_expression_list_1 = DelimitedList("tvf_expression", allow_empty=True)

  def p_tvf_expression_list(p):
    "tvf_expression : '[' tvf_expression_list_1 ']'"
    p[0] = FunExprList(p[2])

  def p_tvf_dict_item(p):
    """tvf_dict_item : identifier FAT_ARROW tvf_expression
                     | tvf_expression ':' tvf_expression
    """
    p[0] = FunExprDictItem(p[1], p[3])

  tvf_dict_item_list = DelimitedList("tvf_dict_item", allow_empty=True)

  def p_tvf_expression_dict(p):
    "tvf_expression : '{' tvf_dict_item_list '}'"
    p[0] = FunExprDict(p[2])

  def p_tvf_expression_binop(p):
    """tvf_expression : tvf_expression '*' tvf_expression
                      | tvf_expression '/' tvf_expression
                      | tvf_expression '%' tvf_expression
                      | tvf_expression DOUBLE_SLASH tvf_expression
                      | tvf_expression '+' tvf_expression
                      | tvf_expression '-' tvf_expression
                      | tvf_expression LSHIFT tvf_expression
                      | tvf_expression RSHIFT tvf_expression
                      | tvf_expression '&' tvf_expression
                      | tvf_expression '|' tvf_expression
                      | tvf_expression '=' tvf_expression
                      | tvf_expression SPACESHIP tvf_expression
                      | tvf_expression SPACESHIP_NOT tvf_expression
                      | tvf_expression GE tvf_expression
                      | tvf_expression GT tvf_expression
                      | tvf_expression LE tvf_expression
                      | tvf_expression LT tvf_expression
                      | tvf_expression NE tvf_expression
                      | tvf_expression DOUBLE_EQ tvf_expression
                      | tvf_expression IS tvf_expression
                      | tvf_expression IS_NOT tvf_expression  %prec IS
                      | tvf_expression AND tvf_expression
                      | tvf_expression OR tvf_expression
    """
    p[0] = BinaryFunExprOperation(p[1], p[2].lower(), p[3])

  def p_tvf_expression_unop(p):
    """tvf_expression : '+' tvf_expression  %prec UNOP
                      | '-' tvf_expression  %prec UNOP
                      | '!' tvf_expression  %prec UNOP
                      | '~' tvf_expression  %prec UNOP
                      | NOT tvf_expression
    """
    p[0] = UnaryFunExprOperation(p[1].lower(), p[2])

  def p_tvf_expression_subquery(p):
    "tvf_expression : '(' select ')'"
    p[0] = TableSubquery(p[2])

  def p_tvf_expression_function_call(p):
    "tvf_expression : tvf_expression '(' tvf_arglist ')'"
    p[0] = TvfFunctionCall(p[1], p[3])

  def p_tvf_expression_parenthesis(p):
    "tvf_expression : '(' tvf_expression ')'"
    p[0] = p[2]

  def p_tvf_expression_table_reference(p):
    "tvf_expression : identifier"
    p[0] = TableReference((p[1],))

  def p_tvf_expression_dot(p):
    "tvf_expression : tvf_expression '.' identifier"
    p[0] = TvfDot(p[1], p[3])

  def p_tvf_funarg_kw(p):
    "tvf_funarg : identifier FAT_ARROW tvf_expression"
    p[0] = FunExprKeywordArgument(p[1], p[3])

  def p_tvf_funarg(p):
    "tvf_funarg : tvf_expression"
    p[0] = p[1]

  tvf_arglist = DelimitedList("tvf_funarg", allow_empty=True)

  def p_table_function_call(p):
    "table_function_call : table_reference '(' tvf_arglist ')'"
    p[0] = TableFunctionCall(p[1].path, p[3])

  def p_table_core_function_call(p):
    "table_core : table_function_call"
    p[0] = p[1]

  def p_table_core_from_paren_table_expr(p):
    "table_core : '(' table_expr ')'"
    p[0] = p[2]

  def p_table_reference(p):
    "table_reference : dotted_identifier"
    p[0] = TableReference(p[1])

  def p_s_where_empty(p):
    "s_where : "

  def p_s_where(p):
    "s_where : WHERE expression"
    p[0] = {"where": p[2]}

  non_empty_expression_list = DelimitedList("expression", allow_empty=False)

  def p_s_group_by_empty(p):
    "s_group_by : "

  def p_s_group_by(p):
    "s_group_by : GROUP BY non_empty_expression_list"
    p[0] = {"gb": GroupByExpressions(p[3])}

  def p_gb_intersect_mode_empty(p):
    "gb_intersect_mode : "
    p[0] = {}

  def p_gb_intersect_mode_intersect(p):
    "gb_intersect_mode : AND INTERSECT"
    p[0] = {"intersect": True}

  def p_gb_intersect_mode_union(p):
    "gb_intersect_mode : AND UNION"
    p[0] = {"intersect": False}

  def p_gb_output_partition_empty(p):
    "gb_output_partition : "
    p[0] = {}

  def p_gb_output_partition(p):
    "gb_output_partition : AND PARTITION BY identifier"
    p[0] = {"output_partition": p[4]}

  def p_s_group_using_partition(p):
    "s_group_by : GROUP gb_intersect_mode SPANS USING PARTITIONS gb_output_partition"
    p[0] = {"gb": GroupBySpanPartition(**merge(p[2], p[6]))}

  def p_s_group_spans_by_spans(p):
    "s_group_by : GROUP gb_intersect_mode SPANS INTO SPANS table_expr"
    p[0] = {"gb": GroupBySpanGrouper(p[6], "span", **p[2])}

  def p_s_group_events_by_spans(p):
    "s_group_by : GROUP gb_intersect_mode EVENTS INTO SPANS table_expr"
    p[0] = {"gb": GroupBySpanGrouper(p[6], "event", **p[2])}

  def p_s_having_empty(p):
    "s_having : "

  def p_s_having(p):
    "s_having : HAVING expression"
    p[0] = {"having": p[2]}

  def p_regular_select(p):
    "regular_select : SELECT s_distinct_or_all s_kind s_result_column_list s_from s_where s_group_by s_having"
    kwargs = {}
    for part in p[2:len(p)]:
      if part:
        assert isinstance(part, dict)
        kwargs.update(part)
    p[0] = RegularSelect(**kwargs)

  def p_select_term_regular(p):
    "select_term : regular_select"
    p[0] = p[1]

  def p_select_term_values(p):
    "select_term : values_core"
    p[0] = p[1]

  def p_select_term_intersect(p):
    "select_term : select_term INTERSECT regular_select"
    p[0] = Compound(p[1], p[2].lower(), p[3])

  def p_select_term_parm(p):
    "select_term : '(' select_item ')'"
    p[0] = p[2]

  def p_select_item_term(p):
    "select_item : select_term"
    p[0] = p[1]

  def p_select_item_union_all(p):
    "select_item : select_item UNION ALL select_term"
    p[0] = Compound(p[1], "union all", p[4])

  def p_select_item_binop(p):
    """select_item : select_item UNION select_term
                   | select_item EXCEPT select_term
    """
    p[0] = Compound(p[1], p[2].lower(), p[3])

  def p_ordering_term_explicit(p):
    """ordering_term : expression ASC
                     | expression DESC
    """
    p[0] = OrderingTerm(p[1], p[2].lower())

  def p_ordering_term(p):
    "ordering_term : expression"
    p[0] = OrderingTerm(p[1])

  order_by_list = DelimitedList("ordering_term", allow_empty=False)

  def p_s_order_by_empty(p):
    "s_order_by : %prec _SELECT_ITEM_HIGH"

  def p_s_order_by(p):
    "s_order_by : ORDER BY order_by_list"
    p[0] = {"ob": p[3]}

  def p_s_limit_offset_empty(p):
    "s_limit_offset : "

  def p_s_limit_offset_f1(p):
    "s_limit_offset : LIMIT expression OFFSET expression"
    p[0] = {"limit": p[2], "offset": p[4]}

  def p_s_limit_offset_f2(p):
    "s_limit_offset : LIMIT expression"
    p[0] = {"limit": p[2]}

  def p_s_limit_offset_f3(p):
    "s_limit_offset : OFFSET expression"
    p[0] = {"offset": p[2]}

  def p_s_limit_offset_f4(p):
    "s_limit_offset : LIMIT expression ',' expression"
    p[0] = {"offset": p[3], "limit": p[2]}

  def p_select(p):
    "select : select_item s_order_by s_limit_offset"
    kwargs = {}
    for item in p[2 : len(p)]:
      if item:
        kwargs.update(item)
    p[0] = SelectDirect(p[1], **kwargs)

  # We'd like to just specify a full tvf_expression as the right side
  # of a CTE binding, preferring the longest-CTE interpretation in
  # case of ambiguity, but we can't express this idea in LALR(1).
  # Instead, we whitelist a few common RHS forms for CTEs and make
  # users encase any complicated expressions in parenthesis.

  def p_cte_binding_name_plain(p):
    "cte_binding_name : identifier"
    p[0] = CteBindingName(p[1])

  def p_cte_binding_name_renamed(p):
    "cte_binding_name : identifier '(' column_name_list ')'"
    p[0] = CteBindingName(p[1], p[3], do_rename=True)

  def p_cte_binding_from_tvf_expression(p):
    "cte_binding : cte_binding_name AS '(' tvf_expression ')'"
    p[0] = CteBinding(p[1], p[4])

  def p_cte_binding_from_select(p):
    "cte_binding : cte_binding_name AS '(' select ')'"
    p[0] = CteBinding(p[1], TableSubquery(p[4]))

  def p_cte_from_tvf_funcall(p):
    "cte_binding : cte_binding_name AS table_reference '(' tvf_arglist ')'"
    p[0] = CteBinding(p[1], TableFunctionCall(p[3].path, p[5]))

  def p_cte_binding_recursive(p):
    # TODO(dancol): non-UNION-ALL recursive CTEs
    "cte_binding : RECURSIVE cte_binding_name AS '(' regular_select UNION ALL regular_select ')'"
    #0             1         2                3  4   5              6     7   8              9
    try:
      from .recursive import RecursiveTableSubquery
      p[0] = CteBinding(p[2], RecursiveTableSubquery(p[5], p[8]))
    except SyntaxError as ex:
      raise RuntimeError from ex

  cte_binding_list = DelimitedList("cte_binding", allow_empty=False)

  def p_common_table_expression(p):
    "select : WITH cte_binding_list select"
    p[0] = SelectWithCte(p[2], p[3])

  def p_opt_if_exists(p):
    "opt_if_exists : %prec _SELECT_ITEM_HIGH"
    p[0] = False

  def p_opt_if_exists_if_exists(p):
    "opt_if_exists : IF EXISTS"
    p[0] = True

  def p_maybe_view_function_empty(p):
    "maybe_view_function : "
    p[0] = {}

  def p_maybe_view_function(p):
    "maybe_view_function : FUNCTION"
    p[0] = {"as_function": True}

  def p_create_options_empty(p):
    "create_options : "
    p[0] = {}

  def p_create_options_or_replace(p):
    "create_options : OR REPLACE"
    p[0] = {"overwrite": True}

  def p_create_options_if(p):
    "create_options : IF tvf_expression"
    p[0] = {"condition": p[2]}

  def p_create_options_or_replace_if(p):
    "create_options : OR REPLACE IF tvf_expression"
    p[0] = {"condition": p[2], "overwrite": True}

  def p_documentation_empty(p):
    "documentation :"
    p[0] = {}

  def p_documentation(p):
    "documentation : WITH DOCUMENTATION quoted_string"
    #0               1    2             3
    p[0] = {"documentation": p[3]}

  def p_create_view(p):
    """create_view : CREATE create_options VIEW maybe_view_function dotted_identifier AS tvf_expression documentation
                   | CREATE create_options VIEW maybe_view_function dotted_identifier AS select documentation
    """
    #  0             1      2              3    4                   5                 6  7      8
    p[0] = CreateView(p[5], p[7], **merge(p[4], p[2], p[8]))

  def p_create_namespace(p):
    "create_namespace : CREATE create_options NAMESPACE dotted_identifier"
    #0                  1      2              3         4
    p[0] = CreateNamespace(p[4], **merge(*p[2]))

  def p_mount_trace(p):
    "mount_trace : MOUNT TRACE quoted_string AS dotted_identifier"
    p[0] = MountTrace(p[3], p[5])

  def p_drop(p):
    "drop : DROP opt_if_exists dotted_identifier"
    p[0] = Drop(p[3], ignore_absent=p[2])

  def p_dml_atom(p):
    """dml_atom : select
                | create_view
                | create_namespace
                | mount_trace
                | drop
    """
    p[0] = p[1]

  dml = DelimitedList("dml_atom", delimiter="';'", allow_empty=True)

  def p_error(p):  # pylint: disable=missing-docstring
    raise SqlParsingException(p.lexpos if p else None)  # pylint: disable=no-member

  @classmethod
  def __load_parser(cls, parser_module, start):
    parser_module_mtime = os.stat(parser_module.__file__).st_mtime
    if parser_module_mtime < _my_modtime():
      raise IOError("parser out of date: regenerate")
    return yacc.yacc(module=cls(),
                     tabmodule=parser_module,
                     debug=False,
                     optimize=True,
                     write_tables=False,
                     start=start)

  # We need these load functions to be explicit so that cx_freeze sees
  # the imports.

  @classmethod
  def load_expression_parser(cls):
    """Load the expression parser"""
    from . import sql_parser_expression
    return cls.__load_parser(sql_parser_expression, "expression")

  @classmethod
  def load_select_parser(cls):
    """Load the SELECT parser"""
    from . import sql_parser_select
    return cls.__load_parser(sql_parser_select, "select")

  @classmethod
  def load_dml_parser(cls):
    """Load the DML parser"""
    from . import sql_parser_dml
    return cls.__load_parser(sql_parser_dml, "dml")

@once()
def _make_lexer_core():
  return SqlLexer().load()

def make_lexer():
  """Return a fresh lexer instance"""
  return _make_lexer_core().clone()

@once()
def _make_expression_parser():
  return SqlParser().load_expression_parser()

@once()
def _make_select_parser():
  return SqlParser().load_select_parser()

@once()
def make_dml_parser():
  """Singleton parser for the full language"""
  return SqlParser().load_dml_parser()

def parse_expression(s):
  """Parse an expression and return a SQL AST"""
  return _make_expression_parser().parse(s, lexer=make_lexer())

def parse_select(s):
  """Parse a whole SELECT and return a SQL AST"""
  return _make_select_parser().parse(s, lexer=make_lexer())

def parse_dml(s):
  """Parse a sequence of data operation directives"""
  return make_dml_parser().parse(s, lexer=make_lexer())

def _regenerate_lexer(lexobj, output_file_name):
  mydir = dirname(__file__)
  lextab_file_name = pjoin(mydir, "lextab.py")  # PLY insists on it
  unlink_if_exists(lextab_file_name)
  lex.lex(object=lexobj, optimize=1)
  os.rename(lextab_file_name, output_file_name)
  log.info("Regenerated %s", output_file_name)

def _regenerate_parser(parser, output_file_name, start,
                       suppress_unreachable=False,
                       suppress_unused=False):
  mydir = dirname(__file__)
  parsetab_file_name = pjoin(mydir, "parsetab.py")
  parser_out_file_name = pjoin(mydir, "parser.out")
  unlink_if_exists(parsetab_file_name)
  unlink_if_exists(parser_out_file_name)
  logged_error = None

  def _log_filter(record):
    msg = record.msg
    if suppress_unreachable and "unreachable" in msg:
      return False
    if suppress_unused and ("defined, but not used" in msg or
                            "unused tokens" in msg):
      return False
    if record.levelno >= logging.WARNING:
      nonlocal logged_error
      if not logged_error:  # pylint: disable=used-before-assignment
        logged_error = record.getMessage()
    return True
  log.addFilter(_log_filter)
  try:
    yacc.yacc(module=parser, start=start, optimize=1, errorlog=log)
  except yacc.YaccError:
    if not logged_error:
      raise
  finally:
    log.removeFilter(_log_filter)
    if os.path.exists(parser_out_file_name):
      os.rename(parser_out_file_name,
                output_file_name + ".parser.out")
    if os.path.exists(parsetab_file_name):
      os.rename(parsetab_file_name, output_file_name)

  if logged_error:
    raise yacc.YaccError(logged_error)

  log.info("Regenerated %s", output_file_name)

def regenerate_sql_parser():
  """Regenerate SQL parser files"""

  # PLY is a pretty decent parser library, but its file regeneration
  # rules are _weird_.  We need to create and import an empty module
  # so that we can convince PLY to write its output to the
  # right place.
  if sys.flags.optimize:
    raise SystemError(
      "cannot regenerate SQL files in Python optimized mode")

  mydir = dirname(__file__)
  _regenerate_lexer(SqlLexer(), pjoin(mydir, "sql_parser_lexer.py"))
  _regenerate_parser(SqlParser(),
                     pjoin(mydir, "sql_parser_expression.py"),
                     "expression",
                     suppress_unreachable=True,
                     suppress_unused=True)
  _regenerate_parser(SqlParser(),
                     pjoin(mydir, "sql_parser_select.py"),
                     "select",
                     suppress_unreachable=True,
                     suppress_unused=True)
  _regenerate_parser(SqlParser(),
                     pjoin(mydir, "sql_parser_dml.py"),
                     "dml",
                     suppress_unused=True)

def dump_keywords():
  """Dump list of DCTV keywords"""
  for keyword in sorted(SqlLexer.keywords):
    print(keyword)

def generate_ebnf():
  """Emit EBNF representation of SQL syntax"""
  if sys.flags.optimize:
    raise SystemError(
      "cannot consult parser in Python optimized mode")
  from collections import defaultdict
  grammar = defaultdict(list)
  def _addprod(non_terminal, words):
    try:
      prec_idx = words.index("%prec")
    except ValueError:
      prec_idx = len(words)
    grammar[non_terminal].append(words[:prec_idx])

  for name in dir(SqlParser):
    if not name.startswith("p_"):
      continue
    value = getattr(SqlParser, name)
    doc = getattr(value, "__doc__", None)
    if not doc:
      continue
    lines = doc.split("\n")
    first_line = lines[0]
    words = first_line.split()
    non_terminal = words[0]
    assert words[1] == ":"
    _addprod(non_terminal, words[2:])
    for line in lines[1:]:
      words = line.split()
      if words:
        assert words[0] == "|"
        _addprod(non_terminal, words[1:])

  for non_terminal in sorted(grammar):
    for production in sorted(grammar[non_terminal]):
      print("{} : {}".format(non_terminal, " ".join(production)))
