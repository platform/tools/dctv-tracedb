// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "input_channel.h"

#include <iterator>
#include <tuple>

#include <stddef.h>

#include "block.h"
#include "block_builder.h"
#include "fmt.h"
#include "hunk.h"
#include "io_spec.h"
#include "npy.h"
#include "operator_context.h"
#include "output_channel.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "query_execution.h"

namespace dctv {

template<>
unique_obj_pyref<InputChannelSpec>
PythonConstructor<InputChannelSpec>::make(
    PyTypeObject* type,
    pyref args,
    pyref kwargs)
{
  PARSEPYARGS(
      (pyref, query)
  )(args, kwargs);
  return unique_obj_pyref<InputChannelSpec>::make(
      type, QueryKey::from_py(query));
}

InputChannelSpec::InputChannelSpec(QueryKey query)
    : query(std::move(query))
{}

int
InputChannelSpec::py_traverse(visitproc visit, void* arg) const noexcept
{
  if (int ret = this->query.py_traverse(visit, arg))
    return ret;
  return 0;
}

unique_obj_pyref<InputChannelSpec>
InputChannelSpec::from_py(pyref py_spec)
{
  if (isinstance_exact(py_spec, &InputChannelSpec::pytype))
    return py_spec.addref_as_unsafe<InputChannelSpec>();
  return make_pyobj<InputChannelSpec>(QueryKey::from_py(py_spec));
}

InputChannel::InputChannel(OperatorContext* oc,
                           const InputChannelSpec& spec,
                           OutputChannel* source)
    : query(spec.query),
      dtype(this->query.get_dtype().notnull()),
      oc(oc),
      source(source)
{
  assume(oc);
  assume(source);
  source->add_sink(this);
}

InputChannel::~InputChannel() noexcept
{
  this->close();
}

OutputChannel*
InputChannel::get_source() const noexcept
{
  return this->source;
}

npy_intp
InputChannel::get_read_wanted_hint() const noexcept
{
  return this->read_wanted_hint;
}

void
InputChannel::set_read_wanted_hint(npy_intp number_rows)
{
  this->read_wanted_hint = number_rows;
  if (this->source) {
    if (this->oc && number_rows > this->oc->get_block_size())
      this->source->enable_dynamic_block_size();
    this->source->on_backpressure_change();
  }
}

void
InputChannel::close() noexcept
{
  if (this->source) {
    this->source->remove_sink(this);
    this->source = nullptr;
  }
  if (this->oc) {
    this->oc->invalidate_score();
    this->oc = nullptr;
  }
  this->nr_available_rows = 0;
  this->read_wanted_hint = -1;
  this->blocks.clear();
}

void
InputChannel::change_nr_available_rows(npy_intp delta)
{
  this->nr_available_rows += delta;
  assume(this->nr_available_rows >= 0);
  if (this->source)
    this->source->on_backpressure_change();
  if (this->oc)
    this->oc->invalidate_score();
}

unique_obj_pyref<Block>
InputChannel::read(const npy_intp wanted_rows)
{
  assume(this->oc);
  assume(this->oc->get_qe());

  QueryCache* qc = this->oc->get_qe()->get_qc();
  assume(qc);

  assume(wanted_rows >= 0);
  assume(wanted_rows <= this->nr_available_rows);

  // No rows: no point doing extra work in the trivial case.
  if (wanted_rows == 0)
    return Block::make_empty(qc, this->dtype.addref());

  assume(!this->blocks.empty());
  // If we can satisfy this read request by just popping a block from
  // the queue, do that: it's the most efficient approach, since we
  // don't have to allocate a new block object.
  if (wanted_rows == this->blocks.front()->get_size()) {
    unique_obj_pyref<Block> head_block = std::move(this->blocks.front());
    assume(head_block->get_size() == wanted_rows);
    this->blocks.pop_front();
    this->change_nr_available_rows(-wanted_rows);
    return head_block;
  }

  // We have to make a new block to satisfy this read.  Luckily, we
  // have HunkReblocker, which abstracts away various complicated
  // merging and compaction code.

  HunkReblocker data_hunk_builder(0, wanted_rows);
  optional<HunkReblocker> mask_hunk_builder;

  for (;;) {
    assume(data_hunk_builder.nelem_remaining() <= wanted_rows);
    assume(!mask_hunk_builder ||
           (mask_hunk_builder->nelem_remaining() ==
            data_hunk_builder.nelem_remaining()));

    if (data_hunk_builder.done())
      break;

    assert(!this->blocks.empty());

    unique_obj_pyref<Block> head = std::move(this->blocks.front());
    this->blocks.pop_front();
    Hunk* data_hunk = head->get_data_hunk();
    Hunk* mask_hunk = head->get_mask_hunk();

    assert(data_hunk->get_size() == head->get_size());
    assert(!mask_hunk ||
           data_hunk->get_size() == mask_hunk->get_size());

    if (mask_hunk && !mask_hunk_builder) {
      mask_hunk_builder.emplace(0, wanted_rows);
      npy_intp catchup_nelem = wanted_rows -
          data_hunk_builder.nelem_remaining();
      mask_hunk_builder->add(
          Hunk::broadcast_false(qc, catchup_nelem).get());
    }

    unique_hunk remnant_data_hunk = data_hunk_builder.add(data_hunk);
    unique_hunk remnant_mask_hunk;
    if (mask_hunk_builder) {
      if (mask_hunk)
        remnant_mask_hunk = mask_hunk_builder->add(mask_hunk);
      else
        mask_hunk_builder->add(
            Hunk::broadcast_false(qc, data_hunk->get_size()).get());
    }

    if (!remnant_data_hunk) {
      assume(!remnant_mask_hunk);
    } else {
      assert(!remnant_mask_hunk ||
             remnant_mask_hunk->get_size() ==
             remnant_data_hunk->get_size());
      // We get a remnant chunk only when we're done building our
      // output chunk.
      assume(data_hunk_builder.done());

      unique_obj_pyref<Block> remnant_block = make_pyobj<Block>(
          std::move(remnant_data_hunk),
          std::move(remnant_mask_hunk));

      this->blocks.push_front(std::move(remnant_block));
    }
  }

  assume(data_hunk_builder.done());
  assume(!mask_hunk_builder || mask_hunk_builder->done());
  unique_hunk out_data_hunk = data_hunk_builder.finish(qc);
  assume(out_data_hunk);
  unique_hunk out_mask_hunk;
  if (mask_hunk_builder) {
    out_mask_hunk = mask_hunk_builder->finish(qc);
    assume(out_mask_hunk);
    assert(out_mask_hunk->get_size() ==
           out_data_hunk->get_size());
  }
  assert(out_data_hunk->get_size() == wanted_rows);
  this->change_nr_available_rows(-wanted_rows);
  return make_pyobj<Block>(
      std::move(out_data_hunk),
      std::move(out_mask_hunk));
}

void
InputChannel::add_block(Block* block)
{
  assume(block->get_dtype() == this->dtype);
  if (block->get_size()) {
    this->change_nr_available_rows(block->get_size());
    this->blocks.push_back(xaddref(block));
  }
}

bool
InputChannel::has_backpressure() const noexcept
{
  npy_intp rows = this->get_nr_available_rows();
  return (rows &&
          (this->read_wanted_hint < 0 ||
           this->read_wanted_hint < this->get_nr_available_rows()));
}

void
InputChannel::on_source_dead() noexcept
{
  assume(this->source);
  this->source = nullptr;
  if (this->oc)
    this->oc->invalidate_score();
}

int
InputChannel::py_traverse(visitproc visit, void* arg) const noexcept
{
  if (int ret = this->query.py_traverse(visit, arg))
    return ret;
  Py_VISIT(this->dtype.get());
  for (auto& block : this->blocks)
    Py_VISIT(block.get());  // NOLINT
  return 0;
}

InputChannel::operator String() const
{
  return fmt("<InputChannel q=%s #avail=%s eof=%s>",
             repr(this->query.as_pyref()),
             this->get_nr_available_rows(),
             this->is_eof());
}

template<typename InputRequest, typename... Args>
static
unique_pyref
make_input_request(pyref channel, Args&&... args)
{
  static_assert(std::is_base_of_v<IoInput, InputRequest>);
  return make_qe_call_io_single(
      InputRequest(
          channel.addref_as_unsafe<InputChannel>(),
          std::forward<Args>(args)...));
}

static
unique_pyref
InputChannel_py_read(PyObject* channel, pyref rows)
{
  npy_intp nr_rows = from_py<npy_intp>(rows);
  return make_input_request<IoInput>(channel, nr_rows, nr_rows);
}

static
unique_pyref
InputChannel_py_read_all(PyObject* channel)
{
  return make_input_request<IoInput>(
      channel,
      std::numeric_limits<npy_intp>::max(),
      std::numeric_limits<npy_intp>::max());
}

static
unique_pyref
InputChannel_py_read_at_least(PyObject* channel, pyref rows)
{
  npy_intp nr_rows = from_py<npy_intp>(rows);
  return make_input_request<IoInput>(
      channel,
      nr_rows,
      std::numeric_limits<npy_intp>::max());
}

static
unique_pyref
InputChannel_py_read_scalar(PyObject* channel)
{
  return make_input_request<IoInputScalar>(channel);
}

static
unique_pyref
InputChannel_py_read_int(PyObject* channel)
{
  return make_input_request<IoInputInt>(channel);
}

static
unique_pyref
InputChannel_get_is_eof(PyObject* self, void*) noexcept
{
  return make_pybool(pyref(self).as_unsafe<InputChannel>()->is_eof());
}



PyTypeObject InputChannelSpec::pytype =
    make_py_type<InputChannelSpec>(
        "dctv._native.InputChannelSpec",
        "Input channel specification");


PyMethodDef InputChannel::pymethods[] = {
  make_methoddef("read",
                 wraperr<&InputChannel_py_read>(),
                 METH_O,
                 "Read a number of rows"),
  make_methoddef("read_all",
                 wraperr<&InputChannel_py_read_all>(),
                 METH_NOARGS,
                 "Read all rows"),
  make_methoddef("read_at_least",
                 wraperr<&InputChannel_py_read_at_least>(),
                 METH_O,
                 "Read at least the given number of rows "
                 "but slurp more if available to avoid copies"),
  make_methoddef("read_scalar",
                 wraperr<&InputChannel_py_read_scalar>(),
                 METH_NOARGS,
                 "Read exactly one value and return it as a scalar, "
                 "not an array"),
  make_methoddef("read_int",
                 wraperr<&InputChannel_py_read_int>(),
                 METH_NOARGS,
                 "Like read_scalar, but ensure the value is an integer"),
  { 0 },
};

PyGetSetDef InputChannel::pygetset[] = {
  make_getset("is_eof",
              "True iff no more data will come through this channel",
              wraperr<InputChannel_get_is_eof>()),
  { 0 },
};

PyMemberDef InputChannel::pymembers[] = {
  make_memberdef("query",
                 T_OBJECT,
                 (offsetof(InputChannel, query)
                  + QueryKey::get_query_node_offset()),
                 READONLY,
                 "QueryNode that this channel provides"),
  make_memberdef("dtype",
                 T_OBJECT,
                 (offsetof(InputChannel, dtype) +
                  unique_pyref::get_pyval_offset()),
                 READONLY,
                 "Numpy dtype that this input channel supplies"),
  { 0 },
};

PyTypeObject InputChannel::pytype =
    make_py_type<InputChannel>(
        "dctv._native.InputChannel",
        "Channel for receiving blocks",
        [](PyTypeObject* t) {
          t->tp_members = InputChannel::pymembers;
          t->tp_getset = InputChannel::pygetset;
          t->tp_methods = InputChannel::pymethods;
        });


void
init_input_channel(pyref m)
{
  register_type(m, &InputChannelSpec::pytype);
  register_type(m, &InputChannel::pytype);
}

}  // namespace dctv
