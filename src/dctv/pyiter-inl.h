// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<bool Fast>
pyseq_iter_full<Fast>::pyseq_iter_full()
    : idx(0), size(0)
{}

template<bool Fast>
pyseq_iter_full<Fast>::pyseq_iter_full(pyref in_seq)
    : seq(Fast
          ? adopt_check(PySequence_Fast(in_seq.notnull().get(),
                                        "bad sequence"))
          : addref(in_seq.notnull())),
      idx(0),
      size(Fast
           ? PySequence_Fast_GET_SIZE(this->seq.get())
           : pyseq_size(this->seq))
{}

template<bool Fast>
pyseq_iter_full<Fast>
pyseq_iter_full<Fast>::end()
{
  return pyseq_iter_full();
}

template<bool Fast>
bool
pyseq_iter_full<Fast>::is_at_end() const
{
  return this->idx == this->size;
}

template<bool Fast>
bool
pyseq_iter_full<Fast>::equal(const pyseq_iter_full& other) const
{
  return (this->is_at_end() && other.is_at_end()) ||
      (this->seq == other.seq &&
       this->idx == other.idx &&
       this->size == other.size);

}

template<bool Fast>
typename pyseq_iter_full<Fast>::difference_type
pyseq_iter_full<Fast>::distance_to(const pyseq_iter_full& other) const
{
  if (this->is_at_end() && other.is_at_end())
    return 0;
  if (other.is_at_end()) {
    assert(!other.seq || this->seq == other.seq);
    return this->size - this->idx;
  }
  if (this->is_at_end()) {
    assert(!this->seq || this->seq == other.seq);
    return -(other.size - other.idx);
  }
  assert(this->seq == other.seq);
  return other.idx - this->idx;
}

pyseq_iter
begin(pyref value)
{
  return pyseq_iter(value);
}

pyseq_iter
end(pyref)
{
  return pyseq_iter::end();
}

template<typename Iterator, typename Functor>
auto
py2vec_full(pyref seq, const Functor& transform)
{
  using Value =
      typename std::decay_t<
        decltype(transform(*std::declval<Iterator>()))>;
  Vector<Value> result;
  Iterator it(seq);
  result.reserve(Iterator::end() - it);
  std::transform(std::move(it),
                 Iterator::end(),
                 std::back_inserter(result),
                 std::move(transform));
  return result;
}

template<typename Functor>
auto
py2vec(pyref seq, const Functor& transform)
{
  return py2vec_full<pyseq_iter, Functor>(seq, transform);
}

template<typename Functor>
auto
py2vec_fast(pyref seq, const Functor& transform)
{
  return py2vec_full<pyseq_iter_fast, Functor>(seq, transform);
}


}  // namespace dctv
