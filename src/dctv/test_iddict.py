# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for identity dictionary"""
from collections import UserList
from .iddict import (IdentityDictionary,
                     IdentitySet,
                     WeakKeyIdentityDictionary)

# pylint: disable=invalid-name

def test_identity_set():
  """Test IdentitySet"""
  s = IdentitySet()
  t1 = [1, 2]
  t2 = [1, 2]
  assert t1 is not t2
  assert t1 == t2
  assert t1 not in s
  assert t2 not in s
  s.add(t1)
  assert t1 in s
  assert t2 not in s
  assert len(s) == 1
  assert list(s) == [t1]
  s.add(t1)
  assert len(s) == 1
  s.add(t2)
  assert len(s) == 2
  assert t2 in s
  s.discard(t2)
  assert t2 not in s
  s.discard(t1)
  assert not s
  assert list(s) == []

def test_identity_dict():
  """Test IdentityDictionary"""
  d = IdentityDictionary()
  assert not d
  t1 = [1, 2]
  t2 = [1, 2]
  assert t1 is not t2
  assert t1 == t2
  assert t1 not in d
  assert t2 not in d
  d[t1] = 1
  assert d
  assert t1 in d
  assert t2 not in d
  assert d[t1] == 1
  d[t2] = 2
  assert d[t1] == 1
  assert d[t2] == 2
  assert len(d) == 2
  assert list(d.keys()) == [t1, t2]
  assert list(d.values()) == [1, 2]
  assert list(d.items()) == [(t1, 1), (t2, 2)]
  del d[t1]
  assert len(d) == 1
  assert d[t2] == 2
  del d[t2]
  assert not d

def test_weak_key_identity_dict():
  """Test WeakKeyIdentityDictionary"""
  d = WeakKeyIdentityDictionary()
  assert not d
  t1 = UserList([1, 2])
  t2 = UserList([1, 2])
  assert t1 is not t2
  assert t1 == t2
  assert t1 not in d
  assert t2 not in d
  d[t1] = 1
  assert d
  assert t1 in d
  assert t2 not in d
  assert d[t1] == 1
  d[t2] = 2
  assert d[t1] == 1
  assert d[t2] == 2
  assert len(d) == 2
  assert list(d.keys()) == [t1, t2]
  assert list(d.values()) == [1, 2]
  assert list(d.items()) == [(t1, 1), (t2, 2)]
  del d[t1]
  assert len(d) == 1
  assert d[t2] == 2
  del d[t2]
  assert not d
  d[t1] = 4
  assert len(d) == 1
  assert d[t1] == 4
  del t1
  import gc
  gc.collect()
  assert not d
  assert len(d) == 0  # pylint: disable=len-as-condition,compare-to-zero
