// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

#define AUTOMETHOD_DEFINE_META_MAKER(sym, arg_specs)             \
  struct sym {                                                   \
    static auto make_meta() {                                    \
      return PARSEPYARGS_MAKE_METADATA(arg_specs);               \
    }                                                            \
  }; static_assert(true, "force trailing semcolon")

template<typename FnType,
         typename R,
         bool IsNoexcept,
         typename... Args>
struct BaseFnInfo {
  using fn_type = FnType;
  using return_type = R;
  using arg_tuple_type = std::tuple<Args...>;
  static constexpr bool is_noexcept = IsNoexcept;
};

template<typename Fn>
struct FnInfo;

template<typename R, typename... Args>
struct FnInfo<R(*)(Args...)>
    : BaseFnInfo<R(*)(Args...),
                 R, /*IsNoexcept=*/false, Args...>
{};

template<typename R, typename... Args>
struct FnInfo<R(*)(Args...) noexcept>
    : BaseFnInfo<R(*)(Args...) noexcept,
                 R, /*IsNoexcept=*/true, Args...> {};

template<typename R, typename T, typename... Args>
struct FnInfo<R(T::*)(Args...)>
    : BaseFnInfo<R(T::*)(Args...),
                 R, /* IsNoexcept=*/false, T*, Args...> {};

template<typename R, typename T, typename... Args>
struct FnInfo<R(T::*)(Args...) noexcept>
    : BaseFnInfo<R(T::*)(Args...) noexcept,
                 R, /* IsNoexcept=*/true, T*, Args...> {};

template<typename R, typename T, typename... Args>
struct FnInfo<R(T::*)(Args...) const>
    : BaseFnInfo<R(T::*)(Args...) const,
                 R, /* IsNoexcept=*/false, const T*, Args...> {};

template<typename R, typename T, typename... Args>
struct FnInfo<R(T::*)(Args...) const noexcept>
    : BaseFnInfo<R(T::*)(Args...) const noexcept,
                 R, /* IsNoexcept=*/true, const T*, Args...> {};

template<typename T>
struct PyRefDecoder {
  static constexpr bool is_pyref = false;
  using object_type = void;
};

template<typename T>
struct PyRefDecoder<obj_pyref<T>> {
  static constexpr bool is_pyref = true;
  using object_type = T;
};

template<typename Want, typename Have>
auto
tweak_auto_arg(Have&& have_rref)
{
  static_assert(std::is_rvalue_reference_v<decltype(have_rref)>);
  using have_type = std::remove_reference_t<decltype(have_rref)>;
  using wanted_type = Want;
  if constexpr (std::is_convertible_v<have_type, wanted_type>) {
    return wanted_type(std::move(have_rref));  // NOLINT
  } else if constexpr (  // NOLINT
      PyRefDecoder<have_type>::is_pyref &&
      std::is_reference_v<wanted_type> &&
      std::is_convertible_v<
      std::add_pointer_t<
      typename PyRefDecoder<have_type>::object_type>,
      std::remove_reference_t<wanted_type>*>
  ) {
    struct Wrapper {
      operator wanted_type() {  // NOLINT
        return *this->obj;
      }
      have_type obj;
    };
    return Wrapper{std::move(have_rref)};  // NOLINT
  } else {
    errhack<have_type>::bad();
  }
}

template<typename ArgSpec,
         auto Fn,
         AutoMethodKind Kind,
         typename MetaMaker,
         typename Name,
         typename Doc>
constexpr
PyMethodDef
automethod(Name&&, Doc&&)
{
  namespace h = hana;
  using Info = FnInfo<decltype(Fn)>;

  constexpr auto wrapper = [](PyObject* py_self,
                              PyObject* py_args,
                              PyObject* py_kwds) noexcept
      -> PyObject* {
    return _exceptions_to_pyerr([&] {
      auto parse_args = [&]() {
        auto is_parse_arg_special = [](auto&& value) {
          return h::traits::is_base_of(h::type_c<pyparseargs_special>,
                                       h::typeid_(value));
        };
        return h::remove_if(
            h::unpack(parsepyargs<ArgSpec>(MetaMaker::make_meta(),
                                           py_args, py_kwds),
                      h::on(h::make_tuple, h::second)),
            is_parse_arg_special);
      };

      auto get_raw_arglist = [&]() {
        static_assert(Kind == AutoMethodKind::FUNCTION ||
                      Kind == AutoMethodKind::METHOD);
        using FnType = typename Info::fn_type;
        if constexpr (Kind == AutoMethodKind::FUNCTION) {
          return parse_args();
        } else if constexpr (  // NOLINT
            std::is_member_function_pointer_v<FnType> ||
            std::is_function_v<std::remove_pointer_t<FnType>>) {
          using ObjTypePtr =
              std::tuple_element_t<0, typename Info::arg_tuple_type>;
          using ObjType = std::decay_t<std::remove_pointer_t<ObjTypePtr>>;
          return h::prepend(parse_args(),
                            pyref(py_self).as_unsafe<ObjType>().get());
        } else {  // NOLINT
          errhack<decltype(Fn)>::unknown_function_type();
        }
      };

      // raw_arglist is the argument list that the pyparsetuple stuff
      // generated.  The function's signature may not match exactly
      // (e.g., it might want a pyref where we have a unique_pyref)
      // use get_arglist() to transform the raw argument list to one
      // suitable for application.  get_arglist() is allowed to move
      // from elements of raw_arglist, but raw_arglist itself is
      // guaranteed to extend over the lifetime of the call.
      // This lifetime guarantee is critical because the tuple that
      // get_arglist() returns may reference memory still owned by
      // raw_arglist.

      auto get_arglist = [&](auto* raw_arglist) {
        auto indices = h::unpack(
            h::make_range(h::size_c<0>, h::length(*raw_arglist)),
            h::make_tuple);
        return h::transform(
            indices,
            [&](auto&& idx) {
              using wanted_type =
                  std::tuple_element_t<CONSTEXPR_VALUE(idx),
                                       typename Info::arg_tuple_type>;
              return tweak_auto_arg<wanted_type>(
                  std::move((*raw_arglist)[idx]));
            });
      };

      auto call_function = [&]() {
        auto raw_arglist = get_raw_arglist();
        return h::unpack(get_arglist(&raw_arglist),
                         h::partial(h::apply, Fn));
      };

      using return_type = decltype(call_function());
      if constexpr (std::is_void_v<return_type>) {
        call_function();
        return addref(Py_None).release();
      } else {  // NOLINT
        return retval_to_py(call_function());
      }
    });
  };

  constexpr auto fnptr = static_cast<PyCFunctionWithKeywords>(wrapper);

  constexpr auto trimmed_name = (
      h::just(Name())
      | [](auto&& c) {
        return h::just(h::unpack(c, h::make_tuple));
      }
      | [](auto&& c) {
        return h::just(h::reverse(c));
      }
      | [](auto&& c) {
        return h::just(
            h::take_while(c,
                          [](auto&& char_) {
                            return (char_ != h::char_c<':'> &&
                                    char_ != h::char_c<'&'>); }));
      }
      | [](auto&& c) {
        return h::just(h::reverse(c));
      }
      | [](auto&& c) {
        return h::just(h::unpack(c, h::make_string));
      }
  ).value();

  return PyMethodDef {
    trimmed_name.c_str(),
    reinterpret_cast<PyCFunction>(fnptr),
    METH_VARARGS | METH_KEYWORDS,
    Doc().c_str(),
  };
}

template<typename T>
PyObject*
retval_to_py(unique_obj_pyref<T> ref) noexcept
{
  if (!ref)
    return addref(Py_None).release();
  return std::move(ref).template addref_as<PyObject>().release();
}

PyObject*
retval_to_py(bool value) noexcept
{
  return make_pybool(value).release();
}

PyObject*
retval_to_py(int value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(unsigned int value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(long value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(unsigned long value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(long long value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(unsigned long long value)
{
  return make_pylong(value).release();
}

PyObject*
retval_to_py(float value)
{
  return make_pyfloat(value).release();
}

PyObject*
retval_to_py(double value)
{
  return make_pyfloat(value).release();
}

PyObject*
retval_to_py(const String& s)
{
  return unique_pyref(make_pystr(s)).release();
}

}  // namespace dctv
