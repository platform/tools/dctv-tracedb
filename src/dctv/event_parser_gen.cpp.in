// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
/* This file is parsed with ragel */

#include "dctv.h"

#include <algorithm>
#include <arpa/inet.h>
#include <memory>

#include "event_parser_gen.h"
#include "fmt.h"
#include "linux.h"
#include "npyiter.h"
#include "optional.h"
#include "pcreutil.h"
#include "pyerr.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "pyseq.h"
#include "pythread.h"
#include "result_buffer.h"
#include "span.h"
#include "string_table.h"

namespace dctv {

namespace {

using std::min;
using std::make_unique;
using std::unique_ptr;
using std::string_view;

using FileOffset = uint64_t;

static const int event_parser_compilation_flags =
    PCRE2_ANCHORED |
    PCRE2_NEVER_UTF |
    PCRE2_NO_AUTO_CAPTURE |
    0;

struct EventParseError final : virtual PyException {
  EventParseError(size_t offset, String message) noexcept;
  const char* what() const noexcept override;
  void set_pyexception() const noexcept override;
 private:
  size_t offset;
  String message;
};

static
RawThreadStateFlags
decode_task_code_char(char c)
{
  // TODO(dancol): some of these flag values vary across kernel
  // versions.  When we parse binary trace logs, we'll have to
  // translate kernel-version-specific flag values to canonical ones.
  int code = static_cast<RawThreadStateFlags>(-1);
  switch (c) {
    case '|':
    case 'R':
      code = 0;
      break;
    case 'S':
      code = LINUX_TASK_INTERRUPTIBLE;
      break;
    case 'D':
      code = LINUX_TASK_UNINTERRUPTIBLE;
      break;
    case 'T':
      code = LINUX_TASK_STOPPED;
      break;
    case 't':
      code = LINUX_TASK_TRACED;
      break;
    case 'Z':
      code = LINUX_EXIT_DEAD;
      break;
    case 'X':
      code = LINUX_EXIT_ZOMBIE;
      break;
    case 'x':
      code = LINUX_TASK_DEAD;
      break;
    case 'K':
      code = LINUX_TASK_WAKEKILL;
      break;
    case 'W':
      code = LINUX_TASK_WAKING;
      break;
    case 'P':
      code = LINUX_TASK_PARKED;
      break;
    case 'N':
      code = LINUX_TASK_NOLOAD;
      break;
    case 'I':
      code = LINUX_TASK_IDLE;
      break;
    case '+':
      // A '+' indicates that trace_sched_switch got a true preempt
      // parameter; the kernel uses TASK_STATE_MAX to represent this
      // value being true because there's no task state flag for
      // task preemption.
      code = LINUX_TASK_STATE_MAX;
      break;
  }
  return code;
}

struct FieldDataSponge {
  using AbsorbFunction = void(*)(FieldDataSponge*,
                                 string_view,
                                 StringTable*);
  virtual AbsorbFunction get_absorb_function() const = 0;
  virtual void flush() = 0;
  virtual ~FieldDataSponge() = default;
};

template<typename Result, typename Converter>
struct FieldDataSpongeImpl final : FieldDataSponge {
  FieldDataSpongeImpl(unique_pyref result_callback,
                      QueryExecution* qe)
      : out(std::move(result_callback), qe)
  {}
  static void do_absorb(FieldDataSponge* fds,
                        string_view match,
                        StringTable* st) {
    auto this_ = static_cast<FieldDataSpongeImpl*>(fds);
    this_->out.add(Converter::convert(match, st));
  }
  AbsorbFunction get_absorb_function() const override {
    return &FieldDataSpongeImpl::do_absorb;
  }
  void flush() override {
    this->out.flush();
  }
 private:
  ResultBuffer<Result> out;
};

struct FieldConverterError final : virtual std::exception {
  explicit FieldConverterError(String message)
      : message(std::move(message))
  {}
  String get_message() const {
    return this->message;
  }
 private:
  String message;
};

struct FieldConverter {
  FieldConverter() = default;
  FieldConverter(const FieldConverter&) = delete;
  FieldConverter& operator=(const FieldConverter&) = delete;
  virtual unique_ptr<FieldDataSponge> make_data_sponge(
      unique_pyref result_callback,
      QueryExecution* qe) const = 0;
  virtual const char* get_dtype() const = 0;
  virtual ~FieldConverter() = default;
};

template<typename Result, typename Derived>
struct FieldConverterCrtp : FieldConverter {
  unique_ptr<FieldDataSponge> make_data_sponge(
      unique_pyref result_callback,
      QueryExecution* qe) const override {
    return make_unique<FieldDataSpongeImpl<Result, Derived>>(
        std::move(result_callback), qe);
  }
  const char* get_dtype() const override {
    return PyStructForType<Result>::tag;
  }
};

template<typename IntType, int Base>
IntType
convert_int(string_view match, StringTable*)
{
  static_assert(Base == 8 || Base == 10 || Base == 16);
  bool negative = false;
  IntType result = 0;

  if (std::is_signed_v<IntType> && !match.empty() && match[0] == '-') {
    negative = true;
    match = string_view(&match[1], match.size() - 1);
  }

  for (char c : match) {
    int place_value;
    if (Base == 10 || Base == 8) {
      place_value = c - '0';
    } else if (Base == 16) {
      if ('a' <= c && c <= 'f')
        place_value = c - 'a';
      else if ('A' <= c && c <= 'F')
        place_value = c - 'A';
      else
        place_value = c - '0';
    }
    if (place_value < 0 || place_value >= Base) {
      if (match == ".")
        return -1;
      throw FieldConverterError(
          fmt("bad integer for base %d: \"%s\"",
              Base, match));
    }
    result = result * Base + place_value;
  }
  return negative ? -result : result;
}

template<typename IntType, int Base>
struct IntConverter final :
    FieldConverterCrtp<IntType, IntConverter<IntType, Base>> {
  static IntType convert(string_view match, StringTable* st) {
    return convert_int<IntType, Base>(match, st);
  }
};

template<bool Strip>
struct StringConverter final :
    FieldConverterCrtp<StringTable::id_type, StringConverter<Strip>> {
  static StringTable::id_type convert(string_view match, StringTable* st) {
    if (Strip) {
      auto begin = match.begin();
      auto end = match.end();
      while (end > begin && *(end - 1) == ' ')
        --end;
      while (begin < end && *begin == ' ')
        ++begin;
      assert(begin <= end);
      match = string_view(begin, end - begin);
    }

    return st->intern(match);
  }
};

struct TimestampConverter final :
    FieldConverterCrtp<int64_t, TimestampConverter> {
  static int64_t convert(string_view match, StringTable* st) {
    string_view::size_type dotidx = match.find('.');
    if (dotidx == string_view::npos)
      throw FieldConverterError(fmt("invalid timestamp \"%s\"", match));
    string_view sec = match.substr(0, dotidx);
    string_view usec = match.substr(dotidx + 1);
    int64_t ts_sec = convert_int<int64_t, 10>(sec, st);
    int64_t ts_usec = convert_int<int64_t, 10>(usec, st);
    return ts_sec * 1000000000 + ts_usec * 1000;
  }
};

struct TaskStateConverter final :
    FieldConverterCrtp<RawThreadStateFlags, TaskStateConverter> {
  static RawThreadStateFlags convert(string_view match, StringTable*) {
    RawThreadStateFlags result = 0;
    for (char c : match) {
      RawThreadStateFlags code = decode_task_code_char(c);
      if (code == static_cast<RawThreadStateFlags>(-1))
        throw FieldConverterError(
            fmt("cannot interpret task status \"%s\"", match));
      result |= code;
    }
    return result;
  }
};

struct OrdConverter final :
    FieldConverterCrtp<int8_t, OrdConverter> {
  static int8_t convert(string_view match, StringTable*) {
    if (match.size() != 1)
      throw FieldConverterError(
          fmt("bad ordinal conversion: \"%s\"", match));
    return match[0];
  };
};

static
unique_ptr<FieldConverter>
make_converter(pyref py_converter)
{
  check_pytype(&PyUnicode_Type, py_converter);
  auto converter_name = pystr_to_string_view(py_converter);
  if (converter_name == "l")
    return make_unique<IntConverter<int64_t, 10>>();
  if (converter_name == "i")
    return make_unique<IntConverter<int32_t, 10>>();
  if (converter_name == "lx")
    return make_unique<IntConverter<int64_t, 16>>();
  if (converter_name == "h")
    return make_unique<IntConverter<int16_t, 10>>();
  if (converter_name == "b")
    return make_unique<IntConverter<int8_t, 10>>();
  if (converter_name == "timestamp")
    return make_unique<TimestampConverter>();
  if (converter_name == "s")
    return make_unique<StringConverter</*Strip=*/false>>();
  if (converter_name == "ss")
    return make_unique<StringConverter</*Strip=*/true>>();
  if (converter_name == "task_state")
    return make_unique<TaskStateConverter>();
  if (converter_name == "ord")
    return make_unique<OrdConverter>();

  _throw_pyerr_fmt(PyExc_ValueError,
                   "unknown converter %R",
                   py_converter.get());
}

struct Pattern final {
  explicit Pattern(string_view re, int options);
  Vector<string_view> get_named_captures() const;
  Vector<string_view> get_nr_to_name() const;
  template<typename T> T pcre2_info(int info) const;
  unique_pcre2_code cpat;
};

Pattern::Pattern(string_view re, int options)
    : cpat(compile_pcre2_pattern(re, options))
{
  jit_compile_pcre2(this->cpat.get());
  size_t used_jit = this->pcre2_info<size_t>(PCRE2_INFO_JITSIZE);
  if (!used_jit)
    throw_pyerr_msg(PyExc_ValueError, "PCRE JIT did not run");
}

template<typename T>
T
Pattern::pcre2_info(int info) const
{
  return get_pcre2_info<T>(info, this->cpat.get());
}

Vector<string_view>
Pattern::get_named_captures() const
{
  uint32_t name_count =
      this->pcre2_info<uint32_t>(PCRE2_INFO_NAMECOUNT);

  uint32_t name_entry_size =
      this->pcre2_info<uint32_t>(PCRE2_INFO_NAMEENTRYSIZE);

  const uint8_t* name_entry_table =
      this->pcre2_info<const uint8_t*>(PCRE2_INFO_NAMETABLE);

  Vector<string_view> names;
  names.reserve(name_count);
  for (uint32_t i = 0; i < name_count; ++i) {
    const uint8_t* entry = name_entry_table + name_entry_size * i;
    const char* name = reinterpret_cast<const char*>(entry + 2);
    names.push_back(name);
  }

  return names;
}

Vector<string_view>
Pattern::get_nr_to_name() const
{
  uint32_t capture_count =
      this->pcre2_info<uint32_t>(PCRE2_INFO_CAPTURECOUNT);

  uint32_t name_count =
      this->pcre2_info<uint32_t>(PCRE2_INFO_NAMECOUNT);

  uint32_t name_entry_size =
      this->pcre2_info<uint32_t>(PCRE2_INFO_NAMEENTRYSIZE);

  const uint8_t* name_entry_table =
      this->pcre2_info<const uint8_t*>(PCRE2_INFO_NAMETABLE);

  Vector<string_view> nr_to_name(capture_count + 1);
  for (uint32_t i = 0; i < name_count; ++i) {
    const uint8_t* entry = name_entry_table + name_entry_size * i;
    uint16_t nr_bigendian;
    memcpy(&nr_bigendian, entry, sizeof (nr_bigendian));
    uint16_t nr = ntohs(nr_bigendian);
    assume(nr < capture_count + 1);
    const char* name = reinterpret_cast<const char*>(entry + 2);
    nr_to_name[nr] = name;
  }
  return nr_to_name;
}

struct EventParser final : BasePyObject, HasPyCtor {
  struct FieldSpec final {
    static FieldSpec from_py(pyref py_spec);
    String name;
    unique_ptr<FieldConverter> converter;
  };

  using FieldSpecs = Vector<FieldSpec>;

  EventParser(String pattern, FieldSpecs specs);

  void parse(StringTable* st,
             const char* subject,
             size_t length,
             PyObject* offset_source,
             PyObject* result_callbacks,
             QueryExecution* qe) const;

  static PyTypeObject pytype;

 private:
  unique_pyref py_get_named_captures();
  static unique_pyref py_get_converter_dtype(PyObject*, PyObject* args);
  unique_pyref py_parse(PyObject* args);

  String pattern_text;
  Pattern pattern;
  uint32_t capture_count;
  Vector<FieldSpec> specs;
  Vector<uint32_t> spec_groupnos;

  Vector<unique_ptr<FieldDataSponge>> make_parse_sponges(
      pyref result_callbacks,
      QueryExecution* qe) const;

  static PyMethodDef pymethods[];
};

EventParser::EventParser(String pattern, FieldSpecs specs)
    : pattern_text(std::move(pattern)),
      pattern(this->pattern_text.c_str(),
              event_parser_compilation_flags),
      specs(std::move(specs))
{
  this->capture_count = this->pattern.pcre2_info<uint32_t>(
      PCRE2_INFO_CAPTURECOUNT);
  if (this->capture_count > 1024)  // Sanity check
    throw_pyerr_msg(PyExc_OverflowError, "too many capture groups");

  if (this->specs.size() > 1024)  // Sanity check
    throw_pyerr_msg(PyExc_OverflowError, "too many group specs");

  auto spec_to_groupno = [this](const FieldSpec& fs) {
    int rc = pcre2_substring_number_from_name(
        this->pattern.cpat.get(),
        reinterpret_cast<PCRE2_SPTR>(fs.name.c_str()));
    if (rc == PCRE2_ERROR_NOSUBSTRING)
      _throw_pyerr_fmt(PyExc_ValueError,
                       "group %R not in pattern",
                       make_pystr(fs.name).get());
    if (rc < 0)
      throw_pcre2_error_nogil(rc, "pcre2_substring_number_from_name");
    assume(rc > 0);  // Never the whole-match group
    return rc;
  };

  this->spec_groupnos.reserve(this->specs.size());
  std::transform(this->specs.begin(), this->specs.end(),
                 std::back_inserter(this->spec_groupnos),
                 spec_to_groupno);
}

EventParser::FieldSpec
EventParser::FieldSpec::from_py(pyref py_spec)
{
  PARSEPYARGS(
      (string_view, field_name)
      (pyref, converter)
  )(py_spec);
  return FieldSpec { String(field_name), make_converter(converter) };
}

Vector<unique_ptr<FieldDataSponge>>
EventParser::make_parse_sponges(
    pyref result_callbacks,
    QueryExecution* qe) const
{
  assume(this->specs.size() <= std::numeric_limits<int>::max());
  assume(this->spec_groupnos.size() == this->specs.size());
  const int nspecs = static_cast<int>(this->specs.size());

  auto throw_count_mismatch = [&] {
    throw_pyerr_fmt(PyExc_ValueError,
                    ("result callback count mismatch: "
                     "have %s callbacks and %s fields"),
                    pyseq_size(result_callbacks),
                    this->specs.size());
  };

  int specno = 0;
  auto mk_sponge = [&](unique_pyref cb)
      -> unique_ptr<FieldDataSponge> {
    if (specno >= nspecs)
      throw_count_mismatch();
    const auto& converter = *this->specs[specno++].converter;
    return converter.make_data_sponge(std::move(cb), qe);
  };
  Vector<unique_ptr<FieldDataSponge>> sponges =
      py2vec(result_callbacks, mk_sponge);
  if (sponges.size() != this->specs.size())
    throw_count_mismatch();
  return sponges;
}

void
EventParser::parse(StringTable* st,
                   const char* subject,
                   size_t length,
                   PyObject* offset_source,
                   PyObject* result_callbacks,
                   QueryExecution* qe) const
{
  const Vector<unique_ptr<FieldDataSponge>> sponges =
      this->make_parse_sponges(result_callbacks, qe);
  assume(sponges.size() < std::numeric_limits<int>::max());
  const int nspecs = sponges.size();

  // We effectively do the vtable lookup in advance, saving a few
  // cacheline misses in the hot call loop below.
  FieldDataSponge::AbsorbFunction* absorb_fns =
      static_cast<decltype(absorb_fns)>(
          alloca(sizeof (*absorb_fns) * nspecs));
  for (int i = 0; i < nspecs; ++i)
    absorb_fns[i] = sponges[i]->get_absorb_function();

  const uint32_t ngroups = this->capture_count + 1;
  const uint32_t* const spec_groupnos = this->spec_groupnos.data();

  const pcre2_code* const cpat_ptr = this->pattern.cpat.get();
  const unique_pcre2_match_data match_data =
      make_pcre2_match_data(cpat_ptr);
  const size_t* const ovec = pcre2_get_ovector_pointer(match_data.get());

  auto offit = PythonChunkIterator<FileOffset>(addref(offset_source));
  if (!offit.is_at_eof()) {
    with_gil_released([&] {
      do {
        const FileOffset raw_offset = std::get<0>(offit.get());
        if (raw_offset > length)
          throw_npyiter_error_nogil("offset out of range");
        const size_t offset = static_cast<size_t>(raw_offset);

        const int rc = pcre2_jit_match(
            cpat_ptr,
            reinterpret_cast<PCRE2_SPTR>(subject),
            length,
            offset,
            /*options=*/0,
            match_data.get(),
            /*mcontext=*/nullptr);

        // TODO(dancol): support skipping bad matches?
        if (rc == PCRE2_ERROR_NOMATCH)
          throw EventParseError(offset,
                                fmt("no match of regex %s",
                                    repr_nogil(this->pattern_text)));

        if (rc <= 0)
          throw_pcre2_error_nogil(rc, "pcre2_jit_match");

        assert(ovec[0] == offset);
        const size_t match_end = ovec[1];
        assert(subject[match_end - 1] == '\n');

        for (int i = 0; i < nspecs; ++i) {
          const uint32_t groupno = spec_groupnos[i];
          assume(groupno >= 1);
          assume(groupno < ngroups);
          const size_t so = ovec[groupno * 2 + 0];
          const size_t eo = ovec[groupno * 2 + 1];
          assert((so == PCRE2_UNSET) == (eo == PCRE2_UNSET));
          assume(so <= eo);
          // TODO(dancol): let converters handle no-match case
          if (so == PCRE2_UNSET)
            throw EventParseError(offset,
                                  fmt("missing match group %s",
                                      repr_nogil(this->specs[i].name)));

          const string_view submatch = so == PCRE2_UNSET
              ? string_view()
              : string_view(subject + so, eo - so);
          try {
            absorb_fns[i](&*sponges[i], submatch, st);
          } catch (const FieldConverterError& ex) {
            throw EventParseError(
                offset,
                fmt("error converting match group %s: %s",
                    repr_nogil(this->specs[i].name),
                    ex.get_message()));
          }
        }
      } while (offit.advance());
    });
  }

  for (int i = 0; i < nspecs; ++i)
    sponges[i]->flush();
}

unique_pyref
EventParser::py_get_named_captures()
{
  Vector<string_view> names = this->pattern.get_named_captures();
  return pytuple::from(names,
                       [](string_view name) {
                         return make_pystr(name);
                       });
}

unique_pyref
EventParser::py_get_converter_dtype(PyObject*, PyObject* args)
{
  PARSEPYARGS(
      (pyref, converter)
  )(args);
  return make_pystr(make_converter(converter)->get_dtype());
}

unique_pyref
EventParser::py_parse(PyObject* args)
{
  PARSEPYARGS(
      (pyref, string_table)
      (pyref, events_mapping)
      (pyref, offset_source)
      (pyref, result_callbacks)
      (QueryExecution*, qe)
  )(args);

  PyBuffer py_buffer(events_mapping);
  const char* const subject = static_cast<const char*>(py_buffer->buf);
  assume(py_buffer->len >= 0);
  this->parse(pyref(string_table).as<StringTable>().get(),
              subject,
              static_cast<size_t>(py_buffer->len),
              offset_source.get(),
              result_callbacks.get(),
              qe);
  return addref(Py_None);
}

PyMethodDef EventParser::pymethods[] = {
  make_methoddef("parse",
                 wraperr<&EventParser::py_parse>(),
                 METH_VARARGS,
                 "Parse a batch of events"),
  make_methoddef("get_named_captures",
                 wraperr<&EventParser::py_get_named_captures>(),
                 METH_NOARGS,
                 "Get list of named capture groups"),
  make_methoddef("get_converter_dtype",
                 wraperr<&EventParser::py_get_converter_dtype>(),
                 METH_VARARGS | METH_STATIC,
                 "Retrieve dtype yielded by a converter"),
  { 0 },
};



struct UserEventMatcher final : BasePyObject, HasPyCtor {
  explicit UserEventMatcher(Pattern pattern);
  optional<string_view> recognize(string_view payload);
  static PyTypeObject pytype;
 private:
  Pattern pattern;
  unique_pcre2_match_data match_data;
  Vector<string_view> nr_to_name;
  const size_t* ovec;
};

UserEventMatcher::UserEventMatcher(Pattern pattern)
    : pattern(std::move(pattern)),
      match_data(make_pcre2_match_data(this->pattern.cpat.get())),
      nr_to_name(this->pattern.get_nr_to_name())
{
  // All our match groups should have been named.
  for (size_t i = 1; i < this->nr_to_name.size(); ++i)
    assume(!this->nr_to_name[i].empty());
  this->ovec = pcre2_get_ovector_pointer(this->match_data.get());
}

optional<string_view>
UserEventMatcher::recognize(string_view payload)
{
  const int rc = pcre2_jit_match(
      this->pattern.cpat.get(),
      reinterpret_cast<PCRE2_SPTR>(payload.data()),
      payload.size(),
      /*startoffset=*/0,
      /*options=*/0,
      this->match_data.get(),
      /*mcontext=*/nullptr);

  // TODO(dancol): maybe it makes sense to use a callout and get the
  // group number directly.

  if (rc == PCRE2_ERROR_NOMATCH)
    return {};
  if (rc <= 0)
    throw_pcre2_error_nogil(rc, "pcre2_jit_match");
  for (size_t grpno = 1; grpno < this->nr_to_name.size(); ++grpno)
    if (this->ovec[grpno*2] != PCRE2_UNSET)
      return this->nr_to_name[grpno];
  return {};
}

%%{
  machine indexed_events;
  include "parsers.rl";
  write data;
}%%;

// TODO(dancol): incorporate timestamp into the index too so that we
// can time-bound event parsing operations.
using IndexResultBuffer = ResultBuffer<FileOffset, StringTable::id_type>;

__attribute__((noinline))
static
void
index_events(const char* const buffer,
             const char* const start,
             const size_t length,
             IndexResultBuffer* irb_io,
             StringTable* st,
             UserEventMatcher* um)
{
  // Temporarily move the result buffer into this function so that the
  // optimizer has tighter aliasing constraints.
  IndexResultBuffer irb = std::move(*irb_io);
  FINALLY(*irb_io = std::move(irb));

  int cs;
  const char* p = start;
  const char* const pe = p + length;

  const char* p_ev_start = nullptr;
  const char* p_ev_type_start = nullptr;
  const char* p_ev_type_end = nullptr;
  String munged_event_buffer;

  auto ev_start = [&] {
    p_ev_start = p;
    p_ev_type_start = nullptr;
    p_ev_type_end = nullptr;
  };

  auto evtype_start = [&] {
    p_ev_type_start = p;
  };

  auto evtype_end = [&] {
    p_ev_type_end = p;
  };

  auto on_user_event_start = [&] {
    p_ev_type_start = p;
  };

  auto on_user_event_end = [&] {
    optional<string_view> recognized =
    um->recognize(
        string_view(p_ev_type_start,
                    p - p_ev_type_start));
    if (!recognized)
      recognized = "trace_marker_write";
    p_ev_type_start = recognized->data();
    p_ev_type_end = p_ev_type_start + recognized->size();
  };

  auto ev_end = [&] {
    string_view event_type(p_ev_type_start,
                           p_ev_type_end - p_ev_type_start);
    FileOffset offset = p_ev_start - buffer;
    StringTable::id_type id = st->intern(event_type);
    irb.add(offset, id);
  };

  with_gil_released([&]{
    %% machine indexed_events;
    %% include "parsers.rl";
    %% write init;
    %% write exec;
  });

  if (cs == %%{ write error; }%% )
    throw EventParseError(p - buffer, "index error");
}

static
unique_pyref
index_event_chunk_native(PyObject*, PyObject* args)
{
  PARSEPYARGS(
      (pyref, events_mapping)
      (Py_ssize_t, segment_start)
      (obj_pyref<StringTable>, st)
      (obj_pyref<UserEventMatcher>, um)
      (pyref, result_callback)
      (QueryExecution*, qe)
  )(args);

  PyBuffer py_buffer(events_mapping);
  if (segment_start < 0 || segment_start > py_buffer->len)
    throw_pyerr_msg(PyExc_ValueError, "segment out of range");

  IndexResultBuffer index_out(result_callback.addref(), qe);
  const char* const buffer = static_cast<const char*>(py_buffer->buf);
  const char* const start = buffer + segment_start;
  size_t length = py_buffer->len - segment_start;
  index_events(buffer, start, length, &index_out, st.get(), um.get());
  index_out.flush();
  return addref(Py_None);
}



EventParseError::EventParseError(size_t offset, String message) noexcept
    : offset(offset), message(std::move(message))
{}

const char*
EventParseError::what() const noexcept
{
  try {
    return this->message.c_str();
  } catch (...) {
    return "[error making exception string]";
  }
}

void
EventParseError::set_pyexception() const noexcept
{
  _exceptions_to_pyerr([&]{
    unique_pyref exception_class =
        find_pyclass("dctv.trace_file_util", "ParseFailureError");
    unique_pyref exception =
        adopt_check(
            PyObject_CallFunction(
                exception_class.get(),
                "ns#",
                static_cast<Py_ssize_t>(this->offset),
                this->message.data(),
                static_cast<Py_ssize_t>(
                    std::min<size_t>(
                        std::numeric_limits<Py_ssize_t>::max(),
                        this->message.size()))));
    PyErr_SetObject(exception_class.get(), exception.get());
  });
}

}  // anonymous namespace



template<>
unique_obj_pyref<EventParser>
PythonConstructor<EventParser>::make(
    PyTypeObject* type,
    pyref args,
    pyref kwargs)
{
  PARSEPYARGS(
      (string_view, pattern)
      (pyref, field_specs)
  )(args, kwargs);
  return unique_obj_pyref<EventParser>::make(
      type,
      String(pattern),
      py2vec(field_specs, EventParser::FieldSpec::from_py)
  );
}

template<>
unique_obj_pyref<UserEventMatcher>
PythonConstructor<UserEventMatcher>::make(
    PyTypeObject* type,
    pyref args,
    pyref kwargs)
{
  PARSEPYARGS(
      (string_view, pattern)
  )(args, kwargs);
  return unique_obj_pyref<UserEventMatcher>::make(
      type,
      Pattern(String(pattern).c_str(),
              event_parser_compilation_flags));
}

PyTypeObject EventParser::pytype = make_py_type<EventParser>(
    "dctv._native.EventParser",
    "Fast event parser",
    [](PyTypeObject* t) {
      t->tp_methods = EventParser::pymethods;
    }
);

PyTypeObject UserEventMatcher::pytype = make_py_type<UserEventMatcher>(
    "dctv._native.UserEventMatcher",
    "Matches user events during indexing");

static PyMethodDef functions[] = {
  make_methoddef("index_event_chunk_native",
                 wraperr<index_event_chunk_native>(),
                 METH_VARARGS,
                 "Index a chunk of events"),
  { 0 }
};

void
init_event_parser_gen(pyref m)
{
  register_functions(m, functions);
  register_type(m, &EventParser::pytype);
  register_type(m, &UserEventMatcher::pytype);
}

}  // namespace dctv
