// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include "npy.h"
#include "pyrefspan.h"
#include "pyseq.h"
#include "pythread.h"
#include "pyutil.h"

namespace dctv {

using Index = int64_t;

struct NpyIteratorDeleter {
  inline void operator()(NpyIter* iter) const noexcept;
};
using unique_npy_iter = std::unique_ptr<NpyIter, NpyIteratorDeleter>;

unique_pyarray npy_iter_view(NpyIter* iter, int num);

DCTV_NORETURN_ERROR
void throw_npyiter_error_nogil(const char* errmsg);

template<int MinInputs, int MaxInputs>
struct InputCountBase {
  inline InputCountBase() noexcept = default;
  inline explicit InputCountBase(int nr_inputs) noexcept;
  inline int get_nr_inputs() const noexcept;
 private:
  int nr_inputs;
};

template<int NumberInputs>
struct InputCountBase<NumberInputs, NumberInputs> {
  InputCountBase() noexcept = default;
  explicit InputCountBase(int nr_inputs) noexcept
  { assume(nr_inputs == NumberInputs); }
  int get_nr_inputs() const noexcept { return NumberInputs; }
};

struct NpyIterConfig {
  NPY_CASTING casting = NPY_SAFE_CASTING;
  int oa_ndim = -1;
  int** op_axes = nullptr;
  npy_intp* itershape = nullptr;
  npy_intp buffersize = 0;
  npy_uint32* extra_op_flags = nullptr;
  npy_int32 iter_flags = NPY_ITER_BUFFERED | NPY_ITER_GROWINNER;
};

template<int MinInputs, int MaxInputs>
struct NpyIteration : InputCountBase<MinInputs, MaxInputs>,
                      private GilHeldCtorDtor
{
  static_assert(MinInputs >= 1, "must have at least one input");
  static_assert(MinInputs <= MaxInputs, "min must be less than max");

  // Constructors require GIL

  constexpr NpyIteration() noexcept;
  constexpr explicit NpyIteration(int nr_inputs) noexcept;
  NpyIteration(PyRefSpan<PyArrayObject> inputs,
               PyRefSpan<PyArray_Descr> dtypes,
               NpyIterConfig config = NpyIterConfig());
  static NpyIteration from_tuple(int nr_inputs,
                                 pyref tuple,
                                 PyRefSpan<PyArray_Descr> dtypes,
                                 NpyIterConfig config = NpyIterConfig());

  // GIL required
  inline pyarray_ref get_operand_array(int array_number) const noexcept;

  // GIL required
  inline unique_pyarray get_operand_view(int array_number) const;

  // Return true if after advance we're at another item, and return
  // false if we're at EOF.  GIL _not_ required.
  inline bool advance() noexcept;

  // GIL _not_ required
  inline bool is_at_eof() const noexcept;

  // GIL _not_ required
  inline size_t size() const noexcept;

  // GIL _not_ required
  void rewind();

  // GIL _not_ required
  template<typename T>
  inline typename std::enable_if_t<std::is_arithmetic_v<T>, T>
  get(int input_nr) const noexcept;

  // GIL _not_ required
  template<typename T>
  inline typename std::enable_if_t<std::is_same_v<T, BoolWrapper>, T>
  get(int input_nr) const noexcept;

  // GIL _not_ required
  template<typename T>
  inline void set(int input_nr, T value) noexcept;

 private:
  // GIL _not_ required
  void start_inner_loop() noexcept;

  // GIL _not_ required
  void reset_state();

  unique_npy_iter iter;
  NpyIter_IterNextFunc* iternext;
  uint8_t** ptr_data_array;
  npy_intp* ptr_inner_size;
  npy_intp* ptr_inner_stride;

  npy_intp inner_remaining;  // NOLINT
  std::array<uint8_t*, MaxInputs> data;
  std::array<npy_intp, MaxInputs> stride;
};

template<typename Value>
struct NpyIteration1 : NpyIteration<1, 1> {
  using Base = NpyIteration<1, 1>;
  explicit NpyIteration1(pyarray_ref input,
                         NpyIterConfig config = NpyIterConfig());

  // GIL _not_ required
  inline Value get() const noexcept;

  // GIL _not_ required
  inline void set(Value v) noexcept;
};

template<int MinInputs, int MaxInputs>
struct FlexiblePythonChunkIterator : private GilHeldCtorDtor {
  FlexiblePythonChunkIterator(int nr_inputs,
                              unique_pyref iter,
                              std::array<unique_dtype, MaxInputs> dtypes,
                              NpyIterConfig npy_config = NpyIterConfig());

  // GIL _not_ required
  template<typename T> inline T get(int input_nr) const noexcept;

  // GIL _not_ required
  inline bool is_at_eof() const noexcept;

  // GIL _not_ required
  inline bool advance();

  // GIL _not_ required
  inline Index get_index() const noexcept;

  // GIL _not_ required
  inline int get_nr_inputs() const noexcept;

 private:
  // GIL _is_ required
  void fetch_chunk();

  Index index;
  unique_pyref iter;
  NpyIterConfig npy_config;
  using MyNpyIteration = NpyIteration<MinInputs, MaxInputs>;
  MyNpyIteration npy_iter;
  std::array<unique_dtype, MaxInputs> dtypes;
};

template<typename... Types>
struct PythonChunkIterator :
    FlexiblePythonChunkIterator<sizeof...(Types), sizeof...(Types)>
{
  static constexpr int NrColumns = sizeof...(Types);
  using Base = FlexiblePythonChunkIterator<NrColumns, NrColumns>;
  using Row = std::tuple<Types...>;

  // Constructor requires GIL
  explicit PythonChunkIterator(
      unique_pyref iter,
      NpyIterConfig config = NpyIterConfig());

  // GIL _not_ required
  inline Row get() const noexcept;

 private:
  // GIL _not_ required
  template<size_t... I>
  Row get_internal(std::index_sequence<I...>) const noexcept;
};

void init_npyiter(pyref m);

}  // namespace dctv

#include "npyiter-inl.h"
