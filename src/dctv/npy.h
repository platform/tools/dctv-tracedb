// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <functional>

#include "pybuffer.h"
#include "pyutil.h"
#include "vector.h"

#define PY_ARRAY_UNIQUE_SYMBOL dctv_numpy_array
#ifndef DCTV_IS_NPY_CPP
# define NO_IMPORT
#endif
#include <numpy/arrayobject.h>
#include <numpy/ufuncobject.h>
#ifndef DCTV_IS_NPY_CPP
# undef NO_IMPORT
#endif

namespace dctv {

// Don't release the GIL for loops smaller than about this number
// of iterations.
constexpr int release_gil_threshold = 500;

using unique_pyarray = unique_obj_pyref<PyArrayObject>;
using pyarray_ref = obj_pyref<PyArrayObject>;
using unique_dtype = unique_obj_pyref<PyArray_Descr>;
using dtype_ref = obj_pyref<PyArray_Descr>;

template<typename T>
struct PyStructForType;

// Unfortunately, a constexpr char array directly in the struct
// triggers compiler bugs that lead to the array being undefined at
// link time, so we just gensym an array.

#define DEFINE_TYPE_TAG(_type, _dtype, _nptype)                         \
  inline const char UTX_GENSYM(_nptype##tag)[2] = {_dtype, '\0'};       \
  template<>                                                            \
  struct PyStructForType<_type> final {                                 \
    static constexpr const char* tag = UTX_GENSYM(_nptype##tag);        \
    static constexpr int nptype = _nptype;                              \
  }

// Numpy supports both explicitly 64-bit wide integers (NPY_LONGLONG,
// typecode 'q') and integers that happen to be as wide as the
// platform long (NPY_LONG, typecode 'l').  Now, you'd think that
// numpy would just implement NPY_LONG by aliasing it to NPY_LONGLONG
// or NPY_INT (32-bit) depending on build architecture, but that would
// make too much sense.  Instead, numpy maintains the
// explicitly-32-bit and explicitly-64-bit and "whatever long is" data
// types as distinct dtypes with distinct codes.  What's more, both
// dtype("l") and dtype("q") print as "dtype('int64')", making them
// hard to tell apart.  The distinction is important, though, since
// some routines (e.g., Pandas) explicitly coerce values to either the
// "platform type" (NPY_INT64) or to NPY_INT64, and NPY_INT64 *is*
// aliased to NPY_LONG, and if you happen to have a 'q'-dtype array,
// and Pandas wants a NPY_INT64 array, it'll make a damned copy for no
// damn reason.  The moral of the story is that you should never use
// the 'q' typecode and that we should always talk about numpy types
// using the npy_common.h explicit width macros.


DEFINE_TYPE_TAG(char, 'c', NPY_BYTE);
DEFINE_TYPE_TAG(int8_t, 'b', NPY_INT8);
DEFINE_TYPE_TAG(uint8_t, 'B', NPY_UINT8);
DEFINE_TYPE_TAG(int16_t, 'h', NPY_INT16);
DEFINE_TYPE_TAG(uint16_t, 'H', NPY_UINT16);
DEFINE_TYPE_TAG(int32_t, 'i', NPY_INT32);
DEFINE_TYPE_TAG(uint32_t, 'I', NPY_UINT32);
DEFINE_TYPE_TAG(int64_t, 'l', NPY_INT64);
DEFINE_TYPE_TAG(uint64_t, 'L', NPY_UINT64);

DEFINE_TYPE_TAG(float, 'f', NPY_FLOAT);
DEFINE_TYPE_TAG(double, 'd', NPY_DOUBLE);

DEFINE_TYPE_TAG(BoolWrapper, '?', NPY_BOOL);
DEFINE_TYPE_TAG(bool, '?', NPY_BOOL);

unique_dtype type_descr_from_type(int type);
unique_dtype as_dtype(pyref obj);
void check_sane_dtype(dtype_ref dtype);
char py_dtype_to_code(pyref orig_py_dtype);

template<typename T>
unique_dtype type_descr();

template<typename TFunctor>
auto npy_type_dispatch(pyref py_dtype, TFunctor&& functor);

template<typename TFunctor>
auto npy_type_dispatch_code(char code, TFunctor&& functor);

unique_pyarray make_uninit_pyarray(unique_dtype dtype, npy_intp nelem);
inline unique_pyarray make_empty_pyarray(unique_dtype dtype);

unique_pyarray make_pyarray(dtype_ref dtype, npy_intp size);
void npy_resize(pyarray_ref array, npy_intp size);
inline npy_intp npy_size(pyarray_ref array) noexcept;
inline npy_intp npy_size1d(pyarray_ref array) noexcept;
inline dtype_ref npy_dtype(pyarray_ref array) noexcept;
inline pyref npy_base(pyarray_ref array) noexcept;
inline int npy_ndim(pyarray_ref array) noexcept;
inline npy_intp* npy_dims(pyarray_ref array) noexcept;
inline npy_intp* npy_strides(pyarray_ref array) noexcept;
inline void* npy_data_raw(pyarray_ref array) noexcept;
template<typename T>
inline T* npy_data(pyarray_ref array) noexcept;
inline int npy_flags(pyarray_ref array) noexcept;
inline bool npy_writeable_p(pyarray_ref array) noexcept;
inline void npy_set_writeable(pyarray_ref array, bool writeable) noexcept;

unique_pyarray npy_flatten(pyarray_ref array);
unique_pyarray npy_ravel(pyarray_ref array);

DEFINE_PYTRAITS_NO_CONSTEXPR(PyArray_Descr, PyArrayDescr_Type);  // NOLINT
DEFINE_PYTRAITS_NO_CONSTEXPR(PyArrayObject, PyArray_Type);  // NOLINT
DEFINE_PYTRAITS_NO_CONSTEXPR(PyUFuncObject, PyUFunc_Type);  // NOLINT

unique_pyarray get_npy_slice(pyarray_ref array,
                             Py_ssize_t i1,
                             Py_ssize_t i2);

bool dtype_equiv(dtype_ref a, dtype_ref b) noexcept;

unique_pyarray pyarray_view(pyarray_ref array, dtype_ref dtype = {});

unique_pyarray as_any_pyarray(pyref value);
unique_pyarray as_base_1d_pyarray(pyref value, dtype_ref dtype = {});
unique_pyarray copy_pyarray(pyarray_ref array);
bool npy_any(pyarray_ref array);
bool npy_all(pyarray_ref array);

unique_pyarray make_true_array();
unique_pyarray make_false_array();

void npy_set_base(pyarray_ref array, unique_pyref obj);

unique_pyref make_ufunc_from_func_and_data(
    PyUFuncGenericFunction *func,
    void **data,
    char *types, int ntypes,
    int nin, int nout, int identity,
    const char *name, const char *doc, int unused);

template<typename... Args>
Vector<unique_dtype> make_dtype_vector();

// Return whether OBJ is a basic numpy array
inline bool is_base_ndarray(pyref obj);
// Typecheck an object as a basic numpy array
inline pyarray_ref check_base_ndarray(pyref obj);

// Equivalent to np.broadcast_to, which is implemented in Python.
unique_pyarray npy_broadcast_to(pyarray_ref array, npy_intp length);

void init_npy_early(pyref m);
void init_npy(pyref m);

}  // namespace dctv

#include "npy-inl.h"
