// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <utility>

#include "list.h"
#include "npy.h"
#include "query.h"
#include "query_key.h"

namespace dctv {

struct Block;
struct OutputChannel;

struct InputChannelSpec final
    : BasePyObject,
      SupportsGc,
      SupportsWeakRefs,
      HasPyCtor
{
  explicit InputChannelSpec(QueryKey query);
  QueryKey query;
  int py_traverse(visitproc visit, void* arg) const noexcept;
  static unique_obj_pyref<InputChannelSpec> from_py(pyref py_spec);
  static PyTypeObject pytype;
};

struct InputChannel final
    : BasePyObject,
      SupportsGc,
      SupportsWeakRefs,
      HasRepr
{
  InputChannel(OperatorContext* oc,
               const InputChannelSpec& spec,
               OutputChannel* source);
  ~InputChannel() noexcept;

  // Get the channel that produces the data going into this
  // input queue.
  OutputChannel* get_source() const noexcept;

  // Called only during setup to wire up the connections between data
  // producers and consumers.  Not just a constructor parameter
  // because we might create a consumer before its producer.

  // Called by OutputChannel when it's flushed or when it disappears.
  void on_source_dead() noexcept;

  // Called by OutputChannel when it has a block of data it wants to
  // propagate to readers.
  void add_block(Block* block);

  // Remove and retrieve data from the front of the queue.  Can return
  // an existing block from the queue or make a new one.
  unique_obj_pyref<Block> read(npy_intp wanted_rows);

  // Set a hint we use for backpressure calculations for the number of
  // rows we want to read.  -1 means to use the default read hint.
  void set_read_wanted_hint(npy_intp number_rows);

  // Return the number of rows we're trying to read right now from
  // this channel.  If -1, no read is currently pending.  In the
  // sloppy backpressure case, we allow for enqueuing one block of
  // data to an input channel that nobody is reading; for the strict
  // backpressure case, we wait for an explicit read.
  npy_intp get_read_wanted_hint() const noexcept;

  // Indicate that no more data will be read from this channel.
  void close() noexcept;

  // Return true iff no more data will be sent to this channel.
  inline bool is_eof() const noexcept;

  // Return the number of rows.
  inline npy_intp get_nr_available_rows() const noexcept;

  // Return a reference to the dtype we expect.
  inline const unique_dtype& get_dtype() const noexcept;

  // The context managing this channel.
  inline OperatorContext* get_owner() const noexcept;

  // Return whether producers should slow down.
  bool has_backpressure() const noexcept;

  // GC support.
  int py_traverse(visitproc visit, void* arg) const noexcept;

  // Description.
  explicit operator String() const;

  // The query whose result vector gets dumped bit by bit into this
  // input channel.
  const QueryKey query;
  const unique_dtype dtype;

  template<typename Functor>
  void enumerate_queued_blocks(Functor&& functor);

  static PyTypeObject pytype;
 private:
  void change_nr_available_rows(npy_intp delta);

  OperatorContext* oc = nullptr;
  OutputChannel* source = nullptr;
  // Why not a Deque?  The 4K-long unchangeable block size in libc++.
  List<unique_obj_pyref<Block>> blocks;
  npy_intp nr_available_rows = 0;
  npy_intp read_wanted_hint = -1;

  static PyMethodDef pymethods[];
  static PyGetSetDef pygetset[];
  static PyMemberDef pymembers[];
};

void init_input_channel(pyref m);

}  // namespace dctv

#include "input_channel-inl.h"
