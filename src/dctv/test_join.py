# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Tests for SQL joins

# pylint: disable=bad-whitespace,missing-docstring

import logging
import pytest

from .query import (
  InvalidQueryException,
)

from .test_sql import (
  ps,
  ps2qt,
)

from .sql import (
  BinaryOperation,
  ColumnNameList,
  ColumnReference,
  ExpressionColumn,
  Join,
  RegularSelect,
  SelectDirect,
  TableReference,
  TableSubquery,
)

from .test_query import (
  ALL_SCHEMAS,
  TestQueryTable,
  _execute_qt,
)

from .test_util import (
  parameterize_dwim,
)

from .test_binop import (
  get_example_tbl,
)

log = logging.getLogger(__name__)
N = None


# Test data

WEIGHTS = TestQueryTable(
  names=("id", "weight"),
  rows=[
    [0,  5],
    [1, 10],
    [2, 30],
    [3, 15],
  ])

LENGTHS = TestQueryTable(
  names=("id", "length"),
  rows=[
    [2, 200],
    [1, 100],
    [3, 300],
    [4, 400],
  ])

COLORS = TestQueryTable(
  names=("id", "color"),
  rows=[
    [2, -2],
    [1, -1],
    [3, -3],
    [4, -4],
  ])


# Parse tests

def test_parse_join():
  assert ps("SELECT a FROM foo INNER JOIN bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableReference(["foo"]), "inner", TableReference(["bar"])),
    ))

  assert ps("SELECT a FROM foo OUTER JOIN bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableReference(["foo"]), "outer", TableReference(["bar"])),
    ))

  assert ps("SELECT a FROM foo left outer join bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableReference(["foo"]), "left", TableReference(["bar"])),
    ))

  assert ps("SELECT a FROM foo right outer join bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableReference(["foo"]), "right", TableReference(["bar"])),
    ))

  assert ps("SELECT a FROM foo, bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableReference(["foo"]), "inner", TableReference(["bar"])),
    ))

  assert ps("SELECT a FROM foo, bar, qux") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(
        Join(TableReference(["foo"]), "inner", TableReference(["bar"])),
        "inner",
        TableReference(["qux"]))))

  assert ps("SELECT a FROM foo, bar ON foo.spam = bar.spam, qux") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(
        Join(TableReference(["foo"]), "inner", TableReference(["bar"]),
             BinaryOperation(ColumnReference("spam", ["foo"]),
                             "=",
                             ColumnReference("spam", ["bar"]))),
        "inner",
        TableReference(["qux"]))))

  assert ps("SELECT a FROM foo, bar USING (spam, wtf), qux") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(
        Join(TableReference(["foo"]), "inner", TableReference(["bar"]),
             ColumnNameList(["spam", "wtf"])),
        "inner",
        TableReference(["qux"]))))

def test_parse_left_subquery_join():
  expected = SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(TableSubquery(ps("SELECT foo")),
                 "inner",
                 TableReference(["bar"])),
    ))
  assert ps("SELECT a FROM (SELECT foo) INNER JOIN bar") == expected
  assert ps("SELECT a FROM ((SELECT foo) INNER JOIN bar)") == expected

def test_parse_named_join():
  assert ps("SELECT a FROM ((foo INNER JOIN bar) AS qux) "
            "INNER JOIN spam") == SelectDirect(
              core=RegularSelect(
                columns=[ExpressionColumn(ColumnReference("a"))],
                from_=Join(
                  Join(TableReference(["foo"]),
                       "inner",
                       TableReference(["bar"]),
                       name="qux"),
                  "inner",
                  TableReference(["spam"]))))


# Tests

def test_inner_one():
  query = ("SELECT lengths.length "
           "FROM weights INNER JOIN lengths ON weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("lengths.length",),
    rows=[
      [100],
      [200],
      [300],
    ])

def test_named_reference():
  query = ("SELECT qux.length "
           "FROM (weights INNER JOIN lengths ON "
           "weights.id = lengths.id) AS qux")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("qux.length",),
    rows=[
      [100],
      [200],
      [300],
    ])

def test_inner_flipped():
  query = ("SELECT lengths.length "
           "FROM weights INNER JOIN lengths ON lengths.id = weights.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("lengths.length",),
    rows=[
      [100],
      [200],
      [300],
    ])

def test_inner():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights INNER JOIN lengths ON weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "weights.weight", "lengths.length"),
    rows=[
      [1, 10, 100],
      [2, 30, 200],
      [3, 15, 300],
    ])

def test_left():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights LEFT OUTER JOIN lengths ON weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "weights.weight", "lengths.length"),
    rows=[
      [0,  5,   N],
      [1, 10, 100],
      [2, 30, 200],
      [3, 15, 300],
    ]).as_dict()

def test_multi_key():
  # Use a large number of key columns to force us down the join
  # "densification" path without an extreme number of rows.
  tbl_left = TestQueryTable(
    names=["key1", "key2", "key3", "key4", "key5", "key6", "value_left"],
    rows=[
      [1, 2, 2, 0, 0, 0, -5],
      [1, 3, 2, 0, 0, 0, -6],
      [0, 2, 4, 0, 0, 0, -7],
    ])
  right_rows=[
    [1, 3, 2, 0, 0, 0, -3],
    [1, 2, 2, 0, 0, 0, -2],
    [0, 2, 4, 0, 0, 0, -4],
  ]
  right_columns=list(map(list, zip(*right_rows)))
  extra_rows = 1500
  for column in right_columns:
    column.extend(range(100, 100 + extra_rows))

  tbl_right = TestQueryTable(
    names=["key1", "key2", "key3", "key4", "key5", "key6", "value_right"],
    columns=right_columns)
  q = """
  SELECT key1, key2, key3, value_left * value_right AS val
  FROM tbl_left INNER JOIN tbl_right
  USING (key1, key2, key3, key4, key5, key5)
  """
  qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
  assert _execute_qt(qt) == TestQueryTable(
    names=["key1", "key2", "key3", "val"],
    rows=[
      [1, 2, 2, 10],
      [1, 3, 2, 18],
      [0, 2, 4, 28],
    ])

def test_right():
  query = ("SELECT weights.id, lengths.id, weights.weight, lengths.length "
           "FROM weights RIGHT OUTER JOIN lengths ON weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "lengths.id", "weights.weight", "lengths.length"),
    rows=[
      [2, 2, 30, 200],
      [1, 1, 10, 100],
      [3, 3, 15, 300],
      [N, 4,  N, 400],
    ])

def test_outer():
  query = ("SELECT weights.id, lengths.id, weights.weight, lengths.length "
           "FROM weights OUTER JOIN lengths ON weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "lengths.id", "weights.weight", "lengths.length"),
    rows=[
      [0, N,  5,   N],
      [1, 1, 10, 100],
      [2, 2, 30, 200],
      [3, 3, 15, 300],
      [N, 4,  N, 400],
    ]).as_dict()

def test_bad_non_equi():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights INNER JOIN lengths")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  with pytest.raises(InvalidQueryException,
                     match="no join condition specified"):
    ps2qt(query, **env)

def test_bad_non_equi_op():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights INNER JOIN lengths ON weights.id < lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  with pytest.raises(InvalidQueryException, match="not an equi-join"):
    ps2qt(query, **env)

def test_where_moved():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights INNER JOIN lengths WHERE weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "weights.weight", "lengths.length"),
    rows=[
      [1, 10, 100],
      [2, 30, 200],
      [3, 15, 300],
    ]).as_dict()

def test_where_merged():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights INNER JOIN lengths "
           "WHERE weights.id = lengths.id AND weights.id <= 2 "
           "AND weights.id >= 1")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "weights.weight", "lengths.length"),
    rows=[
      [1, 10, 100],
      [2, 30, 200],
    ]).as_dict()

def test_where_inner_only():
  query = ("SELECT weights.id, weights.weight, lengths.length "
           "FROM weights LEFT JOIN lengths WHERE weights.id = lengths.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS)
  with pytest.raises(InvalidQueryException, match="no join condition specified"):
    ps2qt(query, **env)

def test_inner_multiple():
  query = ("SELECT weights.id, weights.weight, lengths.length, "
           "color "
           "FROM weights INNER JOIN lengths ON weights.id = lengths.id "
           "INNER JOIN colors ON colors.id = weights.id")
  env = dict(weights=WEIGHTS, lengths=LENGTHS, colors=COLORS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weights.id", "weights.weight", "lengths.length", "color"),
    rows=[
      [1, 10, 100, -1],
      [2, 30, 200, -2],
      [3, 15, 300, -3],
    ]).as_dict()

def test_column_list():
  q = ("SELECT weight, length "
       "FROM weights INNER JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=("weight", "length"),
    rows=[
      [10, 100],
      [30, 200],
      [15, 300],
    ]).as_dict()

def test_column_list_inner():
  q = ("SELECT id "
       "FROM weights INNER JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=["id"],
    rows=[
      [1],
      [2],
      [3],
    ]).as_dict()

def test_column_list_left():
  q = ("SELECT id "
       "FROM weights LEFT JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=["id"],
    rows=[
      [0],
      [1],
      [2],
      [3],
    ]).as_dict()

def test_column_list_right():
  q = ("SELECT id "
       "FROM weights RIGHT JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=["id"],
    rows=[
      [2],
      [1],
      [3],
      [4],
    ]).as_dict()

def test_column_list_outer():
  q = ("SELECT id "
       "FROM weights OUTER JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=["id"],
    rows=[
      [0],
      [1],
      [2],
      [3],
      [4],
    ]).as_dict()

def test_column_list_wildcard():
  q = ("SELECT * "
       "FROM weights OUTER JOIN lengths USING (id)")
  qt = ps2qt(q, weights=WEIGHTS, lengths=LENGTHS)
  assert _execute_qt(qt) == TestQueryTable(
    names=["id", "weight", "length"],
    rows=[
      [0,  5,   N],
      [1, 10, 100],
      [2, 30, 200],
      [3, 15, 300],
      [4,  N, 400],
    ]).as_dict()

def test_nulls():
  tbl_left = TestQueryTable(
    names=["key", "value_left"],
    rows=[
      [1,  2],
      [N,  3],
      [2,  4],
    ])
  tbl_right = TestQueryTable(
    names=["key", "value_right"],
    rows=[
      [N,  -3],
      [1,  -2],
      [2,  -4],
    ])
  q = """SELECT key, value_left+value_right AS x
  FROM tbl_left INNER JOIN tbl_right USING (key)"""
  qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
  assert _execute_qt(qt) == TestQueryTable(
    names=["key", "x"],
    rows=[
      [1, 0],
      [2, 0],
    ])

def test_multiple_nulls():
  tbl_left = TestQueryTable(
    names=["key1", "key2", "value_left"],
    rows=[
      [1,  N,  2],
      [N,  1,  3],
      [2,  3,  4],
    ])
  tbl_right = TestQueryTable(
    names=["key1", "key2", "value_right"],
    rows=[
      [N,  N,  -3],
      [1,  N,  -2],
      [2,  3,  -4],
    ])
  q = """SELECT key1, key2, value_left+value_right AS x
  FROM tbl_left INNER JOIN tbl_right USING (key1, key2)"""
  qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
  assert _execute_qt(qt) == TestQueryTable(
    names=["key1", "key2", "x"],
    rows=[
      [2, 3, 0],
    ])

def test_null_allowed_multi():
  tbl_left = TestQueryTable(
    names=["key1", "key2", "value_left"],
    rows=[
      [1,  0,  9],
      [1,  N,  2],
      [N,  1,  3],
      [2,  3,  4],
    ])
  tbl_right = TestQueryTable(
    names=["key1", "key2", "value_right"],
    rows=[
      [N,  N,  -3],
      [1,  N,  -2],
      [2,  3,  -4],
    ])
  q = """SELECT tbl_left.key1, tbl_left.key2, value_left+value_right AS x
  FROM tbl_left INNER JOIN tbl_right ON (
    tbl_left.key1 = tbl_right.key1 AND
    tbl_left.key2 IS tbl_right.key2)
  """
  qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
  assert _execute_qt(qt) == TestQueryTable(
    names=["tbl_left.key1", "tbl_left.key2", "x"],
    rows=[
      [1, N, 0],
      [2, 3, 0],
    ])

def test_null_allowed_single():
  tbl_left = TestQueryTable(
    names=["key1", "value_left"],
    rows=[
      [1,  2],
      [N,  3],
      [2,  4],
    ])
  tbl_right = TestQueryTable(
    names=["key1", "value_right"],
    rows=[
      [1,  -2],
      [N,  -3],
      [2,  -4],
    ])
  q = """SELECT tbl_left.key1, value_left+value_right AS x
  FROM tbl_left INNER JOIN tbl_right
  ON tbl_left.key1 IS tbl_right.key1
  """
  qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
  assert _execute_qt(qt) == TestQueryTable(
    names=["tbl_left.key1", "x"],
    rows=[
      [1, 0],
      [N, 0],
      [2, 0],
    ])

@parameterize_dwim(
  left_val=["E", "N", "2"],
  left_schema_name=ALL_SCHEMAS,
  right_val=["E", "N", "2"],
  right_schema_name=ALL_SCHEMAS,
)
def test_join_dtype(left_val, left_schema_name, right_val, right_schema_name):
  tbl_left = get_example_tbl(left_schema_name, left_val)
  ls = tbl_left["vl"].schema
  tbl_right = get_example_tbl(right_schema_name, right_val)
  rs = tbl_right["vl"].schema

  # pylint: disable=redefined-variable-type

  should_throw = False
  if not (ls.is_null or rs.is_null):
    if (not should_throw and ls.is_string != rs.is_string):
      should_throw = "string-mismatch"
    if (not should_throw and ls.domain != rs.domain):
      should_throw = "domain-mismatch"
    if (not should_throw and ls.unit != rs.unit):
      should_throw = "unit-mismatch"
    if (not should_throw and
        ls.dtype.kind != rs.dtype.kind and
        not (ls.dtype.kind in "iu" and rs.dtype.kind in "iu")):
      should_throw = "kind-mismatch"

  q = "SELECT vl FROM tbl_left INNER JOIN tbl_right USING (vl)"
  try:
    qt = ps2qt(q, tbl_left=tbl_left, tbl_right=tbl_right)
    result = _execute_qt(qt)["vl"]
  except InvalidQueryException:
    if not should_throw:
      raise
    return
  assert not should_throw

  if ls.is_null or rs.is_null:
    should_have_result = False
  elif left_val == "N" or right_val == "N":
    should_have_result = False
  else:
    should_have_result = (left_val == right_val)
  assert bool(len(result)) == should_have_result
