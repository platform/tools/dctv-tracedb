# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test pivot table internals"""

# pylint: disable=missing-docstring

import logging
from weakref import ref as weak_ref
from contextlib import contextmanager
from concurrent.futures import CancelledError
import pytest
import numpy as np

from modernmp.util import (
  the,
)

from .pivot_engine import (
  AsyncQueryEngine,
  AsynchronousPivotModel,
  ErrorResult,
  InvalidPivotModelOperation,
  LOADING,
  PivotModel,
  PivotQueryEngine,
  Placeholder,
  PlaceholderPivotNode,
  QueryEngine,
  QueryEngineDiedException,
  QueryPage,
  QueryPivotNode,
  RootPivotNode,
  SqlFromPart,
  StaleNodeError,
)

from .sql_util import SqlBundle

from .util import (
  ChainableFuture,
  future_exception,
  future_success_p,
  override,
)

log = logging.getLogger(__name__)

@contextmanager
def _qerror(msg=None, *, cls=InvalidPivotModelOperation):
  with pytest.raises(cls) as ex:
    yield ex
  if msg:
    assert msg in str(ex)

def _pn(value, number_rows=1):
  return PlaceholderPivotNode(value=value, number_rows=number_rows)

def _do_test_pivot_model(pm):
  change_count = 0
  expected_change_count = 0
  def _on_change():
    nonlocal change_count
    change_count += 1
  pm.set_on_change_callback(_on_change)
  assert isinstance(pm.root, RootPivotNode)
  assert list(pm.generate_subtree(pm.root)) == [pm.root]
  node1 = _pn(1)
  assert not pm.has_node_p(node1)
  with _qerror("insertion index out of bounds"):
    pm.insert_node(node1, pm.root, 10)
  assert change_count == expected_change_count
  inserted = pm.insert_node(node1, pm.root, 0)
  assert inserted is node1
  expected_change_count += 1
  assert change_count == expected_change_count

  assert pm.has_node_p(node1)
  assert list(pm.get_children(node1)) == []
  assert list(pm.get_children(pm.root)) == [node1]
  assert pm.get_parent(node1) is pm.root
  assert pm.get_index(node1) == 0  # pylint: disable=compare-to-zero
  with _qerror("node already in tree"):
    pm.insert_node(node1, pm.root, 0)
  node2 = _pn(2, number_rows=10)
  node3 = _pn(3)
  node4 = _pn(4)
  with _qerror("parent not already in tree"):
    pm.insert_node(node3, node2, 0)

  assert not pm.has_node_p(node2)
  pm.insert_node(node2, node1, 1)
  assert pm.has_node_p(node2)
  expected_change_count += 1
  assert change_count == expected_change_count

  pm.remove_node(node2)
  expected_change_count += 1
  assert change_count == expected_change_count
  assert not pm.has_node_p(node2)
  pm.insert_node(node2, node1, 1)
  expected_change_count += 1

  pm.insert_node(node4, node2, 7)
  assert pm.has_node_p(node4)
  expected_change_count += 1
  assert change_count == expected_change_count
  removed = pm.maybe_remove_child_at(node2, 7)
  assert removed
  expected_change_count += 1
  assert change_count == expected_change_count
  assert not pm.has_node_p(node4)

  removed = pm.maybe_remove_child_at(node2, 7)
  assert not removed

  pm.insert_node(node3, node2, 5)
  expected_change_count += 1
  assert change_count == expected_change_count
  assert list(pm.get_children_full(node2)) == [(5, node3)]
  assert pm.maybe_get_child_at(node2, 5) is node3
  assert pm.maybe_get_child_at(node2, 4) is None
  assert pm.maybe_get_child_at(node2, 3423412) is None

  pm.insert_node(node4, node1, 1)
  expected_change_count += 1
  assert change_count == expected_change_count

  assert not pm.has_node_p(node2)
  assert not pm.has_node_p(node3)

  with _qerror("node not in tree"):
    list(pm.get_children_full(node2))

  with pm:
    pm.insert_node(node2, node1, 0)
    pm.insert_node(node3, node1, 1)
    assert change_count == expected_change_count
  expected_change_count += 1
  assert change_count == expected_change_count

def test_pivot_model_basic():
  _do_test_pivot_model(PivotModel())

def test_pivot_model_async():
  _do_test_pivot_model(AsynchronousPivotModel())

def _defer_forever(pm):
  pm.defer_insert(parent_node=ChainableFuture(),
                  index=ChainableFuture(),
                  pivot_node_type=PlaceholderPivotNode)

def test_pivot_model_async_pending():
  pm = AsynchronousPivotModel()
  _defer_forever(pm)
  assert pm.number_pending_inserts == 1
  _do_test_pivot_model(pm)

def test_pivot_model_async_defer_immediate():
  pm = AsynchronousPivotModel()
  future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=ChainableFuture.of(4))
  assert future.done()
  node = future.result()
  assert pm.has_node_p(node)
  assert list(pm.get_children(pm.root)) == [node]

def test_pivot_model_async_defer_simple():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  assert not future.done()
  assert list(pm.get_children(pm.root)) == []
  value_future.set_result(5)
  assert future.done()
  node = future.result()
  assert list(pm.get_children(pm.root)) == [node]

def test_pivot_model_async_defer_simple_multiple():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  assert not future.done()
  assert list(pm.get_children(pm.root)) == []
  value_future.set_result(5)
  assert future.done()
  node = future.result()
  assert list(pm.get_children(pm.root)) == [node]
  value2_future = ChainableFuture()
  future2 = pm.defer_insert(
    parent_node=ChainableFuture.of(node),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not future2.done()
  assert list(pm.get_children(node)) == []
  value2_future.set_result(5)
  assert future2.done()
  node2 = future2.result()
  assert list(pm.get_children(node)) == [node2]

def test_pivot_model_async_defer_plug():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  with pm:
    value_future.set_result(5)
    assert not future.done()
    assert list(pm.get_children(pm.root)) == []
  assert future.done()
  node = future.result()
  assert list(pm.get_children(pm.root)) == [node]

def test_pivot_model_async_defer_explicit_cancel():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  value_future.set_running_or_notify_cancel()  # Inhibit cancel
  node_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  assert not node_future.done()
  assert list(pm.get_children(pm.root)) == []
  node_future.cancel()
  assert value_future.running()
  value_future.set_result(5)
  assert not value_future.running()
  assert list(pm.get_children(pm.root)) == []

def test_pivot_model_async_defer_error():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  row_count_future = ChainableFuture()
  node_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future,
    row_count=row_count_future)
  assert not node_future.done()
  assert list(pm.get_children(pm.root)) == []
  ex = ValueError("foo")
  value_future.set_exception(ex)
  assert node_future.done()
  assert node_future.exception() is ex
  assert list(pm.get_children(pm.root)) == []
  assert row_count_future.cancelled()

def test_pivot_model_async_defer_chained():
  change_count = 0
  expected_change_count = 0
  def _on_change():
    nonlocal change_count
    change_count += 1
  pm = AsynchronousPivotModel()
  pm.set_on_change_callback(_on_change)
  value_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  node2_future = pm.defer_insert(
    parent_node=node1_future,
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=ChainableFuture.of(5))
  assert list(pm.get_children(pm.root)) == []
  assert not node1_future.done()
  assert not node2_future.done()
  assert change_count == expected_change_count
  value_future.set_result(9)
  assert node1_future.done()
  assert node2_future.done()
  node1 = node1_future.result()
  node2 = node2_future.result()
  assert list(pm.get_children(pm.root)) == [node1]
  assert list(pm.get_children(node1)) == [node2]

  expected_change_count += 1
  assert change_count == expected_change_count

def test_pivot_model_async_abort_all():
  pm = AsynchronousPivotModel()
  value_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  node2_future = pm.defer_insert(
    parent_node=node1_future,
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=ChainableFuture.of(5))
  assert list(pm.get_children(pm.root)) == []
  assert not node1_future.done()
  assert not node2_future.done()
  pm.abort_all()
  assert node1_future.done()
  assert isinstance(future_exception(node1_future),
                    (InvalidPivotModelOperation, CancelledError))
  assert node2_future.done()
  assert isinstance(future_exception(node2_future),
                    (InvalidPivotModelOperation, CancelledError))

def test_pivot_model_async_defer_chained_error():
  change_count = 0
  expected_change_count = 0
  def _on_change():
    nonlocal change_count
    change_count += 1
  pm = AsynchronousPivotModel()
  pm.set_on_change_callback(_on_change)
  value_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(10),
    pivot_node_type=PlaceholderPivotNode,
    value=value_future)
  node2_value_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=node1_future,
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=node2_value_future)
  assert list(pm.get_children(pm.root)) == []
  assert not node1_future.done()
  assert not node2_future.done()
  assert change_count == expected_change_count
  value_future.set_result(9)
  assert node1_future.done()
  assert node2_future.done()
  assert isinstance(node1_future.exception(), InvalidPivotModelOperation)
  assert node2_future.exception()
  assert isinstance(node2_future.exception(), InvalidPivotModelOperation)
  assert node2_value_future.cancelled()
  assert change_count == expected_change_count
  assert list(pm.get_children(pm.root)) == []

def test_pivot_model_async_defer_race_win():
  pm = AsynchronousPivotModel()
  value1_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value1_future)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not node1_future.done()
  assert not node2_future.done()
  value1_future.set_result(4)
  node1 = node1_future.result()
  assert list(pm.get_children(pm.root)) == [node1]
  assert not node2_future.done()
  value2_future.set_result(5)
  assert node2_future.done()
  node2 = node2_future.result()
  assert list(pm.get_children(pm.root)) == [node2]

def test_pivot_model_async_defer_race_lose():
  pm = AsynchronousPivotModel()
  pm.auto_cancel = False
  value1_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value1_future)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not node1_future.done()
  assert not node2_future.done()
  value2_future.set_result(5)
  node2 = node2_future.result()
  assert list(pm.get_children(pm.root)) == [node2]
  assert not node1_future.done()
  value1_future.set_result(4)
  assert node1_future.done()
  assert isinstance(node1_future.exception(), StaleNodeError)
  assert list(pm.get_children(pm.root)) == [node2]

def test_pivot_model_async_auto_cancel_insert_conflict():
  pm = AsynchronousPivotModel()
  assert pm.auto_cancel
  value1_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value1_future)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not node1_future.done()
  assert not node2_future.done()
  assert not value1_future.done()
  value2_future.set_result(5)
  assert node2_future.done()
  node2 = node2_future.result()
  assert list(pm.get_children(pm.root)) == [node2]
  assert node1_future.done()
  assert isinstance(node1_future.exception(), StaleNodeError)
  assert value1_future.cancelled()
  assert list(pm.get_children(pm.root)) == [node2]

def test_pivot_model_async_auto_cancel_insert_conflict_delayed():
  pm = AsynchronousPivotModel()
  assert pm.auto_cancel
  value1_future = ChainableFuture()
  index1_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=index1_future,
    pivot_node_type=PlaceholderPivotNode,
    value=value1_future)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not node1_future.done()
  assert not node2_future.done()
  assert not value1_future.done()
  value2_future.set_result(5)
  assert node2_future.done()
  node2 = node2_future.result()
  assert list(pm.get_children(pm.root)) == [node2]
  assert not node1_future.done()
  index1_future.set_result(0)  # Now conflict is observable
  assert node1_future.done()
  assert isinstance(node1_future.exception(), StaleNodeError)
  assert value1_future.cancelled()
  assert list(pm.get_children(pm.root)) == [node2]

def test_pivot_model_async_auto_cancel_delete_conflict():
  pm = AsynchronousPivotModel()
  assert pm.auto_cancel
  node1 = pm.insert_node(_pn(2), pm.root, 0)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(node1),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  assert not node2_future.done()
  assert not value2_future.done()
  pm.remove_node(node1)
  assert node2_future.done()
  assert isinstance(node2_future.exception(), StaleNodeError)
  assert value2_future.cancelled()

def test_pivot_model_async_defer_race_before_delete():
  pm = AsynchronousPivotModel()
  _defer_forever(pm)
  node1 = pm.insert_node(_pn(1), pm.root, 0)
  node2 = pm.insert_node(_pn(2), node1, 0)
  value3_future = ChainableFuture()
  node3_future = pm.defer_insert(parent_node=ChainableFuture.of(node2),
                                 index=ChainableFuture.of(0),
                                 pivot_node_type=PlaceholderPivotNode,
                                 value=value3_future)
  assert not node3_future.done()
  value3_future.set_result(9)
  assert node3_future.result()

def test_pivot_model_async_defer_race_after_delete():
  pm = AsynchronousPivotModel()
  pm.auto_cancel = False
  _defer_forever(pm)
  node1 = pm.insert_node(_pn(1), pm.root, 0)
  node2 = pm.insert_node(_pn(2), node1, 0)
  value3_future = ChainableFuture()
  node3_future = pm.defer_insert(parent_node=ChainableFuture.of(node2),
                                 index=ChainableFuture.of(0),
                                 pivot_node_type=PlaceholderPivotNode,
                                 value=value3_future)
  assert not node3_future.done()
  pm.remove_node(node2)
  value3_future.set_result(9)
  assert node3_future.done()
  assert isinstance(node3_future.exception(), StaleNodeError)

def test_pivot_model_async_defer_race_after_reinsert():
  pm = AsynchronousPivotModel()
  _defer_forever(pm)
  node1 = pm.insert_node(_pn(1), pm.root, 0)
  node2 = pm.insert_node(_pn(2), node1, 0)
  value3_future = ChainableFuture()
  node3_future = pm.defer_insert(parent_node=ChainableFuture.of(node2),
                                 index=ChainableFuture.of(0),
                                 pivot_node_type=PlaceholderPivotNode,
                                 value=value3_future)
  assert not node3_future.done()
  pm.remove_node(node2)
  pm.insert_node(node2, node1, 0)
  value3_future.set_result(9)
  assert node3_future.done()
  assert isinstance(node3_future.exception(), StaleNodeError)

def test_pivot_model_async_defer_race_after_remove_child_at():
  pm = AsynchronousPivotModel()
  pm.auto_cancel = False
  _defer_forever(pm)
  node1 = pm.insert_node(_pn(1), pm.root, 0)
  node2 = pm.insert_node(_pn(2), node1, 0)
  value3_future = ChainableFuture()
  node3_future = pm.defer_insert(parent_node=ChainableFuture.of(node2),
                                 index=ChainableFuture.of(1),
                                 pivot_node_type=PlaceholderPivotNode,
                                 value=value3_future)
  assert not node3_future.done()
  removed = pm.maybe_remove_child_at(node2, 1)
  assert not removed
  assert not node3_future.done()
  value3_future.set_result(9)
  assert node3_future.done()
  assert isinstance(node3_future.exception(), StaleNodeError)

def test_pivot_model_async_defer_auto_cancel_after_remove_child_at():
  pm = AsynchronousPivotModel()
  _defer_forever(pm)
  node1 = pm.insert_node(_pn(1), pm.root, 0)
  node2 = pm.insert_node(_pn(2), node1, 0)
  value3_future = ChainableFuture()
  node3_future = pm.defer_insert(parent_node=ChainableFuture.of(node2),
                                 index=ChainableFuture.of(1),
                                 pivot_node_type=PlaceholderPivotNode,
                                 value=value3_future)
  assert not node3_future.done()
  removed = pm.maybe_remove_child_at(node2, 1)
  assert not removed
  assert node3_future.done()
  assert isinstance(node3_future.exception(), StaleNodeError)
  assert value3_future.cancelled()

def test_pivot_model_async_insert_failure_race():
  pm = AsynchronousPivotModel()
  value1_future = ChainableFuture()
  node1_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value1_future)
  value2_future = ChainableFuture()
  node2_future = pm.defer_insert(
    parent_node=ChainableFuture.of(pm.root),
    index=ChainableFuture.of(0),
    pivot_node_type=PlaceholderPivotNode,
    value=value2_future)
  def _on_node2_done(future):
    assert future is node2_future
    if future_exception(future):
      value1_future.set_result("blarg")
  node2_future.add_done_callback(_on_node2_done)
  value2_future.set_exception(ValueError("test error"))
  assert future_success_p(node1_future)

@pytest.mark.parametrize(
  "mode",
  ["conflict_auto_cancel", "conflict", "normal"])
def test_pivot_model_async_subtree(mode):
  pm = AsynchronousPivotModel()

  if mode == "conflict_auto_cancel":
    assert pm.auto_cancel
  else:
    pm.auto_cancel = False

  _defer_forever(pm)
  node_0 = pm.insert_node(_pn("0"), pm.root, 0)
  node_0_0 = pm.insert_node(_pn("0_0"), node_0, 0)
  node_0_0_1 = pm.insert_node(_pn("0_0_1"), node_0_0, 1)
  value_future = ChainableFuture()
  node_future = pm.defer_insert(parent_node=ChainableFuture.of(node_0),
                                index=ChainableFuture.of(0),
                                pivot_node_type=PlaceholderPivotNode,
                                value=value_future)
  assert not value_future.done()
  assert not node_future.done()

  if mode in ("conflict_auto_cancel", "conflict"):
    pm.insert_node(_pn("0_0_1_0"), node_0_0_1, 0)
    if mode == "conflict":
      assert not value_future.done()
      assert not node_future.done()
      value_future.set_result(-1)
    else:
      assert value_future.cancelled()
    assert pm.has_node_p(node_0_0_1)
    assert node_future.done()
    assert isinstance(node_future.exception(), StaleNodeError)
  else:
    assert mode == "normal"
    value_future.set_result(-1)
    assert node_future.done()
    node_0_0_v2 = node_future.result()
    assert list(pm.get_children_full(pm.root)) == [(0, node_0)]
    assert list(pm.get_children_full(node_0)) == [(0, node_0_0_v2)]
    assert not pm.has_node_p(node_0_0_1)

def _unpaginate_query(query_bundle):
  # Ugly hack to implement pagination in test code
  pagination_tail = " LIMIT :qb_page_size OFFSET :qb_offset"
  pagination = None
  if query_bundle.sql.endswith(pagination_tail):
    pagination = (the(int, query_bundle.args["qb_page_size"]),
                  the(int, query_bundle.args["qb_offset"]))
    query_bundle = SqlBundle.format(
      query_bundle.sql[:-len(pagination_tail)],
      **{k: v for k, v in query_bundle.args.items()
         if k not in ("qb_page_size", "qb_offset")})
  return query_bundle, pagination

class TestQueryEngine(QueryEngine):
  """Simple test-only query engine"""

  @override
  def __init__(self):
    super().__init__()
    self.__queries = {}

  def add(self, query_bundle, result):
    """Pre-set or re-set a query"""
    if isinstance(result, np.ndarray):
      result = ChainableFuture.of(result)
    elif not isinstance(result, ChainableFuture):
      result = ChainableFuture.of(np.array(result))
    assert isinstance(result, ChainableFuture)
    self.__queries[query_bundle] = result
    return result

  def forget(self, query_bundle):
    """Remove stored information about a query"""
    return self.__queries.pop(query_bundle, None)

  @override
  def query_async(self, query_bundle):
    assert isinstance(query_bundle, SqlBundle)
    query_bundle, pagination = _unpaginate_query(query_bundle)
    result = self.__queries.get(query_bundle)
    if result is None:
      future = ChainableFuture()
      future.set_exception(
        KeyError("unknown query {!r}".format(query_bundle)))
      return future
    assert isinstance(result, ChainableFuture)
    if pagination:
      def _do_pagination(array):
        limit, offset = pagination
        return array[offset : offset + limit]
      result = result.then(_do_pagination)
    return result



class _Dummy:
  def __getattr__(self, key):
    raise AttributeError(key)  # Make pylint shut up

def _set_up_pivot_query_engine(*, test_mapping=False):
  query_engine = TestQueryEngine()
  ca = SqlBundle.of("[COL]")
  pivot_engine = PivotQueryEngine(query_engine=query_engine,
                                  page_size=2)
  assert pivot_engine.total_display_rows == 0  # pylint: disable=compare-to-zero
  pm = AsynchronousPivotModel()
  query_root = pm.insert_node(
    QueryPivotNode(qm=SqlFromPart("[BASE]"),
                   number_rows=5,
                   number_restrict=0),
    pm.root, 0)
  root_ca_query = query_root.qm.make_query_for_ca(ca)
  query_engine.add(root_ca_query,
                   (1 + np.arange(query_root.number_rows))*10)

  pivot_engine.model = pm

  if test_mapping:
    assert pivot_engine.total_display_rows == query_root.number_rows
    node_map = {nr: pivot_engine.map_display_row(nr)
                for nr in range(pivot_engine.total_display_rows)}
    assert node_map == {
      0: (query_root, 0),
      1: (query_root, 1),
      2: (query_root, 2),
      3: (query_root, 3),
      4: (query_root, 4),
    }

  query_child = pm.insert_node(
    QueryPivotNode(qm=query_root.qm.evolve(group_by="[GROUPER]"),
                   number_rows=2,
                   number_restrict=1),
    query_root, 1)

  assert pivot_engine.total_display_rows == \
    query_root.number_rows + query_child.number_rows

  child_ca_query = query_child.qm.make_query_for_ca(ca)
  query_engine.add(child_ca_query,
                   (1 + np.arange(query_root.number_rows))*3)

  if test_mapping:
    node_map = {nr: pivot_engine.map_display_row(nr)
                for nr in range(pivot_engine.total_display_rows)}
    assert node_map == {
      0: (query_root, 0),
      1: (query_child, 0),
      2: (query_child, 1),
      3: (query_root, 1),
      4: (query_root, 2),
      5: (query_root, 3),
      6: (query_root, 4),
    }

  info = _Dummy()
  info.__dict__.update(locals())
  return info

def test_pivot_query_engine_basic():
  _set_up_pivot_query_engine(test_mapping=True)

def _all_rows(pivot_engine, ca):
  return [pivot_engine.lookup_display_row_value(nr, ca)
          for nr in range(pivot_engine.total_display_rows)]

def test_pivot_query_engine_display_query():
  info = _set_up_pivot_query_engine()
  pivot_engine = info.pivot_engine
  ca = info.ca
  with pytest.raises(KeyError):
    pivot_engine.lookup_display_row_value(452, ca)
  assert _all_rows(pivot_engine, ca) == [10, 3, 6, 20, 30, 40, 50]

def test_pivot_query_engine_pending():
  info = _set_up_pivot_query_engine()
  pivot_engine = info.pivot_engine
  query_engine = info.query_engine
  query_child = info.query_child
  child_ca_query = info.child_ca_query
  query_engine.forget(child_ca_query)
  child_ca_query_future = \
    query_engine.add(child_ca_query, ChainableFuture())
  ca = info.ca
  assert isinstance(LOADING, Placeholder)
  assert _all_rows(pivot_engine, ca) == [10, LOADING, LOADING, 20, 30, 40, 50]
  child_ca_query_future.set_result(
    2*(1+np.arange(query_child.number_rows))
  )
  assert _all_rows(pivot_engine, ca) == [10, 2, 4, 20, 30, 40, 50]

def test_pivot_query_engine_error():
  info = _set_up_pivot_query_engine()
  pivot_engine = info.pivot_engine
  query_engine = info.query_engine
  root_ca_query = info.root_ca_query
  query_engine.forget(root_ca_query)
  future = \
    query_engine.add(root_ca_query, ChainableFuture())
  err = ValueError("blarg")
  future.set_exception(err)
  err_result = pivot_engine.lookup_display_row_value(0, info.ca)
  assert isinstance(err_result, Placeholder)
  assert isinstance(err_result, ErrorResult)
  assert str(err_result) == "wtf"
  assert err_result.exception is err

def _test_pivot_engine_keep(kept_pages):
  info = _set_up_pivot_query_engine()
  pivot_engine = info.pivot_engine
  query_engine = info.query_engine
  root_ca_query = info.root_ca_query
  child_ca_query = info.child_ca_query
  ca = info.ca
  assert _all_rows(pivot_engine, ca) == [10, 3, 6, 20, 30, 40, 50]
  str(pivot_engine)  # Just to make sure it keeps working
  query_engine.add(root_ca_query, ChainableFuture())
  query_engine.add(child_ca_query, ChainableFuture())
  pivot_engine.keep_display_pages(kept_pages)
  return _all_rows(pivot_engine, ca)

def test_pivot_query_engine_display_page_cache():
  # pylint: disable=bad-whitespace
  L = LOADING
  assert _test_pivot_engine_keep([0]) == [10,  3,  6, 20,  L,  L,  L]
  assert _test_pivot_engine_keep([1]) == [10,  3,  6, 20,  L,  L,  L]
  assert _test_pivot_engine_keep([2]) == [ L,  L,  L,  L, 30, 40,  L]
  assert _test_pivot_engine_keep([3]) == [ L,  L,  L,  L,  L,  L, 50]
  assert _test_pivot_engine_keep([4]) == [ L,  L,  L,  L,  L,  L,  L]
  assert _test_pivot_engine_keep([ ]) == [ L,  L,  L,  L,  L,  L,  L]

def test_pivot_engine_page_drops_page_references():
  info = _set_up_pivot_query_engine()
  pivot_engine = info.pivot_engine
  root_ca_query = info.root_ca_query
  ca = info.ca
  assert _all_rows(pivot_engine, ca) == [10, 3, 6, 20, 30, 40, 50]
  query_page = pivot_engine.debug_lookup_query_page(root_ca_query, 0, 0)
  assert isinstance(query_page, QueryPage)
  assert query_page[0] == 10
  query_page_wr = weak_ref(query_page)
  del query_page
  assert query_page_wr()
  pivot_engine.keep_display_pages([])
  assert not query_page_wr()

@pytest.mark.parametrize("op", [
  "batch_completion_drives_reissue",
  "batch_failure",
  "distinct_futures",
  "expedite",
  "normal",
  "close",
])
def test_async_query_engine(op):
  fake_timeout_future = None
  pending_batches = []
  def _fake_make_timeout_future(_timeout):
    nonlocal fake_timeout_future
    assert fake_timeout_future
    future = fake_timeout_future
    fake_timeout_future = None
    return future
  def _fake_async_batch_query(query_bundles):
    batch_future = ChainableFuture()
    raw_query_futures = []
    for query_bundle in query_bundles:
      raw_query_future = ChainableFuture()
      raw_query_future.bundle = query_bundle
      raw_query_futures.append(raw_query_future)
    batch = batch_future, raw_query_futures
    pending_batches.append(batch)
    return batch
  async_qe = AsyncQueryEngine(
    _fake_async_batch_query,
    make_timeout_future=_fake_make_timeout_future)
  fake_timeout_future = timeout1_future = ChainableFuture()
  q1_future = async_qe.query_async(SqlBundle.of("Q1"))
  assert not fake_timeout_future
  q2_future = async_qe.query_async(SqlBundle.of("Q2"))
  assert not q1_future.done()
  assert not q2_future.done()
  timeout1_future.set_result(None)
  assert len(pending_batches) == 1
  batch_future, raw_query_futures = pending_batches.pop()
  raw_query_futures = list(sorted(raw_query_futures,
                                  key=lambda future: future.bundle))
  assert raw_query_futures[0].bundle.sql == "Q1"
  assert raw_query_futures[1].bundle.sql == "Q2"
  if op == "normal":
    arr = np.array([1, 2, 3])
    raw_query_futures[0].set_result(arr)
    assert q1_future.done()
    assert q1_future.result() is arr
    raw_query_futures[1].set_result(arr)
    assert q2_future.done()
    assert q2_future.result() is arr
    batch_future.set_result(None)
  elif op == "batch_failure":
    ex = ValueError("foo")
    batch_future.set_exception(ex)
    assert raw_query_futures[0].cancelled()
    assert raw_query_futures[1].cancelled()
    assert q1_future.done()
    assert q1_future.exception() is ex
    assert q2_future.done()
    assert q2_future.exception() is ex
  elif op == "batch_completion_drives_reissue":
    fake_timeout_future = timeout2_future = ChainableFuture()
    q3_future = async_qe.query_async(SqlBundle.of("Q2"))
    assert not pending_batches
    assert not fake_timeout_future
    batch_future.set_exception(ValueError("blah"))
    assert pending_batches
    assert not timeout2_future.cancelled()
  elif op == "expedite":
    fake_timeout_future = timeout2_future = ChainableFuture()
    q3_future = async_qe.query_async(SqlBundle.of("Q2"))
    assert not pending_batches
    assert not fake_timeout_future
    async_qe.expedite()
    assert pending_batches
    assert timeout2_future.cancelled()
  elif op == "distinct_futures":
    fake_timeout_future = timeout2_future = ChainableFuture()
    q3_future = async_qe.query_async(SqlBundle.of("BLARG"))
    q4_future = async_qe.query_async(SqlBundle.of("BLARG"))
    assert q3_future is not q4_future
    # TODO(dancol): when we get around to letting users cancel query
    # futures, make sure that callers can cancel them independently.
  elif op == "close":
    fake_timeout_future = timeout2_future = ChainableFuture()
    q3_future = async_qe.query_async(SqlBundle.of("Q2"))
    async_qe.close()
    async_qe.close()  # Make sure it's idempotent
    assert q1_future.done()
    assert isinstance(q1_future.exception(),
                      QueryEngineDiedException)
    assert raw_query_futures[0].cancelled()
    assert q2_future.done()
    assert isinstance(q2_future.exception(),
                      QueryEngineDiedException)
    assert raw_query_futures[1].cancelled()
    assert q3_future.done()
    assert isinstance(q3_future.exception(),
                      QueryEngineDiedException)
    q4_future = async_qe.query_async(SqlBundle.of("Q4"))
    assert q4_future.done()
    assert isinstance(q4_future.exception(),
                      QueryEngineDiedException)
  else:
    assert False, "unknown test op " + op
