# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities for working with trace files"""
import mmap
import threading
import logging
from contextlib import contextmanager

import modernmp.reduction
from modernmp.util import once, ClosingContextManager

log = logging.getLogger(__name__)

class BadTraceFileError(ValueError):
  """Exception raised when a trace file is invalid"""

class ParseFailureError(RuntimeError):
  """Exception raised when parsing an event fails"""
  # N.B. C++ code refers to this class, so don't change its name
  # without updating _native.cpp.in.
  def __init__(self, offset, message):
    assert isinstance(offset, int)
    assert isinstance(message, str)
    super().__init__(message)
    self.offset = offset

  def __str__(self):
    # pylint: disable=unsubscriptable-object
    return "{} at offset={!r}".format(self.args[0], self.offset)

class MappedTrace(ClosingContextManager):
  """Holder for a trace file that we've mapped"""
  def __init__(self, trace_file_name):
    self.__file = open(trace_file_name, "rb")
    self.__mapping = mmap.mmap(
      self.__file.fileno(),
      0,
      flags=mmap.MAP_SHARED,
      prot=mmap.PROT_READ)

  @property
  def file(self):
    """The open file object"""
    return self.__file

  @property
  def mapping(self):
    """The mapping object"""
    return self.__mapping

  def _do_close(self):
    self.__mapping.close()
    self.__file.close()

  def __repr__(self):
    return "<MappedTrace file={!r} fileno={!r}>".format(
      self.__file,
      not self.__file.closed and self.__file.fileno())

  @contextmanager
  def augment_parse_errors(self):
    """Add file information to parse exceptions"""
    # pylint: disable=invalid-slice-index
    try:
      yield
    except ParseFailureError as ex:
      loc = ex.offset
      snippet = self.mapping[loc:loc+128]
      raise BadTraceFileError(
        "{} offset={} snippet={!r}".format(
          ex.args[0], loc, snippet)) from ex

class MappingCache(object):
  """Cache of a mapped trace"""
  def __init__(self):
    self.__lock = threading.Lock()
    self.__mapping = None

  def map(self, trace_file_name):
    """Open or retrieve a trace mapping"""
    with self.__lock:
      if self.__mapping and self.__mapping.file.name != trace_file_name:
        self.__mapping.close()
        self.__mapping = None
      if not self.__mapping:
        self.__mapping = MappedTrace(trace_file_name)
      return self.__mapping

  @staticmethod
  @once()
  def get():
    """Return the global MappingCache"""
    return MappingCache()

def _rebuild_mapped_trace(trace_file_name):
  try:
    return MappingCache.get().map(trace_file_name)
  except Exception:  # pylint: disable=broad-except
    log.warning("mapping file in child failed")
    return None  # Avoid process death

def _reduce_mapped_trace(mapped_trace):
  return _rebuild_mapped_trace, (mapped_trace.file.name,)

modernmp.reduction.register(MappedTrace, _reduce_mapped_trace)
