// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

npy_intp
GenericResultBuffer::prepare_to_add_nogil()
{
  npy_intp row_number = this->size;
  if (row_number == this->chunk_size)
    row_number = 0;
  if (row_number == 0)
    this->restart_nogil();
  // Callers must not fail after prepare_to_add returns successfully,
  // so we can update the size now.
  this->size = row_number + 1;
  return row_number;
}

pyarray_ref
GenericResultBuffer::get_array_nogil(npy_intp index) const noexcept
{
  assume(0 <= index && index < this->arrays.size());
  return this->arrays[index].notnull();
}

template<typename... Types>
ResultBuffer<Types...>::ResultBuffer(
    unique_pyref cb,
    obj_pyref<QueryExecution> qe)
    : GenericResultBuffer(
          std::move(cb),
          qe,
          make_dtype_vector<Types...>())
{}

template<typename... Types>
void
ResultBuffer<Types...>::reset_column_arrays_nogil() noexcept
{
  auto reset_column = [&](auto** column_data_ptr, npy_intp index) {
    using T = std::decay_t<decltype(**column_data_ptr)>;
    PyArrayObject* a = this->get_array_nogil(index).get();
    assume(npy_ndim(a) == 1);
    assume(npy_strides(a)[0] == sizeof (T));
    *column_data_ptr = npy_data<T>(a);
  };
  std::apply([&](auto&... array_pointers) {
    npy_intp index = 0;
    (..., reset_column(&array_pointers, index++));
  }, this->column_arrays);
}

template<typename... Types>
void
ResultBuffer<Types...>::add(Types... args)
{
  npy_intp row_number = this->prepare_to_add_nogil();
  if (row_number == 0)
    this->reset_column_arrays_nogil();
  auto arg_tuple = std::make_tuple(args...);
  std::apply([&](auto... indices) {
    (..., (std::get<indices>(column_arrays)[row_number] =
           std::get<indices>(arg_tuple)));
  }, index_tuple_t<sizeof...(Types)>());
}

}  // namespace dctv
