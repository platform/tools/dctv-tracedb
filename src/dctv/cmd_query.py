# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Command-line query interface

This interface is for batch operation, unlike the repl, which is for
interactive exploration.

The batch query system and the REPL use the same query language, of
course.
"""
import sys
import logging
import numpy as np
from numpy.ma import nomask
from pandas.io.json import dumps
from .model import TraceAnalysisSession, FileTraceContext
from .util import make_pd_dataframe
from ._native import npy_explode

# pylint: disable=compare-to-zero

log = logging.getLogger(__name__)

def _munge_for_pandas(array):
  array, mask = npy_explode(array)
  if mask is not nomask:
    array = np.asarray(array, dtype="O")  # Copies
    array[mask] = None
  # pylint: disable=unidiomatic-typecheck
  assert type(array) == np.ndarray
  return array

def _make_row_data_frames(trace_session, query):
  for chunk, _eof in trace_session.sql_query(query, output_format="rows"):
    df = make_pd_dataframe({
      column: _munge_for_pandas(array)
      for column, array in chunk.items()
    })
    if len(df):  # pylint: disable=len-as-condition
      yield df

def _do_json_records(out_file, trace_session, query):
  out_file.write("[\n")
  dfs = _make_row_data_frames(trace_session, query)
  df = next(dfs, None)
  while df is not None:
    next_df = next(dfs, None)
    is_eof = next_df is None
    assert len(df)  # pylint: disable=len-as-condition
    blob = df.to_json(lines=True, orient="records")
    blob = blob.replace("\n", ",\n")
    out_file.write(blob)
    if not is_eof:
      out_file.write(",")
    out_file.write("\n")
    df = next_df
  out_file.write("]\n")

def _do_json_columns(out_file, trace_session, query):
  # TODO(dancol): teach the query engine how to do columnwise *and*
  # chunked queries.
  qe = trace_session.qe
  st = trace_session.st
  qt = trace_session.parse_sql_query(query)
  cache = trace_session.st.make_lookup_cache(str)
  out_file.write("{\n")
  for colno, column in enumerate(qt.columns):
    if colno:
      out_file.write(",\n")
    out_file.write(dumps(column))
    out_file.write(": [")
    qn = qt[column]
    is_string = qn.schema.is_string
    for i, (rqn, array, is_eof) in enumerate(
        qe.execute((qn,))):
      assert qn is rqn
      if not len(array):  # pylint: disable=len-as-condition
        assert is_eof
        break
      if is_string:
        array, mask = npy_explode(array)
        array = st.vlookup(array, cache)
        if mask is not nomask:
          array[mask] = None
      else:
        array = _munge_for_pandas(array)
      if i:
        out_file.write(", ")
      out_file.write(dumps(array)[1:-1])
    out_file.write("]")
  out_file.write("\n}\n")

def _do_csv_rows(out_file, trace_session, query):
  for i, df in enumerate(_make_row_data_frames(trace_session, query)):
    df.to_csv(out_file, index=False, header=not i)

def _do_none(_out_file, trace_session, query):
  for _chunk, _eof in trace_session.sql_query(
      query, output_format="columns"):
    pass  # Do nothing:

_OUTPUT_FUNCTIONS = {
  "json_records": _do_json_records,
  "json_columns": _do_json_columns,
  "csv_rows": _do_csv_rows,
  "none": _do_none,
}

def run_query(*,
              query,
              trace_mounts,
              output_format,
              out_file=None,
              block_size=None,
              temp_hack_lenient_metadata=False,
              threads=False):
  """Run the query command"""
  out_file = out_file or sys.stdout
  if output_format not in _OUTPUT_FUNCTIONS:
    raise ValueError("output format not supported: {!r}"
                     .format(output_format))
  trace_session = TraceAnalysisSession(threads=threads)
  if block_size is not None:
    trace_session.reconfigure_cache(block_size=block_size)
  for path, trace_file_name in trace_mounts:
    trace_session.mount_trace(path,
                              FileTraceContext(trace_file_name),
                              temp_hack_lenient_metadata=
                              temp_hack_lenient_metadata)
  _OUTPUT_FUNCTIONS[output_format](out_file, trace_session, query)
  out_file.flush()
