# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test cases for the SQL-to-QueryNode layer"""

# pylint: disable=missing-docstring,redefined-outer-name,bad-whitespace

import logging
from textwrap import dedent
from contextlib import contextmanager

import pytest
import numpy as np

from modernmp.util import the

from .sql import (
  BetweenAndOperation,
  BinaryFunExprOperation,
  BinaryOperation,
  BindParameter,
  CollationTag,
  ColumnReference,
  Compound,
  CreateView,
  CteBinding,
  CteBindingName,
  DmlContext,
  Drop,
  ExpressionColumn,
  FilledQueryTable,
  FloatLiteral,
  FunExprDict,
  FunExprDictItem,
  FunExprKeywordArgument,
  FunExprList,
  FunctionCall,
  GroupByExpressions,
  GroupBySpanGrouper,
  GroupBySpanPartition,
  IntegerLiteral,
  InvalidQueryException,
  Join,
  KeywordArgument,
  MountTrace,
  NO_ARGS,
  Namespace,
  NullLiteral,
  OrderingTerm,
  RegularSelect,
  RenamingQueryTableExpression,
  RowValue,
  SelectDirect,
  SelectWithCte,
  SqlAttributeLookup,
  StringLiteral,
  TableFunctionCall,
  TableReference,
  TableSubquery,
  TableValuedFunction,
  TableValues,
  TvfContext,
  TvfDot,
  TvfFunctionCall,
  UnaryFunExprOperation,
  UnaryOperation,
  UnboundReferenceException,
  UnitConversion,
  WildcardColumn,
  _drop_duplicates,
  qargs,
)

from .sql_parser import (
  parse_dml,
  parse_expression,
  parse_select,
)

from .query import (
  BadCastException,
  BadUnitsException,
  DURATION_SCHEMA,
  EVENT_UNPARTITIONED_TIME_MAJOR,
  QueryNode,
  QuerySchema,
  QueryTable,
  SPAN_UNPARTITIONED_TIME_MAJOR,
  STRING_SCHEMA,
  TS_SCHEMA,
  TableKind,
  TableSchema,
  TableSorting,
  UNIQUE,
)

from .test_query import (
  TestQueryTable,
  _execute_qt,
  _make_rng,
)

from .queryengine import (
  QueryCache,
  QueryEngine,
)

from .test_util import (
  pict_parameterize,
)

from .util import (
  BOOL,
  INT16,
  INT32,
  INT64,
  INT8,
  NoneType,
  override,
  ureg,
)

from ._native import (
  SPAN_INVARIANT_CHECKING,
)

log = logging.getLogger(__name__)

def test_parse_ast_nodes():
  assert IntegerLiteral(1) == IntegerLiteral(1)
  assert IntegerLiteral(1) != IntegerLiteral(2)
  assert BinaryOperation(IntegerLiteral(1), "+", IntegerLiteral(2)) == \
    BinaryOperation(IntegerLiteral(1), "+", IntegerLiteral(2))
  assert BinaryOperation(IntegerLiteral(1), "+", IntegerLiteral(2)) != \
    BinaryOperation(IntegerLiteral(2), "+", IntegerLiteral(3))

pe = parse_expression
ps = parse_select
pdml = parse_dml

NP_TRUE = np.bool_(True)
NP_FALSE = np.bool_(False)

T = True
F = False
N = None

def ptbl(query):
  """Parse a SQL table expression: useful in parse tests"""
  q = "SELECT * FROM ({})".format(query)
  ast = ps(q)
  assert isinstance(ast, SelectDirect)
  ast = ast.core
  assert isinstance(ast, RegularSelect)
  return ast.from_

def ps2qt(query, _ns=None, _args=NO_ARGS, **env):  # pylint: disable=dangerous-default-value
  """Parse and compile a SELECT"""
  if _ns:
    assert not env
    tctx = TvfContext.from_ns(_ns, _args)
  else:
    tctx = TvfContext.from_ns(None, _args).let(env)
  return parse_select(query).make_qt(tctx)

def pse(expr, *, _args=NO_ARGS, **kwargs):  # pylint: disable=dangerous-default-value
  """Parse a SQL expression and evaluate it"""
  query = "SELECT ({}) AS value".format(expr)
  result, = _execute_qt(
    ps2qt(query, _args=_args, **kwargs))["value"]
  return result

def _set_name(value, name):
  return value.evolve(name=name)

@contextmanager
def _qerror(msg=None, *, cls=InvalidQueryException):
  if isinstance(msg, type) and \
     issubclass(msg, BaseException) and \
     cls is InvalidQueryException:
    cls = msg
    msg = None
  with pytest.raises(cls) as ex:
    yield ex
  if msg:
    assert msg in str(ex)

def _span_table_time_major(partition=None):
  return TableSchema(TableKind.SPAN, partition, TableSorting.TIME_MAJOR)

def _event_table_time_major(partition=None):
  return TableSchema(TableKind.EVENT, partition, TableSorting.TIME_MAJOR)



def test_ns():
  ns = Namespace()
  assert not ns
  ns["FOO"] = 5
  assert list(ns) == ["FOO"]
  assert ns["foo"] == 5
  assert ns["foO"] == 5
  ns["bar"] = Namespace()
  ns["bar"]["x"] = 1
  assert list(ns.walk()) == [
    ("FOO", 5),
    ("bar.x", 1),
  ]
  with pytest.raises(Namespace.KeyExistsError):
    ns.assign_by_path(["bar", "x"], 6)
  ns.assign_by_path(["bar", "x"], 6, overwrite=True)
  with pytest.raises(Namespace.NotNamespaceError):
    ns.assign_by_path(["bar", "x", "y"], 1)
  with pytest.raises(KeyError):
    ns.assign_by_path(["qux", "spam", "blarg"], 4)
  ns.assign_by_path(["qux", "spam", "blarg"], 4,
                    make_namespaces=True)
  assert list(ns.walk()) == [
    ("FOO", 5),
    ("bar.x", 6),
    ("qux.spam.blarg", 4),
  ]



def test_parse_expression_simple():
  # Integral integer is integral
  assert pe("5") == IntegerLiteral(5)

  # SQL doesn't do C-style backslashes.  Instead, doubling the
  # quotation character escapes the quotation character.
  assert pe("'foo'") == StringLiteral("foo")
  assert pe("'foo''bar'") == StringLiteral("foo'bar")
  assert pe("''") == StringLiteral("")
  assert pe("''''") == StringLiteral("'")
  assert pe("NULL") == NullLiteral()
  assert pe("null") == NullLiteral()
  assert pe("`select`") == ColumnReference("select")
  assert pe("`foo `` bar`") == ColumnReference("foo ` bar")
  assert pe("_foo") == ColumnReference("_foo")

def test_parse_expression_collate():
  assert pe("'foo' COLLATE nocase") == CollationTag(
    StringLiteral("foo"),
    "nocase")
  pe("'foo' COLLATE nocase < 'bar'") == BinaryOperation(
    CollationTag(StringLiteral("foo"), "nocase"),
    "<",
    StringLiteral("bar"))
  with _qerror():
    pe("'foo' COLLATE asdfaa")

def test_parse_integer_with_units():
  assert pe("5ms") == IntegerLiteral(5, unit="ms")
  assert pe("5`byte/second`") == IntegerLiteral(5, unit="byte/second")

def test_parse_float():
  assert pe("3.14") == FloatLiteral(3.14)

def test_parse_empty_identifier_invalid():
  with _qerror("Parse error"):
    pe("``")

def test_parse_identifier_non_reserved_kw():
  assert pe("replace") == ColumnReference("replace")
  assert pdml("DROP IF EXISTS foo;")[0] == \
    Drop(("foo",), ignore_absent=True)

def test_parse_function_call():
  assert pe("foo(1, 2)") == \
    FunctionCall(["foo"], [IntegerLiteral(1), IntegerLiteral(2)])
  assert pe("foo(1, 2,)") == \
    FunctionCall(["foo"], [IntegerLiteral(1), IntegerLiteral(2)])
  assert pe("foo()") == FunctionCall(["foo"])
  assert pe("foo(bar())") == FunctionCall(["foo"], [FunctionCall(["bar"])])
  assert pe("blarg.foo(4)") == \
    FunctionCall(["blarg", "foo"], [IntegerLiteral(4)])


def test_eval_expression_float():
  pe("3.14 + -1") == 2.14
  pe("3e-5 + -1") == 3e-5

def test_parse_complex_expr_arg():
  pe("foo(bar * qux, spam)")

def test_parse_keyword_arguments():
  assert pe("foo(1, 2, foo => 42)") == \
    FunctionCall(["foo"], [IntegerLiteral(1),
                           IntegerLiteral(2),
                           KeywordArgument("foo", IntegerLiteral(42))])

def test_parse_function_call_distinct():
  assert pe("foo(DISTINCT bar)") == FunctionCall(
    ["foo"],
    [ColumnReference("bar")],
    distinct=True)
  assert pe("foo(ALL bar)") == FunctionCall(
    ["foo"],
    [ColumnReference("bar")],
    distinct=False)
  assert pe("foo(ALL)") == FunctionCall(
    ["foo"],
    [],
    distinct=False)

def test_parse_count_star():
  assert pe("count(*)") == FunctionCall(["count"])

def test_parse_column_reference():
  assert pe("foo") == ColumnReference("foo")
  assert pe("foo.bar") == ColumnReference("bar", ["foo"])
  assert pe("qux.foo.bar") == ColumnReference("bar", ["qux", "foo"])
  assert pe("spam.qux.foo.bar") == ColumnReference("bar", ["spam", "qux", "foo"])

def test_parse_expression_tree():
  assert pe("1 + 2") == BinaryOperation(IntegerLiteral(1), "+", IntegerLiteral(2))
  assert pe("1 + 2 + 3") == BinaryOperation(
    BinaryOperation(IntegerLiteral(1), "+", IntegerLiteral(2)),
    "+",
    IntegerLiteral(3))
  assert pe("1 + 2 * 3") == BinaryOperation(
    IntegerLiteral(1),
    "+",
    BinaryOperation(IntegerLiteral(2), "*", IntegerLiteral(3)))
  assert pe("1 - 2 + 3") == BinaryOperation(
    BinaryOperation(IntegerLiteral(1), "-", IntegerLiteral(2)),
    "+",
    IntegerLiteral(3))
  assert pe("1 - (2 + 3)") == BinaryOperation(
    IntegerLiteral(1),
    "-",
    BinaryOperation(IntegerLiteral(2), "+", IntegerLiteral(3)))
  assert pe("-1") == UnaryOperation("-", IntegerLiteral(1))
  assert pe("+-1") == \
    UnaryOperation("+", UnaryOperation("-", IntegerLiteral(1)))
  assert pe("1+-2") == \
    BinaryOperation(IntegerLiteral(1), "+",
                    UnaryOperation("-", IntegerLiteral(2)))

  assert pe("(1)") == IntegerLiteral(1)

  assert pe("NoT 1") == UnaryOperation("not", IntegerLiteral(1))

  assert pe("1 IS NOT 2") == BinaryOperation(
    IntegerLiteral(1), "is not", IntegerLiteral(2))

  assert pe("NOT 1 IS NOT NOT 2") == BinaryOperation(
    UnaryOperation("not", IntegerLiteral(1)),
    "is not",
    UnaryOperation("not", IntegerLiteral(2)))

  assert pe("1 BETWEEN 2 AND 3") == BetweenAndOperation(
    IntegerLiteral(1),
    IntegerLiteral(2),
    IntegerLiteral(3))

  assert pe("1 NOT BETWEEN 2 AND 3") == UnaryOperation(
    "not",
    BetweenAndOperation(
      IntegerLiteral(1),
      IntegerLiteral(2),
      IntegerLiteral(3)))

def test_parse_chained_between():
  pe("1 BETWEEN 2 AND 3 BETWEEN 4 and 5") == BetweenAndOperation(
    BetweenAndOperation(IntegerLiteral(1),
                        IntegerLiteral(2),
                        IntegerLiteral(3)),
    IntegerLiteral(4),
    IntegerLiteral(5))

def test_parse_select_simple():
  assert ps("SELECT 4") == SelectDirect(
    core=RegularSelect(
      columns=[
        ExpressionColumn(IntegerLiteral(4))]))

  assert ps("SELECT 4 AS foo") == SelectDirect(
    core=RegularSelect(
      columns=[
        ExpressionColumn(IntegerLiteral(4), "foo")]))

  assert ps("SELECT 4, 5 AS foo") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4), None),
               ExpressionColumn(IntegerLiteral(5), "foo")]))

def test_parse_select_from_table():
  assert ps("SELECT abc, yyy.def FROM foo") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("abc")),
               ExpressionColumn(ColumnReference("def", ["yyy"]))],
      from_=TableReference(["foo"]))
  )

  assert ps("SELECT abc, yyy.def FROM foo") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("abc")),
               ExpressionColumn(ColumnReference("def", ["yyy"]))],
      from_=TableReference(["foo"]))
  )

  assert ps("SELECT blarg.zzz.abc, yyy.def FROM foo") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference(
        "abc", ["blarg", "zzz"])),
               ExpressionColumn(ColumnReference("def", ["yyy"]))],
      from_=TableReference(["foo"])))

  assert ps("SELECT 4 FROM foo.bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableReference(["foo", "bar"])))

def test_parse_select_from_table_function_simple():
  assert ps("SELECT 4 FROM foo('yyy', 4)") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          StringLiteral("yyy"),
          IntegerLiteral(4),
        ])))

def test_parse_select_from_table_function_nested_funcall():
  assert ps("SELECT 4 FROM foo(spam.bar(5), 'yyy', 4)") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          TvfFunctionCall(TvfDot(TableReference(["spam"]), "bar"),
                          arguments=[IntegerLiteral(5)]),
          StringLiteral("yyy"),
          IntegerLiteral(4),
        ])))

def test_parse_select_from_table_function_complex():
  assert ps("SELECT 4 FROM foo(kw => 4*5+-1)") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          FunExprKeywordArgument(
            "kw",
            BinaryFunExprOperation(
              BinaryFunExprOperation(IntegerLiteral(4),
                                     "*",
                                     IntegerLiteral(5)),
              "+",
              UnaryFunExprOperation("-", IntegerLiteral(1)))
            )])))

def test_parse_list_literal():
  assert ps("SELECT 4 FROM foo([1, 2, 3], [])") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          FunExprList([IntegerLiteral(1),
                       IntegerLiteral(2),
                       IntegerLiteral(3)]),
          FunExprList([])])))

def test_parse_dict_literal():
  assert ps("SELECT 4 FROM foo({foo => 1, 'bar': 5}, {})") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          FunExprDict([
            FunExprDictItem("foo", IntegerLiteral(1)),
            FunExprDictItem(StringLiteral("bar"), IntegerLiteral(5)),
          ]),
          FunExprDict([])])))

def test_parse_subquery_argument():
  assert ps("SELECT 4 FROM foo((SELECT 5))") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=TableFunctionCall(
        ["foo"],
        arguments=[
          TableSubquery(
            subquery=SelectDirect(
              core=RegularSelect(
                columns=[
                  ExpressionColumn(IntegerLiteral(5)),
                ]))),
        ])))

def test_parse_select_named_table():
  assert ps("SELECT 4 FROM foo.bar AS qux") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=_set_name(TableReference(["foo", "bar"]), "qux")))

  assert ps("SELECT 4 FROM (foo.bar AS qux)") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=_set_name(TableReference(["foo", "bar"]), "qux")))

  assert ps("SELECT 4 FROM (foo.bar) AS qux") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=_set_name(TableReference(["foo", "bar"]), "qux")))

def test_parse_select_named_table_with_renamings():
  assert ps("SELECT 4 FROM foo.bar AS qux(foo, bar)") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(IntegerLiteral(4))],
      from_=RenamingQueryTableExpression(
        base=TableReference(["foo", "bar"]),
        name="qux",
        renaming=("foo", "bar"),
      )))

def test_parse_table_subquery():
  assert ps("SELECT a FROM (SELECT b FROM c) AS z") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=_set_name(
        TableSubquery(
          SelectDirect(core=RegularSelect(
            columns=[ExpressionColumn(ColumnReference("b"))],
            from_=TableReference(["c"])))),
        "z")))

def test_parse_group_by():
  assert ps("SELECT 4 GROUP BY a, a + 1 UNION SELECT 5 GROUP BY z") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))],
                    gb=GroupByExpressions(
                      [ColumnReference("a"),
                       BinaryOperation(ColumnReference("a"),
                                       "+",
                                       IntegerLiteral("1"))])),
      "union",
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))],
                    gb=GroupByExpressions([ColumnReference("z")]))))

def test_parse_group_by_having():
  assert ps("SELECT 4 GROUP BY a HAVING 5") == SelectDirect(
    core=RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))],
                       gb=GroupByExpressions([ColumnReference("a")]),
                       having=IntegerLiteral(5)))

def test_parse_order_by():
  assert ps("SELECT aa FROM foo.bar ORDER BY aa DESC, zz") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("aa"))],
      from_=TableReference(["foo", "bar"])),
    ob=[OrderingTerm(ColumnReference("aa"), "desc"),
        OrderingTerm(ColumnReference("zz"))])

def test_parse_select_compound():
  assert ps("SELECT 4 UNION SELECT 5") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
      "union",
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))])
    ))

  assert ps("SELECT 4 UNION ALL SELECT 5") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
      "union all",
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))])
    ))

  assert ps("(SELECT 4) UNION ALL (SELECT 5)") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
      "union all",
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))])
    ))

  assert ps("(SELECT 4) UNION ALL (SELECT 5) EXCEPT SELECT 6") == SelectDirect(
    core=Compound(
      Compound(
        RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
        "union all",
        RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))])
      ),
      "except",
      RegularSelect(
        columns=[
          ExpressionColumn(IntegerLiteral(6))])))

  assert ps("(SELECT 4) UNION ALL (SELECT 5) INTERSECT SELECT 6") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
      "union all",
      Compound(
        RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))]),
        "intersect",
        RegularSelect(columns=[ExpressionColumn(IntegerLiteral(6))])
      )))

  assert ps("SELECT 4 UNION ALL SELECT 5 ORDER BY win") == SelectDirect(
    core=Compound(
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(4))]),
      "union all",
      RegularSelect(columns=[ExpressionColumn(IntegerLiteral(5))]),
    ),
    ob=(OrderingTerm(ColumnReference("win"), direction="asc"),),
  )

def test_union_distinct_order_by():
  qt = ps2qt(dedent("""\
  WITH foo AS (SELECT 'foo' AS str),
       bar AS (SELECT 'FOO' AS str)
  SELECT foo.str AS ss FROM foo
  UNION
  SELECT bar.str AS ss FROM bar
  ORDER BY ss COLLATE nocase
  """))
  assert _execute_qt(qt)["ss"] == ["foo", "FOO"]

def test_select():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT foo, bar FROM qux"
  qt = ps2qt(query, qux=qux)
  assert _execute_qt(qt) == \
    qux.as_dataframe()[["foo", "bar"]].to_dict("list")

def test_select_aliases():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT foo AS x_foo, bar FROM qux"
  qt = ps2qt(query, qux=qux)
  assert _execute_qt(qt) == \
    qux.as_dataframe()[["foo", "bar"]] \
    .rename(columns={"foo": "x_foo"}).to_dict("list")

def test_select_expression():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT foo*2 FROM qux"
  qt = ps2qt(query, qux=qux)
  assert _execute_qt(qt) == \
    (qux.as_dataframe()[["foo"]] * 2) \
    .rename(columns={"foo": "(foo * 2)"}) \
    .to_dict("list")

def test_select_unit():
  """Test that select without a source selects a virtual row"""
  qt = ps2qt("SELECT 1 + 2 + 3 AS v")
  assert _execute_qt(qt) == {
    "v": [1 + 2 + 3],
  }

  qt = ps2qt("SELECT 1 + 2 + 3 AS x, 7 as y")
  assert _execute_qt(qt) == {
    "x": [1 + 2 + 3],
    "y": [7],
  }



# The rows in NULL_TRUTH (reading left-to-right through the tuples)
# correspond to the rows in the convenient truth table at
# https://en.wikipedia.org/w/index.php?title=Null_(SQL)&oldid=833093792#Comparisons_with_NULL_and_the_three-valued_logic_(3VL)
# We've supplemented the table from Wikipedia with two additional
# columns, once each for <=> and <!=>, the "null safe"
# equality-comparison operators.

NULL_TRUTH_OPS = ("or", "and", "=", "<=>", "<!=>")
NULL_TRUTH = {
  # p     q        OR     AND    =      <=>    <!=>
  (True , True):  (True,  True,  True,  True,  False),
  (True,  False): (True,  False, False, False, True ),
  (True,  None):  (True,  None,  None,  False, True ),
  (False, True):  (True,  False, False, False, True ),
  (False, False): (False, False, True,  True,  False),
  (False, None):  (None,  False, None,  False, True ),
  (None,  True):  (True,  None,  None,  False, True ),
  (None,  False): (None,  False, None,  False, True ),
  (None,  None):  (None,  None,  None,  True,  False),
}

# Keep the parametrize annotations in reverse order relative to the
# argument list so that they read left-to-right in test names.
@pytest.mark.parametrize("right", [True, False, None])
@pytest.mark.parametrize("op", NULL_TRUTH_OPS)
@pytest.mark.parametrize("left", [True, False, None])
def test_logic(monkeypatch, left, op, right):
  # Inhibit static simplification of boolean expressions: we want to
  # run the comparisons for real.
  monkeypatch.setattr(BinaryOperation, "simplify",
                      BinaryOperation.mro()[1].simplify)
  def _sv(value):
    return "NULL" if value is None else str(value)
  expr = "{} {} {}".format(_sv(left), op, _sv(right))
  expected = NULL_TRUTH[(left, right)][NULL_TRUTH_OPS.index(op)]
  expected = None if expected is None else np.bool_(expected)
  assert pse(expr) is expected

def test_boolean_literals():
  # pylint: disable=compare-to-zero
  assert pse("true") is NP_TRUE
  assert pse("false") is NP_FALSE

def test_hex_literal():
  assert pse("0xdE4d") == 0xdE4d

def test_fls():
  assert pse("fls(0)") == 0  # pylint: disable=compare-to-zero
  assert pse("fls(1)") == 1
  assert pse("fls(NULL)") is None
  assert pse("fls(IF(3>2, NULL, 1))") is None
  assert pse("fls(IF(2>3, NULL, 1))") == 1
  assert pse("fls(64)") == 7
  assert pse("fls(65)") == 7
  assert pse("fls(-1)") == 64
  assert pse("fls(CAST(-1 AS INT8))") == 8
  assert pse("fls(CAST(5 AS INT8))") == 3
  with _qerror():
    pse("fls('abc')")

def test_between_and():
  assert pse("1 BETWEEN -1 and 2")
  assert not pse("-1 BETWEEN 5 and 10")
  assert pse("1 BETWEEN 1 and 2")
  assert pse("2 BETWEEN 1 and 2")
  assert pse("1 BETWEEN 1 and 1")

def test_null_arithmetic_is_zero():
  q = "SELECT NULL - 5 AS x"
  r = _execute_qt(ps2qt(q), as_raw_array=True)["x"]
  assert isinstance(r, np.ma.masked_array)
  assert list(r.data) == [0]
  assert list(r.mask) == [True]

def test_null_comparison_is_zero():
  q = "SELECT NULL < 5 AS x"
  r = _execute_qt(ps2qt(q), as_raw_array=True)["x"]
  assert isinstance(r, np.ma.masked_array)
  assert list(r.data) == [False]
  assert list(r.mask) == [True]

def test_wildcard_select():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT * FROM qux"
  qt = ps2qt(query, qux=qux)
  assert _execute_qt(qt) == qux.as_dataframe().to_dict("list")

def test_wildcard_select_named():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT qux.* FROM qux"
  qt = ps2qt(query, qux=qux)
  assert _execute_qt(qt) == qux.as_dataframe().to_dict("list")

def test_wildcard_select_unmatched():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT spam.* FROM qux"
  with _qerror():
    ps2qt(query, qux=qux)

def test_column_does_not_exist():
  qux = TestQueryTable(
    names=("foo", "bar", "spam"),
    rows=[
      [1, 2, 3],
      [4, 5, 6],
    ])
  query = "SELECT qux.blarg FROM qux"
  with _qerror():
    ps2qt(query, qux=qux)

WEIGHTS = TestQueryTable(
  names=("id", "weight"),
  rows=[
    [0,  5],
    [1, 10],
    [2, 30],
    [3, 15],
  ])

LENGTHS = TestQueryTable(
  names=("id", "length"),
  rows=[
    [2, 200],
    [1, 100],
    [3, 300],
    [4, 400],
  ])

COLORS = TestQueryTable(
  names=("id", "color"),
  rows=[
    [2, -2],
    [1, -1],
    [3, -3],
    [4, -4],
  ])

def test_sum_ungrouped():
  query = "SELECT SUM(weight) FROM weights"
  env = dict(weights=WEIGHTS, lengths=LENGTHS, colors=COLORS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("SUM(weight)",),
    rows = [
      [60],
    ]).as_dict()

def test_sum_of_nulls():
  q = "SELECT SUM(x) AS c FROM (VALUES (NULL), (NULL), (NULL)) AS foo(x)"
  assert _execute_qt(ps2qt(q))["c"] == [None]

def test_count_star_ungrouped():
  query = "SELECT COUNT(*) AS count FROM weights"
  env = dict(weights=WEIGHTS, lengths=LENGTHS, colors=COLORS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("count",),
    rows = [
      [4],
    ]).as_dict()

def test_count_star_filtered():
  q = """
SELECT COUNT(*) AS c FROM (VALUES (1), (2), (3)) AS foo(bar)
WHERE bar>= 2
  """
  assert _execute_qt(ps2qt(q))["c"][0] == 2

def test_where_true():
  q = "SELECT val FROM (VALUES (1), (2), (3)) AS vals(val) WHERE TRUE"
  assert _execute_qt(ps2qt(q))["val"] == [1, 2, 3]

def test_where_non_boolean():
  q = "SELECT val FROM (VALUES (1)) AS vals(val) WHERE 'asdf'"
  with _qerror("cannot use query of type as filter"):
    _execute_qt(ps2qt(q))

def test_count_specific_column():
  query = "SELECT COUNT(weight) FROM weights"
  env = dict(weights=WEIGHTS, lengths=LENGTHS, colors=COLORS)
  qt = ps2qt(query, **env)
  assert _execute_qt(qt) == TestQueryTable(
    names=("COUNT(weight)",),
    rows = [
      [4],
    ]).as_dict()

DUPS = TestQueryTable(
  names=("id", "value"),
  rows=[
    [0,  5],
    [1,  5],
    [2,  5],
    [3,  6],
    [3,  6],
  ])

def test_count_distinct():
  query = "SELECT COUNT(DISTINCT value) AS foo FROM dups"
  qt = ps2qt(query, dups=DUPS)
  assert _execute_qt(qt) == TestQueryTable(
    names=("foo",),
    rows = [
      [2],
    ]).as_dict()

def test_sum_distinct():
  query = ("SELECT SUM(DISTINCT value) AS foo, "
           "SUM(value) AS bar, "
           "SUM(ALL value) AS qux FROM dups")
  qt = ps2qt(query, dups=DUPS)
  assert _execute_qt(qt) == TestQueryTable(
    names=("foo", "bar", "qux"),
    rows = [
      [11, 27, 27],
    ]).as_dict()

def test_count_distinct_column_name():
  query = "SELECT COUNT(DISTINCT value) FROM dups"
  qt = ps2qt(query, dups=DUPS)
  assert list(qt.columns) == ["COUNT(DISTINCT value)"]

def test_illegal_distinct():
  query = "SELECT COUNT(DISTINCT) FROM dups"
  with _qerror("illegal use of DISTINCT"):
    ps2qt(query, dups=DUPS)

def test_aggregate_sum_distinct_null():
  tbl = TestQueryTable(
    names=("value",),
    rows=[
      [-1],
      [N],
      [-2],
      [-2],
      [-3],
    ])
  query = ("SELECT "
           "SUM(DISTINCT value) AS sum, "
           "COUNT(DISTINCT value) AS dcnt, "
           "COUNT(value) AS cnt "
           "FROM tbl")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("sum", "dcnt", "cnt"),
    rows = [
      [-6, 3, 4],
    ]).as_dict()


def test_select_distinct_non_null():
  tbl = TestQueryTable(
    names=("key1", "key2"),
    rows=[
      [1, 1],
      [2, 2],
      [1, 1],
      [1, 2],
      [2, 1],
      [1, 2],
    ])
  query = ("SELECT DISTINCT key1, key2 FROM tbl")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("key1", "key2"),
    rows = [
      [1, 1],
      [2, 2],
      [1, 2],
      [2, 1],
    ]).as_dict()

def test_select_distinct_null():
  tbl = TestQueryTable(
    names=("key1", "key2"),
    rows=[
      [1, 1],
      [2, 2],
      [1, 1],
      [1, N],
      [N, N],
      [N, N],
      [1, 2],
      [2, 1],
      [1, 2],
    ])
  query = ("SELECT DISTINCT key1, key2 FROM tbl")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("key1", "key2"),
    rows = [
      [1, 1],
      [2, 2],
      [1, N],
      [N, N],
      [1, 2],
      [2, 1],
    ]).as_dict()

def test_group_max_null():
  tbl = TestQueryTable(
    names=("key", "value"),
    rows=[
      [1, 1],
      [1, N],
      [2, N],
    ])
  query = ("SELECT key, MAX(value) AS maxv, MIN(value) AS minv "
           "FROM tbl GROUP BY key")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("key", "maxv", "minv"),
    rows = [
      [1, 1, 1],
      [2, N, N],
    ]).as_dict()

def test_group_having():
  tbl = TestQueryTable(
    names=("key1", "key2", "value"),
    rows=[
      [1, 2, -1.0],
      [1, 1, -2.0],
      [1, 1, -3.0],
      [N, 1, -4.0],
      [N, 1, -4.0],
      [N, 1,  N],
      [1, N, -7.0],
      [N, N,  N],
    ])
  query = ("SELECT key1, key2 "
           "FROM tbl GROUP BY key1, key2 HAVING MIN(value) <= -3")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("key1", "key2"),
    rows = [
      [1, 1],
      [N, 1],
      [1, N],
    ]).as_dict()

def test_non_group_having():
  tbl = TestQueryTable(
    names=("key1", "key2", "value"),
    rows=[
      [1, 2, -1.0],
      [1, 1, -2.0],
      [1, 1, -3.0],
      [N, 1, -4.0],
      [N, 1, -4.0],
      [N, 1,  N],
      [1, N, -7.0],
      [N, N,  N],
    ])
  query = ("SELECT key1, key2 "
           "FROM tbl HAVING value <= -3")
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("key1", "key2"),
    rows = [
      [1, 1],
      [N, 1],
      [N, 1],
      [1, N],
    ]).as_dict()

@pytest.mark.parametrize("order", [True, False])
def test_select_union_all(order):
  values_1 = [1, 2, 3, 1]
  values_2 = [3, 4, 5, 6]
  tbl1 = TestQueryTable(names=["value"], columns=[values_1])
  tbl2 = TestQueryTable(names=["value"], columns=[values_2])
  merged_values = values_1 + values_2
  if order:
    merged_values.sort()
  tbl_merged = TestQueryTable(names=["value"],
                              columns=[merged_values])

  query = "SELECT value FROM tbl1 UNION ALL SELECT value FROM tbl2"
  if order:
    query += " ORDER BY value"
  qt = ps2qt(query, tbl1=tbl1, tbl2=tbl2)
  assert _execute_qt(qt) == tbl_merged.as_dict()

@pytest.mark.parametrize("order", [True, False])
def test_select_union(order):
  values_1 = [1, 2, 3, 1]
  values_2 = [3, 4, 5, 6]
  merged_values = list(_drop_duplicates(values_1 + values_2))
  if order:
    merged_values.sort()
  tbl1 = TestQueryTable(names=["value"], columns=[values_1])
  tbl2 = TestQueryTable(names=["value"], columns=[values_2])
  tbl_merged = TestQueryTable(
    names=["value"],
    columns=[merged_values])
  query = "SELECT value FROM tbl1 UNION SELECT value FROM tbl2"
  if order:
    query += " ORDER BY value"
  qt = ps2qt(query, tbl1=tbl1, tbl2=tbl2)
  assert _execute_qt(qt) == tbl_merged.as_dict()

@pytest.mark.parametrize("order", [True, False])
def test_select_intersect(order):
  values_1 = [1, 2, 3, 1]
  values_2 = [3, 4, 3, 5, 6]
  tbl1 = TestQueryTable(names=["value"], columns=[values_1])
  tbl2 = TestQueryTable(names=["value"], columns=[values_2])
  tbl_merged = TestQueryTable(names=["value"], columns=[[3]])
  query = "SELECT value FROM tbl1 INTERSECT SELECT value FROM tbl2"
  if order:
    query += " ORDER BY value"
  qt = ps2qt(query, tbl1=tbl1, tbl2=tbl2)
  assert _execute_qt(qt) == tbl_merged.as_dict()

@pytest.mark.parametrize("order", [True, False])
def test_select_except(order):
  values_1 = [1, 2, 3, 2, 1, 0]
  values_2 = [2, 3, 4]
  tbl1 = TestQueryTable(names=["value"], columns=[values_1])
  tbl2 = TestQueryTable(names=["value"], columns=[values_2])
  query = "SELECT value FROM tbl1 EXCEPT SELECT value FROM tbl2"
  if order:
    query += " ORDER BY value"
  qt = ps2qt(query, tbl1=tbl1, tbl2=tbl2)
  assert _execute_qt(qt)["value"] == [0, 1] if order else [1, 0]
  # Anti-symmetric difference, so try it the other way too.
  query = "SELECT value FROM tbl2 EXCEPT SELECT value FROM tbl1"
  if order:
    query += " ORDER BY value"
  qt = ps2qt(query, tbl1=tbl1, tbl2=tbl2)
  assert _execute_qt(qt)["value"] == [4]

def test_select_column_order():
  query = "SELECT 1 AS foo, 2 AS bar, 3 AS qux"
  qt = ps2qt(query)
  assert list(qt.columns) == ["foo", "bar", "qux"]
  query = "SELECT 1 AS qux, 2 AS bar, 3 AS foo"
  qt = ps2qt(query)
  assert list(qt.columns) == ["qux", "bar", "foo"]

def test_parse_select_limit_offset():
  assert ps("SELECT 4 LIMIT 1 OFFSET 2") == SelectDirect(
    core=RegularSelect(
      columns=[
        ExpressionColumn(IntegerLiteral(4))]),
    limit=IntegerLiteral(1),
    offset=IntegerLiteral(2),
  )

def test_parse_create_view():
  cv, = pdml("CREATE VIEW fml AS SELECT 4;")
  assert cv == CreateView(["fml"], ps("SELECT 4"))

def test_parse_dml_omit_trailing_semicolon():
  command1 = "CREATE VIEW fml AS SELECT 4"
  cv1 = CreateView(["fml"], ps("SELECT 4"))
  assert pdml(command1) == [cv1]
  assert pdml(command1 + ";") == [cv1]
  command2 = command1 + ";" + command1
  assert pdml(command2) == [cv1, cv1]
  assert pdml(command2 + ";") == [cv1, cv1]

def _limit_offset_to_pyslice(*, limit, offset):
  offset = the(int, offset or 0)
  if offset <= 0:
    return slice(offset, the((int, NoneType), limit))
  if limit is not None:
    return slice(offset, offset + the(int, limit))
  return slice(offset, None)

@pytest.mark.parametrize(("limit", "offset"), [
  (0, 1000),
  (1000, 0),
  (1, 0),
  (2, 3),
  (1, -1),
  (1, -2),
  (None, 2),
  (None, -2),
])
def test_limit_offset(limit, offset):
  rows = [
    [3, 1, 9],
    [2, 2, 8],
    [5, 1, 7],
    [1, 2, 6],
    [1, 2, 5],
    [2, 1, 4],
    [1, 2, 3],
  ]
  tbl = TestQueryTable(names=("key1", "key2", "value"), rows=rows)
  pyslice = _limit_offset_to_pyslice(limit=limit, offset=offset)
  tbl_lim = TestQueryTable(names=("key1", "key2", "value"), # XXX
                           rows=rows[pyslice])
  def _tosql(value):
    return "NULL" if value is None else str(value)
  query = "SELECT * FROM tbl LIMIT {limit} OFFSET {offset}".format(
    limit=_tosql(limit), offset=_tosql(offset))
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == tbl_lim.as_dict()

def test_limit_offset_expr():
  """Test that limit is actually evaluted as an expression"""
  rows = [
    [3, 1, 9],
    [2, 2, 8],
    [5, 1, 7],
    [1, 2, 3],
  ]
  tbl = TestQueryTable(names=("key1", "key2", "value"), rows=rows)
  tbl_lim = TestQueryTable(names=("key1", "key2", "value"),
                           rows=rows[:3])
  query = "SELECT * FROM tbl LIMIT 2+1"
  qt = ps2qt(query, tbl=tbl)
  assert _execute_qt(qt) == tbl_lim.as_dict()

def test_parse_tvf_complex_expression():
  assert ptbl("tvf(1,2)") == TableFunctionCall(
    ["tvf"],
    arguments=[IntegerLiteral(1), IntegerLiteral(2)])
  assert ptbl("tvf((1 + 2))") == TableFunctionCall(
    ["tvf"],
    arguments=[
      BinaryFunExprOperation(IntegerLiteral(1),
                             "+",
                             IntegerLiteral(2))
    ])
  assert ptbl("tvf(1+2*3)") == TableFunctionCall(
    ["tvf"],
    arguments=[
      BinaryFunExprOperation(IntegerLiteral(1),
                             "+",
                             BinaryFunExprOperation(
                               IntegerLiteral(2),
                               "*",
                               IntegerLiteral(3)))])

def test_tvf_basic():
  query = "SELECT * FROM my_function(5, bar=>'hello')"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[
      [1, 2],
      [3, 4],
      [5, 6],
    ])

  def _my_function(foo, *, bar):
    assert foo == 5
    assert bar == "hello"
    return tbl
  qt = ps2qt(query, my_function=TableValuedFunction(_my_function))
  assert _execute_qt(qt) == tbl.as_dict()

def test_tvf_argument_matching():
  def _doit(q, fn):
    fn_ast = parse_select(fn)
    fn_obj = TableValuedFunction.from_select_ast(fn_ast)
    return _execute_qt(ps2qt(q, fn=fn_obj))
  fn = "(VALUES (1, 2), (3, 4))"
  assert _doit("SELECT * FROM fn()",
               fn=fn) == {"col0": [1, 3], "col1": [2, 4]}
  assert _doit("SELECT * FROM fn() AS blarg",
               fn=fn) == {"col0": [1, 3], "col1": [2, 4]}
  assert _doit("SELECT blarg.* FROM fn() AS blarg",
               fn=fn) == {"col0": [1, 3], "col1": [2, 4]}
  assert _doit("SELECT blarg.col0 FROM fn() AS blarg",
               fn=fn) == {"blarg.col0": [1, 3]}
  assert _doit("SELECT fn.col0 FROM fn()",
               fn=fn) == {"fn.col0": [1, 3]}

def test_tvf_dict_list_literal():
  query = "SELECT * FROM my_function([1,2], {'foo'+'bar':4, qux=>7})"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[])
  def _my_function(alist, adict):
    assert alist == [1, 2]
    assert adict == dict(foobar=4, qux=7)
    return tbl
  qt = ps2qt(query, my_function=TableValuedFunction(_my_function))
  assert _execute_qt(qt) == tbl.as_dict()

def test_tvf_tv_argument():
  query = "SELECT * FROM my_function(foo=>tbl)"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[])
  def _my_function(foo):
    assert foo is tbl
    return foo
  qt = ps2qt(query,
             tbl=tbl,
             my_function=TableValuedFunction(_my_function))
  assert _execute_qt(qt) == tbl.as_dict()

def test_tvf_subquery():
  query = "SELECT * FROM my_function((SELECT 5 AS x))"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[])
  def _my_function(x_tbl):
    assert isinstance(x_tbl, QueryTable)
    assert list(x_tbl.columns) == ["x"]
    return tbl
  qt = ps2qt(query, my_function=TableValuedFunction(_my_function))
  assert _execute_qt(qt) == tbl.as_dict()

def test_tvf_from_select_ast():
  def _doit(q, fn):
    fn_ast = parse_select(fn)
    fn_obj = TableValuedFunction.from_select_ast(fn_ast)
    return _execute_qt(ps2qt(q, fn=fn_obj))["col0"]
  assert _doit("SELECT * FROM fn()",
               fn="(VALUES (1), (2))") == [1, 2]
  assert _doit("SELECT * FROM fn(8)",
               fn="(VALUES (1), (?0))") == [1, 8]
  assert _doit("SELECT * FROM fn(arg=>5)",
               fn="(VALUES (1), (:arg))") == [1, 5]
  with _qerror("unused"):
    _doit("SELECT * FROM fn(8)",
          fn="(VALUES (1), (2))")
  with _qerror("no bind parameter"):
    _doit("SELECT * FROM fn()",
          fn="(VALUES (1), (?0))")

def _do_test_span_join(*, op, rows1, rows2, wanted, partition=None):
  spans1 = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=rows1,
    table_schema=(SPAN_UNPARTITIONED_TIME_MAJOR if not partition
                  else TableSchema(TableKind.SPAN,
                                   "value",
                                   TableSorting.TIME_MAJOR)))
  spans2 = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=rows2,
    table_schema=(SPAN_UNPARTITIONED_TIME_MAJOR if not partition
                  else TableSchema(TableKind.SPAN,
                                   "value",
                                   TableSorting.TIME_MAJOR)))
  query = ("SELECT SPAN spans1.value AS value1, spans2.value AS value2 "
           "FROM spans1 {} spans2".format(op))
  qt = ps2qt(query, spans1=spans1, spans2=spans2)\
    .to_time_major()
  names=["_ts", "_duration", "value1", "value2"]
  if partition:
    table_schema = TableSchema(TableKind.SPAN,
                               "value",
                               TableSorting.TIME_MAJOR)
    names[2:2] = ["value"]
  else:
    table_schema = SPAN_UNPARTITIONED_TIME_MAJOR

  assert _execute_qt(qt) == TestQueryTable(
    names=names,
    rows=wanted,
    table_schema=table_schema)

def test_span_join_inner_simple():
  _do_test_span_join(
    op="SPAN INNER JOIN",
    rows1=[
      [10, 10, -1],
      [20,  5, -2],
    ],
    rows2=[
      [10,  5, -3],
    ],
    wanted=[
      [10, 5, -1, -3],
    ])

def test_span_join_inner_back_to_back():
  _do_test_span_join(
    op="SPAN INNER JOIN",
    rows1=[
      [10, 10, -1],
      [20, 10, -2],
    ],
    rows2=[
      [10, 10, -3],
      [20, 10, -4],
    ],
    wanted=[
      [10, 10, -1, -3],
      [20, 10, -2, -4],
    ])

def test_span_join_inner_gap():
  _do_test_span_join(
    op="SPAN INNER JOIN",
    rows1=[
      [10,  5, -1],
      [20, 10, -2],
    ],
    rows2=[
      [10,  7, -3],
      [20, 10, -4],
    ],
    wanted=[
      [10,  5, -1, -3],
      [20, 10, -2, -4],
    ])

def test_span_join_inner_overlap():
  _do_test_span_join(
    op="SPAN INNER JOIN",
    rows1=[
      [10, 30, -1],
    ],
    rows2=[
      [15, 50, -2],
    ],
    wanted=[
      [15,  25, -1, -2],
    ])

@pytest.mark.skipif(
  not SPAN_INVARIANT_CHECKING,
  reason="optimized build doesn't check for span errors")
def test_span_join_inner_invalid():
  with _qerror("before span end"):
    _do_test_span_join(
      op="SPAN INNER JOIN",
      rows1=[
        [10, 10, -1],
        [11, 10, -1],
        [20,  5, -2],
      ],
      rows2=[
        [10,  5, -3],
      ],
      wanted=[])

def test_span_join_outer():
  _do_test_span_join(
    op="SPAN OUTER JOIN",
    rows1=[
      [10, 10, -1],
    ],
    rows2=[
      [ 5, 10, -2],
    ],
    wanted=[
      [ 5,  5,  N,  -2],
      [10,  5, -1,  -2],
      [15,  5, -1,   N],
    ])
def test_span_join_outer_overlap():
  _do_test_span_join(
    op="SPAN OUTER JOIN",
    rows1=[
      [10, 30, -1],
    ],
    rows2=[
      [15, 50, -2],
    ],
    wanted=[
      [10,  5, -1,  N],
      [15, 25, -1, -2],
      [40, 25,  N, -2],
    ])

def test_span_join_left_overlap():
  _do_test_span_join(
    op="SPAN LEFT OUTER JOIN",
    rows1=[
      [10, 30, -1],
    ],
    rows2=[
      [15, 50, -2],
    ],
    wanted=[
      [10,  5, -1,  N],
      [15, 25, -1, -2],
    ])

def test_span_join_right_overlap():
  _do_test_span_join(
    op="SPAN RIGHT OUTER JOIN",
    rows1=[
      [10, 30, -1],
    ],
    rows2=[
      [15, 50, -2],
    ],
    wanted=[
      [15, 25, -1, -2],
      [40, 25,  N, -2],
    ])

def test_span_join_partition():
  _do_test_span_join(
    op="SPAN OUTER JOIN",
    partition=True,
    rows1=[
      [25,  4,  1],
      [26,  5,  2],
      [30,  9,  1],
      [39, 10,  2],
      [40, 10,  1],
    ],
    rows2=[],
    wanted=[
      [25,  4,  1, 1, N],
      [26,  5,  2, 2, N],
      [30,  9,  1, 1, N],
      [39, 10,  2, 2, N],
      [40, 10,  1, 1, N],
    ])

def test_span_join_where_filter():
  spans1 = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  10,  5],
      [10,  15,  6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans2 = TestQueryTable(
    names=("_ts", "_duration", "value2"),
    rows=[
      [ 0,  5,  -5],
      [ 9,  3,  -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  query = ("SELECT SPAN value1, value2 "
           "FROM spans1 SPAN LEFT OUTER JOIN spans2 "
           "WHERE value2=-6 OR value2 is NULL")
  qt = ps2qt(query, spans1=spans1, spans2=spans2)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "value1", "value2"],
    rows=[
      [ 5,  4,  5,  N],
      [ 9,  1,  5, -6],
      [10,  2,  6, -6],
      [12, 13,  6,  N],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_wildcard_columns():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value1", "value2"),
    rows=[
      [ 0,  10,  5,  3],
      [10,  15,  6,  4],
    ],
    table_schema=TableSchema(TableKind.SPAN,
                             "value1",
                             TableSorting.TIME_MAJOR))
  query = "SELECT * FROM xspans"
  qt = ps2qt(query, xspans=spans)
  assert qt.columns == ("_ts", "_duration", "value1", "value2")

def test_span_join_partition_mismatch():
  spans1 = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  10,  5],
      [10,  15,  6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value1",
                             TableSorting.TIME_MAJOR))
  spans2 = TestQueryTable(
    names=("_ts", "_duration", "value2"),
    rows=[
      [ 0,  5,  5],
      [ 9,  3,  9],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value2",
                             TableSorting.TIME_MAJOR))
  query = ("SELECT _ts, _duration, value1, value2 "
           "FROM spans1 SPAN INNER JOIN spans2")
  with _qerror("partition columns differ"):
    ps2qt(query, spans1=spans1, spans2=spans2)

def test_span_invalid_columns():
  spans1 = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=[
      [ 0,  10,  5],
      [10,  15,  6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value",
                             TableSorting.TIME_MAJOR))
  spans2 = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=[
      [ 0,  5,  5],
      [ 9,  3,  9],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value",
                             TableSorting.TIME_MAJOR))
  query = ("SELECT SPAN value "
           "FROM spans1 SPAN INNER JOIN spans2")
  ast = ps(query)
  tctx = TvfContext.from_ns() \
                   .let(dict(spans1=spans1, spans2=spans2))
  with _qerror("must not be explicitly specified"):
    ast.make_qt(tctx)
  query = ("SELECT SPAN _ts "
           "FROM spans1 SPAN INNER JOIN spans2")
  ast = ps(query)
  with _qerror("column '_ts' is reserved"):
    ast.make_qt(tctx)
  query = ("SELECT SPAN _duration "
           "FROM spans1 SPAN INNER JOIN spans2")
  ast = ps(query)
  with _qerror("column '_duration' is reserved"):
    ast.make_qt(tctx)

def test_span_empty_column_list():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=[
      [ 0,  10,  5],
      [10,  15,  6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value", TableSorting.TIME_MAJOR))
  query = "SELECT SPAN FROM xspans"
  qt = ps2qt(query, xspans=spans)
  result_qt = _execute_qt(qt)
  assert list(result_qt) == ["_ts", "_duration", "value"]
  assert result_qt == spans

def test_span_limit_offset():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=[
      [ 0,  10,  5],
      [10,  15,  6],
      [30,   5,  1],
    ],
    table_schema=TableSchema(TableKind.SPAN,
                             "value",
                             TableSorting.TIME_MAJOR))
  query = "SELECT SPAN FROM (SELECT SPAN * FROM xspans LIMIT 1 OFFSET 1)"
  qt = ps2qt(query, xspans=spans).to_time_major()
  result_qt = _execute_qt(qt)
  assert list(result_qt) == ["_ts", "_duration", "value"]
  assert result_qt == TestQueryTable(
    names=("_ts", "_duration", "value"),
    rows=[
      [10,  15,  6],
    ],
    table_schema=spans.table_schema)

def test_span_star():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value",
                             TableSorting.TIME_MAJOR))
  query = "SELECT SPAN * FROM xspans"
  qt = ps2qt(query, xspans=spans)
  result_qt = _execute_qt(qt)
  assert result_qt == spans

def test_span_star_renamed_unpartitioned():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  query = "SELECT SPAN * FROM xspans AS blarg(foo1, foo2)"
  qt = ps2qt(query, xspans=spans)
  result_qt = _execute_qt(qt)
  assert result_qt ==  TestQueryTable(
    names=("_ts", "_duration", "foo1", "foo2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_star_renamed_partitioned():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value",
                             TableSorting.TIME_MAJOR))
  query = "SELECT SPAN * FROM xspans AS blarg(foo1, foo2)"
  qt = ps2qt(query, xspans=spans)
  result_qt = _execute_qt(qt)
  assert result_qt ==  TestQueryTable(
    names=("_ts", "_duration", "foo1", "foo2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "foo1",
                             TableSorting.TIME_MAJOR))

def test_span_star_renamed_partitioned_partial():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "value",
                             TableSorting.TIME_MAJOR))
  query = "SELECT SPAN * FROM xspans AS blarg(value=>blarg)"
  qt = ps2qt(query, xspans=spans)
  result_qt = _execute_qt(qt)
  assert result_qt ==  TestQueryTable(
    names=("_ts", "_duration", "blarg", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=TableSchema(TableKind.SPAN, "blarg",
                             TableSorting.TIME_MAJOR))

def test_span_order_by_invalid():
  spans = TestQueryTable(
    names=("_ts", "_duration", "value", "value2"),
    rows=[
      [ 0,  10,  5, -5],
      [10,  15,  6, -6],
    ],
    table_schema=_span_table_time_major("value"))
  query = "SELECT SPAN * FROM xspans ORDER BY _ts"
  with _qerror("SELECT SPAN incompatible with ORDER BY"):
    ps2qt(query, xspans=spans)

def test_span_broadcast_inner():
  spans_part = TestQueryTable(
    names=("_ts", "_end", "cpu"),
    rows=[
      [ 5, 25, 1],
      [ 6,  8, 0],
      [ 6,  9, 2],
      [10, 20, 0],
    ],
    table_schema=_span_table_time_major("cpu"))

  spans_broadcast = TestQueryTable(
    names=("_ts", "_end", "color"),
    rows=[
      [ 0,  5, -1],
      [ 5,  8, -2],
      [ 9, 18, -3],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = "SELECT SPAN * FROM sb SPAN BROADCAST INTO SPAN PARTITIONS sp"
  qt = ps2qt(query, sp=spans_part, sb=spans_broadcast) \
    .to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "cpu", "color"],
    rows=[
      [ 5,  8,  1, -2],
      [ 6,  8,  0, -2],
      [ 6,  8,  2, -2],
      [ 9, 18,  1, -3],
      [10, 18,  0, -3],
    ],
    table_schema=spans_part.table_schema)

def test_span_broadcast_outer():
  spans_part = TestQueryTable(
    names=("_ts", "_duration", "cpu"),
    rows=[
      [ 9, 10, 0],
      [25, 10, 1],
    ],
    table_schema=_span_table_time_major("cpu"))

  spans_broadcast = TestQueryTable(
    names=("_ts", "_duration", "color"),
    rows=[
      [10, 20, -1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = "SELECT SPAN * FROM sb SPAN RIGHT BROADCAST INTO SPAN PARTITIONS sp"
  qt = ps2qt(query, sp=spans_part, sb=spans_broadcast) \
    .to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "cpu", "color"],
    rows=[
      [ 9,  1,  0,  N],
      [10,  9,  0, -1],
      [25,  5,  1, -1],
      [30,  5,  1,  N],
    ], table_schema=spans_part.table_schema)

  query = "SELECT * FROM sb SPAN LEFT BROADCAST INTO SPAN PARTITIONS sp"
  with _qerror("not supported"):
    _execute_qt(ps2qt(query, sp=spans_part, sb=spans_broadcast))
  query = "SELECT * FROM sb SPAN OUTER BROADCAST INTO SPAN PARTITIONS sp"
  with _qerror("not supported"):
    _execute_qt(ps2qt(query, sp=spans_part, sb=spans_broadcast))

def test_star_join_ambiguity_resolution():
  foo = TestQueryTable(
    names=["thing"],
    rows=[[5]])
  bar = TestQueryTable(
    names=["thing"],
    rows=[[10]])
  q = "SELECT * FROM foo INNER JOIN bar ON foo.thing = bar.thing//2"
  assert _execute_qt(ps2qt(q, foo=foo, bar=bar)) == TestQueryTable(
    names=["thing", "thing:1"],
    rows=[[5, 10]])

def test_span_join_non_span():
  foo = TestQueryTable(
    names=("_ts", "_end", "cpu"),
    rows=[
      [ 5, 25, 1],
      [ 6,  9, 2],
      [ 6,  8, 0],
      [10, 20, 0],
    ],
    table_schema=_span_table_time_major("cpu"))
  bar = TestQueryTable(
    names=("cpu", "mhz"),
    rows=[
      [0, 100],
      [1, 200],
      [2, 300],
    ])
  q = "SELECT SPAN * FROM foo INNER JOIN bar USING (cpu)"
  with _qerror("non-span table"):
    _execute_qt(ps2qt(q, foo=foo, bar=bar))
  q = "SELECT SPAN * FROM foo SPAN INNER JOIN bar ON foo.cpu = bar.cpu"
  with _qerror("only if the join condition is unique"):
    _execute_qt(ps2qt(q, foo=foo, bar=bar))
  qux = TestQueryTable(
    names=("cpu", "mhz"),
    schemas=(QuerySchema(INT32, constraints={UNIQUE}),
             QuerySchema(INT64)),
    rows=[
      [0, 100],
      [1, 200],
    ])
  q = "SELECT SPAN * FROM foo SPAN INNER JOIN qux USING (cpu)"
  assert _execute_qt(ps2qt(q, foo=foo, qux=qux)) == TestQueryTable(
    names=["_ts", "_duration", "cpu", "mhz"],
    rows=[
      [5,  20, 1, 200],
      [6,   2, 0, 100],
      [10, 10, 0, 100],
    ],
    table_schema=foo.table_schema)
  q = "SELECT SPAN * FROM foo SPAN LEFT JOIN qux USING (cpu)"
  assert _execute_qt(ps2qt(q, foo=foo, qux=qux)) == TestQueryTable(
    names=["_ts", "_duration", "cpu", "mhz"],
    rows=[
      [5,  20, 1, 200],
      [6,   3, 2,   N],
      [6,   2, 0, 100],
      [10, 10, 0, 100],
    ],
    table_schema=foo.table_schema)
  q = "SELECT SPAN * FROM foo SPAN RIGHT JOIN qux USING (cpu)"
  with _qerror("illegal here"):
    _execute_qt(ps2qt(q, foo=foo, qux=qux))
  q = "SELECT SPAN * FROM foo SPAN RIGHT JOIN qux"
  with _qerror("not a span table"):
    _execute_qt(ps2qt(q, foo=foo, qux=qux))

@pict_parameterize(dict(
  foo_sort=["none", "time_major", "partition_major"],
  bar_sort=["none", "time_major", "partition_major"],
))
def test_span_join_partition2(foo_sort, bar_sort):
  foo_rows = [
    [ 1, 4, -1 ],
    [ 2, 4, -2 ],
    [ 6, 8, -2 ],
  ]
  bar_rows = [
    [ 1, 5, -1, 100 ],
    [ 2, 4, -2, 200 ],
  ]

  rng = _make_rng()
  rng.shuffle(foo_rows)
  rng.shuffle(bar_rows)

  foo = TestQueryTable(
    names=("_ts", "_end", "part"),
    rows=foo_rows,
    table_schema=TableSchema(
      TableKind.SPAN, "part", TableSorting.NONE)) \
      .to_schema(sorting=TableSorting(foo_sort))
  bar = TestQueryTable(
    names=("_ts", "_end", "part", "value"),
    rows=bar_rows,
    table_schema=TableSchema(
      TableKind.SPAN, "part", TableSorting.NONE)) \
  .to_schema(sorting=TableSorting(bar_sort))
  q = """
  SELECT SPAN part * value AS z
  FROM foo SPAN INNER JOIN bar
  """
  assert _execute_qt(
    ps2qt(q, foo=foo, bar=bar)
    .to_time_major()
  ) == TestQueryTable(
    names=["_ts", "_duration", "part", "z"],
    rows=[
      [1, 3, -1, -100],
      [2, 2, -2, -400]
    ],
    table_schema=_span_table_time_major("part"))

def test_subquery():
  query = "SELECT 4 AS foo FROM (SELECT 4 AS bar)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["foo"],
    rows=[
      [4],
    ]).as_dict()

def test_subquery_star_tblname():
  query = "SELECT x.* FROM (SELECT 4 AS bar) AS x"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["bar"],
    rows=[
      [4],
    ]).as_dict()

def test_subquery_star_tblname_renamed():
  query = "SELECT x.* FROM (SELECT 4 AS bar, 5 AS spam) AS x(qux, foo)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["qux", "foo"],
    rows=[
      [4, 5],
    ]).as_dict()

def test_subquery_star_tblname_renamed_partial():
  query = "SELECT x.* FROM (SELECT 4 AS bar, 5 AS spam) AS x(bar=>hork)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["hork", "spam"],
    rows=[
      [4, 5],
    ])

def test_subquery_star_tblname_renamed_deleted():
  query = "SELECT x.* FROM (SELECT 4 AS bar, 5 AS spam) AS x(bar=>NULL)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["spam"],
    rows=[
      [5],
    ])

def test_subquery_star_tblname_renamed_overwrite():
  query = "SELECT x.* FROM (SELECT 4 AS bar, 5 AS spam) AS x(bar=>spam)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["spam"],
    rows=[
      [4],
    ])

def test_subquery_star_tblname_renamed_partial_missing():
  query = "SELECT x.* FROM (SELECT 4 AS bar, 5 AS spam) AS x(blah=>hork)"
  with _qerror("absent or special columns"):
    ps2qt(query)

def test_subquery_star_with_alias():
  query = "SELECT * FROM (SELECT 4 AS bar) AS x"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["bar"],
    rows=[
      [4],
    ]).as_dict()

def test_subquery_star_without_alias():
  query = "SELECT * FROM (SELECT 4 AS bar)"
  qt = ps2qt(query)
  assert _execute_qt(qt) == TestQueryTable(
    names=["bar"],
    rows=[
      [4],
    ]).as_dict()

def _qty_eval(query):
  query = "SELECT {} AS value".format(query)
  qt = ps2qt(query)
  schema = qt["value"].schema
  assert not schema.is_string
  m = _execute_qt(qt)["value"][0]
  if not schema.unit:
    assert schema.unit is None
    return m
  return ureg().Quantity(m, schema.unit)

def test_string_operations():
  query = "SELECT 'foo' = 'foo' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'foo' = 'bar' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [False]
  query = "SELECT 'foo' = 0 AS value"
  with _qerror("non-string"):
    _execute_qt(ps2qt(query))["value"]
  query = "SELECT 'foo' AND 'foo' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'foo' AND '' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [False]
  query = "SELECT 'foo' + 'bar' AS value"
  with _qerror("unsupported on strings"):
    _execute_qt(ps2qt(query))["value"]
  query = "SELECT 'foo' > 'bar' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]

def test_string_collation():
  query = "SELECT 'foo' = 'FOO' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [False]
  query = "SELECT 'foo' = 'FOO' COLLATE nocase AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'foo' COLLATE nocase = 'FOO' COLLATE nocase AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'foo' COLLATE nocase = 'FOO' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'xyz123' COLLATE length = 'abcdef' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 'xyz1234' COLLATE length = 'abcdef' AS value"
  assert _execute_qt(ps2qt(query))["value"] == [False]
  with _qerror("non-string"):
    query = "SELECT 1 COLLATE binary = 'blarg'"
    _execute_qt(ps2qt(query))
  with _qerror("invalid use of COLLATE"):
    query = "SELECT 'foo' COLLATE nocase"
    _execute_qt(ps2qt(query))

def test_string_broadcast_comparison():
  q = "SELECT 'abc' AS foo FROM dctv.filled(3) ORDER BY foo"
  assert _execute_qt(ps2qt(q))["foo"] == ["abc", "abc", "abc"]
  q = "SELECT IF(0, 'abc', NULL) AS foo FROM dctv.filled(3) ORDER BY foo"
  assert _execute_qt(ps2qt(q))["foo"] == [N, N, N]

def test_unit_comparison():
  query = "SELECT 1000ms = 1s AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 1s = 1000ms AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]
  query = "SELECT 1000s <=> 1000ms AS value"
  assert _execute_qt(ps2qt(query))["value"] == [False]
  query = "SELECT 10`km/hour` = 10000`meters/hour` AS value"
  assert _execute_qt(ps2qt(query))["value"] == [True]

def test_unit_reduction():
  query = "SELECT 3`MB/minute` * 2hour AS value"
  qt = ps2qt(query)
  assert qt["value"].schema.unit == ureg().MB
  assert _execute_qt(qt)["value"] == [360]
  query = "SELECT 3`MB*hour` / 15minute AS value"
  qt = ps2qt(query)
  assert qt["value"].schema.unit == ureg().MB
  assert _execute_qt(qt)["value"] == [12.0]

def test_unit_invalid():
  query = "SELECT 10`km/hour` = 10000`bytes/second` AS value"
  with pytest.raises(BadUnitsException):
    ps2qt(query)
  query = "SELECT 143`foobar/blabberthing`"
  with pytest.raises(BadUnitsException):
    ps2qt(query)
  query = "SELECT 143`foobar/blabberthing`"
  with pytest.raises(BadUnitsException):
    ps2qt(query)

def test_unit_division():
  assert _qty_eval("100 / 30")

def test_unit_arithmetic():
  assert _qty_eval("1s + 100ms") == ureg().parse_expression("1.1s")
  assert _qty_eval("1s + 100ms") == ureg().parse_expression("1100ms")
  assert _qty_eval("100MB * 30s") == (100 * ureg().MB) * (30 * ureg().second)
  assert _qty_eval("100MB / 30s") == (100 * ureg().MB) / (30 * ureg().second)
  with pytest.raises(BadUnitsException):
    _qty_eval("1s + 300bytes")
  with pytest.raises(BadUnitsException):
    _qty_eval("1s - 300bytes")
  with pytest.raises(BadUnitsException):
    _qty_eval("1s - 5")
  assert _qty_eval("100MB / 2") == 50 * ureg().MB
  assert _qty_eval("2 / 100MB") == 1 / (50*ureg().MB)
  assert _qty_eval("100ns / 50ns") == 2.0

def test_arithmetic_bitwise_width():
  res = pse("CAST(1 AS int32) & CAST(0xf AS int32)",
            as_raw_array=True)
  assert res.dtype == INT32
  res = pse("CAST(1 AS int32) + CAST(0xf AS int32)",
            as_raw_array=True)
  assert res.dtype == INT64

def test_create_and_drop_view():
  dmlctx = DmlContext()
  dmlctx.execute(pdml("CREATE VIEW foo AS SELECT 4 AS bar"))
  with _qerror("already exists"):
    dmlctx.execute(pdml("CREATE VIEW foo AS SELECT 4"))
  dmlctx.execute(pdml("CREATE OR REPLACE VIEW foo AS SELECT 5 AS bar"))
  assert _execute_qt(ps2qt("SELECT * FROM foo", _ns=dmlctx.user_ns)) == \
    dict(bar=[5])
  dmlctx.execute(pdml("DROP foo"))
  with pytest.raises(UnboundReferenceException):
    dmlctx.execute(pdml("DROP foo"))
  with pytest.raises(KeyError):
    dmlctx.execute(pdml("CREATE VIEW bar.bar AS SELECT 6"))
  dmlctx.execute(pdml("CREATE NAMESPACE bar"))
  dmlctx.execute(pdml("CREATE VIEW bar.bar AS SELECT 6"))
  assert _execute_qt(ps2qt("SELECT * FROM bar.bar",
                           _ns=dmlctx.user_ns)) == {"6": [6]}

def test_view_creation_is_lazy():
  dmlctx = DmlContext()
  dmlctx.execute(pdml("CREATE VIEW foo AS SELECT * FROM bar"))
  dmlctx.execute(pdml("CREATE VIEW bar AS SELECT 1 AS qux"))
  assert _execute_qt(ps2qt("SELECT * FROM bar",
                           _ns=dmlctx.user_ns)) == {"qux": [1]}

def test_view_creation_conditional():
  qe = QueryEngine(QueryCache())
  dmlctx = DmlContext(None, qe)
  dmlctx.execute(pdml("CREATE IF (SELECT NULL) VIEW x AS SELECT 1 AS value"))
  dmlctx.execute(pdml("CREATE IF (SELECT 1 AS v WHERE v > 2) VIEW x AS SELECT 1 AS value"))
  dmlctx.execute(pdml("CREATE IF (SELECT 2>1) VIEW x AS SELECT 2 AS value"))
  assert _execute_qt(
    ps2qt("SELECT * FROM x", _ns=dmlctx.user_ns), qe=qe) == {"value": [2]}

def test_view_function():
  dmlctx = DmlContext()
  dmlctx.execute(pdml("CREATE VIEW FUNCTION foo AS SELECT ?+1 AS qux"))
  assert _execute_qt(ps2qt("SELECT * FROM foo(7)",
                           _ns=dmlctx.user_ns)) == {"qux": [8]}

def test_parse_drop():
  assert pdml("DROP foo.bar")[0] == Drop(["foo", "bar"])
  assert pdml("DROP IF EXISTS foo.bar")[0] == \
    Drop(["foo", "bar"], True)

def test_parse_mount_trace():
  assert pdml("MOUNT TRACE 'foo' AS qux.spam.blarg")[0] == \
    MountTrace(trace_file_name="foo",
               mount_path=("qux", "spam", "blarg"))

def test_parse_unit_conversion():
  assert pe("5 IN ms") == UnitConversion(
    IntegerLiteral(5), "ms")

def test_unit_conversion_operator():
  # Integer because we're going to a bigger unit, the source is
  # integral, and the unit ratio is integral.
  assert repr(pse("5`MB/hour` IN `MB/day`")) == "120"
  # Float because we're going to a smaller unit
  assert repr(pse("120`MB/day` IN `MB/hour`")) == "5.0"

def test_unit_conversion_to_dimensionless():
  assert repr(pse("5`MB/hour` IN NULL")) == "5"



def test_parse_common_table_expression_select():
  assert ps("WITH foo AS (SELECT 4 AS x), "
            "bar AS (SELECT 5 AS y) "
            "SELECT x") == \
            SelectWithCte((CteBinding(CteBindingName("foo"),
                                      TableSubquery(ps("SELECT 4 AS x"))),
                           CteBinding(CteBindingName("bar"),
                                      TableSubquery(ps("SELECT 5 AS y")))),
                          ps("SELECT x"))

def test_parse_common_table_expression_table_funcall():
  assert ps("WITH foo AS bar() "
            "SELECT x") == \
            SelectWithCte(
              [CteBinding(CteBindingName("foo"),
                          TableFunctionCall(["bar"]))],
              ps("SELECT x"))

def test_parse_common_table_expression_table_reference():
  assert ps("WITH foo AS (qux.baz) "
            "SELECT x") == \
            SelectWithCte(
              [CteBinding(CteBindingName("foo"),
                          TvfDot(TableReference(["qux"]), "baz"))],
              ps("SELECT x"))

def test_common_table_expression():
  qt = ps2qt("WITH foo AS (SELECT 4 AS x) SELECT foo.x+1 AS z FROM foo")
  assert _execute_qt(qt)["z"] == [5]

def test_common_table_expression_named():
  qt = ps2qt("WITH foo(y) AS (SELECT 4 AS x) SELECT foo.y+1 AS z FROM foo")
  assert _execute_qt(qt)["z"] == [5]

def test_common_table_expression_multiple_bindings():
  q = dedent("""\
  WITH foo AS (SELECT 4 AS x),
       bar AS (SELECT foo.x+1 AS y FROM foo)
  SELECT bar.y+1 AS z FROM bar
  """)
  qt = ps2qt(q)
  assert _execute_qt(qt)["z"] == [6]

def test_common_table_expression_funcall():
  def _bar(x):
    assert x == 2
    return TestQueryTable(
      names=["qux", "bar"],
      columns=[
        [1, 2],
        [3, 4],
      ])
  qt = ps2qt("WITH foo AS bar(2) SELECT foo.qux AS z FROM foo",
             bar=TableValuedFunction(_bar))
  assert _execute_qt(qt)["z"] == [1, 2]



def test_parse_literal_values():
  q = "SELECT * FROM (VALUES (1, 'one'), (2, 'two'), (3, 'three')) AS t"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableSubquery(
        SelectDirect(
          core=TableValues(
            row_values=[
              RowValue([IntegerLiteral(1), StringLiteral("one")]),
              RowValue([IntegerLiteral(2), StringLiteral("two")]),
              RowValue([IntegerLiteral(3), StringLiteral("three")]),
            ])), name="t")))

def test_literal_values():
  q = "SELECT * FROM (VALUES (1, 'one'), (2, 'two'), (3, 'one')) AS t"
  qt = ps2qt(q)
  assert not qt["col0"].schema.is_string
  assert qt["col1"].schema.is_string
  result = _execute_qt(qt, strings=False)
  assert result == {
    "col0": [1, 2, 3],
    "col1": [1, 2, 1],  # String codes
  }

def test_literal_values_negative():
  qt = ps2qt("SELECT * FROM (VALUES (-1), (2), (-3)) AS t")
  assert _execute_qt(qt) == {
    "col0": [-1, 2, -3],
  }
  qt = ps2qt("SELECT * FROM (VALUES (1), (-2), (3), (4)) AS t")
  assert _execute_qt(qt) == {
    "col0": [1, -2, 3, 4],
  }

def test_literal_values_as_select_core():
  q = "VALUES (1, 2), (3, 4) UNION VALUES (1, 2), (5, 6)"
  qt = ps2qt(q)
  result = _execute_qt(qt)
  assert result == {
    "col0": [1, 3, 5],
    "col1": [2, 4, 6],
  }

def test_literal_values_named_alias():
  q = "SELECT * FROM (VALUES (1, 2), (3, 4), (5, 6)) AS v(foo, bar)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == {
    "foo": [1, 3, 5],
    "bar": [2, 4, 6],
  }

def test_literal_values_cte():
  q = "WITH v AS (VALUES (1, 2), (3, 4)) SELECT v.* FROM v"
  qt = ps2qt(q)
  assert _execute_qt(qt) == {
    "col0": [1, 3],
    "col1": [2, 4],
  }

def test_literal_values_cte_names():
  q = "WITH v(foo, bar) AS (VALUES (1, 2), (3, 4)) SELECT v.* FROM v"
  qt = ps2qt(q)
  assert _execute_qt(qt) == {
    "foo": [1, 3],
    "bar": [2, 4],
  }

def test_literal_values_length_mismatch():
  with _qerror():
    ps2qt("SELECT * FROM (VALUES (1, 'one'), (2), (3, 'three'))")

def test_literal_values_type_mismatch():
  # N.B. legally allowed to fail either during query parsing or during
  # query execution, so we put _execute_qt *and* ps2qt inside the
  # _qerror().
  with _qerror():
    _execute_qt(
      ps2qt("SELECT * FROM (VALUES (1, 'one'), ('two', 2), (3, 'three'))"))

def test_literal_values_null():
  q = "VALUES ('foo', 2), ('bar', NULL), (NULL, 4)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == {
    "col0": ["foo", "bar", N],
    "col1": [2, N, 4],
  }

def test_literal_values_mixed_type():
  q = "VALUES (3.14), (4), (NULL)"
  qt = ps2qt(q)
  v = _execute_qt(qt)["col0"]
  assert v == [3.14, 4.0, N]
  assert list(map(type, v)) == [np.float64, np.float64, NoneType]



def test_parse_group_partition():
  q = "SELECT * FROM foo GROUP SPANS USING PARTITIONS"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanPartition(intersect=True)))
  q = "SELECT * FROM foo GROUP AND INTERSECT SPANS USING PARTITIONS"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanPartition(intersect=True)))
  q = "SELECT * FROM foo GROUP AND UNION SPANS USING PARTITIONS"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanPartition(intersect=False)))

def test_group_partition_basic():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT16],
    rows=[
      [ 0,  10,  1, 5],
      [ 0,  10,  2, 6],
    ],
    table_schema=_span_table_time_major("part"))
  assert spans["value1"].schema.dtype == np.dtype("h")
  query = dedent("""\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    FROM xspans GROUP SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64],
    rows=[
      [ 0,  10,  11],
    ], table_schema=_span_table_time_major())

def test_group_partition_repartition():
  spans = TestQueryTable(
    names=("_ts", "_end", "part", "value1", "value2"),
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT16, INT8],
    rows=[
      [ 0,  10,  1, 5, -7],
      [ 0,  10,  2, 6, -7],
      [ 0,  10,  3, 9, -8],
    ],
    table_schema=_span_table_time_major("part"))
  assert spans["value1"].schema.dtype == np.dtype("h")
  query = dedent("""\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    FROM xspans GROUP AND UNION SPANS USING PARTITIONS
    AND PARTITION BY value2
  """)
  qt = ps2qt(query, xspans=spans).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "value2", "sum_v1"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT8, INT64],
    rows=[
      [ 0,  10,  -8,  9],
      [ 0,  10,  -7, 11],
    ], table_schema=_span_table_time_major("value2"))

def test_group_partition_functions():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT16],
    rows=[
      [ 0,  10,  1, 5],
      [ 0,  10,  2, 6],
    ],
    table_schema=_span_table_time_major("part"))
  assert spans["value1"].schema.dtype == np.dtype("h")
  query = """\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    PROD(value1) AS prod_v1,
    MIN(value1) AS min_v1,
    MAX(value1) AS max_v1,
    dctv.FIRST(value1) AS first_v1,
    dctv.UNIQUE_MASK(value1) AS unique_v1,
    COUNT(value1) AS count_v1,
    COUNT(*) AS count_star,
    FROM xspans GROUP SPANS USING PARTITIONS
  """
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration",
           "sum_v1", "prod_v1",
           "min_v1", "max_v1",
           "first_v1", "unique_v1",
           "count_v1", "count_star"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA,
             INT64, INT64,
             INT16, INT16,
             INT16, BOOL,
             INT64, INT64],
    rows=[
      [ 0,  10,
        11, 30,
        5, 6,
        5, True,
        2, 2],
    ], table_schema=_span_table_time_major())

def test_group_partition_overlap_intersect():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1,  5],
      [ 5,  10,  2,  6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN COUNT(*) as cstar, COUNT(value1) AS cv1
  FROM xspans GROUP AND INTERSECT SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "cstar", "cv1"],
    rows=[
      [ 5,   5,  2,  2],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_group_partition_overlap_union():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1,  5],
      [ 5,  10,  2,  6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN SUM(value1) AS s,
              COUNT(*) as cstar,
              COUNT(value1) AS cv1,
  FROM xspans GROUP AND UNION SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "s", "cstar", "cv1"],
    rows=[
      [ 0,   5,  5,  2,  1],
      [ 5,   5, 11,  2,  2],
      [10,   5,  6,  2,  1],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_group_partition_where():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1,  5],
      [ 5,  10,  2,  6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN SUM(value1) AS s
  FROM xspans
  WHERE xspans.value1 = 6
  GROUP SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "s"],
    rows=[
      [ 5,  10, 6],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = dedent("""\
  SELECT SPAN SUM(value1) AS s
  FROM xspans
  WHERE xspans._ts = 0ns
  GROUP SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "s"],
    rows=[
      [ 0, 10, 5],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = dedent("""\
  SELECT SPAN SUM(value1) AS s
  FROM xspans
  WHERE _ts = 0ns
  GROUP SPANS USING PARTITIONS
  """)
  with _qerror("invalid use of magic group metadata"):
    ps2qt(query, xspans=spans)

  query = dedent("""\
  SELECT SPAN SUM(value1) AS s,
  FROM xspans GROUP AND UNION SPANS USING PARTITIONS
  HAVING _ts = 10ns
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "s"],
    rows=[
      [10,   5,  6],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_group_partition_complex_overlap_intersect():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part"),
    rows=[
      [ 0, 30,  1],
      [ 2,  8,  3],
      [ 5,  1,  2],
      [ 8,  3,  2],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN FROM xspans GROUP SPANS USING PARTITIONS""")
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration"],
    rows=[
      [ 5,  1],
      [ 8,  2],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_group_partition_complex_overlap_union():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part"),
    rows=[
      [ 0, 30,  1],
      [ 2,  8,  3],
      [ 5,  1,  2],
      [ 8,  3,  2],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN FROM xspans GROUP AND UNION SPANS USING PARTITIONS""")
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration"],
    rows=[
      [ 0,  2],
      [ 2,  3],
      [ 5,  1],
      [ 6,  2],
      [ 8,  2],
      [10,  1],
      [11, 19],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_group_partition_having():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1, 5],
      [ 1,  10,  2, 6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    FROM xspans GROUP AND UNION SPANS USING PARTITIONS
    HAVING COUNT(*) = COUNT(value1)
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [ 1,  9,  11],
    ]).as_dict()

def test_group_partition_missing_aggregations():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1, 5],
      [ 1,  10,  2, 6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    PROD(value1) AS prod_v1,
    MIN(value1) AS min_v1,
    MAX(value1) AS max_v1,
    FROM xspans GROUP AND UNION SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1", "prod_v1", "min_v1", "max_v1"],
    rows=[
      [ 0,  1,   5,  5,  5,  5],
      [ 1,  9,  11, 30,  5,  6],
      [10,  1,   6,  6,  6,  6],
    ]).as_dict()

def test_group_partition_null_aggregations():
  spans = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 0,  10,  1, N],
      [ 0,  10,  2, 6],
    ],
    table_schema=_span_table_time_major("part"))
  query = dedent("""\
  SELECT SPAN
    SUM(value1) AS sum_v1,
    PROD(value1) AS prod_v1,
    MIN(value1) AS min_v1,
    MAX(value1) AS max_v1,
    FROM xspans GROUP AND UNION SPANS USING PARTITIONS
  """)
  qt = ps2qt(query, xspans=spans)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1", "prod_v1", "min_v1", "max_v1"],
    rows=[
      [ 0, 10,   6,  6,  6,  6],
    ]).as_dict()

def test_parse_group_partition_columns():
  q = "SELECT * FROM foo GROUP SPANS USING PARTITIONS"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanPartition()))



def test_parse_span_group():
  q = "SELECT * FROM foo GROUP SPANS INTO SPANS bar"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanGrouper(TableReference(["bar"]), "span")))
  q = "SELECT * FROM foo GROUP AND INTERSECT SPANS INTO SPANS bar"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanGrouper(TableReference(["bar"]), "span",
                            intersect=True)))
  q = "SELECT * FROM foo GROUP AND UNION SPANS INTO SPANS bar"
  assert ps(q) == SelectDirect(
    core=RegularSelect(
      columns=[WildcardColumn()],
      from_=TableReference(["foo"]),
      gb=GroupBySpanGrouper(TableReference(["bar"]), "span",
                            intersect=False)))

def test_span_group_simple():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  1,  5],
      [ 1,  1,  6],
      [ 2,  1,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  2],
      [ 2,  1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [0,  2,  11],
      [2,  1,   7],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

@pict_parameterize(dict(
  grouped_sort=["none", "time_major", "partition_major"],
  grouper_sort=["none", "time_major", "partition_major"],
  part_dtype_code=["i", "L", "b", "l", "g"],
))
def test_span_group_pp(part_dtype_code,
                       grouper_sort,
                       grouped_sort):
  no_sort = TableSchema(TableKind.SPAN, "part", TableSorting.NONE)
  part_dtype = np.dtype(part_dtype_code)

  grouper_rows = [
    [ 0,  2,  0],
    [ 1,  2,  1],
    [ 3,  2,  1],
    [ 5,  2,  1],
    [ 6,  2,  0],
    [12,  2,  0],
    [18,  2,  0],
  ]

  grouped_rows = [
    [ 1,  6,  0,  5],
    [ 1,  2,  1,  1],
    [ 3,  4,  1,  2],
    [ 7,  2,  0,  4],
    [16,  6,  0,  7],
  ]

  rng = _make_rng()
  rng.shuffle(grouper_rows)
  rng.shuffle(grouped_rows)

  spans_grouper = TestQueryTable(
    names=("_ts", "_duration", "part"),
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype),
    rows=grouper_rows,
    table_schema=no_sort)
  spans_grouper = spans_grouper.to_schema(
    sorting=TableSorting(grouper_sort))

  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype, INT64),
    rows=grouped_rows,
    table_schema=no_sort)
  spans_grouped = spans_grouped.to_schema(
    sorting=TableSorting(grouped_sort))

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  if part_dtype_code == "g":
    with pytest.raises(TypeError):
      _execute_qt(qt)
    return

  assert _execute_qt(
    qt.to_time_major()) == TestQueryTable(
      names=["_ts", "_duration", "part", "sum_v1"],
      schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype, INT64),
      rows=[
        [ 0,  2,  0,  5],
        [ 1,  2,  1,  1],
        [ 3,  2,  1,  2],
        [ 5,  2,  1,  2],
        [ 6,  2,  0,  9],
        [12,  2,  0,  N],
        [18,  2,  0,  7],
      ],
      table_schema=_span_table_time_major("part"))

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND INTERSECT SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype, INT64),
    rows=[
      [ 0,  2,  0,  5],
      [ 1,  2,  1,  1],
      [ 3,  2,  1,  2],
      [ 5,  2,  1,  2],
      [ 6,  2,  0,  9],
      [18,  2,  0,  7],
    ], table_schema=_span_table_time_major("part"))

def test_span_group_pp_out_of_order():
  spans_grouper = TestQueryTable(
    names=("_ts", "_end", "part"),
    rows=[
      [ 0, 10,  0],
      [ 5,  7,  1],
    ],
    table_schema=_span_table_time_major("part"))
  spans_grouped = TestQueryTable(
    names=("_ts", "_end", "part", "value1"),
    rows=[
      [ 0,  10,  0,  5],
      [ 5,   7,  1,  7],
    ],
    table_schema=_span_table_time_major("part"))

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    rows=[
      [ 0, 10, 0, 5],
      [ 5,  2, 1, 7],
    ], table_schema=_span_table_time_major("part"))

def test_span_group_up():
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  2],  # [ 0,  2)
      [ 2,  2],  # [ 2,  4)
      [ 6,  2],  # [ 6,  8)
      [12,  2],  # [12, 14)
      [18,  2],  # [18, 20)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 1,  6,  0,  5],  # 0: [ 1,  7)
      [ 1,  2,  1,  1],  # 1: [ 1,  3)
      [ 3,  4,  1,  2],  # 1: [ 3,  7)
      [ 7,  2,  0,  4],  # 0: [ 7,  9)
      [16,  6,  0,  7],  # 0: [16, 22)
    ],
    table_schema=_span_table_time_major("part"))

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    rows=[
      [ 0,  2,  0,  5],  # 0: [ 0,  2)
      [ 0,  2,  1,  1],  # 1: [ 0,  2)
      [ 2,  2,  0,  5],  # 0: [ 2,  4)
      [ 2,  2,  1,  3],  # 1: [ 2,  4)
      [ 6,  2,  0,  9],  # 0: [ 6,  8)
      [ 6,  2,  1,  2],  # 1: [ 6,  8)
      [12,  2,  0,  N],  # 0: [12, 14)
      [12,  2,  1,  N],  # 1: [12, 14)
      [18,  2,  0,  7],  # 0: [18, 20)
      [18,  2,  1,  N],  # 1: [18, 20)
    ], table_schema=_span_table_time_major("part"))

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND INTERSECT SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    rows=[
      [ 0,  2,  0,  5],  # 0: [ 0,  2)
      [ 0,  2,  1,  1],  # 1: [ 0,  2)
      [ 2,  2,  0,  5],  # 0: [ 2,  4)
      [ 2,  2,  1,  3],  # 1: [ 2,  4)
      [ 6,  2,  0,  9],  # 0: [ 6,  8)
      [ 6,  2,  1,  2],  # 1: [ 6,  8)
      [18,  2,  0,  7],  # 0: [18, 20)
    ], table_schema=_span_table_time_major("part"))

def test_span_group_total_size():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  1,  5],
      [ 1,  1,  6],
      [ 2,  1,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  2],
      [ 2,  1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN 1+SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [0,  2,  12],
      [2,  1,   8],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_pu():
  spans_grouper = TestQueryTable(
    names=("_ts", "_end", "part"),
    rows=[
      [ 0,  2,  0],
      [ 2,  4,  0],
      [ 2,  4,  1],
      [ 6,  8,  0],
      [12, 14,  1],
      [18, 20,  1],
    ],
    table_schema=_span_table_time_major("part"))
  spans_grouped = TestQueryTable(
    names=("_ts", "_end", "value1"),
    rows=[
      [ 1,  7,  5],
      [ 7,  9,  4],
      [16, 22,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "part", "sum_v1"],
    rows=[
      [ 0,  2,  0,  5],
      [ 2,  4,  0,  5],
      [ 2,  4,  1,  5],
      [ 6,  8,  0,  9],
      [12, 14,  1,  N],
      [18, 20,  1,  7],
    ], table_schema=_span_table_time_major("part"))


def test_span_group_total_size_2():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  1,  5],
      [ 1,  1,  6],
      [ 2,  1,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  2],
      [ 2,  1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN 1+SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [0,  2,  12],
      [2,  1,   8],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_count():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  1,  N],
      [ 1,  1,  6],
      [ 2,  1,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  2],
      [ 2,  1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN COUNT(*) AS cs, COUNT(value1) AS cv1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "cs", "cv1"],
    rows=[
      [0,  2,  2, 1],
      [2,  1,  1, 1],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_crossing():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 0,  3,  5],
      [ 5,  5,  6],
      [11,  5,  7],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  6],
      [ 9,  3],
      [20,  1],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [ 0,  6, 11],
      [ 9,  3, 13],
      [20,  1,  N],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND INTERSECT SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [ 0,  6, 11],
      [ 9,  3, 13],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_intersected():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 1,  3,  5],  # span is [1, 4)
      [ 5,  2,  6],  # span is [5, 7)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND INTERSECT SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [1,  5, 11],
      [6,  4,  6],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_union():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 1,  3,  5],  # span is [1, 4)
      [ 5,  2,  6],  # span is [5, 7)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND UNION SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [ 0,  1,  N],
      [ 1,  5, 11],
      [ 6,  4,  6],
      [10,  2,  N],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_grouped_operator():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 1,  3,  5],  # span is [1, 4)
      [ 5,  2,  6],  # span is [5, 7)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1+0) AS sum_v1
  FROM spans_grouped
  GROUP AND UNION SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_v1"],
    rows=[
      [ 0,  1,  N],
      [ 1,  5, 11],
      [ 6,  4,  6],
      [10,  2,  N],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_span_group_special_column_resolution():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "value1"),
    rows=[
      [ 1,  3,  5],  # span is [1, 4)
      [ 5,  2,  6],  # span is [5, 7)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN
              SUM(spans_grouped._ts) AS sum_ts2,
              SUM(_ts) AS sum_ts,
  FROM spans_grouped
  GROUP AND UNION SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             spans_grouped=spans_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "sum_ts2", "sum_ts"],
    rows=[
      [ 0,  1,  N,  N],
      [ 1,  5,  6,  6],
      [ 6,  4,  5,  5],
      [10,  2,  N,  N],
    ]).as_dict()

def test_span_group_intersected_partitioned():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 1,  3,  1,  5],  # span is [1, 4)
      [ 2,  2,  2, -2],  # span is [2, 4)
      [ 5,  2,  1,  6],  # span is [5, 7)
    ],
    table_schema=_span_table_time_major("part"))
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND INTERSECT SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    rows=[
      [ 1,  5,  1, 11],
      [ 1,  5,  2, -2],
      [ 6,  4,  1,  6],
    ], table_schema=_span_table_time_major("part"))

def test_span_group_union_partitioned():
  spans_grouped = TestQueryTable(
    names=("_ts", "_duration", "part", "value1"),
    rows=[
      [ 1,  3,  1,  5],  # span is [1, 4)
      [ 2,  2,  2, -2],  # span is [2, 4)
      [ 5,  2,  1,  6],  # span is [5, 7)
    ],
    table_schema=_span_table_time_major("part"))
  spans_grouper = TestQueryTable(
    names=("_ts", "_duration"),
    rows=[
      [ 0,  1],  # span is [0, 1)
      [ 1,  5],  # span is [1, 6)
      [ 6,  4],  # span is [6, 4)
      [10,  2],  # span is [10, 2)
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM spans_grouped
  GROUP AND UNION SPANS INTO SPANS spans_grouper
  """
  qt = ps2qt(
    q,
    spans_grouped=spans_grouped,
    spans_grouper=spans_grouper
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_duration", "part", "sum_v1"],
    rows=[
      [ 0,  1,  1,  N],
      [ 0,  1,  2,  N],
      [ 1,  5,  1, 11],
      [ 1,  5,  2, -2],
      [ 6,  4,  1,  6],
      [ 6,  4,  2,  N],
      [10,  2,  1,  N],
      [10,  2,  2,  N],
    ], table_schema=_span_table_time_major("part"))


# Bind arguments

def test_parse_bind():
  assert pe(":foo") == BindParameter("foo")
  assert pe("?2") == BindParameter(2)
  # Do it twice to make sure numbering doesn't leak across paraes
  for _ in range(2):
    assert pe("foo(?, ?)") == FunctionCall(
      ["foo"],
      [BindParameter(0), BindParameter(1)])

def test_bind():
  assert pse(":foo", _args=qargs(foo=5)) == 5
  assert pse("?+?", _args=qargs(1, 5)) == 6
  assert pse("?1+?1", _args=qargs(1, 5)) == 10
  assert pse("?", _args=qargs("foo")) == "foo"

def test_bind_complex_expression():
  assert pse("(:foo - :bar) + 1",
             _args=qargs(foo=5, bar=2)) == 4

def test_bind_table():
  query = "SELECT * FROM ?"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[[1, 2]])
  qt = ps2qt(query, _args=qargs(tbl))
  assert _execute_qt(qt) == tbl.as_dict()

def test_bind_table_numbered():
  query = "SELECT * FROM ?0"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[[1, 2]])
  qt = ps2qt(query, _args=qargs(tbl))
  assert _execute_qt(qt) == tbl.as_dict()

def test_bind_tvf():
  query = "SELECT * FROM my_function(foo=>?, bar=>:bar)"
  tbl = TestQueryTable(
    names=["foo", "bar"],
    rows=[])
  def _my_function(foo, bar):
    assert foo is tbl
    assert bar is tbl
    return foo
  qt = ps2qt(query,
             _args=qargs(tbl, bar=tbl),
             my_function=TableValuedFunction(_my_function))
  assert _execute_qt(qt) == tbl.as_dict()

def test_bind_tvf_complex_expression():
  query = "SELECT * FROM my_function((:foo + :bar)//2)"
  foo = 7
  bar = 2
  my_function_called = False
  def _my_function(value):
    nonlocal my_function_called
    my_function_called = True
    assert value == (foo+bar)//2
    return FilledQueryTable()
  _execute_qt(ps2qt(query,
                    _args=qargs(foo=foo, bar=bar),
                    my_function=TableValuedFunction(_my_function)))
  assert my_function_called

def test_row_number():
  q = "SELECT ROW_NUMBER() AS rnr FROM dctv.filled(3)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == dict(rnr=[0, 1, 2])

# TODO(dancol): deprecate dctv.filled in favor of magic unit function
def test_filled_tvf():
  q = "SELECT 5 AS value FROM dctv.filled(3)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == dict(value=[5, 5, 5])
  assert not qt["value"].schema.unit
  q = "SELECT 5ns AS value FROM dctv.filled(3)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == dict(value=[5, 5, 5])
  assert qt["value"].schema.unit == ureg().ns

def test_filled_tvf_queries():
  q = "SELECT 5ns AS value FROM dctv.filled((SELECT 3))"
  qt = ps2qt(q)
  assert _execute_qt(qt) == dict(value=[5, 5, 5])
  assert qt["value"].schema.unit == ureg().ns


# Casting

def test_cast():
  # pylint: disable=compare-to-zero,unidiomatic-typecheck
  with _qerror(BadCastException):
    pse("CAST(4.1 AS int32)")
  assert pse("CAST(4.1 AS int32 UNSAFE)") == 4
  assert pse("CAST(4.1 AS INT32 UNSAFE)") == 4
  with _qerror(BadCastException):
    pse("CAST('foo' AS int8)")
  with _qerror(BadCastException):
    pse("CAST(4 AS bool)")
  assert pse("CAST(4 AS bool UNSAFE)") is NP_TRUE
  assert pse("CAST(0 AS bool UNSAFE)") is NP_FALSE
  assert pse("CAST(4 AS float)") == 4.0
  assert type(pse("CAST(4 AS float)")) is np.float64

  with _qerror(BadCastException):
    pse("CAST(9223372036854775807 AS float)")
  with _qerror(BadCastException):
    pse("CAST(-1 AS uint64)")

  assert pse("CAST(9223372036854775807 AS float UNSAFE)") == \
    9.223372036854776e+18

def test_cast_overflow():
  assert pse("CAST(12345 AS int8 UNSAFE)") == 12345 % 256
  assert pse("CAST(-12345 AS int8 UNSAFE)") == -(12345 % 256)

def test_cast_unit():
  assert pse("CAST(4096 AS UNIT bytes) IN kibibytes") == 4


# Non-aggregate functions

def test_unary_functions():
  assert pse("FLOOR(4.1)") == 4.0
  assert pse("FLOOR(-2.5)") == -3.0
  assert pse("trunc(-2.5)") == -2.0
  assert pse("ceilING(4.1)") == 5.0
  assert pse("round(3.1)") == 3.0
  assert pse("round(3.7)") == 4.0

  # Round to even
  assert pse("round(4.5)") == 4.0
  assert pse("round(3.5)") == 4.0

def test_coalesce():
  assert pse("COALESCE(4, 3)") == 4
  assert pse("COALESCE(NULL, 3)") == 3
  assert pse("COALESCE(NULL, NULL)") is None

def test_coalesce_mismatch():
  with _qerror():
    pse("COALESCE('foo', -1)")

def test_coalesce_strings():
  pse("COALESCE('foo', 'bar')") == "foo"
  pse("COALESCE('foo', NULL)") == "foo"


# Event tables and their operations

def test_parse_select_event():
  assert ps("SELECT EVENT FROM foo") == SelectDirect(
    core=RegularSelect(
      kind="event",
      columns=[],
      from_=TableReference(["foo"]),))

def test_select_event_unpartitioned():
  evtbl = TestQueryTable(
    names=("_ts",  "value1"),
    rows=[
      [ 0,  -5],
      [ 5, -10],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  q = "SELECT EVENT FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition is None
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts",),
    rows=[
      [ 0],
      [ 5],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR).as_dict()
  q = "SELECT EVENT value1 FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition is None
  assert _execute_qt(qt) == evtbl.as_dict()
  q = "SELECT EVENT * FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition is None
  assert _execute_qt(qt) == evtbl.as_dict()

def test_select_event_partitioned():
  evtbl = TestQueryTable(
    names=("_ts",  "part", "value1"),
    rows=[
      [ 0, 3,  -5],
      [ 5, 4, -10],
    ],
    table_schema=_event_table_time_major("part"))
  q = "SELECT EVENT FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition == "part"
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "part"),
    rows=[
      [ 0,  3],
      [ 5,  4],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR).as_dict()
  q = "SELECT EVENT value1 FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition == "part"
  assert _execute_qt(qt) == evtbl.as_dict()
  q = "SELECT EVENT * FROM evtbl"
  qt = ps2qt(q, evtbl=evtbl)
  assert qt.table_schema.kind == TableKind.EVENT
  assert qt.table_schema.partition == "part"
  assert _execute_qt(qt) == evtbl.as_dict()

def test_rename_partition_column():
  evtbl = TestQueryTable(
    names=["_ts",  "part", "value1"],
    rows=[
      [ 0, 3,  -5],
      [ 5, 4, -10],
    ],
    table_schema=_event_table_time_major("part"))
  q = "SELECT EVENT * FROM evtbl AS x(part=>z)"
  assert ps2qt(q, evtbl=evtbl).table_schema.partition == "z"
  q = "SELECT EVENT * FROM evtbl AS x(part=>NULL)"
  with _qerror("cannot delete partition column 'part'"):
    ps2qt(q, evtbl=evtbl)
  q = "SELECT EVENT * FROM evtbl AS x(value1=>part)"
  with _qerror("cannot overwrite partition column "
               "'part' with column 'value1'"):
    ps2qt(q, evtbl=evtbl)

def test_select_event_repartition():
  evtbl = TestQueryTable(
    names=["_ts",  "part", "value1"],
    rows=[
      [ 0, 3,  -5],
      [ 5, 4, -10],
    ],
    table_schema=_event_table_time_major("part"))
  q = "SELECT EVENT PARTITIONED BY value1 part FROM evtbl"
  assert _execute_qt(ps2qt(q, evtbl=evtbl)) == \
    TestQueryTable(
      names=["_ts",  "value1", "part"],
      rows=[
        [ 0, -5,  3],
        [ 5, -10, 4],
      ],
      table_schema=TableSchema(TableKind.EVENT,
                               partition="value1",
                               sorting=TableSorting.NONE))

  q = "SELECT EVENT PARTITIONED BY value1 * FROM evtbl"
  assert _execute_qt(ps2qt(q, evtbl=evtbl)) == \
    TestQueryTable(
      names=["_ts",  "value1", "part"],
      rows=[
        [ 0, -5,  3],
        [ 5, -10, 4],
      ],
      table_schema=TableSchema(TableKind.EVENT,
                               partition="value1",
                               sorting=TableSorting.NONE))

  q = "SELECT EVENT PARTITIONED BY NULL FROM evtbl"
  assert ps2qt(q, evtbl=evtbl).table_schema == \
    TableSchema(TableKind.EVENT,
                partition=None,
                sorting=TableSorting.NONE)
  q = "SELECT EVENT PARTITIONED BY fhqwhgads FROM evtbl"
  with _qerror():
    ps2qt(q, evtbl=evtbl)

def test_parse_event_join():
  assert ps("SELECT a FROM foo EVENT INNER JOIN SPANS bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(left=TableReference(["foo"]),
                 op="inner",
                 right=TableReference(["bar"]),
                 kind="event join"),
    ))
  assert ps("SELECT a FROM foo EVENT LEFT OUTER JOIN SPANS bar") == SelectDirect(
    core=RegularSelect(
      columns=[ExpressionColumn(ColumnReference("a"))],
      from_=Join(left=TableReference(["foo"]),
                 op="left",
                 right=TableReference(["bar"]),
                 kind="event join")))

def test_event_join_span_unpartitioned():
  evtbl = TestQueryTable(
    names=("_ts",  "eval"),
    rows=[
      [ 0,  -5],
      [ 5, -10],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  spantbl = TestQueryTable(
    names=("_ts", "_duration", "sval"),
    rows=[
      [ 4,  3,  -4],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT INNER JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "eval", "sval"),
    rows=[
      [ 5, -10,  -4],
    ]).as_dict()

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT LEFT JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "eval", "sval"),
    rows=[
      [ 0,  -5,   N],
      [ 5, -10,  -4],
    ]).as_dict()

def test_event_join_span_boundary():
  evtbl = TestQueryTable(
    names=("_ts",  "eval"),
    rows=[
      [ 6,  60],
      [ 7,  70],
      [ 8,  80],
      [ 9,  90],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  spantbl = TestQueryTable(
    names=("_ts", "_duration", "sval"),
    rows=[
      [ 1,  6,  -5],
      [ 7,  2,  -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT INNER JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "eval", "sval"),
    rows=[
      [ 6, 60, -5],
      [ 7, 70, -6],
      [ 8, 80, -6],
    ]).as_dict()

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT LEFT JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl)
  assert qt.table_schema == EVENT_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "eval", "sval"),
    rows=[
      [ 6, 60, -5],
      [ 7, 70, -6],
      [ 8, 80, -6],
      [ 9, 90,  N],
    ]).as_dict()

def test_event_join_partition_both():
  evtbl = TestQueryTable(
    names=("_ts",  "eval", "part"),
    rows=[
      [ 6,  60,  0],
      [ 7,  70,  1],
      [ 8,  80,  0],
      [ 9,  90,  1],
    ],
    table_schema=_event_table_time_major("part"))
  spantbl = TestQueryTable(
    names=("_ts", "_duration", "part", "sval"),
    rows=[
      [ 6,  4,  1, -5],
    ],
    table_schema=_span_table_time_major("part"))

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT LEFT JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "part", "eval", "sval"),
    rows=[
      [ 6,  0, 60,  N],
      [ 7,  1, 70, -5],
      [ 8,  0, 80,  N],
      [ 9,  1, 90, -5],
    ], table_schema=_event_table_time_major("part"))

def test_event_broadcast():
  evtbl = TestQueryTable(
    names=("_ts",  "eval"),
    rows=[
      [ 6,  60],
      [ 7,  70],
      [ 8,  80],
      [ 9,  90],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
  spantbl = TestQueryTable(
    names=("_ts", "_duration", "part", "sval"),
    rows=[
      [ 5,  3,  0, -5],
      [ 7,  4,  1, -6],
    ],
    table_schema=_span_table_time_major("part"))

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT LEFT JOIN SPANS spantbl"""
  with _qerror("cannot join non-partitioned"):
    ps2qt(query, evtbl=evtbl, spantbl=spantbl)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT RIGHT BROADCAST INTO SPAN PARTITIONS spantbl"""
  with _qerror("not possible to condense a span into a point"):
    ps2qt(query, evtbl=evtbl, spantbl=spantbl)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT OUTER BROADCAST INTO SPAN PARTITIONS spantbl"""
  with _qerror("not possible to condense a span into a point"):
    ps2qt(query, evtbl=evtbl, spantbl=spantbl)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT BROADCAST INTO SPAN PARTITIONS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "part", "eval", "sval"),
    rows=[
      [ 6,  0, 60, -5],
      [ 7,  0, 70, -5],
      [ 7,  1, 70, -6],
      [ 8,  1, 80, -6],
      [ 9,  1, 90, -6],
    ], table_schema=_event_table_time_major("part"))

def test_event_join_partition_event():
  evtbl = TestQueryTable(
    names=("_ts",  "eval", "part"),
    rows=[
      [ 6,  60,  0],
      [ 7,  70,  1],
      [ 8,  80,  0],
      [ 9,  90,  1],
    ],
    table_schema=_event_table_time_major("part"))
  spantbl = TestQueryTable(
    names=("_ts", "_duration", "sval"),
    rows=[
      [ 6,  4,  -5],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT INNER JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "part", "eval", "sval"),
    rows=[
      [ 6,  0, 60, -5],
      [ 7,  1, 70, -5],
      [ 8,  0, 80, -5],
      [ 9,  1, 90, -5],
    ], table_schema=_event_table_time_major("part"))

  query = """SELECT EVENT eval, sval
  FROM evtbl EVENT LEFT JOIN SPANS spantbl"""
  qt = ps2qt(query, evtbl=evtbl, spantbl=spantbl).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "part", "eval", "sval"),
    rows=[
      [ 6,  0, 60, -5],
      [ 7,  1, 70, -5],
      [ 8,  0, 80, -5],
      [ 9,  1, 90, -5],
    ], table_schema=_event_table_time_major("part"))

def test_event_group_uu():
  spans_grouper = TestQueryTable(
    names=("_ts", "_end"),
    rows=[
      [ 0,  2],
      [ 3,  5],
      [ 6,  8],
      [12, 14],
      [18, 20],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  events_grouped = TestQueryTable(
    names=("_ts", "value1"),
    rows=[
      [ 1,  1],
      [ 1,  5],
      [ 3,  2],
      [ 5,  9],
      [ 7,  4],
      [16,  7],
      [20,  1],
    ],
    table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)

  q = """\
  SELECT SPAN SUM(value1) AS sum_v1
  FROM events_grouped
  GROUP EVENTS INTO SPANS spans_grouper
  """
  qt = ps2qt(q,
             events_grouped=events_grouped,
             spans_grouper=spans_grouper)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "sum_v1"],
    rows=[
      [ 0,  2,  6],
      [ 3,  5,  2],
      [ 6,  8,  4],
      [12, 14,  N],
      [18, 20,  N],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)



def test_with_all_partitions_1():
  spn = TestQueryTable(
    names=("_ts", "_end", "part", "value1"),
    rows=[
      [ 0,  30,  1, -1],
      [10,  30,  2, -2],
      [20,  30,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))
  q = "SELECT SPAN * FROM dctv.with_all_partitions(spn)"
  qt = ps2qt(
    q, spn=spn
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "part", "value1"],
    rows=[
      [20, 30,  1, -1],
      [20, 30,  2, -2],
      [20, 30,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))

def test_with_all_partitions_2():
  spn = TestQueryTable(
    names=("_ts", "_end", "part", "value1"),
    rows=[
      [ 0,  30,  1, -1],
      [10,  29,  2, -2],
      [20,  28,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))
  q = "SELECT SPAN * FROM dctv.with_all_partitions(spn)"
  qt = ps2qt(q, spn=spn)
  assert _execute_qt(
    qt.to_time_major()
  ) == TestQueryTable(
    names=["_ts", "_end", "part", "value1"],
    rows=[
      [20, 28,  1, -1],
      [20, 28,  2, -2],
      [20, 28,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))

def test_with_all_partitions_3():
  spn = TestQueryTable(
    names=("_ts", "_end", "part", "value1"),
    rows=[
      [ 0,  30,  1, -1],
      [10,  20,  2, -2],
      [15,  25,  3, -3],
      [22,  27,  2, -4],
    ],
    table_schema=_span_table_time_major("part"))
  q = "SELECT SPAN * FROM dctv.with_all_partitions(spn)"
  qt = ps2qt(
    q, spn=spn
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "part", "value1"],
    rows=[
      [15, 20,  1, -1],
      [15, 20,  2, -2],
      [15, 20,  3, -3],
      [22, 25,  1, -1],
      [22, 25,  2, -4],
      [22, 25,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))

def test_with_all_partitions_4():
  spn = TestQueryTable(
    names=("_ts", "_end", "part", "value1"),
    rows=[
      [ 0,  30,  1, -1],
      [10,  20,  2, -2],
      [15,  25,  3, -3],
      [22,  27,  2, -4],
    ],
    table_schema=_span_table_time_major("part"))
  q = "SELECT SPAN * FROM dctv.with_all_partitions(spn)"
  qt = ps2qt(
    q, spn=spn
  ).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "part", "value1"],
    rows=[
      [15, 20,  1, -1],
      [15, 20,  2, -2],
      [15, 20,  3, -3],
      [22, 25,  1, -1],
      [22, 25,  2, -4],
      [22, 25,  3, -3],
    ],
    table_schema=_span_table_time_major("part"))



def test_greatest_least():
  pse("GREATEST(1, 2)") == 2
  pse("GREATEST(4, 5, 3, 9, 2)") == 9
  pse("GREATEST(2)") == 2
  pse("LEAST(1, 2)") == 1
  pse("LEAST(2)") == 2
  with _qerror("unit mismatch"):
    pse("GREATEST(1ms, 1hour)")

  assert pse("LEAST(NULL, NULL, NULL)") is None
  assert pse("LEAST(1, NULL, 2)") == 1
  assert pse("LEAST(NULL, 2, 5)") == 2
  assert pse("LEAST(NULL, 5, 2)") == 2
  assert pse("LEAST(2, 5, NULL)") == 2
  assert pse("LEAST(5, 2, NULL)") == 2

  assert pse("GREATEST(1, NULL, 2)") == 2
  assert pse("GREATEST(NULL, 2, 5)") == 5
  assert pse("GREATEST(NULL, 5, 2)") == 5
  assert pse("GREATEST(2, 5, NULL)") == 5
  assert pse("GREATEST(5, 2, NULL)") == 5

  # Try both orders so we know the comparison is independent of the
  # string table codes.
  assert pse("GREATEST('bar', 'foo')") == "foo"
  assert pse("GREATEST('bar', NULL, 'foo')") == "foo"
  assert pse("GREATEST('foo', 'bar')") == "foo"
  assert pse("LEAST('bar', 'foo')") == "bar"
  assert pse("LEAST('foo', NULL, 'bar')") == "bar"

def test_greatest_least_multi_row():
  q = """
  SELECT GREATEST(s1, s2) AS a, LEAST(s1, s2) AS b
  FROM (VALUES ("foo", "bar"),
               ("bar", "foo"),
               ("foo", "qux"),
               ("",    "qux"))
        AS tbl(s1, s2)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["a", "b"],
    rows=[
      ["foo", "bar"],
      ["foo", "bar"],
      ["qux", "foo"],
      ["qux", ""],
    ]).as_dict()


def test_lag_basic():
  q = """
  SELECT s1 AS an, LAG(s1, 1) AS al
  FROM (VALUES ("foo"),
               ("bar"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo",     N],
      ["bar", "foo"],
      ["qux", "bar"],
    ])

def test_lag_where_having():
  q = """
  SELECT s1 AS an, LAG(s1, 1) AS al
  FROM (VALUES ("foo"),
               ("bar"),
               ("qux"))
        AS tbl(s1)
  WHERE al == "foo"
  """
  with _qerror("not allowed in WHERE clause"):
    _execute_qt(ps2qt(q))
  q = q.replace("WHERE", "HAVING")
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["bar", "foo"],
    ])

def test_lag_default_value():
  q = """
  SELECT s1 AS an, LAG(s1, 1, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "egg"],
      ["spa", "foo"],
      ["qux", "spa"],
    ])

def test_lag_crazy_bounds():
  q = """
  SELECT s1 AS an, LAG(s1, 5) AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    schemas=[STRING_SCHEMA, STRING_SCHEMA],
    rows=[
      ["foo", N],
      ["spa", N],
      ["qux", N],
    ])

def test_lag_bounds():
  q = """
  SELECT s1 AS an, LAG(s1, 2, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "egg"],
      ["spa", "egg"],
      ["qux", "foo"],
    ])

def test_lag_expression_evaluation():
  q = """
  SELECT s1 AS an, LAG(s1, 1+1, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "egg"],
      ["spa", "egg"],
      ["qux", "foo"],
    ])

  q = """
  SELECT s1 AS an, LAG(s1, s2, "egg") AS al
  FROM (VALUES ("foo", "bar"),
               ("spa", "qux"),
               ("",    "qux"))
        AS tbl(s1, s2)
  """
  with _qerror("s2 is unbound"):
    ps2qt(q)

def test_lead_basic():
  q = """
  SELECT s1 AS an, LEAD(s1, 1) AS al
  FROM (VALUES ("foo"),
               ("bar"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "bar"],
      ["bar", "qux"],
      ["qux",     N],
    ])

def test_lead_default_value():
  q = """
  SELECT s1 AS an, LEAD(s1, 1, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "spa"],
      ["spa", "qux"],
      ["qux", "egg"],
    ])

def test_lead_crazy_bounds():
  q = """
  SELECT s1 AS an, LEAD(s1, 5) AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    schemas=[STRING_SCHEMA, STRING_SCHEMA],
    rows=[
      ["foo", N],
      ["spa", N],
      ["qux", N],
    ])

def test_lead_bounds():
  q = """
  SELECT s1 AS an, LEAD(s1, 2, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "qux"],
      ["spa", "egg"],
      ["qux", "egg"],
    ])

def test_lead_expression_evaluation():
  q = """
  SELECT s1 AS an, LEAD(s1, 1+1, "egg") AS al
  FROM (VALUES ("foo"),
               ("spa"),
               ("qux"))
        AS tbl(s1)
  """
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["an", "al"],
    rows=[
      ["foo", "qux"],
      ["spa", "egg"],
      ["qux", "egg"],
    ])

  q = """
  SELECT s1 AS an, LEAD(s1, s2, "egg") AS al
  FROM (VALUES ("foo", "bar"),
               ("spa", "qux"),
               ("",    "qux"))
        AS tbl(s1, s2)
  """
  with _qerror("s2 is unbound"):
    ps2qt(q)



def test_generate_sequential_spans():
  q = """SELECT SPAN FROM
  dctv.generate_sequential_spans(0ns, 10ns, (SELECT 2ns))"""
  qt = ps2qt(q)
  assert qt.table_schema == SPAN_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["_ts", "_duration"],
    rows=[
      [0, 2],
      [2, 2],
      [4, 2],
      [6, 2],
      [8, 2],
    ]).as_dict()

def test_generate_sequential_spans_start():
  q = """SELECT SPAN FROM
  dctv.generate_sequential_spans(1ns, 10ns, (SELECT 2ns))"""
  qt = ps2qt(q)
  assert qt.table_schema == SPAN_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(ps2qt(q)) == TestQueryTable(
    names=["_ts", "_duration"],
    rows=[
      [1, 2],
      [3, 2],
      [5, 2],
      [7, 2],
    ]).as_dict()



def _tvf_eval(q, *args, **kwargs):
  q = "SELECT value FROM dctv.identity(({})) AS tbl(value)".format(q)
  args=qargs(*args, **kwargs)
  result, = _execute_qt(ps2qt(q, _args=args))["value"]
  return result

def test_tvf_query_math():
  """Test TVF-context operators that work on queries"""
  # pylint: disable=compare-to-zero
  assert _tvf_eval("5") == 5
  assert _tvf_eval("5+5") == 10
  assert _tvf_eval(":x+5", x=QueryNode.scalar(2)) == 7
  assert _tvf_eval(":x-5", x=QueryNode.scalar(2)) == -3
  assert _tvf_eval("5-:x", x=QueryNode.scalar(2)) == 3
  assert _tvf_eval("5-?", QueryTable.coerce_(3)) == 2
  assert _tvf_eval("?-5", QueryTable.coerce_(3)) == -2
  assert _tvf_eval("?<5", QueryTable.coerce_(3)) is NP_TRUE
  assert _tvf_eval("?>5", QueryTable.coerce_(3)) is NP_FALSE



def test_compare_null_unit():
  assert pse("1ns IS NULL") is NP_FALSE
  assert pse("1ns IS NOT NULL") is NP_TRUE
  assert pse("NULL IS 1ns") is NP_FALSE
  assert pse("NULL IS NOT 1s") is NP_TRUE



def test_if():
  assert pse("IF(1, 2, 3)") == 2
  assert pse("IF(1byte, 2ns, 3ns)") == 2
  assert pse("IF(1, 2us, 3us) IN ns") == 2000
  with _qerror("unit mismatch"):
    pse("IF(1, 2us, 3bytes)")

  assert pse("IF(FALSE, 2, 3)") == 3
  assert pse("IF(1, NULL, 3)") is None
  assert pse("IF(0, 2, NULL)") is None
  with _qerror("cannot combine"):
    pse("IF(1, 1.0, 'blarg')")
  assert pse("IF(0, 1, 2.1)") == 2.1
  assert pse("IF('foo', 'yes', 'no')") == "yes"
  assert pse("IF('', 'yes', 'no')") == "no"
  assert pse("IF('', 'yes', NULL)") is None
  assert pse("IF(NULL, 1, 2)") == 2



def test_extend_spans_basic():
  stbl = TestQueryTable(
    names=("_ts", "_end"),
    rows=[
      [ 1,  7],
      [ 7, 10],
      [15, 20],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = "SELECT SPAN * FROM dctv.extend_spans(stbl, 1ns)"
  qt = ps2qt(q, stbl=stbl)
  assert qt.table_schema == SPAN_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "_end"),
    rows=[
      [ 1, 11],
      [15, 21],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR).as_dict()


@pytest.mark.parametrize("part_dtype_code", ["i", "L", "b", "l", "g"])
def test_extend_spans_partitioned(part_dtype_code):
  part_dtype = np.dtype(part_dtype_code)
  stbl = TestQueryTable(
    names=("_ts", "_end", "part"),
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype),
    rows=[
      [ 0, 10, 1],
      [ 1,  7, 0],
      [ 7, 10, 0],
      [15, 20, 0],
    ],
    table_schema=_span_table_time_major("part"))
  q = "SELECT SPAN * FROM dctv.extend_spans(stbl, 1ns)"
  qt = ps2qt(
    q, stbl=stbl
  ).to_time_major()
  assert qt.table_schema == stbl.table_schema
  if part_dtype_code == "g":
    with pytest.raises(TypeError):
      _execute_qt(qt)
    return

  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "_end", "part"),
    schemas=(TS_SCHEMA, DURATION_SCHEMA, part_dtype),
    rows=[
      [ 0, 11, 1],
      [ 1, 11, 0],
      [15, 21, 0],
    ],
    table_schema=stbl.table_schema)

def test_extend_spans_in_cte():
  stbl = TestQueryTable(
    names=("_ts", "_end"),
    rows=[
      [ 1,  7],
      [ 7, 10],
      [15, 20],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = """
  WITH foo AS dctv.extend_spans(stbl, 1ns)
  SELECT SPAN * FROM foo
  """
  qt = ps2qt(q, stbl=stbl)
  assert qt.table_schema == SPAN_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(qt) == TestQueryTable(
    names=("_ts", "_end"),
    rows=[
      [ 1, 11],
      [15, 21],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)

def test_non_table_in_cte():
  q = """
  WITH bar AS dctv.identity(foo),
       qux AS (afunc(bar)),
  SELECT * FROM qux AS qux(n)
  """
  afunc = TableValuedFunction(lambda arg: arg[0] + 1)
  assert _execute_qt(ps2qt(q, foo=[5], afunc=afunc)) == \
    {"n": [6]}



def test_expression_subquery_simple():
  assert pse("(SELECT 4)") == 4
  assert pse("(SELECT 4+1)") == 5
  assert pse("(SELECT 4+:foo)", _args=qargs(foo=2)) == 6

def test_expression_subquery_multiple_rows():
  q = """
  WITH foo AS (VALUES (1), (2), (3)),
       bar AS (VALUES (4), (5), (6)),
  SELECT (SELECT vfoo FROM foo AS foo(vfoo)),
         (SELECT vbar FROM bar AS bar(vbar))
  FROM dctv.filled(3)
  """
  qt = ps2qt(q)
  assert _execute_qt(qt) == TestQueryTable(
    names=("vfoo", "vbar"),
    rows=[
      [1, 4],
      [2, 5],
      [3, 6],
    ]).as_dict()

def test_expression_subquery_broadcast():
  q = "SELECT (SELECT 4 AS thing) FROM dctv.filled(3)"
  qt = ps2qt(q)
  assert _execute_qt(qt) == TestQueryTable(
    names=["thing"],
    rows=[
      [4],
      [4],
      [4],
    ]).as_dict()

def test_expression_subquery_non_coordinated():
  q = """
  WITH foo(col) AS (VALUES (1), (2), (3)),
  SELECT (SELECT col)
  FROM foo
  """
  with _qerror("column reference col is unbound"):
    ps2qt(q)



def test_span_starts_tvf():
  spantbl = TestQueryTable(
    names=["_ts", "_end", "sval"],
    rows=[
      [ 4,  7,  -4],
      [ 7,  9,  -5],
      [11,  13, -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  qt = ps2qt("SELECT EVENT * FROM dctv.span_starts(spantbl)",
             spantbl=spantbl)
  assert qt.table_schema == EVENT_UNPARTITIONED_TIME_MAJOR
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "sval"],
    rows=[
      [ 4, -4],
      [ 7, -5],
      [11, -6],
    ],
    table_schema=qt.table_schema,
  ).as_dict()

def test_span_starts_tvf_partitioned():
  spantbl = TestQueryTable(
    names=["_ts", "_end", "sval"],
    rows=[
      [ 4,  7,  -4],
      [ 7,  9,  -5],
      [11,  13, -6],
    ],
    table_schema=_span_table_time_major("sval"))
  qt = ps2qt("SELECT EVENT * FROM dctv.span_starts(spantbl)",
             spantbl=spantbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "sval"],
    rows=[
      [ 4, -4],
      [ 7, -5],
      [11, -6],
    ],
    table_schema=qt.table_schema)

def test_span_ends_tvf():
  spantbl = TestQueryTable(
    names=["_ts", "_end", "sval"],
    rows=[
      [ 4,  7,  -4],
      [ 7,  9,  -5],
      [11,  13, -6],
    ],
    table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  qt = ps2qt("SELECT EVENT * FROM dctv.span_ends(spantbl)",
             spantbl=spantbl)
  assert qt.table_schema == EVENT_UNPARTITIONED_TIME_MAJOR
  assert qt["_ts"].schema.is_a(TS_SCHEMA)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "sval"],
    rows=[
      [ 7, -4],
      [ 9, -5],
      [13, -6],
    ],
    table_schema=qt.table_schema,
  ).as_dict()

def test_span_ends_tvf_partitioned():
  spantbl = TestQueryTable(
    names=["_ts", "_end", "sval"],
    rows=[
      [ 4,  7,  -4],
      [ 7,  9,  -5],
      [11,  13, -6],
    ],
    table_schema=_span_table_time_major("sval"))
  qt = ps2qt("SELECT EVENT * FROM dctv.span_ends(spantbl)",
             spantbl=spantbl)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "sval"],
    rows=[
      [ 7, -4],
      [ 9, -5],
      [13, -6],
    ],
    table_schema=qt.table_schema)

def test_regexp():
  # pylint: disable=compare-to-zero
  assert pse("'foo' REGEXP 'foo'") is NP_TRUE
  assert pse("'foo' NOT REGEXP 'bar'") is NP_TRUE
  assert pse("'foo' REGEXP 'bar'") is NP_FALSE
  assert pse("'foo' REGEXP 'fo*'") is NP_TRUE
  assert pse("'bar' REGEXP 'fo*'") is NP_FALSE

  tbl = TestQueryTable(
    names=["subject", "pattern", "expected"],
    rows=[
      [ "foo",   "foo",  True],
      [ "bar",   "bar",  True],
      [ "bar",   "bar",  True],
      [ "foo",   "bar",  False],
      [ "xfoox", "foo",  False],
      [ None,    "foo",  None],
      [ "foo",   None,   None],
      [ None,    None,   None],
    ],
  )
  qt = ps2qt("SELECT subject REGEXP pattern AS result FROM tbl",
             tbl=tbl)
  assert _execute_qt(qt)["result"] == list(tbl["expected"].values)

def test_like():
  # pylint: disable=compare-to-zero
  assert pse("'foo' LIKE 'foo'") is NP_TRUE
  assert pse("'foo' LIKE 'bar'") is NP_FALSE
  assert pse("'foo' NOT LIKE 'bar'") is NP_TRUE
  assert pse("'foo' LIKE 'fo_'") is NP_TRUE
  assert pse("'foo' LIKE '%o%'") is NP_TRUE
  assert pse("'f**' LIKE 'f**'") is NP_TRUE

def test_ilike():
  # pylint: disable=compare-to-zero
  assert pse("'foo' ILIKE 'FOO'") is NP_TRUE

def test_string_concat():
  assert pse("'abc' || 'def'") == "abcdef"
  assert pse("'abc' || NULL") is None
  assert pse("NULL || NULL") is None
  assert pse("NULL || 'def'") is None
  assert pse("IF(1, 'abc', NULL) || 'def'") == "abcdef"
  assert pse("IF(0, 'abc', NULL) || 'def'") is None
  assert pse("'def' || IF(0, 'abc', NULL)") is None


# Object attributes

def test_object_attribute():
  class _MyObj(SqlAttributeLookup):

    @override
    def __init__(self, value):
      self.value = value

    @override
    def lookup_sql_attribute(self, name):
      if name == "foo":
        return self.value
      if name == "bar":
        return lambda v: type(self)(v + 2)
      if name == "qux":
        return type(self)(self.value + 1)
      return super().lookup_sql_attribute(name)

  q = "SELECT * FROM obj.foo AS foo(n)"
  assert _execute_qt(ps2qt(q, obj=_MyObj(9))) == {
    "n": [9]}
  q = "SELECT * FROM obj.doesnotexist AS foo(n)"
  with _qerror():
    _execute_qt(ps2qt(q, obj=_MyObj(9)))
  q = "SELECT * FROM obj.doesnotexist AS foo(n)"
  with _qerror():
    _execute_qt(ps2qt(q, obj={}))
  q = "SELECT * FROM obj.bar AS foo(n)"
  with _qerror():
    _execute_qt(ps2qt(q, obj=_MyObj(9)))
  q = "SELECT * FROM obj.qux.foo AS foo(n)"
  assert _execute_qt(ps2qt(q, obj=_MyObj(9))) == {
    "n": [10]}
  q = "SELECT * FROM dctv.identity(obj.qux.foo) AS foo(n)"
  assert _execute_qt(ps2qt(q, obj=_MyObj(9))) == {
    "n": [10]}
  q = "SELECT * FROM dctv.identity(obj.bar(10).foo) AS foo(n)"
  assert _execute_qt(ps2qt(q, obj=_MyObj(9))) == {
    "n": [12]}


# Uniqueness assertion

def test_assert_unique():
  foo = TestQueryTable(
    names=["foo"],
    rows=[
      [1],
      [2],
      [3],
    ])
  assert UNIQUE not in foo["foo"].schema.constraints
  q = "SELECT ASSERT_UNIQUE(foo.foo, verify=>FALSE) AS foo FROM foo"
  bar = ps2qt(q, foo=foo)
  assert UNIQUE in bar["foo"].schema.constraints
  assert _execute_qt(foo)["foo"] == [1, 2, 3]
  assert _execute_qt(bar)["foo"] == [1, 2, 3]


# Backfill

def test_backfill():
  foo = TestQueryTable(
    names=["_ts", "_end", "value"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64],
    rows=[
      [10, 20,  N],
      [20, 40,  N],
      [40, 50, -2],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = "SELECT SPAN * FROM dctv.backfill(foo)"
  qt = ps2qt(q, foo=foo)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "value"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64],
    rows=[
      [10, 20, -2],
      [20, 40, -2],
      [40, 50, -2],
    ], table_schema=TableSchema(
      TableKind.SPAN,
      None,
      TableSorting.PARTITION_MAJOR))

def test_backfill_gap():
  foo = TestQueryTable(
    names=["_ts", "_end", "value"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64],
    rows=[
      [10, 20, -3],
      [20, 40,  N],
      [40, 50, -2],
    ], table_schema=SPAN_UNPARTITIONED_TIME_MAJOR)
  q = "SELECT SPAN * FROM dctv.backfill(foo)"
  qt = ps2qt(q, foo=foo)
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "value"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64],
    rows=[
      [10, 20, -3],
      [20, 40, -2],
      [40, 50, -2],
    ], table_schema=TableSchema(
      TableKind.SPAN,
      None,
      TableSorting.PARTITION_MAJOR))

def test_backfill_partition():
  foo = TestQueryTable(
    names=["_ts", "_end", "value", "part"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT64],
    rows=[
      [ 0, 10,  N, 1],
      [ 5, 20,  N, 2],
      [10, 50, -2, 1],
      [20, 30, -3, 2],
    ], table_schema=TableSchema(
      TableKind.SPAN,
      "part",
      TableSorting.TIME_MAJOR))
  q = "SELECT SPAN * FROM dctv.backfill(foo)"
  qt = ps2qt(q, foo=foo).to_time_major()
  assert _execute_qt(qt) == TestQueryTable(
    names=["_ts", "_end", "part", "value"],
    schemas=[TS_SCHEMA, DURATION_SCHEMA, INT64, INT64],
    rows=[
      [ 0, 10,  1, -2],
      [ 5, 20,  2, -3],
      [10, 50,  1, -2],
      [20, 30,  2, -3],
    ], table_schema=TableSchema(
      TableKind.SPAN,
      "part",
      TableSorting.TIME_MAJOR))
