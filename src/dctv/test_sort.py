# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test sorting by various dtypes"""

# pylint: disable=missing-docstring

import logging
import pytest

from .test_binop import (
  get_example_tbl,
)

from .test_query import (
  ALL_SCHEMAS,
  TestQueryTable,
  _execute_qt,
)

from .test_sql import (
  ps2qt,
)

from .test_util import (
  parameterize_dwim,
)

log = logging.getLogger(__name__)
N = None

@parameterize_dwim(
  val_name=["E", "N", "2"],
  schema_name=ALL_SCHEMAS,
  direction=["ascending", "descending"],
)
def test_sort(val_name, schema_name, direction):
  tbl = get_example_tbl(schema_name, val_name)
  schema = tbl["vl"].schema
  q = "SELECT vl FROM tbl ORDER BY vl {}".format(

    "ASC" if direction == "ascending" else "DESC")
  qt = ps2qt(q, tbl=tbl)
  assert qt["vl"].schema == schema
  _execute_qt(qt)

@pytest.mark.parametrize("direction", ["asc", "desc"])
def test_order_by(direction):
  rows = [
    [3, 1, 9],
    [2, 2, 8],
    [5, 1, 7],
    [1, 2, 6],
    [1, 2, 5],
    [2, 1, 4],
    [1, 2, 3],
  ]
  tbl = TestQueryTable(names=("key1", "key2", "value"), rows=rows)
  query = "SELECT * FROM tbl ORDER BY key1 {d}, key2 {d}"\
    .format(d=direction)
  qt = ps2qt(query, tbl=tbl)
  sorted_rows = list(sorted(rows,
                            key=lambda row: row[:2],
                            reverse=(direction == "desc")))
  tbl_sorted = TestQueryTable(
    names=("key1", "key2", "value"),
    rows=sorted_rows)
  assert _execute_qt(qt) == tbl_sorted.as_dict()

def test_order_by_column_alias():
  rows = [
    [3, 1],
    [2, 2],
    [1, 2],
  ]
  tbl = TestQueryTable(names=("key1", "value"), rows=rows)
  query = "SELECT key1 AS foo, value FROM tbl ORDER BY foo DESC"
  qt = ps2qt(query, tbl=tbl)
  sorted_rows = list(sorted(rows,
                            key=lambda row: row[:2],
                            reverse=True))
  tbl_sorted = TestQueryTable(
    names=("foo", "value"),
    rows=sorted_rows)
  assert _execute_qt(qt) == tbl_sorted.as_dict()

def test_order_noop_because_broadcast():
  q = """
SELECT foo FROM (VALUES (1), (3), (NULL), (2),
                (5), (2), (NULL), (1) ) AS vals(foo)
  ORDER BY 1
  """
  assert _execute_qt(ps2qt(q))["foo"] == \
    [1, 3, N, 2, 5, 2, N, 1]

def test_order_by_null():
  q = """
SELECT foo FROM (VALUES (1), (3), (NULL), (2),
                (5), (2), (NULL), (1) ) AS vals(foo)
  ORDER BY foo
  """
  assert _execute_qt(ps2qt(q))["foo"] == \
    [1, 1, 2, 2, 3, 5, None, None]

def test_string_sort_collate():
  def _test(collation):
    q = ("""
    WITH strings(str) AS (VALUES
                         ("def"),
                         ("abc"),
                         ("BLARG"))
  SELECT str FROM strings ORDER BY str {}
    """.format("" if not collation
               else "COLLATE " + collation))
    return _execute_qt(ps2qt(q))["str"]

  assert _test(None) == ["BLARG", "abc", "def"]
  assert _test("binary") == ["BLARG", "abc", "def"]
  assert _test("nocase") == ["abc", "BLARG", "def"]
  assert _test("length") == ["def", "abc", "BLARG"]

def test_sort_signed_integers():
  q = """
  SELECT foo
  FROM (VALUES (1), (0), (-2), (-4), (5)) AS vals(foo)
  ORDER BY foo
  """
  assert _execute_qt(ps2qt(q))["foo"] == \
    [-4, -2, 0, 1, 5]

def test_sort_float():
  q = """
  SELECT foo
  FROM (VALUES (-0.0), (2), (-2.5), (-1.4), (0), (5)) AS vals(foo)
  ORDER BY foo
  """
  assert _execute_qt(ps2qt(q))["foo"] == \
    [-2.5, -1.4, -0.0, 0.0, 2.0, 5.0]
