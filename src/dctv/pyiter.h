// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include "pyutil.h"
#include "vector.h"

namespace dctv {

template<bool Fast>
struct pyseq_iter_full :
    boost::iterator_facade<
  pyseq_iter_full<Fast>,
  typename std::conditional<Fast, pyref, unique_pyref>::type,
  boost::random_access_traversal_tag,
  typename std::conditional<Fast, pyref, unique_pyref>::type>
{
  pyseq_iter_full();
  explicit pyseq_iter_full(pyref in_seq);
  pyseq_iter_full(const pyseq_iter_full& other)
      : seq(other.seq.addref()),
        idx(other.idx),
        size(other.size)
  {}
  pyseq_iter_full(pyseq_iter_full&& other) noexcept = default;
  pyseq_iter_full& operator=(const pyseq_iter_full& other) noexcept {
    if (&other != this) {
      this->seq = other.seq.addref();
      this->idx = other.idx;
      this->size = other.size;
    }
    return *this;
  }
  pyseq_iter_full& operator=(pyseq_iter_full&& other)
      noexcept = default;
  static pyseq_iter_full end();

 private:
  friend class boost::iterator_core_access;

  // TODO(dancol): write in term of iterator instead

  template<bool XFast = Fast>
  typename std::enable_if<!XFast, unique_pyref>::type
  dereference() const { return get_item(this->seq, this->idx); }

  template<bool XFast = Fast>
  typename std::enable_if<XFast, pyref>::type
  dereference() const {
    return PySequence_Fast_GET_ITEM(this->seq.get(), this->idx);
  }

  bool is_at_end() const;
  bool equal(const pyseq_iter_full& other) const;
  void increment() { this->idx += 1; }
  void decrement() { this->idx -= 1; }
  void advance(typename pyseq_iter_full::difference_type d) {
    this->idx += d;
  }

  typename pyseq_iter_full::difference_type
  distance_to(const pyseq_iter_full& other) const;

  unique_pyref seq;
  Py_ssize_t idx;  // NOLINT
  Py_ssize_t size;  // NOLINT
};

using pyseq_iter = pyseq_iter_full<false>;
using pyseq_iter_fast = pyseq_iter_full<true>;

// Enable range-based for loops
inline pyseq_iter begin(pyref value);
inline pyseq_iter end(pyref value);

// Vector transformation
template<typename Iterator, typename Functor>
auto py2vec_full(pyref seq, const Functor& transform);

template<typename Functor>
auto py2vec(pyref seq, const Functor& transform);

template<typename Functor>
auto py2vec_fast(pyref seq, const Functor& transform);

}  // namespace dctv

#include "pyiter-inl.h"
