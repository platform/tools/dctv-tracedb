# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Basic query constructs

This module contains the basic machinery to the DCTV query engine.
"""
import logging
import sys
from functools import partial, reduce, lru_cache

import numpy as np
from numpy import ndarray
from numpy.ma import masked_array, nomask

from cytoolz import (
  drop,
  first,
  identity,
  merge,
  valmap,
)

from pint.errors import DimensionalityError

from modernmp.util import (
  assert_seq_type,
  the,
)

from .util import (
  AutoNumber,
  BOOL,
  EqImmutable,
  ExplicitInheritance,
  FLOAT32,
  FLOAT64,
  INT16,
  INT32,
  INT64,
  INT8,
  IdentityHashedImmutable,
  Immutable,
  Interned,
  NoneType,
  UINT16,
  UINT32,
  UINT64,
  UINT8,
  abstract,
  all_same,
  cached_property,
  enumattr,
  final,
  iattr,
  ignore_numpy_errors,
  load_pandas,
  override,
  sattr,
  tattr,
  ureg,
)

from ._native import (
  DTYPE_DURATION,
  DTYPE_INDEX,
  DTYPE_STRING_ID,
  DTYPE_STRING_RANK,
  DTYPE_TS,
  MATCH_PCRE,
  MATCH_SQL_ILIKE,
  MATCH_SQL_LIKE,
)

from ._native import (
  QueryCache,
  SequentialTaker,
  dctv_rules_result_type,
  fast_argsort,
  fls,
  npy_broadcast_to,
  npy_explode,
  npy_get_broadcaster,
  npy_get_data,
  npy_get_mask,
  npy_has_mask,
  npy_is_broadcasted,
  str_fast_take,
  study_index_for_take,
)

log = logging.getLogger(__name__)


# Utilities and accessors

class QueryWarning(UserWarning):
  """Warning during query processing"""

def iattr_unit(**kwargs):
  """iattr for a unit"""
  return iattr(type_if_debug=lambda: ureg().Unit, **kwargs)

def make_query_iattr(required_schema):
  """Make an iattr for a QueryNode conforming to a schema"""
  # TODO(dancol): codegen the conversion function?
  def _iattr(*, converter=None, **kwargs):
    assert not converter, "cannot explicitly specify converter here"
    nullable = kwargs.get("nullable")
    def _converter(value):
      q = QueryNode.coerce_(value)
      if nullable and value is None:
        return None
      if q.schema.unit != required_schema.unit:
        q = q.to_unit(required_schema.unit)
      if q.schema.dtype != required_schema.dtype:
        q = q.to_dtype(required_schema.dtype, safe=True)
      if not q.schema.is_a(required_schema):
        raise InvalidQueryException(
          "schema {} expected but found {}".format(
            required_schema, q.schema))
      return q
    ia = iattr(QueryNode, converter=_converter, **kwargs)
    ia.required_schema = required_schema
    return ia
  return _iattr

def _iattr_query_node_int_converter(value):
  qn = QueryNode.coerce_(value)
  schema = qn.schema
  if not (schema.is_integral or schema.is_null) or schema.unit or schema.domain:
    raise InvalidQueryException(
      "invalid schema for integral context: {}".format(schema))
  return qn

def iattr_query_node_int(**kwargs):
  """Immutable attribute that checks for a QueryNode yielding an integer"""
  return iattr(QueryNode,
               converter=_iattr_query_node_int_converter,
               **kwargs)

def int_input(key, inputs):
  """Retrieve an integer input to a query"""
  arr = inputs[key]
  if len(arr) != 1:
    raise InvalidQueryException(
      "non-scalar result of scalar query {!r}".format(key))
  value = arr[0]
  if value is np.ma.masked:
    raise InvalidQueryException("NULL supplied for scalar query")
  return int(value)

def masked_take(array, indices):
  """Like np.ma.take, but returns a plain array if there's no mask"""
  if npy_has_mask(array) or npy_has_mask(indices):
    return np.ma.take(array, indices)
  return np.take(array, indices)

def _is_pandas_series(value):
  return "pandas" in sys.modules and \
    isinstance(value, load_pandas().Series)

def masked_broadcast_to(array, dim):
  """Like np.broadcast_to, but work properly for masked arrays

  Broadcasting a masked singleton results in a masked broadcasted
  array.  np.broadcast_to just ignores the mask and broadcasts the
  underlying data.
  """
  mask = npy_get_mask(array)
  if mask is nomask or not mask[0]:
    return npy_broadcast_to(array, dim)
  return masked_array(npy_broadcast_to(array, dim),
                      npy_broadcast_to(mask, dim))

def die_mismatched_blocks(channels, blocks):
  """Die on mismatched lengths.  Does not return.

  CHANNELS is a sequence of channels.  CHANNELS is a sequence of
  blocks from these channels.  Report an error about the lengths of
  the blocks being mismatched.
  """
  assert not all_same([len(block) for block in blocks])
  raise InvalidQueryException(
    "queries had divergent lengths: {!r}".format(
      [(ic.query, len(block)) for ic, block in
       zip(channels, blocks)]))

def check_same_lengths(channels, blocks):
  """Make sure that BLOCKS and produced in lockstep.

  CHANNELS is a sequence of channels (input or output) and BLOCKS is a
  sequence of produced blocks or, despite the naming, arrays.
  """
  assert len(channels) == len(blocks)
  if not all_same(len(block) for block in blocks):
    die_mismatched_blocks(channels, blocks)

async def transduce(qe, ics, ocs, transducer):
  """Call TRANSDUCER to transform blocks of arrays in ICS to OCS
  """
  if not ics:
    return
  def _run_transducer(inp_arrays):
    rl = inp_arrays[0].size
    if rl <= 1:
      return transducer(*inp_arrays)
    base_arrays = [npy_get_broadcaster(a) for a in inp_arrays]
    if not all(a is not None for a in base_arrays):
      return transducer(*inp_arrays)
    base_out = transducer(*base_arrays)
    return [masked_broadcast_to(a, rl) for a in base_out]
  block_size = qe.block_size
  inps = await qe.async_io(*[ic.read(block_size) for ic in ics])
  check_same_lengths(ics, inps)
  while inps[0]:
    outs = _run_transducer([inp.as_array() for inp in inps])
    assert not isinstance(outs, ndarray), (
      "operator accidentally returned array, "
      "not sequence, from transduce!")
    check_same_lengths(ocs, outs)
    assert len(outs[0]) == len(inps[0])
    eofp = len(inps[0]) < block_size
    del inps
    batch = ([ic.read(block_size) for ic in ics] +
             [oc.write(out, eofp) for oc, out in zip(ocs, outs)])
    del outs
    inps = (await qe.async_io(*batch))[:len(ics)]
    check_same_lengths(ics, inps)

async def passthrough(qe, ic, oc):
  """Forward blocks from IC to QC in QE"""
  block = await ic.read(qe.block_size)
  while block:
    is_eof = len(block) < qe.block_size
    req = qe.async_io(ic.read(qe.block_size), oc.write(block, is_eof))
    del block
    [block, _] = await req

def die_on_length_mismatch(query, rows_delivered, wanted_rows):
  """Raise an error about a query length contract violation"""
  assert rows_delivered != wanted_rows
  raise InvalidQueryException("query {!r} yielded{}{!r} rows "
                              "but we wanted exactly {!r}".format(
                                query,
                                (" exactly " if rows_delivered < wanted_rows
                                 else " at least "),
                                rows_delivered,
                                wanted_rows))

def query_typecheck(value, expected_type, query):
  """Check that VALUE has EXPECTED_TYPE for QUERY

  Raises a useful InvalidQueryException if the type check is violated,
  so use this instead of generic typechecking.
  """
  assert isinstance(query, QueryNode)
  if not isinstance(value, expected_type):
    raise InvalidQueryException("expected a {!r} from {} but got {!r}".format(
      expected_type, query, value))
  return value

def reduce_ufunc_masked(qe, ufunc, dtype, arrays):
  """Apply a ufunc reduction across non-masked values in rows.

  The output is a 1d array, one item per input row, that's masked at
  each row if and only if all inputs at that row are masked.
  """
  # pylint: disable=assignment-from-no-return
  assert all_same(map(len, arrays))
  arrays_mask = [npy_get_mask(array) for array in arrays]
  if not any(arrays_mask):
    out = qe.qc.make_uninit_array(dtype, len(arrays[0]))
    return ufunc.reduce(arrays, dtype=dtype, out=out)
  # Do it the hard way.  We tried using numexpr for the boolean
  # evaluations below, but it ended up being four times slower (even
  # taking care to avoid useless intermediate array allocations) than
  # careful memory management using the `out' keyword argument and
  # standard numpy operations.
  arrays_data = [npy_get_data(array) for array in arrays]
  out_valid_mask = np.logical_not(arrays_mask[0],
                                  out=np.empty(len(arrays_data[0]), BOOL))
  out_data = qe.qc.make_uninit_array(dtype, len(out_valid_mask))
  np.invert(out_valid_mask, out=out_valid_mask)
  np.copyto(out_data, 0, where=out_valid_mask)
  np.invert(out_valid_mask, out=out_valid_mask)
  np.copyto(out_data, arrays_data[0], where=out_valid_mask)
  scratch_mask = np.empty_like(out_valid_mask)
  for array_data, array_mask in drop(1, zip(arrays_data, arrays_mask)):
    # Make scratch_mask a mask of valid locations in array_data.
    np.logical_not(array_mask, out=scratch_mask)

    # Now make scratch_mask a list of locations at which both out_data
    # and array_data are valid.
    np.logical_and(out_valid_mask, scratch_mask, out=scratch_mask)

    # For the locations in scratch_mask, run the ufunc.
    ufunc(out_data, array_data,
          dtype=dtype,
          casting="safe",
          out=out_data,
          where=scratch_mask)

    # Now set scratch_mask to those locations where array_data is
    # valid and out_data is not.  scratch_mask ends up containing
    # do_copy.

    #   do_copy = !old_valid && new_valid
    #  !do_copy = old_valid || !new_valid
    # !!do_copy = !(old_valid || !new_valid)
    #   do_copy = !(old_valid || !new_valid)

    np.logical_or(out_valid_mask, array_mask, out=scratch_mask)
    np.logical_not(scratch_mask, out=scratch_mask)
    np.copyto(out_data, array_data, where=scratch_mask)
    np.copyto(out_valid_mask, True, where=scratch_mask)

  return masked_array(
    out_data,
    np.logical_not(out_valid_mask, out=out_valid_mask))

def _safe_str_fast_take(qc, codes, translate_table):
  assert isinstance(qc, QueryCache)
  assert not npy_has_mask(translate_table)
  assert not npy_is_broadcasted(translate_table), \
    "ST doesn't generate bcasted translations"
  codes_bcast = npy_get_broadcaster(codes)
  if codes_bcast is not None:
    assert len(codes_bcast) == 1
    return masked_broadcast_to(
      str_fast_take(np.empty(1, codes_bcast.dtype),
                    codes_bcast,
                    translate_table),
      len(codes))
  codes, mask = npy_explode(codes)
  out = qc.make_uninit_array(codes.dtype, len(codes))
  str_fast_take(out, codes, translate_table)
  if mask is not nomask:
    # pylint: disable=redefined-variable-type
    out = masked_array(out, mask)  # Reuse original mask
  return out

DTYPE_KINDS = {
  "b": "boolean",
  "i": "signed integer",
  "u": "unsigned integer",
  "f": "floating point",
  "c": "complex",
}

ALL_KINDS = "".join(DTYPE_KINDS)
UNSIGNED_TYPECODES = "BHILQ"


# Query schema support

class QuerySchemaKind(AutoNumber):
  """High-level kind of QuerySchema"""
  NORMAL = ()
  STRING = ()
  MAGIC_NULL = ()

class QuerySchemaConstraint(AutoNumber):
  """Additional constraints on query values"""
  UNIQUE = ()  # All values are distinct

UNIQUE = QuerySchemaConstraint.UNIQUE
C_UNIQUE = frozenset({UNIQUE})

class QuerySchema(EqImmutable):
  """Metadata about the type of a particular query"""

  # TODO(dancol): does it make sense to combine some of these fields
  # for the sake of performance?  Or to move this class to C++?

  # TODO(dancol): expore interning these values

  dtype = iattr(np.dtype)
  kind = enumattr(QuerySchemaKind,
                  default=QuerySchemaKind.NORMAL,
                  kwonly=True)
  domain = iattr(str, nullable=True, default=None, kwonly=True)
  unit = iattr_unit(kwonly=True,
                    nullable=True,
                    default=None)
  constraints = sattr(QuerySchemaConstraint,
                      default=frozenset())

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert not self.is_string or self.unit is None, \
      "strings cannot have numerical units"
    assert not self.is_null or self.unit is None, \
      "magic null cannot have numerical units"
    if self.is_string:
      assert not self.unit
      assert self.dtype == DTYPE_STRING_ID
    if self.is_null:
      assert not self.unit
      assert self.dtype is INT8
    assert all(isinstance(c, QuerySchemaConstraint)
               for c in self.constraints)

  def propagate_constraints(self, known_constraints):
    """Pass through constraints in a set"""
    new_constraints = self.constraints & known_constraints
    if new_constraints == self.constraints:
      return self
    return self.evolve(constraints=new_constraints)

  def unconstrain(self):
    """Make a conservative, unconstrained version of this schema"""
    if self.constraints:
      return self.evolve(constraints=frozenset())
    return self

  def constrain(self, constraints):
    """Make a version of this query schema with added constraints"""
    new_constraints = self.constraints | frozenset(constraints)
    if new_constraints == self.constraints:
      return self
    return self.evolve(constraints=new_constraints)

  @property
  def is_normal(self):
    """Return whether the schema lacks special features"""
    return not (self.is_string or
                self.is_null or
                self.unit or
                self.domain)

  @property
  def is_integral(self):
    """Whether this query yields an integer"""
    return (not self.is_string and
            not self.is_null and
            self.dtype.kind in ("u", "i"))

  @property
  def is_null(self):
    """Whether this query represents a flexible NULL"""
    return self.kind == QuerySchemaKind.MAGIC_NULL

  @property
  def is_string(self):
    """Whether this query represents a string"""
    return self.kind == QuerySchemaKind.STRING

  @staticmethod
  def concat(schemas):
    """Return a covering schema for all SCHEMAS"""
    # pylint: disable=protected-access
    return reduce(lambda s1, s2: s1._concat(s2), schemas)

  def _concat_1(self, other_schema):
    """Return the schema describing a concatenation"""
    is_null = (self.is_null, other_schema.is_null)
    if is_null[0] and is_null[1]:
      return self
    if is_null[0]:
      return other_schema
    if is_null[1]:
      return self
    if self.is_string != other_schema.is_string:
      raise InvalidQueryException(
        "cannot combine string and non-string values")
    # TODO(dancol): whichever query sets a domain wins.  Do we want to
    # refine this rule somehow?  We allow combining domainless values
    # with domained ones for the benefit of the repl.
    domain = self.domain or other_schema.domain or None
    if (self.domain and
        other_schema.domain and
        self.domain != other_schema.domain):
      raise InvalidQueryException("domain mismatch: {!r} vs {!r}".format(
        self.domain, other_schema.domain))
    if self.unit != other_schema.unit:
      raise BadUnitsException("unit mismatch: {} vs {}"
                              .format(self.unit, other_schema.unit))
    dtype = dctv_rules_result_type(self.dtype, other_schema.dtype)
    # TODO(dancol): use evolve
    if dtype == self.dtype and self.domain is domain:
      return self
    if dtype == other_schema.dtype and other_schema.domain is domain:
      return other_schema
    return QuerySchema(dtype,
                       kind=self.kind,
                       domain=domain,
                       unit=self.unit)

  def _concat(self, other):
    # Unconditionally unconstraining is conservative, but let's be
    # conservative for now.
    return self._concat_1(other).unconstrain()

  def is_a(self, req):
    """Is OTHER_SCHEMA a special case of this schema?"""
    return (req.dtype == self.dtype and
            req.kind == self.kind and
            req.domain == self.domain and
            req.unit == self.unit and
            req.constraints.issubset(self.constraints))

  @cached_property
  def qn_iattr(self):
    """An iattr useful for Immutable definitions

    The iattr coerces the field value to a QueryNode and ensures that
    the QueryNode's schema matches this one.
    """
    return make_query_iattr(self)

  def check_no_domain(self):
    """Raise IQE if we have a domain"""
    if self.domain:
      raise InvalidQueryException(
        "operation not supported on "
        "domain {!r}".format(self.domain))


# Basic query and action infrastructure

class InvalidQueryException(ValueError):
  """Exception raised when a query is invalid"""

class BadCastException(InvalidQueryException):
  """Exception raised when a value cannot be cast"""

class BadUnitsException(InvalidQueryException):
  """Exception used to report an invalid unit operation"""

class LiteralCoercionError(InvalidQueryException):
  """Exception raised when we can't coerce a value to a literal"""

class _RestartQueryException(Exception):
  """Special exception used to restart queries"""

class QueryAction(IdentityHashedImmutable):
  """An action to take during query execution.

  Requires zero or more QueryNode values as input; produces zero more
  QueryNode values as output.
  """

  condense = None
  """Transform a recursive connected component into a single node"""

  precious = False
  """Don't prune from query plan when unreachable"""

  async def run_async(self, qe):
    """Run a query operator

    Query operators are async routines that retrieve data blocks from
    query engine via context handle QE methods and deliver result
    blocks back to the querry engine via other methods on QE, using
    the await keyword for each interaction with QE, which operators
    should consider an opaque handle with the methods described below.

    Query operators must not retain long-term state outside the
    context of query execution.  The query engine is allowed to start,
    stop, and cancel query operators at all.

    Query operators must return None.

    Operators may await the following things:

    qe.async_setup(INPUTS, OUTPUTS) -> INPUT_CHANNELS, OUTPUT_CHANNELS

      The first thing an operator does must be to await qe.async_setup
      so as to establish communication channels with the query engine.
      INPUTS and OUTPUTS are sequences of QueryNode instances
      describing the queries that this operator produces and consumes,
      respectively.  INPUT_CHANNELS and OUTPUT_CHANNELS are tuples of
      InputChannel and OutputChannel objects that the query can use to
      request input or deliver output.

      Why go to the trouble of creating channel objects?
      Because depending on precise operation, an operator can consume
      a given input more than once, and not necessarily in a perfectly
      synchronized way.  Each input stream is an independent flow
      of blocks.

      Each output query must correspond to exactly one output stream;
      we use output stream objects for consistency with input streams
      and so that we can provide for output-stream-specific
      access methods.

    qe.async_yield_to_query_runner(VALUE) -> None

      Deliver a value to the query runner.  The query execution
      iterator yields VALUE, whatever it is, to its caller, than
      resumes query processing.

    qe.async_io(DATA_OP_1[, DATA_OP_2, [...]])
      -> (OP_RESULT_1[, OP_RESULT_2, [...]])

      Exchange data with the query engine core.  It's to call this
      method that query operators exist in the first place.
      Each DATA_OP is produced by one of the expressions below.
      Try to batch as many operations as you can into a given qe.io
      call; the query execution engine optimizes execution best when
      it has as much information as possible about what data operators
      want to request and deliver.

      Available data operations are below.  IC is an InputChannel, and
      OC is an OutputChannel, both as created by qe.async_setup.
      Note that these methods on channels return opaque request
      objects useful only for passing to qe.async_io!

        INPUT_CHANNEL.read(WANTED_ROWS) -> Block

          Read a block of WANTED_ROWS rows from the given input
          channel returning a Block with either the requested number
          of rows or a fewer if the query from which INPUT_CHANNEL
          draws terminated before producing the requested number rows.
          If INPUT_CHANNEL is already at EOF, return an empty block.

        INPUT_CHANNEL.read_at_least(WANTED_ROWS) -> Block

          Read a block of at least WANTED_ROWS rows from the given input
          channel returning a Block with either the requested number
          of rows or a fewer if the query from which INPUT_CHANNEL
          draws terminated before producing the requested number rows.
          If INPUT_CHANNEL is already at EOF, return an empty block.

          Unlike read(), may return *more* than WANTED_ROWS if query
          execution makes more rows available without waiting.  Will
          always yield at least WANTED_ROWS except at EOF.

        INPUT_CHANNEL.read_all() -> Block

          Read *all* rows from the given input channel.  This method
          is for non-streaming operations.  You should prefer to
          stream in qe.block_size-sized chunks when possible for
          greatest query execution efficiency --- but if you *must*
          look at entire column vector at once, do it by using
          INPUT_CHANNEL.read_all() and do *NOT* accumulate results in
          some private buffer inside your operator implementation.
          Private buffers cannot be automatically spilled in response
          to memory pressure!

        OUTPUT_CHANNEL.write(ROW_DATA, EOF=False) -> Unspecified

          Supply data to other query operators.  ROW_DATA describes
          the actual data to suppy and can be one of the types
          described immediately below.  EOF must a boolean; if true,
          it indicates that ROWS is the last bit of row data for this
          output channel and that it should be closed.  Continuing to
          write to a closed channel is an error.

          Available types of ROW_DATA:

            - Block object, as from an input channel: most efficient

            - A numpy.mp.masked_array object describing a sequence of
              values and their masks.

            - A tuple (DATA, MASK), in which each can be converted to
              a numpy array.

            - Data, coercable to a numpy array.

            - None, meaning to write no data (useful for EOF=True)
    """
    raise NotImplementedError("abstract")

  @abstract
  def _compute_inputs(self):
    """Describe the inputs to this action.

    Return a sequence of QueryNode objects.
    """
    raise NotImplementedError("abstract")

  @abstract
  def _compute_outputs(self):
    """Describe the outputs of this action.

    Return a sequence of QueryNode objects.
    """
    raise NotImplementedError("abstract")

  @cached_property
  def inputs(self):
    """Frozenset of input queries"""
    inputs = frozenset(self._compute_inputs())
    assert assert_seq_type(frozenset, QueryNode, inputs)
    return inputs

  @cached_property
  def outputs(self):
    """Frozenset of output queries"""
    outputs = frozenset(self._compute_outputs())
    assert assert_seq_type(frozenset, QueryNode, outputs)
    return outputs

  def _compute_merge_key(self):  # pylint: disable=no-self-use
    return None

  @cached_property
  def merge_key(self):
    """Queries with same non-None merge key are executed together"""
    return self._compute_merge_key()

  def merge(self, _others):
    """Return a merged query"""
    raise RuntimeError("merging not supported", type(self))


# QueryNode

class CanBeOneQueryNode(ExplicitInheritance):
  """Common functionality for QueryNode and QueryTable"""
  @abstract
  def make_one_query_node(self):
    """Coerce this thing to a QueryNode"""
    raise NotImplementedError("abstract")

  @abstract
  def handle_unop(self, op):
    """Handle a TVF-context unary operation"""
    raise NotImplementedError("abstract")

  @abstract
  def handle_binop(self, op, other):
    """Handle a TVF-context binary operation"""
    raise NotImplementedError("abstract")

  # These operator overloads make it convenient to work with
  # QueryTable instances in TVF context.
  def __mk_binop(op, reverse=False):  # pylint: disable=no-self-argument
    def _oper(self, other):
      try:
        other = self.coerce_(other)
      except LiteralCoercionError:
        return NotImplemented
      if reverse:
        return other.handle_binop(op, self)
      return self.handle_binop(op, other)
    return _oper

  __add__ = __mk_binop("+")
  __radd__ = __mk_binop("+", reverse=True)
  __sub__ = __mk_binop("-")
  __rsub__ = __mk_binop("-", reverse=True)
  __mul__ = __mk_binop("*")
  __rmul__ = __mk_binop("*", reverse=True)
  __truediv__ = __mk_binop("/")
  __rtruediv__ = __mk_binop("/", reverse=True)
  __floordiv__ = __mk_binop("//")
  __rfloordiv__ = __mk_binop("//", reverse=True)
  __mod__ = __mk_binop("%")
  __rmod__ = __mk_binop("%", reverse=True)

  __lt__ = __mk_binop("<")
  __le__ = __mk_binop("<=")
  __gt__ = __mk_binop(">")
  __ge__ = __mk_binop(">=")

  eq = __mk_binop("=")
  ne = __mk_binop("!=")

  # TODO(dancol): check that these wouldn't screw something up elsewhere
  #__eq__ = __mk_binop("==")
  #__ne__ = __mk_binop("!=")

  def __mk_unop(op):  # pylint: disable=no-self-argument
    def _oper(self):
      return self.handle_unop(op)
    return _oper

  __neg__ = __mk_unop("-")
  __pos__ = __mk_unop("+")
  __invert__ = __mk_unop("~")

  __inherit__ = dict(__lt__=override,
                     __le__=override,
                     __gt__=override,
                     __ge__=override)

  del __mk_binop

class QueryNode(Interned, CanBeOneQueryNode):
  """Description of a desired value"""

  # N.B. the reason we have a QueryNode.filled function and ones
  # like it instead of just putting clever logic in __new__ methods
  # is that we want new methods to have the simple semantics of
  # returning a specific, possibly-interned QueryNode of the type
  # given to __new__, whereas the static utility functions on
  # QueryNode are allowed to be a bit more general and don't promise
  # to return any kind of QueryNode in particular so long as
  # whatever's returned meets the function contract requirement.

  special_add_action = None
  """Add recursive loop edges during action-graph construction"""

  @abstract
  def _compute_schema(self):
    """Compute the QuerySchema for this query."""
    raise NotImplementedError("abstract")

  @final
  @override
  def make_one_query_node(self):
    return self

  @final
  @override
  def handle_unop(self, op):
    return UnaryOperationQuery(op, self)

  @final
  @override
  def handle_binop(self, op, right):
    left = self
    left_known_size = left.countq().eager_evaluate_scalar()
    right_known_size = right.countq().eager_evaluate_scalar()
    if left_known_size != right_known_size:
      if left_known_size == 1:
        left = QueryNode.filled(left, right.countq())
      elif right_known_size == 1:
        right = QueryNode.filled(right, left.countq())
    return BinaryOperationQuery(left, op, right)

  @cached_property
  def schema(self):
    """QuerySchema describing this query"""
    schema = self._compute_schema()
    assert isinstance(schema, QuerySchema), \
      "expected a QuerySchema, got {!r} self={!r}".format(schema, self)
    return schema

  def precompute_schema(self):
    """Compute and cache schema"""
    # pylint: disable=pointless-statement
    self.schema  # For side effect

  def as_table(self, column_name):
    """Make a single-column table containing this query

    COLUMN_NAME is the name of that table's column.  The table schema
    is a regular table.
    """
    return GenericQueryTable({column_name: self})

  @abstract
  def make_action(self):
    """Make a QueryAction that computes this node"""
    raise NotImplementedError("abstract")

  def countq(self):
    """Make a query of the number of values produced by this query"""
    # TODO(dancol): a global query optimizer will obviate subclases
    # providing specializations of this method.
    return CountQuery(self)

  @staticmethod
  def scalar(fill_value):
    """Make a QueryNode yielding a single value.

    FILL_VALUE must be a Python literal that decode_literal accepts.
    """
    return ScalarQuery(*decode_literal(fill_value))

  def eager_evaluate_scalar(self):
    """Try evaluating this QueryNode to a scalar.

    If we can't do that, just return the QueryNode.
    """
    return self

  @staticmethod
  def sequential(count, *, start=0, step=1):
    """Make COUNT rows of an increasing value

    COUNT, START, and STEP can be anything automatically coerced (via
    QueryNode.coerce_) into an int-yielding query yielding of a single
    row.
    """
    return SequentialQuery(start, count, step)

  @staticmethod
  def filled(fill_value, count):
    """Broadcast FILL_VALUE to COUNT.

    FILL_VALUE is either a scalar or a QueryNode yielding one row
    (broadcast to COUNT rows) or a QueryNode yielding exactly COUNT
    rows.  COUNT is either an integer literal or something that we can
    convert into one.
    """
    fill_value = QueryNode.coerce_(fill_value)
    count = QueryNode.coerce_(count)
    # TODO(dancol): more principled rule-based optimization
    if fill_value.countq().eager_evaluate_scalar() == \
       count.eager_evaluate_scalar():
      return fill_value
    if isinstance(count, CountQuery):
      return LockstepFilledArrayQuery(fill_value, count.frm)
    return FilledArrayQuery(fill_value, count)

  @staticmethod
  def coerce_(value):
    """Make VALUE a QueryNode.

    No guarantees about the length of VALUE are made.
    """
    if isinstance(value, CanBeOneQueryNode):
      return value.make_one_query_node()
    return QueryNode.scalar(value)

  def to_unit(self, unit, allow_unitless=False):
    """Return a QueryNode converted to the given unit type"""
    if self.schema.unit == unit:
      return self
    return ConvertToUnitQuery(self, unit, allow_unitless=allow_unitless)

  def take(self, indexer):
    """Return this QueryNode indexed by indices in indexer

    If SEQUENTIAL is true, assume INDEXER produces non-decreasing
    index values.
    """
    return TakeQuery(self, indexer)

  def take_sequential(self, indexer, bcast_conservative=True):
    """Return this QueryNode indexed by indices in indexer

    If BCAST_CONSERVATIVE is false, a sequential broadcast assumes
    that *all* blocks from the data query are broadcast from the same
    value if any of them is.
    """
    cls = (SequentialTakeQuery
           if bcast_conservative
           else SequentialTakeQueryEagerBroadcast)
    return cls(self, indexer)

  def _do_to_dtype(self, dtype, safe):
    if self.schema.dtype == dtype:
      return self
    return CastQuery(self, dtype, safe)

  @final
  def to_dtype(self, dtype, safe=True):
    """Return a QueryNode in the given dtype"""
    return self._do_to_dtype(np.dtype(dtype), safe)

  def strip_unit(self):
    """Return a QueryNode with no unit attached"""
    if not self.schema.unit:
      return self
    return ConvertToUnitQuery(self, None, allow_unitless=True)

  @staticmethod
  def choose(selector, choice1, choice2):
    """SQL IF

    SELECTOR, CHOICE1, and CHOICE2 are all things we can coerce into
    QueryNode instances.  They must have equal-length result vectors.
    """
    return IfQuery(selector, choice1, choice2)

  @staticmethod
  def __greatestleast(op, things):
    things = [QueryNode.coerce_(thing) for thing in things]
    if not things:
      raise InvalidQueryException(
        "cannot find {} of zero items".format(op))
    if len(things) == 1:
      return things[0]
    return GreatestLeastQuery(op, things)

  @staticmethod
  def greatest(*things):
    """Make a query that returns the maximum of THINGS

    Each item in THINGS is something that we can coerce to a
    QueryNode.  All items must have the same length result vectors.
    """
    return QueryNode.__greatestleast("greatest", things)

  @staticmethod
  def least(*things):
    """Make a query that returns the minimum of THINGS

    Each item in THINGS is something that we can coerce into a
    QueryNode.  All items must have the same length result vectors.
    """
    return QueryNode.__greatestleast("least", things)

  def slice(self, low=0, high=None):
    """Python-style slicing for queries.

    This function works like Python slicing, which is a superset
    of traditional SQL limit-offset stuff.
    """
    return WindowQuery(self,
                       SliceWindow(QueryNode.coerce_(low),
                                   QueryNode.coerce_(high)))

  def limit_offset(self, *, limit=None, offset=None):
    """SQL-style slicing for queries.

    This function works like slice(), but expects arguments in SQL
    style instead Python style.
    """
    return WindowQuery(self,
                       LimitOffsetWindow(limit=QueryNode.coerce_(limit),
                                         offset=QueryNode.coerce_(offset)))

  @staticmethod
  def concat(*things):
    """Return the query concatenation of THINGS.

    Each thing in THINGS must be something we can coerce to a
    QueryNode.  Each thing must have a schema compatible with
    the others.
    """
    if len(things) == 1:
      return QueryNode.coerce_(things[0])
    return ConcatenatingQuery(QueryNode.coerce_(q) for q in things)

  def where(self, other):
    """Filter a query by another query"""
    other_q = QueryNode.coerce_(other)
    other_schema = other_q.schema
    if (other_schema.dtype is not BOOL or
        other_schema.domain or
        other_schema.unit or
        other_schema.kind != QuerySchemaKind.NORMAL):
      raise InvalidQueryException(
        "cannot use query as filter: {} with schema {}"
        .format(other_q, other_schema))
    # TODO(dancol): assert queries belong to the same length class.
    # TODO(dancol): broadcast other_q to length of self?
    return FilterQuery(other_q, self)

  def __getitem__(self, other):
    return self.where(other)

  @staticmethod
  def literals(*args):
    """See LiteralQuery"""
    return LiteralQuery(args)

class SimpleQueryNode(QueryNode, QueryAction):
  """QueryNode which is its own action"""

  __abstract__ = True

  @override
  @final
  def make_action(self):
    return self

  @override
  @final
  def _compute_outputs(self):
    return (self,)


# Interface for exposing attributes to SQL.  Defined here instead of
# sql.py so that QueryTable can implement it.

class SqlAttributeLookup(ExplicitInheritance):
  """Base class for objects supporting SQL attribute access"""

  def lookup_sql_attribute(self, name):  # pylint: disable=no-self-use
    """Retrieve a named field of a SQL object

    NAME is a string giving the name of the field to retrieve.
    Raise KeyError if the field doesn't exist.
    """
    raise KeyError(name)

  def enumerate_sql_attributes(self):  # pylint: disable=no-self-use
    """Return an iterator of SQL attributes we support

    Used for completion.
    """
    return frozenset()

  @final
  def lookup_sql_attribute_by_path(self, path):
    """Look up an item by PATH.

    PATH is a sequence of names.

    Raise KeyError if PATH does not refer to a value.
    """
    assert not isinstance(path, str)
    last_idx = len(path) - 1
    assert last_idx >= 0
    if not last_idx:
      return self.lookup_sql_attribute(path[0])
    ns = self
    for i, name in enumerate(path):
      if i == last_idx:
        return ns.lookup_sql_attribute(name)
      # pylint: disable=assignment-from-no-return
      ns = ns.lookup_sql_attribute(name)
      if not isinstance(ns, SqlAttributeLookup):
        raise KeyError
    raise AssertionError("not reached")


# QueryTable

class TableKind(AutoNumber):
  """Describes the specific type of table we have"""
  REGULAR = ()
  EVENT = ()
  SPAN = ()

class TableSorting(AutoNumber):
  """Describes how we've ordered the table"""
  NONE = ()
  """No particular sort order guaranteed"""
  TIME_MAJOR = ()
  """Ordered by time and then partition"""
  PARTITION_MAJOR = ()
  """Ordered by partition and then by time"""

class TableSchema(EqImmutable):
  """Describe the overall shape of a result table"""
  kind = enumattr(TableKind)
  partition = iattr(str, nullable=True)
  sorting = enumattr(TableSorting)

  @property
  def fixed_meta_columns(self):
    """Meta columns with fixed names that appear as a prefix"""
    if self.kind == TableKind.REGULAR:
      return ()
    if self.kind == TableKind.EVENT:
      return ("_ts",)
    if self.kind == TableKind.SPAN:
      return ("_ts", "_duration")
    raise AssertionError("unknown table type")

  @cached_property
  def meta_columns(self):
    """Special columns for this schema"""
    columns = self.fixed_meta_columns
    if self.partition:
      return columns + (self.partition,)
    return columns

  @override
  def __str__(self):
    return "{}/{}/{}".format(
      TableKind.label_of(self.kind),
      self.partition,
      TableSorting.label_of(self.sorting))

REGULAR_TABLE = TableSchema(TableKind.REGULAR,
                            None,
                            TableSorting.NONE)
EVENT_UNPARTITIONED_TIME_MAJOR = \
  TableSchema(TableKind.EVENT, None, TableSorting.TIME_MAJOR)
SPAN_UNPARTITIONED_TIME_MAJOR = \
  TableSchema(TableKind.SPAN, None, TableSorting.TIME_MAJOR)

# TODO(dancol): QueryTables in practice end up being bags of QueryNode
# values, so instead of having a bunch of QueryTable subclasses, just
# allow a flexible constructor.

class QueryTable(Immutable, CanBeOneQueryNode, SqlAttributeLookup):
  """Factory for a family of related queries.

  A QueryTable generates a family of related QueryNode objects, all
  generating result vectors of the same length along with metadata
  about the columns generated.
  """

  table_schema = iattr(TableSchema,
                       default=REGULAR_TABLE,
                       kwonly=True)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    kind = self.table_schema.kind
    def _assert_column_schema(column_name, schema):
      assert self[column_name].schema.is_a(schema), \
        "column {!r} should have schema {} but has {}".format(
          column_name, schema, self[column_name].schema)
    if kind == TableKind.SPAN:
      _assert_column_schema("_ts", TS_SCHEMA)
      _assert_column_schema("_duration", DURATION_SCHEMA)
    elif kind == TableKind.EVENT:
      _assert_column_schema("_ts", TS_SCHEMA)
    partition = self.table_schema.partition
    if partition:
      assert partition in self, \
        "partition {!r} not in {}".format(partition, self.columns)

  def to_schema(self, *, kind=None, sorting=None):
    """Convert a table to the right schema, sorting if needed"""
    table_schema = self.table_schema
    if not kind:
      kind = table_schema.kind
    if not sorting:
      sorting = table_schema.sorting
    assert isinstance(kind, TableKind)
    if table_schema.kind != kind:
      raise InvalidQueryException(
        "wanted {}: found {}".format(
          TableKind.label_of(kind),
          TableKind.label_of(table_schema.kind)))
    if table_schema.sorting == sorting:
      return self  # Nothing to do.
    if sorting == TableSorting.NONE:  # Okay?
      return self.with_table_schema(
        table_schema.evolve(sorting=TableSorting.NONE))
    if not table_schema.partition:
      if table_schema.sorting != TableSorting.NONE:
        # Nothing to do here, since without a partition, time-major
        # and partition-major sorting are the same thing.
        return self.with_table_schema(
          table_schema.evolve(sorting=sorting))
      # No partition, so just sort by timestamp.
      sorts = (
        (self["_ts"], True),
      )
    elif sorting == TableSorting.TIME_MAJOR:
      sorts = (
        (self["_ts"], True),
        (self[table_schema.partition], True),
      )
    else:
      assert sorting == TableSorting.PARTITION_MAJOR
      sorts = (
        (self[table_schema.partition], True),
        (self["_ts"], True),
      )
    indexer = ArgSortQuery(sorts)
    return self.transform(
      lambda q: q.take(indexer),
      table_schema=table_schema.evolve(sorting=sorting))

  def to_time_major(self):
    """Convenient shorthand for self.to_schema(TableSorting.TIME_MAJOR)"""
    return self.to_schema(sorting=TableSorting.TIME_MAJOR)

  @override
  def lookup_sql_attribute(self, name):
    if name in self.columns:
      return self[name].schema
    return super().lookup_sql_attribute(name)

  @override
  def enumerate_sql_attributes(self):
    # Don't enumerate columns: that just ends up being confusing
    yield from super().enumerate_sql_attributes()

  @final
  @override
  def handle_unop(self, op):
    if self.table_schema != REGULAR_TABLE:
      raise InvalidQueryException(
        "scalar-table operations work only on regular tables")
    return GenericQueryTable(
      (column, column.handle_unop(op)) for column in self.columns)

  @final
  @override
  def handle_binop(self, op, other):
    # TODO(dancol): should we support exotic tables?
    if self.table_schema != REGULAR_TABLE or \
       other.table_schema != REGULAR_TABLE:
      raise InvalidQueryException(
        "scalar-table operations work only on regular tables")
    if len(self.columns) != len(other.columns):
      raise InvalidQueryException(
        "scalar-table operation column count mismatch: {!r} vs {!r}"
        .format(self.columns, other.columns))
    # TODO(dancol): table-length equivalence class
    return GenericQueryTable(
      (lhs, self[lhs].handle_binop(op, other[rhs]))
      for lhs, rhs in zip(self.columns, other.columns))

  @final
  @override
  def make_one_query_node(self):
    if len(self.columns) != 1:
      raise InvalidQueryException(
        "cannot coerce multi-column query table to query")
    return self[first(self.columns)]

  @final
  def column(self, column):
    """Make a query that gives a specific column's values"""
    assert column in self.columns, \
      "column {!r} not in available columns {!r}".format(
        column,
        list(sorted(self.columns)))
    return self._make_column_query(column)

  __getitem__ = column  # Alias

  def items(self):
    """Like a mapping"""
    for column in self.columns:
      yield column, self[column]

  @cached_property
  def columns(self):
    """Set of column names that we can generate"""
    columns = self._make_column_tuple()
    assert isinstance(columns, tuple)
    assert assert_seq_type(tuple, str, columns)
    assert len(set(columns)) == len(columns), "no duplicate columns allowed"
    return columns

  @property
  def column_schemas_for_test(self):
    """List of column schemas.  For test code."""
    return [self[c].schema for c in self.columns]

  @final
  def __contains__(self, column_name):
    return column_name in self.columns

  def countq(self):
    """Make a query that counts the total number of resulting rows"""
    # This generic implementation picks a column and counts
    # it. Subclasses with more specialized knowledge should do
    # something more efficient.
    return CountQuery(self.column(first(sorted(self.columns))))

  @property
  def schema(self):
    """Mapping from column to schema"""
    return dict((column, self.column(column).schema)
                for column in self.columns)

  @abstract
  def _make_column_tuple(self):
    """Return a frozenset of available columns"""
    raise NotImplementedError("abstract")

  @abstract
  def _make_column_query(self, column):
    """Return a query for a specific named column"""
    raise NotImplementedError("abstract")

  @override
  def _no_print_keys(self):
    return super()._no_print_keys() | {"_columns"}

  @staticmethod
  def coerce_(value):
    """Attempt to DWIM and build a QueryTable from the given value"""
    return value if isinstance(value, QueryTable) else \
      GenericQueryTable.of1(QueryNode.coerce_(value))

  def with_table_schema(self, table_schema):
    """Set a new table schema without re-sorting

    Unsafe.  Returns a new QueryTable.
    """
    return self.transform(identity, table_schema=table_schema)

  def transform(self, transformer, *, table_schema=None):
    """Make a new query table with transformed queries

    TRANSFORMER is a function of one argument: a QueryNode.
    Return a new QueryNode to replace the passed-in one.

    The table schema is unchanged if TABLE_SCHEMA is None, the
    default, and is otherwise TABLE_SCHEMA.
    """
    # TODO(dancol): lazily transform columns?
    return GenericQueryTable([
      (column, transformer(self[column]))
      for column in self.columns
    ], table_schema=table_schema or self.table_schema)

def _make_columns_dict(value):
  if not isinstance(value, dict):
    value = dict(value)
  if __debug__:
    for column_name, column_q in value.items():
      assert isinstance(column_name, str)
      assert isinstance(column_q, QueryNode)
  return value

@final
class GenericQueryTable(QueryTable):
  """A QueryTable yielding the result of a select"""

  _columns = iattr(dict,
                   name="columns",
                   converter=_make_columns_dict)

  @override
  def with_table_schema(self, table_schema):
    return self.evolve(table_schema=table_schema)

  @override
  def _make_column_tuple(self):
    return tuple(self._columns)

  @override
  def _make_column_query(self, column):
    return self._columns[column]

  @classmethod
  def of(cls, **columns):
    """Make a GenericQueryTable with the given columns"""
    return cls(columns.items())

  @classmethod
  def of1(cls, q):
    """Make a single-column table

    The single column has the canonical name `_value__`.
    """
    return cls.of(_value__=q)

  @staticmethod
  def rename_columns(qt, renaming):
    """Rename the columns of another query table

    RENAMING is either a sequence of length equal to the number of
    columns in this table (skipping any fixed special columns like _ts
    or _duration) or a dict mapping old column names to renamings.

    The to-values are either the new column names or the empty string
    to indicate that the old column column should be deleted instead
    of renamed.
    """
    schema = qt.table_schema
    fixed = schema.fixed_meta_columns
    assert isinstance(qt, QueryTable)
    if isinstance(renaming, dict):
      remap = dict(renaming)
    else:
      remap = dict(zip([c for c in qt.columns if c not in fixed],
                       renaming))
      if len(remap) != len(renaming):
        raise InvalidQueryException("column renaming length mismatch")
    new_columns = {}
    for column in fixed:
      # TODO(dancol): FIXME: reorders columns unnecessarily But
      # shouldn't the fixed columns always be first anyway?
      new_columns[column] = qt[column]
    partition = schema.partition
    for old_column_name in qt.columns:
      new_column_name = remap.pop(old_column_name, None)
      if new_column_name is None:
        if old_column_name not in new_columns:
          new_column_name = old_column_name
        else:
          continue
      if not isinstance(new_column_name, str):
        raise InvalidQueryException(
          "invalid column name {!r}".format(new_column_name))
      if new_column_name in fixed:
        continue
      if new_column_name:
        new_columns[new_column_name] = qt[old_column_name]
      if new_column_name != old_column_name:
        if new_column_name == partition:
          raise InvalidQueryException(
            "cannot overwrite partition column {!r} with column {!r}"
            .format(partition, old_column_name))
        if old_column_name == partition:
          if not new_column_name:
            raise InvalidQueryException(
              "cannot delete partition column {!r}"
              .format(partition))
          schema = schema.evolve(partition=new_column_name)
          partition = new_column_name
    if remap:
      raise InvalidQueryException(
        "cannot rename absent or special columns {!r}"
        .format(sorted(remap)))
    return GenericQueryTable(new_columns, table_schema=schema)

  @override
  def transform(self, transformer, table_schema=None):
    return GenericQueryTable(
      valmap(transformer, self._columns),
      table_schema=table_schema or self.table_schema)


# Fundamental data and literals support.

_LITERAL_DTYPE_PROMOTION = {
  BOOL: INT64,
  FLOAT32: FLOAT64,
  FLOAT64: FLOAT64,
  INT16: INT64,
  INT32: INT64,
  INT64: INT64,
  INT8: INT64,
  UINT16: UINT64,
  UINT32: UINT64,
  UINT64: UINT64,
  UINT8: UINT64,
}

# Lookup table used by decode_literal.  Note that strings are
# special-cased.
NULL_SCHEMA = QuerySchema(INT8, kind=QuerySchemaKind.MAGIC_NULL)
NULL_FILL_VALUE = NULL_SCHEMA.dtype.type(0)
STRING_SCHEMA = QuerySchema(DTYPE_STRING_ID, kind=QuerySchemaKind.STRING)
_SCHEMA_BY_LITERAL_TYPE = merge(
  {dtype.type: QuerySchema(dtype) for dtype in _LITERAL_DTYPE_PROMOTION},
  {
    # N.B. We override bool from _LITERAL_DTYPE_PROMOTION
    BOOL: QuerySchema(BOOL),
    bool: QuerySchema(BOOL),
    float: QuerySchema(FLOAT64),
    int: QuerySchema(INT64),
  })
TS_SCHEMA = QuerySchema(DTYPE_TS, unit=ureg().ns)
DURATION_SCHEMA = QuerySchema(DTYPE_DURATION, unit=ureg().ns)
INDEXER_SCHEMA = QuerySchema(DTYPE_INDEX, domain="indexer")

def decode_literal(literal):
  """Figure out the raw value and schema from a generic literal

  Return a pair (SCHEMA, RAW_VALUE).  SCHEMA is the implied
  QuerySchema; RAW_VALUE is the value we should store in
  the QueryNode.

  Raise LiteralCoercionError if we can't convert.
  """
  if isinstance(literal, str):
    literal = literal.encode("UTF-8")
  if isinstance(literal, bytes):
    return literal, STRING_SCHEMA
  if literal is None:
    return NULL_FILL_VALUE, NULL_SCHEMA
  unit = None
  try:
    schema = _SCHEMA_BY_LITERAL_TYPE[type(literal)]
  except KeyError:
    # We want a scalar. If we get a QueryNode instead, give it a
    # chance to explain itself.
    if isinstance(literal, QueryNode):
      literal = literal.eager_evaluate_scalar()
      if not isinstance(literal, QueryNode):
        return decode_literal(literal)
    if isinstance(literal, ureg().Quantity):
      unit = literal.units
      literal = literal.m
    try:
      schema = _SCHEMA_BY_LITERAL_TYPE[type(literal)]
    except KeyError:
      raise LiteralCoercionError(literal)
  if schema.dtype is not type(literal):
    literal = schema.dtype.type(literal)
  if unit and not schema.unit:
    # TODO(dancol): use evolve
    assert schema.kind == QuerySchemaKind.NORMAL
    assert not schema.domain
    schema = QuerySchema(schema.dtype, unit=unit)
  return literal, schema


# Unary operation

def _round_fake_ufunc(a, *, signature=None, casting=None,  # pylint: disable=unused-argument
                      where=None, **kwargs):
  # np.round is, alas, not a ufunc, so we have to fake
  # some functionality.
  if where is None:
    return np.round(a, **kwargs)
  if a.dtype is not FLOAT64:
    a = np.asarray(a, FLOAT64)
  res = np.round(a[where])
  out = kwargs.pop("out")
  if out is None:
    out = np.empty(res.dtype, len(a))  # pylint: disable=invalid-unary-operand-type
    out[~where] = 0  # pylint: disable=invalid-unary-operand-type
  else:
    out[where] = res
  return out

def _unop_of_dtype(out_dtype):
  def _make_schema(schema):
    schema.check_no_domain()
    return schema.evolve(dtype=out_dtype)
  return _make_schema

def _fixed_positive_ufunc(a, **kwargs):
  # If we pass where=None, Python interprets that is where=[0] and
  # broadcasts it, turning the operation into a no-op.  :'-(.
  if a.dtype == BOOL:
    a = a.view(INT8)
  return np.positive(a, **kwargs)

def _positive_unop_dtype(schema):
  schema.check_no_domain()
  # +bool should become an int8
  if schema.dtype is BOOL:
    schema = schema.evolve(dtype=INT8)
  return schema.propagate_constraints(C_UNIQUE)

def _fixed_negative_ufunc(a, **kwargs):
  if a.dtype == BOOL:
    a = a.view(INT8)
  return np.negative(a, **kwargs)

def _negative_unop_dtype(schema):
  schema.check_no_domain()
  if schema.dtype is BOOL:
    schema = schema.evolve(dtype=INT8)
  return schema.propagate_constraints(C_UNIQUE)

def _fixed_invert_ufunc(a, **kwargs):
  if a.dtype == BOOL:
    a = a.view(INT8)
  return np.invert(a, **kwargs)

def _invert_unop_dtype(schema):
  schema.check_no_domain()
  if schema.dtype == BOOL:
    schema = schema.evolve(dtype=INT8)
  return schema.propagate_constraints(C_UNIQUE)

def _unop_float64(fn, a, **kwargs):
  if a.dtype is not FLOAT64:
    a = np.asarray(a, FLOAT64)
  return fn(a, **kwargs)

@final
class UnaryOperator(Immutable):
  """Describes the unary operation to perform"""
  fn = iattr(kwonly=True)
  dtype_fn = iattr(kwonly=True)
  supports_strings = iattr(bool, kwonly=True)
  supported_kinds = iattr(str, kwonly=True)

  @property
  def supports_domains(self):
    """Whether this operator supports operands with domains"""
    return self.supports_strings  # Close enough

_UNOP = {
  "!": UnaryOperator(
    fn=np.logical_not,
    dtype_fn=lambda s: QuerySchema(BOOL),
    supports_strings=True,
    supported_kinds=ALL_KINDS),
  "+": UnaryOperator(
    fn=_fixed_positive_ufunc,
    dtype_fn=_positive_unop_dtype,
    supports_strings=False,
    supported_kinds=ALL_KINDS),
  "-": UnaryOperator(
    fn=_fixed_negative_ufunc,
    dtype_fn=_negative_unop_dtype,
    supports_strings=False,
    supported_kinds=ALL_KINDS),
  "ceiling": UnaryOperator(
    fn=partial(_unop_float64, np.ceil),
    dtype_fn=_unop_of_dtype(FLOAT64),
    supports_strings=False,
    supported_kinds="iuf"),
  "floor": UnaryOperator(
    fn=partial(_unop_float64, np.floor),
    dtype_fn=_unop_of_dtype(FLOAT64),
    supports_strings=False,
    supported_kinds="iuf"),
  "round": UnaryOperator(
    fn=_round_fake_ufunc,
    dtype_fn=_unop_of_dtype(FLOAT64),
    supports_strings=False,
    supported_kinds="iuf"),
  "trunc": UnaryOperator(
    fn=partial(_unop_float64, np.trunc),
    dtype_fn=_unop_of_dtype(FLOAT64),
    supports_strings=False,
    supported_kinds="iuf"),
  "~": UnaryOperator(
    fn=_fixed_invert_ufunc,
    dtype_fn=_invert_unop_dtype,
    supports_strings=False,
    supported_kinds="iu"),
}

class UnaryOperationQuery(SimpleQueryNode):
  """Perform an unary operation"""

  op = iattr(str)
  value = iattr(QueryNode)

  @override
  def _post_init_check(self):
    super()._post_init_check()
    operator = _UNOP[self.op]
    schema = self.value.schema
    if self.value.schema.is_string and not operator.supports_strings:
      raise InvalidQueryException(
        "illegal unary operation {} on string".format(self.op))
    if schema.dtype.kind not in operator.supported_kinds:
      raise InvalidQueryException(
        "{} unsupported on {!r}"
        .format(self.op, DTYPE_KINDS[schema.dtype.kind]))

  @override
  def _compute_schema(self):
    value_schema = self.value.schema
    if value_schema.is_null:
      return value_schema.propagate_constraints(C_UNIQUE)
    return _UNOP[self.op].dtype_fn(value_schema)

  @override
  def countq(self):
    return self.value.countq()

  @override
  def _compute_inputs(self):
    return (self.value,)

  @override
  async def run_async(self, qe):
    [ic], [oc] = await qe.async_setup([self.value], [self])
    if oc.query.schema.is_null:
      await passthrough(qe, ic, oc)
      return
    operator = _UNOP[self.op]
    def _transduce(inp):
      array, mask = npy_explode(inp)
      out = qe.qc.make_uninit_array(oc.dtype, len(array))
      if mask is nomask:
        operator.fn(array,
                    signature=(None, oc.dtype),
                    casting="safe",
                    out=out)
        return (out,)
      operator.fn(array,
                  signature=(None, oc.dtype),
                  casting="safe",
                  out=out,
                  where=~mask)
      out[mask] = 0
      return masked_array(out, mask),
    await transduce(qe, [ic], [oc], _transduce)


# Binary operation: define here instead of in queryop because
# QueryNode itself uses BinaryOperationQuery to implement its
# own operations.

def _fix_unit_conversion_ratio(ratio):
  # UGH. THIS IS SUCH A HACK. THE UNIT RATIO SHOULD BE EXACT IF WE CAN
  # DO AN EXACT CONVERSION, NOT SOME BULLSHIT FLOAT.  No, using a
  # fractions.Decimal for the magnitude does not help.  In practice,
  # the threshold here should be a good-enough approximation of a true
  # int ratio.
  rratio = round(ratio)
  difference = abs(rratio - ratio)
  if difference < ratio / 100000.0:
    iratio = int(rratio)
    dtype = np.min_scalar_type(iratio)
    return dtype.type(iratio)
  return np.float64(ratio)

def _find_unit_conversion_ratio(unit_from, unit_to):
  """Return a numpy scalar for the given unit conversion"""
  return _fix_unit_conversion_ratio((1 * unit_from).to(unit_to).m)

def _coerce_to_common_units(left_unit, right_unit, left_array, right_array):
  if left_unit == right_unit:
    return left_array, right_array
  if right_unit < left_unit:
    return _coerce_to_common_units(
      right_unit, left_unit, right_array, left_array)[::-1]
  ratio = _find_unit_conversion_ratio(right_unit, left_unit)
  assert ratio >= 1
  if ratio > 1:
    # TODO(dancol): overflow robustness
    right_array = right_array * ratio
  return left_array, right_array

def _compute_dtype_after_unit_promotion(dtype, from_unit, to_unit):
  if from_unit != to_unit:
    ratio = _find_unit_conversion_ratio(from_unit, to_unit)
    if ratio != 1:
      dtype = dctv_rules_result_type(dtype, ratio.dtype, "+")
  return dtype

def _do_null_propagating_binop(ufunc,
                               dtype,
                               qe,
                               left_array,
                               right_array):
  left_data, left_mask = npy_explode(left_array)
  right_data, right_mask = npy_explode(right_array)

  # For the purposes of arithmetic, bools are int8s.
  if left_data.dtype is BOOL:
    left_data = left_data.view("b")
  if right_data.dtype is BOOL:
    right_data = right_data.view("b")

  # Ordinarily, we use ufuncs in "safe" mode to prevent mode unwanted
  # data-losing dtype conversions.  (But see the mixed-sign case above
  # if you're having a good day and don't like that you're having a
  # good day.)  But when our output is a float, we lose data
  # regardless, so let's allow numpy to convert our operands to float.
  # This way, things like int64+float32 Just Work.
  casting = "safe"
  if dtype.kind == "f":
    casting = "unsafe"
  else:
    # numpy doesn't have loops for mixed signed and unsigned types and
    # will in some cases do a float(?!) conversion if it sees the
    # things.  If we have mixed signedness, cast the unsigned array
    # as signed.
    left_is_unsigned = left_data.dtype.char in UNSIGNED_TYPECODES
    right_is_unsigned = right_data.dtype.char in UNSIGNED_TYPECODES
    if left_is_unsigned ^ right_is_unsigned:
      if dtype.char in UNSIGNED_TYPECODES:
        # We want an unsigned result, so treat the signed operand
        # as unsigned.
        if left_is_unsigned:
          assert not right_is_unsigned
          right_data = right_data.view(right_data.dtype.char.upper())
        else:
          assert not left_is_unsigned
          left_data = left_data.view(left_data.dtype.char.upper())
      else:
        # We want a signed result, so treat the unsigned operand
        # as unsigned.
        if left_is_unsigned:  # pylint: disable=else-if-used
          left_data = left_data.view(left_data.dtype.char.lower())
        else:
          assert right_is_unsigned
          right_data = right_data.view(right_data.dtype.char.lower())

  n = len(left_data)
  assert n == len(right_data)
  out_data = qe.qc.make_uninit_array(dtype, n)

  if left_mask is nomask and right_mask is nomask:
    return ufunc(left_data, right_data,
                 signature=(None, None, dtype),
                 casting=casting,
                 out=out_data)
  if left_mask is not nomask and right_mask is not nomask:
    out_mask = qe.qc.make_uninit_array(BOOL, n)
    np.logical_or(left_mask, right_mask, dtype=BOOL, out=out_mask)
  elif left_mask is nomask:
    out_mask = right_mask
  else:
    out_mask = left_mask
  assert len(out_mask) == n
  ufunc(left_data,
        right_data,
        signature=(None, None, dtype),
        casting=casting,
        out=out_data,
        where=~out_mask)
  out_data[out_mask] = 0
  return masked_array(out_data, out_mask)

def _null_propagating_binop(fn):
  ret = partial(_do_null_propagating_binop, fn)
  ret.do_via_numpy = fn
  return ret

def _do_null_safe_binop(ufunc, dtype, qe, left_array, right_array):
  # Output from this function is never itself masked!
  assert dtype is BOOL
  assert ufunc in (np.equal, np.not_equal)
  left_data, left_mask = npy_explode(left_array)
  right_data, right_mask = npy_explode(right_array)
  n = len(left_data)
  def _call_ufunc(where=None):
    kwargs = {} if where is None else {"where": where}
    return ufunc(left_data, right_data,
                 signature=(None, None, dtype),
                 casting="safe",
                 out=qe.qc.make_uninit_array(dtype, n),
                 **kwargs)
  assert n == len(right_data)
  if left_mask is nomask and right_mask is nomask:
    out_data = _call_ufunc()
  elif left_mask is nomask:
    out_data = _call_ufunc(~right_mask)
    out_data[right_mask] = (ufunc is np.not_equal)
  elif right_mask is nomask:
    out_data = _call_ufunc(~left_mask)
    out_data[left_mask] = (ufunc is np.not_equal)
  else:
    mask = left_mask | right_mask
    np.logical_not(mask, out=mask)
    out_data = _call_ufunc(mask)
    is_eq = (ufunc is np.equal)
    np.logical_and(left_mask, right_mask, out=mask)
    out_data[mask] = is_eq
    np.logical_xor(left_mask, right_mask, out=mask)
    out_data[mask] = not is_eq
  return out_data

def _null_safe_test_wrapper(fn, left, right):
  return npy_get_data(fn(left, right))

def _null_safe_comparison(fn):
  ret = partial(_do_null_safe_binop, fn)
  ret.do_via_numpy = partial(_null_safe_test_wrapper, fn)
  return ret

def _null_safe_and(dtype, qe, left, right):  # pylint: disable=unused-argument
  # TODO(dancol): blockify
  assert dtype is BOOL
  result = np.logical_and(left, right)  # pylint: disable=assignment-from-no-return
  if not npy_has_mask(result):
    return npy_get_data(result)
  # TODO(dancol): access raw data instead of using np.ma.filled, which
  # makes a copy
  left_is_false = np.ma.filled(np.logical_not(left), False)
  right_is_false = np.ma.filled(np.logical_not(right), False)
  assert not np.ma.is_masked(left_is_false)
  assert not np.ma.is_masked(right_is_false)
  result[left_is_false | right_is_false] = False
  return result

_null_safe_and.do_via_numpy = np.logical_and

def _null_safe_or(dtype, qe, left, right):  # pylint: disable=unused-argument
  # TODO(dancol): blockify
  assert dtype is BOOL
  result = np.logical_or(left, right)  # pylint: disable=assignment-from-no-return
  if not npy_has_mask(result):
    return npy_get_data(result)
  # TODO(dancol): access raw data instead of using np.ma.filled, which
  # makes a copy
  left_is_true = np.ma.filled(left.astype(BOOL, copy=False), False)
  right_is_true = np.ma.filled(right.astype(BOOL, copy=False), False)
  assert not np.ma.is_masked(left_is_true)
  assert not np.ma.is_masked(right_is_true)
  result[left_is_true | right_is_true] = True
  return result

def _null_safe_or_for_test(left, right):
  result = np.logical_or(left, right)  # pylint: disable=assignment-from-no-return
  if not npy_has_mask(result):
    return npy_get_data(result)
  # TODO(dancol): access raw data instead of using np.ma.filled, which
  # makes a copy
  left_is_true = np.ma.filled(left.astype(BOOL, copy=False), False)
  right_is_true = np.ma.filled(right.astype(BOOL, copy=False), False)
  assert not np.ma.is_masked(left_is_true)
  assert not np.ma.is_masked(right_is_true)
  result[left_is_true | right_is_true] = True
  return result

_null_safe_or.do_via_numpy = _null_safe_or_for_test

class BinaryOperator(Immutable):
  """Operator for BinaryOperationQuery"""

  name = iattr(str, kwonly=True)

  @abstract
  def perform(self,
              qc,
              query,
              left_array,
              left_schema,
              right_array,
              right_schema):
    """Actually perform the binary operation"""
    raise NotImplementedError("abstract")

  @abstract
  def compute_schema(self, left_schema, right_schema):
    """Compute the schema of the binary operation result"""
    raise NotImplementedError("abstract")

  def munge_input_queries(self, query):  # pylint: disable=no-self-use
    """Potentially transform inputs at the QueryAction level"""
    return query.left, query.right

@final
class ComparisonOperator(BinaryOperator):
  """Operator comparing two quantities"""

  fn = iattr(assert_checker=callable, kwonly=True)
  ordered = iattr(bool, kwonly=True)
  null_safe = iattr(bool, default=False, kwonly=True)

  @override
  def compute_schema(self, left_schema, right_schema):
    if left_schema.is_null or right_schema.is_null:
      if self.null_safe:
        return QuerySchema(BOOL)
      return NULL_SCHEMA  # Auto-propagated
    is_string = left_schema.is_string
    if is_string != right_schema.is_string:
      raise InvalidQueryException(
        "Cannot compare string {} to non-string {}".format(
          left_schema, right_schema))
    if left_schema.domain and right_schema.domain and \
       left_schema.domain != right_schema.domain:
      raise InvalidQueryException(
        "Domain mismatch: {!r} vs {!r}".format(
          left_schema.domain,
          right_schema.domain))
    if left_schema.unit or right_schema.unit:
      if not (left_schema.unit and right_schema.unit):
        raise BadUnitsException(
          "Unit mismatch: {!r} with dimensionless".format(
            left_schema.unit or right_schema.unit))
      try:
        left_schema.unit.__lt__(right_schema.unit)
      except DimensionalityError:
        raise BadUnitsException(
          "Unit mismatch: {!r} and {!r}".format(
            left_schema.unit, right_schema.unit))
    return QuerySchema(BOOL)

  @override
  def perform(self,
              qe,
              query,
              left_array,
              left_schema,
              right_array,
              right_schema):
    either_null = left_schema.is_null or right_schema.is_null
    if either_null and not self.null_safe:
      return left_array if left_schema.is_null else right_array
    assert left_schema.is_string == right_schema.is_string or either_null
    if left_schema.is_string and right_schema.is_string:
      collation = query.collation
      # Strings are interned, so non-ordered comparisons in the
      # "binary" (i.e., identity) collation can just compare the
      # string codes.
      if self.ordered or collation != "binary":
        collation_array = qe.st.rank(collation)
        left_array = _safe_str_fast_take(
          qe.qc, left_array, collation_array)
        right_array = _safe_str_fast_take(
          qe.qc, right_array, collation_array)
    elif not either_null:
      left_unit = left_schema.unit
      assert bool(left_unit) == bool(right_schema.unit)
      if left_unit:
        left_array, right_array = _coerce_to_common_units(
          left_unit, right_schema.unit,
          left_array, right_array)
    return self.fn(BOOL, qe, left_array, right_array)

class RegexpOperator(BinaryOperator):
  """Operator for REGEXP and LIKE"""

  language = iattr(int)

  @override
  def compute_schema(self, left_schema, right_schema):
    if not left_schema.is_string or not right_schema.is_string:
      raise InvalidQueryException(
        "operator must have strings on both sides")
    return QuerySchema(BOOL)

  @override
  def perform(self,
              qe,
              query,
              left_array,
              left_schema,
              right_array,
              right_schema):
    left_array_raw, left_array_mask = npy_explode(left_array)
    right_array_raw, right_array_mask = npy_explode(right_array)
    # The ST already uses allocates from the QC
    result = qe.st.match(
      qe, left_array_raw, right_array_raw, self.language)
    if left_array_mask is nomask and right_array_mask is nomask:
      return result
    out_mask = left_array_mask | right_array_mask
    result[out_mask] = False
    return masked_array(result, out_mask)

@final
class StringConcatenationOperator(BinaryOperator):
  """Operator for performing string concatenation"""

  @override
  def compute_schema(self, left_schema, right_schema):
    if ((left_schema.is_null or right_schema.is_null) and
        (left_schema.is_null or left_schema.is_string) and
        (right_schema.is_null or right_schema.is_string)):
      return NULL_SCHEMA
    if not (left_schema.is_string and right_schema.is_string):
      raise InvalidQueryException("can concatenate only strings")
    return QuerySchema.concat((left_schema, right_schema))

  @override
  def perform(self,
              qe,
              query,
              left_array,
              left_schema,
              right_array,
              right_schema):
    assert len(left_array) == len(right_array)
    # NULL concatenated with anything is NULL, so just reuse the NULL
    # array if we see an always-NULL input.
    if left_schema.is_null:
      return left_array
    if right_schema.is_null:
      return right_array
    assert left_schema.is_string and right_schema.is_string
    left_array, left_mask = npy_explode(left_array)
    right_array, right_mask = npy_explode(right_array)
    # The ST already allocates the output from the QC.
    result = qe.st.concat(qe, left_array, right_array)
    # If we have NULL inputs, make sure the output is NULL in the
    # right places.
    if not (left_array is nomask and right_array is nomask):
      result_mask = left_mask | right_mask
      result[result_mask] = 0
      result = masked_array(result, result_mask)
    return result

class ArithmeticOperator(BinaryOperator):
  """Operator for performing arithmetic operations"""

  fn = iattr(assert_checker=callable, kwonly=True)
  left_supported_kinds = iattr(str, kwonly=True, default=ALL_KINDS)
  right_supported_kinds = iattr(str, kwonly=True, default=ALL_KINDS)
  dctv_result_rule = iattr(str, kwonly=True, default="+")

  @final
  @override
  def compute_schema(self, left_schema, right_schema):
    if left_schema.is_string or right_schema.is_string:
      raise InvalidQueryException(
        "{} unsupported on strings".format(self.name))
    if left_schema.domain or right_schema.domain:
      raise InvalidQueryException(
        "{} unsupported in domain {!r}"
        .format(self.name, left_schema.domain or right_schema.domain))
    if (left_schema.dtype.kind not in self.left_supported_kinds
        and not left_schema.is_null):
      raise InvalidQueryException(
        "{} unsupported on {}".format(
          self.name, DTYPE_KINDS[left_schema.dtype.kind]))
    if (right_schema.dtype.kind not in self.right_supported_kinds
        and not right_schema.is_null):
      raise InvalidQueryException(
        "{} unsupported on {}".format(
          self.name, DTYPE_KINDS[right_schema.dtype.kind]))
    if left_schema.is_null:
      return left_schema
    if right_schema.is_null:
      return right_schema

    dtype = dctv_rules_result_type(left_schema.dtype,
                                   right_schema.dtype,
                                   self.dctv_result_rule)
    left_unit = left_schema.unit
    right_unit = right_schema.unit
    if left_unit or right_unit:
      left_unit = left_unit or ureg().dimensionless
      right_unit = right_unit or ureg().dimensionless
    if not left_unit:
      assert not right_unit
      output_unit = None
    else:
      assert right_unit
      # pylint: disable=assignment-from-no-return
      dtype, output_unit = self._compute_output_dtype_and_unit(
        dtype, left_unit, right_unit)
      if output_unit.dimensionless:
        output_unit = None
    return QuerySchema(dtype, unit=output_unit)

  def _compute_output_dtype_and_unit(self, _dtype, left_unit, right_unit):
    # Subclasses override and allow computation with units
    raise InvalidQueryException(
      "Cannot perform operation {!r} with units {!r} and {!r}".format(
        self.name, left_unit, right_unit))

  def _munge_output_array(self, _query, array):  # pylint: disable=no-self-use
    return array

  @override
  def perform(self,
              qe,
              query,
              left_array,
              left_schema,
              right_array,
              right_schema):
    return self._munge_output_array(
      query,
      self.fn(query.schema.dtype, qe, left_array, right_array))

@final
class ArithmeticOperatorUnitsAdd(ArithmeticOperator):
  """Operator with additive same-dimensionality units"""

  @override
  def munge_input_queries(self, query):
    left_query = query.left
    right_query = query.right
    out_unit = query.schema.unit
    if out_unit:
      left_unit = left_query.schema.unit
      right_unit = right_query.schema.unit
      assert left_unit and right_unit
      if left_unit != out_unit:
        left_query = ConvertToUnitQuery(left_query, out_unit)
      if right_unit != out_unit:
        right_query = ConvertToUnitQuery(right_query, out_unit)
    return left_query, right_query

  @override
  def _compute_output_dtype_and_unit(self, dtype, left_unit, right_unit):
    if left_unit.dimensionality != right_unit.dimensionality:
      raise BadUnitsException(
        "Incompatible units for operation {!r}: {!r} and {!r}".format(
          self.name, left_unit, right_unit))
    if left_unit < right_unit:
      output_unit = left_unit
      dtype = _compute_dtype_after_unit_promotion(
        dtype, right_unit, output_unit)
    elif right_unit < left_unit:
      output_unit = right_unit
      dtype = _compute_dtype_after_unit_promotion(
        dtype, left_unit, output_unit)
    else:
      output_unit = left_unit
    return dtype, output_unit

@final
class ArithmeticOperatorUnitsMul(ArithmeticOperator):
  """Operator with multiplicative different-dimensionality units"""

  def __find_conversion_ratio_unit(self, left_unit, right_unit):
    one = np.int64(1)
    qty = self.fn.do_via_numpy(
      one * left_unit, one * right_unit).to_reduced_units()
    return _fix_unit_conversion_ratio(qty.m), qty.units

  @override
  def _compute_output_dtype_and_unit(self, dtype, left_unit, right_unit):
    # Use an np.int64 instead of just an int so that the numpy
    # operations DTRT on it
    ratio, unit = self.__find_conversion_ratio_unit(left_unit, right_unit)
    if ratio != 1:
      dtype = dctv_rules_result_type(dtype, ratio.dtype, "+")
    return dtype, unit

  @override
  def _munge_output_array(self, query, array):
    if query.schema.unit:
      left_unit = query.left.schema.unit
      right_unit = query.right.schema.unit
      if left_unit and right_unit:
        ratio, _unit = self.__find_conversion_ratio_unit(
          left_unit, right_unit)
        if ratio != 1:
          np.multiply(array,
                      ratio,
                      dtype=query.schema.dtype,
                      out=array,
                      casting="safe")
    return super()._munge_output_array(query, array)

_BINOP = {
  op.name: op for op in (
    ArithmeticOperatorUnitsAdd(
      name="+",
      fn=_null_propagating_binop(np.add),
    ),
    ArithmeticOperatorUnitsAdd(
      name="-",
      fn=_null_propagating_binop(np.subtract),
    ),
    ArithmeticOperatorUnitsMul(
      name="*",
      fn=_null_propagating_binop(np.multiply),
    ),
    ArithmeticOperatorUnitsMul(
      name="/",
      fn=_null_propagating_binop(np.true_divide),
      dctv_result_rule="d",
    ),
    ArithmeticOperator(
      name="//",
      fn=_null_propagating_binop(np.floor_divide),
    ),
    ArithmeticOperator(
      name="%",
      fn=_null_propagating_binop(np.mod),
    ),
    ArithmeticOperator(
      name="<<",
      fn=_null_propagating_binop(np.left_shift),
      left_supported_kinds="iu",
      right_supported_kinds="iu",
    ),
    ArithmeticOperator(
      name=">>",
      fn=_null_propagating_binop(np.right_shift),
      left_supported_kinds="iu",
      right_supported_kinds="iu",
    ),
    ArithmeticOperator(
      name="&",
      fn=_null_propagating_binop(np.bitwise_and),
      left_supported_kinds="iu",
      right_supported_kinds="iu",
      dctv_result_rule="c",
    ),
    ArithmeticOperator(
      name="|",
      fn=_null_propagating_binop(np.bitwise_or),
      left_supported_kinds="iu",
      right_supported_kinds="iu",
      dctv_result_rule="c",
    ),

    StringConcatenationOperator(name="||"),

    ComparisonOperator(
      name="=",
      fn=_null_propagating_binop(np.equal),
      ordered=False),
    ComparisonOperator(
      name="<=>",
      fn=_null_safe_comparison(np.equal),
      ordered=False,
      null_safe=True),
    ComparisonOperator(
      name="<!=>",
      fn=_null_safe_comparison(np.not_equal),
      ordered=False,
      null_safe=True),
    ComparisonOperator(
      name=">=",
      fn=_null_propagating_binop(np.greater_equal),
      ordered=True),
    ComparisonOperator(
      name=">",
      fn=_null_propagating_binop(np.greater),
      ordered=True),
    ComparisonOperator(
      name="<=",
      fn=_null_propagating_binop(np.less_equal),
      ordered=True),
    ComparisonOperator(
      name="<",
      fn=_null_propagating_binop(np.less),
      ordered=True),
    ComparisonOperator(
      name="!=",
      fn=_null_propagating_binop(np.not_equal),
      ordered=False),
    ComparisonOperator(
      name="and",
      fn=_null_safe_and,
      ordered=False,
      null_safe=True),
    ComparisonOperator(
      name="or",
      fn=_null_safe_or,
      ordered=False,
      null_safe=True),

    RegexpOperator(name="regexp", language=MATCH_PCRE),
    RegexpOperator(name="like", language=MATCH_SQL_LIKE),
    RegexpOperator(name="ilike", language=MATCH_SQL_ILIKE),
  )}


@final
class BinaryOperationQuery(SimpleQueryNode):
  """Perform a binary operation"""

  left = iattr(QueryNode)
  op = iattr(str)
  right = iattr(QueryNode)
  collation = iattr(str, default="binary")

  @override
  def _post_init_check(self):
    super()._post_init_check()
    assert self.op in _BINOP, \
      "operator {!r} not in known operator set {!r}".format(
        self.op, sorted(_BINOP))
    assert self.collation in ("binary", "nocase", "length")
    self.precompute_schema()

  @override
  def countq(self):
    # TODO(dancol): multiple choice
    return self.left.countq()

  @override
  def _compute_schema(self):
    return _BINOP[self.op].compute_schema(
      self.left.schema, self.right.schema)

  @cached_property
  def binop_inputs(self):
    """Munged input arrays"""
    return _BINOP[self.op].munge_input_queries(self)

  @override
  def _compute_inputs(self):
    return self.binop_inputs

  def __perform_null(self, left, right):
    assert len(left) == len(right)
    sz = len(left)
    data = npy_broadcast_to(np.zeros(1, self.schema.dtype), sz)
    mask = npy_broadcast_to(True, sz)
    return (masked_array(data, mask),)

  @override
  async def run_async(self, qe):
    [ic_left, ic_right], [oc] = await qe.async_setup(
      self.binop_inputs, [self])
    if self.schema.is_null:
      await transduce(qe, [ic_left, ic_right], [oc], self.__perform_null)
      return
    perform = _BINOP[self.op].perform
    left_schema = self.left.schema
    right_schema = self.right.schema
    def _transduce(left_array, right_array):
      with ignore_numpy_errors():
        out = perform(qe, self,
                      left_array, left_schema,
                      right_array, right_schema)
      assert out.dtype == self.schema.dtype  # Don't allow implicit conversion!
      return (out,)
    await transduce(qe, [ic_left, ic_right], [oc], _transduce)


# CastQuery: define here instead of queryop because QueryNode uses
# CastQuery directly.

@lru_cache(16)
def can_cast_losslessly(from_dtype, to_dtype):
  """Like np.can_cast, but doesn't lie about float casting"""
  # Why does numpy report that we can safely cast anything to float?
  # That's just not true. BAD! Bad Python! Bad cast! Bad!
  # TODO(dancol): check that int64<->uint64 conversion isn't going
  # through a float stage.
  if to_dtype.kind != "f":
    return np.can_cast(from_dtype, to_dtype, casting="safe")
  if from_dtype.kind in "iu":
    from_info = np.iinfo(from_dtype)
    to_info = np.finfo(to_dtype)
    to_max = 2**to_info.nmant
    to_min = -to_max
    if from_info.min >= to_min and to_max >= from_info.max:
      return True
  return False


@lru_cache(16)
def _get_cast_array_checker(from_dtype, to_dtype):
  if can_cast_losslessly(from_dtype, to_dtype):
    return identity  # Always value-preserving
  def _make_bounds_checker(min_allowed, max_allowed):
    def _checker(array):
      bad_value = None
      if min_allowed is not None and array.min() < min_allowed:
        bad_value = array.min()
      elif max_allowed and array.max() > max_allowed:
        bad_value = array.max()
      if bad_value:
        raise BadCastException(
          "cannot safe-cast {} to {}: value {!r} won't fit".format(
            from_dtype, to_dtype, bad_value))
      return array
    return _checker
  from_kind = from_dtype.kind
  to_kind = to_dtype.kind
  if from_kind in "ui" and to_kind == "f":
    nmant = np.finfo(to_dtype).nmant
    return _make_bounds_checker(-(2**nmant), 2**nmant)
  if not (from_kind in "iu" and to_kind in "iu"):
    raise BadCastException(
      "cannot safe-cast {} to {}".format(from_dtype, to_dtype))
  from_iinfo = np.iinfo(from_dtype)
  to_iinfo = np.iinfo(to_dtype)
  from_min = from_iinfo.min
  to_min = to_iinfo.min
  from_max = from_iinfo.max
  to_max = to_iinfo.max
  assert to_max < from_max or to_min > from_min, (
    "to:{!r} from:{!r} to_max:{!r} "
    "from_max:{!r} to_min:{!r} from_min:{!r}".format(
      to_dtype, from_dtype,
      to_max, from_max, to_min, from_min))
  if to_max >= from_max:
    to_max = None
  if to_min <= from_min:
    to_min = None
  return _make_bounds_checker(to_min, to_max)

class CastQuery(SimpleQueryNode):
  """Data type conversion"""

  _query = iattr(QueryNode, name="query")
  _dtype = iattr(np.dtype, name="dtype")
  _safe = iattr(bool, name="safe")

  @override
  def _post_init_check(self):
    super()._post_init_check()
    if self._query.schema.is_string and self._safe:
      raise BadCastException("cannot cast string column")
    if self._safe:
      # pylint: disable=pointless-statement
      self.__array_checker  # For side effect

  @override
  def countq(self):
    return self._query.countq()

  @cached_property
  def __array_checker(self):
    return _get_cast_array_checker(self._query.schema.dtype, self._dtype)

  @override
  def _compute_schema(self):
    base_schema = self._query.schema
    # Preserve the uniqueness constraint only when the cast is
    # value-preserving!  Safe casts are always value-preserving,
    # because they'll fail if they detect an out-of-range value
    lossless = self._safe or \
      can_cast_losslessly(self._query.schema.dtype, self._dtype)
    constraints = (base_schema.constraints & C_UNIQUE) \
      if lossless else frozenset()
    return QuerySchema(self._dtype,
                       domain=base_schema.domain,
                       unit=base_schema.unit,
                       constraints=constraints)

  @override
  def _compute_inputs(self):
    return (self._query,)

  @override
  async def run_async(self, qe):
    # We could just let the query engine do the casting inside the
    # write operation, but let's do it here so that the computational
    # cost gets charged to this operator.  Don't bother optimizing the
    # identity transformation, since QueryNode.to_dtype() should have
    # avoided creating a CastQuery in the first place for an
    # unnecessary conversion.
    [ic], [oc] = await qe.async_setup([self._query], [self])
    checker = self.__array_checker if self._safe else identity
    def _convert(block):
      return (checker(block).astype(self._dtype, copy=False)),
    await transduce(qe, [ic], [oc], _convert)


# ConvertToUnitQuery: used by QueryNode directly
@final
class ConvertToUnitQuery(SimpleQueryNode):
  """Convert an output to a different unit"""

  query = iattr(QueryNode)
  unit = iattr_unit(nullable=True, default=None)
  allow_unitless = iattr(bool, default=False, kwonly=True)

  @override
  def _post_init_check(self):
    super()._post_init_check()
    if not self.query.schema.unit and self.unit and not self.allow_unitless:
      raise InvalidQueryException(
        "cannot unit-convert dimensionless quantity {!r}".format(self.query))

  @override
  def _compute_schema(self):
    base_schema = self.query.schema
    assert not base_schema.is_string
    if not self.unit:
      return base_schema.evolve(unit=None)
    if not base_schema.unit:
      assert self.allow_unitless
      dtype = base_schema.dtype
    else:
      dtype = _compute_dtype_after_unit_promotion(
        base_schema.dtype,
        base_schema.unit,
        self.unit)
    if can_cast_losslessly(base_schema.dtype, dtype):
      constraints = base_schema.constraints & C_UNIQUE
    else:
      constraints = frozenset()
    return base_schema.evolve(dtype=dtype,
                              unit=self.unit,
                              constraints=constraints)

  @override
  def countq(self):
    return self.query.countq()

  @override
  def _compute_inputs(self):
    return (self.query,)

  async def __do_conversion(self, qe, ic, oc, ratio):
    dtype = oc.dtype
    def _transduce(array):
      out = np.multiply(array, ratio,
                        dtype=dtype,
                        casting="safe",
                        out=qe.qc.make_uninit_array(dtype, len(array)))
      return (out,)
    await transduce(qe, [ic], [oc], _transduce)

  @override
  async def run_async(self, qe):
    [ic], [oc] = await qe.async_setup([self.query], [self])
    base_unit = self.query.schema.unit
    if not (self.unit and base_unit):
      # We're not a notional conversion, so pass through blocks
      # unchanged.  TODO(dancol): provide a way for the query engine
      # to wire us out of the graph entirely by rearranging
      # the queues.
      await passthrough(qe, ic, oc)
    else:
      # Actual conversion!
      ratio = _find_unit_conversion_ratio(base_unit, self.unit)
      if ratio == 1:
        await passthrough(qe, ic, oc)
      else:
        await self.__do_conversion(qe, ic, oc, ratio)


# CountQuery: defined here instead of queryop for QueryNode support

@final
class CountQuery(SimpleQueryNode):
  """Query that returns the number of rows in another query"""

  frm = iattr(QueryNode)  # API

  @override
  def _compute_schema(self):
    return QuerySchema(INT64)

  @override
  def _compute_inputs(self):
    return (self.frm,)

  @override
  async def run_async(self, qe):
    block_size = qe.block_size
    (ic,), (oc,) = await qe.async_setup([self.frm], [self])
    counted = 0
    while True:
      block_len = len(await ic.read(block_size))
      if not block_len:
        break
      counted += block_len
    await oc.write([counted], True)

  @override
  def countq(self):
    return QueryNode.scalar(1)


# ScalarQuery

@final
class ScalarQuery(SimpleQueryNode):
  """Query that yields a single value

  Don't call directly.  Instead, use the QueryNode methods.
  """
  value = iattr()
  # Need schema to be an explicit field so that QueryNode interning
  # doesn't combine values that are similar, but different, e.g., 0
  # and False.
  _schema = iattr(QuerySchema)

  @override
  def eager_evaluate_scalar(self):
    return self.value

  @override
  def countq(self):
    return QueryNode.scalar(1)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert isinstance(self.value, bytes) == self.schema.is_string

  @override
  def _compute_schema(self):
    # Don't constrain to unique here: it's unique only by virtue of
    # being a single entry long, which might change(?) in context.
    # Let's be conservative with constraints.
    return self._schema

  @override
  def _do_to_dtype(self, dtype, safe):
    schema = self.schema
    if schema.is_integral:
      if safe and not can_cast_losslessly(schema.dtype, dtype):
        _get_cast_array_checker(schema.dtype, dtype)(
          schema.dtype.type(self.value))
      return ScalarQuery(self.value, self._schema.evolve(dtype=dtype))
    return super()._do_to_dtype(dtype, safe)

  @override
  def _compute_inputs(self):
    return ()

  @override
  async def run_async(self, qe):
    _, (oc,) = await qe.async_setup((), (self,))
    value = self.value
    if isinstance(value, bytes):
      value = qe.st.intern(value)
    schema = self.schema
    dtype = schema.dtype
    if schema.is_null:
      assert value == 0  # pylint: disable=compare-to-zero
      array = masked_array([value], [True], dtype=dtype)
    else:
      array = np.full(1, value, dtype)
    await oc.write(array, True)


# LockstepFilledArrayQuery

class LockstepFilledArrayQuery(SimpleQueryNode):
  """Query that broadcasts a query to the length of some other query"""
  fill_value = iattr(QueryNode)
  template = iattr(QueryNode)

  @override
  def countq(self):
    return self.template.countq()

  @override
  def _compute_inputs(self):
    return (
      self.fill_value,
      self.template,
    )

  @override
  def _compute_schema(self):
    return self.fill_value.schema.unconstrain()

  @override
  def eager_evaluate_scalar(self):
    if self.template.countq().eager_evaluate_scalar() == 1:
      return self.fill_value.eager_evaluate_scalar()
    return super().eager_evaluate_scalar()

  async def _do_passthrough(self,
                            qe,
                            oc_filled,
                            ic_fill_value,
                            fill_block,
                            ic_template,
                            template_block):
    block_size = qe.block_size
    rows_delivered = 0
    while True:
      if len(fill_block) != len(template_block):
        die_on_length_mismatch(self.fill_value,
                               rows_delivered + len(fill_block),
                               rows_delivered + len(template_block))
      if not fill_block:
        break
      is_eof = len(fill_block) < block_size
      rows_delivered += len(fill_block)
      req = qe.async_io(
        ic_fill_value.read(block_size),
        ic_template.read(block_size),
        oc_filled.write(fill_block, is_eof))
      del fill_block, template_block
      [fill_block, template_block, _] = await req
    assert not fill_block
    assert not template_block

  async def _do_broadcast(self,
                          qe,
                          oc_filled,
                          fill_block,
                          ic_template,
                          template_block):
    assert len(fill_block) == 1
    broadcast_array = fill_block.as_array()
    block_size = qe.block_size
    while template_block:
      chunk_size = len(template_block)
      filled_array = masked_broadcast_to(broadcast_array, chunk_size)
      is_eof = chunk_size < block_size
      [template_block, _] = await qe.async_io(
        ic_template.read(block_size),
        oc_filled.write(filled_array, is_eof))

  @override
  async def run_async(self, qe):
    [ic_fill_value, ic_template], [oc_filled] = await qe.async_setup(
      (self.fill_value, self.template), [self])
    fill_block, template_block = await qe.async_io(
      ic_fill_value.read(max(2, qe.block_size)),
      ic_template.read(max(2, qe.block_size)))
    if len(fill_block) == 1 and len(template_block) != 1:
      await self._do_broadcast(qe,
                               oc_filled,
                               fill_block,
                               ic_template,
                               template_block)
    else:
      await self._do_passthrough(qe,
                                 oc_filled,
                                 ic_fill_value,
                                 fill_block,
                                 ic_template,
                                 template_block)


# FilledArrayQuery

@final
class FilledArrayQuery(SimpleQueryNode):
  """Query that returns an array filled with a value

  Do not construct directly.  Call QueryNode.filled()!
  """
  fill_value = iattr(QueryNode)
  count = iattr_query_node_int()

  @override
  def _compute_schema(self):
    return self.fill_value.schema.unconstrain()

  @override
  def countq(self):
    return self.count

  @override
  def _compute_inputs(self):
    return (
      self.fill_value,
      self.count,
    )

  @override
  def eager_evaluate_scalar(self):
    if self.count.eager_evaluate_scalar() == 1:
      fill_value = self.fill_value.eager_evaluate_scalar()
      if not isinstance(fill_value, QueryNode):
        return fill_value
    return super().eager_evaluate_scalar()


  async def __do_broadcast(self, qe, wanted_rows, chunk_block, oc_filled):
    assert len(chunk_block) == 1
    broadcast_array = chunk_block.as_array()
    assert len(broadcast_array) == 1
    rows_delivered = 0
    block_size = qe.block_size
    while rows_delivered < wanted_rows:
      chunk_size = min(wanted_rows - rows_delivered, block_size)
      chunk_array = masked_broadcast_to(broadcast_array, chunk_size)
      rows_delivered += chunk_size
      await oc_filled.write(chunk_array, rows_delivered == wanted_rows)

  async def __do_passthrough(self,
                             qe,
                             wanted_rows,
                             chunk_block,
                             oc_filled,
                             ic_fill_value):
    block_size = qe.block_size
    rows_delivered = 0
    while rows_delivered < wanted_rows:
      chunk_size = min(wanted_rows - rows_delivered, block_size)
      if len(chunk_block) < chunk_size:
        die_on_length_mismatch(self.fill_value,
                               rows_delivered + len(chunk_block),
                               wanted_rows)
      rows_delivered += len(chunk_block)
      is_eof = rows_delivered == wanted_rows
      req = qe.async_io(ic_fill_value.read(block_size),
                        oc_filled.write(chunk_block, is_eof))
      del chunk_block
      chunk_block, _ = await req
    if chunk_block:
      die_on_length_mismatch(self.fill_value,
                             rows_delivered + len(chunk_block),
                             wanted_rows)

  @override
  async def run_async(self, qe):
    (ic_fill_value, ic_count), (oc_filled,) = await qe.async_setup(
      (self.fill_value, self.count), (self,))
    # Make sure we can distinguish broadcasts even with block_size == 1
    chunk_block, wanted_rows = \
      await qe.async_io(ic_fill_value.read(max(2, qe.block_size)),
                        ic_count.read_int())
    assert the(int, wanted_rows) >= 0
    # We can be called in one of two modes: either the base query
    # (i.e.  self.fill_block) gives us an array of length one that we
    # broadcast to the desired length or we assert that the underlying
    # query _already_ has the desired length and just forward its
    # blocks unchanged.
    if len(chunk_block) == 1 and wanted_rows != 1:
      await self.__do_broadcast(qe, wanted_rows, chunk_block, oc_filled)
    else:
      await self.__do_passthrough(qe, wanted_rows,
                                  chunk_block, oc_filled, ic_fill_value)


# IfQuery: defined here instead of queryop for QueryNode

class IfQuery(SimpleQueryNode):
  """Implements SQL IF"""

  selector = iattr(QueryNode)
  choice1 = iattr(QueryNode)
  choice2 = iattr(QueryNode)

  @override
  def countq(self):
    # TODO(dancol): multiple choice
    return self.selector.countq()

  @override
  def _post_init_check(self):
    super()._post_init_check()
    self.precompute_schema()

  @override
  def _compute_schema(self):
    return QuerySchema.concat((self.choice1.schema, self.choice2.schema))

  @override
  def _compute_inputs(self):
    return self.selector, self.choice1, self.choice2

  def __null_safe_where(self, cond, left, right):
    # N.B. np.where won't let us specify an output array, so we have
    # to let it allocate one.  Why not use np.choose?  Because choose
    # converts its condition array to NP_INTP internally, and we
    # usually have bool input, and that conversion is worse than
    # letting np.where output the output array.

    # TODO(dancol): write our own damned choose/where/etc.

    # Zero-under-NULL lets us treat NULL as zero
    cond = npy_get_data(cond)
    if not npy_has_mask(left) and not npy_has_mask(right):
      result = np.where(cond, left, right)
    else:
      result = np.ma.where(cond, left, right)
    assert result.dtype == self.schema.dtype
    return (result,)

  @override
  async def run_async(self, qe):
    ics, [oc] = await qe.async_setup(
      [self.selector, self.choice1, self.choice2],
      [self])
    await transduce(qe, ics, [oc], self.__null_safe_where)


# SequentialQuery: defined here instead of queryop for QueryNode

class SequentialQuery(SimpleQueryNode):
  """Query that yields a sequence of values"""

  start = iattr_query_node_int()
  count = iattr_query_node_int()
  step = iattr_query_node_int()

  @override
  def _compute_schema(self):
    # A sequential sequence is _conceptually_ unique.
    return _SCHEMA_BY_LITERAL_TYPE[INT64.type].constrain(C_UNIQUE)

  @override
  def countq(self):
    return self.count

  @override
  def _compute_inputs(self):
    return self.start, self.count, self.step

  @override
  async def run_async(self, qe):
    [ic_start, ic_count, ic_step], [oc] = \
      await qe.async_setup(
        [self.start, self.count, self.step],
        [self])
    start, count, step = await qe.async_io(
      ic_start.read_int(),
      ic_count.read_int(),
      ic_step.read_int())
    rows_delivered = 0
    while rows_delivered < count:
      chunk_size = min(count - rows_delivered, qe.block_size)
      # TODO(dancol): allocate from qc (requires custom arange)
      chunk = np.arange(start, start + chunk_size, step, self.schema.dtype)
      rows_delivered += len(chunk)
      start += step * len(chunk)
      req = oc.write(chunk, rows_delivered == count)
      del chunk
      await req


# TakeQuery: defined here instead of queryop for QueryNode

class TakeQueryBase(SimpleQueryNode):
  """Base class for take operations"""

  __abstract__ = True

  value_query = iattr(QueryNode)
  # TODO(dancol): enforce schema requirements on index_query
  index_query = iattr(QueryNode)

  @final
  @override
  def _compute_schema(self):
    value_schema = self.value_query.schema
    index_schema = self.index_query.schema
    constraints = set()
    if (UNIQUE in value_schema.constraints and
        UNIQUE in index_schema.constraints):
      constraints.add(UNIQUE)
    if constraints == value_schema.constraints:
      return value_schema
    return value_schema.evolve(constraints=constraints)

  @final
  @override
  def _compute_inputs(self):
    return (
      self.value_query,
      self.index_query,
    )

  @final
  @override
  def countq(self):
    return self.index_query.countq()

@final
class TakeQuery(TakeQueryBase):
  """Take values from a column given by an array"""

  @staticmethod
  def __take_chunk_slow_path(qc, value, index, old_mask, has_nulls):
    # N.B. we can't optimize a broadcast here by reusing a broadcasted
    # value array because we need to set any values "under" a -1 to
    # zero, breaking the broadcast.

    # TODO(dancol): see how often we actually hit this case and maybe
    # change the zero-under-null requirement if it'd result in a
    # big win.
    index_lt_zero_mask = index < 0
    if old_mask is nomask:
      chunk_null_mask = index_lt_zero_mask
    else:
      # We need to permute the old mask, otherwise NULLs will end up in
      # the wrong place in the result.
      assert len(old_mask) >= 1
      assert len(old_mask) == len(value)
      # Index contains -1 where we want the output to be masked.
      # Here, -1 takes from the last element of old_mask.  If that
      # element is true, we get what we want by accident.  If it's
      # false, we need to fix up chunk_null_mask after the take.
      chunk_null_mask = old_mask.take(
        index,
        out=qc.make_uninit_array(BOOL, len(index)))
      if has_nulls and not old_mask[-1]:
        chunk_null_mask[index_lt_zero_mask] = True
      value = value.data
    assert chunk_null_mask.dtype is BOOL
    assert len(chunk_null_mask) == len(index)
    if not len(value):  # pylint: disable=len-as-condition
      if not index_lt_zero_mask.all():
        raise InvalidQueryException(
          "bad indexer offsets for empty array")
      value = npy_broadcast_to(np.zeros(1, value.dtype),
                               len(chunk_null_mask))
    else:
      value = value.take(index,
                         out=qc.make_uninit_array(
                           value.dtype, len(index)))
      value[index_lt_zero_mask] = 0
    return masked_array(value, chunk_null_mask)

  @classmethod
  def __take_chunk(cls, qc, value, index):
    # pylint: disable=len-as-condition
    assert len(index)
    indexer_bcast = npy_get_broadcaster(index)
    if indexer_bcast is not None:
      # If the index array is broadcast, we can just take once and
      # broadcast the result.
      assert not indexer_bcast.ndim or len(indexer_bcast) == 1
      return masked_broadcast_to(
        cls.__take_chunk(qc, value, indexer_bcast), len(index))
    if isinstance(index, masked_array):  # Make assertion?
      raise InvalidQueryException("cannot have null index in TakeQuery")
    is_sequential, has_nulls = study_index_for_take(index)
    if not has_nulls:
      # Fast path: identity take.  Just pass through the
      # original block.
      if (is_sequential and
          len(index) and
          len(index) == len(value)
          and not index[0]):
        return value
      # Fast path: indexing into a broadcasted array.  If the value is
      # a broadcast, taking from any index yields the same value, so
      # instead of doing the take, we can just re-broadcast to the
      # length of the take sequence.
      value_bcast = npy_get_broadcaster(value)
      if value_bcast is not None:
        assert not value_bcast.ndim or len(value_bcast) == 1
        return masked_broadcast_to(value_bcast, len(index))
    old_mask = npy_get_mask(value)
    # Fast path: direct native take from the value array
    if old_mask is nomask and not has_nulls:
      if not len(value):
        raise InvalidQueryException("bad indexer offsets for empty array")
      return npy_get_data(value).take(
        index,
        out=qc.make_uninit_array(value.dtype, len(index)))
    return cls.__take_chunk_slow_path(
      qc, value, index, old_mask, has_nulls)

  @override
  async def run_async(self, qe):
    [ic_value, ic_index], [oc] = await qe.async_setup(
      [self.value_query, self.index_query],
      [self])
    # TODO(dancol): do a jive join once we have better QueryNode
    # statistics and we know that the indexer array is much smaller
    # than the value array.
    block_size = qe.block_size
    value, index = await qe.async_io(ic_value.read_all(),
                                     ic_index.read_at_least(block_size))
    value_array = value.as_array()
    del value
    out_io = None
    qc = qe.qc
    while index:
      out_io = oc.write(
        self.__take_chunk(qc, value_array, index.as_array()),
        len(index) < block_size)
      del index
      index, _ = await qe.async_io(ic_index.read(block_size), out_io)
      out_io = None
    if out_io:
      del value_array
      await out_io

class SequentialTakeQueryBase(TakeQueryBase):
  """Take values from a sequence by nondecreasing index.

  The special insert-a-NULL indexer value -1 is not subject to the
  sorting constraint.
  """

  bcast_conservative = True

  @final
  @override
  async def run_async(self, qe):
    [ic_value, ic_index], [oc] = await qe.async_setup(
      [self.value_query, self.index_query],
      [self])
    block_size = qe.block_size
    sto = SequentialTaker(qe.qc, oc.dtype, self.bcast_conservative)
    out = None
    while out is None or out:
      req = qe.async_io(
        (ic_index.read(block_size) if sto.want_index
         else qe.async_dummy()),
        (ic_value.read(block_size) if sto.want_value
         else qe.async_dummy()),
        (oc.write(out, len(out) < block_size)
         if out is not None
         else qe.async_dummy()))
      del out
      out = sto.step(*(await req)[:2])

@final
class SequentialTakeQuery(SequentialTakeQueryBase):
  """Sequential take with conservative broadcasting"""

@final
class SequentialTakeQueryEagerBroadcast(SequentialTakeQueryBase):
  """Sequential take with eager broadcasting"""
  bcast_conservative = False
  __inherit__ = dict(bcast_conservative=override)


# Max/min

# TODO(dancol): now that we have a query execution system that can
# interleave execution and isn't totally awful, do we even need
# GreatestLeastQuery?  Maybe we should just generalize the
# choice operator?

@final
class GreatestLeastQuery(SimpleQueryNode):
  """Perform a greatest or least operation"""

  _op = iattr(str, name="op")

  # TODO(dancol): should be sattr.
  _queries = tattr(QueryNode, name="queries")

  @override
  def _post_init_check(self):
    super()._post_init_check()
    assert self._op in ("greatest", "least")
    assert len(self._queries) >= 1
    self.precompute_schema()

  @override
  def _compute_schema(self):
    return QuerySchema.concat((q.schema for q in self._queries))

  @override
  def _compute_inputs(self):
    return self._queries

  @staticmethod
  def __handle_string_chunk(argfn, st, dtype, arrays):
    # Fetch the ranks for each chunk (without copy) in case query
    # execution interned a string.
    ranks = st.rank("binary")
    # N.B. blob is a 2D array!  Multidimensional arrays seldom appear
    # in DCTV, but here we use, meaning that _safe_str_fast_take won't
    # work and we have to just use np.ma.take instead.
    blob = np.ma.asanyarray(arrays, dtype=dtype)
    # TODO(dancol): take into qc buffer
    return np.take_along_axis(
      blob,
      np.expand_dims(argfn(np.ma.take(ranks, blob), axis=0),
                     axis=0),
      axis=0).ravel()

  @override
  async def run_async(self, qe):
    ics, [oc] = await qe.async_setup(self._queries, [self])
    dtype = self.schema.dtype
    if len(ics) == 1:
      fn = first
    elif self.schema.is_string:
      # For strings, we currently just use the np.ma stuff, even
      # though it's somewhat less efficient than the numeric code
      # below (it allocates intermediate buffers)
      if self._op == "greatest":
        argfn = np.ma.argmax
      else:
        argfn = np.ma.argmin
      fn = partial(self.__handle_string_chunk, argfn, qe.st, dtype)
    else:
      fn = partial(reduce_ufunc_masked,
                   qe,
                   np.maximum if self._op == "greatest" else np.minimum,
                   dtype)
    def _transduce(*args):
      return (fn(args),)
    await transduce(qe, ics, [oc], _transduce)


# Windowing support

# The two distinct window configuration objects let us specify window
# configuration without ambiguity.

class Window(EqImmutable):
  """Window configuration for WindowQuery"""

  @abstract
  def get_queries(self):
    """Get a sequence of queries that the window uses"""
    raise NotImplementedError("abstract")

  @abstract
  def config_to_bounds(self, config):
    """Transform window expression into a (LOW, HIGH) slice pair

    LOW must be an integer.  HIGH may be an integer or None.
    """
    raise NotImplementedError("abstract")

@final
class SliceWindow(Window):
  """Python-style window configuration"""
  low = iattr_query_node_int()
  high = iattr_query_node_int()

  @override
  def get_queries(self):
    return self.low, self.high

  @override
  def config_to_bounds(self, config):
    low, high = config
    low = 0 if low is None else query_typecheck(low, int, self.low)
    if high is not None:
      query_typecheck(high, int, self.high)
    return low, high

@final
class LimitOffsetWindow(Window):
  """SQL-style window configuration"""
  limit = iattr_query_node_int()
  offset = iattr_query_node_int()

  @override
  def get_queries(self):
    return self.limit, self.offset

  @override
  def config_to_bounds(self, config):
    limit, offset = config
    low = 0 if offset is None \
      else query_typecheck(offset, int, self.offset)
    if limit is None:
      high = None
    else:
      query_typecheck(limit, int, self.limit)
      if limit < 0:
        raise InvalidQueryException("negative limit not supported")
      high = limit if low <= 0 else low + limit
    return low, high

@final
class WindowQuery(SimpleQueryNode):
  """Selects a piece of a result vector"""

  _query = iattr(QueryNode, name="query")
  _window = iattr(Window, name="window")

  @override
  def _compute_schema(self):
    return self._query.schema.propagate_constraints(C_UNIQUE)

  @override
  def _compute_inputs(self):
    return (self._query,) + self._window.get_queries()

  @staticmethod
  async def __skip_prefix(qe, ic_data, rows_to_skip):
    assert 0 <= the(int, rows_to_skip)
    while rows_to_skip:
      block = await ic_data.read(min(qe.block_size, rows_to_skip))
      assert len(block) <= rows_to_skip
      if not block:
        break
      rows_to_skip -= len(block)

  async def __do_streaming_limit_offset(self, qe, oc, ic_data, limit, offset):
    await self.__skip_prefix(qe, ic_data, offset)
    if limit is None:
      await passthrough(qe, ic_data, oc)
      return
    assert 0 <= the(int, limit)
    last_write_req = None
    while limit:
      block, _ = await qe.async_io(ic_data.read(min(qe.block_size, limit)),
                                   last_write_req or qe.async_dummy())
      last_write_req = None
      if not block:
        break
      limit = limit - len(block)
      last_write_req = oc.write(block, not limit)
    if last_write_req:
      await last_write_req

  @staticmethod
  async def __do_brute_force(oc, ic_data, low, high):
    # We don't know how to do this operation in a streaming way, so
    # just slurp the whole array and slice it.
    data = (await ic_data.read_all()).as_array()
    out = data[low : high]
    del data
    io = oc.write(out, True)
    del out
    await io

  @override
  async def run_async(self, qe):
    in_queries = (self._query,) + self._window.get_queries()
    [ic_data, *ics_control], [oc] = await qe.async_setup(in_queries, [self])
    config = await qe.async_io(*[ic.read_scalar() for ic in ics_control])
    low, high = self._window.config_to_bounds(config)
    assert isinstance(low, int)
    assert isinstance(high, (int, NoneType))
    if 0 <= low and (high is None or 0 <= high):
      offset = low
      limit = None if high is None else max(0, high - low)
      await self.__do_streaming_limit_offset(qe, oc, ic_data, limit, offset)
    else:
      # TODO(dancol): moar smarts
      await self.__do_brute_force(oc, ic_data, low, high)


# Concatenation

class ConcatenatingQuery(SimpleQueryNode):
  """QueryNode concatenating several result vectors"""

  _queries = tattr(QueryNode, name="queries")

  @override
  def _compute_schema(self):
    if not self._queries:
      # The empty vector is trivially UNIQUE.
      return QuerySchema(INT64, constraints=C_UNIQUE)
    return QuerySchema.concat(
      map(lambda q: q.schema, self._queries))

  @override
  def _compute_inputs(self):
    return self._queries

  @override
  async def run_async(self, qe):
    ics, [oc] = await qe.async_setup(self._queries, [self])
    async def _get_block(ic, out_req):
      if out_req:
        return await qe.async_io(ic.read(qe.block_size), out_req)
      return (await ic.read(qe.block_size)), None
    ics = list(reversed(ics))
    out_req = None
    while ics:
      ic = ics.pop()
      block, out_req = await _get_block(ic, out_req)
      while block:
        is_block_eof = len(block) < qe.block_size
        is_eof = not ics and is_block_eof
        out_req = oc.write(block, is_eof)
        if is_block_eof:
          break
        block, out_req = await _get_block(ic, out_req)
    if out_req:
      await out_req


# Literals

@final
class LiteralQuery(SimpleQueryNode):
  """QueryNode that returns a literal sequence of values

  This query is used for VALUES in SQL.  It's intended to be used with
  a fairly small number of values.  If you want to provide a humongous
  number of values to the query engine, you probably want some other
  form of data input.
  """
  # It's essential to keep the schema as an actual property of the
  # LiteralQuery!  Without it, Interned will wrongly combine
  # LiteralQuery nodes with values that compare equal but that are
  # different (e.g., 0 and False).
  values = tattr()
  null_mask = tattr(bool)
  _schema = iattr(QuerySchema)

  @override
  def __new__(cls, literals):
    # TODO(dancol): mark the schema unique if the values
    # given are in fact unique?
    if not literals:
      values = ()
      null_mask = ()
      schema = _SCHEMA_BY_LITERAL_TYPE[INT64.type]
    else:
      values, raw_schemas = zip(*map(decode_literal, literals))
      schema = QuerySchema.concat(raw_schemas)
      null_mask = [value is None for value in literals]
      if schema.is_string and any(null_mask):
        # If we're a string and we have nulls, we use the empty string
        # instead of zero as the NULL placeholder so that vintern
        # doesn't choke.
        values = [b"" if is_null else value
                  for is_null, value in zip(null_mask, values)]

    return cls._do_new(cls, values, null_mask, schema)

  @override
  def _compute_schema(self):
    return self._schema

  @override
  def _compute_inputs(self):
    return ()

  @override
  async def run_async(self, qe):
    [], [oc] = await qe.async_setup((), (self,))
    # Don't be tempted to cache the values between runs: we need to
    # intern the strings each time for correctness.
    values = self.values
    if self.schema.is_string:
      array = qe.st.vintern(values)
      assert array.dtype == DTYPE_STRING_ID
    else:
      try:
        array = np.asarray(values, dtype=self.schema.dtype)
      except OverflowError:
        # Looks unsafe, but we checked in schema concat.
        if self.schema.dtype is not INT64:
          raise
        array = np.asarray(values, dtype=UINT64).view(INT64)
    if any(self.null_mask):
      array = masked_array(array, self.null_mask)  # pylint: disable=redefined-variable-type
    await oc.write(array, True)


# Coalesce

class CoalesceQuery(SimpleQueryNode):
  """Implement SQL COALESCE"""

  _queries = tattr(QueryNode, name="queries")

  @override
  def _post_init_check(self):
    super()._post_init_check()
    if not self._queries:
      raise InvalidQueryException("COALESCE called with no arguments")
    self.precompute_schema()

  @classmethod
  def of(cls, *args):
    """Build a coalesce query from arguments"""
    return cls(args)

  @override
  def _compute_schema(self):
    return QuerySchema.concat((q.schema for q in self._queries))

  @override
  def _compute_inputs(self):
    return self._queries

  @override
  async def run_async(self, qe):
    ics, [oc] = await qe.async_setup(self._queries, [self])
    dtype = self.schema.dtype
    # Ugh. This is ugly.  Maybe just do it in numba?
    def _transduce(*chunk):
      assert all_same(map(len, chunk))
      nchunk = len(chunk[0])
      out = qe.qc.make_uninit_array(dtype, nchunk)
      out_mask = qe.qc.make_uninit_array(BOOL, nchunk)
      out_mask[:] = True
      for array in chunk:
        array_data, array_mask = npy_explode(array)
        if array_mask is nomask:
          array_inv_mask = True
        else:
          array_inv_mask = ~array_mask
        array_newval_mask = out_mask & array_inv_mask
        np.copyto(out, array_data, where=array_newval_mask)
        np.copyto(out_mask, False, where=array_newval_mask)
      np.copyto(out, 0, where=out_mask)
      return (masked_array(out, out_mask),)
    await transduce(qe, ics, [oc], _transduce)



@final
class FilterQuery(SimpleQueryNode):
  """Select a subset of rows according to a bool array"""

  cond = iattr(QueryNode)
  frm = iattr(QueryNode)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert self.cond.schema.dtype is BOOL

  @override
  def _compute_schema(self):
    return self.frm.schema.propagate_constraints(C_UNIQUE)

  @override
  def _compute_inputs(self):
    return self.frm, self.cond

  @override
  async def run_async(self, qe):
    def _do_filter(cond, frm):
      cond_array = npy_get_data(cond.as_array())
      assert cond_array.dtype == BOOL
      # pylint: disable=using-constant-test
      if True:  # Figure out when this optimization makes sense
        cond_sum = cond_array.sum()
        if not cond_sum:
          return np.empty(0, self.schema.dtype)
        if cond_sum == len(cond_array):
          return frm  # No take!  TODO(dancol): write our own
      # "compress" that takes directly from the input into the output
      # instead of just calling take with np.nonzero, which is sadly
      # what the stock compress does.
      return frm.as_array()[cond_array]
    [ic_cond, ic_frm], [oc] = await qe.async_setup(
      [self.cond, self.frm], [self])
    block_size = qe.block_size
    cond, frm = await qe.async_io(ic_cond.read(block_size),
                                  ic_frm.read(block_size))
    while True:
      assert len(frm) == len(cond)
      if not frm:
        break
      out_io = oc.write(
        _do_filter(cond, frm), len(frm) < block_size)
      del frm, cond
      cond, frm, _ = await qe.async_io(
        ic_cond.read(block_size),
        ic_frm.read(block_size),
        out_io)


# Sorting

@final
class CollateQuery(SimpleQueryNode):
  """Resolve string codes to their collation offsets

  We use this query as a hidden intermediate value when doing an ORDER
  BY a string.
  """

  base = iattr(QueryNode)
  collation = iattr(str)

  @override
  def _post_init_check(self):
    super()._post_init_check()
    if not self.base.schema.is_string:
      raise InvalidQueryException("cannot COLLATE a non-string")

  @override
  def _compute_schema(self):
    return QuerySchema(DTYPE_STRING_RANK, domain="ranks")

  @override
  def _compute_inputs(self):
    return (self.base,)

  @override
  def countq(self):
    return self.base.countq()

  @override
  async def run_async(self, qe):
    [ic], [oc] = await qe.async_setup([self.base], [self])
    # We need to read the entire result set so that we can collate all
    # at once.  If we were to use transduce and the rank tables
    # changed underneath us, between transduce worker function calls,
    # we'd produce incoherent results.
    # TODO(dancol): make this more efficient when we do external
    # sorting.
    codes = await ic.read_all()
    # N.B. Need to wait for string input to arrive
    # before we get the ranks table
    req = oc.write(
      _safe_str_fast_take(qe.qc,
                          codes.as_array(),
                          qe.st.rank(self.collation)),
      True)
    del codes
    await req

def _munge_argsorts(sorts):
  munged_sorts = []
  def _add_sort(query, ascending):
    assert isinstance(query, QueryNode)
    munged_sorts.append((query, bool(ascending)))
  for sort in sorts:
    _add_sort(*sort)
  return tuple(munged_sorts)

@final
class ArgSortQuery(SimpleQueryNode):
  """Query that computes a lexicographical sort order

The result is useful for using as the index input to TakeQuery.
  """
  sorts = tattr(converter=_munge_argsorts)

  @override
  def _compute_schema(self):
    # Argsorting yields an indexer with unique indices
    return INDEXER_SCHEMA.constrain(C_UNIQUE)

  @override
  def _compute_inputs(self):
    return (sort[0] for sort in self.sorts)

  @override
  def countq(self):
    # TODO(dancol): multiple choice
    return self.sorts[0].countq()

  def __compute_sort_indexer(self, qc, blocks):
    assert len(blocks) == len(self.sorts)
    keys = []
    for (query, ascending), block in zip(self.sorts, blocks):
      assert not query.schema.is_string, \
        "users of ArgSortQuery need to pre-process strings " \
        "with CollateQuery"
      array = block.as_array()
      array, mask = npy_explode(array)
      if mask is not nomask and not npy_is_broadcasted(mask):
        # TODO(dancol): PostgreSQL has a bit of SQL syntax to control
        # whether NULLs end up on top or at the bottom; maybe we
        # should support it.
        keys.append(mask if ascending else ~mask)
      if npy_is_broadcasted(array):
        pass  # Can't affect sorting
      elif not ascending:
        keys.append(array * -1)
      else:
        keys.append(array)
    if keys:
      keys.reverse()  # For numpy, whose sort is little-"endian"
      return fast_argsort(qc, keys)
    # No sorting to be done at all
    # TODO(dancol): generate in memory block
    return np.arange(len(blocks[0]), dtype=INDEXER_SCHEMA.dtype)

  @override
  async def run_async(self, qe):
    # TODO(dancol): exterior sorting for huge arrays

    # TODO(dancol): radix sort.  Right now, sorting begins taking a
    # very long time (~5s) once we hit five million rows or so.
    # Pandas has a "groupsort" (a counting sort) that, combined with
    # factorization, would let us sort in linear time.  The problem is
    # that the factorization step requires that in addition to
    # discovering the unique factors in the first place, we sort the
    # factors, and right now, this sorting is still done by comparison
    # sort.  If we have an array with a large number of distinct
    # factors (e.g., trace timestamps), this factor-sorting operation
    # would dominate the total time and leave us even worse off than
    # if we'd just sorted using argsort.  Need a true radix sort.

    # TODO(dancol): probably get rid of CollateQuery since we need to
    # buffer the whole thing anyway.
    ics, [oc] = await qe.async_setup([sort[0] for sort in self.sorts],
                                     [self])
    await oc.write(
      self.__compute_sort_indexer(
        qe.qc,
        await qe.async_io(*[ic.read_all() for ic in ics])),
      True)

  @classmethod
  def sort1(cls, query, ascending=True):
    """Convenience function for sorting by one column"""
    assert isinstance(query, QueryNode)
    assert not query.schema.is_string
    return cls(((query, ascending),))



class FlsQuery(SimpleQueryNode):
  """Find first set bit"""

  _value = iattr(QueryNode, name="value")

  @classmethod
  def of(cls, query):
    """Make an FLS query"""
    query = QueryNode.coerce_(query)
    schema = query.schema
    if schema.is_null:
      return query
    if not (schema.is_integral and schema.is_normal):
      raise InvalidQueryException("cannot compute FLS of {}".format(schema))
    return cls(query)

  @override
  def countq(self):
    return self._value.countq()

  @override
  def _compute_schema(self):
    return QuerySchema(INT8)

  @override
  def _compute_inputs(self):
    return (self._value,)

  @override
  async def run_async(self, qe):
    ics, ocs = await qe.async_setup([self._value], [self])
    def _transduce(v):
      return (fls(v),)
    await transduce(qe, ics, ocs, _transduce)
