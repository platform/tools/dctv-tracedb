# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test query caching facilities"""

# pylint: disable=missing-docstring,compare-to-zero

import logging
from resource import getpagesize

import pytest

from .util import (
  INT8,
  TmpDir,
)

from ._native import (
  QueryCache,
  SPAN_INVARIANT_CHECKING,
)

log = logging.getLogger(__name__)

def test_query_cache_basic():
  qc = QueryCache(
    memory_lwm_nbytes=1024,
    memory_hwm_nbytes=16384,
  )
  assert qc.memory_nbytes == 0
  assert qc.nhunks == 0
  assert qc.memory_lwm_nbytes == 1024
  assert qc.memory_hwm_nbytes == 16384

def _new_array_with_score(qc, dtype, nelem, *, score_override):
  old_score_override = qc.get_new_hunk_score_override()
  qc.set_new_hunk_score_override(score_override)
  try:
    return qc.make_uninit_array(dtype, nelem)
  finally:
    qc.set_new_hunk_score_override(old_score_override)

@pytest.mark.parametrize("bs", ["mmap", "numpy"])
@pytest.mark.skipif(
  not SPAN_INVARIANT_CHECKING,
  reason="optimized build")
def test_query_cache_spill(bs):
  mmap_min_nbytes = 0 if bs == "mmap" else 4096
  memory_lwm_nbytes = 4096 if bs == "mmap" else 0
  qc = QueryCache(
    memory_lwm_nbytes=memory_lwm_nbytes,
    memory_hwm_nbytes=memory_lwm_nbytes,
    mmap_min_nbytes=mmap_min_nbytes,
    tmpdir=TmpDir(),
  )
  assert isinstance(qc.tmpdir, TmpDir)
  array1 = _new_array_with_score(qc, INT8, 4, score_override=5)
  array1[:] = [1, 2, 3, 4]
  hunk1 = array1.base.hunk
  del array1
  assert hunk1.core_name == bs
  assert qc.memory_nbytes == getpagesize() if bs == "mmap" else 4
  array2 = _new_array_with_score(qc, INT8, 40, score_override=4)
  hunk2 = array2.base.hunk
  del array2
  assert hunk2.core_name == bs
  assert hunk1.core_name == "spilled"
  assert hunk1.inflate().tolist() == [1, 2, 3, 4]
  assert hunk1.core_name == bs + "_spilled"
