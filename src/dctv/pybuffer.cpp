// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "pybuffer.h"
#include "pyobj.h"

namespace dctv {

// Sort of like MemoryView, but actually safe.  Calls the contained
// function to retrieve buffer information.
struct BufferHolder : BasePyObject {
  BufferHolder(GetBufferFunction get_buffer, char dtype) noexcept :
      get_buffer(std::move(get_buffer)), dtype(dtype)
  {}
  GetBufferFunction get_buffer;
  char dtype;
  static PyTypeObject pytype;
};

static int
BufferHolder_bf_getbuffer(PyObject* self,
                          Py_buffer* view,
                          int flags) noexcept
{
  return reinterpret_cast<BufferHolder*>(self)
      ->get_buffer(self, view, flags);
}

static PyBufferProcs BufferHolderBufferProcs = {
  BufferHolder_bf_getbuffer,
  nullptr /* bf_releasebuffer */,
};

static PyMemberDef BufferHolderMembers[] = {
  make_memberdef("dtype",
                 T_CHAR,
                 offsetof(BufferHolder, dtype),
                 READONLY,
                 "dtype"),
  { 0 },
};

PyTypeObject BufferHolder::pytype = make_py_type<BufferHolder>(
    "dctv._native.BufferHolder",
    "Buffer holder",
    [](PyTypeObject* t) {
      t->tp_as_buffer = &BufferHolderBufferProcs;
      t->tp_members = BufferHolderMembers;
    }
);

unique_pyref
make_buffer_holder(GetBufferFunction&& get_buffer, char dtype)
{
  return make_pyobj<BufferHolder>(std::move(get_buffer), dtype);
}

void
init_pybuffer(pyref m)
{
  register_type(m, &BufferHolder::pytype);
}

}  // namespace dctv
