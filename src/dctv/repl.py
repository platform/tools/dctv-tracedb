# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""SQL REPL for examining trace files"""

# pylint: disable=no-name-in-module,bad-whitespace

import os
import sys
import logging
import re
import weakref

from textwrap import dedent
from functools import lru_cache
from itertools import count

from cytoolz import take, first, merge_with
import numpy as np

from prompt_toolkit import PromptSession
from prompt_toolkit.completion import Completer, Completion

from prompt_toolkit.enums import DEFAULT_BUFFER
from prompt_toolkit.filters import Condition
from prompt_toolkit.filters.utils import to_filter
from prompt_toolkit.formatted_text import FormattedText
from prompt_toolkit.history import FileHistory
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.lexers.base import Lexer
from prompt_toolkit.output import get_default_output
from prompt_toolkit.shortcuts import print_formatted_text
from prompt_toolkit.validation import Validator, ValidationError

from modernmp.util import (
  ClosingContextManager,
  safe_close,
  the,
)

from .model import TraceAnalysisSession
from .sql import (
  DmlContext,
  LazyResolverFailedError,
  Select,
  get_sql_documentation,
  identifier_quote,
  quote,
)

from .query import (
  QuerySchema,
  TableKind,
  TableSorting,
)

from .sql_parser import (
  SqlLexer,
  SqlParsingException,
  allow_error_recovery,
  make_lexer,
  parse_dml,
)

from .util import (
  ExplicitInheritance,
  Immutable,
  Timed,
  abstract,
  all_same,
  cached_property,
  drain,
  final,
  iattr,
  override,
)

from ._native import (
  npy_explode,
  npy_has_mask,
)

from .query import (
  QueryTable,
)

from .autocomplete import (
  DctvSqlAutoCompleter,
  InvalidAutoCompleteContextError,
)

log = logging.getLogger(__name__)

NULL_INDICATOR = "NULL"
NULL_INDICATOR_LEN = len(NULL_INDICATOR)
NULL_INDICATOR_FORMATTED = ("red", NULL_INDICATOR)

_STYLE_KEYWORD = "class:pygments.keyword"

CONTROL_CHARACTER_RE = re.compile("([\000-\037]|\177)")
def _control_character_escape(c):
  return "\\{:03o}".format(ord(c))

def _summary_from_doc(doc):
  if doc is not None:
    doc, _, _ = doc.partition("\n")
    doc = doc.strip()
  return doc

def _format_column_header_full(name, schema):
  parts = [name, "dt:" + str(schema.dtype)]
  if schema.domain is not None:
    parts.append("d:" + str(schema.domain))
  if schema.unit is not None:
    parts.append("u:" + str(schema.unit))
  if schema.constraints:
    parts.append("cs:" + repr(sorted(schema.constraints)))
  return " ".join(parts)

COLUMN_HEADER_STYLES = {
  "name-only": lambda n, _s: n,
  "full": _format_column_header_full,
}

class Ui(ExplicitInheritance):
  """Interface for ReplSession to talk to the user"""

  @abstract
  def print(self, text):
    """Deliver a message to the user.

    TEXT is a string.
    """
    raise NotImplementedError("abstract")

  @abstract
  def print_table(self, row_chunk_generator, st, table_schema):
    """Print a table somehow

    ROW_CHUNK_GENERATOR is a function of no arguments yielding an
    iterator over ROW_CHUNK objects.  Each ROW_CHUNK is a mapping from
    column name to (ARRAY, SCHEMA) pairs.  String values should be
    delivered as codes.  ST is a string table for decoding string
    codes.  TABLE_SCHEMA is the table schema of the table to print.
    """
    raise NotImplementedError("abstract")

@final
class ColumnStudy(ExplicitInheritance):
  """Summary information about a column of values"""

  @override
  def __init__(self,
               name,
               schema,
               st,
               lookup_cache,
               column_header_formatter):
    self.__name = name
    self.__schema = the(QuerySchema, schema)
    self.__formatted_name = column_header_formatter(name, schema)
    self.__width = len(the(str, self.__formatted_name))
    if schema.is_string:
      self.__format = lambda array: st.vlookup(array, lookup_cache)
    else:
      # TODO(dancol): support fancier formatting
      self.__format = np.frompyfunc(str, 1, 1)
    self.__number_rows = 0

  def study_chunk(self, array):
    """Learn more about this column by studying rows in ARRAY"""
    array, mask = npy_explode(array)
    width = max(map(len, self.raw_format(array)), default=0)
    if mask.any():
      width = max(width, NULL_INDICATOR_LEN)
    self.__width = max(width, self.__width)
    self.__number_rows += len(array)

  @property
  def number_rows(self):
    """Number of rows we've observed"""
    return self.__number_rows

  def raw_format(self, array):
    """Format an array.

    ARRAY must not be masked, i.e., it cannot contain NULL values.
    (Those are handled at a higher level.)  The values are _not_
    padded to the column width.

    Return an ndarray (dtype=object) of strings.
    """
    assert not npy_has_mask(array)
    return self.__format(array)

  @property
  def name(self):
    """The absorbed column name"""
    assert self.__name
    return self.__name

  @property
  def schema(self):
    """The absorbed column schema"""
    assert self.__schema
    return self.__schema

  @property
  def width(self):
    """The maximum width of values in this column"""
    return self.__width

  @property
  def formatted_name(self):
    """The name of this column as modified by the column style"""
    return self.__formatted_name

@final
class TableStudy(ExplicitInheritance):
  """Information needed to format a table"""

  @override
  def __init__(self,
               row_chunk_generator,
               st,
               column_header_formatter):
    self.lookup_cache = lookup_cache = \
      st.make_lookup_cache("backslashescape")
    self.st = st
    self.__column_studies = self.__initial_table_study(
      row_chunk_generator, st, lookup_cache,
      column_header_formatter)

  @staticmethod
  def __initial_table_study(row_chunk_generator,
                            st,
                            lookup_cache,
                            column_header_formatter):
    column_studies = None
    for row_chunk in row_chunk_generator():
      if column_studies is None:
        column_studies = {
          column_name: ColumnStudy(column_name,
                                   schema,
                                   st,
                                   lookup_cache,
                                   column_header_formatter)
          for column_name, (_array, schema) in row_chunk.items()
        }
      else:
        assert tuple(column_studies) == tuple(row_chunk)
        assert all(study.schema == schema for study, (_array, schema)
                   in zip(column_studies.values(), row_chunk.values()))
      for study, (array, _schema) in zip(column_studies.values(),
                                         row_chunk.values()):
        study.study_chunk(array)
    if column_studies is None:
      column_studies = {}
    return column_studies

  @cached_property
  def column_names(self):
    """All column names"""
    return tuple(self.__column_studies.keys())

  @cached_property
  def columns(self):
    """All column studies"""
    return tuple(self.__column_studies.values())

  @property
  def number_rows(self):
    """Number of rows in the output"""
    if not self.__column_studies:
      return 0
    assert all_same(
      [cs.number_rows for cs in self.__column_studies.values()])
    return first(self.__column_studies.values()).number_rows

def _assert_formatted(value):
  assert isinstance(value, tuple)
  assert len(value) == 2
  assert isinstance(value[0], str)
  assert isinstance(value[1], str)
  return True

def _fmtattr():
  def _checker(value):
    assert isinstance(value, tuple)
    assert len(value) == 2
    assert isinstance(value[0], str)
    assert isinstance(value[1], str)
    return True
  return iattr(kwonly=True, assert_checker=_checker)

@final
class LineDrawing(Immutable):
  """Line-drawing configuration for the REPL

  Anything tagged with _fmtattr() should be a formatted-text tuple.
  """
  colsep = _fmtattr()
  """Separator between columns"""
  major_cross_first = _fmtattr()
  """Major-horizontal-rule cross when line continues below"""
  major_cross_both = _fmtattr()
  """Major-horizontal-rule cross when line continues above and below"""
  major_cross_last = _fmtattr()
  """Major-horizontal-rule cross when line continues above"""
  minor_cross_first = _fmtattr()
  """Minor-horizontal-rule cross when line continues below"""
  minor_cross_both = _fmtattr()
  """Minor-horizontal-rule cross when line continues above and below"""
  minor_cross_last = _fmtattr()
  """Minor-horizontal-rule cross when line continues above"""
  rowsep_major = _fmtattr()
  """Fill string for major horizontal rules"""
  rowsep_minor = _fmtattr()
  """Fill string for minor horizontal rules"""
  hanging_indent = _fmtattr()
  """Text indicating a row continuation.  Doubled on each continuation."""
  truncation_indicator = _fmtattr()
  """Text indicating a truncated value"""
  header_style = iattr(str, kwonly=True)
  """Style string applied to column headers"""
  header_sep_by_default = iattr(bool, kwonly=True)
  """Whether we want a horizontal rule after the header by default"""

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert len(self.rowsep_major[1]) == 1
    assert len(self.rowsep_minor[1]) == 1

ASCII_LINE_DRAWING = LineDrawing(
  colsep=("", "|"),
  major_cross_first=("", "="),
  major_cross_both=("",  "="),
  major_cross_last=("",  "="),
  minor_cross_first=("", "-"),
  minor_cross_both=("",  "-"),
  minor_cross_last=("",  "-"),
  rowsep_major=("", "="),
  rowsep_minor=("", "-"),
  hanging_indent=("green", ">"),
  truncation_indicator=("red", "..."),
  header_style="bold",
  header_sep_by_default=True,
)

FANCY_LINE_DRAWING = LineDrawing(
  colsep=("", "│"),
  major_cross_first=("", "╤"),
  major_cross_both=("",  "╪"),
  major_cross_last=("",  "╧"),
  minor_cross_first=("", "┬"),
  minor_cross_both=("",  "┼"),
  minor_cross_last=("",  "┴"),
  rowsep_major=("", "═"),
  rowsep_minor=("", "─"),
  hanging_indent=("green", "▶"),
  truncation_indicator=("red", "…"),
  header_style="bold",
  header_sep_by_default=False,
)

FORMATTED_NEWLINE = ("", "\n")

@final
class TableFormatter(ExplicitInheritance):
  """Format tables in an attractive and flexible way"""

  # TODO(dancol): format tables in C++.  The table output code runs
  # Python code for each cell in the output table, so it's necessarily
  # going to perform poorly for very large outputs.  That's usually
  # fine, because if you're printing thousands of rows in the REPL,
  # you probably did something wrong anyway, but still, writing the
  # table code in C++ would greatly improve performance.

  __indent_row_div = False

  @override
  def __init__(self, *,
               study,
               desired_width,
               line_drawing,
               draw_header_separator=None):
    self.__study = study
    self.__drawing = the(LineDrawing, line_drawing)
    if draw_header_separator is None:
      draw_header_separator = line_drawing.header_sep_by_default
    self.__draw_header_separator = bool(draw_header_separator)
    (self.__display_rows,
     self.__max_row_width,
     self.__display_cross) = self.__pack_columns_into_rows(
       [column_study.width for column_study in study.columns],
       len(line_drawing.colsep[1]),
       self.__generate_hanging_indents_widths,
       len(line_drawing.truncation_indicator[1]),
       desired_width)
    assert len(self.__display_rows) == len(self.__display_cross)
    nr_rows = len(self.__display_rows)
    self.__row_separators = [
      self.__make_row_separator(row_number)
      for row_number in range(nr_rows)
    ]
    self.__hanging_indents = \
      tuple(take(nr_rows, self.__generate_hanging_indents()))

  def __generate_hanging_indents(self):
    hanging_style, hanging_seed = self.__drawing.hanging_indent
    for nr in count():
      yield (hanging_style, hanging_seed * nr)

  def __generate_hanging_indents_widths(self):
    for _hanging_style, hanging_text in self.__generate_hanging_indents():
      yield len(hanging_text)

  @staticmethod
  def __pack_columns_into_rows(column_widths,
                               separator_width,
                               generate_hanging_intent_widths,
                               truncation_indicator_width,
                               desired_width):
    assert the(int, separator_width) >= 0
    assert the(int, desired_width) >= 0
    hanging_indent_width = generate_hanging_intent_widths()
    rows = []
    next_hanging_indent_width = next(hanging_indent_width)
    row_width = next_hanging_indent_width
    row_colw = []
    row_cross = []
    for column_width in column_widths:
      proposed_width = \
        row_width + (separator_width if row_colw else 0) + column_width
      if proposed_width <= desired_width:
        if row_colw:
          row_cross.append(row_width)
        row_width = proposed_width
        row_colw.append(column_width)
      else:
        if row_colw:
          assert row_width
          rows.append((row_colw, row_width, row_cross))
          next_hanging_indent_width = next(hanging_indent_width)
        row_width = next_hanging_indent_width + column_width
        if row_width > desired_width:
          # We can't fit even a single column on this row, so truncate.
          column_width -= (row_width - desired_width)
          column_width = max(truncation_indicator_width, column_width)
          row_width = next_hanging_indent_width + column_width
          assert row_width == desired_width
        row_colw = [column_width]
        row_cross = []
    if row_colw:
      rows.append((row_colw, row_width, row_cross))
    # Pad the last field on each line to the length of the longest row
    # so that things look pretty.
    max_row_width = max((row[1] for row in rows), default=0)
    for row_colw, row_width, _row_cross in rows:
      row_colw[-1] += max_row_width - row_width
    if rows:
      colw, _widths, cross = zip(*rows)
      return colw, max_row_width, cross
    return (), 0, ()

  @staticmethod
  @lru_cache(16)
  def __formatted_padding(width):
    assert width >= 0
    return ("", " " * width)

  def __make_row_separator(self, row_number):
    drawing = self.__drawing
    nr_rows = len(self.__display_cross)
    is_last_row = (row_number + 1 == nr_rows)
    if is_last_row:
      fill = self.__drawing.rowsep_major
      cross_first = drawing.major_cross_first
      cross_both = drawing.major_cross_both
      cross_last = drawing.major_cross_last
    else:
      fill = self.__drawing.rowsep_minor
      cross_first = drawing.minor_cross_first
      cross_both = drawing.minor_cross_both
      cross_last = drawing.minor_cross_last
    fill_fmt, fill_char = fill
    output = []
    pos = 0
    def _fill_to(target_pos):
      nonlocal pos
      gap = target_pos - pos
      if gap > 0:  # pylint: disable=compare-to-zero
        output.append((fill_fmt, fill_char * gap))
        pos = target_pos
    stops = {}
    for stop in self.__display_cross[row_number]:
      stops[stop] = cross_last
    if not is_last_row:
      for stop in self.__display_cross[row_number + 1]:
        if stop in stops:
          stops[stop] = cross_both
        else:
          stops[stop] = cross_first
    for stop in sorted(stops):
      _fill_to(stop)
      cross = stops[stop]
      output.append(cross)
      pos += len(cross[1])
    _fill_to(self.__max_row_width)
    return output

  def __format_with_separators(self, items, *, hr=False):
    drawing = self.__drawing
    output = []
    items = list(items)
    items.reverse()
    column_separator = drawing.colsep
    newline = FORMATTED_NEWLINE
    hanging_indents = self.__hanging_indents
    nr_display_rows = len(self.__display_rows)
    if not nr_display_rows:
      return ()
    assert nr_display_rows >= 1
    for row_number, column_widths in enumerate(self.__display_rows):
      if row_number:
        output.append(newline)
        output.append(hanging_indents[row_number])
      for column_number, column_width in enumerate(column_widths):
        if column_number:
          output.append(column_separator)
        item = items.pop()
        item_width = len(item[1])
        if item_width < column_width:
          output.append(item)
          output.append(self.__formatted_padding(column_width - item_width))
        elif item_width == column_width:
          output.append(item)
        else:
          truncation = drawing.truncation_indicator
          item_width = column_width - len(truncation[1])
          assert item_width >= 0
          output.append((item[0], item[1][:item_width]))
          output.append(truncation)
      if hr:
        output.append(newline)
        output.extend(self.__row_separators[row_number])
    assert not items
    return output  # Don't append trailing newline: that's automatic

  def format_column_headers(self):
    """Format colum headers as a list of formatted text tuples.

    Does _not_ newline-terminate.
    """
    return self.__format_with_separators(
      [(self.__drawing.header_style, column_study.formatted_name)
       for column_study in self.__study.columns],
      hr=True)

  @staticmethod
  def __raw_format_sub_array(array, mask, column_study):
    formatted_array = np.empty_like(array, dtype="O")
    formatted_array[~mask] = column_study.raw_format(array[~mask])
    return formatted_array

  def __raw_format_row_sub_chunk(self, sub_chunk):
    assert len(sub_chunk) == 2 * len(self.__study.columns)
    return [self.__raw_format_sub_array(array, mask, column_study)
            for array, mask, column_study
            in zip(sub_chunk[0::2],
                   sub_chunk[1::2],
                   self.__study.columns)]

  def __raw_format_row_chunk(self, row_chunk):
    assert tuple(row_chunk) == self.__study.column_names
    assert all_same(map(len, row_chunk.values()))
    exploded_arrays = []
    for array, _schema in row_chunk.values():
      exploded_arrays.extend(np.broadcast_arrays(*npy_explode(array)))
    assert all_same(map(len, exploded_arrays))
    # We can't just use np.nditer here because NPY_MAXARGS (in 1.16,
    # 32) would limit the number of columns that we could format.
    formatted_rows = 0
    total_rows = len(exploded_arrays[0])
    sub_chunk_size = 32768
    while formatted_rows < total_rows:
      sub_chunk_start = formatted_rows
      sub_chunk_end = sub_chunk_start + sub_chunk_size
      sub_chunk = [array[sub_chunk_start : sub_chunk_end]
                   for array in exploded_arrays]
      yield self.__raw_format_row_sub_chunk(sub_chunk)
      formatted_rows += sub_chunk_size

  @staticmethod
  def __format_raw_item(raw_item):
    if raw_item is None:
      return NULL_INDICATOR_FORMATTED
    assert isinstance(raw_item, str)
    return ("", raw_item)

  def format_row_chunk(self, row_chunk):
    """Format a row chunk as a list of formatted text

    Terminate each formatted row with a formatted newline.
    """
    format_raw_item = self.__format_raw_item
    parts = []
    formatted_newline = FORMATTED_NEWLINE
    multiline = len(self.__display_rows) > 1
    for raw_formatted_row_sub_chunk in \
        self.__raw_format_row_chunk(row_chunk):
      assert all_same(map(len, raw_formatted_row_sub_chunk))
      for row in zip(*raw_formatted_row_sub_chunk):
        parts.extend(self.__format_with_separators(
          [format_raw_item(row_part) for row_part in row],
          hr=multiline))
        parts.append(formatted_newline)
    return parts

@final
class PromptToolkitUi(Ui):
  """UI that talks to prompt_toolkit"""

  @override
  def __init__(self,
               *,
               line_drawing=FANCY_LINE_DRAWING,
               draw_header_separator=None):
    super().__init__()
    self.__drawing = the(LineDrawing, line_drawing)
    self.__draw_header_separator = draw_header_separator

  @override
  def print(self, text):
    print_formatted_text(text)

  @override
  def print_table(self,
                  row_chunk_generator,
                  st,
                  table_schema,
                  column_header_formatter=lambda n, _: n):
    """Print a table.

    ROW_CHUNK_GENERATOR is a function of zero arguments that, when
    called, yields row chunks in the style of
    TraceAnalysisSession.sql_query(), with output_format="rows",
    strings=None, and want_schema=Yes.  String columns are given as
    raw string codes, i.e., _not_ translated to string objects.

    ST is the string table providing an interpretation of
    string-column results.  TABLE_SCHEMA is the schema of the table to
    prnt.

    COLUMN_HEADER_FORMATTER is a function of two arguments: a column
    name and a column schema.  Return a string to use as the name of
    that column.
    """
    study = TableStudy(row_chunk_generator, st,
                       column_header_formatter)
    output = get_default_output()
    width = output.get_size().columns
    formatter = TableFormatter(
      study=study,
      desired_width=width,
      line_drawing=self.__drawing,
      draw_header_separator=self.__draw_header_separator)
    print_formatted_text("schema={} rows={}".format(
      table_schema,
      study.number_rows))
    print_formatted_text(
      FormattedText(formatter.format_column_headers()))
    for row_chunk in row_chunk_generator():
      print_formatted_text(
        FormattedText(formatter.format_row_chunk(row_chunk)))

@final
class ReplSession(ClosingContextManager, ExplicitInheritance):
  """Interactive REPL loop for DCTV SQL"""

  @override
  def __init__(self, ui):
    self.__trace_session = TraceAnalysisSession()
    self.__ui = the(Ui, ui)
    self.__column_header_style = first(COLUMN_HEADER_STYLES)

  @property
  def ui(self):
    """The user interface object this session uses"""
    return self.__ui

  SETTINGS = (
    "block_size",
    "column_header_style",
  )

  @property
  def block_size(self):
    """The query block size used for execution"""
    return self.__trace_session.qc.block_size

  @block_size.setter
  def block_size(self, value):
    value = int(value)
    self.__trace_session.reconfigure_cache(
      block_size=None if value <=0 else value)

  @property
  def column_header_style(self):
    """The column header style: from COLUMN_HEADER_STYLES"""
    return self.__column_header_style

  @column_header_style.setter
  def column_header_style(self, value):
    if value not in COLUMN_HEADER_STYLES:
      raise KeyError("unknown column style {!r}: choices are {!r}".
                     format(value, sorted(COLUMN_HEADER_STYLES)))
    self.__column_header_style = value

  @override
  def _do_close(self):
    super()._do_close()
    safe_close(self.__trace_session)

  SPECIAL_PREFIX = "_special_"

  def _special_quit(self, _args):  # pylint: disable=no-self-use
    """Exit the REPL"""
    sys.exit(0)

  def _special_gc(self, _args):  # pylint: disable=no-self-use
    """Run a Python-level full garbage collection pass"""
    import gc
    gc.collect()

  def _special_exit(self, args):
    """Exit the REPL"""
    self._special_quit(args)

  def _special_set(self, args):
    """Set a preference"""
    key, value = args
    if key not in self.SETTINGS:
      raise KeyError("unknown setting {!r}".format(key))
    setattr(self, key, value)
    self.__ui.print("Set {} to {}".format(key, getattr(self, key)))

  def _special_show(self, args):
    """Show preferences"""
    if not args:
      for setting in self.SETTINGS:
        self._special_show([setting])
      return
    setting, = args
    if setting not in self.SETTINGS:
      raise KeyError("unknown setting {!r}".format(setting))
    self.__ui.print("{}: {}".format(setting, getattr(self, setting)))

  def _special_dump_ns(self, _args):
    """Dump contents of the SQL namespace"""
    ns = self.__trace_session.user_ns
    for path, _value in ns.walk():
      self.__ui.print(path)

  def _special_clear_query_cache(self, _args):
    """Clear query cache"""
    self.__trace_session.clear_query_cache()

  def _special_xyzzy(self, _args):
    """Invoke a mysterious power"""
    self.__ui.print("Nothing happens")

  def _special_explain(self, args):
    """Print query performance data"""
    from ._native import clock_gettime_ns
    from time import CLOCK_MONOTONIC
    def _now():
      return clock_gettime_ns(CLOCK_MONOTONIC)
    def _fmt_ns(ns):
      return "{}ms".format(ns/1.0e6)
    start_ns = _now()
    asts = parse_dml(args[0])
    end_parse_ns = _now()
    if len(asts) > 1:
      self.__ui.print("ERROR: expected one AST")
      return
    ast = asts[0]
    if not isinstance(ast, Select):
      self.__ui.print("ERROR: Not a select")
      return
    self.__ui.print("Parsing took {}".format(
      _fmt_ns(end_parse_ns - start_ns)))

    trace_session = self.__trace_session
    qt = trace_session.sql_query_to_qt(ast)
    perf_by_cls = {}
    def _on_perf(action, perf):
      perf = dict(perf)
      old_perf = perf_by_cls.get(type(action))
      if old_perf is not None:
        perf = merge_with(sum, perf, old_perf)
      perf_by_cls[type(action)] = perf
    drain(trace_session.sql_query(
      qt,
      output_format="rows",
      strings=None,
      perf_callback=_on_perf,
    ))
    for action_cls, perf in sorted(perf_by_cls.items(),
                                   key=lambda x: x[1]["monotonic_ns"],
                                   reverse=True):
      self.__ui.print("{}: {}".format(action_cls.__qualname__,
                                      _fmt_ns(perf["monotonic_ns"])))

  def __doc_for_special(self, fn):  # pylint: disable=no-self-use
    """Return the docstring for FN"""
    # TODO(dancol): make this work correctly in -OO mode
    return fn.__doc__

  def __help_for_table(self, name):
    lexenv = DmlContext(self.__trace_session.user_ns).make_tctx().lexenv
    pr = self.__ui.print
    try:
      obj = lexenv.lookup_sql_attribute_by_path(name.split("."))
    except KeyError:
      pr("Nothing called {} known".format(identifier_quote(name)))
      return
    if not isinstance(obj, QueryTable):
      pr("{} object".format(type(obj).__name__))
      return

    doc = get_sql_documentation(obj)
    if doc is not None:
      pr(doc.rstrip("\n"))
    if obj.table_schema.kind == TableKind.REGULAR:
      pr("Regular table")
    else:
      pr("{} table partition={} sorting={}"
         .format(TableKind.label_of(obj.table_schema.kind).capitalize(),
                 obj.table_schema.partition,
                 TableSorting.label_of(obj.table_schema.sorting).lower()))

  def __help_for_special(self, special_command):
    assert special_command.startswith(".")
    try:
      fn = getattr(self, self.SPECIAL_PREFIX + special_command[1:])
    except AttributeError:
      self.__ui.print("No special command called {!r}".format(
        special_command))
      return
    doc = self.__doc_for_special(fn)
    head, _, tail = doc.partition("\n")
    self.__ui.print(head)
    munged_tail = dedent(tail).rstrip("\n")
    if munged_tail:
      self.__ui.print(munged_tail)

  def _special_help(self, args):
    """Print a help message or describe a table

    With no arguments, .help describes all available special commands.
    With arguments, .help provides detailed help about specific
    things.  Each thing can be a special command (e.g., ".help .help"
    for this message) or a table name (e.g., ".help mytrace.good").
    When .help is given a table, it prints a description of that
    table.
    """
    if args:
      # Accepting just one argument makes autocomplete easier to
      # reason about, since we don't need to tokenize.
      if len(args) > 1:
        self.__ui.print("ERROR: .help accepts zero arguments or one")
        return
      arg = args[0]
      if arg.startswith("."):
        self.__help_for_special(arg)
      else:
        self.__help_for_table(arg)
      return

    self.__ui.print(dedent("""\
    DCTV REPL. Execute SQL queries against the currently loaded trace(s).

    Each input is a sequence of one or more SQL queries, each
    terminated by a ';'.  The trailing ';' on the last SQL statement
    on a given input line is optional.

    EOF (usually C-d) on an empty line exits the REPL. C-j inserts a
    newline in the current edit buffer, allowing construction of
    multi-line queries.

    Available special commands:
    """))
    for special_command in self.gen_special_commands():
      fn = getattr(self, self.SPECIAL_PREFIX + special_command)
      self.__ui.print(".{} - {}".format(
        special_command,
        self.__doc_for_special(fn).partition("\n")[0]))

  def _complete_special_help(self, text, point):
    m = re.match("^\\.help[ \t]+", text)
    if not m or point < m.end(0):
      return
    token = text[m.end(0):]
    token_len = len(token)
    if not token or token.startswith("."):
      token_lc = token.lower()
      for special_command in self.gen_special_commands():
        spec = "." + special_command
        if spec.startswith(token_lc):
          yield Completion(spec, -token_len)
    try:
      ac = self.make_sql_auto_completer(token, point - m.end(0),
                                        bare_table_reference=True,
                                        decorate=False)
      ac_token_lc = ac.completion_token.lower()
      ac_token_len = len(ac_token_lc)
      for identifier, doc in ac.complete_identifiers():
        if identifier.lower().startswith(ac_token_lc):
          yield Completion(identifier, -ac_token_len,
                           display_meta=_summary_from_doc(doc))
    except InvalidAutoCompleteContextError:
      return

  def gen_special_commands(self):
    """Generate all special command names"""
    prefix = self.SPECIAL_PREFIX
    for name in dir(self):
      if name.startswith(prefix):
        yield name[len(prefix):]

  def run_special_command(self, *args):
    """Run a special command with pre-split args"""
    if not args:
      raise ValueError("no command given")
    command_name = args[0]
    fn_name = self.SPECIAL_PREFIX + command_name
    fn = getattr(self, fn_name, None)
    if not fn:
      raise ValueError("unknown special command {!r}"
                       .format("." + command_name))
    fn(args[1:])

  def __show_select_result(self, ast):
    trace_session = self.__trace_session
    qt = trace_session.sql_query_to_qt(ast)
    def _generate_row_chunks():
      for row_chunk, _is_eof in trace_session.sql_query(
          qt,
          output_format="rows",
          want_schema=True,
          strings=None):
        yield row_chunk
    self.__ui.print_table(
      _generate_row_chunks,
      trace_session.st,
      qt.table_schema,
      COLUMN_HEADER_STYLES[self.__column_header_style],
    )

  def run_dml_command(self, ast):
    """Run a parsed DML or SELECT command"""
    if isinstance(ast, str):
      ast, = parse_dml(ast)
    if isinstance(ast, Select):
      self.__show_select_result(ast)
    else:
      ast.execute_dml(self.__trace_session.dmlctx)

  def report_exception(self, ex):  # pylint: disable=no-self-use
    """Report an exception to the console"""
    log.debug("ERROR", exc_info=True)
    self.__ui.print("ERROR: " + str(ex))

  def handle_input_line(self, input_line):
    """React to user input.

    INPUT_LINE should be stripped.
    """
    if input_line.startswith("."):
      from shlex import split
      self.run_special_command(*split(input_line[1:]))
    else:
      commands = parse_dml(input_line)
      for command in commands:
        with Timed("Running command"):
          self.run_dml_command(command)

  def make_sql_auto_completer(self, sql, point, **kwargs):
    """Make a DctvSqlAutoCompleter object"""
    return DctvSqlAutoCompleter(sql, point, self.__trace_session.user_ns,
                                **kwargs)

  def validate_input(self, sql):
    """Fulfill the prompt-toolkit validator contract"""
    _validate(sql, self.__trace_session.user_ns)

def _make_simple_read_input(prompt):
  def _simple_read_input():
    sys.stdout.flush()
    sys.stdout.write(prompt)
    sys.stdout.flush()
    result = sys.stdin.readline()
    if not result:
      raise EOFError()
    return result
  return _simple_read_input

class DctvSqlLexer(Lexer):
  """Lexer that tokenizes DCTV's SQL dialect"""

  def lex_document(self, document):
    """prompt_toolkit interface"""
    def _lex_line(line_number):
      # TODO(dancol): not correct with multi-line comments
      return self.__lex_line(document.lines[line_number])
    return _lex_line

  @staticmethod
  def __style_for_token(tok):
    if tok.type in SqlLexer.keywords:
      return _STYLE_KEYWORD
    if tok.type in ("SINGLE_QUOTED_STRING", "DOUBLE_QUOTED_STRING"):
      return "class:pygments.literal.string"
    if tok.type == "BACK_QUOTED_STRING":
      return "class:pygments.literal.string.escape"
    return ""

  def __lex_line(self, line):
    parts = []
    if line.startswith("."):
      space_idx = line.find(" ")
      if space_idx < 0:
        space_idx = len(line)

      parts.append(("class:pygments.keyword", line[:space_idx]))
      parts.append(("", line[space_idx:]))
      return parts

    lexer = make_lexer()
    lexer.input(line)
    with allow_error_recovery():
      last_end = 0
      while True:
        tok = lexer.token()
        if not tok:
          if last_end < len(line):
            parts.append(("", line[last_end:]))
          break
        gap = tok.lexpos - last_end
        if gap:
          parts.append(("", line[last_end : tok.lexpos]))
        last_end = tok.lexpos + len(tok.value)
        parts.append((self.__style_for_token(tok), tok.value))
    return parts

class DctvSqlCompleter(Completer):
  """Auto-completion glue for prompt toolkit"""

  def __init__(self, repl_session):
    """Make a new autocompleter.

    REPL_SESSION is a ReplSession.  It's held by this object byy
    weak reference.
    """
    assert isinstance(repl_session, ReplSession)
    self.__repl_session_wr = weakref.ref(repl_session)

  def get_completions(self, document, complete_event):
    repl_session = self.__repl_session_wr()
    if not repl_session:
      return
    try:
      text, point = document.text, document.cursor_position
      if text.startswith("."):
        yield from self.__autocomplete_special(repl_session, text, point)
        return
      ac = repl_session.make_sql_auto_completer(text, point)
      ct = ac.completion_token
      ct_offset = -len(ct)
      for kind, suggestion, doc in ac.complete():
        assert len(suggestion) >= -ct_offset
        style = _STYLE_KEYWORD if kind == "K" else ""
        yield Completion(suggestion, ct_offset, style=style,
                         display_meta=_summary_from_doc(doc))
    except InvalidAutoCompleteContextError:
      return
    except Exception:
      log.debug("completion exception", exc_info=True)

  def __autocomplete_special(self, repl_session, text, point):  # pylint: disable=no-self-use
    assert text.startswith(".")
    tokens = text.split(None, 1)
    head_lc = (tokens or [""])[0].lower()
    if point > len(head_lc):
      special_completer = getattr(repl_session,
                                  "_complete_special_" + head_lc[1:],
                                  None)
      if special_completer is not None:
        yield from special_completer(text, point)
      return
    for special_command in repl_session.gen_special_commands():
      mc = "." + special_command
      assert mc == mc.lower()
      if mc.startswith(head_lc):
        assert len(mc) >= len(head_lc)
        yield Completion(mc, -len(head_lc))

class DctvSqlValidator(Validator):
  """Validator for DCTV SQL"""

  def __init__(self, repl_session):
    """Make a new validator.

    REPL_SESSION is a ReplSession.  It's held by this object byy
    weak reference.
    """
    assert isinstance(repl_session, ReplSession)
    self.__repl_session_wr = weakref.ref(repl_session)

  def validate(self, document):
    repl_session = self.__repl_session_wr()
    if not repl_session:
      return
    if document.text.startswith("."):
      return  # Special command
    repl_session.validate_input(document.text)

def _validate(sql, ns):
  try:
    dml_actions = parse_dml(sql)
  except SqlParsingException as ex:
    raise ValidationError(message=str(ex), cursor_position=ex.lexpos)
  except Exception as ex:
    raise ValidationError(message=str(ex))
  for dml_action in dml_actions:
    try:
      dml_action.eval_for_autocomplete(DmlContext(ns))
    except Exception as ex:
      raise ValidationError(message=str(ex))

def _make_fancy_read_input(prompt, repl_session):
  # Override normal accept binding to accept command line on single
  # press of RET even in multiline mode
  override_kb = KeyBindings()
  @Condition
  def _override_accept_condition():
    # TODO(dancol): silly DCTV: be more ipython!
    return session.app.layout.has_focus(DEFAULT_BUFFER)
  @override_kb.add("enter", filter=_override_accept_condition)
  def _(_event):
    session.default_buffer.validate_and_handle()
  session = PromptSession(
    multiline=True,
    key_bindings=override_kb,
    lexer=DctvSqlLexer(),
    enable_open_in_editor=True,
    enable_system_prompt=True,
    search_ignore_case=to_filter(True),
    history=FileHistory(os.path.expanduser("~/.dctv-history")),
    completer=DctvSqlCompleter(repl_session),
    validator=DctvSqlValidator(repl_session),
    validate_while_typing=False,
  )
  def _read_input():
    return session.prompt(prompt)
  return _read_input

def run_repl(args):
  """Run the command line query inspector"""
  if args.drawing == "auto":
    args.drawing = (
      "fancy"
      if os.isatty(1) and os.environ.get("TERM") != "dumb"
      else "ascii")
  assert args.drawing in ("fancy", "ascii")

  ui = PromptToolkitUi(
    line_drawing=(ASCII_LINE_DRAWING
                  if args.drawing == "ascii"
                  else FANCY_LINE_DRAWING),
  )
  repl_session = ReplSession(ui)

  stdin_tty = os.isatty(0)
  prompt = "DCTV> " if stdin_tty else ""
  if stdin_tty:
    read_input = _make_fancy_read_input(prompt, repl_session)
  else:
    read_input = _make_simple_read_input(prompt)

  for trace_mount in args.trace_mounts:
    if "=" not in trace_mount:
      raise ValueError("invald mount directive: {!r}".format(trace_mount))
    mount_point, trace_file_name = trace_mount.split("=", 1)
    q_mount_point = ".".join(map(identifier_quote, mount_point.split(".")))
    q_trace_file_name = quote(trace_file_name)
    query = "MOUNT TRACE {} AS {}".format(q_trace_file_name, q_mount_point)
    repl_session.run_dml_command(query)

  if stdin_tty:
    print("Type .help for help.")
  while True:
    try:
      user_input = read_input().strip()
    except KeyboardInterrupt:
      continue
    except EOFError:
      break
    if not user_input:
      continue
    try:
      try:
        repl_session.handle_input_line(user_input)
      except LazyResolverFailedError as ex:
        if ex.__cause__:
          raise ex.__cause__ from None
        raise
    except (KeyboardInterrupt, Exception) as ex:
      repl_session.report_exception(ex)
