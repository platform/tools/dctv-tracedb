// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename P, typename D>
auto
handle_pyarg(bool*, P&& parse_arg, D&& default_)
{
  constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
  int tmp;
  if constexpr (has_default) {
    tmp = static_cast<bool>(default_());
  }
  return parse_arg(
      BOOST_HANA_STRING("p"),
      hana::make_tuple(&tmp),
      [&] {
        assume(tmp == 0 || tmp == 1);
        return static_cast<bool>(tmp);
      });
}

template<typename P, typename D>
auto
handle_pyarg(std::string_view*, P&& parse_arg, D&& default_)
{
  constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
  const char* chars;
  Py_ssize_t num_chars;
  if constexpr (has_default) {
    chars = nullptr;
  }
  return parse_arg(
      BOOST_HANA_STRING("s#"),
      hana::make_tuple(&chars, &num_chars),
      [&] {
        std::string_view sv;
        if constexpr (has_default) {
          if (!chars) {
            return std::string_view(default_());
          }
        }
        assume(chars);
        return std::string_view(chars,
                                static_cast<size_t>(num_chars));
      });
}

template<typename T, typename P, typename D>
auto
handle_pyarg(obj_pyref<T>*, P&& parse_arg, D&& default_) {
  constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
  PyObject* ref;
  if constexpr (has_default) {
    ref = nullptr;
  }
  return parse_arg(
      BOOST_HANA_STRING("O"),
      hana::make_tuple(&ref),
      [&] {
        if constexpr (has_default) {
          // If the object we're decoding cannot be None and we see
          // None and the value is optional, just regard the object
          // as missing.
          if (!std::is_same_v<T, PyObject> && ref == Py_None)
            ref = nullptr;
          if (!ref)
            return obj_pyref<T>(default_());
        }
        assume(ref);
        return pyref(ref).as<T>();
      });
}

template<typename T, typename P, typename D>
auto
handle_pyarg(unique_obj_pyref<T>*, P&& parse_arg, D&& default_)
{
  constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
  PyObject* ref;
  if constexpr (has_default) {
    ref = nullptr;
  }
  return parse_arg(
      BOOST_HANA_STRING("O"),
      hana::make_tuple(&ref),
      [&] {
        if constexpr (has_default) {
          // If the object we're decoding cannot be None and we see
          // None and the value is optional, just regard the object
          // as missing.
          if (!std::is_same_v<T, PyObject> && ref == Py_None)
            ref = nullptr;
          if (!ref)
            return obj_pyref<T>(default_()).addref();
        }
        assume(ref);
        return pyref(ref).as<T>().addref();
      });
}

template<typename Args, typename Meta, typename DoParse>
Args
parsepyargs_make_struct(Meta&& meta, DoParse&& do_parse)
{
  namespace h = hana;
  using namespace h::literals;
  using h::traits::remove_reference;
  using h::traits::is_default_constructible;
  using h::typeid_;
  using h::type_c;
  constexpr auto pipe = h::char_c<'|'>;
  constexpr auto dollar = h::char_c<'$'>;

  auto make_struct_tuple = h::fix([&] (
      auto&& process_args,
      auto&& field_info,
      auto&& format_string,
      auto&& varargs,
      auto&& kwarg_names) {
    if constexpr (CONSTEXPR_VALUE(h::is_empty(field_info))) {
      do_parse(format_string, varargs, kwarg_names);
      return h::make_tuple();
    } else {  // NOLINT
      auto this_field_info = h::front(field_info);
      auto field_name = h::at_c<0>(this_field_info);
      auto field_type = h::at_c<1>(this_field_info);
      auto field_meta = h::at_c<2>(this_field_info);
      auto field_default = h::first(field_meta);
      // Use an IIFE so we can use an if constexpr to avoid invoking
      // the call operator on the raw tag no_default, which doesn't
      // have a call operator.
      constexpr bool has_default = ([&]() constexpr {
        if constexpr(CONSTEXPR_VALUE(field_default == no_default)) {
          return false;
        } else {  // NOLINT
          return CONSTEXPR_VALUE(field_default() != no_default);
        }
      })();

      auto field_converter = h::second(field_meta);
      auto required = !h::contains(format_string, pipe);
      auto kwonly = h::contains(format_string, dollar);
      if constexpr (
          CONSTEXPR_VALUE(field_type == type_c<optional_args_follow>)) {
        static_assert(CONSTEXPR_VALUE(required),
                      "optional_args_follow already given");
        static_assert(!has_default,
                      "optional_args_follow cannot have a default value");
        return h::prepend(
            process_args(
                h::drop_front_exactly(field_info),
                format_string + h::make_string(pipe),
                varargs,
                kwarg_names),
            optional_args_follow{});
      } else if constexpr (  // NOLINT
          CONSTEXPR_VALUE(field_type == type_c<kwonly_args_follow>)) {
        static_assert(
            CONSTEXPR_VALUE(!required),
            "kwonly_args_follow must follow optional_args_follow");
        static_assert(
            CONSTEXPR_VALUE(!kwonly),
            "kwonly_args_follow already given");
        static_assert(!has_default,
                      "kwonly_args_follow cannot have a default value");
        return h::prepend(
            process_args(h::drop_front_exactly(field_info),
                         format_string + h::make_string(dollar),
                         varargs,
                         kwarg_names),
            kwonly_args_follow{});
      } else {
        using FieldType = typename decltype(field_type)::type;
        FieldType* dummy_ptr = nullptr;
        auto make_default_maker = [&]() {
          if constexpr (CONSTEXPR_VALUE(required)) {
            static_assert(!has_default,
                          "default value provided for required field");
            return h::false_c;
          } else if constexpr (has_default) {  // NOLINT
            return field_default;
          } else {  // NOLINT
            return []() constexpr { return FieldType{}; };
          }
        };
        return field_converter(
            field_type,
            [&] (auto&& format_string_chunk,
                 auto&& more_varargs,
                 auto&& cb) {
              auto values = process_args(
                  h::drop_front_exactly(field_info),
                  format_string + format_string_chunk,
                  h::concat(varargs, more_varargs),
                  h::append(kwarg_names, field_name));
              return h::prepend(std::move(values),
                                std::forward<decltype(cb)>(cb)());
            },
            make_default_maker());
      }
    }});

  return h::unpack(
      make_struct_tuple(
          /*field_info=*/h::zip(struct_field_names<Args>,
                                struct_field_types<Args>,
                                AUTOFWD(meta)),
          /*format_string=*/BOOST_HANA_STRING(""),
          /*varargs=*/h::make_tuple(),
          /*kwarg_names=*/h::make_tuple()),
      make_aggregate<Args>);
}

template<typename Args, typename Meta>
Args
parsepyargs(Meta&& meta, pyref py_args)
{
  namespace h = hana;
  return parsepyargs_make_struct<Args>(
      AUTOFWD(meta),
      [&](auto&& format_string,
          auto&& varargs,
          auto&& kwarg_names) {
        if (!h::unpack(varargs,
                       h::partial(PyArg_ParseTuple,
                                  py_args.get(),
                                  format_string.c_str())))
          throw_current_pyerr();
      });
}

template<typename Args, typename Meta>
Args
parsepyargs(Meta&& meta, pyref py_args, pyref py_kwargs)
{
  namespace h = hana;
  return parsepyargs_make_struct<Args>(
      AUTOFWD(meta),
      [&](auto&& format_string,
          auto&& varargs,
          auto&& kwarg_names) {
        auto make_kwarg_list = []() constexpr {
          return h::unpack(
              std::decay_t<decltype(kwarg_names)>(),
              [](auto... args) constexpr {
                using KwArray = std::array<
                  const char*, sizeof...(args) + 1>;
                return KwArray{args.c_str()...};
              });
        };
        if constexpr (use_fast_kw_parser) {
          static constexpr auto kwarg_list = make_kwarg_list();
          static constexpr auto format_string_static =
              std::decay_t<decltype(format_string)>().c_str();
          static _PyArg_Parser parser =
              { format_string_static, kwarg_list.data() };
          if (!h::unpack(varargs,
                         h::partial(_PyArg_ParseTupleAndKeywordsFast,
                                    py_args.get(),
                                    py_kwargs.get(),
                                    &parser)))
            throw_current_pyerr();
        } else {
          if (!h::unpack(
                  varargs,
                  h::partial(
                      PyArg_ParseTupleAndKeywords,
                      py_args.get(),
                      py_kwargs.get(),
                      format_string.c_str(),
                      const_cast<char**>(make_kwarg_list().data()))))
            throw_current_pyerr();
        }
      });
}

py_char_code&
py_char_code::operator=(int value) noexcept
{
  this->value = value;
  return *this;
}

py_char_code::operator char()
{
  if (this->value < std::numeric_limits<char>::min() ||
      this->value > std::numeric_limits<char>::max())
    _throw_pyerr_fmt(PyExc_ValueError,
                     "cannot convert to char: %d",
                     this->value);
  return static_cast<char>(this->value);
}

// Work around a Clang bug that makes compilation choke if we wrote
// BOOST_HANA_STRING("O") inside make_pyobject_converter.
constexpr auto pyparsetuple_object_string = BOOST_HANA_STRING("O");

template<typename Function>
constexpr
auto
make_pyobject_converter(Function&& function)
{
  return [fn{AUTOFWD(function)}]
      (auto&& field_type, auto&& parse_arg, auto&& default_) {
    constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
    PyObject* value;
    using FieldType =
        typename std::decay_t<decltype(field_type)>::type;
    using ConverterReturnType =
        typename std::decay_t<decltype(fn(value))>;
    using ConverterTraits = pyref_traits<ConverterReturnType>;
    using FieldTraits = pyref_traits<FieldType>;
    static_assert(!(ConverterTraits::is_owner && !FieldTraits::is_owner),
                  "use owning references in argument lists: "
                  "pyref will dangle!");
    if constexpr (has_default) {
      value = nullptr;
    }
    return parse_arg(
        pyparsetuple_object_string,
        hana::make_tuple(&value),
        [&] {
          if constexpr (has_default) {
            if (FieldTraits::is_pyref &&
                !std::is_same_v<typename FieldTraits::RefType, PyObject> &&
                value == Py_None)
              value = nullptr;
            if (!value)
              return default_();
          }
          return fn(value);
        });
  };
}

#define PARSEPYARGS_DEFAULT_1(value)                                    \
  [&]() { return (value); }

#define PARSEPYARGS_DEFAULT(arg_spec)                                   \
  BOOST_PP_IIF(                                                         \
      BOOST_PP_GREATER_EQUAL(BOOST_PP_LIST_SIZE(arg_spec), 3),          \
      PARSEPYARGS_DEFAULT_1(BOOST_PP_LIST_AT(arg_spec, 2)),             \
      no_default)

#define PARSEPYARGS_CONVERTER(arg_spec)                                 \
  BOOST_PP_IIF(                                                         \
      BOOST_PP_GREATER_EQUAL(BOOST_PP_LIST_SIZE(arg_spec), 4),          \
      BOOST_PP_LIST_AT(arg_spec, 3),                                    \
      default_converter)

#define PARSEPYARGS_ARG_SPEC_TO_META(_r, _d, arg_spec)                  \
  ::boost::hana::make_pair(PARSEPYARGS_DEFAULT(arg_spec),               \
                           PARSEPYARGS_CONVERTER(arg_spec))

#define PARSEPYARGS_MAKE_METADATA(arg_specs)                            \
  ::boost::hana::make_tuple(                                            \
       BOOST_PP_LIST_ENUM(                                              \
           BOOST_PP_LIST_TRANSFORM(                                     \
               PARSEPYARGS_ARG_SPEC_TO_META,                            \
               _,                                                       \
               arg_specs)))

#define PARSEPYARGS_ARG_SPEC_TO_HANA_FIELD_SPEC(d, _data, arg_spec)     \
  (BOOST_PP_LIST_AT_D(d, arg_spec, 0),                                  \
   BOOST_PP_LIST_AT_D(d, arg_spec, 1))

#define PARSEPYARGS_DEFINE_ARGS_STRUCT(struct_name, arg_specs)          \
  struct struct_name final {                                            \
    BOOST_PP_EXPAND(                                                    \
        BOOST_HANA_DEFINE_STRUCT                                        \
        BOOST_PP_LIST_TO_TUPLE(                                         \
            BOOST_PP_LIST_CONS(                                         \
                struct_name,                                            \
                BOOST_PP_LIST_TRANSFORM(                                \
                    PARSEPYARGS_ARG_SPEC_TO_HANA_FIELD_SPEC,            \
                    _,                                                  \
                    arg_specs))));                                      \
  };                                                                    \
  static_assert(true, "force trailing semicolon")

#define PARSEPYARGS_EXPORT_ARG_STRUCT_MEMBER_1(struct_name, field_name) \
  /*NOLINT*/ auto& field_name = struct_name.field_name;

#define PARSEPYARGS_EXPORT_ARG_STRUCT_MEMBER(_r, struct_name, arg_spec) \
  PARSEPYARGS_EXPORT_ARG_STRUCT_MEMBER_1(                               \
      struct_name,                                                      \
      BOOST_PP_LIST_AT(arg_spec, 1))

#define PARSEPYARGS_EXPORT_STRUCT_MEMBERS(from, arg_specs)              \
  BOOST_PP_LIST_FOR_EACH(                                               \
      PARSEPYARGS_EXPORT_ARG_STRUCT_MEMBER, from, arg_specs);           \
  static_assert(true, "force trailing semicolon")

template<typename Args>
struct PyParseTupleArgs final : NoCopy, NoMove {
  union Hack final {
    bool dummy;
    Args args;
    Hack() : dummy() {}  // NOLINT
    ~Hack() noexcept {}  // NOLINT
  } u;
  bool initialized = false;
  ~PyParseTupleArgs() {
    if (!this->initialized)
      this->u.args.~Args();
  }
};

#define PARSEPYARGS_MUNGE_ARG_SPEC(_d, _data, arg_spec)                 \
  BOOST_PP_TUPLE_TO_LIST(arg_spec)

#define PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs)                          \
  BOOST_PP_LIST_TRANSFORM(                                              \
      PARSEPYARGS_MUNGE_ARG_SPEC,                                       \
      _,                                                                \
      BOOST_PP_SEQ_TO_LIST(DCTV_WRAP_SEQ(arg_specs)))

#define PARSEPYARGS_INTERNAL(struct_name, arg_specs)                    \
  PARSEPYARGS_DEFINE_ARGS_STRUCT(struct_name, arg_specs);               \
  ::dctv::PyParseTupleArgs<struct_name> struct_name;                    \
  PARSEPYARGS_EXPORT_STRUCT_MEMBERS(struct_name.u.args, arg_specs);     \
  ([&](auto&&... parse_args) {                                          \
    assume(!struct_name.initialized);                                   \
    new (&struct_name.u.args) struct struct_name(                       \
        ::dctv::parsepyargs<struct struct_name>(                        \
             PARSEPYARGS_MAKE_METADATA(arg_specs),                      \
             ::std::forward<decltype(parse_args)>(parse_args)...));     \
    struct_name.initialized = true;                                     \
  })

#define PARSEPYARGS_V_INTERNAL(struct_name, arg_specs)                  \
  ([&](auto&&... parse_args) {                                          \
    PARSEPYARGS_DEFINE_ARGS_STRUCT(struct_name, arg_specs);             \
    return ::dctv::parsepyargs<struct struct_name>(                     \
        PARSEPYARGS_MAKE_METADATA(arg_specs),                           \
        ::std::forward<decltype(parse_args)>(parse_args)...);           \
  })

}  // namespace dctv
