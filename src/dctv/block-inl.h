// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

Block::Block(unique_obj_pyref<Hunk> data,
             unique_obj_pyref<Hunk> mask)
    : data(std::move(data.notnull())),
      mask(std::move(mask))
{
  assert(this->data->is_frozen());
  if (this->mask) {
    assert(this->mask->get_dtype() == type_descr<bool>());
    assert(this->data->get_size() == this->mask->get_size());
    assert(this->mask->is_frozen());
  }
}

npy_intp
Block::get_size() const noexcept
{
  return this->data->get_size();
}

dtype_ref
Block::get_dtype() const noexcept
{
  return this->data->get_dtype();
}

bool
Block::has_mask() const noexcept
{
  return static_cast<bool>(this->mask);
}

bool
Block::is_broadcasted() const noexcept
{
  return this->data->is_broadcasted() &&
      (!this->has_mask() || this->mask->is_broadcasted());
}

unique_pyarray
Block::get_data() const
{
  return this->data->inflate();
}

unique_pyarray
Block::get_mask() const
{
  return this->mask ? this->mask->inflate() : unique_pyarray();
}

Hunk*
Block::get_data_hunk()
{
  return this->data.get();
}

Hunk*
Block::get_mask_hunk()
{
  return this->mask.get();
}

}  // namespace dctv
