// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include "pyutil.h"

namespace dctv {

void init_thread_analysis(pyref m);

// These are the thread states that DCTV itself uses to model thread
// execution.  Unlike the raw Linux thread model, we distinguish
// between runnable threads and running threads.  We don't have a
// separate state for dead and zombie threads: we don't represent
// those in the process tables at all.

// TODO(dancol): further simplify by combining sleeping and
// disk sleep.

static inline constexpr auto thread_states = make_array<char>(
    'r' /* Runnable */,
    'R' /* Running */,
    'S' /* Sleeping */,
    'D' /* Disk sleep */
);


}  // namespace dctv
