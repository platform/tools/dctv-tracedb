# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Gtk.TreeView is broken for models that have a large number of rows

FhView cuts out the complexity of the GTK TreeView/TreeModel. It
exposes a simple model where each column is an object that's
responsible for updating a Gtk.CellAreaBox containing its own content
using a callback invoked directly from the display code.
"""

import logging
from math import ceil
from modernmp.util import the
from .gtk_util import (
  GObject,
  Gdk,
  Gtk,
  DctvGtkHack,
  cairo,
  ScrollOptimizer,
  distribute_natural_allocation,
  make_gdk_rect,
  make_gdk_window,
  safe_destroy,
  saved_style,
  window_ancestor_p,
  work_around_style_bug,
)

from .util import (
  Timed,
  ExplicitInheritance,
  abstract,
  override,
  final,
)

from .util import DummyTimed as Timed

Dir = Gtk.DirectionType
log = logging.getLogger(__name__)

class ColumnDrag(ExplicitInheritance):
  """Drag stuff"""

  @override
  def __init__(self):
    self.bias_x = self.bias_y = 0

  @abstract
  def _do_on_motion(self, delta_x, delta_y):
    raise NotImplementedError("abstract")

  @abstract
  def on_finish(self, *, cancel):
    """Called when drag ends.

    If CANCEL, effect of drag should be reverted.
    """
    raise NotImplementedError("abstract")

  @final
  def on_motion(self, delta_x, delta_y):
    """Called as drag progresses.

    DELTA_X and DELTA_Y are offsets in pixels from drag start.
    """
    self._do_on_motion(delta_x + self.bias_x, delta_y + self.bias_y)

class FhHeaderBox(Gtk.Viewport):
  """Widget that holds a header widget"""
  __gtype_name__ = "FhHeaderBox"

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    assert self.get_has_window()
    style = self.get_style_context()
    style.add_class("button")
    style.remove_class("frame")
    self.set_events(
      Gdk.EventMask.BUTTON_PRESS_MASK |
      Gdk.EventMask.BUTTON_RELEASE_MASK |
      Gdk.EventMask.BUTTON_MOTION_MASK)

# Ugh: awful hack. Some GTK themes don't respect the "button" class
# and look for the button element name only --- so we pretend to be a
# button for style purposes and reset any undesired styles in gui.css.
FhHeaderBox.set_css_name("button") # Ugh

class ColumnResizeDrag(ColumnDrag):
  """Resizing a column"""

  @override
  def __init__(self, column):
    super().__init__()
    self.__column = the(FhColumn, column)
    self.__initial_width = column.header_widget.get_allocated_width()
    self.__initial_explicit_width = column.explicit_width
    self.__min_width = 10

  @override
  def _do_on_motion(self, delta_x, _delta_y):
    new_width = max(int(self.__initial_width + delta_x), 0)
    new_width = max(self.__min_width, new_width)
    self.__column.explicit_width = new_width

  @override
  def on_finish(self, *, cancel):
    if cancel:
      self.__column.explicit_width = self.__initial_explicit_width

class ColumnReorderDrag(ColumnDrag):
  """Changing column order"""

  @override
  def __init__(self, column, event, offset_x):
    super().__init__()
    column.owner.inhibit_column_expand()
    self.__column = the(FhColumn, column)
    self.__orig_columns = column.owner.columns
    self.__x_basis = column.header_widget.get_allocation().x
    self.__dx_basis = 0
    if (column is column.owner.columns[-1] and
        window_ancestor_p(column.header_widget.get_window(), event.window)):
      win = event.window
      hdr_w = column.header_widget.get_window()
      x = event.x - offset_x
      while win is not hdr_w:
        x += win.get_position().x
        win = win.get_parent()
      if x > win.get_width():
        adj = x - win.get_width()
        self.__dx_basis -= adj
    self.__saved_explicit_width = column.explicit_width
    self.__saved_explicit_header_height = column.explicit_header_height
    column.explicit_width = column.width
    column.explicit_header_height = \
      column.header_widget.get_allocation().height
    column.owner.column_drag_promote(column, event.device)

  @override
  def _do_on_motion(self, delta_x, _delta_y):
    delta_x = int(delta_x)
    rel_delta_x = delta_x - self.__dx_basis
    self.__maybe_reorder_columns(rel_delta_x)
    rel_delta_x = delta_x - self.__dx_basis
    self.__column.owner.set_drag_offset(rel_delta_x + self.__x_basis)

  @override
  def on_finish(self, *, cancel):
    if cancel:
      self.__column.owner.columns = self.__orig_columns
    column = self.__column
    self.__column = None
    column.owner.disinhibit_column_expand()
    column.owner.column_drag_demote(column)
    column.explicit_header_height = self.__saved_explicit_header_height
    column.explicit_width = self.__saved_explicit_width
    column.header_widget.child_focus(Dir.TAB_FORWARD)

  def __maybe_reorder_columns(self, rel_delta_x):
    column = self.__column
    fh_view = column.owner
    old_columns = list(fh_view.columns)
    new_columns = self.__reorder_column_list_for_drag(
      old_columns, column, rel_delta_x)
    if old_columns != new_columns:
      assert len(old_columns) == len(new_columns)
      old_x = sum(c.width for c in
                  old_columns[:old_columns.index(column)])
      new_x = sum(c.width for c in
                  new_columns[:new_columns.index(column)])
      adj = new_x - (old_x + rel_delta_x)
      self.__dx_basis += rel_delta_x + adj
      self.__x_basis = new_x
      fh_view.columns = new_columns

  @staticmethod
  def __reorder_column_list_for_drag(columns, moving_column, delta_x):
    """Compute a new column list based on an existing column list and an offset.

    COLUMNS is the current list of columns. MOVING_COLUMN is the
    column we're dragging. DELTA_X is the amount by which we've
    dragged that column.
    """

    widths = [c.width for c in columns]
    index = columns.index(moving_column)
    orig_x = sum(widths[:index])
    wanted_x = int(delta_x + orig_x)
    if index < len(columns) - 1:
      wanted_x -= widths[index+1] // 2

    reordered_columns = []
    inserted_self = False

    nr_initial_immobile = 0
    for column in columns:
      if column.can_move:
        break
      else:
        nr_initial_immobile += 1

    x = 0
    for i, (column, width) in enumerate(zip(columns, widths)):
      if not inserted_self and wanted_x <= x and i >= nr_initial_immobile:
        reordered_columns.append(moving_column)
        x += widths[index]
        inserted_self = True
      if column is not moving_column:
        reordered_columns.append(column)
      x += width
    if not inserted_self:
      reordered_columns.append(moving_column)
    return reordered_columns

class FhColumn(object):
  """One column in a FhStore"""

  GRIP_WIDTH = 6

  def __init__(self, *, label):
    self.__cell_area = Gtk.CellAreaBox()
    self.__cell_area_context = self.__cell_area.create_context()

    self.__can_move = True
    self.__can_resize = True
    self.__explicit_width = None

    self.__label = the(str, label)

    self.__owner = None
    self.__wgrip = None
    self.__header_widget = None
    self.__header_content_widget = Gtk.Button.new_with_label(label)
    self.__header_content_widget.show_all()

    self.explicit_header_height = None

  @property
  def wgrip(self):
    """The grip window"""
    assert self.__wgrip
    return self.__wgrip

  def size_allocate(self, alloc):
    """Allocate space to the column

    ALLOC is a GdkRectangle, just as if this object were a GTK widget.
    """
    self.header_widget.size_allocate(alloc)
    if self.__wgrip:
      self.__wgrip.move_resize(
        alloc.x - self.GRIP_WIDTH // 2, alloc.y,
        self.GRIP_WIDTH, alloc.height)

  @property
  def header_widget(self):
    """The column header_widget widget

    Valid only while the column is part of a FhView. At other times,
    you want header_content_widget.
    """
    assert self.__header_widget
    return self.__header_widget

  @property
  def header_content_widget(self):
    """The actual contents of the header widget.

    Valid at all times.
    """
    return self.__header_content_widget

  @header_content_widget.setter
  def header_content_widget(self, widget):
    """Set the header content widget"""
    if self.__header_content_widget is not widget:
      assert isinstance(widget, Gtk.Widget)
      assert not widget.get_parent()
      if self.__header_widget:
        self.__header_widget.remove(self.__header_content_widget)
        self.__header_widget.add(widget)
      self.__header_content_widget = widget

  def get_header_widget_preferred_height(self):
    """Return height we want header widget to have"""
    # Unconditional call to get_preferred_height avoids GTK+ warning
    # about requestless allocation.
    height = self.header_widget.get_preferred_height()[0]
    if self.explicit_header_height is not None:
      height = self.explicit_header_height
    return height

  @property
  def can_move(self):
    """Whether the user is allowed to drag this column"""
    return self.__can_move

  @can_move.setter
  def can_move(self, can_move):
    """Set the move-ability of this column"""
    self.__can_move = bool(can_move)

  @property
  def can_resize(self):
    """Whether the user is allowed to resize this column"""
    return self.__can_resize

  @can_resize.setter
  def can_resize(self, can_resize):
    self.__can_resize = bool(can_resize)
    if self.__owner and self.__owner.get_mapped():
      self.__owner.map_buttons()

  @property
  def owner(self):
    """FhView owning this column or None"""
    return self.__owner

  @owner.setter
  def owner(self, new_owner):
    """Set the owner. Illegal to claim an owned column"""
    if self.__owner is new_owner:
      pass
    elif not new_owner:
      assert new_owner is None
      assert isinstance(self.__owner, FhView)
      self._on_disown()
      self.__owner = None
    else:
      assert isinstance(new_owner, FhView)
      assert not self.__owner
      self.__owner = new_owner
      self._on_own()

  @property
  def explicit_width(self):
    """Explicitly-specified width or None if automatic sizing"""
    return self.__explicit_width

  @explicit_width.setter
  def explicit_width(self, value):
    assert isinstance(value, (type(None), int))
    assert value is None or value >= 0
    if self.__explicit_width != value:
      self.__explicit_width = value
      if self.__owner:
        self.__owner.queue_resize()

  @property
  def width(self):
    """Retrieve the actual allocated width"""
    return self.header_widget.get_allocated_width()

  def __on_cell_renderers_changed(self):
    if self.owner:
      self.owner.invalidate_row_height()
      self.owner.queue_resize()
      self.owner.force_full_redraw()

  def add_renderer(self,  # pylint: disable=too-many-arguments
                   renderer,
                   expand=False,
                   align=False,
                   fixed=True,
                   at_end=False):
    """Pack a Gtk.CellRenderer at the start"""
    fn = self.__cell_area.pack_end if at_end else self.__cell_area.pack_end
    fn(renderer, expand, align, fixed)
    self.__on_cell_renderers_changed()

  def clear_renderers(self):
    """Remove any placed Gtk.CellRenderer instances"""
    children = []
    # pylint: disable=unnecessary-lambda
    self.__cell_area.foreach(lambda child: children.append(child))
    for child in children:
      self.__cell_area.remove(child)
    self.__on_cell_renderers_changed()

  def prepare(self, row_number):
    """Put this column in a position to render the indicator row

    A row number of -1 is a dummy row we use for computing the row
    height of the FhView. Note that we never get -1 here if
    prepare_dummy_row is overridden.
    """
    pass

  def prepare_dummy_row(self):
    """Prepare a dummy row used for sizing rows."""
    self.prepare(-1)

  def compute_nat_row_height(self, fh_view):
    """Retrieve the best height for rendering this column."""
    self.__cell_area_context.reset()
    self.prepare_dummy_row()
    self.__cell_area.get_preferred_height(self.__cell_area_context, fh_view)
    _, nat_height = self.__cell_area_context.get_preferred_height()
    return nat_height

  def get_preferred_column_width(self, fh_view):
    """Retrieve the desired width of this column."""
    if self.__explicit_width is not None:
      return self.__explicit_width, self.__explicit_width
    self.__cell_area_context.reset()
    self.prepare_dummy_row()
    self.__cell_area.get_preferred_width(self.__cell_area_context, fh_view)
    min_cwidth, nat_cwidth = self.__cell_area_context.get_preferred_width()
    min_hwidth, nat_hwidth = self.header_widget.get_preferred_width()
    return max(min_cwidth, min_hwidth), max(nat_cwidth, nat_hwidth)

  def render_cell(self,  # pylint: disable=too-many-arguments
                  owning_widget,
                  cr,
                  bg_area,
                  area,
                  cell_renderer_state,
                  paint_focus):
    """Paint this column in the indicated way"""
    self.__cell_area.render(
      self.__cell_area_context,
      owning_widget,
      cr,
      bg_area,
      area,
      cell_renderer_state,
      paint_focus)

  def realize_header_widget(self):
    """Private for FhView"""
    assert not self.__wgrip
    owner = self.owner
    header_widget = self.header_widget
    header_widget.set_parent_window(owner.wheader)
    alloc = header_widget.get_allocation()
    cursor = Gdk.Cursor.new_for_display(
      owner.get_window().get_display(),
      Gdk.CursorType.SB_H_DOUBLE_ARROW)
    wheader = owner.wheader
    hh = wheader.get_height()
    self.__wgrip = make_gdk_window(
      parent=wheader,
      x=alloc.x + self.GRIP_WIDTH // 2,
      y=0,
      width=self.GRIP_WIDTH,
      height=hh,
      wclass=Gdk.WindowWindowClass.INPUT_ONLY,
      cursor=cursor,
      event_mask=(Gdk.EventMask.BUTTON_PRESS_MASK |
                  Gdk.EventMask.BUTTON_RELEASE_MASK |
                  Gdk.EventMask.BUTTON_MOTION_MASK),
    )
    owner.register_window(self.__wgrip)
    self.__update_header_widget()

  def unrealize_header_widget(self):
    """Private for FhView"""
    assert self.__wgrip
    self.owner.unregister_window(self.__wgrip)
    safe_destroy(self.__wgrip)
    self.__wgrip = None

  def _on_own(self):
    assert not self.__wgrip
    self._create_header_widget()
    if self.owner.get_realized():
      self.realize_header_widget()

  def _on_disown(self):
    if self.owner.get_realized():
      self.unrealize_header_widget()
    assert not self.__wgrip
    assert self.__header_content_widget.get_parent() is self.__header_widget
    self.__header_widget.remove(self.__header_content_widget)
    self.__header_widget = None

  def _create_header_widget(self):
    assert not self.__header_widget
    box = FhHeaderBox()
    box.add(self.header_content_widget)

    owner = self.owner
    if owner.get_realized():
      box.set_parent_window(self.owner.wheader)
    box.set_parent(self.owner)

    self.__header_widget = box

  def __update_header_widget(self):
    if not self.__header_widget:
      self._create_header_widget()
    if self.owner.get_realized():
      self.owner.queue_resize()

class FhView(Gtk.Container, Gtk.Scrollable):
  """List view specialized for large number of rows"""

  __gtype_name__ = "FhView"

  __gproperties__ = {
    "hscroll-policy": (Gtk.ScrollablePolicy, "hscroll-policy",
                       "hscroll-policy", Gtk.ScrollablePolicy.MINIMUM,
                       GObject.PARAM_READWRITE),
    "hadjustment": (Gtk.Adjustment, "hadjustment", "hadjustment",
                    GObject.PARAM_READWRITE),
    "vscroll-policy": (Gtk.ScrollablePolicy, "hscroll-policy",
                       "hscroll-policy", Gtk.ScrollablePolicy.MINIMUM,
                       GObject.PARAM_READWRITE),
    "vadjustment": (Gtk.Adjustment, "hadjustment", "hadjustment",
                    GObject.PARAM_READWRITE),
  }

  __gsignals__ = {
    "columns-changed": (GObject.SIGNAL_RUN_FIRST, None, ()),
    "row-clicked": (GObject.SIGNAL_RUN_FIRST, None,
                    (GObject.TYPE_PYOBJECT,
                     GObject.TYPE_INT,
                     GObject.TYPE_PYOBJECT,
                    )),
  }

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.set_can_focus(True)
    self.set_redraw_on_allocate(True)
    self.set_has_window(True)

    self.connect("destroy", self.__on_destroy)
    self.__dummy_cell_renderer = Gtk.CellRendererText()
    self.__total_rows = 0
    self.__row_height = None
    self.__header_height = 0
    self.__columns = ()
    self.__focus_column = None
    self.__last_alloc_extra_space = 0

    self.__vadjustment = None
    self.__hadjustment = None
    self.__vscroll_policy = Gtk.ScrollablePolicy.NATURAL
    self.__hscroll_policy = Gtk.ScrollablePolicy.NATURAL

    self.__table_width = 0

    self.__wtable = None
    self.__wheader = None
    self.__wdrag = None

    self.__scroll_optimizer = ScrollOptimizer(self)

    # When a drag gesture starts, we don't know whether it's going to
    # ultimately be an actual column drag or some kind of pass-through
    # to the underlying button, so we remember the column here in
    # gesture start so we can use it in gesture update.
    self.__maybe_header_drag = None
    self.__column_drag = None
    self.__inhibit_column_expand = 0

    cdd = self.__column_drag_gesture = Gtk.GestureDrag(widget=self)
    cdd.connect("drag-begin", self.__on_column_drag_begin)
    cdd.connect("drag-update", self.__on_column_drag_update)
    cdd.connect("drag-end", self.__on_column_drag_end)
    cdd.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)

    self.add_events(
      Gdk.EventMask.BUTTON_PRESS_MASK |
      Gdk.EventMask.BUTTON_RELEASE_MASK |
      Gdk.EventMask.SCROLL_MASK |
      Gdk.EventMask.TOUCH_MASK |
      Gdk.EventMask.SMOOTH_SCROLL_MASK)

    self.get_style_context().add_class("view")

  @property
  def wheader(self):
    """The GDK header window. View must be realized."""
    assert self.__wheader
    return self.__wheader

  def map_buttons(self):
    """Rearrange button windows"""
    # Must be idempotent. We call this function whenever the column
    # list changes even if we're already mapped.
    columns = list(self.__columns)
    wdrag = self.__wdrag
    first_column = columns and columns[0]
    for column in columns:
      header_widget = column.header_widget
      header_widget.reset_style()
      header_widget.show()
      if header_widget.get_visible() and not header_widget.get_mapped():
        header_widget.map()

    prev_column = None
    for column in columns:
      wgrip = column.wgrip
      if (column is first_column or
          (prev_column and not prev_column.can_resize)):
        wgrip.hide()
      else:
        wgrip.raise_()
        wgrip.show()
      prev_column = column
    self.__wheader.show()
    if wdrag:
      wdrag.raise_()

  def force_full_redraw(self):
    """Force a full table redraw"""
    self.__scroll_optimizer.invalidate()
    if self.__wtable:
      self.__wtable.invalidate_rect(None, True)

  def do_realize(self):
    """Gtk.Object protocol"""
    assert not self.__wtable
    assert not self.__wheader

    self.set_realized(True)
    alloc = self.get_allocation()
    real_width = max(self.__table_width, alloc.width)

    # Overall clip window
    wclip = make_gdk_window(
      parent=self.get_parent_window(),
      window_type=Gdk.WindowType.CHILD,
      x=alloc.x,
      y=alloc.y,
      width=alloc.width,
      height=alloc.height,
      wclass=Gdk.WindowWindowClass.INPUT_ONLY,
      visual=self.get_visual(),
      event_mask=Gdk.EventMask.VISIBILITY_NOTIFY_MASK)
    self.set_window(wclip)
    self.register_window(wclip)

    # Table window
    self.__wtable = make_gdk_window(
      parent=wclip,
      x=0,
      y=self.__header_height,
      width=real_width,
      height=alloc.height,
      event_mask=(Gdk.EventMask.EXPOSURE_MASK |
                  Gdk.EventMask.SCROLL_MASK |
                  Gdk.EventMask.SMOOTH_SCROLL_MASK |
                  Gdk.EventMask.POINTER_MOTION_MASK |
                  Gdk.EventMask.ENTER_NOTIFY_MASK |
                  Gdk.EventMask.LEAVE_NOTIFY_MASK |
                  Gdk.EventMask.BUTTON_PRESS_MASK |
                  Gdk.EventMask.BUTTON_RELEASE_MASK |
                  self.get_events()))
    self.register_window(self.__wtable)

    # Header window
    self.__wheader = make_gdk_window(
      parent=wclip,
      x=0,
      y=0,
      width=real_width,
      height=self.__header_height,
      event_mask=(Gdk.EventMask.EXPOSURE_MASK |
                  Gdk.EventMask.SCROLL_MASK |
                  Gdk.EventMask.ENTER_NOTIFY_MASK |
                  Gdk.EventMask.LEAVE_NOTIFY_MASK |
                  Gdk.EventMask.BUTTON_PRESS_MASK |
                  Gdk.EventMask.BUTTON_RELEASE_MASK |
                  Gdk.EventMask.KEY_PRESS_MASK |
                  Gdk.EventMask.KEY_RELEASE_MASK |
                  self.get_events()))
    self.register_window(self.__wheader)
    for column in self.__columns:
      column.realize_header_widget()

  def do_unrealize(self):
    """Gtk.Widget protocol"""
    for column in self.__columns:
      column.unrealize_header_widget()
    assert not self.__wdrag
    self.unregister_window(self.__wtable)
    safe_destroy(self.__wtable)
    self.__wtable = None
    self.unregister_window(self.__wheader)
    safe_destroy(self.__wheader)
    self.__wheader = None
    self.__maybe_header_drag = None
    Gtk.Container.do_unrealize(self)

  def do_map(self):
    """Gtk.Widget protocol"""
    self.set_mapped(True)
    self.__wtable.show()
    self.map_buttons()
    self.get_window().show()

  def do_unmap(self):
    """Gtk.Widget protocol"""
    if self.__column_drag:
      self.__column_drag.on_cancel()
      self.__column_drag = None
    self.__maybe_header_drag = None
    Gtk.Container.do_unmap(self)

  def do_button_press_event(self, event):
    if event.button == Gdk.BUTTON_MIDDLE:
      resized_column, _ = self.__find_dragged_column(event)
      if resized_column:
        # Reset to default width on middle click
        resized_column.explicit_width = None
        return True
    return Gtk.Container.do_button_press_event(self, event)

  def do_forall(self, include_internals, callback, *callback_parameters):
    """Gtk.Container protocol"""
    if not include_internals:
      return
    for column in list(self.__columns):
      header_widget = column.header_widget
      if header_widget:
        callback(header_widget, *callback_parameters)

  @property
  def columns(self):
    """Return a refernece to the current column list"""
    return self.__columns

  @columns.setter
  def columns(self, in_new_columns):
    """Set a new column list"""
    new_columns = tuple(in_new_columns)
    if self.__columns == new_columns:
      return  # Nothing to do
    old_columns_set = set(self.__columns)
    new_columns_set = set(new_columns)
    added_columns = new_columns_set - old_columns_set
    removed_columns = old_columns_set - new_columns_set
    for added_column in added_columns:
      assert added_column.owner is None
      added_column.owner = self
    for removed_column in removed_columns:
      assert removed_column.owner is self
      removed_column.owner = None
      if self.__focus_column is removed_column:
        self.__focus_column = None
    self.__columns = new_columns
    if self.get_mapped():
      self.map_buttons()
    if added_columns or removed_columns:
      self.invalidate_row_height()
    self.force_full_redraw()
    self.queue_resize()
    self.__maybe_header_drag = None
    self.emit("columns-changed")

  @property
  def total_rows(self):
    """Return the number of rows for which we're configured"""
    return self.__total_rows

  @total_rows.setter
  def total_rows(self, total_rows):
    """Change the number of rows for which we're configured"""
    assert isinstance(total_rows, int)
    if self.__total_rows != total_rows:
      self.__total_rows = total_rows
      self.__configure_vadjustment()
      self.queue_resize()

  @property
  def row_height(self):
    """The lazily-computed row height"""
    row_height = self.__row_height
    if row_height is None:
      row_height = self.__row_height = self.__recompute_row_height()
    return the(int, row_height)

  def __recompute_row_height(self):
    """Recompute the preferred row height for this widget.

    We configure all columns with a dummy value and them for their
    heights.
    """
    nat_height = 1
    for column in self.__columns:
      nat_height = max(nat_height, column.compute_nat_row_height(self))
    return nat_height

  def invalidate_row_height(self):
    """Signal that we should recompute row height"""
    self.__row_height = None
    self.queue_resize()
    self.force_full_redraw()

  def do_get_preferred_width(self):  # pylint: disable=arguments-differ
    """Gtk.Object protocol"""
    width_requests = [c.get_preferred_column_width(self)
                      for c in self.__columns]
    min_width = sum(req[0] for req in width_requests)
    nat_width = sum(req[1] for req in width_requests)
    return min_width, nat_width

  def do_get_preferred_height(self):  # pylint: disable=arguments-differ
    """Gtk.Object protocol"""
    hh = self.__header_height = max(
      (c.get_header_widget_preferred_height() for c in self.__columns),
      default=0)
    nat_height = min(self.row_height * self.total_rows, 10000)
    min_h = hh + self.row_height
    nat_h = hh + nat_height * max(1, self.total_rows)
    max_int = 2**31 -1
    return min(max_int, min_h), min(max_int, nat_h)

  def do_get_request_mode(self):
    """Gtk.Widget protocol"""
    return Gtk.SizeRequestMode.CONSTANT_SIZE

  def inhibit_column_expand(self):
    """Temporarily block expanding the last column to fill allocation

    Used during drag. Balance with disinhibit_column_expand().
    """
    self.__inhibit_column_expand += 1
    if self.__inhibit_column_expand == 1:
      if self.__last_alloc_extra_space:
        self.queue_resize()
        if self.get_mapped():
          self.do_size_allocate(self.get_allocation())

  def disinhibit_column_expand(self):
    """Undo the effect of inhibit_column_expand()"""
    assert self.__inhibit_column_expand >= 1
    self.__inhibit_column_expand -= 1
    if not self.__inhibit_column_expand:
      if self.__last_alloc_extra_space:
        self.queue_resize()
        if self.get_mapped():
          self.do_size_allocate(self.get_allocation())

  def __size_allocate_columns(self):
    """Update the header buttons to match our size"""
    width = self.get_allocated_width()
    columns = self.__columns
    column_sizes, extra_space = distribute_natural_allocation(
      width,
      (column.get_preferred_column_width(self) for column in columns))
    assert len(columns) == len(column_sizes)
    self.__last_alloc_extra_space = extra_space
    if columns and extra_space and not self.__inhibit_column_expand:
      column_sizes[-1] += extra_space
    x = 0
    wdrag = self.__wdrag
    for column, w_allocation in zip(columns, column_sizes):
      walloc = make_gdk_rect(x, 0, w_allocation, self.__header_height)
      if column.width != w_allocation:
        self.force_full_redraw()
      if wdrag and column.header_widget.get_parent_window() is wdrag:
        #assert walloc.width == wdrag.get_width()
        assert walloc.height == wdrag.get_height()
        walloc.x = 0
      column.size_allocate(walloc)
      x += w_allocation
    self.__table_width = x

  def do_size_allocate(self, alloc):  # pylint: disable=arguments-differ
    """Gtk.Object protocol"""
    self.set_allocation(alloc)
    self.__size_allocate_columns()
    self.__configure_hadjustment()
    self.__configure_vadjustment()
    if not self.get_realized():
      return
    self.get_window().move_resize(
      alloc.x, alloc.y,
      alloc.width, alloc.height)
    real_width = max(self.__table_width, alloc.width)
    _vscroll, hscroll = self.__get_scroll_offsets()
    self.__wheader.move_resize(
      -hscroll, 0,
      real_width, self.__header_height)
    self.__wtable.move_resize(
      0, self.__header_height,
      real_width,
      max(0, alloc.height - self.__header_height))

  def __get_scroll_offsets(self):
    """Return commanded scroll offsets

    Return a tuple (VSCROLL, HSCROLL) giving the vertical and horizontal
    displacements, in pixels, commanded by any attached adjustments."""
    vscroll = int(self.__vadjustment.get_value()) \
              if self.__vadjustment else 0
    hscroll = int(self.__hadjustment.get_value()) \
              if self.__hadjustment else 0
    return vscroll, hscroll

  def __resolve_window_position(self, x, y):
    """Find row and column for a given window position X, Y

    Return a tuple (COLUMN, ROW_NUMBER), where COLUMN is the column
    object and ROW_NUMBER is the display row number, that corresponds
    to the point (X, Y) given in widget window coordinates.

    Return None if the point does not correspond to any part of the
    data model.
    """
    vscroll, hscroll = self.__get_scroll_offsets()
    column_x = -hscroll
    found_column = None
    for column in self.__columns:
      next_column_x = column_x + column.width
      if column_x <= x < next_column_x:
        found_column = column
        break
      column_x = next_column_x
    if found_column:
      row_height = self.row_height
      row_number = int((vscroll + y) // row_height)
      if row_number < self.total_rows:
        return found_column, row_number
    return None

  def do_button_release_event(self, event):
    if event.window is self.__wtable:
      location_info = self.__resolve_window_position(event.x, event.y)
      if location_info:
        column, row_number = location_info
        if row_number >= 0:
          self.emit("row-clicked", column, row_number, event)
      return True
    return Gtk.Container.do_button_release_event(self, event)

  def do_state_flags_changed(self, prev_flags):
    Gtk.Container.do_state_flags_changed(self, prev_flags)
    self.force_full_redraw()

  def do_style_updated(self):
    Gtk.Container.do_style_updated(self)
    self.force_full_redraw()

  def do_draw(self, cr):
    """Gtk.Object protocol"""
    if Gtk.cairo_should_draw_window(cr, self.__wtable):
      self.__draw_table(cr)
    else:
      Gtk.render_background(self.get_style_context(),
                            cr, 0, 0,
                            self.get_allocated_width(),
                            self.get_allocated_height())

    wdrag = self.__wdrag
    style = self.get_style_context()
    with saved_style(style):
      style.remove_class("view")
      if Gtk.cairo_should_draw_window(cr, self.__wheader):
        for column in self.__columns:
          header_widget = column.header_widget
          if not wdrag or header_widget.get_parent_window() is not wdrag:
            self.propagate_draw(header_widget, cr)
      if wdrag and Gtk.cairo_should_draw_window(cr, self.__wdrag):
        for column in self.__columns:
          header_widget = column.header_widget
          if header_widget.get_parent_window() is wdrag:
            self.propagate_draw(header_widget, cr)

  def __draw_table(self, cr):
    window = self.__wtable
    style = self.get_style_context()
    vscroll, hscroll = self.__get_scroll_offsets()
    with self.__scroll_optimizer.cached_render(
        window, cr, hscroll, vscroll) as (ocr, clip_region):
      self.__draw_table_1(
        ocr, style, vscroll, hscroll, clip_region)

  def __draw_table_1(
      self,
      cr,
      style,
      vscroll,
      hscroll,
      clip_region):
    """Draw all rows in clip region"""
    have_clip, clip = Gdk.cairo_get_clip_rectangle(cr)
    if not have_clip:
      return

    Gtk.render_background(self.get_style_context(),
                          cr,
                          clip.x, clip.y,
                          clip.width, clip.height)

    # row_height can't be None here
    # pylint: disable=invalid-unary-operand-type

    row_height = self.__row_height
    total_rows = self.__total_rows
    start_row = int((vscroll + clip.y) // row_height)
    end_row = start_row + int(ceil(clip.height / row_height))
    vscroll_fine = vscroll - start_row * row_height
    end_row = min(end_row + 1, total_rows)

    cr.translate(-hscroll, -vscroll_fine)
    clip_region.translate(hscroll, vscroll_fine)

    with saved_style(style):
      style.add_class("cell")
      for row_number in range(start_row, end_row):
        with Timed("drawing row {}".format(row_number)):
          with saved_style(style):
            self.__draw_row(cr, style, row_number, clip_region)
            cr.translate(0, row_height)
            clip_region.translate(0, -row_height)

  def __draw_row(self, cr, style, row_number, clip_region):
    """Draw one row"""
    cell_area = make_gdk_rect(0, 0, 0, self.row_height)
    cell_area_cr = cairo.RectangleInt(0, 0, 0, cell_area.height)
    columns = self.__columns
    for column_number in range(len(columns)):
      cell_area.width = self.__columns[column_number].width
      cell_area_cr.x = cell_area.x
      cell_area_cr.width = cell_area.width
      if (clip_region.contains_rectangle(cell_area_cr) !=
          cairo.REGION_OVERLAP_OUT):
        with saved_style(style):
          self.__draw_cell(cr, style, column_number, row_number, cell_area)
      cell_area.x += cell_area.width

  # pylint: disable=too-many-arguments
  def __draw_cell(self, cr, style, column_number, row_number, cell_area):
    """Draw a particular cell"""
    paint_focus = False
    cell_renderer_state = Gtk.CellRendererState(0)
    css_state = self.__dummy_cell_renderer.get_state(
      self,
      cell_renderer_state)
    style.set_state(css_state)

    column = self.__columns[column_number]
    with Timed("prepare"):
      column.prepare(row_number)
    with Timed("render"):
      column.render_cell(self,
                         cr,
                         cell_area,
                         cell_area,
                         cell_renderer_state,
                         paint_focus)

  @property
  def vadjustment(self):
    """The currently-configured adjustment for scrolling"""
    return self.__vadjustment

  @vadjustment.setter
  def vadjustment(self, value):
    """Change the vertical scrolling adjustment"""
    if self.__vadjustment:
      self.__vadjustment.disconnect_by_func(self.__on_adjust_changed)
    if value:
      value.connect("value-changed", self.__on_adjust_changed)
    self.__vadjustment = value
    self.__configure_vadjustment()

  @property
  def hadjustment(self):
    """The currently-configured horizontal adjustor"""
    return self.__hadjustment

  @hadjustment.setter
  def hadjustment(self, value):
    """Change the horizontal scrolling adjustment"""
    if self.__hadjustment:
      self.__hadjustment.disconnect_by_func(self.__on_adjust_changed)
    if value:
      value.connect("value-changed", self.__on_adjust_changed)
    self.__hadjustment = value
    self.__configure_hadjustment()

  def __configure_hadjustment(self):
    if not self.__hadjustment:
      return
    alloc = self.get_allocation()
    self.__hadjustment.configure(
      self.__hadjustment.get_value(),
      0,
      max(alloc.width, self.__table_width),
      alloc.width * 0.1,
      alloc.width * 0.9,
      alloc.width)

  def __configure_vadjustment(self):
    if not self.__vadjustment:
      return
    desired_height = self.row_height * self.__total_rows
    allocated_height = max(0, self.get_allocated_height() - self.__header_height)
    allocated_height = min(desired_height, allocated_height)
    self.__vadjustment.configure(
      self.__vadjustment.get_value(),
      0,
      desired_height,
      allocated_height * 0.1,
      allocated_height * 0.9,
      allocated_height)

  def __on_adjust_changed(self, adj):
    if not self.get_realized():
      return
    if adj is self.__hadjustment:
      _vscroll, hscroll = self.__get_scroll_offsets()
      self.__wheader.move(-hscroll, 0)
    # Don't use force_full_redraw(): that will invalidate the scroll
    # cache too. The whole point of the scroll cache is to respond to
    # GtkAdjustment changes without doing a full redraw.
    self.__wtable.invalidate_rect(None, True)

  @staticmethod
  def __on_destroy(self):
    assert not self.get_realized()
    self.vadjustment = None
    self.hadjustment = None
    self.columns = ()
    self.__column_drag_gesture = None

  def do_get_property(self, prop):
    """GObject protocol"""
    prop_name = prop.name
    if prop_name == "hadjustment":
      return self.hadjustment
    if prop_name == "vadjustment":
      return self.vadjustment
    if prop_name == "hscroll-policy":
      return self.__hscroll_policy
    if prop_name == "vscroll-policy":
      return self.__vscroll_policy
    raise KeyError(prop_name)

  def do_set_property(self, prop, value):
    """GObject protocol"""
    prop_name = prop.name
    if prop_name == "hadjustment":
      self.hadjustment = value
    elif prop_name == "vadjustment":
      self.vadjustment = value
    elif prop_name == "hscroll-policy":
      self.__hscroll_policy = value
      self.queue_resize()
    elif prop_name == "vscroll-policy":
      self.__vscroll_policy = value
      self.queue_resize()
    else:
      raise KeyError(prop_name)

  def do_get_path_for_child(self, child):
    """Gtk.Container protocol"""
    header_widgets = [c.header_widget for c in self.__columns]
    if child not in header_widgets:
      return Gtk.Container.do_get_path_for_child(self, child)
    npath = self.get_path().copy()
    npath.append_type(FhViewHeader.__gtype__)  # pylint: disable=no-member
    siblings = Gtk.WidgetPath()
    for header_widget in header_widgets:
      siblings.append_for_widget(header_widget)
    npath.append_with_siblings(siblings, header_widgets.index(child))
    return npath

  def __find_dragged_column(self, event):
    resized_column = None
    prev_column = None
    for column in self.__columns:
      if column.wgrip is event.window:
        assert not resized_column
        resized_column = prev_column
        break
      prev_column = column
    reordered_column = None
    wheader = self.wheader
    if not resized_column and window_ancestor_p(wheader, event.window):
      w = event.window
      x = event.x
      y = event.y
      while w is not wheader:
        w_x, w_y = w.get_position()
        x += w_x
        y += w_y
        w = w.get_parent()
      for column in self.__columns:
        ca = column.header_widget.get_allocation()
        if ca.x <= x < ca.x + ca.width and ca.y <= y < ca.y + ca.height:
          reordered_column = column
          break
    return resized_column, reordered_column

  def __set_column_drag_controller(self, gesture, column_drag):
    assert not self.__column_drag
    assert isinstance(column_drag, ColumnDrag)
    self.__column_drag = column_drag
    gesture.set_state(Gtk.EventSequenceState.CLAIMED)

  def __on_column_drag_begin(self, gesture, _start_x, _start_y):
    if not self.get_realized():
      return
    sequence = gesture.get_current_sequence()
    event = DctvGtkHack.gtk_gesture_get_last_event(gesture, sequence)
    resized_column, dragged_column = self.__find_dragged_column(event)
    if resized_column:
      if not self.has_focus():
        self.grab_focus()
      self.__set_column_drag_controller(gesture, ColumnResizeDrag(resized_column))
    elif dragged_column:
      self.__maybe_header_drag = (dragged_column, event.x, event.y)

  def column_drag_promote(self, column, device):
    """Move given column widget to its own window"""
    assert not self.__wdrag
    assert column in self.columns
    assert column.explicit_width is not None
    header_widget = column.header_widget
    assert header_widget.get_has_window()  # So we can avoid unrealize
    work_around_style_bug(header_widget)
    header_widget.get_style_context().add_class("dnd")
    column.wgrip.hide()
    alloc = header_widget.get_allocation()
    assert alloc.y == 0  # pylint: disable=compare-to-zero
    self.__wdrag = make_gdk_window(
      parent=self.wheader,
      x=alloc.x,
      y=0,
      width=alloc.width,
      height=alloc.height,
      visual=self.get_visual(),
      event_mask=(Gdk.EventMask.VISIBILITY_NOTIFY_MASK |
                  Gdk.EventMask.POINTER_MOTION_MASK))
    self.register_window(self.__wdrag)
    header_widget.unmap()
    header_widget.get_window().reparent(self.__wdrag, 0, 0)
    header_widget.set_parent_window(self.__wdrag)
    header_widget.map()
    alloc.x = 0
    header_widget.get_preferred_width()  # Silence warning
    header_widget.size_allocate(alloc)
    self.grab_focus()
    self.__wdrag.show()
    self.__wdrag.raise_()
    # The unmap broke the implicit grab, so make an explicit one.
    device.get_seat().grab(self.__wdrag,
                           Gdk.SeatCapabilities.ALL,
                           False,
                           None,
                           None,
                           None)

  def set_drag_offset(self, x):
    """Update the current drag.

    A drag column must have been set with column_drag_promote.
    """
    self.__wdrag.move(x, 0)
    self.__wdrag.raise_()

  def column_drag_demote(self, column):
    """Restore the given column widget to the header window"""
    wdrag = self.__wdrag
    assert wdrag
    header_widget = column.header_widget
    work_around_style_bug(header_widget)
    header_widget.get_style_context().remove_class("dnd")
    assert header_widget.get_parent_window() is wdrag
    header_widget.unmap()
    header_widget.get_window().reparent(self.__wheader, 0, 0)
    header_widget.set_parent_window(self.__wheader)
    self.unregister_window(wdrag)
    safe_destroy(wdrag)
    self.__wdrag = None
    self.queue_resize()
    self.map_buttons()

  def __horizontal_autoscroll(self, gesture):
    hadjustment = self.__hadjustment
    if not hadjustment:
      return
    scroll_edge_size = 15
    sequence = gesture.get_current_sequence()
    vis_x = hadjustment.get_value()
    vis_width = self.get_allocated_width()
    x = gesture.get_point(sequence).x + vis_x
    offset = x - (vis_x + scroll_edge_size)
    # pylint: disable=compare-to-zero
    if offset > 0:
      offset = x - (vis_x + vis_width - scroll_edge_size)
      if offset < 0:
        return
    offset = offset // 3
    hadjustment.set_value(max(0, vis_x + offset))
    if self.__column_drag:
      self.__column_drag.bias_x += hadjustment.get_value() - vis_x

  def __on_column_drag_update(self, gesture, offset_x, offset_y):
    sequence = gesture.get_current_sequence()
    state = DctvGtkHack.gtk_gesture_get_sequence_state(gesture, sequence)
    if state == Gtk.EventSequenceState.CLAIMED:
      if self.__column_drag:
        self.__column_drag.on_motion(offset_x, offset_y)
        if self.__wdrag:
          self.__horizontal_autoscroll(gesture)
    elif (state == Gtk.EventSequenceState.NONE and
          self.__maybe_header_drag):
      event = DctvGtkHack.gtk_gesture_get_last_event(gesture, sequence)
      reordered_column, start_x, start_y = self.__maybe_header_drag
      if (reordered_column.can_move and
          reordered_column.header_widget.drag_check_threshold(
            start_x, start_y, event.x, event.y)):
        self.__maybe_header_drag = None
        self.grab_focus()
        self.__set_column_drag_controller(
          gesture,
          ColumnReorderDrag(reordered_column, event, offset_x))

  def __on_column_drag_end(self, gesture, offset_x, offset_y):
    self.__maybe_header_drag = None
    sequence = gesture.get_current_sequence()
    is_active = DctvGtkHack.gtk_gesture_handles_sequence(gesture, sequence)
    if self.__column_drag:
      if is_active:
        self.__column_drag.on_motion(offset_x, offset_y)
        self.__column_drag.on_finish(cancel=False)
      else:
        self.__column_drag.on_finish(cancel=True)
      self.__column_drag = None

  def __header_focus(self, direction, clamp_column_visible):
    focus_child = self.get_focus_child()
    focus_column = self.__focus_column
    columns = self.__columns
    if direction in (Dir.TAB_BACKWARD, Dir.TAB_FORWARD, Dir.UP, Dir.DOWN):
      if focus_child:
        return False  # Get out of header mode
      # Try focusing something in the focus_column header first
      if focus_column:
        focus_column.header_widget.child_focus(Dir.TAB_FORWARD)
        focus_child = self.get_focus_child()
      # That didn't work? Try columns in order.
      if not focus_child:
        for column in columns:
          column.header_widget.child_focus(Dir.TAB_FORWARD)
          focus_child = self.get_focus_child()
          if focus_child:
            break
    elif direction in (Dir.LEFT, Dir.RIGHT):
      if not focus_child:
        # Try restore focus to header column; if that didn't work, try
        # columns in order again.
        if focus_column:
          focus_column.header.child_focus(Dir.TAB_FORWARD)
          focus_child = self.get_focus_child()
        if not focus_child:
          for column in columns[::-1 if direction == Dir.LEFT else 1]:
            column.header_widget.child_focus(Dir.TAB_FORWARD)
            focus_child = self.get_focus_child()
            if focus_child:
              break
      elif focus_child.child_focus(direction):
        pass  # Focus is child-internal
      else:
        # Focus next column
        focused_column_idx = 0
        while focused_column_idx < len(columns):
          column = columns[focused_column_idx]
          if window_ancestor_p(column.header_widget, focus_child):
            break
          focused_column_idx += 1
        idx_iter = 1 if direction == Dir.RIGHT else -1
        while True:
          focused_column_idx += idx_iter
          if not 0 <= focused_column_idx < len(columns):
            focus_child = None
            break
          column = columns[focused_column_idx]
          column.header_widget.child_focus(Dir.TAB_FORWARD)
          focus_child = self.get_focus_child()
          if focus_child:
            break
    else:
      assert False, "unexpected direction {!r}".format(direction)

    if focus_child:
      for column in columns:
        if window_ancestor_p(column.header_widget, focus_child):
          self.__focus_column = column
      if clamp_column_visible:
        self.__clamp_column_visible(self.__focus_column)
    return bool(focus_child)

  def __clamp_column_visible(self, column):
    hadjustment = self.__hadjustment
    if not column or not hadjustment:
      return
    allocation = column.header_widget.get_allocation()
    x = allocation.x
    width = allocation.width
    if width > hadjustment.get_page_size():
      hadjustment.set_value(x)
    elif hadjustment.get_value() + hadjustment.get_page_size() < x + width:
      hadjustment.set_value(x + width - hadjustment.get_page_size())
    elif hadjustment.get_value() > x:
      hadjustment.set_value(x)

  def do_focus(self, direction):
    """Gtk.Object protocol."""
    if not self.is_sensitive() or not self.get_can_focus():
      return False
    focus_child = self.get_focus_child()

    # Headers have focus.
    if focus_child:
      if direction in (Dir.LEFT, Dir.RIGHT):
        self.__header_focus(direction, True)
        return True
      if direction in (Dir.TAB_BACKWARD, Dir.UP):
        return False
      if direction in (Dir.TAB_FORWARD, Dir.DOWN):
        self.grab_focus()
        return True
      assert False, "unknown direction {!r}".format(direction)

    # We don't have focus at all.
    if not self.has_focus():
      self.grab_focus()
      return True

    # We have focus already.
    if direction == Dir.TAB_BACKWARD:
      return self.__header_focus(direction, False)
    if direction == Dir.TAB_FORWARD:
      return False

    # Weird case.
    self.grab_focus()
    return True

  def do_set_focus_child(self, child):
    if child:
      for column in self.__columns:
        if window_ancestor_p(column.header_widget, child):
          self.__focus_column = column
          self.queue_resize()
          break
    Gtk.Container.do_set_focus_child(self, child)


FhView.set_css_name("FhView")
class FhViewHeader(Gtk.Widget):
  """Dummy widget type used for CSS paths"""
  __gtype_name__ = "FhViewHeader"
