// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "pyutil.h"

#include <stdarg.h>
#include <assert.h>
#include <string.h>

namespace dctv {

using std::string_view;

static_assert(is_safe_pycast<PyTypeObject, PyObject>());
static_assert(!is_safe_pycast<PyObject, PyTypeObject>());
static_assert(is_safe_pycast<PyTypeObject, PyTypeObject>());

String
pystr_to_string(pyref str)
{
  return String(pystr_to_string_view(str));
}

string_view
pystr_to_string_view(pyref str)
{
  Py_ssize_t utf8_size;
  const char* utf8 = PyUnicode_AsUTF8AndSize(str.get(), &utf8_size);
  if (!utf8)
    throw_current_pyerr();
  return string_view(utf8, utf8_size);
}

const char*
pystr_to_c_string(pyref str)
{
  // Python strings have an extra NUL byte, making this safe.
  return pystr_to_string_view(str).data();
}

String
repr(pyref thing)
{
  if (!thing)
    return "<NULLPTR>";
  return pystr_to_string(adopt_check(PyObject_Repr(thing.get())));
}

String
str(pyref thing)
{
  if (!thing)
    return "<NULLPTR>";
  return pystr_to_string(adopt_check(PyObject_Str(thing.get())));
}

String
repr(string_view str)
{
  return repr(make_pystr(str));
}

unique_pyref
import_pymodule(string_view name)
{
  return adopt_check(
      PyImport_Import(make_pystr(name).as<PyObject>().get()));
}

unique_obj_pyref<PyTypeObject>
find_pyclass(string_view module_name, const char* class_name)
{
  return getattr(import_pymodule(module_name), class_name)
      .addref_as<PyTypeObject>();
}

bool
isinstance(pyref obj, obj_pyref<PyTypeObject> type) noexcept
{
  return (isinstance_exact(obj, type) ||
          PyObject_TypeCheck(obj.get(), type.get()));
}

void
check_pytype(obj_pyref<PyTypeObject> type, pyref obj)
{
  if (!isinstance(obj, type))
    throw_wrong_type_error(obj, type);
}

unique_pystr
make_pystr(string_view s, const char* errors)
{
  if (s.size() > static_cast<size_t>(
          std::numeric_limits<Py_ssize_t>::max()))
    throw_pyerr_msg(PyExc_ValueError, "string too long to convert");
  return adopt_check_as<PyUnicodeObject>(
      PyUnicode_DecodeUTF8(
          s.data(),
          static_cast<Py_ssize_t>(s.size()),
          errors));
}

unique_pystr
make_pystr_fmt(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  FINALLY(va_end(args));
  return adopt_check_as_unsafe<PyUnicodeObject>(
      PyUnicode_FromFormatV(format, args));
}

Py_ssize_t
pyseq_size(pyref seq)
{
  Py_ssize_t size = PySequence_Size(seq.get());
  if (size < 0)
    throw_current_pyerr();
  return size;
}

unique_pyref
getattr(pyref obj, const char* attr_name)
{
  return adopt_check(PyObject_GetAttrString(obj.get(), attr_name));
}

unique_pyref
getattr(pyref obj, _Py_Identifier* interned)
{
  return adopt_check(_PyObject_GetAttrId(obj.get(), interned));
}

bool
true_(pyref obj)
{
  int ret = PyObject_IsTrue(obj.get());
  if (ret < 0)
    throw_current_pyerr();
  return ret > 0;
}

bool
not_(pyref obj)
{
  int ret = PyObject_Not(obj.get());
  if (ret < 0)
    throw_current_pyerr();
  return ret > 0;
}

unique_pyref
fetch_stop_iteration_value()
{
  assume(pyerr_occurred());
  PyObject* retval;
  if (_PyGen_FetchStopIterationValue(&retval))
    throw_current_pyerr();
  assume(!pyerr_occurred());
  assume(retval);
  return adopt(retval);
}

void
register_type(pyref m, PyTypeObject* type)
{
  if (PyType_Ready(type) < 0)
    throw_current_pyerr();
  const char* base_name = strrchr(type->tp_name, '.');
  assert(base_name);
  base_name += 1;
  Py_INCREF(type);
  if (PyModule_AddObject(m.get(),
                         base_name,
                         reinterpret_cast<PyObject*>(type)))
    throw_current_pyerr();
}

void
register_functions(pyref module, PyMethodDef* methods)
{
  if (PyModule_AddFunctions(module.get(), methods))
    throw_current_pyerr();
}

void
register_constant(pyref module, const char* name, long value)
{
  if (PyModule_AddIntConstant(module.get(), name, value))
    throw_current_pyerr();
}

void
register_constant(pyref module, const char* name, unique_pyref value)
{
  if (PyModule_AddObject(module.get(), name, value.get()))
    throw_current_pyerr();
  value.release();  // Reference stolen only on success.
}

void
set_slice(pyref assignee, Py_ssize_t i1, Py_ssize_t i2, pyref value)
{
  if (PySequence_SetSlice(assignee.get(), i1, i2, value.get()))
    throw_current_pyerr();
}

unique_pyref
get_slice(pyref obj, Py_ssize_t i1, Py_ssize_t i2)
{
  return adopt_check(PySequence_GetSlice(obj.get(), i1, i2));
}

unique_pyref
get_item(pyref obj, Py_ssize_t index)
{
  return adopt_check(PySequence_GetItem(obj.get(), index));
}

unique_pyref
get_item(pyref obj, pyref item)
{
  return adopt_check(PyObject_GetItem(obj.get(), item.get()));
}

unique_obj_pyref<PyDictObject>
make_pydict()
{
  return adopt_check_as_unsafe<PyDictObject>(PyDict_New());
}

unique_obj_pyref<PySliceObject>
make_sliceobj(pyref start, pyref stop, pyref step)
{
  return adopt_check_as_unsafe<PySliceObject>(
      PySlice_New(start.get(), stop.get(), step.get()));
}

static
bool
rich_compare(pyref o1, pyref o2, int opid)
{
  int ret = PyObject_RichCompareBool(o1.get(), o2.get(), opid);
  if (ret < 0)
    throw_current_pyerr();
  return static_cast<bool>(ret);
}

static
unique_pyref
xrich_compare(pyref o1, pyref o2, int opid)
{
  return adopt_check(PyObject_RichCompare(o1.get(), o2.get(), opid));
}

bool
py_lt(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_LT);
}

bool
py_le(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_LE);
}

bool
py_eq(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_EQ);
}

bool
py_ne(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_NE);
}

bool
py_gt(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_GT);
}

bool
py_ge(pyref o1, pyref o2)
{
  return rich_compare(o1, o2, Py_GE);
}

unique_pyref
py_xlt(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_LT);
}

unique_pyref
py_xle(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_LE);
}

unique_pyref
py_xeq(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_EQ);
}

unique_pyref
py_xne(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_NE);
}

unique_pyref
py_xgt(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_GT);
}

unique_pyref
py_xge(pyref o1, pyref o2)
{
  return xrich_compare(o1, o2, Py_GE);
}

unique_pyref
make_pylong(long value)
{
  return adopt_check(PyLong_FromLong(value));
}

unique_pyref
make_pylong(unsigned long value)
{
  return adopt_check(PyLong_FromUnsignedLong(value));
}

unique_pyref
make_pylong(long long value)
{
  return adopt_check(PyLong_FromLongLong(value));
}

unique_pyref
make_pylong(unsigned long long value)
{
  return adopt_check(PyLong_FromUnsignedLongLong(value));
}

unique_pyref
make_pyfloat(double value)
{
  return adopt_check(PyFloat_FromDouble(value));
}

unique_pyref
pyiter_for(pyref obj)
{
  return adopt_check(PyObject_GetIter(obj.get()));
}

unique_pyref
pyiter_next(pyref iter)
{
  unique_pyref ret = adopt(PyIter_Next(iter.get()));
  if (!ret && pyerr_occurred())
    throw_current_pyerr();
  return ret;
}

int
HasDict::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->py_dict.get());
  return 0;
}

int
HasDict::py_clear() noexcept
{
  this->py_dict.reset();
  return 0;
}

SupportsWeakRefs::~SupportsWeakRefs()
{
  if (this->py_weakref_list)
    PyObject_ClearWeakRefs(pyref(this).as<PyObject>().get());
}

bool
pyerr_occurred() noexcept
{
  return PyErr_Occurred();
}

void
pywarn(obj_pyref<PyTypeObject> category /*nullable*/,
       const char* msg,
       Py_ssize_t stack_level)
{
  if (PyErr_WarnEx(pyref(category).get(), msg, stack_level))
    throw_current_pyerr();
}

pyref
py_wr_dereference(obj_pyref<PyWeakReference> wr) noexcept
{
  pyref ret = PyWeakref_GET_OBJECT(wr.get());
  if (ret == Py_None)
    ret = pyref();
  return ret;
}

}  // namespace dctv
