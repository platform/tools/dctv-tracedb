// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename T>
void
_object_dealloc_impl(PyObject* obj) noexcept
{
  if (std::is_base_of_v<SupportsGc, T>)
    PyObject_GC_UnTrack(obj);
  reinterpret_cast<T*>(obj)->~T();
  pytype(obj)->tp_free(obj);
}

template<typename T>
void
safe_clear(T* value)
{
  using std::swap;
  T dummy;
  swap(*value, dummy);
}

template<typename T>
unique_pyref
_object_repr_impl(PyObject* obj)
{
  return make_pystr(
      static_cast<String>(*pyref(obj).as_unsafe<T>()));
}

template<typename T>
unique_obj_pyref<T>
PythonConstructor<T>::make(PyTypeObject* type,
                           pyref args,
                           pyref kwargs)
{
  return unique_obj_pyref<T>::make(type, args, kwargs);
}

template<typename T, typename Functor>
constexpr
PyTypeObject
make_py_type(const char* name,
             const char* doc,
             Functor init_type_functor)
{
  // We can't support fancy C++ object layouts, so no vtables for
  // Python objects.  No, we can't just stick the vtable in a hidden
  // before-object memory area, since Python GC _already_ uses that
  // trick for its own tags, making it unavailable for us.
  static_assert(offsetof(T, ob_base.ob_refcnt)
                == offsetof(PyObject, ob_refcnt),
                "type must have same layout as PyObject "
                "and can't have a fancy C++ layout, e.g., a vtable");

  PyTypeObject t = {
    PyVarObject_HEAD_INIT(nullptr, 0)
  };
  t.tp_flags = Py_TPFLAGS_DEFAULT;
  t.tp_name = name;
  t.tp_doc = doc;
  t.tp_basicsize = sizeof (T);
  t.tp_dealloc = _object_dealloc_impl<T>;
  if constexpr (std::is_base_of_v<HasRepr, T>) {
    t.tp_repr = wraperr<_object_repr_impl<T>>();
  }
  if constexpr (std::is_base_of_v<SupportsGc, T>) {
    t.tp_flags |= Py_TPFLAGS_HAVE_GC;
    t.tp_alloc = tp_alloc_for_gc;
    t.tp_free = PyObject_GC_Del;
    t.tp_traverse = wraperr<&T::py_traverse>();
    if constexpr (std::is_base_of_v<SupportsGcClear, T>) {
      t.tp_clear = wraperr<&T::py_clear>();
    }
  }
  if constexpr (std::is_base_of_v<HasPyCtor, T>) {
    t.tp_new = wraperr<
        &PythonConstructor<T>::make,
        AutoErrorSentinel{},
        /*Munge1st=*/false>();
  }
  // TODO(dancol): classes could in theory shadow these fields, which
  // would make the offsetof() below wrong.
  if constexpr (std::is_base_of_v<SupportsWeakRefs, T>) {
    t.tp_weaklistoffset = offsetof(T, py_weakref_list);
  }
  if constexpr (std::is_base_of_v<HasDict, T>) {
    t.tp_dictoffset = offsetof(T, py_dict) +
        unique_pyref::get_pyval_offset();
  }
  init_type_functor(&t);
  return t;
}

template<typename T>
constexpr
PyTypeObject
make_py_type(const char* name, const char* doc)
{
  return make_py_type<T>(name, doc, [](PyTypeObject*){});
}

constexpr
PyMemberDef
make_memberdef(const char* name,
               int type,
               Py_ssize_t offset,
               int flags,
               const char* doc)
{
  return PyMemberDef{
    const_cast<char*>(name),
    type,
    offset,
    flags,
    const_cast<char*>(doc),
  };
}

constexpr
PyMethodDef
make_methoddef(const char* name,
               PyNoArgsFunction function,
               int flags,
               const char* doc)
{
  return { name, (PyCFunction)function, flags, doc };  // NOLINT
}

constexpr
PyMethodDef
make_methoddef(const char* name,
               PyCFunction function,
               int flags,
               const char* doc)
{
  return { name, function, flags, doc };
}

constexpr
PyMethodDef
make_methoddef(const char* name,
               PyCFunctionWithKeywords function,
               int flags,
               const char* doc)
{
  return { name, (PyCFunction)function, flags, doc };  // NOLINT
}

constexpr
PyGetSetDef
make_getset(const char* name,
            const char* doc,
            getter getter,
            setter setter,
            void* closure)
{
  return { const_cast<char*>(name),
           getter,
           setter,
           const_cast<char*>(doc),
           closure };
}

}  // namespace dctv
