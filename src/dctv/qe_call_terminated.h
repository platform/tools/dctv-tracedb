// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include "io_spec.h"
#include "pyutil.h"
#include "query.h"
#include "vector.h"

namespace dctv {

struct QeCallTerminated final : QeCall {
  explicit QeCallTerminated(Vector<IoSpec> terminal_io_specs);
  void setup(OperatorContext* oc) final;
  unique_pyref do_it(OperatorContext* oc) override;
  Score compute_score(const OperatorContext* oc) const override;
  int py_traverse(visitproc visit, void* arg) const noexcept override;
 private:
  Vector<IoSpec> terminal_io_specs;
};

inline void init_qe_call_terminated(pyref m) {}

}  // namespace dctv
