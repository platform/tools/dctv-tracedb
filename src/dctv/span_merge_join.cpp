// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "span_merge_join.h"

#include <tuple>

#include "autoevent.h"
#include "hash_table.h"
#include "npyiter.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "result_buffer.h"
#include "span.h"
#include "span_result_buffer.h"

namespace dctv {
namespace {

static
unique_pyref
span_merge_join(PyObject*, PyObject* py_args)
{
  namespace ae = auto_event;
  using SourceNumber = int;

  static_assert(std::is_signed_v<Index>);

  auto args = PARSEPYARGS_V(
      (pyref, sources)
      (pyref, span_cb)
      (QueryExecution*, qe)
  )(py_args);

  struct SourceState final {
    bool is_open() const {
      return this->index >= 0;
    }

    void open(Index index) {
      assert(!this->is_open());
      assume(index >= 0);
      this->index = index;
    }

    void close() {
      assume(this->is_open());
      this->index = -1;
    }

    Index get_index() const {
      return this->index;
    }

   private:
    Index index = -1;
  };

  struct Source final {
    Source(unique_pyref py_iter,
           unique_pyref py_sink,
           QueryExecution* qe,
           bool required,
           bool has_partition)
        : span_out(std::move(py_sink), qe),
          py_iter(std::move(py_iter)),
          has_partition(has_partition),
          required(required)
    {}

    ResultBuffer<Index> span_out;
    optional<SourceState> broadcast;
    unique_pyref py_iter;
    bool has_partition;
    TimeStamp last_ts = 0;
    bool required;
  };

  struct PartitionState final {
    explicit PartitionState(SourceNumber nsources) :
        source_state(nsources),
        source_state_at_last_change(nsources) {}
    Vector<SourceState> source_state;
    Vector<SourceState> source_state_at_last_change;
    TimeStamp last_change_ts = 0;
    bool changed = false;
  };

  using PartitionTable = HashTable<Partition, PartitionState>;
  using PartitionTableIterator = typename PartitionTable::iterator;
  PartitionTable partitions;
  Vector<Partition> changed_partitions;

  auto mk_source = [&](pyref spec) -> Source {
    // TODO(dancol): support masking
    PARSEPYARGS(
        (pyref, iter)
        (pyref, span_out_cb)
        (bool, required)
        (bool, has_partition)
    )(spec);
    return Source(iter.addref(),
                  span_out_cb.addref(),
                  args.qe,
                  required,
                  has_partition);
  };
  Vector<Source> sources = py2vec(args.sources, mk_source);
  if (sources.size() >=
      static_cast<size_t>(std::numeric_limits<int>::max()) / 4)
    throw_pyerr_msg(PyExc_OverflowError, "too many span sources");
  const SourceNumber nsources = static_cast<int>(sources.size());
  const bool using_partition = std::any_of(
      sources.begin(),
      sources.end(),
      [](const auto& source) { return source.has_partition; });

  SpanTableResultBuffer span_out(args.span_cb.addref(),
                                 args.qe,
                                 using_partition);

  if (using_partition) {
    bool any_source_is_broadcast = false;
    bool any_non_broadcast_is_required = false;
    for (Source& source : sources) {
      if (source.has_partition) {
        if (source.required)
          any_non_broadcast_is_required = true;
      } else {
        source.broadcast = SourceState();
        any_source_is_broadcast = true;
      }
    }
    if (any_source_is_broadcast && !any_non_broadcast_is_required) {
      // Our output is partitioned; i.e., each output row has a
      // partition field.  If a partitioned span isn't required for
      // each output span, what value do we use to fill in the
      // partition field of the output?  We may one day do something
      // sensible here, but we haven't figured out what that is yet,
      // so punt.
      throw_invalid_query_error("full outer broadcast join not supported");
    }
  }

  auto should_emit_partition = [&] (const PartitionState& state) {
    bool any_open = false;
    for (SourceNumber source_id = 0; source_id < nsources; ++source_id) {
      const Source& source = sources[source_id];
      const SourceState& ss = state.source_state_at_last_change[source_id];
      const bool source_is_open = ss.is_open();
      if (source.required && !source_is_open)
        return false;
      any_open = any_open || source_is_open;
    }
    return any_open;
  };

  auto maybe_emit_partition = [&]
      (Partition partition,
       const PartitionState& state,
       TimeStamp now)
  {
    if (should_emit_partition(state)) {
      assert(state.last_change_ts < now);
      TimeStamp ts = state.last_change_ts;
      TimeStamp duration = now - ts;
      span_out.add(ts, duration, partition);
      for (SourceNumber source_id = 0; source_id < nsources; ++source_id) {
        const SourceState& ss =
            state.source_state_at_last_change[source_id];
        sources[source_id].span_out.add(ss.get_index());
      }
    }
  };

  auto mark_partition_changed = [&] (PartitionTableIterator it) {
    PartitionState& state = it->second;
    if (!state.changed) {
      state.changed = true;
      changed_partitions.push_back(it->first);
    }
  };

  auto mark_all_partitions_changed = [&] {
    for (auto it = partitions.begin(); it != partitions.end(); ++it)
      mark_partition_changed(it);
  };

  auto get_current_source_state = [&](const PartitionState& state,
                                      SourceNumber source_id)
      -> const SourceState& {
    // TODO(dancol): we could be clever and store a forwarding pointer
    // in the state source_state array.
    return sources[source_id].broadcast
        ? *sources[source_id].broadcast
        : state.source_state[source_id];
  };

  auto snapshot_partition = [&](PartitionState& state, TimeStamp now) {
    state.last_change_ts = now;
    for (SourceNumber source_id = 0; source_id < nsources; ++source_id)
      state.source_state_at_last_change[source_id] =
          get_current_source_state(state, source_id);
  };

  auto process_changed_partitions = [&](TimeStamp now) {
    // We don't need to sort changed_partitions here: if we're
    // unpartitioned, we're emitting one output row anyway.  If we
    // _are_ partitioned, then higher-level code will insert a sorting
    // postprocessing step, and if we're sorting anyway, we might as
    // well sort by partition.
    for (Partition partition : changed_partitions) {
      auto partition_it = partitions.find(partition);
      assume(partition_it != partitions.end());
      PartitionState& state = partition_it->second;
      maybe_emit_partition(partition, state, now);
      snapshot_partition(state, now);
      state.changed = false;
    }
    changed_partitions.clear();
  };

  AUTOEVENT_DECLARE_EVENT(SpanBegin,
                          (TimeStamp, _duration, ae::span_duration)
                          (Partition, partition, ae::partition)
  );

  AUTOEVENT_DECLARE_EVENT(SpanEnd,
                          (Partition, partition, ae::partition)
                          (SourceNumber, source_number)
  );

  auto handle_span_begin = [&]
      (SourceNumber source_number,
       auto&& ae,
       TimeStamp ts,
       const SpanBegin& sb,
       Index index)
  {
    Source& source = sources[source_number];
    SourceState* ss;
    if (source.broadcast) {
      mark_all_partitions_changed();
      ss = &*source.broadcast;
    } else {
      auto [partition_it, _inserted] =
          partitions.try_emplace(sb.partition, nsources);
      mark_partition_changed(partition_it);
      ss = &partition_it->second.source_state[source_number];
    }
    ss->open(index);
    ae::enqueue_event(
        ae,
        ts + sb._duration,
        SpanEnd {sb.partition, source_number});
  };

  auto handle_span_end = [&]
      (auto&& ae, TimeStamp ts, const SpanEnd& se)
  {
    Source& source = sources[se.source_number];
    SourceState* ss;

    if (source.broadcast) {
      mark_all_partitions_changed();
      ss = &*source.broadcast;
    } else {
      auto partition_it = partitions.find(se.partition);
      assume(partition_it != partitions.end());
      mark_partition_changed(partition_it);
      ss = &partition_it->second.source_state[se.source_number];
    }
    ss->close();
  };

  ae::process(
      ae::ts_batch_end_hook([&](auto&& ae, TimeStamp ts) {
        process_changed_partitions(ts);
      }),
      ae::synthetic_event<SpanEnd>(handle_span_end),
      ae::multi_input<SpanBegin, SourceNumber>(
          [&]{
            Vector<pyref> py_iterators;
            py_iterators.reserve(nsources);
            for (Source& source : sources)
              py_iterators.push_back(source.py_iter);
            return py_iterators;
          }(),
          handle_span_begin,
          ae::add_index_argument)
  );

  assume(changed_partitions.empty());
  span_out.flush();
  for (Source& source : sources)
    source.span_out.flush();
  return addref(Py_None);
}

}  // anonymous namespace



static PyMethodDef functions[] = {
  make_methoddef("span_merge_join",
                 wraperr<span_merge_join>(),
                 METH_VARARGS,
                 "Join two time series"),
  { 0 }
};


void
init_span_merge_join(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
