# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Query planner"""
import logging
from collections import defaultdict
from itertools import count as xcount
from heapq import heappush, heappop
from cytoolz import valmap, first
from modernmp.util import the
from .util import (
  Timed,
  load_networkx,
)

from .query import (
  QueryAction,
  QueryNode,
)

log = logging.getLogger(__name__)

def _do_merge_inplace(nx, action_graph, merge_group):
  """Apply a single action merge to the graph"""
  merged_node = merge_group[0].merge(merge_group[1:])
  assert isinstance(merged_node, QueryAction)
  assert frozenset(merged_node.inputs).issubset(frozenset().union(
    *(frozenset(mg.inputs) for mg in merge_group))), \
    "merged node may not require additional inputs"
  # Add the merged node to the graph, wiring up dependencies.
  abo = build_local_actions_by_output(nx, action_graph, merge_group)
  action_graph.add_node(merged_node)
  # Merge node -> inputs
  for input_query in merged_node.inputs:
    add_action_graph_edge(action_graph,
                          merged_node,
                          abo[input_query],
                          input_query)
  # Consumers -> merge node
  for src, _dst, data in action_graph.in_edges(merge_group, data=True):
    # Should have no back-edges at this point, so we can access
    # data["queries"] without checking whether this attribute exists.
    for edge_query in data["queries"]:
      add_action_graph_edge(action_graph,
                            src,
                            merged_node,
                            edge_query)
  # Kill the original nodes, which we've merged into merged_node
  action_graph.remove_nodes_from(merge_group)

def _merge_actions_inplace(nx, action_graph):
  """Merge compatible actions in a query-planner dependency graph"""
  candidates = defaultdict(list)
  for action in action_graph:
    merge_key = action.merge_key
    if merge_key:
      candidates[merge_key].append(action)
  for merge_group in candidates.values():
    if len(merge_group) < 2:
      continue
    _do_merge_inplace(nx, action_graph, merge_group)

def condense_cycles_inplace(nx, action_graph):
  """Transform recursive queries out of the graph"""
  def _find_entry_point(component):
    assert component
    if len(component) == 1:
      return first(component)
    entry_points = set()
    for src, dst in action_graph.in_edges(component):
      if src not in component:
        entry_points.add(dst)
    assert len(entry_points) <= 1, "loop is not reducible"
    assert entry_points, "group is not needed?!"
    return entry_points.pop()
  components = tuple(
    nx.strongly_connected_components_recursive(action_graph))
  for component in components:
    entry_point = _find_entry_point(component)
    assert entry_point in component
    assert entry_point.condense or len(component) == 1, \
      ("component not a condensation root but is an "
       "entry point to a recursive cycle! {!r}".format(entry_point))
    if entry_point.condense:
      entry_point.condense(nx, action_graph, component)
      assert not any(action in action_graph for action in component)

def _scan_action_graph_for_redundant_computation(action_graph):
  all_outputs = set()
  for action in action_graph:
    for output in action.outputs:
      if output in all_outputs:
        log.warning("more than one action produces %s: ignoring one", output)
      else:
        all_outputs.add(output)

def _make_initial_action_graph(nx, seed_actions, cached):
  # TODO(dancol): FIXME FIXME FIXME FIXME RESTORE CACHE SUPPORT
  assert cached is not None
  action_graph = nx.DiGraph()
  actions_by_output = {}
  actions_being_added = []
  def _produce_query(query):
    action = actions_by_output.get(query)
    if action:
      return action
    if query.special_add_action:
      return query.special_add_action(action_graph, actions_being_added)
    action = _add_action(query.make_action())
    assert actions_by_output[query] is action
    assert query in action.outputs, \
      "query {!r} not found in outputs {!r}".format(
        query, action.outputs)
    return action
  def _add_action(action):
    actions_being_added.append(action)
    action_graph.add_node(the(QueryAction, action))
    outputs = action.outputs
    for output_query in outputs:
      if output_query in actions_by_output:
        # More than one action produces this query; ignore that
        # here.  If we're running with debug checks enabled and we
        # don't end up merging, we'll warn later.
        pass
      else:
        actions_by_output[output_query] = action
    for input_query in action.inputs:
      producing_action = _produce_query(input_query)
      add_action_graph_edge(
        action_graph, action, producing_action, input_query)
    actions_being_added.pop()
    return action
  for action in seed_actions:
    _add_action(action)
  return action_graph

def build_local_actions_by_output(_nx, action_graph, nodes):
  """Figure out where we get our dependencies.

  Return a mapping from a dependency of NODES to the action in
  ACTION_GRAPH that produces that dependency.
  """
  actions_by_output = {}
  def _update_with_node(node):
    for dst, edge in action_graph[node].items():
      assert isinstance(dst, QueryAction)
      for query in edge["queries"]:
        stored_action = actions_by_output.get(query)
        assert stored_action is dst or stored_action is None
        if not stored_action:
          actions_by_output[query] = dst
  for node in nodes:
    _update_with_node(node)
  return actions_by_output

def delete_unreachable(nx, g, roots):
  """In graph G, delete all nodes not reachable from ROOTS

  ROOTS is a set (frozen or thawed) of nodes in G.
  """
  assert isinstance(roots, (set, frozenset))
  reachable = set()
  for src, dsts in nx.all_pairs_shortest_path_length(g):
    if src in roots:
      reachable.update(dsts)
  g.remove_nodes_from(set(g) - reachable)

def add_action_graph_edge(action_graph, src, dst, query):
  """Add an edge to action graph ACTION_GRAPH.

  QUERY is a QueryNode. SRC is the QueryAction that depends on it.
  DST is the QueryAction that provides it.
  """
  assert isinstance(src, QueryAction)
  assert isinstance(dst, QueryAction)
  assert isinstance(query, QueryNode)
  assert query in src.inputs
  assert query in dst.outputs
  action_graph.add_edge(src, dst)
  edge = action_graph[src][dst]
  edge_queries = edge.get("queries")
  if not edge_queries:
    edge["queries"] = edge_queries = set()
  edge_queries.add(query)

def assert_action_graph_valid(action_graph):
  """Check that ACTION_GRAPH is valid"""

  nx = load_networkx()
  assert nx.is_directed_acyclic_graph(action_graph), \
    ("cycle found in action graph: {}".
     format(nx.find_cycle(action_graph, orientation="original")))
  global_abo = {}
  def _check_node(action):
    assert isinstance(action, QueryAction)
    local_abo = {}
    for dst, edge in action_graph[action].items():
      assert isinstance(dst, QueryAction)
      edge_queries = edge.get("queries", ())
      assert len(edge_queries) == len(set(edge_queries))
      for query in edge_queries:
        assert query not in local_abo, \
          "query produced by two actions: #1={} #2={}".format(
            local_abo[query],
            dst)
        assert query in dst.outputs
        local_abo[query] = dst
    assert set(local_abo) == action.inputs, \
      "bad input actions for {}: missing={} excess={}".format(
        action,
        sorted(action.inputs - set(local_abo), key=str),
        sorted(set(local_abo) - action.inputs), key=str)
    for query, producing_action in local_abo.items():
      noted_action = global_abo.get(query)
      assert noted_action is producing_action or noted_action is None
      if not noted_action:
        global_abo[query] = producing_action
  for action in action_graph:
    _check_node(action)
  return True

def format_action_graph(action_graph):
  """Return a string with a human-readable representation of ACTION_GRAPH"""

  lines = []
  action_by_number = dict(enumerate(action_graph))
  number_by_action = {action: number for number, action
                      in action_by_number.items()}
  assert len(action_by_number) == len(number_by_action)
  for number in sorted(action_by_number):
    action = action_by_number[number]
    lines.append("#{}= {}".format(number, action))
  for number in sorted(action_by_number):
    lines.append("#{}:".format(number))
    action = action_by_number[number]
    depends_on_numbers = [
      number_by_action[depends_on]
      for depends_on in action_graph[action]
    ]
    for depends_on_number in sorted(depends_on_numbers):
      depends_on = action_by_number[depends_on_number]
      edge = action_graph.adj[action][depends_on]
      lines.append("  depends on {} ({})".format(
        number_by_action[depends_on],
        "forward" if "queries" in edge else "back"))
  return "\n".join(lines)

def make_action_graph(seed_actions, cached):
  """Make a networkx DAG of QueryAction instances.

  SEED_ACTIONS is a sequence of QueryActions from which to seed the
  generated graph.  The graph is made up of the actions in
  SEED_ACTIONS and any actions needed to produce their inputs.

  CACHED is a set giving the set of queries known to be in cache at
  the time of query planning.
  """
  # The action graph directed graph of QueryAction objects.  An edge
  # from A to B means that A depends on something B produces.  It may
  # initially contain cycles, but we condense all the cycles before
  # returning to the caller.
  nx = load_networkx()
  action_graph = _make_initial_action_graph(nx, seed_actions, cached)
  _merge_actions_inplace(nx, action_graph)
  condense_cycles_inplace(nx, action_graph)
  assert assert_action_graph_valid(action_graph)
  if __debug__:
    _scan_action_graph_for_redundant_computation(action_graph)
  return action_graph

def _compute_dag_needed(action_graph, completed, ba_inputs):
  return frozenset().union(*(ba_inputs[a] for a in action_graph
                             if a not in completed))

def _compute_production_distances(action_graph, wanted_queries, ba_outputs):
  nx = load_networkx()
  distances = defaultdict(list)
  for producing_action in (a for a in action_graph
                           if ba_outputs[a] & wanted_queries):
    for target, distance in nx.shortest_path_length(
        action_graph, producing_action).items():
      distances[target].append(distance)
  return valmap(min, distances)

def _tmb_cost(schedule, inputs, outputs, max_query_nr):
  cost = 0
  last_consumer_idx = [len(schedule)] * max_query_nr
  for idx, action in enumerate(schedule):
    for input_ in inputs[action]:
      last_consumer_idx[input_] = idx
  for idx, action in enumerate(schedule):
    for output in outputs[action]:
      consumer_idx = last_consumer_idx[output]
      cost += consumer_idx - idx
  return cost

def plan_query_beam_search(action_graph, wanted_queries, cached):
  """Analyze a query and figure out how to execute it.

  ACTION_GRAPH is a networkx action DAG of QueryActions, as produced
  by make_action_graph.

  WANTED_QUERIES is the set of queries that the caller has
  explicitly requested.

  CACHED is a set of QueryNode objects known to be in cache at the
  time of query planning.

  Return a list of queries to evaluate.
  """

  # This function effectively implements a breadth-first beam search
  # through the DAG of toposorts of action_graph. It's essentially
  # Dijkstra's algorithm over the implied DAG of toposorts of
  # action_graph, except that we truncate the search queue (acting like
  # the Prolog cut operator) when we detect that we've produced the
  # result of one of the queries of interest. In this way, we perform
  # a hybrid search that's greedy between query results but optimal
  # between them. We also truncate the queue when it grows insanely
  # large (which makes this algorithm a beam search): this way, we
  # guarantee forward progress. This truncation doesn't affect the
  # ability of the algorithm to actually complete the search, since we
  # can complete it from any state.

  deliver_threshold_gap = 0
  force_cut = True
  max_ql = 30
  debug_log = False

  assert isinstance(cached, frozenset)

  assert deliver_threshold_gap >= 0

  def _cache_inputs_outputs(action_graph):
    ba_inputs = {}
    ba_outputs = {}
    for action in action_graph:
      ba_inputs[action] = action.inputs
      ba_outputs[action] = action.outputs
    return ba_inputs, ba_outputs

  with Timed("cache action inputs, outputs"):
    ba_inputs, ba_outputs = _cache_inputs_outputs(action_graph)
  with Timed("production distances"):
    production_distances = _compute_production_distances(
      action_graph, wanted_queries, ba_outputs)

  best = {}
  deliver_threshold = 0
  # Breaks ties in heapq so that the priority queue code never tries
  # to compare the set objects at the end of the state-tuple.
  seqno = xcount()

  queue = []
  def _enqueue_1(state):
    heappush(queue, state)
    if len(queue) > max_ql * 2:
      old_queue_len = len(queue)
      def _pred(state):
        return not _state_obsolete_p(state)
      queue[:] = list(filter(_pred, queue))
      if old_queue_len > len(queue):
        debug_log and log.debug(
          "pruning queue: killed %d items below deliver_threshold",
          old_queue_len - len(queue))
      def _sort_key(state):
        return production_distances.get(state[-4], 100)
      queue.sort(key=_sort_key)
      del queue[max_ql:]

  def _dequeue():
    return heappop(queue)

  def _enqueue(state):
    debug_log and log.debug("enqueue %r", _state_pp(state))
    _enqueue_1(state)

  def _state_pp(state):
    pp = list(state)
    pp[-4] = type(pp[-4]).__name__
    return tuple(pp[:-2])

  def _state_memory_cost(state):
    return state[:2]

  def _enqueue_unless_suboptimal(state):
    nonlocal deliver_threshold
    state_key = state[-1]
    if _state_obsolete_p(state) and queue:
      debug_log and log.debug("dropping enqueue: below deliver_threshold")
      return
    delivered = state[-3]
    if delivered > deliver_threshold:
      deliver_threshold = delivered
    best_so_far = best.get(state_key)
    if not best_so_far or state < best_so_far:
      best[state_key] = state
      _enqueue(state)

  def _plan_state_initial():
    max_memory_use = 0
    live_memory_use = 0
    parent = None
    completed = frozenset()
    delivered = 0
    action = None
    return (
      max_memory_use, # 0
      live_memory_use, # 1
      next(seqno), # 2
      # Add fields here
      action, # -4
      delivered, # -3
      parent, # -2
      completed, # -1
    )

  def _state_execute(parent, parent_produced, action):
    parent_max_memory_use, parent_len_live = parent[0], parent[1]
    parent_completed = parent[-1]
    assert action not in parent_completed
    action_set = {action}
    completed = parent_completed | action_set
    assert len(completed) == len(parent_completed) + 1
    produced = parent_produced | ba_outputs[action]
    delivered = len(produced & wanted_queries)
    needed = _compute_dag_needed(action_graph, completed, ba_inputs)
    live = produced & needed
    result = (
      max(parent_max_memory_use, len(live)), # 0
      parent_len_live + len(live), # 1
      next(seqno), # 2
      action, # -4
      delivered, # -3
      parent, # -2
      completed, # -1
    )
    return result

  def _state_obsolete_p(state):
    delivered = state[-3]
    return deliver_threshold - delivered > deliver_threshold_gap

  def _state_path(state):
    """Return tuple of actions leading to this state"""
    actions = []
    parent, parent_completed = state[-2:]
    while parent:
      completed = parent_completed
      parent, parent_completed = parent[-2:]
      assert len(completed - parent_completed) == 1
      actions.append(first(completed - parent_completed))
    actions.reverse()
    return tuple(actions)

  def _search_iteration(state):
    # pylint: disable=len-as-condition
    state_completed = state[-1]
    if len(state_completed) == len(action_graph):
      return True  # Found complete path; break loop
    if state < best[state_completed] and queue:
      debug_log and log.debug("skipping: obsolete state")
      return False  # Obsolete state; continue loop
    if _state_obsolete_p(state) and queue:
      debug_log and log.debug("skipping: below deliver_threshold")
      return False  # Old generation; continue loop

    debug_log and log.debug("ql=%3d s=%r", len(queue), _state_pp(state))

    state_produced = cached.union(
      *(ba_outputs[a] for a in state_completed))
    def _action_ready_p(action):
      return (action not in state_completed and
              ba_inputs[action].issubset(state_produced))
    next_states = [_state_execute(state, state_produced, action)
                   for action
                   in filter(_action_ready_p, action_graph)]
    assert next_states, (
      "how can there be no next? completed={!r} dag={!r}".format(
        list(state_completed),
        list(action_graph),
      ))

    if force_cut:
      best_next_state_cost = min(map(_state_memory_cost, next_states))
      if best_next_state_cost <= _state_memory_cost(state):
        debug_log and log.debug("force cut")
        _enqueue_unless_suboptimal(min(next_states))
        return False  # Continue loop
    for next_state in next_states:
      _enqueue_unless_suboptimal(next_state)
    return False  # Continue loop

  def _init():
    initial = _plan_state_initial()
    _enqueue(initial)
    best[initial[-1]] = initial

  _init()
  while True:
    state = _dequeue()
    if _search_iteration(state):
      break
  debug_log and log.debug("DONE bs=%d ql=%3d s=%r",
                          len(best), len(queue), _state_pp(state))
  return _state_path(state)

def plan_query_minstar(dag):
  """Analyze a query and figure out how to execute it.

  This function uses the heuristic approach described in Arian Bar's
  2015 paper "Cache-Oblivious Scheduling of Shared Workloads".
  The main advantage of this routine over plan_query_beam_search is
  replacing the multiple parallel search heads each guided by distance
  _to_ a "wanted" QueryNode with a single search head guided by
  distance _from_ the search root.

  It seems better, aesthetically, for query optimization not to depend
  on which of the executed queries is in the wanted set, since such a
  scheme works less well for large networks of query operators behind
  a single "wanted" search.
  """
  nx = load_networkx()
  dag = nx.classes.graphviews.reverse_view(dag)

  # This routine works in terms of integers representing QueryAction
  # and QueryNode instances; this way, we can represent sets as
  # integers and perform set operations via bit-bashing instead of
  # using set.  Using integers here gives us a major performance win.

  # TODO(dancol): move this code to C++

  def _setup():
    needed = frozenset() if not dag else \
      frozenset.union(*(action.inputs for action in dag))
    next_node_nr = 0
    node_to_nr = {}
    nr_to_node = {}
    for v in dag:
      node_to_nr[v] = next_node_nr
      nr_to_node[next_node_nr] = v
      next_node_nr += 1
    max_node_nr = next_node_nr
    def _intern_node(v):
      return node_to_nr[v]
    next_query_nr = 0
    query_to_nr = {}
    def _intern_query(q):
      nonlocal next_query_nr
      nr = query_to_nr.get(q, -1)
      if nr < 0:
        query_to_nr[q] = nr = next_query_nr
        next_query_nr += 1
      return nr
    node_data_template = [None] * max_node_nr
    depth = node_data_template[:]
    prednode = node_data_template[:]
    inputs = node_data_template[:]
    needed_outputs = node_data_template[:]
    succnode = node_data_template

    def _node_bitmap(nodes):
      bitmap = 0
      for node in nodes:
        bitmap |= 1 << node_to_nr[node]
      return bitmap

    for v in nx.topological_sort(dag):
      nr = node_to_nr[v]
      incoming = [_intern_node(node) for node in dag.predecessors(v)]
      prednode[nr] = _node_bitmap(dag.predecessors(v))
      succnode[nr] = tuple(map(_intern_node, dag.successors(v)))
      inputs[nr] = tuple(map(_intern_query, v.inputs))
      needed_outputs[nr] = tuple(map(_intern_query, needed & v.outputs))
      if incoming:
        depth[nr] = 1 + max(depth[nr_u] for nr_u in incoming)
      else:
        depth[nr] = 0
    depth = [-depth for depth in depth]
    max_query_nr = next_query_nr
    return max_query_nr, prednode, depth, succnode, inputs, \
      needed_outputs, nr_to_node

  (max_query_nr, prednode, depth, succnode,
   inputs, needed_outputs, nr_to_node) = _setup()
  max_node_nr = len(prednode)

  def _nrlist_to_bitmap(nrlist):
    bitmap = 0
    for nr in nrlist:
      bitmap |= 1<<nr
    return bitmap

  def _find_schedulable(schedule):
    schedule_bitmap = _nrlist_to_bitmap(schedule)
    schedulable = []
    for nr in range(max_node_nr):
      if nr in schedule:
        continue
      nr_prednode = prednode[nr]
      if not nr_prednode & schedule_bitmap == nr_prednode:
        continue
      schedulable.append(nr)
    return schedulable

  def _plan_deepest(partial_schedule,
                    partial_schedule_bitmap,
                    depth_heap,
                    id=id,    # pylint: disable=redefined-builtin
                    heappush=heappush,  # pylint: disable=redefined-outer-name
                    heappop=heappop):  # pylint: disable=redefined-outer-name
    # This function is called _very_ frequently, since it's used as a
    # lookahead heuristic below, and it's a performance hot spot.
    depth_heap = depth_heap[:]
    last = partial_schedule[-1]
    for node in succnode[last]:
      node_pred = prednode[node]
      if node_pred & partial_schedule_bitmap == node_pred:
        heappush(depth_heap, (depth[node], id(node), node))
    n = 0
    while depth_heap and n < 10:  # Limit lookahead
      _, _, step = heappop(depth_heap)
      if step is last:
        continue
      partial_schedule.append(step)
      partial_schedule_bitmap |= 1<<step
      for node in succnode[step]:
        node_pred = prednode[node]
        if node_pred & partial_schedule_bitmap == node_pred:
          heappush(depth_heap, (depth[node], id(node), node))
      n += 1
    return partial_schedule

  def _plan_heuristic():
    partial_schedule = []
    partial_schedule_bitmap = 0
    schedulable = _find_schedulable(partial_schedule)
    def _score(node):
      return _tmb_cost(
        _plan_deepest(partial_schedule + [node],
                      partial_schedule_bitmap | 1<<node,
                      depth_heap),
        inputs,
        needed_outputs,
        max_query_nr)
    while schedulable:
      depth_heap = []
      for node in schedulable:
        heappush(depth_heap, (depth[node], id(node), node))
      step = min(schedulable, key=_score)
      schedulable.remove(step)
      partial_schedule.append(step)
      partial_schedule_bitmap |= 1<<step
      for node in succnode[step]:
        node_pred = prednode[node]
        if node_pred & partial_schedule_bitmap == node_pred:
          schedulable.append(node)
    return partial_schedule

  plan = _plan_heuristic()
  assert len(plan) == len(dag)
  assert _tmb_cost(plan, inputs, needed_outputs, max_query_nr) >= 0
  return [nr_to_node[nr] for nr in plan]
