// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

// Utilities useful for defining objects.  The facilities for just
// using objects lives in pyutil.h.

#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

#include "pyerr.h"
#include "pyutil.h"

namespace dctv {

// Supports clear implementations: makes clearing atomic through move.
template<typename T>
void safe_clear(T* value);

template<typename T>
unique_pyref _object_new_impl(PyTypeObject* type,
                              PyObject* args,
                              PyObject* kwargs);

template<typename T>
void _object_dealloc_impl(PyObject* obj) noexcept;

// Like PyType_GenericAlloc in the GC case, but delay wiring up the GC
// until we run the C++ constructor.
PyObject* tp_alloc_for_gc(PyTypeObject* type, Py_ssize_t n) noexcept;

// Convenient function to use for methods that just return self.
PyObject* return_self(PyObject* obj) noexcept;

// Specialize to customize how we implement object construction.
// By default, invokes the args, kwargs constructor.
template<typename T>
struct PythonConstructor {
  static inline unique_obj_pyref<T> make(PyTypeObject* type,
                                         pyref args,
                                         pyref kwargs);
};

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !! WARNING! ACHTHUNG! READ THIS COMMENT!  !!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// make_py_type creates a Python type that, on initialization, will
// run a C++ class constructor and upon deinitialization, run the C++
// class destructor.  To a first approximation, this approach works
// fine, but think about what happens when a constructor creates a
// Python reference to the object under construction and then fails
// (via throwing) leaving a reference the object whose construction
// failed outlive that object.  (A common source of these references
// comes from exception object traceback frames.)
//
// Rule: C++ constructor that throws must not leak references to the
// object under construction.  Most constructors don't do that.
// Still, you may want to leave complex object initialization to a
// static wrapper function.

template<typename T, typename Functor>
constexpr PyTypeObject make_py_type(const char* name,
                                    const char* doc,
                                    Functor init_type_functor);

template<typename T>
constexpr PyTypeObject make_py_type(const char* name,
                                    const char* doc);

// Use these functions to more safely define members.

constexpr PyMemberDef make_memberdef(const char* name,
                                     int type,
                                     Py_ssize_t offset,
                                     int flags,
                                     const char* doc);

constexpr PyMethodDef make_methoddef(const char* name,
                                     PyNoArgsFunction function,
                                     int flags,
                                     const char* doc);

constexpr PyMethodDef make_methoddef(const char* name,
                                     PyCFunction function,
                                     int flags,
                                     const char* doc);

constexpr PyMethodDef make_methoddef(const char* name,
                                     PyCFunctionWithKeywords function,
                                     int flags,
                                     const char* doc);

constexpr PyGetSetDef make_getset(const char* name,
                                  const char* doc,
                                  getter getter,
                                  setter setter = nullptr,
                                  void* closure = nullptr);


inline void init_pyobj(pyref m) {};

}  // namespace dctv

#include "pyobj-inl.h"
