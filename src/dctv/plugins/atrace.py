# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Routines for parsing atrace events"""
from ..event_parser import EventSchemaBuilder as esb

def dctv_plugin_get_schemas():
  """Return a tuple of all schemas that we define"""
  return (
    (esb("E_atrace",
         r"E\|[0-9]+")
     .user_event()
     .build()),
    (esb("dctv_snapshot_start",
         r"dctv_snapshot_start (?P<snapshot_id>[0-9]+)")
     .column("snapshot_id", "i", domain="snapshot_number")
     .user_event()
     .build()),
    (esb("dctv_snapshot_end",
         r"dctv_snapshot_end (?P<snapshot_id>[0-9]+)")
     .column("snapshot_id", "i", domain="snapshot_number")
     .user_event()
     .build())
  )
