# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Linux memory events"""
from ..event_parser import (
  EventSchemaBuilder as esb,
  RAW_TID_TYPE,
)

def dctv_plugin_get_schemas():
  """Return all schemas that we define"""
  return (
    (esb("rss_stat",
         (r"member=(?P<member>[0-9]+) "
          # Why can size be negative? Who knows? Why is anything?
          r"size=(?P<size>-?[0-9]+)"))
     .column("member", "b")
     .column("size", "l", unit="bytes")
     .build()),
    (esb("oom_score_adj_update",
         (r"pid=(?P<target_tid>[0-9]+) "
          r"comm=(?P<target_comm>[^\n]*) "
          r"oom_score_adj=(?P<oom_score_adj>-?[0-9]+)"))
     .column("target_tid", RAW_TID_TYPE)
     .column("target_comm", "ss")
     # TODO(dancol): define custom unit for oom_score_adj?
     .column("oom_score_adj", "i")
     .build()),
    (esb("ion_heap_shrink",
         (r"heap_name=(?P<heap>[^,]*), "
          r"len=(?P<len>[0-9]+), "
          r"total_allocated=(?P<total_allocated>[0-9]+)"))
     .column("heap", "s")
     .column("len", "l", unit="bytes")
     .column("total_allocated", "l", unit="bytes")
     .build()),
    (esb("ion_heap_grow",
         (r"heap_name=(?P<heap>[^,]*), "
          r"len=(?P<len>[0-9]+), "
          r"total_allocated=(?P<total_allocated>[0-9]+)"))
     .column("heap", "s")
     .column("len", "l", unit="bytes")
     .column("total_allocated", "l", unit="bytes")
     .build()),
    (esb("ion_buffer_create",
         r"addr=(?P<addr>[0-9a-fA-F]+), "
         r"len=(?P<len>[0-9]+)")
     .column("addr", "lx")
     .column("len", "l", unit="bytes")
     .build()),
    (esb("ion_buffer_destroy",
         r"addr=(?P<addr>[0-9a-fA-F]+), "
         r"len=(?P<len>[0-9]+)")
     .column("addr", "lx")
     .column("len", "l", unit="bytes")
     .build()),
  )

# TODO(dancol): unhork memory analysis, port to stdlib.sql.

# def dctv_plugin_prepare_trace(augment_trace):
#   """Decorate a newly-mounted trace file with useful facilities"""

#   rss_pages_template = """
#   SELECT SPAN value AS %s
#   FROM dctv.time_series_to_spans(
#     sources=>[
#       {
#         source=>(SELECT EVENT * FROM cooked_events.rss_stat
#                 WHERE member=%s AND size >= 0bytes),
#         `partition`=>'tid',
#         nickname=>'rss_stat',
#       },
#       {
#         source=>scheduler.exits_p_pid,
#         role=>'stop',
#       },
#       {
#         source=>last_ts,
#         role=>'stop_broadcast',
#       },
#     ],
#     columns=>[
#       {
#         column=>'value',
#         source_column=>'size',
#         source=>'rss_stat',
#       },
#     ],
#     `partition`=>'pid',
#   )
#   """

#   augment_trace.table(
#     ["memory", "rss_stat_file_pages_p_pid"],
#     rss_pages_template % ("file", 0))
#   augment_trace.table(
#     ["memory", "rss_stat_anon_pages_p_pid"],
#     rss_pages_template % ("anon", 1))

#   # augment_trace.table(
#   #   ["memory", "oom_score_adj_p_pid"],
#   #   """
#   #   SELECT SPAN *
#   #   FROM dctv.time_series_to_spans(
#   #     sources=>[
#   #       {
#   #         source=>raw_events.oom_score_adj_update,
#   #         `partition`=>'tid2',
#   #         nickname=>'oom_adj',
#   #       },
#   #       {
#   #         source=>scheduler.exits_p_pid,
#   #         role=>'stop',
#   #       },
#   #       {
#   #         source=>last_ts,
#   #         role=>'stop_broadcast',
#   #       },
#   #   ],
#   #     columns=>[
#   #        {column=>'oom_score_adj', source=>'oom_adj'},
#   #     ],
#   #     `partition`=>'pid',
#   #   )
#   #   """)

#   augment_trace.table(
#     ["memory", "ion_heaps"],
#     """
#     (SELECT _ts, heap, (total_allocated + len) as size, len as length FROM raw_events.ion_heap_grow)
#     UNION ALL
#     (SELECT _ts, heap, (total_allocated - len) as size, -len as length FROM raw_events.ion_heap_shrink)
#     ORDER BY _ts
#     """)

#   augment_trace.table(
#     ["memory", "ion_heaps_p_heap"],
#     """
#     SELECT SPAN * FROM dctv.time_series_to_spans(
#       `partition`=>'heap',
#       sources=>[{
#         source=>(SELECT * FROM memory.ion_heaps),
#         `partition`=>'heap',
#        }],
#        columns=>[
#          {column=>'size', source_column=>'size'},
#        ])
#       """)
