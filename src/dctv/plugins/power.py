# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Linux power events"""
from ..event_parser import EventSchemaBuilder as esb

def _make_memlat_dev_meas_schema():
  # kworker/u16:18-2878  [003] ....  1317.316776: memlat_dev_meas: dev: soc:qcom,memlat-cpu0, id=0, inst=89588, mem=762, freq=36, stall=100, ratio=117
  return (esb("memlat_dev_meas",
              (r"dev: (?P<dev>[^ ]+), "
               r"id=(?P<id>[0-9]+), "
               r"inst=(?P<inst>[0-9]+), "
               r"mem=(?P<mem>[0-9]+), "
               r"freq=(?P<freq>[0-9]+), "
               r"stall=(?P<stall>[0-9]+), "
               r"ratio=(?P<ratio>[0-9]+)"))
          .column("dev", "s")
          .column("id", "i")
          .column("inst", "l")
          .column("mem", "l")
          .column("freq", "l")
          .column("stall", "l")
          .column("ratio", "i")
          .build())

def _make_memlat_dev_update_schema():
  # memlat_dev_update: dev: soc:qcom,memlat-cpu0, id=2, inst=445876, mem=37752, freq=368, vote=1720
  return (esb("memlat_dev_update",
              (r"dev: (?P<dev>[^ ]+), "
               r"id=(?P<id>[0-9]+), "
               r"inst=(?P<inst>[0-9]+), "
               r"mem=(?P<mem>[0-9]+), "
               r"freq=(?P<freq>[0-9]+), "
               r"vote=(?P<vote>[0-9]+)"))
          .column("dev", "s")
          .column("id", "i")
          .column("inst", "l")
          .column("mem", "l")
          .column("freq", "l")
          .column("vote", "l")
          .build())

def dctv_plugin_get_schemas():
  """Return all the schemas we define"""
  return (
    _make_memlat_dev_meas_schema(),
    _make_memlat_dev_update_schema(),
  )

# def dctv_plugin_prepare_trace(augment_trace):
#   """Decorate a newly-mounted trace file with useful facilities"""
#   augment_trace.table(
#     ["power", "memlat_p_device"],
#     """\
#     SELECT SPAN * FROM dctv.time_series_to_spans(
#     `partition`=>'device_id',
#       sources=>[{
#         source=>raw_events.memlat_dev_update,
#         `partition`=>'id',
#        }],
#        columns=>[
#          {column=>'frequency', source_column=>'vote'}
#        ])
#     """)
