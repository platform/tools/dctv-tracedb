# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Plugins package.

This package contains modules that each teach the DCTV core how to do
something.  Plugins can contribute event schema definitions, facet
definitions, and other crud.
"""

import logging
from importlib import import_module
from ..util import once

log = logging.getLogger(__name__)

PLUGIN_EXPORTS = (
  "dctv_plugin_get_schemas",
  "dctv_plugin_prepare_trace"
)

@once()
def get_plugins():
  """Load and return all plugins

  Return a tuple of plugin module objects."""

  from pkgutil import iter_modules
  plugins = []
  for _finder, name, _ispkg in iter_modules(__path__, "."):
    module = import_module(name, __package__)
    for export in dir(module):
      if export.startswith("dctv_") and export not in PLUGIN_EXPORTS:
        raise ImportError(
          "module {!r} defines a reserved 'dctv_'-prefixed symbol {!r}"
          .format(module.__name__, export))
    plugins.append(module)
  return tuple(plugins)

@once()
def get_all_event_schemas():
  """Gather event schemas from plugins"""
  schemas = []
  for plugin in get_plugins():
    dctv_plugin_get_schemas = \
      getattr(plugin, "dctv_plugin_get_schemas", None)
    if not dctv_plugin_get_schemas:
      continue
    schemas.extend(dctv_plugin_get_schemas())
  return tuple(schemas)

def prepare_trace(trace_augmentation):
  """Give plugins an opportunity to prepare a new trace

  Plugins can do things like add well-known tables and functions.
  """
  for plugin in get_plugins():
    prepare_trace_fn = getattr(plugin, "dctv_plugin_prepare_trace", None)
    if prepare_trace_fn:
      prepare_trace_fn(trace_augmentation)
