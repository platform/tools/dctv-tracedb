# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for autocompletion"""

# pylint: disable=missing-docstring,compare-to-empty-string

import logging
import pytest
from .autocomplete import (
  InvalidAutoCompleteContextError,
  DctvSqlAutoCompleter,
)

from .test_query import (
  TestQueryTable,
)

from .sql import (
  Namespace,
  add_sql_documentation,
)

log = logging.getLogger(__name__)

def _ac(sql, ns=None, **kwargs):
  """Make an autocompletion object.

  SQL is a string.  It must contain a single caret ('^') character.
  The caret represents the position of the point; it is removed from
  the string given to the autocompleter.
  """
  point = sql.index("^")
  munged_sql = sql[:point] + sql[point + 1:]
  assert "^" not in munged_sql
  return DctvSqlAutoCompleter(
    munged_sql, point, ns or Namespace(), **kwargs)

def _kw_c(sql):
  """Return a list of keyword completions.

  SQL is a string suitable for _ac()
  """
  return list(_ac(sql).complete_keywords())

def _id_c(sql, ns=None, **kwargs):
  """Return a list of identifier completions.

  SQL is a string suitable for _ac()
  """
  return list(_ac(sql, ns, **kwargs).complete_identifiers())

def _all_c(sql):
  """Return a list of all completions.

  SQL is a string suitable for _ac()
  """
  return list(_ac(sql).complete())

def test_token_detection():
  assert _ac("^").completion_token == ""
  assert _ac("SELE^ FOO").completion_token == "SELE"
  assert _ac("SELE^CT FOO").completion_token == "SELE"
  assert _ac("SELECT^ FOO").completion_token == "SELECT"
  assert _ac("SELECT ^ FOO").completion_token == ""
  assert _ac("SELECT 'foo'^").completion_token == ""
  assert _ac("SELECT `foo`^").completion_token == ""
  assert _ac("SELECT ^").completion_token == ""
  assert _ac("SELECT a FROM foo.a^").completion_token == "a"
  assert _ac("SELECT a FROM foo.^").completion_token == ""
  assert _ac("SELECT a FROM bar==^").completion_token == ""

def test_token_detection_invalid_middle_of_string():
  with pytest.raises(InvalidAutoCompleteContextError):
    _ac("SELECT 'fo^o'").completion_token

def test_token_detection_invalid_unterminated_string():
  with pytest.raises(InvalidAutoCompleteContextError):
    _ac("SELECT 'fo^").completion_token
  with pytest.raises(InvalidAutoCompleteContextError):
    _ac("SELECT `fo^").completion_token
  with pytest.raises(InvalidAutoCompleteContextError):
    _ac("SELECT \"fo^").completion_token

def test_keyword_simple():
  assert _kw_c("SELE^") == [("SELECT", None)]

def test_keyword_eof():
  assert _kw_c("SELECT x F^") == [("FROM", None)]

def test_keyword_exclude_non_reserved():
  # We *don't* want nonsense like "WITH MOUNT" just because we can
  # parse MOUNT as an identifier.  If "MOUNT" is legal in identifier
  # context, identifier completion will pick it up.
  assert _kw_c("WITH ^") == [("RECURSIVE", None)]

_TEST_QT = TestQueryTable(names=["plugh", "plover"], rows=[[1, 2]])

def test_table():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT a FROM x^", ns) == [("xyzzy", None)]

def test_table_join():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT a FROM foo INNER JOIN x^ ON", ns) == \
    [("xyzzy", None)]

def test_table_inside_parens():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT a FROM (x^", ns) == [("xyzzy", None)]

def test_table_inside_parens_tail():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT a FROM (x^ )", ns) == [("xyzzy", None)]

def test_column_completion_deep():
  ns = Namespace()
  ns.assign_by_path(["foo", "bar", "qux"], _TEST_QT, make_namespaces=True)
  assert _id_c("SELECT * FROM foo.bar.q^", ns) == [("qux", None)]

def test_table_nested_namespace():
  ns = Namespace()
  ns.assign_by_path(["blarg", "xyzzy"], _TEST_QT, make_namespaces=True)
  assert _id_c("SELECT * FROM b^", ns) == [("blarg.", None)]

def test_column_completion_bare():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT plu^ FROM xyzzy", ns) == [("plugh", None)]

def test_column_completion_table_qualified():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT)
  assert _id_c("SELECT xyzzy.pl^ FROM xyzzy", ns) == \
    [("plugh", None), ("plover", None), ]

def test_column_completion_table_deep():
  ns = Namespace()
  ns.assign_by_path(["foo", "bar", "qux"], _TEST_QT, make_namespaces=True)
  assert _id_c("SELECT foo.b^ FROM foo.bar.qux", ns) == [("bar.", None)]

def test_bare_table_reference():
  ns = Namespace()
  ns.assign_by_path(["xyzzy"], _TEST_QT, make_namespaces=True)
  assert _id_c("x^", ns=ns, bare_table_reference=True) == \
    [("xyzzy", None)]

def test_bare_table_reference_deep():
  ns = Namespace()
  ns.assign_by_path(["foo", "bar", "qux"], _TEST_QT, make_namespaces=True)
  assert _id_c("foo.bar.q^", ns=ns, bare_table_reference=True) == \
    [("qux", None)]

def test_table_reference_documentation():
  ns = Namespace()
  qt = TestQueryTable(names=["plugh", "plover"], rows=[[1, 2]])
  add_sql_documentation(qt, "hello world")
  ns.assign_by_path(["xyzzy"], qt, make_namespaces=True)
  assert _id_c("x^", ns=ns, bare_table_reference=True) == \
    [("xyzzy", "hello world")]
