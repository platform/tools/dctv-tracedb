// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <array>
#include <tuple>
#include <type_traits>
#include <utility>

#include "npy.h"
#include "pythread.h"
#include "pyutil.h"
#include "tuple_util.h"
#include "vector.h"

namespace dctv {

struct QueryExecution;
struct QueryCache;

struct GenericResultBuffer : NoCopy, private GilHeldCtorDtor {
  GenericResultBuffer(unique_pyref cb,
                      obj_pyref<QueryExecution> qe,
                      Vector<unique_dtype> dtypes);
  // GIL required
  void flush();
 protected:
  inline npy_intp prepare_to_add_nogil();
  inline pyarray_ref get_array_nogil(npy_intp index) const noexcept;
 private:
  void restart_nogil();
  unique_pyref cb;
  QueryCache* qc;
  npy_intp chunk_size;
  npy_intp size;
  Vector<unique_dtype> dtypes;
  Vector<unique_pyarray> arrays;
};

template<typename... Types>
struct ResultBuffer final : GenericResultBuffer {
  // Constructor requires GIL
  ResultBuffer(unique_pyref cb, obj_pyref<QueryExecution> qe);

  // Does _not_ require GIL
  void add(Types... args);
 private:
  void reset_column_arrays_nogil() noexcept;
  std::tuple<Types*...> column_arrays;
};

inline void init_result_buffer(pyref m) {}

}  // namespace dctv

#include "result_buffer-inl.h"
