// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

IoInput::IoInput(unique_obj_pyref<InputChannel> channel,
                 npy_intp min_wanted_rows,
                 npy_intp max_wanted_rows)
    : channel(std::move(channel)),
      min_wanted_rows(min_wanted_rows),
      max_wanted_rows(max_wanted_rows)
{
  assume(this->channel);
  assume(min_wanted_rows <= max_wanted_rows);
}

IoInputScalar::IoInputScalar(unique_obj_pyref<InputChannel> channel)
    // We use 2, not 1, for the minimum and maximum row counts so that
    // we can observe a non-EOF after one row, which is an interface
    // violation.  When nothing goes wrong, we still end up reading
    // only one row.
    : IoInput(std::move(channel), 2, 2)
{}

IoOutput::IoOutput(unique_obj_pyref<OutputChannel> channel, unique_pyref data)
    : channel(std::move(channel)),
      data(std::move(data))
{
  assume(this->channel);
}

IoResizeBuffer::IoResizeBuffer(unique_obj_pyref<OutputChannel> channel)
    : channel(std::move(channel))
{
  assume(this->channel);
}

IoTerminalFlush::IoTerminalFlush(unique_obj_pyref<OutputChannel> channel)
    : channel(std::move(channel))
{
  assume(this->channel);
}


}  // namespace dctv
