// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "qe_call_terminated.h"
#include "operator_context.h"

namespace dctv {

QeCallTerminated::QeCallTerminated(Vector<IoSpec> terminal_io_specs)
    : terminal_io_specs(std::move(terminal_io_specs))
{}

void
QeCallTerminated::setup(OperatorContext* oc)
{
  QeCall::setup(oc);
  io_setup(oc,
           this->terminal_io_specs.data(),
           this->terminal_io_specs.size());
}

unique_pyref
QeCallTerminated::do_it(OperatorContext* oc)
{
  bool more_to_do =
      do_writes_incremental(this->terminal_io_specs.data(),
                            this->terminal_io_specs.size());
  if (more_to_do) {
    oc->invalidate_score();
    return {};
  }

  // We shouldn't have any non-write IO at this point, so we can just
  // drop our IO specs on the floor.  Where would we deliver
  // the results?
  oc->flush_perf_to_qe();
  oc->close();
  return {};
}

Score
QeCallTerminated::compute_score(const OperatorContext* oc) const
{
  // If we have to do IO as part of closing this operator, treat the
  // close as a normal IO with respect to scoring.
  Score score = oc->compute_basic_score();
  if (this->terminal_io_specs.empty()) {
    score.state = OperatorState::TERMINATED;
    return score;
  }
  return compute_io_score(score,
                          this->terminal_io_specs.data(),
                          this->terminal_io_specs.size());
}

int
QeCallTerminated::py_traverse(visitproc visit, void* arg) const noexcept
{
  for (const IoSpec& io_spec: this->terminal_io_specs)
    if (int ret = io_spec.py_traverse(visit, arg))
      return ret;
  return 0;
}

}  // namespace dctv
