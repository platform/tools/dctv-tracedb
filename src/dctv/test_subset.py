# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for the subset tool"""
import logging
from os.path import join as pjoin
from tempfile import NamedTemporaryFile
from .subset import run_subset
from .util import dctv_dir

# pylint: disable=missing-docstring,line-too-long

log = logging.getLogger(__name__)

def _do_test_subset(*, absolute_time):
  test_trace_file_name = pjoin(dctv_dir(),
                               "test-data",
                               "dragonball.mini.trace")
  with NamedTemporaryFile(prefix="dctv-subset-test-",
                          suffix=".trace", mode="w+") as ntf:
    if absolute_time:
      filter_expression = "_ts < 276946s + 400ms and cpu=0"
      target_number = 2474
    else:
      filter_expression = "_ts < 200ms and cpu=0"
      target_number = 4212

    run_subset(trace_file_name=test_trace_file_name,
               filter_expression=filter_expression,
               absolute_time=absolute_time,
               out_file=ntf.file)
    ntf.seek(0)
    file_iter = iter(ntf.file)
    line = next(file_iter, None)
    assert line == "cpus=8\n"
    line = next(file_iter, None)
    assert line == "          <idle>-0     [000] d..3 276946.286245: sched_waking:         comm=writer pid=5743 prio=96 target_cpu=000\n"
    nr_lines = 0
    for _line in file_iter:
      nr_lines += 1
    assert nr_lines == target_number

def test_subset():
  _do_test_subset(absolute_time=False)

def test_subset_absolute_time():
  _do_test_subset(absolute_time=True)
