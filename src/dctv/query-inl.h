// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

bool
Score::operator<(const Score& other) const noexcept
{
  return this->get_sort_key() < other.get_sort_key();
}

bool
Score::operator==(const Score& other) const noexcept
{
  return this->get_sort_key() == other.get_sort_key();
}

const QueryClasses&
QueryClasses::get()
{
  if (unlikely(!instance.initialized()))
    QueryClasses::ensure();
  return instance;
}

bool
QueryClasses::initialized() const
{
  return static_cast<bool>(this->cls_masked_array);
}

template<typename... Args>
DCTV_NORETURN_ERROR
void
throw_invalid_query_fmt(const char* format, Args&&... args)
{
  throw_invalid_query(fmt(format, std::forward<Args>(args)...));
}

pyarray_ref
maybe_ma(pyref obj)
{
  if (!isinstance_exact(obj, QueryClasses::get().cls_masked_array))
    return {};
  return obj.as_unsafe<PyArrayObject>();
}

pyref
nomask()
{
  return QueryClasses::get().np_ma_nomask.notnull();
}

}  // namespace dctv
