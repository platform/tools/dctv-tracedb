// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "dctv.h"

#include "include_all_modules.h"
#include "pylog.h"

namespace dctv {

void*
_do_py_malloc(size_t nbytes)
{
  void* p = PyMem_RawMalloc(nbytes);
  if (!p)
    throw std::bad_alloc();
  return p;
}

void
_do_py_free(void* mem) noexcept
{
  PyMem_RawFree(mem);
}

static struct PyModuleDef _native_module = {
  PyModuleDef_HEAD_INIT,
  "_native",
  "Native helpers for dctv",
  -1 /* per-interpreter state */,
  nullptr /* init functions add their own methods */,
};

void
in_place_fmt_v(String* buffer, const char* fmt, va_list args)
{
  buffer->clear();
  {
    va_list args2;
    va_copy(args2, args);
    buffer->resize(vsnprintf(nullptr, 0, fmt, args2) + 1);
    va_end(args2);
  }
  vsnprintf(buffer->data(), buffer->size(), fmt, args);
  buffer->resize(buffer->size() - 1);
}

void
in_place_fmt(String* buffer, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  in_place_fmt_v(buffer, fmt, args);
  va_end(args);
}

static void
init_dctv(pyref m)
{}

static void
init_fsutil(pyref m)
{}

// We define dummy init functions here when their hosting modules
// don't include pyutils and so can't declare a pyref.

static void init_pcreutil(pyref m) {}
static void init_simple_variant(pyref m) {}
static void init_mmap(pyref m) {}
static void init_unique_fd(pyref m) {}

static PyObject*
do_init()
{
  if (safe_mode)
    test_simple_variant();

  unique_pyref m = adopt_check(PyModule_Create(&_native_module));
  pylog.init();

  // Make sure numpy APIs are available for all module
  // initialization functions.
  init_npy_early(m);

#define DOIT(mod) init_##mod(m);
#include "do_all_modules.h"
#undef DOIT
  return m.release();
}

}  // namespace dctv



#pragma GCC visibility push(default)

PyMODINIT_FUNC PyInit__native() noexcept;

PyMODINIT_FUNC
PyInit__native() noexcept
{
  return dctv::_exceptions_to_pyerr(dctv::do_init);
}

#pragma GCC visibility pop
