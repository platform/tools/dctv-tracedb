// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

inline
OperatorContext*
QueryExecution::get_current_operator() const
{
  return this->current_operator;
}

QueryCache*
QueryExecution::get_qc() const noexcept
{
  assume(this->qc);
  return this->qc.get();
}

const perf::Sampler*
QueryExecution::get_perf_sampler() const noexcept
{
  if (this->perf_info)
    return &this->perf_info->sampler;
  return nullptr;
}

bool
OperatorContextCompare::operator()(
    const OperatorContext& left,
    const OperatorContext& right) const noexcept
{
  return left.link_score < right.link_score;
}

template<typename Functor>
void
QueryExecution::enumerate_operators(Functor&& functor)
{
  // TODO(dancol): should we flush the score queue and just provide
  // the score explicitly to the cache pruner?
  for (auto& oc : this->re_score_queue)
    functor(&oc);
  for (auto& oc : this->run_queue)
    functor(&oc);
}

}  // namespace dctv
