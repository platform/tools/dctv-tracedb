// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

void
NpyIteratorDeleter::operator()(NpyIter* iter) const noexcept
{
  if (NpyIter_Deallocate(iter) == NPY_FAIL)  // NOLINT
    abort();
}

template<int MinInputs, int MaxInputs>
InputCountBase<MinInputs, MaxInputs>::InputCountBase(int nr_inputs)
    noexcept
    : nr_inputs(nr_inputs)
{
  assert(MinInputs <= this->nr_inputs);
  assert(this->nr_inputs <= MaxInputs);
}

template<int MinInputs, int MaxInputs>
int
InputCountBase<MinInputs, MaxInputs>::get_nr_inputs() const noexcept
{
  assume(MinInputs <= this->nr_inputs);
  assume(this->nr_inputs <= MaxInputs);
  return this->nr_inputs;
}

template<int MinInputs, int MaxInputs>
constexpr
NpyIteration<MinInputs, MaxInputs>::NpyIteration() noexcept
    : inner_remaining(0)
{}

template<int MinInputs, int MaxInputs>
constexpr
NpyIteration<MinInputs, MaxInputs>::NpyIteration(int nr_inputs) noexcept
    : InputCountBase<MinInputs, MaxInputs>(nr_inputs),
      inner_remaining(0)
{}

template<int MinInputs, int MaxInputs>
NpyIteration<MinInputs, MaxInputs>::NpyIteration(
    PyRefSpan<PyArrayObject> inputs,
    PyRefSpan<PyArray_Descr> dtypes,
    NpyIterConfig config)
    : InputCountBase<MinInputs, MaxInputs>(
          static_cast<int>(inputs.size()))
{
  assert(0 < inputs.size() && inputs.size()
         <= std::numeric_limits<int>::max());
  assert(inputs.size() <= dtypes.size());
  int nr_inputs = inputs.size();
  assume(MinInputs <= nr_inputs && nr_inputs <= MaxInputs);
  for (int i = 0; i < nr_inputs; ++i)
    if (inputs[i])
      check_pytype_exact(pytype_of<PyArrayObject>(), inputs[i]);
  std::array<npy_uint32, MaxInputs> flags;
  for (Py_ssize_t i = 0; i < nr_inputs; ++i) {
    flags[i] = inputs[i]
        ? NPY_ITER_READONLY
        : (NPY_ITER_WRITEONLY |
           NPY_ITER_ALLOCATE |
           NPY_ITER_NO_SUBTYPE);
    if (config.extra_op_flags)
      flags[i] |= config.extra_op_flags[i];
  }
  this->iter.reset(
      NpyIter_AdvancedNew(
          nr_inputs,
          const_cast<PyArrayObject**>(inputs.data()),
          (NPY_ITER_EXTERNAL_LOOP |
           NPY_ITER_ZEROSIZE_OK |
           config.iter_flags),
          NPY_KEEPORDER,
          config.casting,
          flags.data(),
          const_cast<PyArray_Descr**>(dtypes.data()),
          config.oa_ndim,
          config.op_axes,
          config.itershape,
          config.buffersize));
  if (!this->iter)
    throw_current_pyerr();
  this->reset_state();
}

template<int MinInputs, int MaxInputs>
NpyIteration<MinInputs, MaxInputs>
NpyIteration<MinInputs, MaxInputs>::from_tuple(
    int nr_inputs,
    pyref in_tuple,
    PyRefSpan<PyArray_Descr> dtypes,
    NpyIterConfig config)
{
  assume(MinInputs <= nr_inputs && nr_inputs <= MaxInputs);
  pytuple::ref tuple = in_tuple.as<PyTupleObject>();
  if (pytuple::size(tuple) != static_cast<size_t>(nr_inputs))
    _throw_pyerr_fmt(PyExc_ValueError,
                     "wrong number of inputs: wanted %d got %d in %R",
                     nr_inputs,
                     static_cast<int>(pytuple::size(tuple)),
                     tuple.get());
  std::array<unique_pyarray, MaxInputs> inputs;
  std::array<PyArrayObject*, MaxInputs> ptrs;
  for (int i = 0; i < nr_inputs; ++i) {
    pyref obj = pytuple::get(tuple, i);
    if (PyArray_Check(obj.get()))
      inputs[i] = obj.addref_as_unsafe<PyArrayObject>();
    else
      inputs[i] = adopt_check_as_unsafe<PyArrayObject>(
          PyArray_FROM_O(obj.get()));
    ptrs[i] = inputs[i].get();
  }
  return NpyIteration(PyRefSpan<PyArrayObject>(&ptrs[0], nr_inputs),
                      dtypes, config);
}

template<int MinInputs, int MaxInputs>
pyarray_ref
NpyIteration<MinInputs, MaxInputs>::get_operand_array(int array_number) const noexcept
{
  return NpyIter_GetOperandArray(this->iter.get())[array_number];
}

template<int MinInputs, int MaxInputs>
unique_pyarray
NpyIteration<MinInputs, MaxInputs>::get_operand_view(int array_number) const
{
  return npy_iter_view(this->iter.get(), array_number);
}

template<int MinInputs, int MaxInputs>
bool
NpyIteration<MinInputs, MaxInputs>::advance() noexcept
{
  assume(MinInputs <= this->get_nr_inputs() &&
         this->get_nr_inputs() <= MaxInputs);

  // inner_remaining is the number of items left _before_ the advance.

  if (this->inner_remaining > 1) {
    this->inner_remaining -= 1;
    for (int i = 0; i < this->get_nr_inputs(); ++i)
      this->data[i] += this->stride[i];
    return true;
  }

  // We advanced past the last item.  Fetch more items if possible.
  // While we see zero-sized chunks, keep asking for more.  This way,
  // inner_remaining is zero only if we're truly at EOF.
  do {
    if (!this->iternext(this->iter.get())) {
      this->inner_remaining = 0;
      return false;
    }
    this->start_inner_loop();
  } while (this->inner_remaining == 0);
  assume(this->inner_remaining > 0);
  return true;
}

template<int MinInputs, int MaxInputs>
bool
NpyIteration<MinInputs, MaxInputs>::is_at_eof() const noexcept
{
  return this->inner_remaining == 0;
}

template<int MinInputs, int MaxInputs>
size_t
NpyIteration<MinInputs, MaxInputs>::size() const noexcept
{
  npy_intp size = NpyIter_GetIterSize(this->iter.get());
  assume(size >= 0);
  return static_cast<size_t>(size);
}

template<int MinInputs, int MaxInputs>
void
NpyIteration<MinInputs, MaxInputs>::rewind()
{
  char* errmsg;
  if (NpyIter_Reset(this->iter.get(), &errmsg) == NPY_FAIL)
    throw_npyiter_error_nogil(errmsg);
  this->reset_state();
}

template<int MinInputs, int MaxInputs>
template<typename T>
typename std::enable_if_t<std::is_arithmetic_v<T>, T>
NpyIteration<MinInputs, MaxInputs>::get(int input_nr) const noexcept
{
  assume(!this->is_at_eof());
  assume(input_nr >= 0);
  assume(input_nr <= this->get_nr_inputs());
  assume(MinInputs <= this->get_nr_inputs() &&
         this->get_nr_inputs() <= MaxInputs);
  assert(NpyIter_GetDescrArray(this->iter.get())[input_nr]->elsize
         == sizeof (T));
  T value;
  memcpy(&value, this->data[input_nr], sizeof (value));
  return value;
}

template<int MinInputs, int MaxInputs>
template<typename T>
typename std::enable_if_t<std::is_same_v<T, BoolWrapper>, T>
NpyIteration<MinInputs, MaxInputs>::get(int input_nr) const noexcept
{
  return BoolWrapper(this->get<bool>(input_nr));
}

template<int MinInputs, int MaxInputs>
template<typename T>
void
NpyIteration<MinInputs, MaxInputs>::set(int input_nr, T value) noexcept
{
  assume(!this->is_at_eof());
  assume(input_nr >= 0);
  assume(input_nr <= this->get_nr_inputs());
  assume(MinInputs <= this->get_nr_inputs() &&
         this->get_nr_inputs() <= MaxInputs);
  assert(NpyIter_GetDescrArray(this->iter.get())[input_nr]->elsize
         == sizeof (T));
  memcpy(this->data[input_nr], &value, sizeof (value));
}

template<int MinInputs, int MaxInputs>
void
NpyIteration<MinInputs, MaxInputs>::start_inner_loop() noexcept
{
  assume(MinInputs <= this->get_nr_inputs() &&
         this->get_nr_inputs() <= MaxInputs);
  memcpy(this->data.data(),
         this->ptr_data_array,
         this->get_nr_inputs() * sizeof (this->data[0]));
  this->inner_remaining = *this->ptr_inner_size;
  memcpy(this->stride.data(),
         this->ptr_inner_stride,
         this->get_nr_inputs() * sizeof (this->stride[0]));
}

template<int MinInputs, int MaxInputs>
void
NpyIteration<MinInputs, MaxInputs>::reset_state()
{
  if (this->size() == 0) {
    this->inner_remaining = 0;
    // Silence compiler warnings
    this->iternext = nullptr;
    this->ptr_inner_size = nullptr;
    this->ptr_data_array = nullptr;
    this->ptr_inner_stride = nullptr;
    std::fill(this->data.begin(), this->data.end(), nullptr);
    std::fill(this->stride.begin(), this->stride.end(), 0);
    return;
  }
  char* errmsg;
  this->iternext = NpyIter_GetIterNext(this->iter.get(), &errmsg);
  if (!this->iternext)
    throw_npyiter_error_nogil(errmsg);
  char** data = NpyIter_GetDataPtrArray(this->iter.get());
  this->ptr_data_array = reinterpret_cast<uint8_t**>(data);
  this->ptr_inner_size = NpyIter_GetInnerLoopSizePtr(this->iter.get());
  this->ptr_inner_stride = NpyIter_GetInnerStrideArray(this->iter.get());
  this->start_inner_loop();
  if (this->inner_remaining == 0)
    this->advance();
}

template<typename Value>
NpyIteration1<Value>::NpyIteration1(
    pyarray_ref input,
    NpyIterConfig config)
    : Base(input.as_span(),
           type_descr<Value>().as_span(),
           std::move(config))
{}

template<typename Value>
Value
NpyIteration1<Value>::get() const noexcept
{
  return this->Base::template get<Value>(0);
}

template<typename Value>
void
NpyIteration1<Value>::set(Value v) noexcept
{
  this->Base::template set<Value>(0, v);
}

template<int MinInputs, int MaxInputs>
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::FlexiblePythonChunkIterator(
    int nr_inputs,
    unique_pyref iter,
    std::array<unique_dtype, MaxInputs> dtypes,
    NpyIterConfig npy_config)
    : index(-1),
      iter(pyiter_for(std::move(iter))),
      npy_config(npy_config),
      npy_iter(nr_inputs),
      dtypes(std::move(dtypes))
{
  if (!PyIter_Check(this->iter.get()))
    _throw_pyerr_fmt(PyExc_ValueError,
                     "wanted an iterator: got %R",
                     this->iter.get());
  this->fetch_chunk();
}

template<int MinInputs, int MaxInputs>
bool
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::is_at_eof()
    const noexcept
{
  return this->npy_iter.is_at_eof();
}

template<int MinInputs, int MaxInputs>
bool
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::advance()
{
  if (npy_iter.advance()) {
    ++index;
    return true;
  }

  with_gil_acquired([&] {
    this->fetch_chunk();
  });
  return !this->is_at_eof();
}

template<int MinInputs, int MaxInputs>
template<typename T>
T
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::get(int input_nr)
    const noexcept
{
  return this->npy_iter.template get<T>(input_nr);
}

template<int MinInputs, int MaxInputs>
Index
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::get_index()
    const noexcept
{
  return this->index;
}

template<int MinInputs, int MaxInputs>
int
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::get_nr_inputs()
    const noexcept
{
  return this->npy_iter.get_nr_inputs();
}

template<int MinInputs, int MaxInputs>
void
FlexiblePythonChunkIterator<MinInputs, MaxInputs>::fetch_chunk()
{
  assert_gil_held();
  do {
    unique_pyref chunk = pyiter_next(this->iter);
    if (!chunk) {
      this->npy_iter = MyNpyIteration(this->npy_iter.get_nr_inputs());
      return;
    }
    std::array<PyArray_Descr*, MaxInputs> dtypes_buffer;
    assume(MinInputs <= this->npy_iter.get_nr_inputs());
    assume(this->npy_iter.get_nr_inputs() <= MaxInputs);
    for (int i = 0; i < this->npy_iter.get_nr_inputs(); ++i)
      dtypes_buffer[i] = this->dtypes[i].get();
    this->npy_iter = MyNpyIteration::from_tuple(
        this->npy_iter.get_nr_inputs(),
        chunk,
        PyRefSpan(dtypes_buffer.begin(), this->npy_iter.get_nr_inputs()),
        this->npy_config);
  } while (this->npy_iter.is_at_eof());
  ++this->index;
}

template<typename... Types>
PythonChunkIterator<Types...>::PythonChunkIterator(
    unique_pyref iter,
    NpyIterConfig config)
    : Base(NrColumns,
           std::move(iter),
           {type_descr<Types>()...},
           std::move(config))
{}

template<typename... Types>
typename PythonChunkIterator<Types...>::Row
PythonChunkIterator<Types...>::get() const noexcept
{
  return this->get_internal(
      std::make_index_sequence<std::tuple_size_v<Row>>());
}

template<typename... Types>
template<size_t... I>
typename PythonChunkIterator<Types...>::Row
PythonChunkIterator<Types...>::get_internal(std::index_sequence<I...>)
    const noexcept
{
  return Row{this->Base::template get<Types>(I)...};
}

}  // namespace dctv
