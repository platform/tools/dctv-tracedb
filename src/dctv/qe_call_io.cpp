// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "qe_call_io.h"

#include <type_traits>

#include "awaitable_qe_call.h"
#include "input_channel.h"
#include "npy.h"
#include "operator_context.h"
#include "output_channel.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pylog.h"
#include "pyseq.h"
#include "vector.h"

namespace dctv {

QeCallIo::QeCallIo(Vector<IoSpec> io_specs)
    : io_specs(std::move(io_specs))
{}

void
QeCallIo::setup(OperatorContext* oc)
{
  QeCall::setup(oc);
  io_setup(oc, this->io_specs.data(), this->io_specs.size());
}

Score
QeCallIo::compute_score(const OperatorContext* oc) const
{
  return compute_io_score(oc->compute_basic_score(),
                          this->io_specs.data(),
                          this->io_specs.size());
}

bool
QeCallIo::do_try_writes()
{
  return do_writes_incremental(
      this->io_specs.data(), this->io_specs.size());
}

int
QeCallIo::py_traverse(visitproc visit, void* arg) const noexcept
{
  for (const IoSpec& io_spec: this->io_specs)
    if (int ret = io_spec.py_traverse(visit, arg))
      return ret;
  return 0;
}

QeCallIoSingle::QeCallIoSingle(IoSpec io_spec)
    : QeCallIo({})
{
  this->io_specs.push_back(std::move(io_spec));
}

unique_pyref
QeCallIoSingle::do_it(OperatorContext* oc)
{
  // See commentary in QeCallIoMulti::do_it()
  assume(this->io_specs.size() == 1);
  if (!this->do_try_writes())
    oc->communicate(this->io_specs[0].do_it());
  oc->invalidate_score();
  return {};
}

unique_pyref
QeCallIoMulti::do_it(OperatorContext* oc)
{
  if (this->do_try_writes()) {
    // If we did some writes but still have more to do, just leave
    // this QeCall in place: the engine will call do_it() again.
  } else {
    // Otherwise, fetch the next operator.  At this point, we'll
    // perform any reads, having already done the writes.
    oc->communicate(
        pylist::from(this->io_specs,
                     [](auto& io_spec) { return io_spec.do_it(); }));
  }
  oc->invalidate_score();
  return {};
}

IoSpec
QeCallIoSingle::extract_io_spec()
{
  if (this->io_specs.empty())
    throw_pyerr_msg(PyExc_AssertionError, "IO call already used");
  assume(this->io_specs.size() == 1);
  IoSpec ret = std::move(this->io_specs[0]);
  this->io_specs.clear();
  return ret;
}

unique_pyref
make_qe_call_io_single(IoSpec&& io_spec)
{
  return make_qe_call(std::make_unique<QeCallIoSingle>(std::move(io_spec)));
}

unique_pyref
make_qe_call_io_multi(pytuple::ref py_io_specs)
{
  size_t nspecs = pytuple::size(py_io_specs);
  Vector<IoSpec> io_specs;
  for (size_t i = 0; i < nspecs; ++i)
    io_specs.push_back(
        pytuple::get(py_io_specs, i)
        .as<AwaitableQeCall>()
        ->extract_request()
        ->extract_io_spec());
  return make_qe_call(std::make_unique<QeCallIoMulti>(std::move(io_specs)));
}

}  // namespace dctv
