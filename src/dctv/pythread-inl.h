// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename Functor>
auto
with_gil_acquired(Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()))
{
  PyGILState_STATE state = PyGILState_Ensure();
  FINALLY(PyGILState_Release(state));
  return AUTOFWD(functor)();
}

template<typename Functor>
auto
with_gil_released(Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()))
{
  PyThreadState* save =
      PyGILState_Check() ? PyEval_SaveThread() : nullptr;
  FINALLY(if (save) PyEval_RestoreThread(save));
  return AUTOFWD(functor)();
}

template<typename Functor>
auto
with_gil_released_if(bool condition, Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()))
{
  if (condition)
    return with_gil_released(AUTOFWD(functor));
  return AUTOFWD(functor);
}

UniqueLock::UniqueLock(Mutex& mutex) noexcept
    : lock(mutex.mutex)
{}

template<typename Predicate>
void
Condition::wait_for(UniqueLock& lock,
                    Predicate&& predicate) noexcept
{
  while (!predicate()) {
    with_gil_released([&] {
      this->condition.wait(lock.lock,
                           std::forward<Predicate>(predicate));
    });
  }
}

void
assert_gil_held() noexcept
{
  assert(PyGILState_Check());
}

GilHeldCtorDtor::GilHeldCtorDtor() noexcept
{
  assert_gil_held();
}

GilHeldCtorDtor::~GilHeldCtorDtor() noexcept
{
  assert_gil_held();
}

template<typename T>
String
repr_nogil(T&& arg)
{
  return with_gil_acquired([&] {
    return repr(AUTOFWD(arg));
  });
}

}  // namespace dctv
