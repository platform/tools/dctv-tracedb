// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

OperatorContext*
OutputChannel::get_owner() const noexcept
{
  return this->oc;
}

bool
OutputChannel::is_disconnected() const
{
  return this->sinks.empty();
}

void
OutputChannel::enable_dynamic_block_size()
{
  if (!this->dynamic_block_size)
    this->dynamic_block_size = true;
}

bool
OutputChannel::is_dynamic_buffer_size_enabled() const noexcept
{
  return this->dynamic_block_size;
}

bool
OutputChannel::needs_flush() const noexcept
{
  return this->block_builder.get_partial_size();
}

}  // namespace dctv
