// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <functional>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>

#include "absl/container/flat_hash_map.h"
#include "absl/container/flat_hash_set.h"
#include "absl/hash/hash.h"

namespace dctv {

template<typename T>
using Hash = std::conditional_t<use_absl_hash_tables,
                                absl::Hash<T>, std::hash<T>>;

template<typename Key,
         typename Value,
         typename Hash = Hash<Key>,
         typename Equal = std::equal_to<Key>,
         typename Allocator = PyAllocator<std::pair<const Key, Value>>>
using HashTable = std::conditional_t<use_absl_hash_tables,
  absl::flat_hash_map<Key, Value, Hash, Equal, Allocator>,
  std::unordered_map<Key, Value, Hash, Equal, Allocator>>;

template<typename Key,
         typename Hash = Hash<Key>,
         typename Equal = std::equal_to<Key>,
         typename Allocator = PyAllocator<Key>>
using HashSet = std::conditional_t<use_absl_hash_tables,
  absl::flat_hash_set<Key, Hash, Equal, Allocator>,
  std::unordered_set<Key, Hash, Equal, Allocator>>;

// TODO(dancol): move HashSponge to stackify code
struct HashSponge final {
  HashSponge() = default;
  size_t squeeze() const { return seed; }
  template<typename T>
  void absorb(const T& thing) noexcept;
  private:
  size_t seed = 0;
};

}  // namespace dctv

#include "hash_table-inl.h"
