// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#define DCTV_IS_NPY_CPP
#include "npy.h"

#include <array>
#include <iterator>
#include <utility>

#include "automethod.h"
#include "bitbash.h"
#include "pyerrfmt.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"

namespace dctv {

// TODO(dancol): deal with this mess a different way when we port to
// Windows or some other LLP64 architecture.
static_assert(sizeof (long) == sizeof (int64_t));

static_assert(sizeof (long long) == sizeof (int64_t));
static_assert(sizeof (unsigned long long) == sizeof (uint64_t));

static_assert(NPY_INT8 == NPY_BYTE);  // NOLINT
static_assert(NPY_UINT8 == NPY_UBYTE);  // NOLINT
static_assert(NPY_INT16 == NPY_SHORT);  // NOLINT
static_assert(NPY_UINT16 == NPY_USHORT);  // NOLINT
static_assert(NPY_INT32 == NPY_INT);  // NOLINT
static_assert(NPY_UINT32 == NPY_UINT);  // NOLINT
static_assert(NPY_INT64 == NPY_LONG);  // NOLINT
static_assert(NPY_UINT64 == NPY_ULONG);  // NOLINT

// Helps for initializing type tables so that the compiler helps us
// get them exactly right.

static constexpr char known_dtypes[] = "?bBhHiIlLfd";
static constexpr int nr_known_dtypes = sizeof (known_dtypes) - 1;

// Use raw pointers so we don't try to deallocate on exit:
// intentionally leaked!
// TODO(dancol): figure out robust callback for module unload
// and exit
static PyArrayObject* true_array;
static PyArrayObject* false_array;

template<size_t N, size_t... Is>
constexpr
std::array<char, N - 1>
sa(const char (&a)[N], std::index_sequence<Is...>)
{
  return {{a[Is]...}};
}

template<size_t N>
constexpr
std::array<char, N - 1>
sa(const char (&a)[N])
{
  return sa(a, std::make_index_sequence<N - 1>());
}

using TypeConvTableData =
    std::array<std::array<char, nr_known_dtypes>, nr_known_dtypes>;

struct TypeConvTable : TypeConvTableData {
  template<typename... Args>
  constexpr
  TypeConvTable(Args&&... args)  // NOLINT
        : TypeConvTableData{sa(args)...}
  {}
};

void
check_sane_dtype(dtype_ref dtype)
{
  // We deliberately haven't bothered with support for complex numbers
  // or float128.

  if (dtype->flags != 0 ||
      dtype->elsize == 0 ||
      dtype->subarray != nullptr ||
      dtype->fields != Py_None ||
      !strchr(known_dtypes, dtype->type) ||
      !strchr("=|", dtype->byteorder)) {
    throw_pyerr_fmt(PyExc_TypeError,
                    "unsupported exotic dtype %s",
                    repr(dtype));
  }
  assume(dtype->elsize == 1 || dtype->elsize == 2 ||
         dtype->elsize == 4 || dtype->elsize == 8);
  assume((dtype->kind == 'b') == (dtype->type == '?'));
  assert(strchr("biuf", dtype->kind));
}

static
int
type_table_index(dtype_ref dtype)
{
  check_sane_dtype(dtype);
  const char* ptr = strchr(known_dtypes, dtype->type);
  assume(ptr);
  ptrdiff_t index = ptr - &known_dtypes[0];
  assume(index < nr_known_dtypes);
  return static_cast<int>(index);
}

unique_dtype
as_dtype(pyref obj)
{
  PyArray_Descr* py_dtype;
  if (PyArray_DescrConverter(obj.get(), &py_dtype)
      != NPY_SUCCEED)
    throw_current_pyerr();
  return unique_dtype::adopt(py_dtype);
}

char
py_dtype_to_code(pyref orig_py_dtype)
{
  unique_dtype dtype = as_dtype(orig_py_dtype);
  check_sane_dtype(dtype);
  return dtype->type;
}

unique_dtype
type_descr_from_type(int type)
{
  PyArray_Descr* descr = PyArray_DescrFromType(type);
  if (!descr) {
    if (pyerr_occurred())
      throw_current_pyerr();
    throw_pyerr_msg(PyExc_ValueError, "invalid type");
  }
  if (safe_mode)
    check_sane_dtype(descr);
  return unique_dtype::adopt(descr);
}

unique_pyarray
make_uninit_pyarray(unique_dtype dtype, npy_intp nelem)
{
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_Empty(/*nd=*/1,
                    &nelem,
                    dtype.release() /* sic: steals reference */,
                    /*fortran=*/0));
}

unique_pyarray
make_pyarray(unique_dtype dtype, npy_intp size)
{
  assume(size >= 0);
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_SimpleNewFromDescr(
          1, &size, dtype.release() /* sic: steals */));
}

void
npy_resize(pyarray_ref array, npy_intp size)
{
  assume(size >= 0);
  assume(npy_ndim(array) == 1);
  PyArray_Dims dims = {&size, 1};
  adopt_check(
      PyArray_Resize(array.get(),
                     &dims,
                     /*refcheck=*/0,
                     NPY_ANYORDER));
}

unique_pyarray
get_npy_slice(pyarray_ref array,
              Py_ssize_t i1,
              Py_ssize_t i2)
{
  return get_slice(array, i1, i2).addref_as_unsafe<PyArrayObject>();
}

static
unique_pyarray
as_pyarray_1(pyref value,
             unique_dtype dtype,
             int requirements,
             int min_depth,
             int max_depth)
{
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_FromAny(value.get(),
                      dtype.release() /* sic */,
                      min_depth,
                      max_depth,
                      requirements,
                      /*context=*/nullptr));
}

unique_pyarray
as_any_pyarray(pyref value)
{
  return as_pyarray_1(value,
                      /*dtype=*/unique_dtype(),
                      /*requirements=*/0,
                      /*min_depth=*/0,
                      /*max_depth=*/0);
}

unique_pyarray
as_base_1d_pyarray(pyref value, dtype_ref dtype)
{
  unique_pyarray array = as_pyarray_1(value,
                                      dtype.addref(),
                                      NPY_ARRAY_ENSUREARRAY,
                                      /*min_depth=*/0,
                                      /*max_depth=*/1);
  // Ugh: PyArray_FromAny can return an array with a dtype other than
  // the one requested if the returned dtype is "equivalent" to the
  // requested one --- basically, if it has the same bit pattern.
  // We'd prefer an exact match, so in this case, return a view.
  if (dtype && npy_dtype(array) != dtype) {
    assert(dtype_equiv(npy_dtype(array), dtype));
    array = pyarray_view(array, dtype);
    assert(npy_dtype(array) == dtype);
  }

  if (npy_ndim(array) == 0)
    array = npy_flatten(array);
  return array;
}

unique_pyarray
copy_pyarray(pyarray_ref array)
{
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_NewCopy(array.get(), NPY_ANYORDER));
}

bool
npy_any(pyarray_ref array)
{
  return true_(adopt_check(PyArray_Any(array.get(), NPY_MAXDIMS, nullptr)));
}

bool
npy_all(pyarray_ref array)
{
  return true_(adopt_check(PyArray_All(array.get(), NPY_MAXDIMS, nullptr)));
}

// TODO(dancol): intern these arrays?

unique_pyarray
make_true_array()
{
  assume(true_array);
  return xaddref(true_array);
}

unique_pyarray
make_false_array()
{
  assume(false_array);
  return xaddref(false_array);
}

void
npy_set_base(pyarray_ref array, unique_pyref obj)
{
  if (PyArray_SetBaseObject(
          array.get(),
          obj.release() /* sic */))
    throw_current_pyerr();
}

unique_pyarray
npy_flatten(pyarray_ref array)
{
  return adopt_check(PyArray_Flatten(array.get(), NPY_ANYORDER))
      .addref_as_unsafe<PyArrayObject>();
}

unique_pyarray
npy_ravel(pyarray_ref array)
{
  return adopt_check(PyArray_Ravel(array.get(), NPY_ANYORDER))
      .addref_as_unsafe<PyArrayObject>();
}

unique_pyarray
npy_broadcast_to(pyarray_ref array, npy_intp length)
{
  PyArrayObject* array_o = array.get();
  npy_uint32 op_flags[1] = { NPY_ITER_READONLY };
  int* axes[1] = { nullptr };
  NpyIter* iter = NpyIter_AdvancedNew(
      /*nop=*/1,
      &array_o,
      (NPY_ITER_MULTI_INDEX |
       NPY_ITER_ZEROSIZE_OK |
       NPY_ITER_REFS_OK),
      NPY_CORDER,
      NPY_NO_CASTING,
      &op_flags[0],
      /*op_dtypes=*/nullptr,
      /*oa_ndim=*/1,
      /*oa_axes=*/&axes[0],
      /*itershape=*/&length,
      /*buffersize=*/0);
  if (!iter)
    throw_current_pyerr();
  FINALLY(NpyIter_Deallocate(iter));
  return adopt_check_as_unsafe<PyArrayObject>(
      NpyIter_GetIterView(iter, 0));
}



// This function defines DCTV's rules for computing the result type of
// an operation.  We don't use np.result_type because that thing has
// bogus opinions, e.g., that int64 + uint64 -> float.
// We need data type coercion in two places: 1) when we're
// concatenating two schemas (e.g., in UNION ALL) and 2) when we're
// performing an arithmetic operation.  In case #1, the goal is to
// find a dtype that can represent all values that we're combining.
// In case #2, we want to find a dtype that can hold any intermediate
// value we might produce.  OP can be:
//
//   'c' --- concatenation
//   'd' --- true division
//   '+' --- arithmetic
//
static
unique_dtype
dctv_rules_result_type(dtype_ref left, dtype_ref right, char op)
{
  // Type conversion is symmetric, so we leave these tables
  // half-filled.  If we look up a conversion and get a blank, we just
  // flip the operands and try again.

  // This is the table we use for concatenation.  We keep the type
  // width as small as possible while being value-preserving, and if
  // we can't preserve value, we go to int64.  Note that in the table
  // below, the only integer-type column that's not value-preserving
  // is "L"!
  static constexpr TypeConvTable concatenation = {
    //       ?bBhHiIlLfd
    /* ? */ "?bBhHiIlLfd",
    /* b */ " bhhiilllfd",
    /* B */ "  BhHiIlLfd",
    /* h */ "   hiilllfd",
    /* H */ "    HiIlLfd",
    /* i */ "     illlfd",
    /* I */ "      IlLfd",
    /* l */ "       llfd",
    /* L */ "        Lfd",
    /* f */ "         fd",
    /* d */ "          d",
  };

  // When we perform true division, we already get a floating point
  // value of some sort.
  static constexpr TypeConvTable true_division = {
    //       ?bBhHiIlLfd
    /* ? */ "dddddddddfd",
    /* b */ " ddddddddfd",
    /* B */ "  dddddddfd",
    /* h */ "   ddddddfd",
    /* H */ "    dddddfd",
    /* i */ "     ddddfd",
    /* I */ "      dddfd",
    /* l */ "       ddfd",
    /* L */ "        dfd",
    /* f */ "         fd",
    /* d */ "          d",
  };

  // I have no idea what the optimal coercion rules are for the
  // integers when we're doing math.  On one hand, we're
  // memory-bandwidth limited, so we want to keep values as small as
  // possible.  On the other hand, if we keep values small, then
  // overflow will surprise users.  We can't just trap overflow: numpy
  // integer ufuncs don't care about carry, which means that we need
  // to just let math overflow if it's going to overflow --- and this
  // overflow is observable and so becomes part of DCTV's API surface.
  //
  // Let's promote straight to 64-bit values.  It'll cost us a bit of
  // efficiency right now, but it gives us the maximum API
  // flexibility.  In the future, when we've fixed numpy to detect
  // integer overflow or switched to Apache Arrow or something, we can
  // try performing arithmetic in narrowed form and transparently
  // upgrade to wide types and still let these wide types overflow.
  // Since we operate block-by-block, trying each block twice
  // won't hurt.
  //
  // We still don't want to gratuitously value-destroying though, so
  // don't bump L down to l when we're mixing it with an
  // unsigned type.

  static constexpr TypeConvTable arithmetic = {
    //       ?bBhHiIlLfd
    /* ? */ "lllllllllfd",
    /* b */ " llllllllfd",
    /* B */ "  llllllLfd",
    /* h */ "   llllllfd",
    /* H */ "    llllLfd",
    /* i */ "     llllfd",
    /* I */ "      llLfd",
    /* l */ "       llfd",
    /* L */ "        Lfd",
    /* f */ "         fd",
    /* d */ "          d",
  };

  const TypeConvTable* table;

  if (op == 'c')
    table = &concatenation;
  else if (op == 'd')
    table = &true_division;
  else if (op == '+')
    table = &arithmetic;
  else
    throw_pyerr_fmt(PyExc_ValueError,
                    "unknown conversion operation %s",
                    repr(std::string_view(&op, 1)));

  int left_index = type_table_index(left);
  int right_index = type_table_index(right);
  char result = (*table)[left_index][right_index];
  if (result == ' ')
    result = (*table)[right_index][left_index];
  return type_descr_from_type(result);
}

static
unique_pyref
py_dctv_rules_result_type(PyObject*, pyref args)
{
  PARSEPYARGS(
      (pyref, left)
      (pyref, right)
      (OPTIONAL_ARGS_FOLLOW)
      (py_char_code, op, 'c')
  )(args);
  return dctv_rules_result_type(as_dtype(left),
                                as_dtype(right),
                                op.value);
}

bool
dtype_equiv(dtype_ref a, dtype_ref b) noexcept
{
  return PyArray_EquivTypes(a.get(), b.get()) != NPY_FALSE;
}

unique_pyarray
pyarray_view(pyarray_ref array, dtype_ref dtype)
{
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_View(array.get(), dtype.get(), /*ptype=*/nullptr));
}

static
uintptr_t
npy_memory_location(pyarray_ref array)
{
  return reinterpret_cast<uintptr_t>(npy_data_raw(array));
}

static
unique_pyref
npy_make_rw_view(pyarray_ref array, dtype_ref dtype)
{
  unique_pyarray view = pyarray_view(array, dtype);
  PyArray_ENABLEFLAGS(view.get(), NPY_ARRAY_WRITEABLE);
  return view;
}



unique_pyref
make_ufunc_from_func_and_data(
    PyUFuncGenericFunction *func, void **data,
    char *types, int ntypes,
    int nin, int nout, int identity,
    const char *name, const char *doc, int unused)
{
  return adopt_check(
      PyUFunc_FromFuncAndData(
          func,
          data,
          types,
          ntypes,
          nin,
          nout,
          identity,
          name,
          doc,
          unused));
}

template<typename T, typename O>
void
fls_loop(char** args,
         npy_intp* dimensions,  // NOLINT
         npy_intp* steps,  // NOLINT
         void* /*data*/) noexcept
{
  char* inp = args[0];
  npy_intp inp_step = steps[0];
  char* out = args[1];
  npy_intp out_step = steps[1];
  npy_intp n = dimensions[0];
  for (npy_intp i = 0; i < n; ++i) {
    *reinterpret_cast<O*>(out) =
        static_cast<O>(
            fls(*reinterpret_cast<T*>(inp)));
    inp += inp_step;
    out += out_step;
  }
}



static bool
init_npy_1()
{
  import_array(); // Macro: returns false on failure (eww)
  return true;
}

static bool
import_ufunc_1()
{
  import_ufunc(); // Macro: returns false on failure (eww)
  return true;
}



static PyMethodDef functions[] = {
  make_methoddef("dctv_rules_result_type",
                 wraperr<py_dctv_rules_result_type>(),
                 METH_VARARGS,
                 "Figure out the proper result type for an operation"),
  AUTOFUNCTION(npy_broadcast_to,
               "Broadcast-to in C++",
               (unique_pyarray, array, no_default, convert_any_array)
               (npy_intp, shape)
  ),
  AUTOFUNCTION(npy_memory_location,
               "Actual memory location of numpy data: for debugging",
               (unique_pyarray, array, no_default, convert_any_array)
  ),
  AUTOFUNCTION(npy_make_rw_view,
               "Make a forced writeable view: awful hack",
               (unique_pyarray, array, no_default, convert_any_array)
               (OPTIONAL_ARGS_FOLLOW)
               (unique_dtype, dtype, unique_dtype(), convert_dtype)
  ),
  { 0 }
};

void
init_npy_early(pyref /*m*/)
{
  if (!init_npy_1())
    throw_current_pyerr();
  if (!import_ufunc_1())
    throw_current_pyerr();
  false_array = make_uninit_pyarray(type_descr<bool>(), 1)
      .release();
  *npy_data<bool>(false_array) = false;
  true_array = make_uninit_pyarray(type_descr<bool>(), 1)
      .release();
  *npy_data<bool>(true_array) = true;
}

static
unique_pyref
make_fls_ufunc()
{
  static PyUFuncGenericFunction fls_loops[] = {
    &fls_loop<int8_t, int8_t>,
    &fls_loop<int16_t, int8_t>,
    &fls_loop<int32_t, int8_t>,
    &fls_loop<int64_t, int8_t>,
    &fls_loop<uint8_t, int8_t>,
    &fls_loop<uint16_t, int8_t>,
    &fls_loop<uint32_t, int8_t>,
    &fls_loop<uint64_t, int8_t>,
    &fls_loop<long, int8_t>,
    &fls_loop<ulong, int8_t>,
  };

  static char fls_types[] = {
    NPY_INT8, NPY_INT8,
    NPY_INT16, NPY_INT8,
    NPY_INT32, NPY_INT8,
    NPY_INT64, NPY_INT8,
    NPY_UINT8, NPY_INT8,
    NPY_UINT16, NPY_INT8,
    NPY_UINT32, NPY_INT8,
    NPY_UINT64, NPY_INT8,
    NPY_LONG, NPY_INT8,
    NPY_ULONG, NPY_INT8,
  };

  static void* fls_data[] = {
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
  };

  static_assert(std::size(fls_types) == 2 * std::size(fls_loops));
  static_assert(std::size(fls_data) == std::size(fls_loops));

  return adopt_check(
      PyUFunc_FromFuncAndData(
          fls_loops,
          fls_data,
          fls_types,
          static_cast<int>(std::size(fls_loops)),
          /*nin=*/1,
          /*nout=*/1,
          PyUFunc_None,
          "fls",
          "Find most significant bit in word",
          0));
}

void
init_npy(pyref m)
{
  register_functions(m, functions);
  register_constant(m, "fls", make_fls_ufunc());
}

}  // namespace dctv
