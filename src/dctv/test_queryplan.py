# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for the query planner"""
import logging

from .query import (
  BinaryOperationQuery,
  QueryNode,
  QueryTable,
)

from .queryplan import (
  make_action_graph,
  plan_query_beam_search,
  plan_query_minstar,
)

from .util import (
  Timed,
  final,
  iattr,
  load_networkx,
  override,
)

from .test_query import (
  TestQueryTable,
  _make_rng,
)

from .test_queryop import (
  GroupingQueryTable,
)

log = logging.getLogger(__name__)

@final
class FilterQueryTable(QueryTable):  # TODO(dancol): remove me!
  """Select a subset of rows in another Querytable"""

  base_table = iattr(QueryTable)
  condition = iattr(QueryNode)

  @override
  def _make_column_tuple(self):
    return self.base_table.columns

  @override
  def _make_column_query(self, column):
    return self.base_table.column(column)[self.condition]

def _torture_planner(nr_columns, nr_rows):
  rng = _make_rng()
  columns = {}
  column_names = ["col{}".format(n) for n in range(nr_columns)]
  for column_name in column_names:
    values = [rng.randint(0, 10) for _ in range(nr_rows)]
    columns[column_name] = values
  # pylint: disable=redefined-variable-type
  qt = TestQueryTable(columns=columns.values(), names=columns.keys())
  qt = GroupingQueryTable(
    base_table=qt,
    group_by=[column_names[0]],
    aggregations=[
      (column_name, "sum") for column_name in column_names
    ])
  qt = FilterQueryTable(
    qt,
    BinaryOperationQuery(qt[column_names[0]], "=",
                         QueryNode.scalar(2)))
  wanted = frozenset(qt[column_name] for column_name in column_names)
  cache = frozenset()
  action_graph = make_action_graph(wanted, cache)
  load_networkx()

  with Timed("minstar"):
    plan_query_minstar(action_graph)
  with Timed("beam"):
    plan_query_beam_search(action_graph, wanted, cache)

def test_query_planner_lesser_torture():
  """Lesser combinatoric torture test for query planner"""
  _torture_planner(6, 5)

def test_query_planner_greater_torture():
  """Greater combinatoric torture test for query planner"""
  _torture_planner(40, 5)
