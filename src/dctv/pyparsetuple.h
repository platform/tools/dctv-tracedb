// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <boost/preprocessor.hpp>
#include <limits>
#include <type_traits>

#include "meta_util.h"
#include "pyutil.h"

namespace dctv {

constexpr bool use_fast_kw_parser = true;

// The map pyarg_basic_conversions describes conversions we can
// perform by simply passing a pointer to the destination value
// directly into Python.

template<typename T, char fmt>
constexpr auto pyarg_basic_conversion =
    hana::make_pair(hana::type_c<T>, hana::char_c<fmt>);

constexpr auto pyarg_basic_conversions = hana::make_map(
    pyarg_basic_conversion<unsigned char, 'b'>,
    pyarg_basic_conversion<short, 'h'>,
    pyarg_basic_conversion<unsigned short, 'H'>,
    pyarg_basic_conversion<int, 'i'>,
    pyarg_basic_conversion<unsigned, 'I'>,
    pyarg_basic_conversion<long, 'l'>,
    pyarg_basic_conversion<unsigned long, 'L'>,
    pyarg_basic_conversion<long long, 'L'>,
    pyarg_basic_conversion<unsigned long long, 'K'>,
    pyarg_basic_conversion<char, 'c'>,
    pyarg_basic_conversion<float, 'f'>,
    pyarg_basic_conversion<double, 'd'>);

template<typename T, typename P, typename D>
auto
handle_pyarg(T*,
             P&& parse_arg,
             D&& default_,
             typename std::enable_if<
             hana::contains(pyarg_basic_conversions, hana::type_c<T>)
             >::type* dummy=0)  // NOLINT
{
  constexpr bool has_default =
      CONSTEXPR_VALUE(default_ != hana::false_c);
  T value;
  if constexpr (has_default) {
    value = default_();
  }
  return parse_arg(
      hana::make_string(pyarg_basic_conversions[hana::type_c<T>]),
      hana::make_tuple(&value),
      [&] { return value; });
}

template<typename P, typename D>
auto handle_pyarg(bool*, P&& parse_arg, D&& default_);

template<typename P, typename D>
auto handle_pyarg(std::string_view*, P&& parse_arg, D&& default_);

template<typename T, typename P, typename D>
auto handle_pyarg(obj_pyref<T>*, P&& parse_arg, D&& default_);

template<typename T, typename P, typename D>
auto handle_pyarg(unique_obj_pyref<T>*, P&& parse_arg, D&& default_);

struct py_char_code {
  int value;
  inline py_char_code& operator=(int value) noexcept;
  inline operator char();  // NOLINT
  template<typename P, typename D>
  friend auto
  handle_pyarg(py_char_code*, P&& parse_arg, D&& default_) {
    py_char_code cc;
    constexpr bool has_default =
        CONSTEXPR_VALUE(default_ != hana::false_c);
    if constexpr (has_default) {
      cc = default_();
    }
    return parse_arg(BOOST_HANA_STRING("C"),
                     hana::make_tuple(&cc.value),
                     [&] { return cc; });
  }
};

struct py_byte_view {
  const char* bytes;
  Py_ssize_t nr_bytes;
  template<typename P, typename D>
  friend auto
  handle_pyarg(py_byte_view*, P&& parse_arg, D&& default_) {
    py_byte_view bv;
    constexpr bool has_default =
        CONSTEXPR_VALUE(default_ != hana::false_c);
    if constexpr (has_default) {
      bv.bytes = nullptr;
      bv.nr_bytes = 0;
    }
    return parse_arg(BOOST_HANA_STRING("y#"),
                     hana::make_tuple(&bv.bytes, &bv.nr_bytes),
                     [&] { return bv; });
  }
  std::string_view as_string_view() const noexcept {
    assume(this->nr_bytes >= 0);
    return std::string_view(this->bytes, this->nr_bytes);
  }
};

struct pyparseargs_special {};
struct optional_args_follow final : pyparseargs_special {};
struct kwonly_args_follow final : pyparseargs_special {};

DCTV_DEFINE_TAG(no_default);

constexpr auto default_converter = [](auto&& field_type,
                                      auto&& parse_arg,
                                      auto&& default_) {
  using FieldType =
      typename std::remove_reference_t<decltype(field_type)>::type;
  FieldType* dummy_ptr = nullptr;
  return handle_pyarg(
      dummy_ptr,
      std::forward<decltype(parse_arg)>(parse_arg),
      std::forward<decltype(default_)>(default_));
};

template<typename Function>
constexpr auto make_pyobject_converter(Function&& function);

#define OPTIONAL_ARGS_FOLLOW optional_args_follow,_opt_dummy
#define KWONLY_ARGS_FOLLOW kwonly_args_follow,_kwonly_dummy

template<typename Args, typename Meta>
Args parsepyargs(Meta&& meta, pyref py_args);

template<typename Args, typename Meta>
Args parsepyargs(Meta&& meta, pyref py_args, pyref py_kwargs);

// Make a magic local function that parses Python arguments into
// variables in the scope enclosing PARSEPYARGS.
//
// ARG_SPECS is a sequence of ARG_SPEC items, where each ARG_SPEC is
// either (FIELD_TYPE, FIELD_NAME[, DEFAULT[, CONVERTER]]).  There is
// no comma between the ARG_SPEC values in ARG_SPECS: the format is
// (type1, name1)(type2, name2)(type3, name3), and so on.  FIELD_NAME
// is a bareword, not a string, but is provided to Python as a keyword
// argument name.  Each FIELD_NAME is provided in the scope of the
// PARSEPYARGS as a variable of type FIELD_TYPE.  If DEFAULT is
// provided, the field is initialized to that value in case Python
// doesn't provide it.  If CONVERT is provided, it must be a defined
// converter that signals non-default conversion.
//
// The result is PARSEPYARGS(ARG_SPECS) is a callable object.
// This object calls parsepyargs() internally when called.
// PARSEPYARGS provides the first argument to parsepyargs() magically;
// the rest of the arguments are perfect-forwarded from the invocaiton
// of PARSEPYARGS to parsepyargs().

#define PARSEPYARGS(arg_specs)                                          \
  PARSEPYARGS_INTERNAL(                                                 \
      BOOST_PP_CAT(_pyargs_, __LINE__),                                 \
      PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs))

// Like PARSEPYARGS, but instead of magically injecting the parser
// arguments into the enclosing scope, instead return a struct of
// unspecified type containing the parsed arguments as fields.
#define PARSEPYARGS_V(arg_specs)                                        \
  PARSEPYARGS_V_INTERNAL(                                               \
      BOOST_PP_CAT(_pyargs_, __LINE__),                                 \
      PARSEPYARGS_MUNGE_ARG_SPECS(arg_specs))

}  // namespace dctv

#include "pyparsetuple-inl.h"
