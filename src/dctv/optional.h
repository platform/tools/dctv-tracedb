// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#if defined(__APPLE__) && defined(__MACH__)
# include <experimental/optional>
# define DCTV_EXPERIMENTAL_OPTIONAL 1
#else
# include <optional>
# define DCTV_EXPERIMENTAL_OPTIONAL 0
#endif

#if defined(__clang__) || __GNUC__ > 7
# define DCTV_OPTIONAL_HACK 0
#else
# include <boost/optional.hpp>
# define DCTV_OPTIONAL_HACK 1
#endif

namespace dctv {

#if DCTV_EXPERIMENTAL_OPTIONAL
using std::experimental::optional;
#else
using std::optional;
#endif

// Work around GCC bug
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80654 by using boost's
// optional in cases where the compiler barfs when determining whether
// the held type is trivially copy-constructible.
#if DCTV_OPTIONAL_HACK
template<typename T>
using nocopy_optional = boost::optional<T>;
#else
template<typename T>
using nocopy_optional = optional<T>;
#endif

}  // namespace dctv
