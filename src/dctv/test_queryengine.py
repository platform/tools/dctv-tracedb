# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Query engine tests"""

# pylint: disable=missing-docstring

import logging

import pytest

from ._native import (
  QueryExecution,
)

from .query import (
  QueryAction,
)

from .util import (
  iattr,
  override,
)

log = logging.getLogger(__name__)

class DelQueryAction(QueryAction):
  fn = iattr()

  @override
  def _compute_outputs(self):
    return frozenset()

  @override
  def _compute_inputs(self):
    return ()

  @override
  async def run_async(self, qe):
    await self.fn(self, qe)

class TestError(Exception):
  pass

def test_qe_error():
  def _make_qe(fn):
    return QueryExecution.make([DelQueryAction(fn)])

  async def _exit_immediately(_self, _qe):
    pass
  with pytest.raises(RuntimeError) as ex:
    _make_qe(_exit_immediately)
  assert "did not call setup" in str(ex)

  async def _fail_before_setup(_self, qe):
    raise TestError
  with pytest.raises(TestError):
    _make_qe(_fail_before_setup)

  async def _succeed_during_execution(_self, qe):
    _, _ = await qe.async_setup([], [])
    await qe.async_yield_to_query_runner(5)
  assert next(_make_qe(_succeed_during_execution)) == 5

  async def _success_after_fail(_self, qe):
    _, _ = await qe.async_setup([], [])
    blarg = qe.async_setup([], [])
    with pytest.raises(RuntimeError) as ex:
      await blarg
    assert "cannot run setup" in str(ex)
    await qe.async_yield_to_query_runner(10)
  assert next(_make_qe(_success_after_fail)) == 10
