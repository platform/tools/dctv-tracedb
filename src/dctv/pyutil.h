// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <exception>
#include <stddef.h>
#include <type_traits>
#include <utility>

#define PY_SSIZE_T_CLEAN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <Python.h>
#include <frameobject.h>
#include <structmember.h>
#pragma GCC diagnostic pop

namespace dctv {

template<typename T> struct unique_obj_pyref;
template<typename T> struct obj_pyref;
template<typename T> struct pytype_traits;
template<typename T> struct PyRefSpan;

template<typename From, typename To>
constexpr bool is_safe_pycast() {
  return std::is_same_v<From, To> || std::is_same_v<To, PyObject>;
}

template<typename From, typename To>
inline void typecheck_cast(obj_pyref<From> obj)
    noexcept(is_safe_pycast<From, To>());

using unique_pyref = unique_obj_pyref<PyObject>;
using pyref = obj_pyref<PyObject>;

void check_pytype(obj_pyref<PyTypeObject> type, pyref obj);
bool isinstance(pyref obj, obj_pyref<PyTypeObject> type) noexcept;

template<typename T>
struct DCTV_TRIVIAL_ABI unique_obj_pyref final {
  // Make a unique_obj_pyref pointing to nullptr.
  inline constexpr unique_obj_pyref() noexcept;

  // Release any contained reference.
  inline ~unique_obj_pyref() noexcept;

  // Use explicit addref operations to make copies.
  unique_obj_pyref(const unique_obj_pyref& other) = delete;
  unique_obj_pyref& operator=(const unique_obj_pyref&& other) = delete;

  // Moving works
  inline unique_obj_pyref(unique_obj_pyref&& other) noexcept;
  inline unique_obj_pyref& operator=(unique_obj_pyref&& other) noexcept;

  // Automatic move from a unique_obj_pyref of a different type if
  // know statically that the type conversion always succeeds.
  template<typename R>
  unique_obj_pyref(  // NOLINT
      unique_obj_pyref<R>&& other,
      typename std::enable_if_t<is_safe_pycast<R, T>()>* = 0) noexcept
      : unique_obj_pyref(other.template addref_as<T>())
  {}

  template<typename R>
  // NOLINTNEXTLINE
  typename std::enable_if_t<is_safe_pycast<R, T>(), unique_obj_pyref&>
  operator=(unique_obj_pyref<R>&& other) noexcept {
    if constexpr(std::is_same_v<R, T>) {
      if (&other == this)
        return *this;
    }
    return (*this = other.template addref_as<T>());
  }

  // Like other smart pointers.
  inline T* get() const noexcept;
  inline T& operator*() const noexcept;
  inline T* operator->() const noexcept;
  inline T* release() noexcept;
  inline void reset() noexcept;
  inline explicit operator bool() const noexcept;

  // Make a new reference to the underlying object,
  // propagating nullptr.
  inline unique_obj_pyref addref() const & noexcept;
  inline unique_obj_pyref addref() && noexcept;

  // Assume ownership of a raw Python object pointer.  Unsafe!
  inline static unique_obj_pyref adopt(T* obj) noexcept;

  // Return a reference to the contained object as the given type,
  // performing no type checking.
  template<typename R>
  inline obj_pyref<R> as_unsafe() const noexcept;

  // Return a reference as a given type, type-checking at runtime.
  // nullptr is propagated unchecked.
  template<typename R>
  inline obj_pyref<R> as() const noexcept(is_safe_pycast<T, R>());

  // Return a new reference to the pointed-to object, but without
  // doing any runtime type checking.
  template<typename R>
  inline unique_obj_pyref<R> addref_as_unsafe() const & noexcept;
  template<typename R>
  inline unique_obj_pyref<R> addref_as_unsafe() && noexcept;

  // Return a new reference to the pointed-to object, checking at
  // runtime that the pointed-to object has the correct type.
  // Propagates nullptr without type checking.
  template<typename R>
  inline unique_obj_pyref<R> addref_as() const &
      noexcept(is_safe_pycast<T, R>());
  template<typename R>
  inline unique_obj_pyref<R> addref_as() &&
      noexcept(is_safe_pycast<T, R>());

  // Make BasePyObject-derived object instances
  template<typename... Args>
  static unique_obj_pyref make(obj_pyref<PyTypeObject> type,
                               Args&&... args);

  // Get the offset from the start of the structure to a Python object
  // pointer.  Useful for Python member definitions.
  static constexpr size_t get_pyval_offset();

  // Nothrow swap: define inline to avoid friend template weirdness
  friend inline void swap(unique_obj_pyref& left,
                          unique_obj_pyref& right) noexcept
  { T* tmp = left.ref; left.ref = right.ref; right.ref = tmp; }

  // Return a view of this reference as a single-element PyRefSpan.
  // This function is actually defined via pyrefspan.h.
  inline PyRefSpan<T> as_span() const noexcept;

  // Assert that the value is not nullptr and return the
  // original object.
  inline unique_obj_pyref& notnull() & noexcept;
  inline unique_obj_pyref&& notnull() && noexcept;
  inline unique_obj_pyref const& notnull() const& noexcept;

 private:
  inline explicit unique_obj_pyref(T* obj) noexcept;

  template<typename R>
  inline unique_obj_pyref<R> move_as()
      noexcept(is_safe_pycast<T, R>());

  template<typename R>
  inline unique_obj_pyref<R> move_as_unsafe() noexcept;

  T* ref;
};

template<typename T>
struct DCTV_TRIVIAL_ABI obj_pyref final {
  inline constexpr obj_pyref() noexcept;

  template<typename R>
  obj_pyref(  // NOLINT
      const R* obj,
      typename std::enable_if_t<is_safe_pycast<R, T>()>* = 0) noexcept
      : ref(reinterpret_cast<T*>(const_cast<R*>(obj)))
  {}

  template<typename R>
  obj_pyref(  // NOLINT
      const unique_obj_pyref<R>& ref,
      typename std::enable_if_t<is_safe_pycast<R, T>()>* = 0) noexcept
      : ref(reinterpret_cast<T*>(ref.get()))
  {}

  template<typename R>
  obj_pyref(  // NOLINT
      const obj_pyref<R>& ref,
      typename std::enable_if_t<is_safe_pycast<R, T>()>* = 0) noexcept
      : ref(reinterpret_cast<T*>(ref.get()))
  {}

  inline explicit operator bool() const noexcept;

  inline T* get() const noexcept;
  inline T& operator*() const noexcept;
  inline T* operator->() const noexcept;

  inline unique_obj_pyref<T> addref() const noexcept;

  template<typename R>
  inline obj_pyref<R> as_unsafe() const noexcept;

  template<typename R>
  inline obj_pyref<R> as() const noexcept(is_safe_pycast<T, R>());

  template<typename R>
  inline unique_obj_pyref<R> addref_as_unsafe() const noexcept;

  template<typename R>
  inline unique_obj_pyref<R> addref_as() const
      noexcept(is_safe_pycast<T, R>());

  inline PyRefSpan<T> as_span() const noexcept;
  inline obj_pyref& notnull() & noexcept;
  inline obj_pyref&& notnull() && noexcept;
  inline obj_pyref const& notnull() const& noexcept;
 private:
  T* ref;
};

inline bool operator==(pyref left, pyref right) noexcept;
inline bool operator!=(pyref left, pyref right) noexcept;
template<typename T> obj_pyref(T*) -> obj_pyref<T>;
template<typename T> obj_pyref(const unique_obj_pyref<T>&)
    -> obj_pyref<T>;

template<typename T, typename... Args>
inline unique_obj_pyref<T> make_pyobj(Args&&... args);

// Base class for C++/Python interop objects.
struct BasePyObject : NoCopy, NoMove {
  PyObject_HEAD;
};

// Make a BasePyObject child derive from this class autogenerate a
// tp_new that calls the class Python constructor, which we define to
// be the one with signature unique_pyref (pyref args, pyref kwds).
// You can always just make a named static constructor instead.
struct HasPyCtor {};

// Make a BasePyObject child derive from this class as well to make
// the object support Python's tracing GC protocol.  We don't have a
// vtable, so we can't have a real abstract method, but conceptually,
// each object must implement a int (T::py_traverse)(visitproc vp,
// void* arg) noexcept as described in the Python documentation.
// The glue code takes care of setting flags, wiring up memory, and
// so on.
struct SupportsGc {};

// Like SupportsGc, but indicate that we also have support for
// tp_clear.
struct SupportsGcClear : SupportsGc {};

// Mixin for objects supporting a custom dictionary If the object
// supports GC, the GC procedure must visit delegate to traverse and
// (if applicable) clear.
struct HasDict {
  unique_pyref py_dict;
  int py_traverse(visitproc visit, void* arg) const noexcept;
  int py_clear() noexcept;
};

// Mixin for supporting repr via string coercion
struct HasRepr {};

// Mixin for supporting weak references
struct SupportsWeakRefs {
  PyObject* py_weakref_list = nullptr;
  ~SupportsWeakRefs();
};

// Trait support that lets C++ know what the PyTypeObject should be
// for each C++ object type.
struct pytype_enable {};
template<typename T>
struct pytype_traits final
    : std::enable_if_t<std::is_base_of_v<BasePyObject, T>, pytype_enable>
{
  static constexpr bool exists = true;
  static constexpr PyTypeObject* get_pytype() {
    return &T::pytype;
  }
};

#define DEFINE_PYTRAITS_FULL(cxx_type, type_object, tag)        \
  template<>                                                    \
  struct pytype_traits<cxx_type> final                          \
  {                                                             \
    static constexpr bool exists = true;                        \
    tag PyTypeObject* get_pytype() {                            \
      return &(type_object);                                    \
    }                                                           \
  }

template<typename T>
inline constexpr PyTypeObject* pytype_of();

// Define an association between a C++ type and a Python type object
#define DEFINE_PYTRAITS(cxx_type, type_object)                  \
  DEFINE_PYTRAITS_FULL(cxx_type, type_object, static constexpr)

// Need to look up numpy type objects at runtime
#define DEFINE_PYTRAITS_NO_CONSTEXPR(cxx_type, type_object)     \
  DEFINE_PYTRAITS_FULL(cxx_type, type_object, static)

DEFINE_PYTRAITS(PyObject, PyBaseObject_Type);
DEFINE_PYTRAITS(PyTypeObject, PyType_Type);
DEFINE_PYTRAITS(PyDictObject, PyDict_Type);
DEFINE_PYTRAITS(PyCoroObject, PyCoro_Type);
DEFINE_PYTRAITS(PyGenObject, PyGen_Type);
DEFINE_PYTRAITS(PyFrameObject, PyFrame_Type);
DEFINE_PYTRAITS(PyWeakReference, _PyWeakref_RefType);
DEFINE_PYTRAITS(PySliceObject, PySlice_Type);

inline void check_pytype_exact(obj_pyref<PyTypeObject> type, pyref obj);
inline bool isinstance_exact(pyref obj, obj_pyref<PyTypeObject> type)
    noexcept;

// In the functions below, "adopt" means to acquire ownership of a raw
// Python pointer.  *_as<T> means to make the returned PYthon
// reference type T instead of PyObject.  _as_unsafe<T> means the
// same, but without a runtime type check.  _check means to throw if
// the input is nullptr; non-_check variants just propagate nulls into
// empty Python object references.

inline unique_pyref adopt(PyObject* obj) noexcept;
inline unique_pyref adopt_check(PyObject* obj);

template<typename T>
inline unique_obj_pyref<T> adopt_as(PyObject* obj) noexcept;

template<typename T>
inline unique_obj_pyref<T> adopt_check_as(PyObject* obj);

template<typename T>
inline unique_obj_pyref<T> adopt_as_unsafe(PyObject* obj) noexcept;

template<typename T>
inline unique_obj_pyref<T> adopt_check_as_unsafe(PyObject* obj);

template<typename T>
inline unique_obj_pyref<T> adopt_check_as(T* obj);

template<typename T>
inline unique_obj_pyref<T> adopt_as_unsafe(T* obj) noexcept;

template<typename T>
inline unique_obj_pyref<T> adopt_check_as_unsafe(T* obj);

inline unique_pyref addref(pyref obj) noexcept;

template<typename T>
inline unique_pyref addref(unique_obj_pyref<T>&& obj) noexcept;

// Make a properly-typed unique_obj_pyref from a pointer.
// Convenient in member functions for use with `this'.
template<typename T>
inline
unique_obj_pyref<T> xaddref(
    const T* obj,
    typename std::enable_if_t<pytype_traits<T>::exists>* = 0)
    noexcept;

// Like xaddref, but doesn't do any type checking and works on
// incomplete types.
template<typename T>
inline unique_obj_pyref<T> xaddref_unsafe(const T* obj) noexcept;

// String utilities

using unique_pystr = unique_obj_pyref<PyUnicodeObject>;
using pystr_ref = obj_pyref<PyUnicodeObject>;
using unique_pybytes = unique_obj_pyref<PyBytesObject>;
using pybytes_ref = obj_pyref<PyBytesObject>;

std::string_view pystr_to_string_view(pyref str);
const char* pystr_to_c_string(pyref str);
String pystr_to_string(pyref str);
String repr(pyref thing);
String repr(std::string_view str);
String str(pyref thing);
unique_pystr make_pystr(std::string_view s, const char* errors="strict");
inline unique_pystr make_pystr(char c, const char* errors="strict");
unique_pystr make_pystr_fmt(const char* format, ...);

// Generic object utilities
unique_pyref getattr(pyref obj, const char* attr_name);
unique_pyref getattr(pyref obj, _Py_Identifier* interned);
bool true_(pyref object);
bool not_(pyref object);
inline obj_pyref<PyTypeObject> pytype(pyref obj) noexcept;
inline Py_ssize_t refcount(pyref obj) noexcept;

void pywarn(obj_pyref<PyTypeObject> category /*nullable*/,
            const char* msg,
            Py_ssize_t stack_level);

DEFINE_PYTRAITS(PyUnicodeObject, PyUnicode_Type);
DEFINE_PYTRAITS(PyBytesObject, PyBytes_Type);

// Calling functions and methods

// Convenient call wrapper
template<typename... Args>
inline unique_pyref call(pyref callable, Args&&... args);

enum class GenRet {
  YIELDED,
  RETURNED,
};

// Like call(), but look for a StopIteration exception from the
// callable and capture it efficiently as a special case.
template<typename... Args>
std::pair<GenRet, unique_pyref> call_gen(pyref callable, Args&&... args);

// Look at the pending Python exception: if it's a StopIteration,
// clear the error state and return the value stapled to the
// StopIteration (e.g., a coroutine return value).  If we have any
// other kind of Python exception pending, translate it to a C++
// exception and throw.
unique_pyref fetch_stop_iteration_value();

// Module and type utilities
void register_type(pyref m, PyTypeObject* type);
void register_constant(pyref m, const char* name, long value);
void register_constant(pyref m, const char* name, unique_pyref value);
void register_functions(pyref m, PyMethodDef* methods);

unique_pyref import_pymodule(std::string_view name);
unique_obj_pyref<PyTypeObject> find_pyclass(std::string_view module_name,
                                            const char* class_name);

// Sequence utilities
Py_ssize_t pyseq_size(pyref seq);
void set_slice(pyref assignee, Py_ssize_t i1, Py_ssize_t i2, pyref value);
unique_pyref get_slice(pyref obj, Py_ssize_t i1, Py_ssize_t i2);
unique_pyref get_item(pyref obj, Py_ssize_t index);
unique_pyref get_item(pyref obj, pyref item);

unique_obj_pyref<PySliceObject> make_sliceobj(pyref start = pyref(),
                                              pyref stop = pyref(),
                                              pyref step = pyref());

// Dict utilities
unique_obj_pyref<PyDictObject> make_pydict();

// Operators
bool py_lt(pyref o1, pyref o2);
bool py_le(pyref o1, pyref o2);
bool py_eq(pyref o1, pyref o2);
bool py_ne(pyref o1, pyref o2);
bool py_gt(pyref o1, pyref o2);
bool py_ge(pyref o1, pyref o2);
unique_pyref py_xlt(pyref o1, pyref o2);
unique_pyref py_xle(pyref o1, pyref o2);
unique_pyref py_xeq(pyref o1, pyref o2);
unique_pyref py_xne(pyref o1, pyref o2);
unique_pyref py_xgt(pyref o1, pyref o2);
unique_pyref py_xge(pyref o1, pyref o2);

// Integer utilities
// N.B. we need all big integral types so as to avoid overload ambiguity.
inline unique_pyref make_pylong(int value);
inline unique_pyref make_pylong(unsigned int value);
unique_pyref make_pylong(long value);
unique_pyref make_pylong(unsigned long value);
unique_pyref make_pylong(long long value);
unique_pyref make_pylong(unsigned long long value);
template<typename T> extern T from_py(pyref value);

unique_pyref make_pyfloat(double value);

inline unique_pyref make_pybool(bool value) noexcept;

unique_pyref pyiter_for(pyref obj);
unique_pyref pyiter_next(pyref iter);  // Returns empty ref on exhaustion

// Common error-throwing functions.  Actually defined in pyerr.cpp,
// but declared here so more modules can call them.

DCTV_NORETURN_ERROR
void throw_current_pyerr();

DCTV_NORETURN_ERROR
void throw_pyerr_msg(pyref cls, const char* msg);

DCTV_NORETURN_ERROR
void throw_pyerr_fmtv(pyref cls, const char* fmt, va_list args);

// For when we don't want helpful GCC format string checking because
// we're using a Python-specific bit of formatting.
DCTV_NORETURN_ERROR
void _throw_pyerr_fmt(pyref cls, const char* fmt, ...);

DCTV_NORETURN_ERROR
void throw_from_errno();

DCTV_NORETURN_ERROR
void throw_from_errno_with_filename(const char* filename);

DCTV_NORETURN_ERROR
void throw_wrong_type_error(pyref obj,
                            obj_pyref<PyTypeObject> expected_type);

// This function is just here to tell the compiler that checking for
// Python exceptions doesn't clobber global memory.  If we called
// PyErr_Occurred() directly, the compiler would have to make
// pessimistic assumptions; the pure annotation promises that
// pyerr_occurred() doesn't change global memory.
__attribute__((pure))
bool pyerr_occurred() noexcept;

template<typename T>
struct pyref_traits final {
  static constexpr bool is_pyref = false;
  static constexpr bool is_owner = false;
  using RefType = void;
};

template<typename T>
struct pyref_traits<obj_pyref<T>> final {
  static constexpr bool is_pyref = true;
  static constexpr bool is_owner = false;
  using RefType = T;
};

template<typename T>
struct pyref_traits<unique_obj_pyref<T>> final {
  static constexpr bool is_pyref = true;
  static constexpr bool is_owner = true;
  using RefType = T;
};

template<typename T>
inline T mul_raise_on_overflow(T a, T b);

template<typename T>
inline T mul_raise_on_overflow_safe_only(T a, T b)
    noexcept(!safe_mode);

template<typename T>
inline T add_raise_on_overflow(T a, T b);

pyref py_wr_dereference(obj_pyref<PyWeakReference> wr) noexcept;

__attribute__((const)) inline bool is_py_debug() noexcept;

inline void init_pyutil(pyref m) {}

}  // namespace dctv

#include "pyutil-inl.h"
