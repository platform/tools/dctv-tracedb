// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "output_channel.h"

#include <functional>
#include <tuple>
#include <utility>

#include "fmt.h"
#include "input_channel.h"
#include "io_spec.h"
#include "operator_context.h"
#include "pylog.h"
#include "pyobj.h"
#include "pyparsetuple.h"

namespace dctv {

using namespace std::placeholders;
using std::bind;
using std::tie;

template<>
unique_obj_pyref<OutputChannelSpec>
PythonConstructor<OutputChannelSpec>::make(
    PyTypeObject* type,
    pyref args,
    pyref kwargs)
{
  PARSEPYARGS(
      (pyref, query)
      (OPTIONAL_ARGS_FOLLOW)
      (KWONLY_ARGS_FOLLOW)
      (bool, strict_backpressure, false)
  )(args, kwargs);
  auto spec = unique_obj_pyref<OutputChannelSpec>::make(
      type, QueryKey::from_py(query));
  spec->strict_backpressure = strict_backpressure;
  return spec;
}

unique_obj_pyref<OutputChannelSpec>
OutputChannelSpec::from_py(pyref py_spec)
{
  if (isinstance_exact(py_spec, &OutputChannelSpec::pytype))
    return py_spec.addref_as_unsafe<OutputChannelSpec>();
  return make_pyobj<OutputChannelSpec>(QueryKey::from_py(py_spec));
}

OutputChannelSpec::OutputChannelSpec(QueryKey query)
    : query(std::move(query))
{}

int
OutputChannelSpec::py_traverse(visitproc visit, void* arg) const noexcept
{
  if (int ret = this->query.py_traverse(visit, arg))
    return ret;
  return 0;
}

OutputChannel::OutputChannel(OperatorContext* oc,
                             QueryCache* qc,
                             const OutputChannelSpec& spec)
    : query(spec.query),
      oc(oc),
      block_builder(qc,
                    this->query.get_dtype().notnull().addref(),
                    bind(&OutputChannel::on_block_produced, this, _1)),
      strict_backpressure(spec.strict_backpressure),
      dynamic_block_size(this->strict_backpressure),
      block_size_dirty(this->dynamic_block_size)
{
  if (this->block_size_dirty)
    oc->queue_pending_block_size_change();
}

OutputChannel::~OutputChannel() noexcept
{
  this->close();
}

void
OutputChannel::on_block_produced(unique_obj_pyref<Block> block)
{
  for (InputChannel* sink : this->sinks)
    sink->add_block(block.get());
}

void
OutputChannel::remove_sink(const InputChannel* input_channel) noexcept
{
  // We can fail to find the iterator if we die during setup.
  auto it = std::find(this->sinks.begin(), this->sinks.end(), input_channel);
  if (it != this->sinks.end()) {
    this->sinks.erase(it);
    if (this->oc) {
      this->oc->invalidate_score();
      if (this->sinks.empty()) {
        // Must be last: may close and deallocate *this.
        this->oc->on_output_channel_disconnected();
      }
    }
  }
}

void
OutputChannel::add_sink(InputChannel* sink)
{
  assume(sink->get_source() == this);
  this->sinks.push_back(sink);
}

std::pair<npy_intp, npy_intp>
OutputChannel::compute_correct_block_size() const
{
  npy_intp smallest_read_wanted = -1;
  for (const auto& sink : this->sinks) {
    npy_intp read_request = sink->get_read_wanted_hint();
    if (read_request > 0 &&
        (smallest_read_wanted < 0 ||
         read_request < smallest_read_wanted))
      smallest_read_wanted = read_request;
  }

  // In strict backpressure mode, we crank the block size down to the
  // smallest anyone currently wants to read.  In normal mode, we use
  // the query execution block size as a floor.
  npy_intp block_size = this->oc->get_block_size();
  if (this->strict_backpressure &&
      smallest_read_wanted > 0 &&
      smallest_read_wanted < block_size)
    block_size = smallest_read_wanted;

  npy_intp maximum_block_size =
      std::max(block_size, smallest_read_wanted);

  return {block_size, maximum_block_size};
}

void
OutputChannel::resize_buffer_now()
{
  if (!this->dynamic_block_size) {
    assume(!this->block_size_dirty);
    assume(!this->strict_backpressure);
  }
  if (this->oc && this->block_size_dirty) {
    assume(this->dynamic_block_size);
    npy_intp block_size, maximum_block_size;
    tie(block_size, maximum_block_size) =
        this->compute_correct_block_size();
    // N.B. the set_block_size() below may call
    // this->on_block_produced().
    this->block_builder.set_block_size(block_size, maximum_block_size);
    this->block_size_dirty = false;
  }
}

void
OutputChannel::add_data(pyref thing, bool is_eof)
{
  if (this->oc) {
    // We should have been resized via QeCallResizeBuffers.
    assume(!this->block_size_dirty);
    this->block_builder.add(thing, is_eof);
    this->oc->invalidate_score();
  }
}

void
OutputChannel::flush()
{
  this->add_data(pyref(), /*is_eof=*/true);
}

bool
OutputChannel::has_backpressure() const noexcept
{
  // Normally, we don't exert backpressure on an OutputChannel (i.e.,
  // source) until at least one InputChannel's (i.e., sink's) input
  // queue has some stuff in it, even if nobody is reading the
  // InputChannels.  In strict backpressure mode, we wait for *actual
  // reads* to be in progress on all sinks before we turn off the
  // backpressure on this OutputChannel.
  return std::any_of(this->sinks.begin(), this->sinks.end(),
                     [&](const auto& sink) {
                       return sink->has_backpressure() ||
                           (this->strict_backpressure
                            && sink->get_read_wanted_hint() < 0);
                     });
}

bool
OutputChannel::has_flush_backpressure() const noexcept
{
  if (!this->needs_flush())
    return false;
  return this->has_backpressure();
}

bool
OutputChannel::has_buffer_resize_backpressure() const noexcept
{
  if (!this->dynamic_block_size)
    return false;
  npy_intp block_size, maximum_block_size;
  tie(block_size, maximum_block_size) =
      this->compute_correct_block_size();
  if (this->block_builder.get_partial_size() < block_size)
    return false;
  return this->has_backpressure();
}

void
OutputChannel::on_backpressure_change()
{
  if (this->oc) {
    this->oc->invalidate_score();
    if (this->dynamic_block_size && !this->block_size_dirty) {
      this->block_size_dirty = true;
      this->oc->queue_pending_block_size_change();
    }
  }
}

void
OutputChannel::close() noexcept
{
  if (this->oc) {
    for (auto& sink : this->sinks)
      sink->on_source_dead();
    this->sinks.clear();
    this->oc = nullptr;
  }
}

int
OutputChannel::py_traverse(visitproc visit, void* arg) const noexcept
{
  if (int ret = this->query.py_traverse(visit, arg); ret)
    return ret;
  if (int ret = this->block_builder.py_traverse(visit, arg); ret)
    return ret;
  return 0;
}

OutputChannel::operator String() const
{
  return fmt("<OutputChannel q=%s off=%s #s=%s eof=%s>",
             repr(this->query.as_pyref()),
             this->block_builder.get_partial_size(),
             this->sinks.size(),
             this->block_builder.is_eof());
}

unique_pyref
OutputChannel::py_write(pyref args) const
{
  PARSEPYARGS(
      (pyref, data)
      (OPTIONAL_ARGS_FOLLOW)
      (bool, eof, false)
  )(args);
  return make_qe_call_io_single(
      eof
      ? IoSpec(IoOutputEof(xaddref(this), addref(data)))
      : IoSpec(IoOutput(xaddref(this), addref(data))));
}



PyTypeObject OutputChannelSpec::pytype =
    make_py_type<OutputChannelSpec>(
        "dctv._native.OutputChannelSpec",
        "Specification for output channels");



PyMethodDef OutputChannel::pymethods[] = {
  make_methoddef("write",
                 wraperr<&OutputChannel::py_write>(),
                 METH_VARARGS,
                 "Write data"),
  { 0 },
};

PyMemberDef OutputChannel::pymembers[] = {
  make_memberdef("query",
                 T_OBJECT,
                 (offsetof(OutputChannel, query)
                  + QueryKey::get_query_node_offset()),
                 READONLY,
                 "QueryNode that this channel consumes"),
  make_memberdef("dtype",
                 T_OBJECT,
                 (offsetof(OutputChannel, block_builder) +
                  BlockBuilder::get_dtype_offset()),
                 READONLY,
                 "Numpy dtype that this output channel consumes"),
  { 0 },
};

PyTypeObject OutputChannel::pytype =
    make_py_type<OutputChannel>(
        "dctv._native.OutputChannel",
        "Channel for producing query results",
        [](PyTypeObject* t) {
          t->tp_methods = OutputChannel::pymethods;
          t->tp_members = OutputChannel::pymembers;
        });

void
init_output_channel(pyref m)
{
  register_type(m, &OutputChannelSpec::pytype);
  register_type(m, &OutputChannel::pytype);
}

}  // namespace dctv
