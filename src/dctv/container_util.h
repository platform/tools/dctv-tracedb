// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

namespace dctv {

template<typename Mapping, typename Key>
constexpr bool map_contains(const Mapping& mapping, const Key& key) noexcept;

template<typename Sequence, typename Key>
constexpr bool seq_contains(const Sequence& sequence, const Key& key) noexcept;

template<typename Mapping, typename Key>
inline typename Mapping::mapped_type* map_try_get(
    Mapping& mapping, const Key& key) noexcept;

template<typename Sequence, typename Key>
constexpr typename Sequence::difference_type
index_of(const Sequence& sequence, const Key& key);

}  // namespace dctv

#include "container_util-inl.h"
