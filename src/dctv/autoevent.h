// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"  // NOLINT

#include <algorithm>
#include <functional>
#include <queue>
#include <tuple>
#include <type_traits>

#include "container_util.h"
#include "hash_table.h"
#include "meta_util.h"
#include "npyiter.h"
#include "optional.h"
#include "pyutil.h"
#include "query.h"
#include "simple_variant.h"
#include "span.h"
#include "vector.h"

namespace dctv {
namespace auto_event {

// Top-level macros

#define AUTOEVENT_DECLARE_EVENT(struct_name, field_specs)               \
  AUTOEVENT_DECLARE_EVENT_1(                                            \
      struct_name,                                                      \
      AUTOEVENT_MUNGE_EVENT_FIELD_SPECS(field_specs))

// Input with a number of sources.

constexpr bool check_source_invariants = safe_mode;

template<typename Payload,
         typename SourceNumber = std::size_t,
         typename Container,
         typename Callback,
         typename... Options>
auto multi_input(const Container& iterators,
                 Callback callback,
                 Options&&... options);

// Input with one source.

template<typename Payload,
         typename Callback,
         typename... Options>
auto input(pyref iterator,
           Callback callback,
           Options&&... options);

// Event we enqueue ourselves.

template<typename Payload, typename Callback>
auto synthetic_event(Callback callback);

// Top-level ae::process() options.

template<typename Callback>
auto before_dispatch_hook(Callback callback);

template<typename Callback>
auto ts_batch_end_hook(Callback callback);

template<typename Callback>
auto partition_end_hook(Callback callback);

DCTV_DEFINE_TAG(partition_major_mode);

// Event options

DCTV_DEFINE_TAG(add_index_argument);
DCTV_DEFINE_TAG(eager_slurp);

template<typename Condition>
auto enable_when(Condition condition);

template<typename Tiebreaker>
auto ordering_tiebreaker(Tiebreaker tiebreaker);

// API

template<typename Ae, typename Payload>
void enqueue_event(Ae&& ae, TimeStamp ts, Payload payload);

template<typename Ae, typename Predicate>
bool dispatch_from_pending_events(Ae&& ae, Predicate&& predicate);

template<typename Ae>
auto is_partition_major_mode(Ae&& ae);

template<typename... Args>
void process(Args&&... args);


// Event field annotations.

DCTV_DEFINE_TAG(span_duration);
DCTV_DEFINE_TAG(partition);
DCTV_DEFINE_TAG(no_annotation);

}  // namespace auto_event

void init_autoevent(pyref m);
}  // namespace dctv

#include "autoevent-inl.h"
