// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "string_table.h"

#include <array>

#include "automethod.h"
#include "npy.h"
#include "npyiter.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "pyseq.h"
#include "pythread.h"
#include "query.h"
#include "vector.h"

namespace dctv {

using std::string_view;

template<typename Left, typename Right, typename Out, typename Functor>
static
unique_pyarray
do_npy_binop(QueryExecution* qe,
             pyref left,
             pyref right,
             Functor&& functor)
{
  constexpr int nop = 2;
  std::array<PyArrayObject*, nop> arrays = {
    left.as<PyArrayObject>().notnull().get(),
    right.as<PyArrayObject>().notnull().get(),
  };
  std::array<unique_dtype, nop> dtypes = {
    type_descr<Left>(),
    type_descr<Right>(),
  };
  std::array<PyArray_Descr*, nop> dtype_ptrs = {
    dtypes[0].get(),
    dtypes[1].get(),
  };
  auto iter = NpyIteration<nop, nop>(
      PyRefSpan<PyArrayObject>(arrays.data(), nop),
      PyRefSpan<PyArray_Descr>(dtype_ptrs.data(), nop));
  if (iter.is_at_eof())
    return make_empty_pyarray(type_descr<Out>());
  unique_pyarray out =
      make_uninit_hunk_array(qe,
                             type_descr<Out>(),
                             iter.size());
  Out* restrict out_data = npy_data<Out>(out);
  do {
    *out_data++ = functor(iter.get<Left>(0), iter.get<Right>(1));
  } while (iter.advance());
  return out;
}

StringTable::StringTable()
{
  // Always intern the empty string first so that it gets
  // ID 0, simplifying some other code.
  this->intern({});
}

StringTable::~StringTable() noexcept
{
  while (!this->lookup_caches.empty()) {
    auto& cache = this->lookup_caches.front();
    assert(cache.st == this);
    cache.clear();
    cache.st = nullptr;
    this->lookup_caches.pop_front();
  }
  this->lookup_caches.clear();
}

StringTable::StringTable(pyref args, pyref kwds)
    : StringTable()
{
  PARSEPYARGS()(args, kwds);
}

StringTable::id_type
StringTable::allocate_id()
{
  // TODO(dancol): switch to free list
  assert(this->c2s.size() == this->free_slots.size());
  id_type id = this->next_free_id;
  assume(id >= 0);
  assume(id < std::numeric_limits<id_type>::max());
  if (static_cast<size_t>(id) >= this->c2s.size()) {
    this->c2s.resize(id + 1);
    this->free_slots.resize(id + 1, /*value=*/true);
  }
  assert(this->free_slots[id]);
  this->free_slots[id] = false;
  size_t next_idx = this->free_slots.find_next(id + 1);
  if (next_idx == this->free_slots.npos)  // NOLINT
    next_idx = this->c2s.size();
  if (next_idx >= std::numeric_limits<id_type>::max()) {
    with_gil_acquired([&]{
      throw_pyerr_msg(PyExc_OverflowError,
                      "too many strings in table");
    });
  }
  this->next_free_id = static_cast<id_type>(next_idx);
  ++this->seqno;
  if (!this->rank_cache.empty() || this->matcher_cache.has_value()) {
    with_gil_acquired([&] {
      this->rank_cache.clear();
      this->matcher_cache.reset();
    });
  }
  return id;
}

StringTable::id_type
StringTable::intern(string_view key)
{
  auto it = this->s2c.find(key);
  if (it == this->s2c.end()) {
    size_t id = this->allocate_id();
    assert(id < this->c2s.size());
    this->c2s[id] = StringTableEntry(key);
    auto [inserted_it, inserted] =
        this->s2c.insert(std::pair(this->c2s[id].view(), id));
    assume(inserted);
    it = inserted_it;
  }
  return it->second;
}

optional<StringTable::id_type>
StringTable::intern_soft(string_view key)
{
  auto it = this->s2c.find(key);
  if (it == this->s2c.end())
    return {};
  return {it->second};
}

size_t
StringTable::size() const
{
  return this->c2s.size();
}

const StringTableEntry&
StringTable::lookup(id_type id) const
{
  assert(0 <= id && static_cast<size_t>(id) < this->size());
  return this->c2s[id];
}

StringTableEntry::StringTableEntry(string_view contents)
{
  this->data = new char[contents.size() + 1 /* cannot overflow */];
  this->size = contents.size();
  memcpy(this->data, contents.data(), contents.size());
  this->data[contents.size()] = '\0';
}

StringTableEntry::~StringTableEntry()
{
  delete[] this->data;
}

StringTableEntry::StringTableEntry(StringTableEntry&& other) noexcept
{
  this->data = other.data;
  this->size = other.size;
  other.data = nullptr;
  other.size = 0;
}

StringTableEntry&
StringTableEntry::operator=(StringTableEntry&& other) noexcept
{
  if (this != &other) {
    this->~StringTableEntry();
    new (this) StringTableEntry(std::move(other));
  }
  return *this;
}

const char*
StringTableEntry::c_str() const
{
  assert(this->data);
  return this->data;
}

unique_pyarray
StringTable::rank_1(Comparator c) const
{
  auto fill_it = boost::counting_iterator<id_type>(0);
  Vector<id_type> ids(fill_it, fill_it + this->size());
  auto comp_wrapper = [&](id_type left_id, id_type right_id){
    return c.is_lt(this->lookup(left_id).view(),
                   this->lookup(right_id).view());
  };
  std::sort(ids.begin(), ids.end(), comp_wrapper);
  unique_pyarray ranks_array = make_uninit_pyarray(
      type_descr<Rank>(), ids.size());
  assume(npy_ndim(ranks_array) == 1);
  assume(npy_strides(ranks_array)[0] == sizeof (Rank));
  Rank* restrict ranks = npy_data<Rank>(ranks_array);
  assume(ids.size() <= std::numeric_limits<Rank>::max());
  int64_t rank = 0;
  for (size_t idx = 0; idx < ids.size(); ++idx) {
    ranks[ids[idx]] = rank;
    // N.B. Under a collations, strings with different IDs (which
    // correspond to differ net bitwise spellings) can compare equal.
    // We want to assign these equal-under-collation strings the
    // same rank.
    bool next_is_unequal_under_collation =
        idx + 1 < ids.size() &&
        c.is_lt(this->lookup(ids[idx + 0]).view(),
                this->lookup(ids[idx + 1]).view());
    if (next_is_unequal_under_collation) {
      ++rank;
    } else {
      assert(idx + 1 == ids.size() ||
             !c.is_lt(this->lookup(ids[idx + 1]).view(),
                      this->lookup(ids[idx + 0]).view()));
    }
  }
  return ranks_array;
}

static inline
char
dumb_tolower(char c) noexcept
{
  if ('A' <= c && c <= 'Z')
    c += 'a' - 'A';
  return c;
}

static
bool
nocase_is_lt(string_view left, string_view right) noexcept
{
  for (size_t i = 0; i < std::min(left.size(), right.size()); ++i) {
    char left_c = dumb_tolower(left[i]);
    char right_c = dumb_tolower(right[i]);
    if (left_c < right_c)
      return true;
    if (left_c > right_c)
      return false;
  }
  return left.size() < right.size();
}

StringTable::Comparator::Comparator(string_view collation_name)
    : kind([&]{
      // TODO(dancol): support user-defined collations one day
      if (collation_name == "binary")
        return Kind::BINARY;
      if (collation_name == "length")
        return Kind::LENGTH;
      if (collation_name == "nocase")
        return  Kind::NOCASE;
      throw_pyerr_fmt(PyExc_ValueError,
                      "unknown collation %s",
                      repr(collation_name));
    }())
{}

bool
StringTable::Comparator::is_lt(string_view left, string_view right)
    const noexcept
{
  switch (this->kind) {
    case Kind::BINARY:
      return left < right;
    case Kind::LENGTH:
      return left.size() < right.size();
    case Kind::NOCASE:
      return nocase_is_lt(left, right);
  }
}

unique_pyarray
StringTable::rank(string_view collation_name)
{
  auto it = this->rank_cache.find<string_view>(collation_name);
  if (it == this->rank_cache.end()) {
    unique_pyarray cache_entry =
        this->rank_1(Comparator(collation_name));
    auto [inserted_it, inserted] =
        this->rank_cache.insert({String(collation_name),
                                 std::move(cache_entry)});
    assume(inserted);
    it = inserted_it;
  }
  return it->second.notnull().addref();
}

unique_pcre2_code
StringTable::compile_pattern(Pattern pattern) const
{
  return compile_pcre2_pattern(
      (pattern.second == PatternLanguage::PCRE
       ? this->lookup(pattern.first).view()
       : this->translate_pattern_to_regexp(pattern)),
      (PCRE2_ANCHORED | PCRE2_NO_AUTO_CAPTURE |
       (pattern.second == PatternLanguage::SQL_ILIKE ? PCRE2_CASELESS : 0)));
}

StringTable::Matcher::Matcher(const StringTable* st, Pattern pattern)
    : pattern(std::move(pattern)),
      compiled_pattern(st->compile_pattern(this->pattern)),
      match_data(make_pcre2_match_data(this->compiled_pattern.get()))
{
  jit_compile_pcre2(this->compiled_pattern.get());
  size_t used_jit = get_pcre2_info<size_t>(
      PCRE2_INFO_JITSIZE,
      this->compiled_pattern.get());
  if (!used_jit)
    throw_pyerr_msg(PyExc_ValueError, "PCRE JIT did not run");

}

bool
StringTable::Matcher::match_uncached(const StringTable* st,
                                     id_type subject_sid)
{
  // TODO(dancol): benchmark and figure out at what point we want to
  // switch from non-JIT to JIT matching.  JIT-compiling every single
  // pattern is probably wasteful if we match a lot of different
  // patterns instead of matching the same one over and over.
  const StringTableEntry& subject = st->lookup(subject_sid);
  const int rc = pcre2_jit_match(
      this->compiled_pattern.get(),
      reinterpret_cast<PCRE2_SPTR>(subject.view().data()),
      subject.view().size(),
      /*startoffset=*/0,
      /*options=*/0,
      this->match_data.get(),
      /*mcontext=*/nullptr);
  if (rc == PCRE2_ERROR_NOMATCH)
    return false;
  if (rc <= 0)
    throw_pcre2_error_nogil(rc, "pcre2_jit_match");
  return true;
}

bool
StringTable::Matcher::match(const StringTable* st, id_type subject_sid)
{
  if (subject_sid >= static_cast<id_type>(this->match_state_by_sid.size()))
    this->match_state_by_sid.resize(st->size());
  assume(subject_sid >= 0);
  assume(subject_sid < static_cast<id_type>(this->match_state_by_sid.size()));
  int8_t match = this->match_state_by_sid[subject_sid];
  if (!match) {
    match = this->match_uncached(st, subject_sid) ? 1 : -1;
    this->match_state_by_sid[subject_sid] = match;
  }
  assume(match == -1 || match == 1);
  return match > 0;
}

static
String
translate_like_to_regexp(const char* pattern_cstr)
{
  String translated_pattern;
  while (*pattern_cstr) {
    char c = *pattern_cstr++;
    switch (c) {
      case '%':
        translated_pattern.append(".*");
        break;
      case '_':
        translated_pattern.push_back('.');
        break;
      case '\\':
      case '^':
      case '$':
      case '.':
      case '[':
      case ']':
      case '(':
      case ')':
      case '?':
      case '*':
      case '+':
      case '{':
        translated_pattern.push_back('\\');
        /* fall through */
      default:
        translated_pattern.push_back(c);
        break;
    }
  }
  return translated_pattern;
}

String
StringTable::translate_pattern_to_regexp(Pattern pattern) const
{
  const char* pattern_cstr = this->lookup(pattern.first).c_str();
  assert(pattern.second == PatternLanguage::PCRE ||
         pattern.second == PatternLanguage::SQL_LIKE ||
         pattern.second == PatternLanguage::SQL_ILIKE);
  String translated_pattern;
  if (pattern.second == PatternLanguage::PCRE)
    translated_pattern = String(pattern_cstr);
  else
    translated_pattern = translate_like_to_regexp(pattern_cstr);
  return translated_pattern;
}

StringTable::Matcher&
StringTable::get_matcher_for_pattern(Pattern pattern)
{
  if (!this->matcher_cache ||
      this->matcher_cache->get_pattern() != pattern)
    this->matcher_cache = Matcher(this, pattern);
  return *this->matcher_cache;
}

bool
StringTable::match1(Pattern pattern, id_type subject_sid)
{
  return this->get_matcher_for_pattern(pattern).match(this, subject_sid);
}

Py_ssize_t
StringTable::py_length() noexcept
{
  return static_cast<Py_ssize_t>(this->size());
}

int
StringTable::py_contains(PyObject* value)
{
  if (!isinstance_exact(value, &PyBytes_Type))
    return 0;
  const char* s = PyBytes_AS_STRING(value);
  Py_ssize_t slen = PyBytes_GET_SIZE(value);
  return !!this->intern_soft(string_view(s, slen));
}

unique_pyref
StringTable::py_intern(pyref args)
{
  PARSEPYARGS(
      (py_byte_view, s)
  )(args);
  return make_pylong(this->intern(s.as_string_view()));
}

unique_pyarray
StringTable::vintern(pyref byte_obj_seq)
{
  auto it = pyseq_iter_fast(byte_obj_seq);
  auto end = it.end();
  npy_intp nr = std::distance(it, end);
  unique_pyarray array = make_uninit_pyarray(type_descr<id_type>(), nr);
  id_type* out = npy_data<id_type>(array);
  while (it != end) {
    pyref item = *it++;
    check_pytype(&PyBytes_Type, item);
    const char* item_data = PyBytes_AS_STRING(item.get());
    Py_ssize_t item_size = PyBytes_GET_SIZE(item.get());
    assume(item_size >= 0);
    *out++ = this->intern(string_view(item_data, item_size));
  }
  return array;
}

unique_obj_pyref<StringLookupCache>
StringTable::make_lookup_cache(pyref decode_mode)
{
  auto cache = make_pyobj<StringLookupCache>(addref(decode_mode));
  cache->st = this;
  this->lookup_caches.push_back(*cache);
  return cache;
}

void
StringTable::throw_invalid_id(id_type id) const
{
  throw_pyerr_fmt(PyExc_KeyError, "unknown string ID: %s", id);
}

unique_pyarray
StringTable::vlookup(pyarray_ref codes, pyref decode, bool subst)
{
  obj_pyref<StringLookupCache> cache;
  unique_obj_pyref<StringLookupCache> local_cache;
  if (decode && isinstance_exact(decode, &StringLookupCache::pytype)) {
    cache = decode.as_unsafe<StringLookupCache>();
    if (cache->st != this)
      throw_pyerr_msg(PyExc_ValueError, "wrong cache");
  } else {
    if (!decode || decode == Py_None)
      decode = &PyBytes_Type;
    local_cache = this->make_lookup_cache(decode);
    cache = local_cache;
  }

  size_t nids = this->size();
  cache->ensure_size(nids);
  auto iter = NpyIteration1<StringTable::id_type>(codes);
  npy_intp nr = iter.size();
  // Use PyArray_SimpleNew directly so that we can work with an
  // object-reference numpy array, which we ordinarily forbid.
  unique_pyarray aout = adopt_check_as_unsafe<PyArrayObject>(
      PyArray_SimpleNew(1, &nr, NPY_OBJECT));
  PyObject** pout = static_cast<PyObject**>(PyArray_DATA(aout.get()));
  size_t out_idx = 0;
  for (; !iter.is_at_eof(); iter.advance()) {
    StringTable::id_type id = iter.get();
    pyref lookup_result;
    if (!(0 <= id && static_cast<size_t>(id) < nids)) {
      if (subst)
        lookup_result = Py_None;
      else
        this->throw_invalid_id(id);
    } else {
      lookup_result = cache->get(id);
      if (!lookup_result) {
        const auto& entry = this->lookup(id);
        if (entry.valid())
          lookup_result = cache->recompute(id, this->lookup(id));
        else if (subst)
          lookup_result = Py_None;
        else
          this->throw_invalid_id(id);
      }
    }
    char* dest = reinterpret_cast<char*>(&pout[out_idx++]);
    if (PyArray_SETITEM(aout.get(), dest, lookup_result.get()))
      throw_current_pyerr();
  }
  return aout;
}

static bool
is_valid_pattern_language(int py_pattern_language)
{
  constexpr auto known_languages = make_array(
      StringTable::PatternLanguage::PCRE,
      StringTable::PatternLanguage::SQL_LIKE,
      StringTable::PatternLanguage::SQL_ILIKE);
  for (StringTable::PatternLanguage language : known_languages)
    if (static_cast<int>(language) == py_pattern_language)
      return true;
  return false;
}

unique_pyarray
StringTable::match(QueryExecution* qe,
                   pyarray_ref subjects,
                   pyarray_ref patterns,
                   int pattern_language)
{
  if (!is_valid_pattern_language(pattern_language))
    _throw_pyerr_fmt(PyExc_ValueError,
                     "invalid pattern language %d",
                     pattern_language);
  auto pl = static_cast<StringTable::PatternLanguage>(pattern_language);
  auto do_match = [&](id_type subject_id, id_type pattern_id) -> bool {
    StringTable::Pattern pattern = { pattern_id, pl };
    return this->match1(pattern, subject_id);
  };
  return do_npy_binop<id_type, id_type, bool>(
      qe, subjects, patterns, do_match);
}

unique_pyref
StringTable::concat(QueryExecution* qe,
                    pyarray_ref left,
                    pyarray_ref right)
{
  String buffer;
  auto do_concat = [&](id_type left_id, id_type right_id) -> id_type {
    // TODO(dancol): we could optimize this loop by computing the hash
    // from the two parts of the string and checking whether the
    // result is in the table before actually building the
    // String buffer.
    buffer.clear();
    buffer.append(this->lookup(left_id).view());
    buffer.append(this->lookup(right_id).view());
    return this->intern(buffer);
  };
  return do_npy_binop<id_type, id_type, id_type>(
      qe, left, right, do_concat);
}

int
StringTable::py_traverse(visitproc visit, void* arg) const noexcept
{
  for (auto& [collation, rank] : this->rank_cache)
    Py_VISIT(rank.get());
  if (int ret = this->HasDict::py_traverse(visit, arg))
    return ret;
  return 0;
}

int
StringTable::py_clear() noexcept
{
  this->rank_cache.clear();
  this->matcher_cache.reset();
  this->HasDict::py_clear();
  return 0;
}

unique_pyarray
StringTable::make_translation_table(obj_pyref<StringTable> other) const
{
  assert(this->size() <=
         static_cast<size_t>(std::numeric_limits<npy_intp>::max()));
  npy_intp n = static_cast<npy_intp>(this->size());
  unique_pyarray ret = make_uninit_pyarray(
      type_descr<id_type>(), n);
  id_type* out = npy_data<id_type>(ret);
  for (npy_intp i = 0; i < n; ++i)
    out[i] = other->intern(this->lookup(static_cast<id_type>(i)).view());
  return ret;
}



StringLookupCache::~StringLookupCache() noexcept
{
  if (this->st) {
    auto it = this->st->lookup_caches.iterator_to(*this);
    this->st->lookup_caches.erase(it);
  }
}

void
StringLookupCache::ensure_size(size_t n)
{
  this->cached.resize(n);
}

void
StringLookupCache::clear() noexcept
{
  this->cached.clear();
  this->cached.shrink_to_fit();
}

pyref
StringLookupCache::recompute(StringIdType id,
                             const StringTableEntry& entry)
{
  assume(entry.valid());
  string_view view = entry.view();
  assume(view.size() <= std::numeric_limits<Py_ssize_t>::max());
  unique_pyref obj;
  if (this->decode_mode == &PyBytes_Type) {
    obj = adopt_check(PyBytes_FromStringAndSize(view.data(), view.size()));
  } else {
    const char* errors = nullptr;
    if (this->decode_mode != &PyUnicode_Type) {
      errors = PyUnicode_AsUTF8(this->decode_mode.get());
      if (!errors)
        throw_current_pyerr();
    }
    obj = adopt_check(PyUnicode_DecodeUTF8Stateful(
        view.data(),
        static_cast<Py_ssize_t>(view.size()),
        errors, /*consumed=*/nullptr));
  }
  assume(0 <= id && id <
         static_cast<StringTable::id_type>(this->cached.size()));
  pyref ret = obj;
  this->cached[id] = std::move(obj);
  return ret;
}

int
StringLookupCache::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->decode_mode.get());
  for (const auto& ref : this->cached)
    Py_VISIT(ref.get());
  return 0;
}

StringLookupCache::StringLookupCache(unique_pyref decode_mode)
    : decode_mode(std::move(decode_mode))
{
  assume(this->decode_mode);
  if (this->decode_mode != &PyUnicode_Type &&
      this->decode_mode != &PyBytes_Type &&
      !isinstance_exact(this->decode_mode, &PyUnicode_Type))
  {
    throw_pyerr_fmt(PyExc_TypeError,
                    "bad decode mode %s: must be bytes, str, "
                    "or a string giving the UTF-8 "
                    "error substitution mode",
                    repr(this->decode_mode));
  }
}

static
void
check_contiguous_str_codes_array(pyarray_ref codes)
{
  if (npy_dtype(codes) != type_descr<StringTable::id_type>())
    throw_pyerr_fmt(PyExc_TypeError,
                    "array has wrong dtype %s",
                    repr(npy_dtype(codes)));
  if (npy_strides(codes)[0] != sizeof (StringTable::id_type))
    throw_pyerr_fmt(PyExc_TypeError,
                    "array has wrong stride %s expecting %s",
                    npy_strides(codes)[0],
                    sizeof (StringTable::id_type));
}

static
unique_pyref
str_fast_take(unique_pyarray out,
              pyarray_ref codes,
              pyarray_ref tx_table)
{
  using id_type = StringTable::id_type;
  using id_type_unsigned = std::make_unsigned_t<id_type>;

  check_contiguous_str_codes_array(out);
  check_contiguous_str_codes_array(codes);
  check_contiguous_str_codes_array(tx_table);

  npy_intp nr_codes = npy_size1d(codes);
  if (nr_codes != npy_size1d(out))
    throw_pyerr_fmt(PyExc_ValueError,
                    "codes and output array sizes differ: %s and %s "
                    "respectively",
                    npy_size1d(codes), npy_size1d(out));

  npy_intp tx_table_size = npy_size1d(tx_table);
  assume(nr_codes >= 0);
  assume(tx_table_size >= 0);
  id_type* restrict codes_data = npy_data<id_type>(codes);
  const id_type* restrict tx_data = npy_data<id_type>(tx_table);
  id_type* out_data;
  if (npy_data<id_type>(out) == npy_data<id_type>(codes)) {
    out_data = codes_data;
  } else {
    id_type* restrict explicit_out_data = npy_data<id_type>(out);
    out_data = explicit_out_data;
  }

  for (npy_intp i = 0; i < nr_codes; ++i) {
    id_type code = codes_data[i];
    assume(0 <= code);
    if (safe_mode && code >= tx_table_size) {
      throw_pyerr_fmt(PyExc_IndexError,
                      "bad source string code %s (max=%s)",
                      code, tx_table_size);
    }
    assume(code < tx_table_size);
    // The static_cast allows the compiler to avoid sign-extending the
    // move from a 32-bit location to a 64-bit register.
    out_data[i] = tx_data[static_cast<id_type_unsigned>(code)];
  }
  return out;
}



PyMethodDef StringTable::pymethods[] = {
  make_methoddef("intern",
                 wraperr<&StringTable::py_intern>(),
                 METH_VARARGS,
                 "Intern a string"),
  AUTOMETHOD(&StringTable::vintern,
             "Vectorized intern of bytes objects",
             (pyref, things)
  ),
  AUTOMETHOD(&StringTable::vlookup,
             "Vectorized lookup of string codes",
             (unique_pyarray, codes, no_default, convert_any_array)
             (OPTIONAL_ARGS_FOLLOW)
             (pyref, decode)
             (bool, subst, false)
  ),
  AUTOMETHOD(&StringTable::make_lookup_cache,
             "Make byte object cache for vlookup",
             (OPTIONAL_ARGS_FOLLOW)
             (pyref, decode_mode, &PyBytes_Type)
  ),
  AUTOMETHOD(&StringTable::rank,
             "Rank table under various transformations",
             (string_view, collation_name)
  ),
  AUTOMETHOD(&StringTable::match,
             "Vectorized match against patterns",
             (QueryExecution*, qe)
             (unique_pyarray, subjects, no_default, convert_any_array)
             (unique_pyarray, patterns, no_default, convert_any_array)
             (int, pattern_language)
  ),
  AUTOMETHOD(&StringTable::concat,
             "Vectorized string concatenation",
             (QueryExecution*, qe)
             (unique_pyarray, left, no_default, convert_any_array)
             (unique_pyarray, right, no_default, convert_any_array)
  ),
  AUTOMETHOD(&StringTable::make_translation_table,
             "Map one StringTable's codes to another's",
             (obj_pyref<StringTable>, other_st)
  ),
  { 0 },
};

unique_pyref
StringTable::py_get_c2s(void*)
{
  // Inefficient, but this function isn't on a hot path.
  return this->vlookup(
      adopt_check_as_unsafe<PyArrayObject>(
          PyArray_ArangeObj(make_pylong(0).get(),
                            make_pylong(this->size()).get(),
                            make_pylong(1).get(),
                            type_descr<StringTable::id_type>().get())),
      /*decode=*/pyref(),
      /*subst=*/true);
}

PyGetSetDef StringTable::pygetset[] = {
  make_getset("c2s",
              "Mapping from string code to bytes object",
              wraperr<&StringTable::py_get_c2s>()),
  { 0 },
};

PyMemberDef StringTable::pymembers[] = {
  make_memberdef("seqno",
                 T_ULONG,
                 offsetof(StringTable, seqno),
                 READONLY,
                 "seqno"),
  { 0 },
};

PySequenceMethods StringTable::pyseq[] = {
  wraperr<&StringTable::py_length, -1>(),    /* sq_length */
  nullptr,                                   /* sq_concat */
  nullptr,                                   /* sq_repeat */
  nullptr,                                   /* sq_item */
  nullptr,                                   /* sq_slice */
  nullptr,                                   /* sq_ass_item */
  nullptr,                                   /* sq_ass_slice */
  wraperr<&StringTable::py_contains, -1>(),  /* sq_contains */
};

PyTypeObject StringTable::pytype = make_py_type<StringTable>(
    "dctv._native.StringTable",
    "String table",
    [](PyTypeObject* t) {
      t->tp_getset = StringTable::pygetset;
      t->tp_as_sequence = StringTable::pyseq;
      t->tp_methods = StringTable::pymethods;
      t->tp_members = StringTable::pymembers;
    }
);

PyTypeObject StringLookupCache::pytype = make_py_type<StringLookupCache>(
    "dctv._native.StringLookupCache",
    "Cache of string objects for lookup",
    [](PyTypeObject* t) {}
);

static PyMethodDef functions[] = {
  AUTOFUNCTION(
      str_fast_take,
      "Specialized translation function for string codes",
      (unique_pyarray, out, no_default, convert_base_1d_array)
      (unique_pyarray, codes, no_default, convert_base_1d_array)
      (unique_pyarray, tx_table, no_default, convert_base_1d_array)
  ),
  { 0 }
};

void
init_string_table(pyref m)
{
  register_type(m, &StringTable::pytype);
  register_type(m, &StringLookupCache::pytype);
  register_functions(m, functions);
  register_constant(m, "DTYPE_STRING_ID",
                    type_descr<StringTable::id_type>());
  register_constant(m, "DTYPE_STRING_RANK",
                    type_descr<StringTable::Rank>());
  register_constant(m, "MATCH_PCRE",
                    static_cast<int>(StringTable::PatternLanguage::PCRE));
  register_constant(m, "MATCH_SQL_LIKE",
                    static_cast<int>(StringTable::PatternLanguage::SQL_LIKE));
  register_constant(m, "MATCH_SQL_ILIKE",
                    static_cast<int>(StringTable::PatternLanguage::SQL_ILIKE));

}

}  // namespace dctv
