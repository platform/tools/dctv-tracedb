// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include <stdint.h>

namespace dctv {

// We prepend LINUX_ here both for namespacing and to avoid
// preprocessor expansion of these symbols.

static const int LINUX_TASK_RUNNING = 0;
static const int LINUX_TASK_INTERRUPTIBLE = 1;
static const int LINUX_TASK_UNINTERRUPTIBLE = 2;
static const int LINUX_TASK_STOPPED = 4;
static const int LINUX_TASK_TRACED = 8;
static const int LINUX_EXIT_DEAD = 16;
static const int LINUX_EXIT_ZOMBIE = 32;
static const int LINUX_TASK_DEAD = 64;
static const int LINUX_TASK_WAKEKILL = 128;
static const int LINUX_TASK_WAKING = 256;
static const int LINUX_TASK_PARKED = 512;
static const int LINUX_TASK_NOLOAD = 1024;
static const int LINUX_TASK_STATE_MAX = 4096;

static const int LINUX_TASK_IDLE = (LINUX_TASK_UNINTERRUPTIBLE |
                                    LINUX_TASK_NOLOAD);

static const int LINUX_CSIGNAL = 0x000000ff;
static const int LINUX_CLONE_VM = 0x00000100;
static const int LINUX_CLONE_FS = 0x00000200;
static const int LINUX_CLONE_FILES = 0x00000400;
static const int LINUX_CLONE_SIGHAND = 0x00000800;
static const int LINUX_CLONE_PTRACE = 0x00002000;
static const int LINUX_CLONE_VFORK = 0x00004000;
static const int LINUX_CLONE_PARENT = 0x00008000;
static const int LINUX_CLONE_THREAD = 0x00010000;
static const int LINUX_CLONE_NEWNS = 0x00020000;
static const int LINUX_CLONE_SYSVSEM = 0x00040000;
static const int LINUX_CLONE_SETTLS = 0x00080000;
static const int LINUX_CLONE_PARENT_SETTID = 0x00100000;
static const int LINUX_CLONE_CHILD_CLEARTID = 0x00200000;
static const int LINUX_CLONE_DETACHED = 0x00400000;
static const int LINUX_CLONE_UNTRACED = 0x00800000;
static const int LINUX_CLONE_CHILD_SETTID = 0x01000000;
static const int LINUX_CLONE_NEWCGROUP = 0x02000000;
static const int LINUX_CLONE_NEWUTS = 0x04000000;
static const int LINUX_CLONE_NEWIPC = 0x08000000;
static const int LINUX_CLONE_NEWUSER = 0x10000000;
static const int LINUX_CLONE_NEWPID = 0x20000000;
static const int LINUX_CLONE_NEWNET = 0x40000000;
static const int LINUX_CLONE_IO = 0x80000000;

static const int32_t PID_MAX_LIMIT = 0x400000;

// Unusual states like "I" and "P" get translated to normal states
// during ingestion.

static inline constexpr auto procfs_thread_states = make_array<char>(
    'R' /* running: actually means runnable */,
    'S' /* sleeping */,
    'D' /* disk sleep */,
    'T' /* stopped */,
    't' /* tracing stop */,
    'X' /* dead */,
    'Z' /* zombie */
);

} // namespace dctv
