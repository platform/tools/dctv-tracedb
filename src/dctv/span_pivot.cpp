// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// Group by partition, essentially.  The metadata logic is in this
// file; the actual aggregation kernel generation is over in
// span_pivot_column.cpp.

#include "dctv.h"

#include "autoevent.h"
#include "command_stream.h"
#include "hash_table.h"
#include "npyiter.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "result_buffer.h"
#include "span.h"
#include "span_pivot.h"
#include "vector.h"

namespace dctv {
namespace {

using std::string_view;

struct OutputPartitionState final {
  optional<TimeStamp> last_ts;
  Index nr_open = 0;
  bool changed = false;
};

unique_pyref
span_pivot(PyObject*, PyObject* py_args)
{
  namespace ae = auto_event;

  auto args = PARSEPYARGS_V(
      (pyref, metadata_source)
      (pyref, metadata_out)
      (pyref, command_out)
      (npy_intp, min_npartitions)
      (QueryExecution*, qe)
  )(py_args);

  const auto min_npartitions = args.min_npartitions;
  if (min_npartitions < 0)
    throw_invalid_query_error("invalid minimum partition count %s",
                              min_npartitions);

  HashTable<Partition, OutputPartitionState> output_partitions;
  Vector<Partition> changed_output_partitions;
  ResultBuffer<TimeStamp, TimeStamp, Partition> span_out(
      args.metadata_out.addref(),
      args.qe);

  CommandOut<SpCommand> command_out(
      args.command_out.notnull().addref(),
      args.qe);

  auto mark_partition_changed = [&]
      (TimeStamp ts,
       Partition output_partition,
       OutputPartitionState* ops)
  {
    if (!ops->changed) {
      ops->changed = true;
      changed_output_partitions.push_back(output_partition);
      // TODO(dancol): maybe batch changes and reduce command stream
      // chatter in cases where actions have no point, e.g., clearing
      // all input partitions of an output partition state when we're
      // about to destroy the whole output partition.
      if (ops->last_ts.has_value() && ops->nr_open >= min_npartitions) {
        TimeStamp last_ts = *ops->last_ts;
        assume(ts > last_ts);
        TimeStamp duration = ts - last_ts;
        span_out.add(last_ts, duration, output_partition);
        command_out.emit(SpCommand::EMIT_OUTPUT_PARTITION,
                         output_partition);
      }
    }
  };

  AUTOEVENT_DECLARE_EVENT(
      SpanStart,
      (TimeStamp, duration, ae::span_duration)
      (Partition, input_partition, ae::partition)
      (Partition, output_partition)
  );

  AUTOEVENT_DECLARE_EVENT(
      SpanEnd,
      (Partition, input_partition)
      (Partition, output_partition)
  );

  auto handle_span_start = [&](
      auto&& ae,
      TimeStamp ts,
      const SpanStart& ss)
  {
    auto& ops = output_partitions[ss.output_partition];
    mark_partition_changed(ts, ss.output_partition, &ops);
    assume(0 <= ops.nr_open);
    assume(ops.nr_open < std::numeric_limits<Index>::max());
    ops.nr_open += 1;
    command_out.emit(SpCommand::SLURP_INPUT_PARTITION,
                     ss.input_partition,
                     ss.output_partition);
    ae::enqueue_event(
        ae,
        ts + ss.duration,
        SpanEnd {
          ss.input_partition,
          ss.output_partition,
        });
  };

  auto handle_span_end = [&](
      auto&& ae,
      TimeStamp ts,
      const SpanEnd& se)
  {
    auto it = output_partitions.find(se.output_partition);
    assert(it != output_partitions.end());
    OutputPartitionState* ops = &it->second;
    mark_partition_changed(ts, se.output_partition, ops);
    assume(0 < ops->nr_open);
    ops->nr_open -= 1;
    command_out.emit(SpCommand::CLEAR_INPUT_PARTITION,
                     se.input_partition,
                     se.output_partition);
  };

  auto process_changed_output_partition = [&]
      (TimeStamp ts, Partition output_partition)
  {
    auto it = output_partitions.find(output_partition);
    assert(it != output_partitions.end());
    OutputPartitionState* ops = &it->second;
    assume(ops->changed);

    if (ops->nr_open == 0) {
      // TODO(dancol): benchmark leaving it open
      output_partitions.erase(it);
      command_out.emit(SpCommand::FORGET_OUTPUT_PARTITION,
                       output_partition);
    } else {
      ops->changed = false;
      ops->last_ts = ts;
    }
  };

  auto process_changed_output_partitions = [&](TimeStamp ts) {
    for (Partition output_partition : changed_output_partitions)
      process_changed_output_partition(ts, output_partition);
    changed_output_partitions.clear();
  };

  ae::process(
      ae::ts_batch_end_hook([&](auto&& ae, TimeStamp ts) {
        process_changed_output_partitions(ts);
      }),
      ae::synthetic_event<SpanEnd>(
          handle_span_end),
      ae::input<SpanStart>(
          args.metadata_source,
          handle_span_start)
  );

  span_out.flush();
  command_out.flush();
  return addref(Py_None);
}
}  // anonymous namespace



static PyMethodDef functions[] = {
  make_methoddef("span_pivot",
                 wraperr<span_pivot>(),
                 METH_VARARGS,
                 "Aggregate partitions in a span table"),
  { 0 }
};

void
init_span_pivot(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
