// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "backfill.h"

#include "autoevent.h"
#include "hash_table.h"
#include "pyerr.h"
#include "pylog.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "result_buffer.h"
#include "span.h"

namespace dctv {

template<typename T>
static
void
backfill_1(pyref py_inp,
           unique_pyref py_out,
           QueryExecution* qe)
{
  // Backfill is a partition-sequential operator, which means that
  // instead of maintaining a hash table mapping partition to current
  // state and handling all partitions separately, we process one
  // partition at a time.
  //
  // TODO(dancol): fix calling convention so that we can forward
  // blocks zero-copy for long runs of unfilled rows.
  //
  namespace ae = auto_event;
  ResultBuffer<T, bool> out(std::move(py_out), qe);

  AUTOEVENT_DECLARE_EVENT(
      Span,
      (TimeStamp, duration, ae::span_duration)
      (Partition, partition, ae::partition)
      (T, value)
      (bool, masked)
  );

  Index group_size = 0;
  optional<TimeStamp> last_end_ts;

  auto null_fill_group = [&]() {
    for (; group_size; --group_size)
      out.add(T{}, /*masked=*/true);
  };

  auto value_fill_group = [&](T value) {
    for (; group_size; --group_size)
      out.add(value, /*masked=*/false);
  };

  auto handle_span = [&](auto&& ae,
                         TimeStamp ts,
                         const Span& span) {
    assume(!last_end_ts || *last_end_ts <= ts);
    if (last_end_ts && *last_end_ts != ts) {
      // We don't fill across gaps in the span sequence.
      null_fill_group();
    }
    group_size += 1;
    if (!span.masked)
      value_fill_group(span.value);
    last_end_ts = ts + span.duration;
  };


  ae::process(
      ae::partition_major_mode,
      ae::partition_end_hook([&](auto&& ae){
        null_fill_group();
        last_end_ts = {};
      }),
      ae::input<Span>(py_inp, handle_span)
  );

  null_fill_group();
  out.flush();
}

static
unique_pyref
backfill(PyObject*, PyObject* args)
{
  PARSEPYARGS(
      (pyref, py_inp)
      (pyref, py_out)
      (QueryExecution*, qe)
      (pyref, py_dtype)
  )(args);

  npy_type_dispatch(py_dtype, [&](auto&& arg) {
    backfill_1<std::decay_t<decltype(arg)>>(
        py_inp, py_out.addref(), qe);
  });

  return addref(Py_None);
}



static PyMethodDef functions[] = {
  make_methoddef("backfill",
                 wraperr<backfill>(),
                 METH_VARARGS,
                 "Fill data holes"),
  { 0 }
};

void
init_backfill(pyref m)
{
  register_functions(m, functions);
}


}  // namespace dctv
