#!/system/bin/mksh
# -*- sh-indentation: 2 -*-

# Take a snapshot of the system process state.

# Read the whole file $1 and backslash-escape double quote, backslash
# and newline, placing the entire quoted string in double quotes.
# Any final newline is ignored.  The slurped and escaped value is put
# into $REPLY.  Return success only iff we were able to actually read
# and escape the file.
read_and_escape() {
  # The implementation is convoluted to work around mksh's slow
  # implementation of string-wide variable substitution.  Ideally,
  # we'd just slurp the whole file and substitute the special
  # characters, but we do it line-by-line instead.
  typeset -a lines
  typeset -a elines
  typeset line
  typeset prefix
  typeset IFS=$'\n'
  read -Ard '' lines <"$1"
  [[ -z $lines ]] && return 1
  for line in "${lines[@]}"; do
    if [[ $line = *['\''"']* ]]; then
      line=${line//'\'/'\\'}
      line=${line//'"'/'\"'}
    fi
    elines+=("$prefix$line")
    prefix='\n'
  done
  IFS=''
  REPLY="\"${elines[*]}\""
  return 0
}
collect_snapshot() {
  exec 3>/d/tracing/trace_marker
  print -r >&3 "dctv_snapshot_start $1"
  for x in /proc/[0-9]*; do
    cd $x || continue
    tgid=${x##*/}
    if read -rd '' <oom_score_adj; then
      print -r "tgid_oom_score_adj $tgid $REPLY"
    fi
    if read_and_escape status; then
      print -r "tid_status $tgid $REPLY"
    fi
    for tid_dir in task/*; do
      tid=${tid_dir##*/}
      print -r "tid_tgid $tid $tgid"
      if read_and_escape $tid_dir/stat; then
        print -r "tid_stat $tid $REPLY"
      fi
    done
  done
  print -r >&3 "dctv_snapshot_end $1"
}
collect_snapshot 0
