// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <stddef.h>
#include <tuple>
#include <type_traits>
#include <utility>

namespace dctv {

template<std::size_t... I>
auto make_index_tuple_impl(std::index_sequence<I...>) {
  return std::tuple<std::integral_constant<std::size_t, I>...>();
}

template<std::size_t N>
using index_tuple_t =
    decltype(make_index_tuple_impl(std::make_index_sequence<N>()));

}  // namespace dctv
