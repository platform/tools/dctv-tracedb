// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<bool Munge1st, typename R, typename T, typename... Args>
template<typename base_py_fn_info<Munge1st, R, T, Args...>::delegate_type Delegate>
auto
base_py_fn_info<Munge1st, R, T, Args...>::plain_c_wrapper(
    first_argument_type obj,
    typename py_fn_arg_decay<Args>::plain_c_type... args) noexcept
{
  T* this_ = pyref(obj).as_unsafe<T>().get();
  auto args_tuple = std::make_tuple(this_, py_fn_arg_decay<Args>::convert(args)...);
  if constexpr(!(std::is_void_v<R> || std::is_integral_v<R>)) {
    // No actual refcount change if Delegate returns a
    // unique_obj_pyref we can move into addref.
    return addref(Delegate(std::move(args_tuple))).release();
  } else {  // NOLINT
    return Delegate(std::move(args_tuple));
  }
}

template<typename FunctionType,
         typename ErrorSentinelType,
         bool Munge1st>
template<FunctionType Function, ErrorSentinelType ErrorSentinel>
auto
_py_error_wrapper<FunctionType, ErrorSentinelType, Munge1st>::wrapper<Function, ErrorSentinel>::delegate(
    typename info::args_tuple_type args_tuple) noexcept
{
  if constexpr(info::is_noexcept) {
    return std::apply(Function, std::move(args_tuple));
  } else {  // NOLINT
    return _exceptions_to_pyerr(
        [&](){return std::apply(Function, std::move(args_tuple));},
        ErrorSentinel);
  }
}

template<auto Function,
         auto ErrorSentinel,
         bool Munge1st>
constexpr auto
wraperr()
{
  return _py_error_wrapper<decltype(Function),
                           decltype(ErrorSentinel),
                           Munge1st>
      ::template wrapper<Function, ErrorSentinel>::function;
}

template<typename Functor, typename ErrorSentinel>
auto
_exceptions_to_pyerr(Functor&& functor,
                     ErrorSentinel error_sentinel)
    noexcept
{
  using ReturnType = typename std::conditional_t<
    std::is_same_v<ErrorSentinel, AutoErrorSentinel>,
    decltype(functor()),
    ErrorSentinel>;

  try {
    return functor();
  } catch (...) {
    _set_pending_cxx_exception_as_pyexception();
  }
  assume(pyerr_occurred());
  if constexpr(!std::is_void_v<ReturnType>) {
    if constexpr(std::is_same_v<ErrorSentinel, AutoErrorSentinel>) {
      return ReturnType();
    } else {  // NOLINT
      return error_sentinel;
    }
  }
}

PyExceptionSaver::PyExceptionSaver() noexcept
    : info(PyExceptionInfo::fetch())
{}

PyExceptionSaver::~PyExceptionSaver() noexcept
{
  std::move(this->info).restore();
}

}  // namespace dctv
