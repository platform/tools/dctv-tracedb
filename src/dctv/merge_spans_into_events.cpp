// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "merge_spans_into_events.h"

#include <tuple>

#include "autoevent.h"
#include "hash_table.h"
#include "pyerrfmt.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "result_buffer.h"
#include "span.h"
#include "span_result_buffer.h"

namespace dctv {

// Augment an event table with spans

static
unique_pyref
merge_spans_into_events(PyObject*, PyObject* args)
{
  namespace ae = auto_event;

  PARSEPYARGS(
      (pyref, py_event_meta_source)
      (pyref, py_span_meta_source)
      (pyref, py_event_out)
      (pyref, py_index_out)
      (QueryExecution*, qe)
      (bool, event_is_partitioned)
      (bool, span_is_partitioned)
      (bool, span_is_required)
  )(args);

  const bool event_out_is_partitioned =
      event_is_partitioned || span_is_partitioned;
  const bool broadcast_mode =
      span_is_partitioned && !event_is_partitioned;

  if (broadcast_mode && !span_is_required)
    throw_invalid_query_error(
        "EVENT JOIN in broadcast mode requires span");

  if (broadcast_mode)
    assert(span_is_partitioned);

  EventTableResultBuffer event_out(addref(py_event_out), qe,
                                   event_out_is_partitioned);

  ResultBuffer<Index, Index> index_out(addref(py_index_out), qe);

  auto emit = [&](TimeStamp ts,
                  Partition partition,
                  Index event_index,
                  Index span_index) {
    event_out.add(ts, partition);
    index_out.add(event_index, span_index);
  };

  struct PartitionState final {
    void open(Index index) {
      if (this->is_open())
        throw_pyerr_msg(PyExc_AssertionError, "bad span ordering");
      this->index = index;
    }

    void close() {
      if (!this->is_open())
        throw_pyerr_msg(PyExc_AssertionError, "span start inversion");
      this->index = -1;
    }

    Index get_index() const {
      assert(this->is_open());
      return this->index;
    }

    bool is_open() const { return this->index >= 0; }
   private:
    Index index = -1;
  };

  HashTable<Partition, PartitionState> partitions;

  auto maybe_get_ps = [&](Partition partition) -> PartitionState* {
    auto it = partitions.find(partition);
    return it == partitions.end() ? nullptr : &it->second;
  };

  auto get_ps = [&](Partition partition) -> PartitionState& {
    return *maybe_get_ps(partition);
  };

  auto get_or_create_ps = [&](Partition partition)
      -> PartitionState& {
    return partitions.try_emplace(partition).first->second;
  };

  AUTOEVENT_DECLARE_EVENT(
      SpanStart,
      (TimeStamp, duration, ae::span_duration)
      (Partition, partition, ae::partition)
  );

  AUTOEVENT_DECLARE_EVENT(
      SpanEnd,
      (Partition, partition, ae::partition)
  );

  AUTOEVENT_DECLARE_EVENT(
      Event,
      (Partition, partition, ae::partition)
  );

  auto handle_span_start = [&](auto&& ae,
                               TimeStamp ts,
                               const SpanStart& ss,
                               Index index)
  {
    get_or_create_ps(ss.partition).open(index);
    ae::enqueue_event(ae,
                      ts + ss.duration,
                      SpanEnd { ss.partition });
  };

  auto handle_span_end = [&](auto&& ae,
                             TimeStamp ts,
                             const SpanEnd& se)
  {
    get_ps(se.partition).close();
  };


  auto handle_event = [&]
      (auto&& ae,
       TimeStamp ts,
       const Event& event,
       Index index)
  {
    if (broadcast_mode) {
      for (const auto& [partition, ps] : partitions)
        if (ps.is_open())
          emit(ts,
               partition,
               index,
               ps.get_index());
    } else {
      PartitionState* ps = maybe_get_ps(
          span_is_partitioned ? event.partition : 0);
      Index span_index = (ps && ps->is_open())
          ? ps->get_index() : -1;
      if (span_index >= 0 || !span_is_required)
        emit(ts,
             event.partition,
             index,
             span_index);
    }
  };

  ae::process(
      ae::synthetic_event<SpanEnd>(
          handle_span_end),
      ae::input<SpanStart>(
          py_span_meta_source,
          handle_span_start,
          ae::add_index_argument),
      ae::input<Event>(
          py_event_meta_source,
          handle_event,
          ae::add_index_argument)
  );

  event_out.flush();
  index_out.flush();

  return addref(Py_None);
}



static PyMethodDef functions[] = {
  make_methoddef("merge_spans_into_events",
                 wraperr<merge_spans_into_events>(),
                 METH_VARARGS,
                 "Merge an event table with spans"),
  { 0 }
};

void
init_merge_spans_into_events(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
