# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Random tests for pylint"""

# pylint: disable=missing-docstring,no-self-use

from .util import (
  Immutable,
  tattr,
  iattr,
  sattr,
  enumattr,
  AutoNumber,
)

class _TestObject(object):
  def foo(self):
    return 5

class _TestEnum(AutoNumber):
  FOO = ()
  BAR = ()

def _tattr_wrapper():
  return tattr(_TestObject)

class _TestImmutable(Immutable):
  ta_typeless = tattr()
  ta_typed = tattr(_TestObject)
  ta_type_tuple = tattr((_TestObject, int))
  sa_typeless = sattr()
  sa_typed = sattr(_TestObject)
  sa_type_tuple = sattr((_TestObject, int))
  ia_typeless = iattr(_TestObject)
  ia_typed = iattr(_TestObject)
  ia_type_tuple = iattr((_TestObject, int))
  ea = enumattr(_TestEnum)
  ta_wrapped = _tattr_wrapper()

  def calls(self):
    self.ta_typeless[0].foo()
    self.ta_typed[0].foo()
    self.ta_type_tuple[0].foo()
    self.sa_typeless.pop().foo()
    self.sa_typed.pop().foo()
    self.sa_type_tuple.pop().foo()
    self.ia_typeless.foo()
    self.ia_typed.foo()
    self.ia_type_tuple.foo()
    self.ta_wrapped[0].foo()
