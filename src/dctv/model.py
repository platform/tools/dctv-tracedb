# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Looking at trace files"""
import logging
import weakref
from collections import defaultdict
from itertools import count as xcount
from os.path import dirname, join as pjoin

from cytoolz import first
from numpy.ma import nomask

from modernmp.util import (
  ClosingContextManager,
  exceptions_are_fatal,
  once,
  safe_close,
  the,
)

from .trace_file_util import MappedTrace

from ._native import (
  DTYPE_CPU_NUMBER,
  DTYPE_ID_VALUE,
  QueryCache,
  SYSTEM_UTID_BASE,
  Snapshot,
  npy_explode,
)

from .query import (
  EVENT_UNPARTITIONED_TIME_MAJOR,
  GenericQueryTable,
  InvalidQueryException,
  QueryNode,
  QueryTable,
  SimpleQueryNode,
  TS_SCHEMA,
  TableSorting,
)

from .queryengine import (
  QueryEngine,
  Delivery,
)

from .util import (
  ExplicitInheritance,
  TmpDir,
  abstract,
  all_same,
  cached_property,
  final,
  get_process_thread_pool,
  iattr,
  override,
  ureg,
)

from .sql import (
  DmlContext,
  FnNormalFunction,
  LazyNsEntry,
  NO_ARGS,
  Namespace,
  ReplaceSchemaQuery,
  Select,
  TableKind,
  TableSchema,
  add_sql_documentation,
  path2str,
)

from .sql_parser import parse_select, parse_dml
from .trace_file_parser import (
  ParsedTrace,
  parse_mapped_trace,
)
# TODO(dancol): rename IndexedEvents
from .event_parser import IndexedEvents, IndexEvents

log = logging.getLogger(__name__)

# TODO(dancol): restore multi-process support

@once()
def _get_stdlib_ast():
  stdlib_file_name = pjoin(dirname(__file__), "stdlib.sql")
  with open(stdlib_file_name) as stdlib_file:
    return parse_dml(stdlib_file.read())

_TRACE_ID_SEQ = xcount()

@final
class MagicRawEventNamespace(Namespace):
  """Namespace that auto-creates raw event tables"""

  @override
  def __init__(self, trace_context):
    super().__init__()
    assert isinstance(trace_context, TraceContext)
    self.__trace_context_wr = weakref.ref(trace_context)  # Help out GC

  @override
  def __missing__(self, key):
    # TODO(dancol): do we really need this auto-create behavior?
    trace_context = self.__trace_context_wr()
    if trace_context:
      table = trace_context.make_event_query_table(key)
      self[key] = table
      return table
    return super().__missing__(key)

EVENT_PARTITIONED_RAW_TID = TableSchema(
  TableKind.EVENT,
  "raw_tid",
  TableSorting.TIME_MAJOR)
EVENT_PARTITIONED_RAW_TGID = TableSchema(
  TableKind.EVENT,
  "raw_tgid",
  TableSorting.TIME_MAJOR)

@final
class MagicCookedEventNamespace(Namespace):
  """Namespace that transforms raw events to usable ones

  Transparently remap tids and tgids to their unique equivalents.
  """

  @override
  def __init__(self, trace_ns_wr):
    super().__init__()
    self.__trace_ns_wr = trace_ns_wr

  @staticmethod
  def __make_cooked_event_table(trace_ns, key):
    raw_qt = trace_ns["raw_events"][key]\
      .to_schema(sorting=TableSorting.TIME_MAJOR)
    def _cook_column(query):
      domain = query.schema.domain
      if domain == "raw_tid":
        query = trace_ns["internal"]["remap_raw_tid"](
          GenericQueryTable({
            "_ts": raw_qt["_ts"],
            "cpu": raw_qt["cpu"],
            "raw_tid": query,
          }, table_schema=EVENT_PARTITIONED_RAW_TID))["utid"]
      if domain == "raw_tgid":
        query = trace_ns["internal"]["remap_raw_tgid"](
          GenericQueryTable((
            ("_ts", raw_qt["_ts"]),
            ("raw_tgid", query),
          ), table_schema=EVENT_PARTITIONED_RAW_TGID))["utgid"]
      return query
    return raw_qt.transform(_cook_column)

  @override
  def __missing__(self, key):
    trace_ns = self.__trace_ns_wr()
    if trace_ns:
      table = self.__make_cooked_event_table(trace_ns, key)
      self[key] = table
      return table
    return super().__missing__(key)

def utid_of_cpu_idler(v):
  """Given a CPU number, return the synthetic idler utid"""
  q = QueryNode.coerce_(v)
  if q.schema.domain != "cpu":
    raise InvalidQueryException(
      "expected a cpu number: found {}".format(q.schema))
  q = (QueryNode.scalar(DTYPE_ID_VALUE.type(SYSTEM_UTID_BASE)) -
       ReplaceSchemaQuery(q, q.schema.evolve(domain=None)))
  return ReplaceSchemaQuery(q, q.schema.evolve(
    domain="utid")).to_dtype(DTYPE_ID_VALUE, safe=False)

@final
class TraceAnalysisSession(ClosingContextManager, ExplicitInheritance):
  """Top-level environment for analyzing trace files"""

  @override
  def __init__(self, threads=False):
    """Create a new analysis session

    THREADS controls whether to allow certain query operators to
    pipeline their work with threads.  the query thread.  False means
    to do all work in the query thread itself.  True uses the process
    thread pool, which is sized according to the core count on
    the system.
    """
    self.__dmlctx = TraceAnalysisDmlContext(self)
    self.__tmpdir = TmpDir(prefix="dctv-spill-")
    self.__env = {
      "get_trace_context_by_trace_id": self.__get_trace_context_by_trace_id,
    }
    if the(bool, threads):
      self.__env["get_tp"] = get_process_thread_pool
    self.__traces_by_id = {}
    self.__qe = None  # Shut up lint
    self.reconfigure_cache()

  def reconfigure_cache(self, *, block_size=None):
    """Change cache parameters"""
    qc_kwargs = {}
    if block_size is not None:
      qc_kwargs["block_size"] = block_size
    cache = QueryCache(tmpdir=self.__tmpdir, **qc_kwargs)
    self.__qe = QueryEngine(cache, env=self.__env)

  @property
  def qe(self):
    """The query engine itself"""
    return self.__qe

  @property
  def qc(self):
    """The query cache"""
    return self.__qe.qc

  def clear_query_cache(self):
    """Clear the query cache"""
    self.qc.clear()

  @property
  def st(self):
    """String table in use for this analysis"""
    return self.__qe.st

  @property
  def dmlctx(self):
    """DML context for trace querying"""
    return self.__dmlctx

  @property
  def user_ns(self):
    """The user SQL namespaace"""
    return self.__dmlctx.user_ns

  @override
  def _do_close(self):
    self.__dmlctx = None
    self.__env.clear()
    for trace_context in self.__traces_by_id.values():
      safe_close(trace_context)

  @staticmethod
  def allocate_trace_id(trace_file_name):
    """Allocate a unique trace ID for contexts"""
    return (trace_file_name, next(_TRACE_ID_SEQ))

  @staticmethod
  def __make_raw_events_ns(trace_context):
    ns_raw_events = MagicRawEventNamespace(trace_context)
    add_sql_documentation(ns_raw_events, """\
    Raw events extracted directly from the trace
    """)
    for event_type in trace_context.known_event_types:
      event_table = trace_context.make_event_query_table(event_type)
      ns_raw_events[event_type] = event_table
    return ns_raw_events

  def __prepare_trace_ns(self, trace_context, trace_id, sql_prefix):
    # We don't have a plugin for events, so we just hardcode the
    # raw_events stuff.  Other convenience facilities come from
    # plugins and model.sql.
    ns = Namespace()
    # Make sure we forget about the trace after its
    # namespace disappears.
    self_wr = weakref.ref(self)
    def _on_ns_dead():
      self_ = self_wr()
      if self_:
        # pylint: disable=protected-access
        self_.__on_trace_removed(trace_id)
    weakref.finalize(ns, _on_ns_dead)

    ns_wr = weakref.ref(ns)

    # Actually populate the trace namespace.  Most of the
    # functionality comes from model.sql.  Here, we just define a few
    # of the more magical constructs that we can't yet express in SQL.
    ns["raw_events"] = self.__make_raw_events_ns(trace_context)
    ns["cooked_events"] = MagicCookedEventNamespace(ns_wr)
    ns["metadata"] = ns_meta = Namespace()
    ns["internal"] = ns_internal = Namespace()
    ns_internal.disable_autocomplete = True

    weak_trace_context = weakref.ref(trace_context)
    def _get_snapshot_data(snapshot_number):
      return weak_trace_context().get_snapshot_data(snapshot_number)
    ns_internal["get_snapshot_data"] = _get_snapshot_data
    metadata = trace_context.metadata
    nr_cpus = DTYPE_CPU_NUMBER.type(metadata["nr_cpus"])
    ns_meta["nr_cpus"] = QueryNode.scalar(nr_cpus).as_table("nr_cpus")
    if "enabled_events" in metadata:
      ns_meta["enabled_events"] = QueryNode.literals(
        *sorted(metadata["enabled_events"])).as_table("event_name")
    # TODO(dancol): input events aren't guaranteed to be strictly
    # ordered, so first_ts and last_ts here may be wrong.
    ns["first_ts"] = first_ts_qt = GenericQueryTable(
      {"_ts": QueryNode.scalar(ureg().ns*0)},
      table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
    add_sql_documentation(first_ts_qt, """\
    First timestamp in the trace
    """)
    ns["last_ts"] = last_ts_qt = GenericQueryTable(
      {"_ts": trace_context.get_last_ts_query()},
      table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)
    add_sql_documentation(last_ts_qt, """\
    Last timestamp in the trace
    """)
    ns["raw_event_index"] = raw_event_index = LazyNsEntry(
      lambda: IndexEvents(trace_id=trace_id).make_query_table())
    add_sql_documentation(raw_event_index, """\
    Raw file-level information about each event in the trace
    """)
    ns["__colfunc_utid_of_cpu_idler"] = \
      FnNormalFunction(utid_of_cpu_idler)
    dctx = DmlContext(ns, self.__qe)
    if sql_prefix:
      dctx.execute(parse_dml(sql_prefix))
    dctx.execute(_get_stdlib_ast())
    return ns

  def __on_trace_removed(self, trace_id):
    # log.warning("Unmounting trace: flushing query cache! "
    #             "TODO(dancol): implement fine-grained tracking!")
    with exceptions_are_fatal("trace cleanup"):
      self.qc.clear()
      safe_close(self.__traces_by_id.pop(trace_id, None))

  def mount_trace(self,
                  mount_path,
                  trace_context,
                  *,
                  sql_prefix=None,
                  temp_hack_lenient_metadata=False):
    """Mount an already-created TraceContext"""
    assert isinstance(trace_context, TraceContext)
    trace_id = trace_context.trace_id
    assert trace_id not in self.__traces_by_id
    if not temp_hack_lenient_metadata:
      assert "nr_cpus" in trace_context.metadata
    try:
      self.__traces_by_id[trace_id] = trace_context
      self.__dmlctx.user_ns.assign_by_path(
        mount_path,
        self.__prepare_trace_ns(trace_context, trace_id, sql_prefix))
      ret = trace_context
      trace_context = None
      trace_id = None
    finally:
      safe_close(trace_context)
      self.__traces_by_id.pop(trace_id, None)
    return ret

  def __get_trace_context_by_trace_id(self, trace_id):
    return self.__traces_by_id[trace_id]

  def get_mounted_trace(self, path):
    """Return the raw trace object mounted at a namespace prefix

    Specialized uses only; most of the time, you want to learn things
    about the trace by making SQL queries.
    """
    # Just steal the trace ID from a dummy query.  We don't actually
    # execute the query.  This is such a hack.
    assert not isinstance(path, str)
    q = "SELECT _ts FROM {}".format(path2str(list(path) + ["last_ts"]))
    qn = parse_select(q).make_qt(self.dmlctx.make_tctx())["_ts"]
    assert isinstance(qn, LastTsQuery)
    return self.__traces_by_id[qn.trace_id]

  def parse_sql_query(self, query, args=NO_ARGS):  # pylint: disable=dangerous-default-value
    """Build, but do not run, a SQL query.

    Useful for probing whether a query would work, but without
    attempting to actually run it.  Raises exceptions (mostly
    .query.InvalidQueryException subclasses) on failure.

    Return the QueryTable object we build from the query.
    """
    # TODO(dancol): cache query parses?
    ast = parse_select(query)
    return ast.make_qt(self.dmlctx.make_tctx(args))

  @staticmethod
  def __sql_query_by_column(qe, qt, munge, **kwargs):
    # Two differently-named columns can resolve to the same QueryNode
    # object and the query engine gives us only QueryNode objects, so
    # we need to be able to find all the column names for a
    # given QueryNode.
    qn_to_column_name = defaultdict(list)
    for column_name in qt.columns:
      qn_to_column_name[qt[column_name]].append(column_name)
    for qn, array in qe.execute_for_columns(
        qn_to_column_name.keys(), **kwargs):
      schema = qn.schema
      column_data = munge(array, schema)
      del array
      for column_name in qn_to_column_name[qn]:
        yield column_name, column_data

  @staticmethod
  def __sql_query_by_row(qe, qt, munge, **kwargs):
    # N.B. make sure to add to the row-chunk dicts in column order!
    columns = tuple(qt.columns)
    assert columns
    qn_by_column = {
      column: qt[column] for column in columns
    }
    queries = set(qn_by_column.values())
    arrays_by_qn = {qn: [] for qn in queries}
    arrays = tuple(arrays_by_qn.values())
    query_iter = qe.execute(queries,
                            delivery_hint=Delivery.ROWWISE,
                            **kwargs)
    def _get_row_chunk():
      nr_eof = 0
      while not all(arrays):
        try:
          qn, array, is_eof = next(query_iter)
        except StopIteration:
          raise RuntimeError("query iter stopped early!")
        nr_eof += is_eof
        arrays_by_qn[qn].append(munge(array, qn.schema))
      assert not nr_eof or nr_eof == len(queries), \
        "all columns should be at EOF or none shold be"
      assert all_same(len(q_arrays[0]) for q_arrays in arrays)
      data_by_qn = {qn: array.pop(0) for qn, array in zip(queries, arrays)}
      data = {
        column: data_by_qn[qn_by_column[column]]
        for column in columns
      }
      return data, bool(nr_eof)
    is_eof = False
    while not is_eof:
      row_chunk_data, is_eof = _get_row_chunk()
      yield row_chunk_data, is_eof
    try:
      next(query_iter)
    except StopIteration:
      pass
    else:
      raise AssertionError("query_iter should be done")

  def sql_query_to_qt(self, query, query_args=NO_ARGS):  # pylint: disable=dangerous-default-value
    """Transform a SQL query to a QueryTable

    QUERY can be SQL query text, or a SQL AST, or a QueryTable.  (In
    this last case, the query is returned unchanged.)

    The return value, a QueryTable, is suitable for passing to
    sql_query().
    """
    if isinstance(query, QueryTable):
      assert not query_args, (
        "cannot specify SQL args if "
        "query is already baked into a QueryTable")
      return query
    ast = query if isinstance(query, Select) else parse_select(query)
    return ast.make_qt(self.dmlctx.make_tctx(query_args))

  def sql_query(self, query, *,
                query_args=NO_ARGS,
                strings=str,
                output_format="columns",
                want_schema=False,
                perf_callback=None):
    """High-level interface to performing SQL queries.

    QUERY is a string containing a SQL query to perform.  QUERY_ARGS,
    if present, is a query arguments object that provides values for
    the parameter references in QUERY.  QUERY can also be a
    pre-computed SELECT AST or a QueryTable.

    If OUTPUT_FORMAT is "columns", yield COLUMN_RESULT tuples, where
    each COLUMN_RESULT is (COLUMN_NAME, COLUMN_DATA), and COLUMN_DATA
    is a pair (COLUMN_ARRAY, COLUMN_SCHEMA).  COLUMN_NAME is the name
    of the column, COLUMN_ARRAY is a numpy array describing the data
    in the column, and COLUMN_SCHEMA is a QuerySchema object
    describing the result generated.

    If OUTPUT_FORMAT is "rows", yield a ROW_CHUNK tuples, in which
    each ROW_CHUNK is (ROW_CHUNK_DATA, IS_EOF).  ROW_CHUNK_DATA is a
    mapping from column name to COLUMN_DATA as above; the column
    schemas are the same in every chunk.  IS_EOF is a boolean
    indicating whether the given row chunk is the last row chunk.

    If WANT_SCHEMA is true, COLUMN_DATA is as above.  If, however,
    WANT_SCHEMA is false, then COLUMN_DATA is just COLUMN_ARRAY,
    omitting COLUMN_SCHEMA.

    All column vectors have the same length.

    If STRINGS is None, we return raw string codes.  Otherwise,
    STRINGS is something that StringTable.vlookup accepts for its MODE
    argument and we return string columns as numpy object arrays.

    N.B. don't use this function if you want maximum control over
    query execution for advanced tasks, e.g., if you want to multiple
    SQL queries simultaneously.  Instead, parse the SQL query to a
    QueryTable using self.parse_sql_query() and talk to the underyling
    QueryEngine (accessible via self.qe) directly.  This function has
    no special privileges; you can do by hand everything function
    does, and if you find yourself wondering whether you should, you
    probably just should.
    """
    qt = self.sql_query_to_qt(query, query_args)
    if not qt.columns:
      return  # Nothing to do
    st = self.__qe.st
    string_cache = st.make_lookup_cache(strings) if strings else None
    def _munge(array, schema):
      if string_cache and schema.is_string:
        array, mask = npy_explode(array)
        array = st.vlookup(array, string_cache)
        if mask is not nomask:
          array[mask] = None
      return (array, schema) if want_schema else array
    if output_format == "columns":
      fn = self.__sql_query_by_column
    elif output_format == "rows":
      fn = self.__sql_query_by_row
    else:
      raise ValueError("unknown output format {!r}".format(output_format))
    yield from fn(self.__qe, qt, _munge, perf_callback=perf_callback)

  def sql_query1(self, sql, **kwargs):
    """Like sql_query(), but return one column's data"""
    assert "output_format" not in kwargs
    result = dict(self.sql_query(sql, **kwargs))
    if len(result) != 1:
      raise ValueError("query generated {} columns but we want one"
                       .format(len(result)))
    return first(result.values())

  def sql_query_scalar(self, sql, **kwargs):
    """Like sql_query1(), but expect only one value"""
    assert "want_schema" not in kwargs
    result = self.sql_query1(sql, **kwargs)
    if len(result) != 1:
      raise ValueError("query generated {} rows but we want one"
                       .format(len(result)))
    return result.tolist()[0]

class TraceContext(ClosingContextManager, ExplicitInheritance):
  """Per-trace information"""

  @override
  def __init__(self, name):
    self.__trace_id = TraceAnalysisSession.allocate_trace_id(name)

  @cached_property
  def trace_id(self):
    """Opaque identifying object for this trace context"""
    return self.__trace_id

  @cached_property
  def metadata(self):
    """Metadata for this trace as a dict"""
    return self._get_metadata()

  @cached_property
  def known_event_types(self):
    """Sequence of event types that we know about"""
    return self._get_known_event_types()

  @abstract
  def make_event_query_table(self, event_type):
    """Make a QueryTable for event type EVENT_TYPE"""
    raise NotImplementedError("abstract")

  @abstract
  def _get_metadata(self):
    raise NotImplementedError("abstract")

  @abstract
  def _get_known_event_types(self):
    raise NotImplementedError("abstract")

  @abstract
  def get_last_ts_query(self):
    """Get a QueryNode for the last query timestamp"""
    raise NotImplementedError("abstract")

  @abstract
  def get_snapshot_data(self, snapshot_number):
    """Get the Snapshot object for the numbered snapshot"""
    raise NotImplementedError("abstract")

@final
class FileTraceContext(TraceContext):
  """Per-trace-file information"""

  @override
  def __init__(self,
               trace_file_name,
               *,
               force_ftrace=False,
               time_basis_override=None):
    mapped_trace = MappedTrace(trace_file_name)
    from .plugins import get_all_event_schemas
    parse_result = parse_mapped_trace(
      mapped_trace=mapped_trace,
      force_ftrace=force_ftrace,
      time_basis_override=time_basis_override,
      event_schemas=get_all_event_schemas())
    super().__init__(trace_file_name)
    assert isinstance(parse_result, ParsedTrace)
    self.mapped_trace = mapped_trace
    self.events = parse_result.events
    self.__metadata = parse_result.metadata

  @override
  def make_event_query_table(self, event_type):
    """Make event table"""
    return self.events.make_event_query_table(
      event_type=event_type,
      trace_id=self.trace_id)

  @override
  def _get_metadata(self):
    return self.__metadata

  @override
  def _get_known_event_types(self):
    return self.events.event_schemas

  @override
  def _do_close(self):
    safe_close(self.mapped_trace)

  @override
  def get_last_ts_query(self):
    return LastTsQuery(self.trace_id)

  @override
  def get_snapshot_data(self, snapshot_number):
    stext = self.__metadata["snapshots"][snapshot_number]
    ss = Snapshot()
    ss.parse_text_dctv_snapshot(stext)
    return ss

class TraceAnalysisDmlContext(DmlContext):
  """DmlContext that talks to a TraceAnalysisSession"""

  @override
  def __init__(self, session):
    super().__init__()
    self.__session = weakref.ref(the(TraceAnalysisSession, session))

  @override
  def mount_trace(self, mount_path, trace_file_name):
    self.__session().mount_trace(
      mount_path,
      FileTraceContext(trace_file_name))

@final
class LastTsQuery(SimpleQueryNode):
  """Query yielding the last event timestamp"""

  trace_id = iattr()

  @cached_property
  def __last_position_query(self):
    # pylint: disable=protected-access
    events = IndexEvents(trace_id=self.trace_id)
    q = events.metaq(IndexEvents.Meta._POS)
    q = q.slice(-1)
    return q

  @override
  def _compute_schema(self):
    return TS_SCHEMA

  @override
  def _compute_inputs(self):
    return (self.__last_position_query,)

  @override
  def countq(self):
    return QueryNode.scalar(1)

  @override
  async def run_async(self, qe):
    [ic_last_pos], [oc] = await qe.async_setup(
      (self.__last_position_query,),
      (self,))
    last_pos = await ic_last_pos.read_int()
    ie = IndexedEvents.from_qe(qe, self.trace_id)
    last_ts = ie.get_event_ts_at(last_pos)
    await oc.write([last_ts], True)
