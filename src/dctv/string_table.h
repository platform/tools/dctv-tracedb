// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <array>
#include <boost/intrusive/list.hpp>

#include "dynamic_bitset.h"
#include "hash_table.h"
#include "map.h"
#include "npy.h"
#include "optional.h"
#include "pcreutil.h"
#include "pythread.h"
#include "pyutil.h"
#include "vector.h"

namespace dctv {

struct StringTable;
struct StringTableEntry;
using StringIdType = int32_t;

struct QueryExecution;

namespace strlink {
using namespace boost::intrusive;
using StringLookupCacheListLink = list_member_hook<>;
}  // namespace strlink

// Cache of Python objects corresponding to string table entries.
struct StringLookupCache final : BasePyObject, SupportsGc {
  explicit StringLookupCache(unique_pyref decode_mode);
  ~StringLookupCache() noexcept;
  StringTable* st;
  strlink::StringLookupCacheListLink link;
  static PyTypeObject pytype;
  void clear() noexcept;
  inline pyref get(StringIdType id) noexcept;
  pyref recompute(StringIdType id, const StringTableEntry& entry);
  void ensure_size(size_t n);

  int py_traverse(visitproc visit, void* arg) const noexcept;

 private:
  unique_pyref decode_mode;
  Vector<unique_pyref> cached;
};

namespace strlink {
using namespace boost::intrusive;
using StringLookupCacheList = list<
  StringLookupCache,
  constant_time_size<false>,
  member_hook<StringLookupCache,
              strlink::StringLookupCacheListLink,
              &StringLookupCache::link>
  >;
}  // namespace strlink

// StringTableEntry is bit like String in that it owns a pointer to
// character data, but we guarantee that the owned buffer is never
// moved in memory, allowing a string_view that points to its buffer
// to remain valid even if the owning StringTableEntry moves around.
// We don't want to just refer to bytes inside a Python bytes object.
struct StringTableEntry final : NoCopy {
  inline StringTableEntry() noexcept = default;
  explicit StringTableEntry(std::string_view contents);
  StringTableEntry(StringTableEntry&& other) noexcept;
  ~StringTableEntry();
  StringTableEntry& operator=(StringTableEntry&& other) noexcept;

  inline std::string_view view() const noexcept;
  inline bool valid() const noexcept;
  const char* c_str() const;

 private:
  char* data = nullptr;
  size_t size = 0;
};

// All functions require that the GIL be held unless
// otherwise specified.
struct StringTable final : BasePyObject, SupportsGcClear,
                           HasPyCtor, HasDict,
                           private GilHeldCtorDtor {
  // TODO(dancol): evaluate whether we really want to limit ourselves
  // to 2^31-1 strings.  Do we even detect overflow properly?
  using id_type = StringIdType;

  static_assert(std::numeric_limits<id_type>::max()
                <= std::numeric_limits<size_t>::max());

  // ---------- BEGIN GIL-free interface ----------

  id_type intern(std::string_view key);
  optional<id_type> intern_soft(std::string_view key);
  id_type allocate_id();

  // ---------- END GIL-free interface ----------

  size_t size() const;
  unique_pyarray vintern(pyref byte_obj_seq);

  using SequenceNumber = unsigned long;
  inline SequenceNumber get_seqno() const noexcept;

  const StringTableEntry& lookup(id_type id) const;
  unique_pyarray vlookup(pyarray_ref codes,
                         pyref decode=pyref(),
                         bool subst=false);

  int compare(id_type left,
              id_type right,
              std::string_view collation_name);

  using Rank = id_type;
  unique_pyarray rank(std::string_view collation_name);

  enum class PatternLanguage { PCRE, SQL_LIKE, SQL_ILIKE };
  using Pattern = std::pair<id_type, PatternLanguage>;

  bool match1(Pattern pattern, id_type subject);
  unique_pyarray match(QueryExecution* qe,
                       pyarray_ref subjects,
                       pyarray_ref patterns,
                       int pattern_language);
  unique_pyref concat(QueryExecution* qe,
                      pyarray_ref left,
                      pyarray_ref right);

  String translate_pattern_to_regexp(Pattern pattern) const;

  // Make an array mapping each code in this table to the
  // corresponding code in OTHER.  C++ can make this table a lot more
  // efficiently than Python can by avoiding
  // string-object materialization.
  unique_pyarray make_translation_table(obj_pyref<StringTable> other)
      const;

  StringTable();
  StringTable(pyref args, pyref kwds);
  ~StringTable() noexcept;

  int py_traverse(visitproc visit, void* arg) const noexcept;
  int py_clear() noexcept;

  static PyTypeObject pytype;

 private:
  friend struct StringLookupCache;

  struct Comparator final {
    explicit inline Comparator(std::string_view name);
    inline bool is_lt(std::string_view left, std::string_view right) const noexcept;
    enum class Kind {
      BINARY,
      LENGTH,
      NOCASE,
    };
    const Kind kind;
    static Kind decode_kind(std::string_view name);
  };

  struct Matcher {
    Matcher(const StringTable* st, Pattern);
    Pattern get_pattern() const { return this->pattern; }
    bool match(const StringTable* st, id_type subject_sid);
    bool match_uncached(const StringTable* st, id_type subject_sid);
   private:
    Pattern pattern;
    unique_pcre2_code compiled_pattern;
    // One entry per string ID.  Zero is indeterminate; negative is no
    // match; positive is match.
    mutable Vector<int8_t> match_state_by_sid;
    unique_pcre2_match_data match_data;
  };

  unique_pyarray rank_1(Comparator c) const;
  unique_pcre2_code compile_pattern(Pattern pattern) const;
  Matcher& get_matcher_for_pattern(Pattern pattern);
  unique_obj_pyref<StringLookupCache> make_lookup_cache(pyref decode_mode);

  unique_pyref py_intern(pyref args);
  unique_pyref py_get_c2s(void*);

  unique_pyref py_getitem(Py_ssize_t id);
  Py_ssize_t py_length() noexcept;
  int py_contains(PyObject* value);

  DCTV_NORETURN_ERROR
  void throw_invalid_id(id_type id) const;

  // You might think "can't we use Boost's Bimap?". The problem with
  // that is that we want the code->string lookup to be super-fast,
  // and Boost.MultiIndex (on which Boost.Bimap is based) doesn't
  // provide a key-based "sparse vector" index type, only map and
  // unordered_map equivalents. If we use MultiIndex, we'd have to use
  // a hash table in the code->string direction, which is less
  // efficient than what we can do by hand.

  SequenceNumber seqno = 1;
  HashTable<std::string_view, id_type> s2c;
  Vector<StringTableEntry> c2s;
  DynamicBitset<uint64_t> free_slots;
  id_type next_free_id = 0;
  Map<String, unique_pyarray, std::less<>> rank_cache;
  strlink::StringLookupCacheList lookup_caches;

  mutable optional<Matcher> matcher_cache;

  static PyGetSetDef pygetset[];
  static PyMemberDef pymembers[];
  static PySequenceMethods pyseq[];
  static PyMethodDef pymethods[];
};

void init_string_table(pyref m);

}  // namespace dctv

#include "string_table-inl.h"
