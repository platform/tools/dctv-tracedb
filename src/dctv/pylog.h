// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include "fmt.h"
#include "pyutil.h"

namespace dctv {

struct Logger final {
  enum class Level {
    CRITICAL = 50,
    FATAL = CRITICAL,
    ERROR = 40,
    WARNING = 30,
    INFO = 20,
    DEBUG = 10,
  };

  pyref init();
#define L(name, level)                                          \
  template<typename... Args>                                    \
  void name(const char* format, Args&&... args) const {         \
    this->log(level, format, std::forward<Args>(args)...);      \
  }
  L(critical, Level::CRITICAL);
  L(fatal, Level::FATAL);
  L(error, Level::ERROR);
  L(warning, Level::WARNING);
  L(info, Level::INFO);
  L(debug, Level::DEBUG);
#undef L
  template<typename... Args>
  void log(Level level, const char* format, Args&&... args) const {
    this->_log(level, fmt(format, std::forward<Args>(args)...));
  }
  void _log(Level level, std::string_view msg) const;

 private:
  unique_pyref logger;
};

extern Logger pylog;  // Global for syntactic convenience

inline void init_pylog(pyref m) {}

}  // namespace dctv
