// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <limits.h>
#include <stdlib.h>
#include <type_traits>

namespace dctv {

// Find the last (most significant) bit set in a word.  fls(0) is
// defined to be zero.  Intended to work identically to the
// similarly-named Linux function.
template<typename T>
inline int fls(T value) noexcept;

// Find the first (least significant) bit set in a word.  ffs(0) is
// defined to be zero.  Intended to work identically to the
// similarly-named Linux function.
template<typename T>
inline int ffs(T value) noexcept;

// Divide unsigned DIVIDEND by unsigned DIVISOR_POW2, yielding the
// floor of the result.  As the name suggests, DIVISOR_POW2 must be a
// power of two.  Zero is not a power of two.
template<typename T>
inline T pow2_divide(T dividend, T divisor_pow2) noexcept;

// Round unsigned NR down to the previous multiple of THE_POW2.
// THE_POW2 must be a power of two.  Zero is not a power of two.
template<typename T>
inline T pow2_round_down(T nr, T the_pow2) noexcept;

// Round unsigned NR up to the next multiple of THE_POW2.  THE_POW2
// must be a power of two.  Zero is not a power of two.
template<typename T>
inline T pow2_round_up(T nr, T the_pow2) noexcept;

// Determine whether an unsigned number is a power of two.  Zero is
// not a power of two.
template<typename T>
inline bool is_pow2(T nr) noexcept;

}  // namespace dctv

#include "bitbash-inl.h"
