# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Teach pylint about our metaprogramming"""
import sys
import logging
import warnings

import astroid
from astroid import MANAGER

from .gtk_util import gtk_noop
gtk_noop()  # So gtk_util looks used

log = logging.getLogger(__name__)

warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

_BUILDER = astroid.builder.AstroidBuilder(MANAGER)

def register(_linter):
  """Dummy?"""
  # Needed for registering the plugin.
  pass

def is_dctv(cls):
  """Determine whether a module is a dctv one"""
  return cls.root().name.startswith("dctv.")



def is_immutable(cls):
  """Determine whether a class is our custom immutable stuff"""
  if not cls.qname().startswith("dctv."):
    return False
  for ancestor in cls.ancestors():
    for aval in ancestor.infer():
      if aval.qname() == "dctv.util.Immutable":
        return True
  return False

def _is_immutable_attr(node):
  return (isinstance(node, astroid.Call) and
          isinstance(node.func, astroid.Name) and
          node.func.name in ("iattr", "tattr", "sattr", "enumattr"))

def extract_attr_types(call, context):
  """Extract from an attribute its known types and yield them"""
  assert isinstance(call, astroid.Call)
  args = call.args
  if not args:
    return
  for inferred_arg in call.args[0].infer(context=context):
    if isinstance(inferred_arg, astroid.ClassDef):
      yield inferred_arg
    elif isinstance(inferred_arg, astroid.Tuple):
      for item in inferred_arg.elts:
        for item_res in item.infer(context=context):
          if isinstance(item_res, astroid.ClassDef):
            yield item_res
          else:
            log.debug("unknown tuple member %r", item_res)
            raise astroid.InferenceError
    else:
      log.debug("unknown inferred_arg %r", inferred_arg)
      raise astroid.InferenceError

def _infer_immutable_attr_tattr(thing, context):
  found = False
  for type_ in extract_attr_types(thing, context):
    value = astroid.Tuple()
    value.postinit([type_.instantiate_class()])
    found = True
    yield value
  if not found:
    yield astroid.Tuple()

def _infer_immutable_attr_sattr(thing, context):
  found = False
  for type_ in extract_attr_types(thing, context):
    value = astroid.Set()
    value.postinit([type_.instantiate_class()])
    found = True
    yield value
  if not found:
    yield astroid.Set()

def _infer_immutable_attr_iattr(thing, context):
  found = False
  for type_ in extract_attr_types(thing, context):
    found = True
    yield type_.instantiate_class()
  if not found:
    raise astroid.InferenceError

def _infer_immutable_attr_enumattr(thing, context):
  found = False
  for type_ in extract_attr_types(thing, context):
    if not isinstance(type_, astroid.ClassDef):
      raise astroid.InferenceError
    if len(type_.bases) != 1:
      raise astroid.InferenceError
    for inferred_base in type_.bases[0].infer():
      for value_type in inferred_base.getattr(
          "value_type", context=context):
        for vt in value_type.infer(context=context):
          if isinstance(vt, astroid.ClassDef):
            found = True
            yield vt.instantiate_class()
  if not found:
    raise astroid.InferenceError

def _infer_immutable_attr(thing, context=None):
  if not isinstance(thing, astroid.Call):
    raise astroid.UseInferenceDefault
  for func in thing.func.infer(context=context):
    if not func.is_function:
      raise astroid.InferenceError
    qname = func.qname()
    if qname == "dctv.util.tattr":
      yield from _infer_immutable_attr_tattr(thing, context)
    elif qname == "dctv.util.sattr":
      yield from _infer_immutable_attr_sattr(thing, context)
    elif qname == "dctv.util.iattr":
      yield from _infer_immutable_attr_iattr(thing, context)
    elif qname == "dctv.util.enumattr":
      yield from _infer_immutable_attr_enumattr(thing, context)
    else:
      log.warning("unknown immutable attr qname %r", qname)
      raise astroid.InferenceError

MANAGER.register_transform(
  astroid.Call,
  astroid.inference_tip(_infer_immutable_attr),
  _is_immutable_attr)

def _fix_immutable_class(cls):
  assert is_immutable(cls)
  # Just load the class for real and inspect its brains
  if not cls.parent:
    return
  frame = cls.parent.frame()
  if isinstance(frame, astroid.Module):
    from importlib import import_module
    module = import_module(frame.qname())
    real_cls = getattr(module, cls.name)
    newfn_source = real_cls.get_newfn_source()
    fake_module = _BUILDER.string_build(newfn_source)
    fake_new = fake_module["__new__"]
    fake_evolve = fake_module["evolve"]
  else:
    # Can't get the real argument list because the class is embedded
    # inside something, so just supply a catch-all.
    fake_new = _BUILDER.string_build("""\
def __new__(cls, *args, **kwargs):
  pass
""")["__new__"]
    fake_evolve = _BUILDER.string_build("""\
def evolve(cls, *args, **kwargs):
  pass
""")["evolve"]

  fake_new.parent = cls
  if "__new__" in cls.locals:
    fake_new.name = "_do_new"
    cls["_do_new"] = fake_new
  else:
    cls["__new__"] = fake_new
  cls["evolve"] = fake_evolve
  cls["__immutable_key__"] = ()  # hack

MANAGER.register_transform(
  astroid.ClassDef,
  _fix_immutable_class,
  is_immutable)



def _is_brain_suck_ast(cls):
  return any(kw.arg == "brain_suck_abc" for kw in cls.keywords)

def _fix_brain_suck_ast(cls, context=None):  # pylint: disable=unused-argument
  brain_suck_abc = None
  for kw in cls.keywords:
    if kw.arg == "brain_suck_abc":
      brain_suck_abc = kw.value
  if brain_suck_abc:
    cls.bases.append(brain_suck_abc)

MANAGER.register_transform(
  astroid.ClassDef,
  _fix_brain_suck_ast,
  _is_brain_suck_ast)



def _infer_attrs_wrapper_instances(raw_types, context):
  for inferred_type in raw_types.infer(context):
    if isinstance(inferred_type, astroid.Tuple):
      for ctype in inferred_type.itered():
        for ctype_inferred in ctype.infer(context):
          yield ctype_inferred.instantiate_class()
    else:
      yield inferred_type.instantiate_class()

def _infer_attrs_wrapper(thing, context=None):
  raw_types = thing.args[0]
  if thing.func.name == "_tattr":
    for inferred_instance in \
        _infer_attrs_wrapper_instances(raw_types, context):
      tvalue = astroid.Tuple(
        ctx=context,
        lineno=thing.lineno,
        col_offset=thing.col_offset,
        parent=thing.parent,
      )
      tvalue.postinit([inferred_instance])
      yield tvalue
  else:
    yield from _infer_attrs_wrapper_instances(raw_types, context)

def _is_attrs_wrapper(node):
  return (isinstance(node, astroid.Call) and
          isinstance(node.parent, astroid.Assign) and
          node is node.parent.value and
          isinstance(node.parent.parent, astroid.ClassDef) and
          isinstance(node.func, astroid.Name) and
          node.func.name in ("_cattr", "_tattr") and
          len(node.args) >= 1 and
          isinstance(node.args[0], (astroid.Tuple, astroid.Name)))

def _is_decorated_with_attrs(
    node, decorator_names=('attr.s', 'attr.attrs', 'attr.attributes')):
  """Return True if a decorated node has
  an attr decorator applied."""
  if not node.decorators:
    return False
  for decorator_attribute in node.decorators.nodes:
    if decorator_attribute.as_string() in decorator_names:
      return True
  return False

MANAGER.register_transform(
  astroid.Call,
  astroid.inference_tip(_infer_attrs_wrapper),
  _is_attrs_wrapper)

def _fix_ordered_dict(node):
  node["move_to_end"] = lambda _key, _last=True: True

def _is_ordered_dict(node):
  return isinstance(node, astroid.ClassDef) and node.name == "OrderedDict"

MANAGER.register_transform(
  astroid.ClassDef,
  _fix_ordered_dict,
  _is_ordered_dict)

def _fix_numpy(module):
  if module.name == "numpy":
    module["floating"] = module["float64"]

MANAGER.register_transform(
  astroid.Module,
  _fix_numpy)

logging.basicConfig(level=logging.DEBUG)

def _munge():
  brain_attrs = sys.modules.get("brain_attrs")
  if not brain_attrs:
    return
  def _is_not_ba(item):
    _transform, predicate = item
    return predicate is not brain_attrs.is_decorated_with_attrs
  # pylint: disable=protected-access
  transforms = MANAGER._transform.transforms
  for node_class, orig_tlist in tuple(transforms.items()):
    if node_class != astroid.ClassDef:
      continue
    tlist = list(filter(_is_not_ba, orig_tlist))
    transforms[node_class] = tlist
_munge()

astroid.bases.POSSIBLE_PROPERTIES.add("cache_readonly")
astroid.bases.POSSIBLE_PROPERTIES.add("gobject_property_object")
astroid.bases.POSSIBLE_PROPERTIES.add("gobject_property_str")
astroid.bases.POSSIBLE_PROPERTIES.add("CachedProperty")

ManagerType = type(MANAGER)
