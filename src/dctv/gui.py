# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""GUI core"""
from os.path import dirname, join as pjoin
import logging
import asyncio

from modernmp.util import the
from modernmp.apartment import ApartmentFactory, proxy_for

from .util import (
  Immutable,
  NoneType,
  future_exception,
  iattr,
  xabspath,
)

from .gtk_util import (
  GLib,
  GObject,
  Gdk,
  Gio,
  Gtk,
  add_ui_template,
  gobject_property_object,
  gobject_property_str,
  safe_destroy,
)
from .gui_remote import (
  RemoteSession,
  StudyResult,
)

from .sql_util import (
  SqlBundle,
)

from .pivot_engine import (
  AsyncQueryEngine,
)

from .pivot import (
  PivotTable,
)

# IMPORTANT!!!!! Avoid calls into GObject bindings with fewer than the
# documented number of arguments.  These calls can cause memory
# corruption due to a bug in pygobject.
# See https://bugzilla.gnome.org/show_bug.cgi?id=786872.

# To check whether we're making any of these calls, run Python under
# GDB, break on pyglib__gi_module_create, then set a watchpoint like
# so: watch -l _PyGIDefaultArgPlaceholder->ob_refcnt.  When that
# watchpoint hits, run py-bt to see the Python code that's triggering
# the pygobject bug.

# Another option would be to disable GC, since the buggy pygobject
# placeholder object is reachable only through GC inspection of active
# stack frames.

log = logging.getLogger(__name__)
mydir = dirname(__file__)  # pylint: disable=invalid-name

class GuiConfig:
  """Configuration for GUI; comes from main"""
  def __init__(self):
    pass  # TODO(dancol): remove now that it's empty
  @staticmethod
  def get(widget):
    """Return the GuiConfig for a widget in a hierarchy"""
    while widget:
      if hasattr(widget.props, "gui_config"):
        return widget.props.gui_config
      widget = widget.get_parent()
    raise ValueError("Widget has no parent providing a GuiConfig")

class CpuGraph(Gtk.Grid):
  """Displays a graph of CPU time"""
  __gtype_name__ = "CpuGraph"

  def __init__(self, *, trace, **kwargs):
    super().__init__(**kwargs)
    self.__trace = trace
    label = Gtk.Label(
      "[cpu graph goes here]",
      name="graph-placeholder",
      hexpand=True,
      vexpand=True,
      halign=Gtk.Align.CENTER,
      valign=Gtk.Align.CENTER)
    self.attach(label, 0, 0, 1, 1)
    label.show()

class TraceIntroWidget(Gtk.Bin):
  """Widget shown inside a TraceWidgetShell when we don't have a trace loaded"""
  __gtype_name__ = "TraceIntroWidget"
  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.init_template()
add_ui_template(TraceIntroWidget)

class FutureProgressWidget(Gtk.Grid):
  """Display progress information while waiting for a future to complete"""
  __gtype_name__ = "FutureProgressWidget"

  __gsignals__ = {
    "loaded": (GObject.SIGNAL_RUN_FIRST, None, ())
  }

  PULSE_INTERVAL_MS = 100
  PULSE_PERIOD_MS = 5000

  def __init__(self, future_factory, **kwargs):
    """Create a new FutureProgressWidget.

    FUTURE_FACTORY is a function of one argument that returns a future
    that represents the loading process. The single argument to
    FUTURE_FACTORY is a modernmp-compatible callable to which whatever
    code generates the future value should write progress information.

    (We pass a factory instead of a future so that the future creator
    can see the progress callback, which we can't create until the
    widget exists.)
    """
    self.__pulse_timer = None
    super().__init__(**kwargs)
    self.init_template()
    progress_bar = self.__progress_bar
    pulse_step = self.PULSE_INTERVAL_MS / self.PULSE_PERIOD_MS
    progress_bar.props.pulse_step = pulse_step
    # pylint: disable=protected-access
    progress_callback = proxy_for(self)._update_progress.call_oneway
    self.__future = asyncio.ensure_future(future_factory(progress_callback))
    self.__future.add_done_callback(self.__on_future_complete)
    self.__start_pulse_timer()

  @property
  def __progress_bar(self):
    return self.get_template_child(self, "progress")

  def __start_pulse_timer(self):
    if self.__pulse_timer is None:
      self.__pulse_timer = GLib.timeout_add(
        self.PULSE_INTERVAL_MS,
        self.__on_pulse_timer,
        None)
      assert self.__pulse_timer is not None

  def __stop_pulse_timer(self):
    if self.__pulse_timer is not None:
      GLib.source_remove(self.__pulse_timer)
      self.__pulse_timer = None

  def __on_pulse_timer(self, _=None):
    self.__progress_bar.pulse()
    return True

  def do_destroy(self):
    """Gtk.Widget protocol"""
    self.__stop_pulse_timer()
    type(self).mro()[1].do_destroy(self)

  def _update_progress(self, message, current=0, total=0):
    progress_bar = self.__progress_bar
    if not progress_bar:
      return  # XXX: race with destruction?!
    if not current and not total:
      progress_bar.pulse()
      self.__start_pulse_timer()
    else:
      self.__stop_pulse_timer()
      progress_bar.set_fraction(current / total)
    progress_bar.set_text(message)

  def __on_future_complete(self, future):
    assert self.__future is future
    if not self.in_destruction():
      self.emit("loaded")

  def consume_result(self):
    """Return and reset result of load"""
    assert self.__future, "result already consumed"
    assert self.__future.done(), "result not yet ready"
    # Transfer ownership to caller
    ret = self.__future
    self.__future = None
    return ret

add_ui_template(FutureProgressWidget, (
  "progress",
))

class ExceptionDisplayWidget(Gtk.Label):
  """Displays an error"""

  def __init__(self, *, exception, **kwargs):
    super().__init__(**kwargs)
    import traceback
    error_text = "".join(traceback.format_exception(
      type(exception),
      exception,
      getattr(exception, "__traceback__", None)))
    self.set_text(error_text)

class TraceFacetSpec(Immutable):
  """Describes what's in a trace facet"""
  tab_name = iattr(str)
  query_bundle = iattr(SqlBundle)
  ### TODO(dancol): other stuff here, like default columns?

class TracePivotTable(Gtk.Bin):
  """Thin wrapper around PivotTable that uses a TraceFacetWidget config"""
  __gtype_name__ = "TracePivotTable"
  __config = None

  def __set_child(self, new_child):
    old_child = self.get_child()
    if old_child is not None:
      safe_destroy(old_child)
    self.add(new_child)
    new_child.show()

  @gobject_property_object
  def config(self):
    """Current description"""
    return self.__config

  @config.setter
  def config(self, config):
    """Set a new config"""
    self.__config = the(TraceFacetWidget.Config, config)
    pivot_table = PivotTable(
      query_engine=config.query_engine,
      base_query=config.spec.query_bundle,
      known_column_info=zip(config.study_result.columns,
                            config.study_result.schemas),
    )
    self.__set_child(pivot_table)

class TraceFacetWidget(Gtk.Bin):
  """Displays a facet of a loaded trace"""
  __gtype_name__ = "TraceFacetWidget"
  __config = None

  class Config(Immutable):
    """Setup information for a TraceFacetWidget"""
    spec = iattr(TraceFacetSpec)
    remote_session = iattr(RemoteSession)
    query_engine = iattr(AsyncQueryEngine)
    study_result = iattr(StudyResult)

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.init_template()

  @gobject_property_object
  def config(self):
    """Current config"""
    return self.__config

  @config.setter
  def config(self, config):
    """Set new config"""
    self.__config = the(self.Config, config)

  @property
  def __pivot(self):
    pivot = self.get_template_child(self, "pivot")
    assert pivot
    return pivot

add_ui_template(TraceFacetWidget, (
  "pivot",
))

class TraceFacetWidgetShell(Gtk.Bin):
  """Displays a pretty load-progress wrapper around a TraceFacetWidget"""
  __gtype_name__ = "TraceFacetWidgetShell"

  class Config(Immutable):
    """The configuration for this loading shell"""
    spec = iattr(TraceFacetSpec)
    remote_session = iattr(RemoteSession)
    query_engine = iattr(AsyncQueryEngine)

  __config = None
  """Configuration we're going to display"""

  def __set_child(self, new_child):
    old_child = self.get_child()
    if old_child is not None:
      safe_destroy(old_child)
    self.add(new_child)
    new_child.show()

  @gobject_property_object
  def config(self):
    """Current facet specification"""
    return self.__config

  @config.setter
  def config(self, config):
    """Set a new facet spec; rebuilds UI"""
    # N.B. The config property defaults to None, but we always _set_ it
    # to a valid config.  This way, we can use the GTK UI XML goo.
    self.__config = the(self.Config, config)
    self.__enter_loading_state()

  def __enter_loading_state(self):
    # We pass a function FutureProgressWidget because only
    # FutureProgressWidget can create the right progress_callback.
    def _future_factory(progress_callback):
      return self.config.remote_session.study_query_bundle_async(
        self.config.spec.query_bundle,
        progress_callback=progress_callback)
    loader = FutureProgressWidget(future_factory=_future_factory)
    loader.connect("loaded", self.__on_facet_loaded)
    self.__set_child(loader)

  def __on_facet_loaded(self, loader):
    future = loader.consume_result()
    if future_exception(future):
      child = ExceptionDisplayWidget(exception=future_exception(future))
    else:
      child = TraceFacetWidget(
        config=TraceFacetWidget.Config(
          spec=self.config.spec,
          remote_session=self.config.remote_session,
          query_engine=self.config.query_engine,
          study_result=future.result()))
    self.__set_child(child)

class TraceWidget(Gtk.Bin):
  """Main widget for interacting with a trace"""
  __gtype_name__ = "TraceWidget"
  __remote_session = None
  __init_done = False
  __query_engine = None

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.init_template()
    self.__init_done = True
    self.__on_remote_session_set()

  def do_destroy(self):
    """Gtk.Widget protocol"""
    if self.__query_engine:
      self.__query_engine.close()
      self.__query_engine = None
    if self.__remote_session:
      self.__remote_session.close()
      self.__remote_session = None
    type(self).mro()[1].do_destroy(self)

  @GObject.property(type=object)
  def remote_session(self):
    """Current remote trace"""
    return self.__remote_session

  @remote_session.setter
  def remote_session(self, remote_session):
    """Set a new remote trace, rebuilding the UI"""
    assert isinstance(remote_session, (RemoteSession, NoneType))
    self.__remote_session = remote_session
    if self.__query_engine:
      self.__query_engine.close()
      self.__query_engine = None
    if remote_session:
      self.__query_engine = \
        AsyncQueryEngine(remote_session.batch_sql_query_async)
    if self.__init_done:
      self.__on_remote_session_set()

  def __on_remote_session_set(self):
    notebook = self.__notebook

    # We have a new remote trace, so nuke all the previous tabs
    # we had.
    while notebook.get_n_pages():
      notebook.remove_page(-1)

    # Create the default tab

    # TODO(dancol): ugh. actually support multiple configs.
    # Here, we hardcode one to get stuff working again.
    q = "(VALUES (1), (2), (3))"
    q = "(SELECT * FROM gui_trace.raw_events.sched_switch)"
    spec = TraceFacetSpec(
      tab_name="XXX",
      query_bundle=SqlBundle.of(q))
    self.add_facet_page(spec)

  def add_facet_page(self, spec):
    """Add a facet to this trace view"""
    config = TraceFacetWidgetShell.Config(
      spec=spec,
      remote_session=self.__remote_session,
      query_engine=self.__query_engine,
    )
    shell = TraceFacetWidgetShell(config=config)
    shell.show()
    self.__notebook.append_page(
      shell, Gtk.Label(label=config.spec.tab_name, visible=True))

  @property
  def __notebook(self):
    notebook = self.get_template_child(self, "notebook")
    assert notebook
    return notebook

add_ui_template(TraceWidget, (
  "notebook",
))

class TraceWidgetShell(Gtk.Bin):
  """Displays a pretty load-progress wrapper around a TraceWidget"""
  __gtype_name__ = "TraceWidgetShell"

  def __init__(self, **kwargs):
    self.__trace_file_name = ""
    super().__init__(**kwargs)

  def __set_child(self, new_child):
    old_child = self.get_child()
    if old_child is not None:
      safe_destroy(old_child)
    self.add(new_child)
    new_child.show()

  def __enter_initial_state(self):
    self.__set_child(TraceIntroWidget())

  def __enter_loading_state(self):
    trace_file_name = self.__trace_file_name
    assert trace_file_name
    def _future_factory(progress_callback):
      # TODO(dancol): asyncify more
      async def _do_the_thing():
        remote_session = await RemoteSession.open_async()
        await remote_session.mount_trace_async(
          ["gui_trace"],
          trace_file_name,
          progress_callback=progress_callback)
        return remote_session
      return asyncio.ensure_future(_do_the_thing())
    loader = FutureProgressWidget(future_factory=_future_factory)
    loader.connect("loaded", self.__on_trace_loaded)
    self.__set_child(loader)

  def __on_trace_loaded(self, loader):
    future = loader.consume_result()
    if future_exception(future):
      child = ExceptionDisplayWidget(exception=future_exception(future))
    else:
      child = TraceWidget(remote_session=future.result())
    self.__set_child(child)

  @gobject_property_str
  def trace_file_name(self):
    """Current trace file name"""
    return self.__trace_file_name

  @trace_file_name.setter
  def trace_file_name(self, trace_file_name):
    self.__trace_file_name = the(str, trace_file_name)
    if trace_file_name:
      self.__enter_loading_state()
    else:
      self.__enter_initial_state()

class TraceWindow(Gtk.ApplicationWindow):
  """Window that displays a TraceWidgetShell"""
  __gtype_name__ = "TraceWindow"

  def __init__(self, **kwargs):
    self.__gui_config = None
    self.__trace_file_name = ""
    super().__init__(**kwargs)
    self.init_template()

  @gobject_property_str
  def trace_file_name(self):
    """Current trace file name"""
    return self.__trace_file_name

  @trace_file_name.setter
  def trace_file_name(self, trace_file_name):
    """Set a new trace file name and rebuild the UI"""
    self.__trace_file_name = the(str, trace_file_name)

  @gobject_property_object
  def gui_config(self):
    """Current GUI config"""
    return self.__gui_config

  @gui_config.setter
  def gui_config(self, value):
    """Set the GUI config"""
    self.__gui_config = value

add_ui_template(TraceWindow)

class DctvApplication(Gtk.Application):
  """Main application for viewing traces"""
  def __init__(self, *,
               trace_file_names,
               gui_config,
               **kwargs):
    super().__init__(
      application_id="com.android.dctv",
      flags=Gio.ApplicationFlags.NON_UNIQUE,
      **kwargs)
    self.__trace_file_names = trace_file_names
    self.__gui_config = the(GuiConfig, gui_config)

  @staticmethod
  def __configure_custom_css():
    screen = Gdk.Screen.get_default()
    style_provider = Gtk.CssProvider()
    with open(pjoin(mydir, "gui.css"), "rb") as css_file:
      style_provider.load_from_data(css_file.read())
    Gtk.StyleContext.add_provider_for_screen(
      screen,
      style_provider,
      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

  def do_startup(self):  # pylint: disable=arguments-differ
    Gtk.Application.do_startup(self)
    self.__configure_custom_css()

    action = Gio.SimpleAction.new("quit", None)
    action.connect("activate", self.__on_quit)
    self.add_action(action)

    builder = Gtk.Builder.new_from_file(pjoin(mydir, "menu.ui"))
    self.set_app_menu(builder.get_object("app-menu"))

  def __on_quit(self, _action, _param):
    self.quit()

  def do_activate(self):  # pylint: disable=arguments-differ
    Gtk.Application.do_activate(self)
    trace_file_names = self.__trace_file_names
    if not trace_file_names:
      trace_file_names = [""]

    for trace_file_name in trace_file_names:
      window = TraceWindow(
        application=self,
        title="DCTV",
        trace_file_name=xabspath(trace_file_name),
        gui_config=self.__gui_config)
      self.add_window(window)
      window.present()

def run_gui(*, initial_trace_file_names):
  """Run the GUI main loop"""
  import gbulb
  gbulb.install(gtk=True)
  loop = asyncio.get_event_loop()
  gui_config = GuiConfig()
  # TODO(dancol): absolutify the filenames so worker process (chdired
  # into root) an use them
  with ApartmentFactory(name="dctv").make_apartment().attached():
    loop.run_forever(
      application=DctvApplication(
        trace_file_names=initial_trace_file_names,
        gui_config=gui_config))
