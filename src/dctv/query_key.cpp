// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "query_key.h"

namespace dctv {

unique_dtype
QueryKey::get_dtype() const
{
  unique_pyref schema = getattr(this->query_node, "schema");
  return getattr(schema, "dtype").addref_as<PyArray_Descr>();
}

int
QueryKey::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->query_node.get());
  return 0;
}

}  // namespace dctv
