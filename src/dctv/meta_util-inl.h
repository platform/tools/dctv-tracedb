// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

// Hack hack hack McHackityFace

#define DCTV_SEQ_CREATOR_0_END
#define DCTV_SEQ_CREATOR_0(...)                 \
  ((__VA_ARGS__)) DCTV_SEQ_CREATOR_1

#define DCTV_SEQ_CREATOR_1_END
#define DCTV_SEQ_CREATOR_1(...)                 \
  ((__VA_ARGS__)) DCTV_SEQ_CREATOR_0

#define DCTV_DEFINE_HANA_STRUCT_1(struct_name, field_defs)      \
  struct struct_name {                                          \
    BOOST_PP_EXPAND(                                            \
        BOOST_HANA_DEFINE_STRUCT                                \
        BOOST_PP_SEQ_TO_TUPLE(                                  \
            (struct_name)                                       \
            field_defs /*NOLINT*/));                            \
  }

}  // namespace dctv
