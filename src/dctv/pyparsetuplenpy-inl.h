// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<auto Checker, typename P, typename D>
auto
do_query_indir_hack(P&& parse_arg, D&& default_)
{
  constexpr bool has_default = CONSTEXPR_VALUE(default_ != hana::false_c);
  PyObject* ref;
  if constexpr (has_default) {
    ref = nullptr;
  }
  return parse_arg(
      BOOST_HANA_STRING("O"),
      hana::make_tuple(&ref),
      [&] {
        if constexpr (has_default) {
          if (ref == Py_None)
            ref = nullptr;
          if (!ref)
            return default_();
        }
        assume(ref);
        return Checker(ref);
      });
}

}  // namespace dctv
