// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include "io_spec.h"
#include "pyutil.h"
#include "query.h"

namespace dctv {

struct QeCallIo : QeCall {
  explicit QeCallIo(Vector<IoSpec> io_specs);
  void setup(OperatorContext* oc) final;
  Score compute_score(const OperatorContext* oc) const final;
  int py_traverse(visitproc visit, void* arg) const noexcept final;
 protected:
  bool do_try_writes();
  Vector<IoSpec> io_specs;
};

struct QeCallIoSingle final : QeCallIo {
  explicit QeCallIoSingle(IoSpec io_spec);
  unique_pyref do_it(OperatorContext* oc) override;
  IoSpec extract_io_spec() override;
};

struct QeCallIoMulti final : QeCallIo {
  using QeCallIo::QeCallIo;
  unique_pyref do_it(OperatorContext* oc) override;
};

inline void init_qe_call_io(pyref m) {}

}  // namespace dctv

#include "qe_call_io-inl.h"
