// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "result_buffer.h"

#include <alloca.h>

#include "hunk.h"
#include "pythread.h"
#include "pythread.h"
#include "query_execution.h"

namespace dctv {

GenericResultBuffer::GenericResultBuffer(
    unique_pyref cb,
    obj_pyref<QueryExecution> qe,
    Vector<unique_dtype> dtypes)
    : cb(std::move(cb.notnull())),
      qc(qe->get_qc()),
      chunk_size(qe->block_size),
      size(0),
      dtypes(std::move(dtypes)),
      arrays(this->dtypes.size())
{
  assume(this->qc);
  assume(this->chunk_size > 0);
}

void
GenericResultBuffer::flush()
{
  assert_gil_held();
  assert(this->dtypes.size() <= std::numeric_limits<npy_intp>::max());
  npy_intp n = static_cast<npy_intp>(this->dtypes.size());
  npy_intp size = this->size;
  npy_intp chunk_size = this->chunk_size;
  auto* arrays = this->arrays.data();
  Vector<PyObject*> argbuf_buf;
  PyObject** argbuf;
  if (n < 16) {
    argbuf = reinterpret_cast<PyObject**>(alloca(sizeof (*argbuf) * n));
  } else {
    argbuf_buf.resize(n);
    argbuf = argbuf_buf.data();
  };

  Vector<unique_pyref> empty_arrays;
  if (size == 0) {
    empty_arrays.reserve(n);
    auto* dtypes = this->dtypes.data();
    for (npy_intp i = 0; i < n; ++i) {
      empty_arrays.push_back(make_uninit_pyarray(
          dtypes[i].notnull().addref(), 0));
      argbuf[i] = pyref(empty_arrays[i]).get();
    }
  } else {
    for (npy_intp i = 0; i < n; ++i) {
      assert(npy_size1d(arrays[i]) == chunk_size);
      if (size < chunk_size) {
        // TODO(dancol): this reference-swapping thing is exceedingly
        // silly and ugly.
        unique_hunk hunk = Hunk::retrieve_backing_hunk(arrays[i]).notnull();
        arrays[i].reset();  // Unpins
        hunk->resize(size);
        arrays[i] = hunk->inflate();
        assert(npy_size1d(arrays[i]) == size);
      }
      argbuf[i] = pyref(arrays[i]).get();
    }
  }
  // TODO(dancol): pass raw hunks, not arrays, to callback
  adopt_check(_PyObject_FastCall(this->cb.get(), argbuf, n));
  if (size)
    for (npy_intp i = 0; i < n; ++i)
      arrays[i].reset();
}

void GenericResultBuffer::restart_nogil()
{
  with_gil_acquired([&] {
    assume(this->size >= 0);
    assume(this->size <= this->chunk_size);
    if (this->size > 0)
      this->flush();
    for (npy_intp i = 0; i < this->dtypes.size(); ++i) {
      assert(!this->arrays[i]);
      this->arrays[i] = make_uninit_hunk_array(
          this->qc,
          this->dtypes[i].notnull().addref(),
          this->chunk_size);
    }
  });
}

}  // namespace dctv
