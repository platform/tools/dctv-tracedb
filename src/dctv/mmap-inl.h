// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

Mmap::Mmap(void* addr, size_t nbytes) noexcept
    : addr_(addr), nbytes_(nbytes)
{}

constexpr
Mmap::Mmap(Mmap&& other) noexcept
    : addr_(other.addr_),
      nbytes_(other.nbytes_)
{
  other.release();
}

void
Mmap::release() noexcept
{
  this->addr_ = nullptr;
  this->nbytes_ = 0;
}

Mmap::~Mmap() noexcept
{
  if (this->nbytes_)
    this->do_unmap();
}

constexpr
Mmap&
Mmap::operator=(Mmap&& other) noexcept
{
  if (this != &other) {
    this->~Mmap();
    new (this) Mmap(std::move(other));
  }
  return *this;
}

void*
Mmap::addr() const noexcept
{
  return this->addr_;
}

size_t
Mmap::nbytes() const noexcept
{
  return this->nbytes_;
}

MmapShared::MmapShared(Mmap&& mmap) noexcept
    : Mmap(std::move(mmap))
{}

MmapShared::MmapShared(void* addr, size_t nbytes) noexcept
    : Mmap(addr, nbytes)
{}

size_t
page_size() noexcept
{
  return cached_page_size;
}

size_t
mmap_round_size_up(size_t nbytes) noexcept
{
  return pow2_round_up(nbytes, page_size());
}

size_t
mmap_round_size_down(size_t nbytes) noexcept
{
  return pow2_round_down(nbytes, page_size());
}

bool
mmap_aligned_p(size_t bytes) noexcept
{
  return mmap_round_size_down(bytes) == bytes;
}

}  // namespace dctv
