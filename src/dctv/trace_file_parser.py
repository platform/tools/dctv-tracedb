# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Routines for dissecting a systrace-format trace file"""
import logging
import re
from collections import namedtuple

from modernmp.util import ignore

from .event_parser import IndexedEvents
from .util import Timed
from .trace_file_util import MappedTrace, BadTraceFileError

log = logging.getLogger(__name__)

TRACE_DATA_START = (
  b'\n  <script class="trace-data" type="application/text">\n')
TRACE_DATA_END = b'\n  </script>\n'

TRACE_DATA_END_RE = re.compile(
  rb" *</script>$",
  re.MULTILINE)

# If we see any of these _anchored_ expressions match the first 4KB
# of the trace file, we deem the trace to be an "ftrace" file instead
# of a systrace HTML one.  If match group "cpus" is non-empty, its
# integer parse becomes the nr_cpus metadatum.  If the "discard" group
# matches, we skip past the header when calling the lower-level event
# parsing code.  (More specifically, we seek forward to the end of the
# "discard" group.)

FTRACE_TRACE_HEADERS = (
  re.compile(
    rb"(?P<discard>cpus=(?P<cpus>[0-9]+)\n)",
    re.MULTILINE),
  re.compile(
    rb"(?P<discard>(?:capturing trace... done\n)?TRACE:\n)",
    re.MULTILINE),
  re.compile(
    rb"(?:\n|.)*# entries-in-buffer.*(?:#P:(?P<cpus>[0-9]+))",
    re.MULTILINE),
  re.compile(
    rb"(?P<discard>\A# DCTV (?P<dctv_footer>[0-9]+) *\n)",
    re.MULTILINE),
)

LINE_RE = re.compile(rb"[^\n]*\n", re.MULTILINE)

ParsedTrace = namedtuple("ParsedTrace", ["events", "metadata"])

def _parse_trace_json_section(trace_mapping, header_start_pos):
  m = LINE_RE.match(trace_mapping, pos=header_start_pos)
  if not m:
    raise ValueError("no JSON data")
  import json
  decoder = json.JSONDecoder()
  obj, obj_len = decoder.raw_decode(m.group(0).decode("UTF-8"))
  return obj, header_start_pos + obj_len

def _line_at(trace_mapping, pos):
  nl = trace_mapping.find(b"\n", pos)
  if nl < 0:
    nl = len(trace_mapping)
  return trace_mapping[pos:nl]

def _parse_mapped_trace_html(
    *,
    pos,
    mapped_trace,
    progress_callback,
    **event_parse_kwargs):
  trace_mapping = mapped_trace.mapping
  events = None
  metadata = None

  while True:
    with Timed("finding data section start"):
      section_header_pos = trace_mapping.find(TRACE_DATA_START, pos)
    if section_header_pos < 0:
      break
    header_start_pos = section_header_pos + len(TRACE_DATA_START)
    if trace_mapping[header_start_pos] == ord(b"{"):
      if metadata is not None:
        raise BadTraceFileError("duplicate metadata blob")
      progress_callback("Loading metadata")
      with Timed("parse_trace_json_section"):
        metadata, end = _parse_trace_json_section(
          trace_mapping,
          header_start_pos)
    elif _line_at(trace_mapping, header_start_pos) == b"PROCESS DUMP":
      log.warning("IMPLEMENT PROCESS DUMP PARSING")  # TODO
      end = trace_mapping.find(b"  </script>\n", header_start_pos)
    else:
      if events is not None:
        raise BadTraceFileError("duplicate events section")
      with Timed("find end of section"):
        end = trace_mapping.find(b"  </script>\n", header_start_pos)
        if end < 0:
          raise BadTraceFileError("could not locate end of events section")
      with Timed("scanning event section"):
        # pylint: disable=missing-kwoa
        events = IndexedEvents(mapped_trace=mapped_trace,
                               progress_callback=progress_callback,
                               events_start=header_start_pos,
                               events_end=end,
                               **event_parse_kwargs)
    end_match = TRACE_DATA_END_RE.match(trace_mapping, pos=end)
    if not end_match:
      raise BadTraceFileError(
        "garbage at end of section: pos={!r}".format(end),
        bytes(trace_mapping[end:end+80]))
    pos = end_match.end()
  return events, metadata

def _last_newline_position(trace_mapping):
  return trace_mapping.rfind(b"\n") + 1

def _find_ftrace_header_match(trace_mapping):
  pmap = trace_mapping[:4096]
  for pattern in FTRACE_TRACE_HEADERS:
    m = pattern.match(pmap)
    if m:
      return m
  return None

def _decode_dctv_metadata(mapping, footer_length):
  footer_prefix = mapping[-footer_length : -footer_length + 2]
  if footer_prefix != b"# ":
    raise ValueError("bad DCTV footer prefix {!r}" .format(footer_prefix))
  import json
  metadata = json.loads(mapping[-footer_length + 2:].decode("UTF-8"))
  if "cpuinfo" in metadata:
    metadata["nr_cpus"] = len(re.findall(r"^processor[ \t]",
                                         metadata["cpuinfo"],
                                         re.MULTILINE))
  return metadata

def parse_mapped_trace(
    *,
    mapped_trace,
    event_schemas=(),
    progress_callback=None,
    force_ftrace=False,
    time_basis_override=None):
  """Parse a trace file that's been mapped into memory.

  MAPPED_TRACE is a MappedTrace object.

  EVENT_SCHEMAS is a sequence of event schemas to register.

  PROGRESS_CALLBACK receives information about the progress of
  the trace.

  Return a ParsedTrace structure.
  """

  progress_callback = progress_callback or ignore
  assert isinstance(mapped_trace, MappedTrace)
  assert callable(progress_callback)

  metadata = None
  events = None

  progress_callback("Parsing trace")

  event_parse_kwargs = dict(
    mapped_trace=mapped_trace,
    time_basis_override=time_basis_override,
    event_schemas=event_schemas,
    progress_callback=progress_callback,
  )

  mapping = mapped_trace.mapping
  with mapped_trace.augment_parse_errors():
    m = _find_ftrace_header_match(mapping)
    if m or force_ftrace:
      log.debug("Found known header: parsing as ftrace")
      mdict = m.groupdict() if m else {}
      actual_data_start = m.end("discard") if "discard" in mdict else 0
      dctv_footer = mdict.get("dctv_footer")
      if dctv_footer:
        dctv_footer_length = int(dctv_footer)
        if dctv_footer_length <= 0:
          raise ValueError("invalid DCTV-trace footer {!r}"
                           .format(dctv_footer))
        metadata = _decode_dctv_metadata(mapping, dctv_footer_length)
        events_end = len(mapping) - dctv_footer_length
      else:
        metadata = dict(nr_cpus=int(mdict.get("cpus", 0)))
        events_end = _last_newline_position(mapping)
      events = IndexedEvents(
        events_start=actual_data_start,
        events_end=events_end,
        **event_parse_kwargs)
    else:
      log.debug("No event header found: parsing as HTML")
      events, metadata = _parse_mapped_trace_html(pos=0, **event_parse_kwargs)

  if events is None:
    raise ValueError("no events found in {!r}".format(mapped_trace))

  return ParsedTrace(events=events, metadata=metadata or {})
