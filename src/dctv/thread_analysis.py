# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Python-side code for thread analysis"""

import logging

from .query import (
  C_UNIQUE,
  DURATION_SCHEMA,
  EVENT_UNPARTITIONED_TIME_MAJOR,
  GenericQueryTable,
  InvalidQueryException,
  QueryNode,
  QuerySchema,
  QueryTable,
  STRING_SCHEMA,
  SqlAttributeLookup,
  TS_SCHEMA,
  TableKind,
  TableSchema,
  TableSorting,
)

from .queryop import (
  AutoMultiQuery,
)

from .util import (
  EqImmutable,
  cached_property,
  final,
  iattr,
  override,
  ureg,
)

from ._native import (
  DTYPE_CLONE_FLAGS,
  DTYPE_ID_VALUE,
  DTYPE_MEMORY_SIZE,
  DTYPE_OOM_SCORE,
  DTYPE_RAW_THREAD_STATE_FLAGS,
  DTYPE_SNAPSHOT_NUMBER,
  DTYPE_CPU_NUMBER,
  analyze_threads,
)

from .querythread import(
  SynchronousInput,
  SynchronousOutput,
  SynchronousQueryExecution,
)

log = logging.getLogger(__name__)

RAW_TID_SCHEMA = QuerySchema(DTYPE_ID_VALUE, domain="raw_tid")
RAW_TGID_SCHEMA = QuerySchema(DTYPE_ID_VALUE, domain="raw_tgid")
UTID_SCHEMA = QuerySchema(DTYPE_ID_VALUE, domain="utid")
UTGID_SCHEMA = QuerySchema(DTYPE_ID_VALUE, domain="utgid")
CLONE_FLAGS_SCHEMA = QuerySchema(DTYPE_CLONE_FLAGS, domain="clone_flags")
SNAPSHOT_NUMBER_SCHEMA = QuerySchema(DTYPE_SNAPSHOT_NUMBER,
                                     domain="snapshot_number")
MEMORY_SIZE_SCHEMA = QuerySchema(DTYPE_MEMORY_SIZE, unit=ureg().byte)
OOM_SCORE_ADJ_SCHEMA = QuerySchema(DTYPE_OOM_SCORE, domain="oom_score_adj")
RAW_THREAD_STATE_SCHEMA = QuerySchema(DTYPE_RAW_THREAD_STATE_FLAGS)

CPU_NUMBER_SCHEMA = QuerySchema(DTYPE_CPU_NUMBER, domain="cpu")

class QueryBundle(EqImmutable):
  """Related bundle of queries

  Has automated iattr creation, sync input conversion.
  """

  _required_table_schema = None

  @cached_property
  def inputs(self):
    """QueryNode inputs needed for this bundle at query time"""
    return tuple([getattr(self, field_name)
                  for field_name in type(self).fields])

  @classmethod
  def iattr(cls, **kwargs):
    """Make an immutable attribute holding this QueryBundle

    Automatically provides conversion from a QueryTable, making the
    iattr directly usable from SQL without additional Python-level
    conversion code.
    """
    assert "converter" not in kwargs
    def _converter(value):
      if isinstance(value, cls):
        return value
      qt = QueryTable.coerce_(value)
      if cls._required_table_schema:
        if qt.table_schema.sorting != cls._required_table_schema.sorting:
          qt = qt.to_schema(sorting=cls._required_table_schema.sorting)
        if qt.table_schema != cls._required_table_schema:
          raise InvalidQueryException(
            "table schema mismatch in {}: wanted {} but found {}".format(
              cls, cls._required_table_schema, qt.table_schema))
      return cls(**{field_name: qt[field_name]
                    for field_name in cls.fields})
    return iattr(cls, converter=_converter, **kwargs)

@final
class ThreadAnalysis(AutoMultiQuery, SqlAttributeLookup):
  """Analyze threads and decode trace snapshots"""

  # Sampler list here should match the order in thread_analysis.cpp!
  sampler_info = {
    "comm": ("utid", STRING_SCHEMA),
    "oom_score_adj": ("utgid", OOM_SCORE_ADJ_SCHEMA),
    "rss_anon": ("utgid", MEMORY_SIZE_SCHEMA),
    "rss_file": ("utgid", MEMORY_SIZE_SCHEMA),
    "state": ("utid", RAW_THREAD_STATE_SCHEMA),
  }

  @final
  class Sampler(EqImmutable):
    """Sample subtable"""
    ta = iattr()
    name = iattr(str)
    task_name = iattr(str)
    schema = iattr(QuerySchema)
    @final
    class Meta(AutoMultiQuery.AutoMeta):
      """Meta datum"""
      # pylint: disable=no-member
      TS = TS_SCHEMA
      TASK = (lambda self: UTGID_SCHEMA
              if self.task_name == "utgid" else UTID_SCHEMA)
      PAYLOAD = lambda self: self.schema
    @final
    class MetaQuery(QueryNode):
      """Meta query"""
      sampler = iattr()
      meta = iattr()
      @override
      def _compute_schema(self):
        # pylint: disable=protected-access
        return self.sampler.Meta._meta_schema[self.meta](self.sampler)
      @override
      def make_action(self):
        config = self.sampler.ta
        return config.Action(config=config)
    def metaq(self, meta):
      """Retrieve metadata query"""
      return self.MetaQuery(self, meta)
    def compute_outputs(self):
      """Query outputs"""
      return [self.metaq(m) for m in self.Meta]
    def make_query_table(self):
      """Make the actual query table for this bundle"""
      Meta = self.Meta
      return GenericQueryTable({
        "_ts": self.metaq(Meta.TS),
        self.task_name: self.metaq(Meta.TASK),
        self.name: self.metaq(Meta.PAYLOAD),
      }, table_schema=EVENT_UNPARTITIONED_TIME_MAJOR)

  def sampler(self, name):
    """Retrieve a sampler object by name"""
    return self.Sampler(self, name, *self.sampler_info[name])

  @property
  def samplers(self):
    """List of all sampler objects we support"""
    return [self.sampler(name) for name in self.sampler_info]

  @override
  def enumerate_sql_attributes(self):
    return tuple(self.sampler_info) + ("threads", "processes")

  @override
  def lookup_sql_attribute(self, name):
    if name == "threads":
      return self.thread_mapping_qt
    if name == "processes":
      return self.process_mapping_qt
    if name in self.sampler_info:
      return self.sampler(name).make_query_table()
    return super().lookup_sql_attribute(name)

  class Meta(AutoMultiQuery.AutoMeta):
    """Metadata for ThreadAnalysis"""
    # Span table partitioned by raw tid mapping to utid
    THREAD_TS = TS_SCHEMA
    THREAD_DURATION = DURATION_SCHEMA
    THREAD_LIVE_DURATION = DURATION_SCHEMA
    THREAD_RAW_TID = RAW_TID_SCHEMA
    THREAD_UTID = UTID_SCHEMA.constrain(C_UNIQUE)
    THREAD_UTGID = UTGID_SCHEMA

    # Span table partitioned by raw tgid mapping to utgid
    PROCESS_TS = TS_SCHEMA
    PROCESS_DURATION = DURATION_SCHEMA
    PROCESS_LIVE_DURATION = DURATION_SCHEMA
    PROCESS_RAW_TGID = RAW_TGID_SCHEMA
    PROCESS_UTGID = UTGID_SCHEMA.constrain(C_UNIQUE)
    PROCESS_PARENT_UTGID = UTGID_SCHEMA

  class SchedProcessExit(QueryBundle):
    """Task exit events"""
    _ts = TS_SCHEMA.qn_iattr()
    tid = RAW_TID_SCHEMA.qn_iattr()
    _required_table_schema = EVENT_UNPARTITIONED_TIME_MAJOR
    __inherit__ = dict(_required_table_schema=override)

  class TaskNewTask(QueryBundle):
    """Task creation events"""
    _ts = TS_SCHEMA.qn_iattr()
    tid = RAW_TID_SCHEMA.qn_iattr()
    new_tid = RAW_TID_SCHEMA.qn_iattr()
    clone_flags = CLONE_FLAGS_SCHEMA.qn_iattr()
    _required_table_schema = EVENT_UNPARTITIONED_TIME_MAJOR
    __inherit__ = dict(_required_table_schema=override)

  class SnapshotStart(QueryBundle):
    """Start-of-snapshot events"""
    _ts = TS_SCHEMA.qn_iattr()
    number = SNAPSHOT_NUMBER_SCHEMA.qn_iattr()
    _required_table_schema = EVENT_UNPARTITIONED_TIME_MAJOR
    __inherit__ = dict(_required_table_schema=override)

  class SnapshotEnd(QueryBundle):
    """End-of-snapshot events"""
    _ts = TS_SCHEMA.qn_iattr()
    _required_table_schema = EVENT_UNPARTITIONED_TIME_MAJOR
    __inherit__ = dict(_required_table_schema=override)

  sched_process_exit = SchedProcessExit.iattr(kwonly=True)
  task_newtask = TaskNewTask.iattr(kwonly=True)
  snapshot_start = SnapshotStart.iattr(kwonly=True)
  snapshot_end = SnapshotEnd.iattr(kwonly=True)
  first_ts = TS_SCHEMA.qn_iattr(kwonly=True, nullable=True)
  last_ts = TS_SCHEMA.qn_iattr(kwonly=True, nullable=True)
  get_snapshot_data = iattr(kwonly=True)
  kernel_threads_as_threads = iattr(bool, default=True)
  nr_cpus = QuerySchema(DTYPE_CPU_NUMBER).qn_iattr(
    kwonly=True, nullable=True)

  @override
  def compute_inputs(self):
    return (self.sched_process_exit.inputs +
            self.task_newtask.inputs +
            self.snapshot_start.inputs +
            self.snapshot_end.inputs +
            (self.first_ts,) +
            ((self.last_ts,) if self.last_ts else ()) +
            ((self.nr_cpus,) if self.nr_cpus else ()))

  @override
  def compute_outputs(self):
    outputs = [self.metaq(meta) for meta in self.Meta]
    for sampler in self.samplers:
      outputs.extend(sampler.compute_outputs())
    return outputs

  @override
  async def run_async(self, qe):
    Meta = self.Meta
    io_specs = []
    def _io(io_spec):
      io_specs.append(io_spec)
      return io_spec

    sched_process_exit = \
      _io(SynchronousInput(*self.sched_process_exit.inputs))
    task_newtask = \
      _io(SynchronousInput(*self.task_newtask.inputs))
    snapshot_start = \
      _io(SynchronousInput(*self.snapshot_start.inputs))
    snapshot_end = \
      _io(SynchronousInput(*self.snapshot_end.inputs))
    first_ts = \
      _io(SynchronousInput(self.first_ts))
    last_ts = None if not self.last_ts else \
      _io(SynchronousInput(self.last_ts))
    nr_cpus = None if not self.nr_cpus else \
      _io(SynchronousInput(self.nr_cpus))

    def _mo(*meta):
      return _io(SynchronousOutput(*[self.metaq(m) for m in meta]))

    thread_map_out = _mo(
      Meta.THREAD_TS,
      Meta.THREAD_DURATION,
      Meta.THREAD_LIVE_DURATION,
      Meta.THREAD_RAW_TID,
      Meta.THREAD_UTID,
      Meta.THREAD_UTGID,
    )

    process_map_out = _mo(
      Meta.PROCESS_TS,
      Meta.PROCESS_DURATION,
      Meta.PROCESS_LIVE_DURATION,
      Meta.PROCESS_RAW_TGID,
      Meta.PROCESS_UTGID,
      Meta.PROCESS_PARENT_UTGID,
    )

    sampler_outs = [
      _io(SynchronousOutput(*[sampler.metaq(m) for m in sampler.Meta]))
      for sampler in self.samplers
    ]

    sync_qe, (), () = await SynchronousQueryExecution.async_setup(
      qe, (), (), sync_io=io_specs)

    def _run_operator():
      analyze_threads(
        sched_process_exit=sched_process_exit,
        task_newtask=task_newtask,
        snapshot_start=snapshot_start,
        snapshot_end=snapshot_end,
        first_ts=first_ts,
        last_ts=last_ts,
        get_snapshot_data=self.get_snapshot_data,
        tid_mapping_out=thread_map_out,
        tgid_mapping_out=process_map_out,
        sampler_outs=sampler_outs,
        qe=qe,
        kernel_threads_as_threads=self.kernel_threads_as_threads,
        nr_cpus=nr_cpus)
    await sync_qe.async_run(_run_operator)

  def __meta_table(self, *columns, partition):
    return GenericQueryTable([
      (cname, self.metaq(cmeta))
      for cname, cmeta in columns
    ], table_schema=TableSchema(
      TableKind.SPAN, partition, TableSorting.NONE))

  @cached_property
  def thread_mapping_qt(self):
    """Span QueryTable of thread data"""
    Meta = self.Meta
    return self.__meta_table(
      ("_ts", Meta.THREAD_TS),
      ("_duration", Meta.THREAD_DURATION),
      ("raw_tid", Meta.THREAD_RAW_TID),
      ("live_duration", Meta.THREAD_LIVE_DURATION),
      ("utid", Meta.THREAD_UTID),
      ("utgid", Meta.THREAD_UTGID),
      partition="raw_tid")

  @cached_property
  def process_mapping_qt(self):
    """Span QueryTable of process data"""
    Meta = self.Meta
    return self.__meta_table(
      ("_ts", Meta.PROCESS_TS),
      ("_duration", Meta.PROCESS_DURATION),
      ("raw_tgid", Meta.PROCESS_RAW_TGID),
      ("live_duration", Meta.PROCESS_LIVE_DURATION),
      ("utgid", Meta.PROCESS_UTGID),
      ("parent_utgid", Meta.PROCESS_PARENT_UTGID),
      partition="raw_tgid")
