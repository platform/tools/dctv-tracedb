// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "query_cache.h"

#include <algorithm>
#include <cmath>
#include <exception>
#include <queue>
#include <tuple>

#include <boost/iterator/transform_iterator.hpp>

#include "automethod.h"
#include "block.h"
#include "fmt.h"
#include "fsutil.h"
#include "fsutil.h"
#include "hash_table.h"
#include "input_channel.h"
#include "map.h"
#include "operator_context.h"
#include "pylog.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "pyutil.h"
#include "query_execution.h"
#include "string_table.h"
#include "vector.h"

namespace dctv {

constexpr bool report_allocations = false;

npy_intp
block_size_from_qc(QueryCache* qc)
{
  return qc->get_block_size();
}

QueryCache*
query_cache_check(PyObject* ref)
{
  return pyref(ref).as<QueryCache>().get();
}

QueryCache::QueryCache(QueryCacheConfig config)
    : config(std::move(config)),
      st(make_pyobj<StringTable>())
{
  assume(this->config.memory_lwm_nbytes <=
         this->config.memory_hwm_nbytes);

  Map<size_t, Freelist> freelists;
  auto add_freelist = [&](size_t nbytes) {
    nbytes = mmap_round_size_up(nbytes);
    auto it = freelists.find(nbytes);
    if (it == freelists.end())
      freelists.insert(std::pair(nbytes, Freelist{nbytes}));
  };

  size_t freelist_size = mmap_round_size_up(page_size());
  size_t max_freelist_size = mmap_round_size_up(
      static_cast<size_t>(this->get_block_size()) * sizeof (int64_t));
  while (freelist_size <= max_freelist_size) {
    add_freelist(freelist_size);
    freelist_size = mul_raise_on_overflow(freelist_size,
                                          static_cast<size_t>(2));
  }
  // Make sure we have precisely-sized freelists for the standard
  // dtypes in the actual block size.  We don't add duplicates.
  for (size_t sz : make_array(1, 2, 4, 8))
    add_freelist(sz * static_cast<size_t>(this->get_block_size()));

  // Copy in sorted order, de-duped
  for (auto& [sz,  fl] : freelists) {
    assume(sz == fl.nbytes);
    this->freelists.emplace_back(std::move(fl));
  }

  // Pre-open the cache directory if supplied.
  if (this->config.tmpdir)
    this->cachedir = xopen(
        str(getattr(this->config.tmpdir, "name")).c_str(),
        O_DIRECTORY | O_RDONLY);
}

QueryCache::~QueryCache() noexcept
{
  assume(this->hunks.empty());
}

int
QueryCache::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->config.tmpdir.get());
  Py_VISIT(this->st.get());
  if (int ret = this->HasDict::py_traverse(visit, arg))
    return ret;
  return 0;
}

int
QueryCache::py_clear() noexcept
{
  this->config.tmpdir.reset();
  this->HasDict::py_clear();
  return 0;
}

unique_pyref
QueryCache::clear()
{
  // TODO(dancol): implement cache clearing
  return addref(Py_None);
}

void
QueryCache::note_execution_in_progress(QueryExecution* qe)
{
  if (safe_mode) {
    auto it = std::find(this->executions.begin(),
                        this->executions.end(),
                        qe);
    assume(it == this->executions.end());
  }
  this->executions.push_back(qe);
}

void
QueryCache::note_execution_done(QueryExecution* qe) noexcept
{
  auto it = std::find(this->executions.begin(),
                      this->executions.end(),
                      qe);
  assume(it != this->executions.end());
  this->executions.erase(it);
}

QueryCache::Freelist*
QueryCache::find_fl(size_t sz) noexcept
{
  auto it = std::lower_bound(
      this->freelists.begin(),
      this->freelists.end(),
      sz,
      [](const Freelist& fl, size_t n) {
        return fl.nbytes < n;
      });
  if (it == this->freelists.end())
    return nullptr;
  return &*it;
}

void
QueryCache::donate_mmap(MmapShared for_freelist) noexcept
{
  size_t nbytes = for_freelist.nbytes();
  Freelist* fl = this->find_fl(nbytes);
  assume(!fl || fl->nbytes >= nbytes);
  if (fl &&
      fl->nbytes == for_freelist.nbytes() &&
      fl->spare.size() < this->config.max_freelist) {
    try {
      fl->spare.push_back(std::move(for_freelist));
    } catch (const std::bad_alloc&) {
      // It's okay to drop on the floor instead of adding to
      // the freelist.
      return;
    }
  }
}

unique_pyarray
QueryCache::make_uninit_array(unique_dtype dtype, npy_intp nelem)
{
  return Hunk::make_uninit(this, std::move(dtype), nelem)->inflate();
}

void
QueryCache::account_memory(size_t nbytes, Hunk* protect_hunk)
{
  this->memory_nbytes += nbytes;
  this->report();
  if (this->memory_nbytes >= this->config.memory_hwm_nbytes)
    this->trim_memory(this->config.memory_lwm_nbytes, protect_hunk);
}

enum class HunkClassification {
  FLOATING,
  QUEUED,
  PINNED,
};

constexpr int nr_hunk_classifications = 3;

struct HunkStats {
  npy_intp nr = 0;
  size_t memory_use = 0;
  size_t disk_use = 0;
};

struct HunkInfo {
  npy_intp references = 0;
  HunkScore score = HunkScore{};
  HunkClassification classification = HunkClassification::FLOATING;
};

using HunkTable = HashTable<Hunk*, HunkInfo>;

// Don't bother trying to compact hunks using less than this many
// bytes of memory.
constexpr size_t de_minimis_memory_nbytes = 128;

HunkInfo*
hi_for_hunk(HunkTable* ht, Hunk* hunk)
{
  auto it = ht->find(hunk);
  assert(it != ht->end());
  return &it->second;
}

static
npy_intp
get_oc_ordinal(const OperatorContext* oc)
{
  return -oc->compute_basic_score().negated_ordinal;
}

static
HunkScore
compute_hunk_score(npy_intp nr_operators,
                   npy_intp operator_ordinal,
                   npy_intp block_ordinal)
{
  assume(1 <= nr_operators);
  assume(0 <= operator_ordinal);
  assume(0 <= block_ordinal);
  assume(operator_ordinal < nr_operators);
  double pct = static_cast<double>(operator_ordinal) /
      static_cast<double>(block_ordinal);
  return pct * 100 + block_ordinal;
}

static
void
scan_qe_queues(HunkTable* ht, QueryExecution* qe)
{
  npy_intp nr_operators = 0;
  int min_ordinal;

  auto do_mark_hunk = [&]
      (Hunk* hunk,
       npy_intp ic_block_ordinal,
       npy_intp operator_ordinal)
  {
    assume(ic_block_ordinal >= 0);
    assume(operator_ordinal >= 0);
    Hunk::ResourceUse mu = hunk->get_resource_use();
    if (mu.memory_nbytes < de_minimis_memory_nbytes)
      return;
    HunkInfo* hi = hi_for_hunk(ht, hunk);
    hi->references += 1;
    HunkScore score =
        compute_hunk_score(nr_operators,
                           operator_ordinal,
                           ic_block_ordinal);
    hi->score = std::max(hi->score, score);
  };

  qe->enumerate_operators([&](OperatorContext* oc) {
    npy_intp oc_ordinal = get_oc_ordinal(oc);
    if (nr_operators == 0)
      min_ordinal = oc_ordinal;
    else if (oc_ordinal < min_ordinal)
      min_ordinal = oc_ordinal;
    ++nr_operators;
  });

  if (nr_operators == 0)
    return;  // Nothing to do?!

  qe->enumerate_operators([&](OperatorContext* oc) {
    npy_intp oc_ordinal = get_oc_ordinal(oc);
    oc->enumerate_input_channels([&](InputChannel* ic) {
      npy_intp ic_block_ordinal = 0;
      auto mark_hunk = [&](Hunk* hunk) {
        do_mark_hunk(hunk, ic_block_ordinal, oc_ordinal - min_ordinal);
      };
      ic->enumerate_queued_blocks([&](Block* block) {
        mark_hunk(block->get_data_hunk());
        if (block->get_mask_hunk())
          mark_hunk(block->get_mask_hunk());
        ic_block_ordinal += 1;
      });
    });
  });
}

void
QueryCache::trim_memory(size_t target_memory_nbytes, Hunk* protect_hunk)
{
  if (this->get_memory_nbytes() <= target_memory_nbytes)
    return;  // Nothing to do

  // Don't recurse
  if (this->disable_trim)
    return;
  this->disable_trim = 1;
  FINALLY(this->disable_trim = 0);

  HunkTable ht;

  // Initialize the hunk table.  All subsequent work asserts that any
  // hunk we find is already in the table.
  for (Hunk& hunk : this->hunks)
    ht[&hunk].score = INFINITY;

  // Walk the input queues and find the minimum scores for each hunk,
  // also counting references.
  for (QueryExecution* qe : this->executions)
    scan_qe_queues(&ht, qe);

  // Any hunks with unaccounted references gets a minimum score
  // because it's probably sitting in some IO buffer, about to
  // be used.
  for (auto& [hunk, hi] : ht)
    if (refcount(hunk) != hi.references)
      hi.score = -INFINITY;

  // Score overrides exist for debugging.  (Must be last.)
  if (safe_mode) {
    for (Hunk& hunk : this->hunks) {
      HunkInfo* hi = hi_for_hunk(&ht, &hunk);
      HunkScore score_override = hunk.get_score_override();
      if (!isnan(score_override))
        hi->score = score_override;
    }
  }

  struct ScoredHunk final {
    Hunk* hunk;
    HunkInfo hi;
    bool operator<(const ScoredHunk& other) const {
      return this->hi.score < other.hi.score;
    }
  };

  using ScoredHunks = std::priority_queue<ScoredHunk, Vector<ScoredHunk>>;

  auto xit = [&](HunkTable::iterator it) {
    return boost::make_transform_iterator(
        it,
        [](const HunkTable::value_type& v) {
          return ScoredHunk { v.first, v.second };
        });
  };

  ScoredHunks scored_hunks(xit(ht.begin()), xit(ht.end()));

  while (this->get_memory_nbytes() >= target_memory_nbytes &&
         !scored_hunks.empty()) {
    const ScoredHunk sh = scored_hunks.top();
    scored_hunks.pop();
    if (!sh.hunk->is_pinned() && sh.hunk != protect_hunk)
      sh.hunk->deflate();
  }
}

void
QueryCache::unaccount_memory(size_t nbytes) noexcept
{
  assume(nbytes >= 0);
  assume(this->memory_nbytes >= nbytes);
  this->memory_nbytes -= nbytes;
  this->report();
}

void
QueryCache::report() noexcept
{
  if (report_allocations)
    pylog.debug("XXX memory_nbytes=%s nhunks=%s",
                this->memory_nbytes,
                this->get_nhunks());

  if (this->hunks.empty())
    assert(this->memory_nbytes == 0);

  if (safe_mode) {
    npy_intp memory_nbytes_manual = 0;
    for (const auto& hunk : this->hunks) {
      Hunk::ResourceUse mu = hunk.get_resource_use();
      memory_nbytes_manual += mu.memory_nbytes;
    }
    assert(memory_nbytes_manual == this->memory_nbytes);
  }
}

unique_pyarray
make_uninit_hunk_array(obj_pyref<QueryCache> qc,
                       unique_dtype dtype,
                       npy_intp nelem)
{
  return qc->make_uninit_array(std::move(dtype), nelem);
}

bool
QueryCache::should_use_heap(size_t nbytes) const noexcept
{
  return this->freelists.empty() ||
      nbytes < this->config.mmap_min_nbytes;
}

bool
QueryCache::should_spill_to_memory(size_t nbytes) const noexcept
{
  return nbytes < this->config.min_disk_spill_nbytes;
}

MmapShared
QueryCache::allocate_mmap(size_t nbytes)
{
  Freelist* fl = this->find_fl(nbytes);
  assume(!fl || nbytes <= fl->nbytes);
  if (fl && !fl->spare.empty()) {
      MmapShared section = std::move(fl->spare.back());
      fl->spare.pop_back();
      assume(section.nbytes() == fl->nbytes);
      return section;
  }
  return MmapShared::make(nbytes);
}

npy_intp
QueryCache::get_nhunks() const noexcept
{
  return this->hunks.size();
}

void
QueryCache::resize_mmap(MmapShared* map, size_t new_nbytes)
{
  // If the memory map we're resizing is a size on our freelist, round
  // the requested size up to the size of the next freelist entry so
  // that when we donate the mapping after we're done with it, we can
  // reuse it.
  Freelist* fl = this->find_fl(new_nbytes);
  if (fl) {
    assume(new_nbytes <= fl->nbytes);
    new_nbytes = fl->nbytes;
  }
  map->resize(new_nbytes);
}

HunkScore
QueryCache::get_new_hunk_score_override()
{
  return this->new_hunk_score_override;
}

void
QueryCache::set_new_hunk_score_override(HunkScore score_override)
{
  if (!safe_mode)
    throw_pyerr_msg(PyExc_AssertionError,
                    "can override hunk scores only in debug mode");
  this->new_hunk_score_override = score_override;
}

std::pair<String, UniqueFd>
QueryCache::make_cache_file(std::string_view label)
{
  // We use our own incrementing counter and create files only in a
  // private directory, so we don't need to detect EBUSY and retry: we
  // should never reuse file names.  Instead, just treat EBUSY like
  // any other fatal error.  The O_EXCL is just for sanity checking.
  String name = fmt("%s.%s", label, this->file_name_counter++);
  UniqueFd fd = xopenat(this->cachedir.get(),
                        name.c_str(),
                        O_CREAT | O_EXCL | O_RDWR, 0600);
  return std::pair(std::move(name), std::move(fd));
}

UniqueFd
QueryCache::open_cache_file(const String& name, int mode)
{
  return xopenat(
      this->cachedir.get(),
      name.c_str(),
      O_RDONLY);
}

void
QueryCache::delete_cache_file(const String& name) noexcept
{
  safe_unlinkat(this->cachedir.get(), name.c_str());
}

static
unique_pyref
QueryCache_get_nhunks(PyObject* self, void*)
{
  return make_pylong(
      pyref(self).as_unsafe<QueryCache>()->get_nhunks());
}

static
unique_pyref
QueryCache_get_memory_lwm_nbytes(PyObject* self, void*)
{
  return make_pylong(
      pyref(self).as_unsafe<QueryCache>()
      ->get_config().memory_lwm_nbytes);
}

static
unique_pyref
QueryCache_get_memory_hwm_nbytes(PyObject* self, void*)
{
  return make_pylong(
      pyref(self).as_unsafe<QueryCache>()
      ->get_config().memory_hwm_nbytes);
}

PyGetSetDef QueryCache::pygetset[] = {
  make_getset("nhunks",
              "Number of actively managed hunks",
              wraperr<QueryCache_get_nhunks>()),
  make_getset("memory_lwm_nbytes",
              "Low water mark for memory",
              wraperr<QueryCache_get_memory_lwm_nbytes>()),
  make_getset("memory_hwm_nbytes",
              "High water mark for memory",
              wraperr<QueryCache_get_memory_hwm_nbytes>()),
  { 0 },
};

PyMemberDef QueryCache::pymembers[] = {
  make_memberdef("block_size",
                 T_PYSSIZET,
                 offsetof(QueryCache, config.block_size),
                 READONLY,
                 "Preferred block size"),
  make_memberdef("memory_nbytes",
                 T_PYSSIZET,
                 offsetof(QueryCache, memory_nbytes),
                 READONLY,
                 "Memory bytes currently used"),
  make_memberdef("disk_nbytes",
                 T_PYSSIZET,
                 offsetof(QueryCache, disk_nbytes),
                 READONLY,
                 "Disk bytes currently used"),
  make_memberdef("st",
                 T_OBJECT,
                 (offsetof(QueryCache, st) +
                  unique_pyref::get_pyval_offset()),
                 READONLY,
                 "String table for blocks in this cache"),
  make_memberdef("mmap_min_nbytes",
                 T_PYSSIZET,
                 offsetof(QueryCache, config.mmap_min_nbytes),
                 /*flags=*/0,
                 "Minimum number of bytes to send to OS mmap"),
  make_memberdef("min_disk_spill_nbytes",
                 T_PYSSIZET,
                 offsetof(QueryCache, config.min_disk_spill_nbytes),
                 /*flags=*/0,
                 "Minimum number of bytes to spill to disk "
                 "after compression"),
  make_memberdef("tmpdir",
                 T_OBJECT,
                 (offsetof(QueryCache, config.tmpdir) +
                  unique_pyref::get_pyval_offset()),
                 READONLY,
                 "Temporary directory used by the cache"),
  { 0 },
};


PyMethodDef QueryCache::pymethods[] = {
  AUTOMETHOD(&QueryCache::clear,
             "Clear cache contents",
  ),
  AUTOMETHOD(&QueryCache::make_uninit_array,
             "Make a hunk of memory for output",
             (unique_dtype, dtype, no_default, convert_dtype)
             (npy_intp, nelem)
  ),
  AUTOMETHOD(&QueryCache::get_new_hunk_score_override,
             "Get score override for new hunks",
  ),
  AUTOMETHOD(&QueryCache::set_new_hunk_score_override,
             "Set score override for new hunks",
             (double, score_override)
  ),
  { 0 },
};

template<>
unique_obj_pyref<QueryCache>
PythonConstructor<QueryCache>::make(
    PyTypeObject* type,
    pyref args,
    pyref kwargs)
{
  return unique_obj_pyref<QueryCache>::make(
      type, QueryCacheConfig(args, kwargs));
}

QueryCacheConfig::QueryCacheConfig()
    : mmap_min_nbytes(page_size())
{}

QueryCacheConfig::QueryCacheConfig(pyref py_args, pyref py_kwargs)
    : QueryCacheConfig()
{
  auto args = PARSEPYARGS_V(
      (OPTIONAL_ARGS_FOLLOW)
      (size_t, memory_lwm_nbytes, this->memory_lwm_nbytes)
      (size_t, memory_hwm_nbytes, this->memory_hwm_nbytes)
      (size_t, disk_max_nbytes, this->disk_max_nbytes)
      (size_t, mmap_min_nbytes, this->mmap_min_nbytes)
      (size_t, block_size, this->block_size)
      (size_t, min_disk_spill_nbytes, this->min_disk_spill_nbytes)
      (pyref, tmpdir, pyref(this->tmpdir))
  )(py_args, py_kwargs);

  if (args.memory_lwm_nbytes > args.memory_hwm_nbytes)
    throw_pyerr_msg(PyExc_ValueError,
                    "low water mark greater than high water mark");

  this->memory_lwm_nbytes = args.memory_lwm_nbytes;
  this->memory_hwm_nbytes = args.memory_hwm_nbytes;
  this->disk_max_nbytes = args.disk_max_nbytes;
  this->mmap_min_nbytes = args.mmap_min_nbytes;
  this->block_size = args.block_size;
  this->min_disk_spill_nbytes = args.min_disk_spill_nbytes;
  this->tmpdir = args.tmpdir.addref();
}

PyTypeObject QueryCache::pytype = make_py_type<QueryCache>(
    "dctv._native.QueryCache",
    "Query cache",
    [](PyTypeObject* t) {
      t->tp_members = QueryCache::pymembers;
      t->tp_getset = QueryCache::pygetset;
      t->tp_methods = QueryCache::pymethods;
    }
);

void
init_query_cache(pyref m)
{
  register_type(m, &QueryCache::pytype);
}

}  // namespace dctv
