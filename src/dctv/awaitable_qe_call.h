// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include "pyutil.h"
#include "query.h"

namespace dctv {

struct AwaitableQeCall final : BasePyObject, SupportsGcClear {
  explicit AwaitableQeCall(std::unique_ptr<QeCall> request) noexcept;
  std::unique_ptr<QeCall> extract_request();
  unique_pyref next();
  unique_pyref send(pyref obj);

  int py_traverse(visitproc visit, void* arg) const noexcept;
  int py_clear() noexcept;

  static PyTypeObject pytype;
 private:
  std::unique_ptr<QeCall> request;
  bool drained = false;

  static PyAsyncMethods pyasync[];
  static PyMethodDef pymethods[];
};

void init_awaitable_qe_call(pyref m);

}  // namespace dctv

#include "awaitable_qe_call-inl.h"
