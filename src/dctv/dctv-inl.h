// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename Functor>
Finally<Functor>::Finally(Functor functor) noexcept
    : functor(std::move(functor))
{}

template<typename Functor>
Finally<Functor>::~Finally()
{
  this->functor();
}

template<typename T>
T*
PyAllocator<T>::allocate(std::size_t nr)
{
  size_t nbytes;
  if (unlikely(mul_overflow(nr, sizeof (T), &nbytes)))
    throw std::bad_alloc();
  return static_cast<T*>(_do_py_malloc(nbytes));
}

template<typename T>
void
PyAllocator<T>::deallocate(T* p, std::size_t) noexcept
{
  _do_py_free(p);
}

template<typename T>
size_t
PyAllocator<T>::max_size() const noexcept
{
  return std::numeric_limits<ssize_t>::max() / sizeof (T);
}

template<typename T, typename U>
bool
operator==(const PyAllocator<T>&, const PyAllocator<U>&) noexcept
{
  return true;
}

template<typename T, typename U>
bool
operator!=(const PyAllocator<T>&, const PyAllocator<U>&) noexcept
{
  return false;
}

template<typename T>
typename PyAllocator<T>::pointer
PyAllocator<T>::address(reference x) const noexcept
{
  return &x;
}

template<typename T>
typename PyAllocator<T>::const_pointer
PyAllocator<T>::address(const_reference x) const noexcept
{
  return &x;
}

template<typename Comparator>
template<typename Left, typename Right>
bool
InvertingComparator<Comparator>::operator()(Left&& left, Right&& right)
    const
{
  return !this->functor(std::forward<Left>(left),
                        std::forward<Right>(right));
}

template<typename T>
bool
mul_overflow(T a, T b, T* out) noexcept
{
  return __builtin_mul_overflow(a, b, out);
}

template<typename T>
bool
add_overflow(T a, T b, T* out) noexcept
{
  return __builtin_add_overflow(a, b, out);
}

}  // namespace dctv
