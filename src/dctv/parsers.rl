%%{
machine indexed_events;

action es {
  ev_start();
}

action ee {
  ev_end();
}

action ets {
  evtype_start();
}

action ete {
  evtype_end();       
}

action ues {
  on_user_event_start();
}

action uee {
  on_user_event_end();
}

c = [^\n];
spc = " ";
hexnum = "0x" [0-9a-f]+;
evsep = /:  */;

prefix = (c{16} | "<...>") "-" c* ": ";

standard_evtype = [a-zA-Z0-9_]+ >ets %ete;
standard_payload = c*;
standard_suffix = standard_evtype :> evsep <: standard_payload;

print_evtype = "print";
# TODO(dancol): figure out where this "s" comes from
print_payload = hexnum "s" evsep <: ((c*) >ues %uee);
print_suffix = print_evtype :> evsep <: print_payload;

tmr_evtype = "tracing_mark_write";
tmr_payload =  (c*) >ues %uee;
tmr_suffix = tmr_evtype :> evsep <: tmr_payload;

suffix = standard_suffix | print_suffix | tmr_suffix;
event = prefix :>> suffix <: "\n";
events = (event >es @ee)**;
main := events;
}%%
