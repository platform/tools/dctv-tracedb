// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "test_dummy.h"

#include "npyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "pyseq.h"
#include "query.h"
#include "result_buffer.h"

namespace dctv {

static
unique_pyref
test_dummy(PyObject*, pyref py_args)
{
  PARSEPYARGS(
      (pyref, chunk_iter)
      (pyref, result_cb)
      (QueryExecution*, qe)
  )(py_args);

  // make sure that ResultBuffer still works after move
  ResultBuffer<int32_t, int32_t, int32_t> results_orig(
      addref(result_cb), qe);
  auto results = std::move(results_orig);
  auto iter = PythonChunkIterator<int32_t, int64_t>(addref(chunk_iter));
  for (;!iter.is_at_eof(); iter.advance()) {
    auto [a, b] = iter.get();
    static_assert(std::is_same_v<decltype(a), int32_t>,
                  "a should be int32");
    static_assert(std::is_same_v<decltype(b), int64_t>,
                  "b should be int64");
    results.add(a + b, a - b, iter.get_index());
  }
  results.flush();
  return addref(Py_None);
}

static
unique_pyref
test_dummy_keywords(PyObject*, pyref py_args, pyref py_kwargs)
{
  PARSEPYARGS(
      (pyref, thing1)
      (OPTIONAL_ARGS_FOLLOW)
      (KWONLY_ARGS_FOLLOW)
      (bool, thing2, true)
  )(py_args, py_kwargs);
  return pylist::of(thing1, make_pylong(thing2));
}

static
unique_pyref
debug_hook(PyObject*, PyObject* obj)
{
  (void) obj;  // Let debugger name it.
  return addref(Py_None);
}



static PyMethodDef functions[] = {
  make_methoddef("_test_dummy",
                 wraperr<test_dummy>(),
                 METH_VARARGS,
                 "Test function for iteration code"),
  make_methoddef("_test_dummy_keywords",
                 wraperr<test_dummy_keywords>(),
                 METH_VARARGS | METH_KEYWORDS,
                 "Test for keyword parsing"),
  make_methoddef("debug_hook",
                 wraperr<debug_hook>(),
                 METH_O,
                 "Test function to break into debugger"),
  { 0 },
};

void
init_test_dummy(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
