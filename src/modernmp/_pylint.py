# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Teach pylint about our metaprogramming"""
import astroid
from astroid import MANAGER

def register(_linter):
  """Dummy?"""
  # Needed for registering the plugin.

def transform_client_connection(cls):
  """Pretend ClientConnectionBase has the API methods registered"""
  import modernmp.shm as shm  # pylint: disable=useless-import-alias
  # pylint: disable=protected-access
  for fn in shm._resman_api_functions:
    cls[fn.__name__] = fn

MANAGER.register_transform(
  astroid.ClassDef,
  transform_client_connection,
  lambda node: node.name == "ClientConnectionBase")

# Fix exception report bug

# pylint: disable=wrong-import-position
import pylint.checkers.exceptions
def _replacement_visit_classdef(_self, _cls):
  pass
pylint.checkers.exceptions.ExceptionRaiseLeafVisitor.visit_classdef = _replacement_visit_classdef
