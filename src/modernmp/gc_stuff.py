# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Object analysis for memory debugging"""

# pylint: disable=missing-docstring,unused-variable

import sys
import logging
from types import BuiltinFunctionType, MemberDescriptorType
import refcycle
import networkx as nx
from .util import cached_property

log = logging.getLogger(__name__)

class _ObjWrapper(object):
  def __init__(self, obj):
    self.obj = obj
  __slots__ = ["obj"]

class Wrappers(object):
  def __init__(self):
    self.__wrappers = {}
  def wrap(self, obj):
    obj_id = id(obj)
    wrapper = self.__wrappers.get(obj_id)
    if not wrapper:
      self.__wrappers[obj_id] = wrapper = _ObjWrapper(obj)
    return wrapper
  def wrap_nocreate(self, obj):
    return self.__wrappers[id(obj)]
  def by_id(self, obj_id):
    return self.__wrappers[obj_id]

def _fix_refcycle_annotations():
  from refcycle.annotations import (
    add_attr,
    type_based_references,
    add_function_references,
  )
  def _do_exception(obj, references):
    add_attr(obj, "__cause__", references)
    add_attr(obj, "__context__", references)
    add_attr(obj, "__traceback__", references)
  type_based_references.setdefault(BaseException, _do_exception)

  def _do_builtin_function(obj, references):
    add_function_references(obj, references)
    add_attr(obj, "__self__", references)
  type_based_references.setdefault(BuiltinFunctionType,
                                   _do_builtin_function)

  def _do_member_descriptor(obj, references):
    for attr in dir(obj):
      add_attr(obj, attr, references)
  type_based_references.setdefault(MemberDescriptorType,
                                   _do_member_descriptor)

_fix_refcycle_annotations()

def _disassemble_annotations(rc_graph):
  rc_graph_annotated = rc_graph.annotated()
  annotations_by_obj_id = {}
  for vertex in rc_graph_annotated.vertices:
    annotations_by_obj_id[vertex.id] = vertex.annotation
  annotations_by_edge_number = {}
  for edge in rc_graph_annotated.edges:
    annotations_by_edge_number[edge.id] = edge.annotation
  return annotations_by_obj_id, annotations_by_edge_number

def _remove_edge_if(graph, predicate):
  edges_to_remove = set()
  for src, dst, label in graph.edges(data="label"):
    if predicate(src, dst, label):
      edges_to_remove.add((src, dst))
  for src, dst in edges_to_remove:
    graph.remove_edge(src, dst)

def _filter_useless_cycles(wrappers, graph):
  # sys.modules is permanent
  wrap = wrappers.wrap
  all_module_dicts = set(wrap(module.__dict__)
                         for module in sys.modules.values())
  all_module_exports = set(wrap(module) for
                           module in sys.modules.values())
  for module in sys.modules.values():
    for export in module.__dict__.values():
      all_module_exports.add(wrap(export))
  edges_to_remove = set()
  def _predicate(src, dst, label):
    return (False
            # It's okay for functions to refer to globals of loaded
            # modules
            or (dst in all_module_dicts and label == "__globals__")
            # For namedtuple
            or (isinstance(dst.obj, dict) and
                label == "__globals__" and
                dst.obj.get("__name__", "").startswith("namedtuple_"))
            # Inter-module links are fine
            or (src in all_module_dicts and dst in all_module_exports)
            # Built-in function linkage to module: fine
            or (isinstance(src.obj, BuiltinFunctionType) and
                dst in all_module_exports and label == "__self__")
            # Internal goo
            or (label in ("__mro__", "__objclass__")))
  _remove_edge_if(graph, _predicate)

def _filter_reachable_from_sys_modules(wrappers, graph):
  wrap = wrappers.wrap
  sys_modules_wrapper = wrap(sys.modules)
  for node in list(nx.dfs_preorder_nodes(graph, sys_modules_wrapper)):
    graph.remove_node(node)

def make_heap_snapshot():
  rc_graph = refcycle.creators.snapshot()
  assert isinstance(rc_graph, refcycle.object_graph.ObjectGraph)
  annotations_by_obj_id, annotations_by_edge_number = \
    _disassemble_annotations(rc_graph)
  wrappers = Wrappers()
  wrap = wrappers.wrap
  wrap_nocreate = wrappers.wrap_nocreate
  nx_graph = nx.DiGraph()
  for obj in rc_graph.vertices:
    wrapper = wrap(obj)
    label = annotations_by_obj_id.get(id(obj))
    if label:
      nx_graph.add_node(wrapper, label=label)
    else:
      nx_graph.add_node(wrapper)
  for edge in rc_graph.edges:
    destination = wrap_nocreate(rc_graph.head(edge))
    source = wrap_nocreate(rc_graph.tail(edge))
    label = annotations_by_edge_number.get(edge)
    assert source in nx_graph
    assert destination in nx_graph
    if label:
      nx_graph.add_edge(source, destination, label=label)
    else:
      nx_graph.add_edge(source, destination)
  _filter_reachable_from_sys_modules(wrappers, nx_graph)
  _filter_useless_cycles(wrappers, nx_graph)
  return wrappers, nx_graph

class Segment:
  def __init__(self, wrappers, segment_graph):
    self.wrappers = wrappers
    self.graph = segment_graph
    log.debug("segment #obj=%r #scc=%r #root=%r",
              len(self.graph),
              len(self.sccs),
              len(self.root_scc_nrs))

  @cached_property
  def dict_to_owner(self):
    dict_to_owner = {}
    for src, dst, label in self.graph.edges(data="label"):
      if isinstance(dst.obj, dict) and label == "__dict__":
        dict_to_owner[dst] = src
    return dict_to_owner

  @cached_property
  def sccs(self):
    graph = self.graph
    return [graph.subgraph(scc)
            for scc in nx.strongly_connected_components(graph)]

  @cached_property
  def scc_nr_graph(self):
    return nx.condensation(self.graph, self.sccs)

  @cached_property
  def root_scc_nrs(self):
    return [scc_nr for scc_nr, in_degree in self.scc_nr_graph.in_degree
            if not in_degree]

  def analyze_scc_nr(self, scc_nr, obj=None):
    scc = self.sccs[scc_nr]
    if obj:
      obj_wrapper = self.wrappers.wrap(obj)
      if not obj_wrapper in scc:
        return

    log.debug("#scc=%r", len(scc))
    max_in = max(scc, key=lambda node: scc.in_degree[node])
    log.debug("max_in %r", type(max_in.obj))
    d2o = self.dict_to_owner
    def _dwim_lookup(wrapper):
      if wrapper in d2o:
        return "__dict__ of " + _dwim_lookup(d2o[wrapper])
      obj = wrapper.obj
      if isinstance(obj, dict) and "__builtins__" in obj:
        obj = obj.copy()
        del obj["__builtins__"]
      return repr(obj)

    for src, dst, label in scc.in_edges(max_in, data="label"):
      log.debug("%s -> %s -> %s",
                _dwim_lookup(src),
                repr(label) if label else "???",
                _dwim_lookup(dst))

  @cached_property
  def biggest_scc_nr(self):
    return max(self.scc_nr_graph,
               key=lambda scc_nr: len(self.sccs[scc_nr]))

  def analyze(self, obj):
    for scc_nr in self.scc_nr_graph:
      self.analyze_scc_nr(scc_nr, obj)

def analyze_heap(obj=None, snapshot=None):
  wrappers, graph = make_heap_snapshot()
  obj_wrapper = wrappers.wrap_nocreate(obj)
  if obj not in graph:
    log.debug("obj %r not in graph", obj)
    return

  segments = [segment for segment in nx.weakly_connected_components(graph)
              if len(segment) > 1]
  log.debug("#segments=%s", len(segments))
  for segment in segments:
    segment_graph = graph.subgraph(segment)
    if not nx.is_directed_acyclic_graph(segment_graph):
      Segment(wrappers, segment_graph).analyze(obj)
