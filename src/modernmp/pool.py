# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Manage a pool of apartments that can handle work items"""

import threading
import logging
from itertools import count

from .util import (
  CannotPickle,
  ChainableFuture,
  ClosingContextManager,
  die_due_to_fatal_exception,
  weak_bind,
)

from .apartment import (
  ApartmentAffinityMixin,
  ThreadPool,
)

from .shm import get_process_resmanc
from .process import WorkerProcess

log = logging.getLogger(__name__)

class ApartmentBrokerClosedException(Exception):
  """Exception raised for work on closed apartment"""

class ApartmentBroker(ClosingContextManager,
                      ApartmentAffinityMixin,
                      CannotPickle):
  """Distributes work among a pool of apartments

  This object is thread-safe with apartment affinity.
  """
  __init_done = False

  def __init__(self, nr_threads=0, nr_processes=0):
    super().__init__()
    self.__lock = threading.Lock()
    self.__call_gates = {}
    self.__ready_apartment_ids = []
    self.__apartment_waiters = []
    self.__apartment_id_sequence = count(1)
    self.__workers = []
    self.__death_keys = set()
    self.__init_done = True
    for _ in range(nr_threads):
      self.add_threaded_apartment()
    for _ in range(nr_processes):
      self.add_process_apartment()

  def __add_apartment(self, call_gate, worker):
    """Add a new apartment to this broker

    WORKER is a closable object that "owns" the added apartment and
    that we close when we close.
    """
    with self.__lock:
      self._check_apartment()
      if self.closed:
        raise ApartmentBrokerClosedException
      assert call_gate not in self.__call_gates.values()
      assert worker not in self.__workers
      apartment_id = next(self.__apartment_id_sequence)
      death_key = None
      self.__call_gates[apartment_id] = call_gate
      try:
        death_key = get_process_resmanc().add_death_callback(
          call_gate.apartment_oid,
          weak_bind(self.__call_close_in_apartment))
        self.__death_keys.add(death_key)
        self.__workers.append(worker)
      except:
        if death_key:
          get_process_resmanc().remove_death_callback(death_key)
          self.__death_keys.discard(death_key)
        if worker in self.__workers:
          del self.__workers[self.__workers.index(worker)]
        del self.__call_gates[apartment_id]
        raise
    self.__on_apartment_ready(apartment_id)

  def __call_close_in_apartment(self):
    future = self._apartment.call_async(self.close)
    if future.done():
      assert self.closed

  def add_threaded_apartment(self, nr_threads=1):
    """Add an independent thread apartment"""
    tp = ThreadPool(nr_threads)
    try:
      self.__add_apartment(tp.call_gate, tp)
    except:
      tp.close()
      raise

  def add_process_apartment(self):
    """Add a worker process as an apartment"""
    wp = WorkerProcess()
    try:
      self.__add_apartment(wp.call_gate, wp)
    except:
      wp.close()
      raise

  async def submit_work_async(self, fn):
    """Convenience wrapper for just doing work at some point"""
    while True:
      future = self.try_submit_work(fn)
      if future:
        return await future
      await self.wait_for_apartment_async()

  def wait_for_apartment_async(self):
    """Return a future that resolves when an apartment becomes available

    The future may resolve spuriously in rare cases; a caller that
    tries try_submit_work() after the future resolves may see that
    try_submit_work() call fail.  In this case, the caller should try
    wait_for_apartment_async() again.
    """
    future = ChainableFuture()
    with self.__lock:
      self._check_apartment()
      if self.__ready_apartment_ids:
        future.set_result(None)
      elif self.closed:
        raise ApartmentBrokerClosedException
      else:
        self.__apartment_waiters.append(future)
    return future

  def try_submit_work(self, fn):
    """Call FN in a brokered apartment

    Return a future that resolves when the work does.  If no apartment
    is available for performing this work, instead return None.
    Callers should use wait_for_apartment_async() in this latter case.
    """
    self._check_apartment()
    try:
      with self.__lock:
        if not self.__ready_apartment_ids:
          return None
        apartment_id = self.__ready_apartment_ids.pop()
        work_future = self.__call_gates[apartment_id].fn(fn).call_async()
      # Wire up the future with the lock unheld so that we can deal with
      # the future resolving synchronously.
      weak_cb = weak_bind(self.__on_apartment_ready)
      def _on_done(_future):
        weak_cb(apartment_id)
      work_future.add_done_callback(_on_done)
      return work_future
    except:
      # All errors should be embodied in the returned future, not
      # propagated out of this function.
      die_due_to_fatal_exception("future manipulation error")

  def __on_apartment_ready(self, apartment_id):
    ready_future = None
    with self.__lock:
      # if apartment_id isn't in our call gate dict, it's because
      # somebody removed that apartment while we had the lock released
      # (or we're racing with close) and in this case we do nothing.
      assert apartment_id not in self.__ready_apartment_ids
      if apartment_id in self.__call_gates:
        self._check_apartment()
        self.__ready_apartment_ids.append(apartment_id)
        while self.__apartment_waiters:
          future = self.__apartment_waiters.pop(0)
          if not future.cancelled():
            ready_future = future
            break
    # Resolve future with lock unheld!
    if ready_future:
      ready_future.set_result(None)

  def __on_apartment_death(self):
    """Called when one of our managed apartments dies"""
    self._check_apartment()
    self.close()

  def _do_close(self):
    with self.__lock:  # Synchronize with __on_apartment_ready
      for death_key in self.__death_keys:
        get_process_resmanc().remove_death_callback(death_key)
      self.__call_gates.clear()
      self.__ready_apartment_ids.clear()
    # _do_close is called exactly once, after close flag is set, and
    # all mutating operations check for the close flag, so it's okay
    # to do the rest of the cleanup work outside of the lock --- and
    # doing so can avoid deadlocks if future callbacks do
    # interesting things.
    for future in self.__apartment_waiters:
      future.set_exception(ApartmentBrokerClosedException())
    for worker in self.__workers:
      worker.close()
    self.__workers.clear()
    super()._do_close()

  def __del__(self):
    if self.__init_done:
      self.close()
