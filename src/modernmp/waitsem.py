# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Semaphore waitable with select"""
import os
import threading
from .util import unix_pipe

class WaitableSemaphore(object):
  """Intraprocess semaphore that works with mp_wait"""
  def __init__(self, value=0):
    assert isinstance(value, int)
    self.__value = value
    self.__lock = threading.Lock()
    self.__read_pipe, self.__write_pipe = unix_pipe()

  def fileno(self):
    """Return file that becomes readable when try_acquire will not block"""
    return self.__read_pipe.fileno()

  def get_value(self):
    """Return a snapshot of the current value of the semaphore.

    Useful primarily when using the semaphore as an event: i.e, when
    the counter moves in one direction.
    """
    return self.__value

  def try_acquire(self):
    """Try to down the semaphore; return true if successful"""
    with self.__lock:
      old_value = self.__value
      assert old_value >= 0
      if not old_value:
        return False
      self.__value = old_value - 1
      if old_value == 1:
        received = os.read(self.__read_pipe.fileno(), 1)
        assert received == b"."
      return True

  def try_acquire_all(self):
    """Try to acquire all pending counts.

    Return number of counts acquired.
    """
    with self.__lock:
      value = self.__value
      if not value:
        return 0
      self.__value = 0
      received = os.read(self.__read_pipe.fileno(), 1)
      assert received == b"."
      return value

  def release(self):
    """Up the semaphore"""
    with self.__lock:
      old_value = self.__value
      self.__value = old_value + 1
      if not old_value:
        os.write(self.__write_pipe.fileno(), b".")

  def __repr__(self):
    return "<WaitableSemaphore fds=({!r}, {!r}) value={!r}>".format(
      self.__read_pipe.fileno(),
      self.__write_pipe.fileno(),
      self.__value)
