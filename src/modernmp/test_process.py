# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for RobustProcess and related functionality"""

# pylint: disable=missing-docstring,redefined-outer-name,unused-argument

import logging
import os
import weakref
import signal
import time

import pytest

from .process import WorkerProcess
from .util_test import isolated_resman  # pylint: disable=unused-import
from .util_test import current_thread_apartment
from .util_test import busy_wait_for_predicate
from .util import unix_pipe, fdid
from .util_test import ValueProducer
from .shm import SharedObject
from .apartment import ApartmentQuitError, proxy_for, Apartment

log = logging.getLogger(__name__)

@pytest.yield_fixture()
def event_loop():
  with current_thread_apartment() as apartment:
    yield apartment

def _say_hello():
  return "hello {}".format(os.getpid())

@pytest.mark.asyncio
async def test_process_start(isolated_resman):
  worker = WorkerProcess()
  assert worker.pid != os.getpid()
  assert worker.pid != isolated_resman.getpid()
  msg = await worker.call_gate.fn(_say_hello).call_aio()
  assert msg == "hello {}".format(worker.pid)

def _maybe_fdid(fd):
  try:
    return fdid(fd)
  except OSError:
    return None

class _FdOwningThing(SharedObject):
  inst_wr = None
  def __init__(self):
    self.fd = unix_pipe()[0]
    self.fd_number_in_resman = self.fd.fileno()
    self.fdid_in_resman = fdid(self.fd_number_in_resman)
    assert not _FdOwningThing.inst_wr
    _FdOwningThing.inst_wr = weakref.ref(self)

@pytest.mark.asyncio
async def test_process_closes_fds_in_child(isolated_resman):
  thing = _FdOwningThing()
  opened_id = fdid(thing.fd.fileno())
  assert opened_id == thing.fdid_in_resman
  worker = WorkerProcess()
  opened_id_in_worker = \
    await worker.call_gate.fn(_maybe_fdid) \
                          .call_aio(thing.fd_number_in_resman)
  assert opened_id_in_worker != opened_id

@pytest.mark.asyncio
async def test_process_death_aborts_call(isolated_resman):
  from select import select
  from signal import SIGKILL
  worker = WorkerProcess()
  log.debug("XXX doomed worker apartment oid=%r rc=%r",
            worker.call_gate.apartment_oid,
            isolated_resman.get_refcounts(worker.call_gate.apartment_oid))

  forever_future = worker.call_gate.fn(select).call_async((), (), ())
  assert not forever_future.done()
  log.debug("XXXX killing worker")
  os.kill(worker.pid, SIGKILL)
  with pytest.raises(ApartmentQuitError):
    await forever_future

def _call_value_producer_and_add(x):
  return x() + 1

@pytest.mark.asyncio
async def test_call_gate_cache(isolated_resman):
  # Test the cache here instead of test_apartment.py because here we
  # have a separate process set up, and the cache only has an effect
  # in the inter-process case.
  assert not isolated_resman.snapshot_api_counts()
  wp = WorkerProcess()
  obj = ValueProducer(5)
  proxy = proxy_for(obj)
  log.debug("wp.pid=%r wpcgid=%r mycgid=%r proxyoid=%r",
            wp.pid,
            wp.call_gate.oid,
            Apartment.current().call_gate.oid,
            proxy.oid)
  val = wp.call_gate.fn(_call_value_producer_and_add)(proxy)
  assert val == 6
  snapshot = isolated_resman.snapshot_api_counts()
  assert "link_to_death_internal" in snapshot
  assert "make_resource" in snapshot
  log.debug("starting loop")
  for _ in range(10):
    wp.call_gate.fn(_call_value_producer_and_add)(proxy)
  snapshot = isolated_resman.snapshot_api_counts()
  for api, count in sorted(snapshot.items()):
    log.debug("api %-20s %s", api, count)

_saved_things = []
def _hang_on_to_thing(thing):
  _saved_things.append(thing)

@pytest.mark.asyncio
async def test_worker_automatic_death(isolated_resman):
  pr, pw = unix_pipe()
  wp = WorkerProcess()
  wp.call_gate.fn(_hang_on_to_thing)(pw)
  wp.call_gate.fn(_hang_on_to_thing)(wp)
  pw.close()
  isolated_resman.close()
  assert os.read(pr.fileno(), 1) == b""

def _monkeypatch_process_death(fd):
  # pylint: disable=protected-access
  from . import util
  def _hack_do_die_due_to_fatal_exception(msg, ex):
    # Loop forever so we rely on SIGHUP to be killed
    while True:
      time.sleep(0)
  util._do_die_due_to_fatal_exception = _hack_do_die_due_to_fatal_exception

@pytest.mark.asyncio
async def test_worker_automatic_death_sudden(isolated_resman):
  pr, pw = unix_pipe()
  wp = WorkerProcess()
  wp.call_gate.fn(_hang_on_to_thing)(pw)
  wp.call_gate.fn(_hang_on_to_thing)(wp)
  wp.call_gate.fn(_monkeypatch_process_death)(pw)
  pw.close()
  isolated_resman.set_critical(False)
  resman_pid = isolated_resman.getpid()
  isolated_resman.close()
  os.kill(resman_pid, signal.SIGKILL)
  assert os.read(pr.fileno(), 1) == b""

@pytest.mark.asyncio
async def test_child_reaping(isolated_resman):
  pr, pw = unix_pipe()
  exit_status = 4
  wp = WorkerProcess()
  wp.call_gate.fn(_hang_on_to_thing)(pw)
  pw.close()
  wp.call_gate.fn(os._exit).call_oneway(exit_status)  # pylint: disable=protected-access
  assert os.read(pr.fileno(), 1) == b""
  # If this check passes, we know we're reaping properly,
  # because there's no other way we could learn the process
  # exit status.
  def _is_exited():
    return isolated_resman.get_process_status(wp.oid) is not None
  busy_wait_for_predicate(_is_exited)
  status = isolated_resman.get_process_status(wp.oid)
  assert status == exit_status << 8
