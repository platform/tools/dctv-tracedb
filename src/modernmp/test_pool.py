# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for pool"""

# pylint: disable=missing-docstring,redefined-outer-name,unused-argument

import logging
import os
import threading
import asyncio
from contextlib import contextmanager
from functools import partial
from socket import MSG_DONTWAIT, socketpair
from select import select

import pytest

from .util_test import isolated_resman  # pylint: disable=unused-import
from .util_test import current_thread_apartment

from .pool import (
  ApartmentBroker,
)

from .apartment import (
  ApartmentQuitError,
)

log = logging.getLogger(__name__)
the_value = None  # shut up pylint

@pytest.yield_fixture()
def event_loop():
  with current_thread_apartment() as apartment:
    yield apartment

@pytest.mark.asyncio
async def test_empty(isolated_resman):
  with ApartmentBroker():
    pass

def _add_value(x):
  global the_value
  the_value += x
  return the_value

@pytest.mark.asyncio
async def test_hello_world(isolated_resman):
  global the_value
  the_value = 1
  with ApartmentBroker() as ab:
    ab.add_threaded_apartment()
    assert await ab.try_submit_work(partial(_add_value, 2)) == 3
    assert the_value == 3

_monitor = threading.Condition()
_waiting_threads = []

def _blocking_worker(idx):
  global _waiting_threads
  with _monitor:
    assert not _waiting_threads[idx]
    _waiting_threads[idx] = threading.current_thread()
    _monitor.notify_all()
    _monitor.wait_for(lambda: not _waiting_threads[idx])
  return idx

def _clear_blocking_worker(idx):
  with _monitor:
    _waiting_threads[idx] = None
    _monitor.notify_all()

@contextmanager
def _waiting_threads_cleanup():
  try:
    yield
  finally:
    with _monitor:  # Make sure they die
      for idx in range(len(_waiting_threads)):
        _clear_blocking_worker(idx)

@pytest.mark.asyncio
async def test_wait_for_work(isolated_resman):
  global _waiting_threads
  _waiting_threads = [None, None]
  with ApartmentBroker() as ab:
    with _waiting_threads_cleanup():
      ab.add_threaded_apartment()
      ab.add_threaded_apartment()
      future0 = ab.try_submit_work(partial(_blocking_worker, 0))
      future1 = ab.try_submit_work(partial(_blocking_worker, 1))
      assert future0
      assert not ab.try_submit_work(partial(_blocking_worker, 0))
      avail_future = ab.wait_for_apartment_async()
      assert not avail_future.done()
      with _monitor:
        _monitor.wait_for(lambda: all(_waiting_threads))
        threads = _waiting_threads[:]
        _clear_blocking_worker(0)
      assert await future0 == 0  # pylint: disable=compare-to-zero
      assert not future1.done()
      assert threads[0].is_alive()
      assert threads[1].is_alive()
      assert await avail_future is None
      future0 = ab.try_submit_work(partial(_blocking_worker, 0))
      with _monitor:
        _monitor.wait_for(lambda: all(_waiting_threads))
        _clear_blocking_worker(0)
      assert await future0 == 0  # pylint: disable=compare-to-zero
      _clear_blocking_worker(1)
      assert await future1 == 1
  assert not threads[0].is_alive()
  assert not threads[1].is_alive()

def _echo(value):
  return value

@pytest.mark.asyncio
async def test_async_work_queue(isolated_resman):
  with ApartmentBroker() as ab:
    ab.add_threaded_apartment()
    ab.add_threaded_apartment()
    nr_items = 10
    futures = [ab.submit_work_async(partial(_echo, item_nr))
               for item_nr in range(nr_items)]
    set(await asyncio.gather(*futures)) == set(range(nr_items))

@pytest.mark.asyncio
async def test_auto_close(isolated_resman):
  global _waiting_threads
  _waiting_threads = [None]
  ab = ApartmentBroker()
  with _waiting_threads_cleanup():
    ab.add_threaded_apartment(1)
    future0 = ab.try_submit_work(partial(_blocking_worker, 0))
    with _monitor:
      _monitor.wait_for(lambda: all(_waiting_threads))
      threads = _waiting_threads[:]
      _clear_blocking_worker(0)
    await future0
  thread, = threads
  assert thread.is_alive()
  del ab
  thread.join()
  assert not thread.is_alive()

_retained_thing = None
def _retain_thing(thing):
  global _retained_thing
  _retained_thing = thing

@pytest.mark.asyncio
async def test_process(isolated_resman):
  s1, s2 = socketpair()  # Detect process death, sort-of
  with ApartmentBroker() as ab:
    ab.add_process_apartment()
    assert await ab.submit_work_async(partial(_echo, "blarg")) == "blarg"
    assert await ab.submit_work_async(os.getpid) != os.getpid()
    assert not _retained_thing
    await ab.submit_work_async(partial(_retain_thing, s2))
    assert not _retained_thing  # Different process: different globals
    del s2
    with pytest.raises(BlockingIOError):
      s1.recv(1, MSG_DONTWAIT)  # Process still alive
  assert s1.recv(1) == b""  # Process died: read EOF

def _wait_forever():
  select((), (), ())
  assert False, "not reached"

def _die_horribly():
  os._exit(1)  # pylint: disable=protected-access

@pytest.mark.asyncio
async def test_group_exit(isolated_resman):
  """Test that if one contained apartment dies, they all do"""
  with ApartmentBroker() as ab:
    ab.add_process_apartment()
    ab.add_process_apartment()
    # Make sure we have a job that'll never finish
    future1 = asyncio.ensure_future(ab.submit_work_async(_wait_forever))
    # Make sure we've actually scheduled that work
    await asyncio.sleep(0)
    # In the other process, die horribly
    future2 = asyncio.ensure_future(ab.submit_work_async(_die_horribly))
    # Our original future should fail now
    with pytest.raises(ApartmentQuitError):
      await future1
    with pytest.raises(ApartmentQuitError):
      await future2
