# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Bootstrap script for spawn.py

When we use modernmp.spawn to make a new process, execution begins
with this file as the top-level script.  We slurp startup information
from the environment, set up initial process environment, then defer
to modernmp.spawn to complete initialization.
"""

# We could put this bootstrap code in spawn.py itself, but doing so
# would result in a weird file layout in spawn.py, since we'd have to
# run the bootstrap code before the majority of our imports (which would
# work only once we finished setting up the path).  It's cleaner to
# separate the early logic into its own module.

import sys
import os
import types
from base64 import b64decode
from pickle import loads

ENVVAR = "MODERNMP_SPAWN_DATA"

# _fixup_main_from_name and _fixup_main_from_path come from
# multiprocessing, which we don't want to import unless absolutely
# necessary.  Here, we've simplified the logic and omitted some
# obsolete compatibility hacks.

def _fixup_main_from_name(mod_name):
  current_main = sys.modules["__main__"]
  if mod_name == "__main__" or mod_name.endswith(".__main__"):
    return
  if getattr(current_main.__spec__, "name", None) == mod_name:
    return
  import runpy
  main_module = types.ModuleType("__mp_main__")
  main_content = runpy.run_module(mod_name,
                                  run_name="__mp_main__",
                                  alter_sys=True)
  main_module.__dict__.update(main_content)
  sys.modules["__main__"] = sys.modules["__mp_main__"] = main_module

def _fixup_main_from_path(main_path):
  current_main = sys.modules["__main__"]
  if getattr(current_main, "__file__", None) == main_path:
    return
  import runpy
  main_module = types.ModuleType("__mp_main__")
  main_content = runpy.run_path(main_path, run_name="__mp_main__")
  main_module.__dict__.update(main_content)
  sys.modules["__main__"] = sys.modules["__mp_main__"] = main_module

def _run_spawn_child(*,
                     name,
                     sys_path,
                     sys_argv,
                     startup_cwd,
                     main_module_name,
                     main_module_path,
                     startup_bundle,
                     inherited_fd_numbers):
  sys.path = sys_path
  sys.argv = sys_argv

  # See early util.py initialization
  setattr(os, "__modernmp_startup_cwd", startup_cwd)

  from setproctitle import setproctitle
  setproctitle(name)
  if main_module_name:
    _fixup_main_from_name(main_module_name)
  elif main_module_path:
    _fixup_main_from_path(main_module_path)

  from importlib import import_module
  spawn = import_module("modernmp.spawn")
  return spawn.spawned_child_main(startup_bundle, inherited_fd_numbers)

if __name__ == "__main__" and ENVVAR in os.environ:
  sys.exit(_run_spawn_child(**loads(b64decode(os.environ.pop(ENVVAR)))))
