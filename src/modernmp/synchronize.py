# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Synchronization primitives

Roughly analogous to the ones in multiprocessing.
"""

import logging

import _multiprocessing

from .shm import SharedObject, get_temp_name_sequence
from .reduction import register

SEM_PREFIX = "/mmp.sem."
RECURSIVE_MUTEX, SEMAPHORE = 0, 1
SEM_VALUE_MAX = _multiprocessing.SemLock.SEM_VALUE_MAX

log = logging.getLogger(__name__)

class ShmLock(SharedObject):
  """Interprocess lock holder."""

  # We can't, unfortunately, use the semlock object directly as the
  # IPC resource, since it doesn't support being the t target of weak
  # references. This holder, however, is suitable.  We use _semlock as
  # the name of our lock member so that we can just abuse
  # multiprocessing's __repr__ functions for lock objects.

  def __init__(self, kind, value, maxvalue):
    shm_name_sequence = get_temp_name_sequence()
    lock = None
    while not lock:
      name = SEM_PREFIX + next(shm_name_sequence)
      try:
        lock = _multiprocessing.SemLock(kind, value, maxvalue, name, False)
      except FileExistsError:
        pass
    self._semlock = lock

  def acquire(self, block=True):
    """Acquire the semaphore"""
    return self._semlock.acquire(block=block)

  def release(self):
    """Release the semaphore"""
    return self._semlock.release()

  def __enter__(self):
    return self._semlock.__enter__()

  def __exit__(self, exc_type, exc_val, exc_tb):
    return self._semlock.__exit__()

  def _resman_destroy(self):
    log.debug("Unlinking lock %r", self.name)
    try:
      _multiprocessing.sem_unlink(self.name)
    except IOError:
      pass

  @property
  def name(self):
    """Name of the sem_open file backing the semaphore"""
    return self._semlock.name

class Lock(ShmLock):
  """Non-recursive lock"""
  def __init__(self):
    ShmLock.__init__(self, SEMAPHORE, 1, 1)

  def __repr__(self):
    import multiprocessing.synchronize
    return multiprocessing.synchronize.Lock.__repr__(self)

class RLock(ShmLock):
  """Recursive lock"""
  def __init__(self):
    ShmLock.__init__(self, RECURSIVE_MUTEX, 1, 1)

  def __repr__(self):
    import multiprocessing.synchronize
    return multiprocessing.synchronize.RLock.__repr__(self)

class Semaphore(ShmLock):
  """Counting semaphore"""
  def __init__(self, value=1):
    ShmLock.__init__(self, SEMAPHORE, value, SEM_VALUE_MAX)

  def __repr__(self):
    import multiprocessing.synchronize
    return multiprocessing.synchronize.Semaphore.__repr__(self)

def _rebuild_semlock(handle, kind, maxvalue, name):
  # pylint: disable=protected-access
  return _multiprocessing.SemLock._rebuild(handle, kind, maxvalue, name)

def _reduce_semlock(sl):
  return _rebuild_semlock, (sl.handle, sl.kind, sl.maxvalue, sl.name)

register(_multiprocessing.SemLock, _reduce_semlock)
