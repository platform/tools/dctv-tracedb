# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Utilities for tests"""

from contextlib import contextmanager
import os
import sys
import threading
import asyncio
import logging
import select
import time

import pytest

# pylint: disable=missing-docstring

from .shm import (
  USE_RESMANC_THREAD,
  get_global_process_resmanc,
)
from .util import (
  current_event_loop,
  tls,
)

log = logging.getLogger(__name__)

@contextmanager
def run_resmanc():
  """Context manager that makes a resource manager current.

  The process hosting the resource manager depends on
  USE_RESMANC_THREAD.
  """
  assert tls.resmanc is None
  get_global_process_resmanc.reset()
  resmanc = get_global_process_resmanc()
  try:
    yield resmanc
  finally:
    if not resmanc.closed:
      exit_fd = resmanc.get_exit_fd()
      resmanc.close()
      get_global_process_resmanc.reset()
      poll = select.poll()
      poll.register(exit_fd, select.POLLIN)
      if not poll.poll(1000):
        msg = "timeout waiting for resman exit"
        if sys.exc_info()[1]:
          log.warning("%s", msg)
          raise  # pylint: disable=misplaced-bare-raise
        else:
          raise TimeoutError(msg)
      exit_file = os.fdopen(exit_fd.fileno(), closefd=False)
      if not USE_RESMANC_THREAD:
        buf = exit_file.read()
        if buf != ".":
          raise RuntimeError("resmanc did not close cleanly")

@contextmanager
def current_thread_apartment():
  """Context manager that runs code in the context of an apartment"""
  from .apartment import ApartmentFactory
  loop = asyncio.new_event_loop()
  with current_event_loop(loop):
    with ApartmentFactory("test").make_apartment().attached() as apartment:
      yield loop
    apartment.close()

@pytest.fixture()
def isolated_resman():
  with run_resmanc() as resmanc:
    yield resmanc

class ValueProducer(object):
  def __init__(self, value):
    self.value = value
  def __call__(self):
    if callable(self.value):
      return self.value()
    return self.value
  def do_call(self):
    return self()

_state_lock = threading.Lock()  # pylint: disable=invalid-name
_state_cond = threading.Condition(_state_lock)  # pylint: disable=invalid-name
_state = 0  # pylint: disable=invalid-name
def fetch_and_set_state(new_state):
  # pylint: disable=invalid-name
  global _state
  with _state_lock:
    old_state = _state
    _state = new_state
    _state_cond.notify_all()
  return old_state

def fetch_state():
  return _state

def wait_for_state_or_die(value, timeout=1.0):
  assert isinstance(value, int)
  with _state_lock:
    if not _state_cond.wait_for(lambda: _state == value, timeout):
      assert False, "timed out waiting for state change"

def dump_gc_info(thing):
  # pylint: disable=protected-access
  import gc
  gc.collect()
  myframe = sys._getframe(0)
  parentframe = sys._getframe(1)

  log.debug("Xthing=%s", repr(thing))
  for referrer in tuple(gc.get_referrers(thing)):
    if referrer is myframe or referrer is parentframe:
      continue
    log.debug("referer: %s", repr(referrer))
    for referrer2 in tuple(gc.get_referrers(referrer)):
      log.debug("  referer2: %s", repr(referrer2))
      for referrer3 in tuple(gc.get_referrers(referrer2)):
        log.debug("    referer3: %s", repr(referrer3))
        for referrer4 in tuple(gc.get_referrers(referrer3)):
          log.debug("      referer4: %s", repr(referrer4))


def busy_wait_for_predicate(predicate, timeout=5.0):
  """Wait for predicate to become true.

  Wait happens using a busy loop. USE ONLY FOR TEST.

  PREDICATE is a zero-arity callback. TIMEOUT is a number of seconds.
  """
  deadline = time.time() + timeout
  delay = 0.01
  while True:
    if predicate():
      return
    if time.time() >= deadline:
      raise TimeoutError()
    time.sleep(delay)
    delay = min(0.1, delay * 2)
