# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests of reduction"""

# pylint: disable=missing-docstring,consider-using-enumerate

from socket import socketpair, AF_UNIX

from .util import FdHolder, fdid, unix_pipe
from .reduction import fancy_pickle, fancy_unpickle

def _do_test_2e2(fd_things, *, check_fd_same_after_pickle=True):
  things1 = fd_things
  pickle_data, pc = fancy_pickle(things1)
  fds = pc.fdhs
  if check_fd_same_after_pickle:
    for idx in range(len(things1)):
      assert things1[idx].fileno() == fds[idx].fileno()
  fds_copy = [fd.dup() for fd in fds]
  things2 = fancy_unpickle(pickle_data, fds_copy)
  assert len(things1) == len(things2)
  for idx in range(len(things2)):
    assert things1[idx].fileno() != things2[idx].fileno()
    assert type(things1[idx]) is type(things2[idx])
    assert fdid(things1[idx].fileno()) == fdid(things2[idx].fileno())

def test_socket_pickle_e2e():
  _do_test_2e2(socketpair(AF_UNIX))

def test_fdholder_e2e():
  _do_test_2e2(unix_pipe())

def test_same_numeric_fd():
  fd = unix_pipe()[0]
  fds_hack = [fd, FdHolder.borrow(fd)]
  assert fds_hack[0].fileno() == fds_hack[1].fileno()
  _do_test_2e2(fds_hack, check_fd_same_after_pickle=False)
