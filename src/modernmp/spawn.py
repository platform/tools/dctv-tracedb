# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Simple process spawning with inherited stuff

Unlike multiprocessing's spawn, we avoid creating a useless-for-us
semaphore tracker process.  We're also able to use our pickle stuff
instead of multiprocessing's, so the subprocess data transmission
works like other forms of IPC.
"""

import os
from os.path import isabs, join as pjoin

import sys
import threading
import logging
from base64 import b64encode

from .util import (
  FdHolder,
  STARTUP_CWD,
  die_due_to_fatal_exception,
)

from .reduction import (
  fancy_pickle,
  fancy_unpickle,
)

log = logging.getLogger(__name__)
SPAWN_LOCK = threading.Lock()

def spawned_child_main(spawn_bundle, inherited_fd_numbers):
  """Main routine for spawned subprocesses

  SPAWN_BUNDLE is a pickled blob containing the caller-supplied logic
  to run inside the subprocess.
  """
  def _accept_fd(inherited_fd_number):
    assert os.get_inheritable(inherited_fd_number)
    os.set_inheritable(inherited_fd_number, False)
    return FdHolder.steal(inherited_fd_number)
  fn = fancy_unpickle(
    spawn_bundle,
    [_accept_fd(fd_number) for fd_number in inherited_fd_numbers])
  del spawn_bundle, inherited_fd_numbers
  return fn()

def start_subprocess(fn,
                     name="modernmp-spawned",
                     **kwargs):
  """Run FN a brand-new Python subprocess

  FN can be any pickle-able callable.  It cannot reference shm
  resources; to share shm-managed objects, consider a
  process.WorkerProcess, not this low-level facility.

  To prevent stray file descriptor inheritance, all subprocess
  creation in this process must synchronize on SPAWN_LOCK in
  this module.

  Remaining arguments are as for the subprocess.Popen constructor.
  FD-related and environment-related arguments are not currently
  supported.  (No need currently.)

  Return a subprocess Popen object.
  """
  import pickle
  from subprocess import Popen, _args_from_interpreter_flags
  from . import _spawn_init

  # We'd have to merge passed-in values with the ones we use
  # internally.  Easier to just forbid these for now.
  if __debug__:
    for feature in ("close_fds", "pass_fds", "env"):
      assert feature not in kwargs, "not implemented: " + feature

  pickle_data, pc = fancy_pickle(fn)

  inherited_fd_numbers = [fd.fileno() for fd in pc.fdhs]
  assert len(pc.fdhs) == len(set(inherited_fd_numbers)), \
    "shouldn't be preserving duplicate FDs"

  main_module = sys.modules["__main__"]
  main_module_name = getattr(main_module.__spec__, "name", None)
  if main_module_name:
    main_module_path = None
  else:
    main_module_path = getattr(main_module, "__file__", None)
    if main_module_path and not isabs(main_module_path):
      main_module_path = pjoin(STARTUP_CWD, main_module_path)

  startup_info = dict(
    name=name,
    sys_path=sys.path,
    sys_argv=sys.argv,
    startup_cwd=STARTUP_CWD,
    main_module_name=main_module_name,
    main_module_path=main_module_path,
    inherited_fd_numbers=inherited_fd_numbers,
    startup_bundle=bytes(pickle_data),
  )
  new_environ = os.environ.copy()
  init_blob = pickle.dumps(startup_info, pickle.HIGHEST_PROTOCOL)
  new_environ[_spawn_init.ENVVAR] = b64encode(init_blob)

  cmdline = ([sys.executable]
             + _args_from_interpreter_flags()
             + ["--", _spawn_init.__file__])

  with SPAWN_LOCK:
    try:
      assert not any(os.get_inheritable(fd) for fd in inherited_fd_numbers)
      for fd in inherited_fd_numbers:
        os.set_inheritable(fd, True)
      return Popen(args=cmdline,
                   close_fds=False,
                   env=new_environ,
                   **kwargs)
    finally:
      try:
        for fd in inherited_fd_numbers:
          os.set_inheritable(fd, False)
      except:
        die_due_to_fatal_exception("cleaning up from spawn")
