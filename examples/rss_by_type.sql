# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
SELECT SPAN SUM(IF(oom_score_adj < 0,
                   file + anon,
                   0bytes)) AS rss_oom_lt_zero,
            SUM(IF(oom_score_adj == 0,
                   file + anon,
                   0bytes)) AS rss_oom_eq_zero,
            SUM(IF(oom_score_adj BETWEEN 1 AND 200,
                   file + anon,
                   0bytes)) AS rss_fg,
            SUM(IF(oom_score_adj BETWEEN 201 AND 700,
                   file + anon, 0bytes)) AS rss_bg,
            SUM(IF(oom_score_adj > 700,
                   file + anon,
                   0bytes)) AS rss_cached,
            SUM(IF(oom_score_adj IS NULL,
                   file + anon,
                   0bytes)) AS rss_unknown,
            SUM(oom_score_adj IS NULL) AS nr_unknown_oom_score,
            COUNT(_ts) AS nr_processes,
FROM ((trace.memory.rss_stat_file_pages_p_pid SPAN INNER JOIN
       trace.memory.rss_stat_anon_pages_p_pid)
      SPAN LEFT JOIN trace.memory.oom_score_adj_p_pid)
GROUP AND UNION SPANS USING PARTITIONS
