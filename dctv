#!/bin/zsh -e
# -*- python -*-
# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""":;s="${0:P}";. "${s:h}/shebang.zsh" python3 -- "$s" "$@";:<<'' #"""
import sys
import os
from os.path import (
  dirname,
  exists,
  isdir,
  join as pjoin,
  realpath,
)

def _pretend_load_venv(venv):
  # Pretend we were executed in a venv by filtering out all site
  # packages and installing our venv's.
  import re
  bad_dir_pattern = re.compile(r"/(site|dist)-packages($|/)")
  sys.path[:] = [path for path in sys.path
                 if not bad_dir_pattern.search(path)]
  sys.path.insert(0, pjoin(venv,
                           "lib",
                           "python" + sys.version[:3],
                           "site-packages"))

def _very_early_init_paths():
  mydir = dirname(realpath(__file__))
  venv = os.environ.get("VIRTUAL_ENV")
  if not venv and exists(pjoin(mydir, "src", "dctv")):
    source_venv = pjoin(mydir, "venv")
    try:
      venv_stat = os.stat(pjoin(source_venv, "setup.stamp"))
    except FileNotFoundError:
      sys.stderr.write(
        "ERROR: running DCTV out of source distribution "
        "but venv not built: did you forget to run 'make dev'?\n")
      sys.exit(1)
    venv_python_stat = os.stat(pjoin(source_venv, "bin", "python"))
    our_python_stat = os.stat(sys.executable)
    if ((venv_python_stat.st_ino, venv_python_stat.st_dev) !=
        (our_python_stat.st_ino, our_python_stat.st_dev)):
      sys.stderr.write(
        "ERROR: venv built for different interpreter. "
        "Run 'make clean && make dev' to rebuild or use "
        "correct binary.\n")
      sys.exit(1)
    if our_python_stat.st_mtime > venv_stat.st_mtime:
      sys.stderr.write(
        "ERROR: venv out of date: interpreter is newer.  Rebuild using "
        "'make clean && make dev'.\n");
      sys.exit(1)
    _pretend_load_venv(source_venv)
  def _prepend_load_path(*path):
    full_path = pjoin(mydir, *path)
    assert isdir(full_path)
    sys.path.insert(0, full_path)
  _prepend_load_path("src")

def very_early_init():
  """Set up load paths"""
  required_pyver = (3, 8)
  our_pyver = sys.version_info[:len(required_pyver)]
  if our_pyver < required_pyver:
    sys.stderr.write("ERROR: DCTV requires Python {!r} but we have {!r}"
                     .format(required_pyver, our_pyver))
    sys.exit(1)
  if getattr(sys, "frozen", False):
    raise NotImplemented("TODO(dancol): restore freeze support")
  else:
    _very_early_init_paths()

if __name__ == "__main__":
  very_early_init()
  from dctv.main import main
  sys.exit(main(sys.argv[1:]))
