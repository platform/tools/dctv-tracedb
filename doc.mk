# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for documentation stuff
include doc-base.mk
-include x20-push.local

ifneq ($(X20_HOST),)
	X20_RSYNC_PREFIX:=$(X20_HOST):
	PRODACCESS:=ssh -t $(X20_HOST) prodaccess
else
	X20_RSYNC_PREFIX:=
	PRODACCESS:=prodaccess
endif
ifeq ($(X20_USER),)
	X20_USER:=$(USER)
endif
ifeq ($(X20_SUBDIR),)
	X20_SUBDIR=www/dctv
endif
X20_USER_PREFIX:=$(shell echo '$(X20_USER)' | head -c 2)
X20_DIR:=/google/data/rw/users/$(X20_USER_PREFIX)/$(X20_USER)/$(X20_SUBDIR)
RSYNC_FLAGS=-ar --no-perms --delete --delete-excluded
RSYNC_FLAGS+= --partial --copy-links -P --progress
RSYNC_FLAGS+=--exclude=*.syntax.ps --exclude=*.syntax.pdf
RSYNC_FLAGS+=--exclude=*.tcl --exclude=*.stamp

ALL_SYNTAX_DIAGRAMS:=$(shell tclsh doc/bubble-generator.tcl dump-all)
ALL_SYNTAX_PS:=$(patsubst %,doc/%.syntax.ps,$(ALL_SYNTAX_DIAGRAMS))

# Generate all syntax ps files by side effect of making the stamp The
# generation code below ensures that we only update a .syntax.ps file
# when it's actually changed so that we can skip the ps2pdf and
# pdftocairo steps for the diagrams that haven't changed.
$(ALL_SYNTAX_PS): doc/syntax-diagram-ps.stamp
doc/syntax-diagram-ps.stamp: doc/bubble-generator.tcl
doc/syntax-diagram-ps.stamp: doc/bubble-generator-data.tcl

define update-if-different
{ [ -e $2 ] && cmp -s $1 $2; } || mv -vf $1 $2;
endef

define strip-creation-date
sed --in-place -e '/^%%CreationDate:/d'	$1;
endef

define update-syntax-if-different
$(call strip-creation-date,doc/$1.syntax.tmp.ps)
$(call update-if-different,doc/$1.syntax.tmp.ps,doc/$1.syntax.ps)
endef

define update-syntax-ps-files
$(foreach name,$(ALL_SYNTAX_DIAGRAMS),\
 $(call update-syntax-if-different,$(name)))
endef

doc/syntax-diagram-ps.stamp:
	cd doc && wish ./bubble-generator.tcl
	echo 'Updating syntax ps files...'
	@$(update-syntax-ps-files)
	rm -f doc/*.syntax.tmp.ps
	touch $@

# Use a recursive make invocation to force reevaluation of
# ps->pdf->svg dependencies.
doc/syntax-diagram-svg.stamp: doc/syntax-diagram-ps.stamp
	$(MAKE) -f doc.mk regenerate-all-svg
	touch $@

# TODO(dancol): go back to using inkscape to generate the SVG,
# allowing text embedding to work.
doc/%.syntax.pdf: doc/%.syntax.ps
	ps2pdf -dEPSFitPage $< $@
doc/%.syntax.svg: doc/%.syntax.pdf doc/%.syntax.ps
	pdftocairo -svg $< $@

doc/all.html: doc/syntax-diagram-ps.stamp
doc/syntax_linkage.tcl:	doc/syntax-diagram-ps.stamp

ALL_SYNTAX_SVG:=$(patsubst %.syntax.ps,%.syntax.svg,$(ALL_SYNTAX_PS))
regenerate-all-svg: $(ALL_SYNTAX_SVG)

.PHONY: push-doc-x20
push-doc-x20: doc
	$(PRODACCESS)
	rsync $(RSYNC_FLAGS) doc/ $(X20_RSYNC_PREFIX)$(X20_DIR)
	printf "DCTV documentation is available at %s\n" \
		"https://$(X20_USER).users.x20web.corp.google.com/$(X20_SUBDIR)/index.xml"

.PHONY: all
all: doc/all.html doc/syntax-diagram-svg.stamp

.DEFAULT_GOAL:=all
