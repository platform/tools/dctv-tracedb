# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile for setting up the virtual environment
include venv-base.mk
include	native-deps-conf.mk

# N.B. Not the venv python3!
PYVER:=$(shell $(PYBIN) -c \
	'import sys; print("{}.{}".format(*sys.version_info[:2]))')
ifeq ($(PYVER),)
$(error "No $(PYBIN) binary found")
endif

PYPREFIX:=$(shell $(PYBIN) -c 'import sys; print(sys.prefix)')
ifeq ($(PYPREFIX),)
$(error "Could not determine $(PYBIN) prefix")
endif

prebuilt_dir:=$(PWD)/prebuilt/$(PYVER)/$(shell uname -s)/$(shell uname -m)

# Make setting PYTHONHOME to run dctv work (important for -O speed)
# EXPERIMENTAL!  Set COPY_WHOLE_ENVIRONMENT=1 to try it.
define copy-whole-environment
( \
	cd $(PYPREFIX) && \
	tar -cf- \
		--exclude='*/__pycache__*' \
		--exclude='./lib/python$(PYVER)/config-*' \
		./lib/python$(PYVER) \
) | ( cd $(VENV) && tar -xvf - )
endef

ifneq ($(COPY_WHOLE_ENVIRONMENT),1)
copy-whole-environment=
endif

bootstrap-dir:=$(PWD)/dep-bootstrap
bootstrap-download-cmd:=$(VENV)/run pip3 download --no-binary=:all: -d $(bootstrap-dir)
bootstrap-install-cmd:=$(VENV)/run pip3 install --no-binary=:all: --no-index -f $(bootstrap-dir)

# N.B. The packages explicitly installed below need to be installed
# with separate installation lines, as shown, to avoid weird version
# bootstrapping problems.  If you see packages mysteriously installed
# with version 0.0.0, you've probably broken this delicate ordering.
define do-bootstrap-packages
	$(1) wheel setuptools_scm
	$(1) pyrsistent cachecontrol futures-then
	$(1) --pre jsonschema
	$(1) poetry
endef

# Set up a bare venv with no installed packages except Python itself.
# pip3 does, however, work in this environment, via the run script.

define make-venv-bare
	rm -rf $(VENV)
	$(PYBIN) -m venv $(VENV)
	(cd $(VENV)/lib/python$(PYVER)/site-packages && tar -xvf $(bootstrap-dir)/tomlkit-bootstrap.tgz)
	sed -e 's,@VENV@,$(PWD)/$(VENV),g' < venv-run.sh.in > $(VENV)/run
	chmod +x $(VENV)/run
	$(copy-whole-environment)
endef

# Avoid dependencies to avoid unwanted reruns

define apply-abseil-patch
	patch -d $(ABSEIL_SRC_DIR) -p1 < $(1)
	$(newline)
endef

define apply-abseil-patches
	$(foreach patch,$(wildcard abseil-patches/*.patch),$(call apply-abseil-patch,$(patch)))
endef

.PHONY: venv-bare
venv-bare:
	$(make-venv-bare)

.PHONY: download-bootstrap-packages
download-bootstrap-packages:
	$(make-venv-bare)
	rm -rf $(bootstrap-dir)
	$(call do-bootstrap-packages, $(bootstrap-download-cmd))

.PHONY: install-bootstrap-packages
install-bootstrap-packages:
	$(make-venv-bare)
	$(call do-bootstrap-packages, $(bootstrap-install-cmd))

ifneq ($(DCTV_MAKE_DEV),1)
$(VENV)/setup.stamp: venv.mk
endif

$(VENV)/setup.stamp:
	$(make-venv-bare)
	$(call do-bootstrap-packages, $(bootstrap-install-cmd))
	$(install-abseil)
	touch $@

$(VENV)/poetry.stamp: $(VENV)/setup.stamp poetry.lock
	$(VENV)/run poetry install -v
	touch $@

$(VENV)/abseil.stamp: $(VENV)/setup.stamp native-deps-conf.mk
	rm -rf $(ABSEIL_SRC_DIR)
	cd $(VENV) && git clone -n $(abseil_upstream) $(ABSEIL_SRC_DIR_VENV_RELATIVE)
	cd $(ABSEIL_SRC_DIR) && git -c advice.detachedHead=false checkout $(abseil_rev)
	$(apply-abseil-patches)
	touch $@

pypict_dir:=$(VENV)/pypict
pypict_repo:="https://github.com/kmaehashi/pypict.git"
pypict_rev=c3d067a48543a4055e54a119ebb0da027f128a5f
pypict_setup=$(VENV)/run env -C $(pypict_dir) -uCXX -uCXXFLAGS python3 setup.py $(1)

$(VENV)/pypict.stamp: $(VENV)/poetry.stamp
	rm -rf $(pypict_dir)
	git clone $(pypict_repo) $(pypict_dir)
	env -C $(pypict_dir) git checkout $(pypict_rev)
	env -C $(pypict_dir) git submodule init
	env -C $(pypict_dir) git submodule update
	$(call pypict_setup, build_pict -v)
	$(call pypict_setup, build_ext -g --rpath=$(PWD)/$(VENV)/lib -f -v)
	$(call pypict_setup, install -f -v --skip-build)
	cp -vf $(pypict_dir)/pict/libpict.so $(VENV)/lib/
	touch $@

boost_dir:=$(VENV)/boost

$(VENV)/boost.stamp: $(VENV)/setup.stamp native-deps-conf.mk
	rm -rf $(boost_dir)
	mkdir -p $(boost_dir)
	env -C $(boost_dir) tar -xf $(PWD)/third_party/boost_$(boost_version).tar.xz
	env -C $(boost_dir) sh -c 'mv -v boost_*/* . && rmdir boost_*'
	touch $@

.PHONY: all
all: $(VENV)/poetry.stamp
all: $(VENV)/abseil.stamp
all: $(VENV)/pypict.stamp
all: $(VENV)/boost.stamp
all: dummy

.DEFAULT_GOAL:=all
