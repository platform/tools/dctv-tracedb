# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Hacks for setting up vendor paths in tests"""
import os
import sys
from os.path import dirname, realpath, join as pjoin
from importlib.machinery import SourceFileLoader

mydir = dirname(realpath(__file__))
SourceFileLoader("dctv_main", pjoin(mydir, "dctv")) \
  .load_module() \
  .very_early_init()

import logging
import dctv.util

from dctv.test_util import assertrepr_compare_hooks
def pytest_assertrepr_compare(config, op, left, right):
  for hook in assertrepr_compare_hooks:
    result = hook(config, op, left, right)
    if result is not None:
      return result
  return None

from dctv._native import set_min_radix_sort
set_min_radix_sort(0)
